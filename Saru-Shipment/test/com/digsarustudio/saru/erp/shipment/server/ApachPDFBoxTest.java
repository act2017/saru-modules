/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server;

import static org.junit.jupiter.api.Assertions.*;

import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * To test Apache PDF Box.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
class ApachPDFBoxTest {
	
	private static final String			PDF_ROOT	= "/Users/totsi/Desktop";
	private static final String			CONTENT		= "%PDF-1.4\n" + 
			"%âãÏÓ\n" + 
			"3 0 obj\n" + 
			"<</Filter/FlateDecode/Length 1198>>stream\n" + 
			"xµ[s£6Çßùç¡3ÍN·Tû¾9¶&ãÍÍdv¶Ãe³þöÀ\n" + 
			"OëI|ôÿ?ÒÑA¼K¤`!àw}þq¥Áy.àó7mG|w»¥\bb1ëA¬¥ûdEXæaè&;x\"Û8ÉJíú¾+¨¢×|Äúé DB²Ê^ð±÷tÍÅäi¸ïÂCf Úx¸Ï²þÌ£uã1áZRöÄýõj²ÞrÑï¶í½¼æ2\n" + 
			"I	x7YÜ I3?t3ú+¸Ñªhö#/ÈSÿø\n" + 
			"®|l\bmfßÜí6ðI*³ñS QQü}@BÞsFå§âðJ?[w(á{'¹?kÊðJØÜ~¸t¶W@çwçà¾etêwlüÖM<×Ä«5Äü'ÄGÑtÑÜr2w¤Gé½øÇp[¬Ò#K\\¥0¦jl¶Pá¨,Ò,CüÂdµJH~94>\n" + 
			"Û\n" + 
			"u$`þ\fQNç½U@A¶9ÞqÈÓÐXÁ¾w|$ iùg@&Üæ`D·±¿h\b¾vû·ôsýßåá+IF8?\fÅ9¿D\baÛ¶õn«¦:Þj«L</Î£l¼÷ÃØM\f),ÍÖº½Êxï_IBu»=7óã\bnf#,ä,+Êí¬Û­vÆ6¬Ý2y×êÕíT=cû-XÕb®\\Ïül7Âða@n\n" + 
			"æËûç§»9<L¦ó\\M¦7ç;Ì¾-¹k [¦vûÖ\n" + 
			"v\n" + 
			"%S1Þÿ\n" + 
			"X\f®£üHË½J2¶j5/©;ÛACGY~VASu8I(©Xm£!pº³M4t¥ÊÍU\bªóÀIBIÓ°l¨\n" + 
			"Ôm 	%aÙlL\b¢º·ÍDtn× AQv5êw,h,'tEÆ\\R?pÍÒÓå	Áôëz<J\bM¢Äªµ@6ønª©§E-(¡ÊS©Uk­CtSELd-jÙXÎi¡ÊSÄ©Uk­CtSEL\"¶j-°\\n®ÈÙ'hÊ\n" + 
			"\"/âöXVU\fm¾øvÔ#ðÑZË3M1ûQÅÑqX´M³È`»{ö	ÓxE`FR/ñ·ìö;ä&AÏxì´8qæ½³RaÌÌ4R±û[:Ä±Ù¾ù¯\n" + 
			"Ô!~'Iæ{âUÅF±Ç¥¿,?á¹¢1ýøëM/ÿ¬_>\n" + 
			"qNÏ¾g¥LóWß;±e\n" + 
			"öÿa[Ó´¢¢ØxÔþ9¸ÚCö£ûô×\n" + 
			"jjC cçBGZQGÎÅx}T^õ:¯Øc¦#ÈaMÈõÒy¹øe`:Ó{F»¦r[úùø}_{ûsóÃýÒß`ys}7qæÇqUUìá©Ê²<õ\bíG}èSÊÏ+åFíÑbz>®­rû¬ÀUôÉ¸ëùJÕvÖ·v{Ú¿D=òþ%êX¢>å©%êÑ^¢³WKÔ#>X¢áD½\n" + 
			"endstream\n" + 
			"endobj\n" + 
			"5 0 obj\n" + 
			"<</Contents 3 0 R/Type/Page/Resources<</ProcSet [/PDF /Text /ImageB /ImageC /ImageI]/Font<</F1 1 0 R/F2 2 0 R>>>>/Parent 4 0 R/MediaBox[0 0 595 842]>>\n" + 
			"endobj\n" + 
			"6 0 obj\n" + 
			"<</Filter/FlateDecode/Length 1312>>stream\n" + 
			"xµX[s¢H~çW73µY¨ÌQM*¦R»5/-t¤Mñßïi.FAHíZvó]ú>Æ7åb¥#h#X¹¿ë¸øãRÝÕ«r_Vÿà&Í×Gq2@\bÑ{ÊÙ=w)e²Ý¾'ºc<Î°Þ|»jÊ¯y¼+j¼çrOWÝiÅ>Å ÛxeîãÁIèn'\\)zÎIOªÕ+6hÝ~×åËx¡ço(D4ÄDçàPû¯{p\bç>ñ¨\n" + 
			"/ó#dïu=wH {Mïwº¾Cbê d18,ú¡.	=ÊYÇ&¨\\ZækKö°¦d\\ïøHèKðÜØËªrxÓ¾®gíÙõTXüÚ,òÒèü´§Ë`z×¸Ös1Îs LQ)b2Æ\n" + 
			"ÓæÐH`l*qø¼J=!ð`ÚO@Ö4À¤À»o`0¸Ã 7ËS'¢6õù`ÆmC~â\b¥!ì08yrÆ]á·ª0<Lîb:T,bÆÂ(ÙâÆ~LHû±O;¤qµe¡/ñ]ø!c¬?ÚdYËH=dâb±R·ôÏ´EkÑSUÌ1lKÒ¶WÏPáL]Ó(úz´\\Q¿¢fØ:\"XCàFÿ\n" + 
			"BC8ýgfÆiZisüßÃñ¡`I¼Ú$ç á&	ÁÐ°ÍêÆ×¡ßýõÏú¿K¶kÊ{8?ª8¿Ð4Í°mÛj¶jÙý­ÖZ<KÂ¸¿÷CíRC×ôáH\fía³÷á¸¿÷o;¬Y¸e¢ñ°®ç=,JV,ëúÍ¼Ù­ù2üpÛËä¡R5®Å§Ù©ñò»eø\fÜR´zI?ðã}Ã,n/îîð0}-nár:»¾½^ýóee\n" + 
			"rUñ.X¥áåVÛªmåßtÂ6ªé·@YæÈêí[Å0Æª­ «r²Æ a¶©I$P9[ã@xÌxT.B|³u*h«\f\n" + 
			"KÙ%GTÎÖ8$Þeëê¨\f*gëUÐV±¬ÒÈ§>ÐoOSLtÊ#²+ÒgU£¿ìk*W¦O¸Ò1õxMb(ó)3£)eIßÌ*Ó)iÑ¶¢ÑÌ+ói¯ñæ£Y´~er+È¼ÅhÆ[4óÊ<åö¨ñæ£)oE¤WâÉwLe%}Æ(&`E²«\n" + 
			"¯át´!Ü£Gû[s6µvª¿YxHn=Íà¨òÄììO«.9îïÄ#¯Kc6ìt+ö\n" + 
			"ËÅ$hJAÛ%2âí{¡x¦Dz¢ÿØqAÛÅñ4­éâv¤§¥Óß-·âäé½éê{¾ýð¾éà|MJç'Ïµï´/Èÿã¶öñ¤W\n" + 
			"Þ\"qQ]jR¼-w#ÕºöèNf½T6òóñÃaÞZùíá~¹ß`y}u7]=?-ÓÝ Ïl8¥ê'Ø\n" + 
			"È¼4ÚjÓá¸Ø-`@\n" + 
			"à<£R	Óë·¶I£ö(·ÀOD¹y\"ÊmÈQnrweùÅAS\n" + 
			"qÚ³Å{¾ fGì:;<T«*vB±Þ&¹eúâ %>ÍHðò	Tÿ¹Uó\n" + 
			"endstream\n" + 
			"endobj\n" + 
			"7 0 obj\n" + 
			"<</Contents 6 0 R/Type/Page/Resources<</ProcSet [/PDF /Text /ImageB /ImageC /ImageI]/Font<</F1 1 0 R/F2 2 0 R>>>>/Parent 4 0 R/MediaBox[0 0 595 842]>>\n" + 
			"endobj\n" + 
			"1 0 obj\n" + 
			"<</Subtype/Type1/Type/Font/BaseFont/Helvetica/Encoding/WinAnsiEncoding>>\n" + 
			"endobj\n" + 
			"2 0 obj\n" + 
			"<</Subtype/Type1/FirstChar 32/Type/Font/BaseFont/Helvetica/Encoding<</Type/Encoding/Differences[32/space 36/dollar 39/quotesingle/parenleft/parenright 43/plus/comma 46/period 48/zero/one/two/three/four/five/six/seven/eight/nine/colon 65/A/B/C/D/E/F/G/H/I/J 76/L/M/N/O/P/Q/R/S/T/U 87/W 89/Y/Z 97/a/b/c/d/e/f/g/h/i 107/k/l/m/n/o/p/q/r/s/t/u/v/w/x/y]>>/LastChar 121/Widths[278 0 0 0 556 0 0 191 333 333 0 584 278 0 278 0 556 556 556 556 556 556 556 556 556 556 278 0 0 0 0 0 0 667 667 722 722 667 611 778 722 278 500 0 556 833 722 778 667 778 722 667 611 722 0 944 0 667 611 0 0 0 0 0 0 556 556 500 556 556 278 556 556 222 0 500 222 833 556 556 556 556 333 500 278 556 500 722 500 500]>>\n" + 
			"endobj\n" + 
			"4 0 obj\n" + 
			"<</Kids[5 0 R 7 0 R]/Type/Pages/Count 2/ITXT(2.1.7)>>\n" + 
			"endobj\n" + 
			"8 0 obj\n" + 
			"<</Type/Catalog/Pages 4 0 R>>\n" + 
			"endobj\n" + 
			"9 0 obj\n" + 
			"<</ModDate(D:20180607124015+10'00')/CreationDate(D:20180607124015+10'00')/Producer(iText 2.1.7 by 1T3XT)>>\n" + 
			"endobj\n" + 
			"xref\n" + 
			"0 10\n" + 
			"0000000000 65535 f \n" + 
			"0000002993 00000 n \n" + 
			"0000003081 00000 n \n" + 
			"0000000015 00000 n \n" + 
			"0000003781 00000 n \n" + 
			"0000001281 00000 n \n" + 
			"0000001447 00000 n \n" + 
			"0000002827 00000 n \n" + 
			"0000003850 00000 n \n" + 
			"0000003895 00000 n \n" + 
			"trailer\n" + 
			"<</Info 9 0 R/ID [<018a38b38269317cb322cef546f09221><28d7393cec06ff63d50083886e396fc5>]/Root 8 0 R/Size 10>>\n" + 
			"startxref\n" + 
			"4017\n" + 
			"%%EOF";

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}
	
	@Test
	void testNewLineStringToPDF() throws IOException {
		String path = PDF_ROOT + "/newLineStringString.pdf";
		this.writeOrderSummaryPDFEnahnced(path, "This is the test file.\nThis is the second line", PDType1Font.HELVETICA_BOLD);
	}
	
	@Test
	void testNewLineFileStringToPDF() throws IOException {
		String path = PDF_ROOT + "/newLineFileString.pdf";
		this.writeOrderSummaryPDFEnahnced(path, CONTENT, PDType1Font.HELVETICA_BOLD);
	}
	
	@Test
	void testNormalStringToPDF() throws IOException {
		String path = PDF_ROOT + "/normalString.pdf";
		this.writeOrderSummaryPDF(path, "This is the test file. This is the second line", PDType1Font.HELVETICA_BOLD);
	}

	@Test
	void testWritePDFStringWithoutFontDataOutputStream() throws IOException {
		assertThrows(IllegalStateException.class, ()->{
			String path = PDF_ROOT + "/noFontString.pdf";
			this.writeOrderSummaryPDF(path, CONTENT, null);
		});
		
		
	}

	@Test
	void testWritePDFStringDataOutputStream() throws IOException {
		String path = PDF_ROOT + "/generalString.pdf";
		this.writeOrderSummaryPDFJava(path, CONTENT.getBytes());
	}

	@Test
	void testWritePDFBase64DecodedDataOutputStream() throws IOException {
		String path = PDF_ROOT + "/generalBase64Decode.pdf";
		byte[] decodedBytes = java.util.Base64.getDecoder().decode(CONTENT);
		this.writeOrderSummaryPDFJava(path, decodedBytes);
	}

	@Test
	void testFont1_TIMES_ROMAN() throws IOException {
		String path = PDF_ROOT + "/TIMES_ROMAN.pdf";
		
		String content = CONTENT.replace("\r", "").replace("\n", "");
		
		this.writeOrderSummaryPDF(path, content, PDType1Font.TIMES_ROMAN);
	}

	@Test
	void testFont1_TIMES_BOLD() throws IOException {
		String path = PDF_ROOT + "/TIMES_BOLD.pdf";
		this.writeOrderSummaryPDF(path, CONTENT, PDType1Font.TIMES_BOLD);
	}

	@Test
	void testFont1_TIMES_ITALIC() throws IOException {
		String path = PDF_ROOT + "/TIMES_ITALIC.pdf";
		this.writeOrderSummaryPDF(path, CONTENT, PDType1Font.TIMES_ITALIC);
	}

	@Test
	void testFont1_TIMES_BOLD_ITALIC() throws IOException {
		String path = PDF_ROOT + "/TIMES_BOLD_ITALIC.pdf";
		this.writeOrderSummaryPDF(path, CONTENT, PDType1Font.TIMES_BOLD_ITALIC);
	}

	@Test
	void testFont1_HELVETICA() throws IOException {
		String path = PDF_ROOT + "/HELVETICA.pdf";
		this.writeOrderSummaryPDF(path, CONTENT, PDType1Font.HELVETICA);
	}

	@Test
	void testFont1_HELVETICA_BOLD() throws IOException {
		String path = PDF_ROOT + "/HELVETICA_BOLD.pdf";
		this.writeOrderSummaryPDF(path, CONTENT, PDType1Font.HELVETICA_BOLD);
	}

	@Test
	void testFont1_HELVETICA_OBLIQUE() throws IOException {
		String path = PDF_ROOT + "/HELVETICA_OBLIQUE.pdf";
		this.writeOrderSummaryPDF(path, CONTENT, PDType1Font.HELVETICA_OBLIQUE);
	}

	@Test
	void testFont1_HELVETICA_BOLD_OBLIQUE() throws IOException {
		String path = PDF_ROOT + "/HELVETICA_BOLD_OBLIQUE.pdf";
		this.writeOrderSummaryPDF(path, CONTENT, PDType1Font.HELVETICA_BOLD_OBLIQUE);
	}

	@Test
	void testFont1_COURIERE() throws IOException {
		String path = PDF_ROOT + "/COURIER.pdf";
		this.writeOrderSummaryPDF(path, CONTENT, PDType1Font.COURIER);
	}

	@Test
	void testFont1_COURIER_BOLD() throws IOException {
		String path = PDF_ROOT + "/COURIER_BOLD.pdf";
		this.writeOrderSummaryPDF(path, CONTENT, PDType1Font.COURIER_BOLD);
	}

	@Test
	void testFont1_COURIER_OBLIQUE() throws IOException {
		String path = PDF_ROOT + "/COURIER_OBLIQUE.pdf";
		this.writeOrderSummaryPDF(path, CONTENT, PDType1Font.COURIER_OBLIQUE);
	}

	@Test
	void testFont1_COURIER_BOLD_OBLIQUE() throws IOException {
		String path = PDF_ROOT + "/COURIER_BOLD_OBLIQUE.pdf";
		this.writeOrderSummaryPDF(path, CONTENT, PDType1Font.COURIER_BOLD_OBLIQUE);
	}

	@Test
	void testFont1_ZAPF_DINGBATS() throws IOException {
		String path = PDF_ROOT + "/ZAPF_DINGBATS.pdf";
		this.writeOrderSummaryPDF(path, CONTENT, PDType1Font.ZAPF_DINGBATS);
	}

	@Test
	void testFont1_SYMBOL() throws IOException {
		String path = PDF_ROOT + "/SYMBOL.pdf";
		this.writeOrderSummaryPDF(path, CONTENT, PDType1Font.SYMBOL);
	}

	private void writeOrderSummaryPDF(String path, String content, PDFont font) throws IOException {
		PDDocument	document 	= new PDDocument();
		PDPage		page		= new PDPage();
		document.addPage(page);
		
		PDPageContentStream contentStream = new PDPageContentStream(document, page);
		
		contentStream.beginText();
		
		if(null != font) {
			contentStream.setFont(font, 12);
		}		
		contentStream.showText(content);
		contentStream.endText();		
		contentStream.close();
		
		document.save(path);
		document.close();
	}

	private void writeOrderSummaryPDFJava(String path, byte[] content) throws IOException {
		DataOutputStream os = new DataOutputStream(new FileOutputStream(path));
		os.write(content);
		os.close();
	}
	
	/**
	 * @see	https://stackoverflow.com/questions/19635275/how-to-generate-multiple-lines-in-pdf-using-apache-pdfbox
	 * @param path
	 * @param content
	 * @param font
	 * @throws IOException 
	 */
	private void writeOrderSummaryPDFEnahnced(String path, String content, PDFont font) throws IOException {
		PDDocument doc = null;
		
		try {
			doc = new PDDocument();
			PDPage page	= new PDPage();
			doc.addPage(page);
			
			PDPageContentStream contentStream = new PDPageContentStream(doc, page);
			float fontSize	= 25;
			float leading = 1.5f * fontSize;
			
			PDRectangle  mediaBox = page.getMediaBox();
			float margin	= 72;
			float width		= mediaBox.getWidth() - 2 * margin;
			float startX	= mediaBox.getLowerLeftX() + margin;
			float startY	= mediaBox.getUpperRightY() - margin;
			
			String[]	texts = content.split("\\n");
			List<String> lines = new ArrayList<>();
						
			for (String text : texts) {
				int lastSpace = -1;
				while(text.length() > 0) {
					int spaceIndex = text.indexOf(' ', lastSpace + 1);
					if(spaceIndex < 0) {
						spaceIndex = text.length();
					}
					
					String subString = text.substring(0, spaceIndex);
					float size = fontSize * font.getStringWidth(subString) / 1000;
					
					if(size > width) {
						if( lastSpace < 0 ) {
							lastSpace = spaceIndex;
						}
						
						subString = text.substring(0, lastSpace);
						lines.add(subString);
						
						text = text.substring(lastSpace).trim();
						lastSpace = -1;
					}else if(spaceIndex == text.length()) {
						lines.add(text);
						text = "";
					}else {
						lastSpace = spaceIndex;
					}				
				}				
			}

           contentStream.beginText();
           contentStream.setFont(font, fontSize);
           contentStream.newLineAtOffset(startX, startY);
           float currentY=startY;
           for (String line: lines)
           {
               currentY -=leading;

               if(currentY<=margin)
               {

                   contentStream.endText(); 
                   contentStream.close();
                   PDPage new_Page = new PDPage();
                   doc.addPage(new_Page);
                   contentStream = new PDPageContentStream(doc, new_Page);
                   contentStream.beginText();
                   contentStream.setFont(font, fontSize);
                   contentStream.newLineAtOffset(startX, startY);
                   currentY=startY;
               }
               contentStream.showText(line);
               contentStream.newLineAtOffset(0, -leading);
           }
           contentStream.endText(); 
           contentStream.close();

           doc.save(path);
			
		}finally {
           if (doc != null)
           {
               doc.close();
           }
		}
	}
}
