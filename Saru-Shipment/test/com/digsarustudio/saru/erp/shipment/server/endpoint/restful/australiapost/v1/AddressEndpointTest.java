/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.AustralianPostcode;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.Suburb;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.AustralianState;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostConfig;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.environment.sandbox.tog.AusPostMerchant;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationResponse;

/**
 * To test {@link AddressEndpoint}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
class AddressEndpointTest {
	private static AustraliaPostConfig CONFIG = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		CONFIG = AustraliaPostConfig.getInstance();		
		CONFIG.setMerchant(new AusPostMerchant());
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.AddressEndpoint#enableSandboxEnvironment()}.
	 */
	@Test
	void testEnableSandboxEnvironment() {

		AddressEndpoint.builder().setMerchant(CONFIG.getMerchant())
								  .enableSandboxEnvironment()
								  .build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.AddressEndpoint#enableProductionEnvironment()}.
	 */
	@Test
	void testEnableProductionEnvironment() {

		AddressEndpoint.builder().setMerchant(CONFIG.getMerchant())
								  .enableProductionEnvironment()
								  .build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.AddressEndpoint#validate(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb, com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode, com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State)}.
	 * @throws EndpointException 
	 */
	@Test
	void testValidate4116() throws EndpointException {
		
		Suburb 		suburb 		= Suburb.builder().setName("Calamvale")
												  .build();
		State 		state 		= AustralianState.Queensland;
		Postcode	postcode	= AustralianPostcode.builder().setCode("4116")
															  .build();

		AddressEndpoint endpoint = AddressEndpoint.builder().setMerchant(CONFIG.getMerchant())
								  							.enableProductionEnvironment()
								  							.build();
		SuburbValidationResponse result = endpoint.validate(suburb, postcode, state);
		
		assertNotNull(result);
		assertTrue(result.isFound());
		assertTrue((3 == result.getSuburbs().size()));
		
		for (String rtnSuburb : result.getSuburbsString()) {
			if( !rtnSuburb.equalsIgnoreCase("Calamvale") 
			  && !rtnSuburb.equalsIgnoreCase("Drewvale")
			  && !rtnSuburb.equalsIgnoreCase("Stretton")) {
				fail(rtnSuburb + " is not in 4116");
			}
		}
	}

	/**
	 * In this circumstance, the Suburb Validate API will throw a list of correct suburbs under this post code, but
	 * the "found" field will be set as false to indicate the incoming suburb is not in this postcode.<br>
	 * 
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.AddressEndpoint#validate(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb, com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode, com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State)}.
	 * @throws EndpointException 
	 */
	@Test
	void testValidateWrongSuburb() throws EndpointException {
		
		Suburb 		suburb 		= Suburb.builder().setName("Calamvale")
												  .build();
		State 		state 		= AustralianState.Queensland;
		Postcode	postcode	= AustralianPostcode.builder().setCode("4119")
															  .build();

		AddressEndpoint endpoint = AddressEndpoint.builder().setMerchant(CONFIG.getMerchant())
								  							.enableProductionEnvironment()
								  							.build();
		SuburbValidationResponse result = endpoint.validate(suburb, postcode, state);
		
		assertNotNull(result);
		assertFalse(result.isFound());
		assertNotNull(result.getSuburbs());
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.AddressEndpoint#validate(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb, com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode, com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State)}.
	 * @throws EndpointException 
	 */
	@Test
	void testValidateWrongState() throws EndpointException {
		
		Suburb 		suburb 		= Suburb.builder().setName("Calamvale")
												  .build();
		State 		state 		= AustralianState.Victoria;
		Postcode	postcode	= AustralianPostcode.builder().setCode("4116")
															  .build();

		AddressEndpoint endpoint = AddressEndpoint.builder().setMerchant(CONFIG.getMerchant())
								  							.enableProductionEnvironment()
								  							.build();
		SuburbValidationResponse result = endpoint.validate(suburb, postcode, state);
		
		assertNotNull(result);
		assertFalse(result.isFound());
		assertNull(result.getSuburbs());
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.AddressEndpoint#validate(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb, com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode, com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State)}.
	 * @throws EndpointException 
	 */
	@Test
	void testValidateWrongStateAndPostcode() throws EndpointException {
		
		Suburb 		suburb 		= Suburb.builder().setName("Calamvale")
												  .build();
		State 		state 		= AustralianState.Victoria;
		Postcode	postcode	= AustralianPostcode.builder().setCode("2100")
															  .build();

		AddressEndpoint endpoint = AddressEndpoint.builder().setMerchant(CONFIG.getMerchant())
								  							.enableProductionEnvironment()
								  							.build();
		SuburbValidationResponse result = endpoint.validate(suburb, postcode, state);
		
		assertNotNull(result);
		assertFalse(result.isFound());
		assertNull(result.getSuburbs());
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.AddressEndpoint#builder()}.
	 */
	@Test
	void testBuilder() {
		AddressEndpoint.builder();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#setMerchant(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails)}.
	 */
	@Test
	void testSetMerchant() {
		AddressEndpoint.builder().setMerchant(CONFIG.getMerchant());
	}

}
