/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.AustralianPostcode;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.AustralianState;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostConfig;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.environment.sandbox.tog.AusPostMerchant;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.environment.sandbox.tog.AustraliaPostPostageProducts;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.LabelGenerationRequestStatuses;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureTransitCover;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemHeight;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemLength;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemWeight;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemWidth;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentAddressLines;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentBusinessName;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentEmail;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentPersonalName;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentPhoneNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentSuburb;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.SenderReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelBranded;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormat;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatGroup;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatLeftOffset;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatTopOffset;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.javabean.LabelPreference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.javabean.LabelPreferenceType;

/**
 * To test {@link LabelEndpoint}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
class LabelEndpointTest {
	private static AustraliaPostConfig CONFIG = null;
	private static com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference PREFERENCE	= null;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		CONFIG = AustraliaPostConfig.getInstance();		
		CONFIG.setMerchant(new AusPostMerchant());
		

		
		@SuppressWarnings("static-access")
		LabelFormat format = LabelFormat.builder().setGroup(LabelFormatGroup.ParcelPost)
												  .setLayout(LabelFormatGroup.ParcelPost.ThermalLableA6_1pp)
												  .setLabelBranded(LabelBranded.builder().setBranded()
														  								 .build())
												  
												  .setLeftOffset(LabelFormatLeftOffset.builder().setValue(0)
														  										.build())
												  .setTopOffset(LabelFormatTopOffset.builder().setValue(0)
														  									  .build())
												  .build();
		
		
		
		PREFERENCE = LabelPreference.builder().setType(LabelPreferenceType.Print)
											  .addLabelFormat(format)
											  .build();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	} 
	
	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.LabelEndpoint#enableSandboxEnvironment()}.
	 */
	@Test
	void testEnableSandboxEnvironment() {

		ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
								  .enableSandboxEnvironment()
								  .build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.LabelEndpoint#enableProductionEnvironment()}.
	 */
	@Test
	void testEnableProductionEnvironment() {

		ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
								  .enableProductionEnvironment()
								  .build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.LabelEndpoint#create(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment)}.
	 * @throws EndpointException 
	 */
	@Test
	void testCreate() throws EndpointException {		
		Shipment.Builder shipmentBuilder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.builder();
		shipmentBuilder.setShipmentReference(this.getShipmentReference());
		shipmentBuilder.setSenderReferences(this.getSenderReferences());
		shipmentBuilder.setEmailTracking(this.isEmailTracking());
		shipmentBuilder.setSender(this.getSender());
		shipmentBuilder.setReceiver(this.getReceiver());
		shipmentBuilder.addItem(this.getItem1());
		shipmentBuilder.addItem(this.getItem2());
		shipmentBuilder.addItem(this.getItem3());
		
		Shipment result = this.createShipment(shipmentBuilder.build());
		
		assertNotNull(result);
		assertNotNull(result.getShipmentId());
		assertTrue(result.getShipmentId().validate());
		
		//Create label
		LabelEndpoint endpoint = LabelEndpoint.builder().setMerchant(CONFIG.getMerchant())
														.enableSandboxEnvironment()
														.build();
		
		try {
			LabelCollection labels = endpoint.create(result, PREFERENCE);
			assertNotNull(labels);
			assertTrue(labels.validate());
			
			for (Label label : labels.getLabels()) {
				assertNotNull(label.getRequestId(), String.format("Msg: %s, Code: %s", endpoint.getGenerationProgressMessage(), endpoint.getGenerationProgressCode()));
				
				System.out.println("testCreate::Request Id: " + label.getRequestId().getValue());
			}		
		}catch(EndpointException e) {
			this.printErrorMessage(endpoint.getOperationErrors(), null);
			
			
			throw e;
		}
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.LabelEndpoint#get(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label)}.
	 * @throws EndpointException 
	 * @throws InterruptedException 
	 */
	@Test
	void testGetLabel() throws EndpointException, InterruptedException {
		Shipment.Builder shipmentBuilder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.builder();
		shipmentBuilder.setShipmentReference(this.getShipmentReference());
		shipmentBuilder.setSenderReferences(this.getSenderReferences());
		shipmentBuilder.setEmailTracking(this.isEmailTracking());
		shipmentBuilder.setSender(this.getSender());
		shipmentBuilder.setReceiver(this.getReceiver());
		shipmentBuilder.addItem(this.getItem1());
		shipmentBuilder.addItem(this.getItem2());
		shipmentBuilder.addItem(this.getItem3());
		
		Shipment result = this.createShipment(shipmentBuilder.build());
		
		assertNotNull(result);
		assertNotNull(result.getShipmentId());
		assertTrue(result.getShipmentId().validate());
		
		//Create label
		LabelEndpoint endpoint = LabelEndpoint.builder().setMerchant(CONFIG.getMerchant())
														.enableSandboxEnvironment()
														.build();
		
		try {
			LabelCollection createdLabels = endpoint.create(result, PREFERENCE);
			assertNotNull(createdLabels);
			assertTrue(createdLabels.validate());
			
			//After the label generation requested, it takes over 2 sec to generated a label.
			Thread.sleep(3000);
			
			for (Label label : createdLabels.getLabels()) {
				assertNotNull(label.getRequestId(), String.format("Msg: %s, Code: %s", endpoint.getGenerationProgressMessage(), endpoint.getGenerationProgressCode()));
				
				System.out.println("Request Id: " + label.getRequestId().getValue());
				
				LabelCollection labelsWithUrl = endpoint.get(label);
				
				assertNotNull(labelsWithUrl);
				assertNotNull(labelsWithUrl.getLabels());				
				
				for (Label labelWithUrl : labelsWithUrl.getLabels()) {
					assertNotNull(labelWithUrl, "Reqeust Id = " + labelWithUrl.getRequestId().getValue());
					assertNotNull(labelWithUrl.getURL(), "Reqeust Id = " + labelWithUrl.getRequestId().getValue() + ", Status = " + labelWithUrl.getStatus().getValue());
					assertNotNull(labelWithUrl.getURL().getValue(), "Reqeust Id = " + labelWithUrl.getRequestId().getValue() + ", Status = " + labelWithUrl.getStatus().getValue());
					assertFalse(labelWithUrl.getURL().getValue().isEmpty(), "Reqeust Id = " + labelWithUrl.getRequestId().getValue() + ", Status = " + labelWithUrl.getStatus().getValue());
					
					System.out.println(String.format("testGet::Reqeust Id: %s, Url: %s", labelWithUrl.getRequestId().getValue(), labelWithUrl.getURL().getValue()));
				}
			}		
		}catch(EndpointException e) {
			this.printErrorMessage(endpoint.getOperationErrors(), e.getMessage());
			
			
			throw e;
		}
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.LabelEndpoint#get(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label)}.
	 * @throws EndpointException 
	 * @throws InterruptedException 
	 */
	@Test
	void testListShipment2GetLabel() throws EndpointException, InterruptedException {
		Shipment.Builder shipmentBuilder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.builder();
		shipmentBuilder.setShipmentReference(this.getShipmentReference());
		shipmentBuilder.setSenderReferences(this.getSenderReferences());
		shipmentBuilder.setEmailTracking(this.isEmailTracking());
		shipmentBuilder.setSender(this.getSender());
		shipmentBuilder.setReceiver(this.getReceiver());
		shipmentBuilder.addItem(this.getItem1());
		shipmentBuilder.addItem(this.getItem2());
		shipmentBuilder.addItem(this.getItem3());
		
		Shipment result = null;
		
		ShipmentEndpoint endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  .enableSandboxEnvironment()
															  .build();
		
		try {
			result = endpoint.create(shipmentBuilder.build());	
		}catch(EndpointException e) {
			this.printErrorMessage(endpoint.getOperationErrors(), e.getMessage());
			throw e;
		}
		
		assertNotNull(result);
		assertNotNull(result.getShipmentId());
		assertTrue(result.getShipmentId().validate());
		
		//Create label
		LabelEndpoint labelEndpoint = LabelEndpoint.builder().setMerchant(CONFIG.getMerchant())
															.enableSandboxEnvironment()
															.build();
		
		try {
			LabelCollection createdLabels = labelEndpoint.create(result, PREFERENCE);
			assertNotNull(createdLabels);
			assertTrue(createdLabels.validate());
			
			//After the label generation requested, it takes over 2 sec to generated a label.
			Thread.sleep(3000);
			
			for (Label label : createdLabels.getLabels()) {
				assertNotNull(label.getRequestId(), String.format("Msg: %s, Code: %s", labelEndpoint.getGenerationProgressMessage(), labelEndpoint.getGenerationProgressCode()));
				
				System.out.println("Request Id: " + label.getRequestId().getValue());
			}	
			
			Shipment shipmentWithLabelInfo = endpoint.get(result);					
			
			assertNotNull(shipmentWithLabelInfo);
			assertNotNull(shipmentWithLabelInfo.getItems());				
			for (Item item : shipmentWithLabelInfo.getItems().getItems()) {
				Label labelWithUrl = item.getLabel();
				
				assertNotNull(labelWithUrl, "Shipment Id:" + result.getShipmentId().getValue() + ", Item Id: " + item.getItemId().getValue());
				assertNotNull(labelWithUrl.getPrintableURL(), "Shipment Id:" + result.getShipmentId().getValue() + ", Item Id: " + item.getItemId().getValue());
				assertNotNull(labelWithUrl.getPrintableURL().getValue(), "Shipment Id:" + result.getShipmentId().getValue() + ", Item Id: " + item.getItemId().getValue());
				assertFalse(labelWithUrl.getPrintableURL().getValue().isEmpty(), "Shipment Id:" + result.getShipmentId().getValue() + ", Item Id: " + item.getItemId().getValue());
				
				assertEquals(LabelGenerationRequestStatuses.Available, item.getLabel().getStatus().getLabelStatus());
				
				System.out.println(String.format("testList::Shipment Id: %s, Item Id: %s, URL: %s.", result.getShipmentId().getValue()
																								   , item.getItemId().getValue()
																								   , labelWithUrl.getPrintableURL().getValue()));
			}
			
		}catch(EndpointException e) {
			this.printErrorMessage(endpoint.getOperationErrors(), null);
			
			
			throw e;
		}
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.LabelEndpoint#insert(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Disabled
	@Test
	void testInsert() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.LabelEndpoint#update(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Disabled
	@Test
	void testUpdateLabelResponseCallbackOfLabel() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.LabelEndpoint#update(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)}.
	 */
	@Disabled
	@Test
	void testUpdateLabelResponseCallbackOfLabelInteger() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.LabelEndpoint#list(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Disabled
	@Test
	void testListLabelResponseCallbackOfCollectionResponseOfLabel() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.LabelEndpoint#list(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer, java.lang.Integer)}.
	 */
	@Disabled
	@Test
	void testListLabelResponseCallbackOfCollectionResponseOfLabelIntegerInteger() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.LabelEndpoint#delete(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Disabled
	@Test
	void testDeleteLabelResponseCallbackOfLabel() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.LabelEndpoint#delete(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)}.
	 */
	@Disabled
	@Test
	void testDeleteLabelResponseCallbackOfLabelInteger() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.LabelEndpoint#get(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Disabled
	@Test
	void testGetLabelResponseCallbackOfLabel() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.LabelEndpoint#builder()}.
	 */
	@Test
	void testBuilder() {
		LabelEndpoint.builder();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#setMerchant(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails)}.
	 */
	@Test
	void testSetMerchant() {

		LabelEndpoint.builder().setMerchant(CONFIG.getMerchant())
							  .enableSandboxEnvironment()
							  .build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#getOperationErrors()}.
	 */
	@Disabled
	@Test
	void testGetOperationErrors() {
		fail("Not yet implemented");
	}


	private ShipmentReference getShipmentReference() {
		ShipmentReference.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentReference.builder();
		
		builder.setValue("XYZ-001-01");
				
		return builder.build();
	}
	
	private SenderReferenceCollection getSenderReferences() {
		SenderReferenceCollection.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.SenderReferenceCollection.builder();
		
		builder.addSenderReference(SenderReference.builder().setReference("Order 001")
															.build());

		builder.addSenderReference(SenderReference.builder().setReference("SKU-1, SKU-2, SKU-3")
															.build());
		return builder.build();
	}
	
	private Boolean isEmailTracking() {
		return true;
	}
	
	private Sender getSender() {
		Sender.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Sender.builder();
		
		builder.setName(AusPostShipmentPersonalName.builder().setFullName("Otto Hung")
															 .build());
		
		builder.setAddressLines(AusPostShipmentAddressLines.builder().addAddress("2, 74 perrin drive")
																	 .build());
		builder.setSuburb(AusPostShipmentSuburb.builder().setName("MELBOURNE")
														 .build());
		builder.setState(AustralianState.Victoria);
		builder.setPostcode(AustralianPostcode.builder().setCode("3000")
														.build());
		builder.setPhone(AusPostShipmentPhoneNumber.builder().setNumber("0732723318")
															 .build());
		builder.setEmail(AusPostShipmentEmail.builder().setAddress("it@togauto.com.au")
													   .build());
		
		return builder.build();
	}
	
	private Receiver getReceiver() {
		Receiver.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Receiver.builder();
		
		builder.setName(AusPostShipmentPersonalName.builder().setFullName("Jane Smith")
															 .build());
		builder.setBusinessName(AusPostShipmentBusinessName.builder().setName("Smith Pty Ltd")
																	 .build());
		builder.setAddressLines(AusPostShipmentAddressLines.builder().addAddress("123 Centre Road")
																	 .build());
		builder.setSuburb(AusPostShipmentSuburb.builder().setName("Sydney")
														 .build());
		builder.setState(AustralianState.NewSouhtWales);
		builder.setPostcode(AustralianPostcode.builder().setCode("2000")
														.build());
		builder.setPhone(AusPostShipmentPhoneNumber.builder().setNumber("0412345678")
															 .build());
		builder.setEmail(AusPostShipmentEmail.builder().setAddress("jane.smith@smith.com")
													   .build());
		
		return builder.build();
	}
	
	@SuppressWarnings("unused")
	private ItemCollection getItems() {
		ItemCollection.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemCollection.builder();
		
		builder.addItem(this.getItem1());
		builder.addItem(this.getItem2());
		builder.addItem(this.getItem3());
		
		return builder.build();
	}
	
	private Item getItem1() {
		Item.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Item.builder();
		
		builder.setReference(ItemReference.builder().setValue("SKU-1")
													.build());
		builder.setProductId(AustraliaPostPostageProducts.eParcelStandard.getProductId());
		builder.setLength(ItemLength.builder().setLength(10)
											  .build());
		builder.setWidth(ItemWidth.builder().setWidth(10)
											  .build());
		builder.setHeight(ItemHeight.builder().setHeight(10)
											  .build());

		builder.setWeight(ItemWeight.builder().setWeight(1)
											  .build());
		builder.setAuthorityToLeave(false);
		builder.setAllowPartialDelivery(true);
		builder.addItemFeature(ItemFeatureTransitCover.builder().setTransitCoverAmount(500)
													 			.build());		
		return builder.build();
	}
	
	@SuppressWarnings("unused")
	private Item getItem1OverTransitCoverAmount() {
		Item.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Item.builder();
		
		builder.setReference(ItemReference.builder().setValue("SKU-1")
													.build());
		builder.setProductId(AustraliaPostPostageProducts.eParcelStandard.getProductId());
		builder.setLength(ItemLength.builder().setLength(10)
											  .build());
		builder.setWidth(ItemWidth.builder().setWidth(10)
											  .build());
		builder.setHeight(ItemHeight.builder().setHeight(10)
											  .build());

		builder.setWeight(ItemWeight.builder().setWeight(1)
											  .build());
		builder.setAuthorityToLeave(false);
		builder.setAllowPartialDelivery(true);
		builder.addItemFeature(ItemFeatureTransitCover.builder().setTransitCoverAmount(1000)
													 			.build());		
		return builder.build();
	}
	
	private Item getItem2() {
		Item.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Item.builder();
		
		builder.setReference(ItemReference.builder().setValue("SKU-2")
													.build());
		builder.setProductId(AustraliaPostPostageProducts.eParcelStandard.getProductId());
		builder.setLength(ItemLength.builder().setLength(10)
											  .build());
		builder.setWidth(ItemWidth.builder().setWidth(10)
											  .build());
		builder.setHeight(ItemHeight.builder().setHeight(10)
											  .build());

		builder.setWeight(ItemWeight.builder().setWeight(1)
											  .build());
		builder.setAuthorityToLeave(false);
		builder.setAllowPartialDelivery(true);
		
		return builder.build();
	}
	
	private Item getItem3() {
		Item.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Item.builder();
		
		builder.setReference(ItemReference.builder().setValue("SKU-3")
													.build());
		builder.setProductId(AustraliaPostPostageProducts.eParcelStandard.getProductId());
		builder.setLength(ItemLength.builder().setLength(10)
											  .build());
		builder.setWidth(ItemWidth.builder().setWidth(10)
											  .build());
		builder.setHeight(ItemHeight.builder().setHeight(10)
											  .build());

		builder.setWeight(ItemWeight.builder().setWeight(1)
											  .build());
		builder.setAuthorityToLeave(false);
		builder.setAllowPartialDelivery(true);		
		
		return builder.build();
	}
	
	private void printErrorMessage(AusPostErrorCollection errors, String exceptionMsg) {		
		if(null != errors && errors.validate()) {
			for (AusPostError error : errors.getErrors()) {
				String msg = null;
				if(null == error) {
					msg = String.format("No error returned with exception. Msg = %s", exceptionMsg);
				}else {
					msg = String.format("Code = %s, Name = %s, Msg = %s", error.getCode(), error.getName(), error.getMessage());
				}
				
				System.err.println(msg);
			}				
		}				
	}
	
	private Shipment createShipment(Shipment data) throws EndpointException {		
		ShipmentEndpoint endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  .enableSandboxEnvironment()
															  .build();
		
		try {
			Shipment result = endpoint.create(data);			
			return result;
		}catch(EndpointException e) {
			this.printErrorMessage(endpoint.getOperationErrors(), e.getMessage());
			throw e;
		}
	}
}
