/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.AustralianPostcode;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.AustralianState;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostConfig;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.ErrorCodes;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.environment.sandbox.tog.AusPostMerchant;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.environment.sandbox.tog.AustraliaPostPostageProducts;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.OrderShipmentItemStatuses;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureTransitCover;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemHeight;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemLength;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemWeight;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemWidth;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentAddressLines;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentBusinessName;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentEmail;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentPersonalName;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentPhoneNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentSuburb;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Date;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.SenderReference;

/**
 * To test {@link ShipmentEndpoint}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
class ShipmentEndpointTest {
	private static AustraliaPostConfig CONFIG = null;
	
		/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		CONFIG = AustraliaPostConfig.getInstance();		
		CONFIG.setMerchant(new AusPostMerchant());		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#enableSandboxEnvironment()}.
	 */
	@Test
	void testEnableSandboxEnvironment() {
		ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
								  .enableSandboxEnvironment()
								  .build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#enableProductionEnvironment()}.
	 */
	@Test
	void testEnableProductionEnvironment() {
		ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
								  .enableProductionEnvironment()
								  .build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#create(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment)}.
	 * @throws EndpointException 
	 */
	@Test
	void testCreate() throws EndpointException {
		Shipment.Builder shipmentBuilder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.builder();
		shipmentBuilder.setShipmentReference(this.getShipmentReference());
		shipmentBuilder.setSenderReferences(this.getSenderReferences());
		shipmentBuilder.setEmailTracking(this.isEmailTracking());
		shipmentBuilder.setSender(this.getSender());
		shipmentBuilder.setReceiver(this.getReceiver());
		shipmentBuilder.addItem(this.getItem1());
		shipmentBuilder.addItem(this.getItem2());
		shipmentBuilder.addItem(this.getItem3());
		
		Shipment result = this.createShipment(shipmentBuilder.build());
		
		assertNotNull(result);
		assertNotNull(result.getShipmentId());
		assertTrue(result.getShipmentId().validate());		
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#get(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment)}.
	 * @throws EndpointException 
	 */
	@Test
	void testGetShipment() throws EndpointException {
		Shipment.Builder shipmentBuilder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.builder();
		shipmentBuilder.setShipmentReference(this.getShipmentReference());
		shipmentBuilder.setSenderReferences(this.getSenderReferences());
		shipmentBuilder.setEmailTracking(this.isEmailTracking());
		shipmentBuilder.setSender(this.getSender());
		shipmentBuilder.setReceiver(this.getReceiver());
		shipmentBuilder.addItem(this.getItem1());
		shipmentBuilder.addItem(this.getItem2());
		shipmentBuilder.addItem(this.getItem3());
		
		Shipment result = this.createShipment(shipmentBuilder.build());
		
		assertNotNull(result);
		assertNotNull(result.getShipmentId());
		assertTrue(result.getShipmentId().validate());		
		
		ShipmentEndpoint endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  .enableSandboxEnvironment()
															  .build();
		
		Shipment fetchedShipment = endpoint.get(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean
												.Shipment.builder().setShipmentId(result.getShipmentId())
																   .build());
		
		assertNotNull(fetchedShipment);
		assertNotNull(fetchedShipment.getShipmentId());
		assertTrue(fetchedShipment.getShipmentId().validate());
		assertEquals(fetchedShipment.getShipmentId(), result.getShipmentId());
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#get(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment)}.
	 * @throws EndpointException 
	 */
	@Test
	void testGetShipmentWithOverReqeustedTransitCover(){
		Shipment.Builder shipmentBuilder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.builder();
		shipmentBuilder.setSenderReferences(this.getSenderReferences());
		shipmentBuilder.setEmailTracking(this.isEmailTracking());
		shipmentBuilder.setSender(this.getSender());
		shipmentBuilder.setReceiver(this.getReceiver());
		shipmentBuilder.addItem(this.getItem1OverTransitCoverAmount());
		shipmentBuilder.addItem(this.getItem2());
		shipmentBuilder.addItem(this.getItem3());
		
		ShipmentEndpoint endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  .enableSandboxEnvironment()
															  .build();
		
		try {
			@SuppressWarnings("unused")
			Shipment result = endpoint.create(shipmentBuilder.build());				
		}catch(EndpointException e) {
			AusPostErrorCollection errors = endpoint.getOperationErrors();
			if(null != errors && errors.validate()) {
				for (AusPostError error : errors.getErrors()) {
					if(null == error) {
						String msg = "No error. Msg = " + e.getMessage();
						System.err.println(msg);
						fail(msg);
					}else {
						System.err.println(error.getName() + "[" + error.getCode() + "]: " + error.getMessage());
						assertEquals(ErrorCodes.TransitCoverAmountLagrgerThanAVT.getCode().toString(), error.getCode());					
					}	
				}				
			}
		}
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#list(java.lang.Integer, java.lang.Integer)}.
	 * @throws EndpointException 
	 */
	@Test
	void testListByOffsetAndNumberPerPage() throws EndpointException {
		ShipmentEndpoint endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  .enableSandboxEnvironment()
															  .build();
		
		ShipmentCollection fetchedShipments = endpoint.list(0, 10);
		
		assertNotNull(fetchedShipments);
		assertTrue(fetchedShipments.validate());
		assertNotNull(fetchedShipments.getShipments());		
		assertTrue((0 <= fetchedShipments.getShipments().size()));
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#list(java.lang.Integer, java.lang.Integer, com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.OrderShipmentItemStatuses)}.
	 * @throws EndpointException 
	 */
	@Test
	void testListByOffsetAndNumberPerPageForCreatedShipment() throws EndpointException {

		ShipmentEndpoint endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  .enableSandboxEnvironment()
															  .build();
		
		ShipmentCollection fetchedShipments = endpoint.list(0, 10, OrderShipmentItemStatuses.Created);
		
		assertNotNull(fetchedShipments);
		assertTrue(fetchedShipments.validate());
		assertNotNull(fetchedShipments.getShipments());		
		assertTrue((0 <= fetchedShipments.getShipments().size()));
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#list(java.lang.Integer, java.lang.Integer, com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime)}.
	 * @throws EndpointException 
	 */
	@Test
	void testListByOffsetAndNumberPerPageFor03062018() throws EndpointException {
		ShipmentEndpoint endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  .enableSandboxEnvironment()
															  .build();
		Date date = Date.builder().setYear("2018")
								 .setMonth("06")
								 .setDay("03")
								 .build();
		
		ShipmentCollection fetchedShipments = endpoint.list(0, 10, date);
		
		assertNotNull(fetchedShipments);
		assertFalse(fetchedShipments.validate());
		assertNotNull(fetchedShipments.getShipments());		
		assertTrue(fetchedShipments.getShipments().isEmpty());
		assertFalse( (0 < fetchedShipments.getShipments().size()) );
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#list(java.lang.Integer, java.lang.Integer, com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime)}.
	 * @throws EndpointException 
	 */
	@Test
	void testListByOffsetAndNumberPerPageFor03062017() throws EndpointException {
		ShipmentEndpoint endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  .enableSandboxEnvironment()
															  .build();
		Date date = Date.builder().setYear("2017")
								 .setMonth("06")
								 .setDay("03")
								 .build();
		
		ShipmentCollection fetchedShipments = endpoint.list(0, 10, date);
		
		assertNotNull(fetchedShipments);
		assertFalse(fetchedShipments.validate());
		assertNotNull(fetchedShipments.getShipments());		
		assertTrue(fetchedShipments.getShipments().isEmpty());
		assertFalse( (0 < fetchedShipments.getShipments().size()) );
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#list(java.lang.Integer, java.lang.Integer, com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection)}.
	 * @throws EndpointException 
	 */
	@Test
	void testListByOffsetAndNumberSenderReferenceCollection() throws EndpointException {

		ShipmentEndpoint endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  .enableSandboxEnvironment()
															  .build();
		
		com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference reference1 = SenderReference.builder().setReference("Order 001")
																																		   .build();
		com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference reference2 = SenderReference.builder().setReference("SKU-1, SKU-2, SKU-3")
				   																															.build();
		
		SenderReferenceCollection senderRefs = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.SenderReferenceCollection.builder().addSenderReference(reference1)
																																										.addSenderReference(reference2)
																																										.build();
		ShipmentCollection fetchedShipments = endpoint.list(0, 10, senderRefs);
		
		assertNotNull(fetchedShipments);
		assertTrue(fetchedShipments.validate());
		assertNotNull(fetchedShipments.getShipments());		
		assertFalse(fetchedShipments.getShipments().isEmpty());
		assertTrue( (0 < fetchedShipments.getShipments().size()) );
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#list(java.lang.Integer, java.lang.Integer, com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.OrderShipmentItemStatuses, com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection)}.
	 * @throws EndpointException 
	 */
	@Test
	void testListByOffsetAndNumberPerPageAndOrderShipmentItemStatusesAndSenderReferenceCollection() throws EndpointException {

		ShipmentEndpoint endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  .enableSandboxEnvironment()
															  .build();
		
		com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference reference1 = SenderReference.builder().setReference("Order 001")
																																		   .build();
		com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference reference2 = SenderReference.builder().setReference("SKU-1, SKU-2, SKU-3")
				   																															.build();
		
		SenderReferenceCollection senderRefs = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.SenderReferenceCollection.builder().addSenderReference(reference1)
																																										.addSenderReference(reference2)
																																										.build();
		ShipmentCollection fetchedShipments = endpoint.list(0, 10, OrderShipmentItemStatuses.Created, senderRefs);
		
		assertNotNull(fetchedShipments);
		assertTrue(fetchedShipments.validate());
		assertNotNull(fetchedShipments.getShipments());		
		assertFalse(fetchedShipments.getShipments().isEmpty());
		assertTrue( (0 < fetchedShipments.getShipments().size()) );
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#list(java.lang.Integer, java.lang.Integer, com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.OrderShipmentItemStatuses, com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime)}.
	 * @throws EndpointException 
	 */
	@Test
	void testListByOffsetAndNumberPerPageAndOrderShipmentItemStatusesWtihDespatchedDate() throws EndpointException {

		ShipmentEndpoint endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  .enableSandboxEnvironment()
															  .build();
		
		Date date = Date.builder().setYear("2018")
								 .setMonth("06")
								 .setDay("03")
								 .build();
		ShipmentCollection fetchedShipments = endpoint.list(0, 10, OrderShipmentItemStatuses.Created, date);
		
		assertNotNull(fetchedShipments);
		assertFalse(fetchedShipments.validate());
		assertNotNull(fetchedShipments.getShipments());		
		assertTrue(fetchedShipments.getShipments().isEmpty());
		assertFalse( (0 < fetchedShipments.getShipments().size()) );
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#list(java.lang.Integer, java.lang.Integer, com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.OrderShipmentItemStatuses, com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime, com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection)}.
	 * @throws EndpointException 
	 */
	@Test
	void testListByOffsetAndNumberPerPageAndOrderShipmentItemStatusesAndDateTimeSenderWithReferenceCollection() throws EndpointException {

		ShipmentEndpoint endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  .enableSandboxEnvironment()
															  .build();
		
		com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference reference1 = SenderReference.builder().setReference("Order 001")
																																		   .build();
		com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference reference2 = SenderReference.builder().setReference("SKU-1, SKU-2, SKU-3")
				   																															.build();
		
		SenderReferenceCollection senderRefs = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.SenderReferenceCollection.builder().addSenderReference(reference1)
																																										.addSenderReference(reference2)
																																										.build();
		Date date = Date.builder().setYear("2018")
								 .setMonth("06")
								 .setDay("03")
								 .build();
		ShipmentCollection fetchedShipments = endpoint.list(0, 10, OrderShipmentItemStatuses.Created, date, senderRefs);
		
		assertNotNull(fetchedShipments);
		assertFalse(fetchedShipments.validate());
		assertNotNull(fetchedShipments.getShipments());		
		assertTrue(fetchedShipments.getShipments().isEmpty());
		assertFalse( (0 < fetchedShipments.getShipments().size()) );
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#update(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment)}.
	 * @throws EndpointException 
	 */
	@Test
	void testUpdateShipment() throws EndpointException {
		ShipmentEndpoint endpoint = null;
		
		try {
			Shipment.Builder shipmentBuilder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.builder();
			shipmentBuilder.setShipmentReference(this.getShipmentReference());
			shipmentBuilder.setSenderReferences(this.getSenderReferences());
			shipmentBuilder.setEmailTracking(this.isEmailTracking());
			shipmentBuilder.setSender(this.getSender());
			shipmentBuilder.setReceiver(this.getReceiver());
			shipmentBuilder.addItem(this.getItem1());
			shipmentBuilder.addItem(this.getItem2());
			shipmentBuilder.addItem(this.getItem3());
			
			Shipment request = shipmentBuilder.build();
			
			Shipment result = this.createShipment(request);
			
			assertNotNull(result);
			assertNotNull(result.getShipmentId());
			assertTrue(result.getShipmentId().validate());		
			
			endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
												  .enableSandboxEnvironment()
												  .build();
			
			Shipment fetchedShipment = endpoint.get(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean
													.Shipment.builder().setShipmentId(result.getShipmentId())
																	   .build());
			
			assertNotNull(fetchedShipment);
			assertNotNull(fetchedShipment.getShipmentId());
			assertTrue(fetchedShipment.getShipmentId().validate());
			assertEquals(fetchedShipment.getShipmentId(), result.getShipmentId());
			
			fetchedShipment.getItems().getItem(0).getWeight().setWeight(10);
			//Find Item 1
			List<Item> items = fetchedShipment.getItems().getItems();
			Item item1 = this.getItem1();
			Item fetchedItem1 = null;
			for (Item item : items) {
				if( !item.getItemReference().equals(item1.getItemReference()) ) {
					continue;
				}
				
				fetchedItem1 = item;
				break;
			}
			
			fetchedItem1.getWeight().setWeight(10);
			
			endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
											    .enableSandboxEnvironment()
											    .build();
			try {
				endpoint.update(fetchedShipment);
			}catch(EndpointException e) {
				System.err.println(String.format("Update fail. Shipment Id = %s", fetchedShipment.getShipmentId().getValue()));
				AusPostErrorCollection errors = endpoint.getOperationErrors();
				if(null != errors && errors.validate()) {
					for (AusPostError error : errors.getErrors()) {
						System.err.println(String.format("%s[%s]: %s", error.getName(), error.getCode(), error.getMessage()));	
					}				
				}
				
				throw e;
			}
			
			endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
										  		.enableSandboxEnvironment()
										  		.build();
			
			Shipment updatedShipment = endpoint.get(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean
														.Shipment.builder().setShipmentId(fetchedShipment.getShipmentId())
																	   .build());
			
			assertNotNull(updatedShipment);
			assertNotNull(updatedShipment.getShipmentId());
			assertTrue(updatedShipment.getShipmentId().validate());
			assertEquals(updatedShipment.getShipmentId(), fetchedShipment.getShipmentId());
			
			Item	updatedItem	= null;
			List<Item> updatedItems = updatedShipment.getItems().getItems();
			for (Item item : updatedItems) {
				if( !item.getItemReference().equals(item1.getItemReference()) ) {
					continue;		
				}
				
				updatedItem = item;
				break;
			}
			
			if( null != updatedItem ) {
				assertEquals("10.000", updatedItem.getWeight().getDecimalWeight(), "Shipment Id: " + updatedShipment.getShipmentId().getValue());
			}else {
				fail("No matched updated item found");
			}
		}catch(EndpointException e) {
			AusPostErrorCollection errors = endpoint.getOperationErrors();
			if(null != errors && errors.validate()) {
				for (AusPostError error : errors.getErrors()) {
					System.err.println(String.format("%s[%s]: %s", error.getName(), error.getCode(), error.getMessage()));	
				}				
			}
			throw e;
		}
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#delete(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment)}.
	 * @throws EndpointException 
	 */
	@Test
	void testDeleteShipment() throws EndpointException {
		Shipment.Builder shipmentBuilder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.builder();
		shipmentBuilder.setShipmentReference(this.getShipmentReference());
		shipmentBuilder.setSenderReferences(this.getSenderReferences());
		shipmentBuilder.setEmailTracking(this.isEmailTracking());
		shipmentBuilder.setSender(this.getSender());
		shipmentBuilder.setReceiver(this.getReceiver());
		shipmentBuilder.addItem(this.getItem1());
		shipmentBuilder.addItem(this.getItem2());
		shipmentBuilder.addItem(this.getItem3());
		
		Shipment result = this.createShipment(shipmentBuilder.build());
		
		assertNotNull(result);
		assertNotNull(result.getShipmentId());
		assertTrue(result.getShipmentId().validate());		
		
		ShipmentEndpoint endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  .enableSandboxEnvironment()
															  .build();
		
		Shipment fetchedShipment = endpoint.get(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean
												.Shipment.builder().setShipmentId(result.getShipmentId())
																   .build());
		
		assertNotNull(fetchedShipment);
		assertNotNull(fetchedShipment.getShipmentId());
		assertTrue(fetchedShipment.getShipmentId().validate());
		assertEquals(fetchedShipment.getShipmentId(), result.getShipmentId());			
		
		ShipmentEndpoint deleteEndpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  		.enableSandboxEnvironment()
															  		.build();
		
		deleteEndpoint.delete(fetchedShipment);
				
		ShipmentEndpoint deleteCheckEndpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
																  		.enableSandboxEnvironment()
																  		.build();		
		try {
			Shipment fetchedDeletedShipment = deleteCheckEndpoint.get(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean
																	.Shipment.builder().setShipmentId(fetchedShipment.getShipmentId())
																					   .build());
			assertNull(fetchedDeletedShipment);
			fail("The target shipment[" + fetchedShipment.getShipmentId().getValue() + "] should not exist. Please check the ShipmentEndpoint#delete().");
		}catch(EndpointException e) {
			AusPostErrorCollection errors = deleteCheckEndpoint.getOperationErrors();
			assertNotNull(errors);
			assertNotNull(errors.getError(ErrorCodes.ShipmentNotFoundError.getCode()));
			
			throw e;
		}
		
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#delete(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentId)}.
	 * @throws EndpointException 
	 */
	@Test
	void testDeleteShipmentId() throws EndpointException {
		Shipment.Builder shipmentBuilder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.builder();
		shipmentBuilder.setShipmentReference(this.getShipmentReference());
		shipmentBuilder.setSenderReferences(this.getSenderReferences());
		shipmentBuilder.setEmailTracking(this.isEmailTracking());
		shipmentBuilder.setSender(this.getSender());
		shipmentBuilder.setReceiver(this.getReceiver());
		shipmentBuilder.addItem(this.getItem1());
		shipmentBuilder.addItem(this.getItem2());
		shipmentBuilder.addItem(this.getItem3());
		
		Shipment result = this.createShipment(shipmentBuilder.build());
		
		assertNotNull(result);
		assertNotNull(result.getShipmentId());
		assertTrue(result.getShipmentId().validate());		
		
		ShipmentEndpoint endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  .enableSandboxEnvironment()
															  .build();
		
		Shipment fetchedShipment = endpoint.get(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean
												.Shipment.builder().setShipmentId(result.getShipmentId())
																   .build());
		
		assertNotNull(fetchedShipment);
		assertNotNull(fetchedShipment.getShipmentId());
		assertTrue(fetchedShipment.getShipmentId().validate());
		assertEquals(fetchedShipment.getShipmentId(), result.getShipmentId());			
		
		ShipmentEndpoint deleteEndpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  		.enableSandboxEnvironment()
															  		.build();
		
		deleteEndpoint.delete(fetchedShipment.getShipmentId());

		
		ShipmentEndpoint deleteCheckEndpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
																  		.enableSandboxEnvironment()
																  		.build();		
		try {
			Shipment fetchedDeletedShipment = deleteCheckEndpoint.get(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean
																	.Shipment.builder().setShipmentId(fetchedShipment.getShipmentId())
																					   .build());
			assertNull(fetchedDeletedShipment);
			fail("The target shipment[" + fetchedShipment.getShipmentId().getValue() + "] should not exist. Please check the ShipmentEndpoint#delete().");
		}catch(EndpointException e) {
			AusPostErrorCollection errors = deleteCheckEndpoint.getOperationErrors();
			assertNotNull(errors);
			assertNotNull(errors.getError(ErrorCodes.ShipmentNotFoundError.getCode()));
			
			throw e;
		}
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#delete(java.lang.String)}.
	 * @throws EndpointException 
	 */
	@Test
	void testDeleteString() throws EndpointException {
		Shipment.Builder shipmentBuilder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.builder();
		shipmentBuilder.setShipmentReference(this.getShipmentReference());
		shipmentBuilder.setSenderReferences(this.getSenderReferences());
		shipmentBuilder.setEmailTracking(this.isEmailTracking());
		shipmentBuilder.setSender(this.getSender());
		shipmentBuilder.setReceiver(this.getReceiver());
		shipmentBuilder.addItem(this.getItem1());
		shipmentBuilder.addItem(this.getItem2());
		shipmentBuilder.addItem(this.getItem3());
		
		Shipment result = this.createShipment(shipmentBuilder.build());
		
		assertNotNull(result);
		assertNotNull(result.getShipmentId());
		assertTrue(result.getShipmentId().validate());		
		
		ShipmentEndpoint endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  .enableSandboxEnvironment()
															  .build();
		
		Shipment fetchedShipment = endpoint.get(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean
												.Shipment.builder().setShipmentId(result.getShipmentId())
																   .build());
		
		assertNotNull(fetchedShipment);
		assertNotNull(fetchedShipment.getShipmentId());
		assertTrue(fetchedShipment.getShipmentId().validate());
		assertEquals(fetchedShipment.getShipmentId(), result.getShipmentId());			
		
		ShipmentEndpoint deleteEndpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  		.enableSandboxEnvironment()
															  		.build();
		
		deleteEndpoint.delete(fetchedShipment.getShipmentId().getValue());
		
		ShipmentEndpoint deleteCheckEndpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
																  		.enableSandboxEnvironment()
																  		.build();		
		try {
			Shipment fetchedDeletedShipment = deleteCheckEndpoint.get(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean
																	.Shipment.builder().setShipmentId(fetchedShipment.getShipmentId())
																					   .build());
			assertNull(fetchedDeletedShipment);
			fail("The target shipment[" + fetchedShipment.getShipmentId().getValue() + "] should not exist. Please check the ShipmentEndpoint#delete().");
		}catch(EndpointException e) {
			AusPostErrorCollection errors = deleteCheckEndpoint.getOperationErrors();
			assertNotNull(errors);
			assertNotNull(errors.getError(ErrorCodes.ShipmentNotFoundError.getCode()));
			
			throw e;
		}
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#insert(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Disabled
	@Test
	void testInsert() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#update(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Disabled
	@Test
	void testUpdateShipmentResponseCallbackOfShipment() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#update(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)}.
	 */
	@Disabled
	@Test
	void testUpdateShipmentResponseCallbackOfShipmentInteger() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#list(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Disabled
	@Test
	void testListShipmentResponseCallbackOfCollectionResponseOfShipment() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#list(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer, java.lang.Integer)}.
	 */
	@Disabled
	@Test
	void testListShipmentResponseCallbackOfCollectionResponseOfShipmentIntegerInteger() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#delete(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Disabled
	@Test
	void testDeleteShipmentResponseCallbackOfShipment() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#delete(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)}.
	 */
	@Disabled
	@Test
	void testDeleteShipmentResponseCallbackOfShipmentInteger() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#get(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Disabled
	@Test
	void testGetShipmentResponseCallbackOfShipment() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#builder()}.
	 */
	@Test
	void testBuilder() {
		ShipmentEndpoint.builder();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#setMerchant(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails)}.
	 */
	@Test
	void testSetMerchant() {
		ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
								  .enableSandboxEnvironment()
								  .build();
	}

	private ShipmentReference getShipmentReference() {
		ShipmentReference.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentReference.builder();
		
		builder.setValue("XYZ-001-01");
				
		return builder.build();
	}
	
	private SenderReferenceCollection getSenderReferences() {
		SenderReferenceCollection.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.SenderReferenceCollection.builder();
		
		builder.addSenderReference(SenderReference.builder().setReference("Order 001")
															.build());

		builder.addSenderReference(SenderReference.builder().setReference("SKU-1, SKU-2, SKU-3")
															.build());
		return builder.build();
	}
	
	private Boolean isEmailTracking() {
		return true;
	}
	
	private Sender getSender() {
		Sender.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Sender.builder();
		
		builder.setName(AusPostShipmentPersonalName.builder().setFullName("Otto Hung")
															 .build());
		
		builder.setAddressLines(AusPostShipmentAddressLines.builder().addAddress("2, 74 perrin drive")
																	 .build());
		builder.setSuburb(AusPostShipmentSuburb.builder().setName("MELBOURNE")
														 .build());
		builder.setState(AustralianState.Victoria);
		builder.setPostcode(AustralianPostcode.builder().setCode("3000")
														.build());
		builder.setPhone(AusPostShipmentPhoneNumber.builder().setNumber("0732723318")
															 .build());
		builder.setEmail(AusPostShipmentEmail.builder().setAddress("it@togauto.com.au")
													   .build());
		
		return builder.build();
	}
	
	private Receiver getReceiver() {
		Receiver.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Receiver.builder();
		
		builder.setName(AusPostShipmentPersonalName.builder().setFullName("Jane Smith")
															 .build());
		builder.setBusinessName(AusPostShipmentBusinessName.builder().setName("Smith Pty Ltd")
																	 .build());
		builder.setAddressLines(AusPostShipmentAddressLines.builder().addAddress("123 Centre Road")
																	 .build());
		builder.setSuburb(AusPostShipmentSuburb.builder().setName("Sydney")
														 .build());
		builder.setState(AustralianState.NewSouhtWales);
		builder.setPostcode(AustralianPostcode.builder().setCode("2000")
														.build());
		builder.setPhone(AusPostShipmentPhoneNumber.builder().setNumber("0412345678")
															 .build());
		builder.setEmail(AusPostShipmentEmail.builder().setAddress("jane.smith@smith.com")
													   .build());
		
		return builder.build();
	}
	
	@SuppressWarnings("unused")
	private ItemCollection getItems() {
		ItemCollection.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemCollection.builder();
		
		builder.addItem(this.getItem1());
		builder.addItem(this.getItem2());
		builder.addItem(this.getItem3());
		
		return builder.build();
	}
	
	private Item getItem1() {
		Item.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Item.builder();
		
		builder.setReference(ItemReference.builder().setValue("SKU-1")
													.build());
		builder.setProductId(AustraliaPostPostageProducts.eParcelStandard.getProductId());
		builder.setLength(ItemLength.builder().setLength(10)
											  .build());
		builder.setWidth(ItemWidth.builder().setWidth(10)
											  .build());
		builder.setHeight(ItemHeight.builder().setHeight(10)
											  .build());

		builder.setWeight(ItemWeight.builder().setWeight(1)
											  .build());
		builder.setAuthorityToLeave(false);
		builder.setAllowPartialDelivery(true);
		builder.addItemFeature(ItemFeatureTransitCover.builder().setTransitCoverAmount(500)
													 			.build());		
		return builder.build();
	}
	
	private Item getItem1OverTransitCoverAmount() {
		Item.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Item.builder();
		
		builder.setReference(ItemReference.builder().setValue("SKU-1")
													.build());
		builder.setProductId(AustraliaPostPostageProducts.eParcelStandard.getProductId());
		builder.setLength(ItemLength.builder().setLength(10)
											  .build());
		builder.setWidth(ItemWidth.builder().setWidth(10)
											  .build());
		builder.setHeight(ItemHeight.builder().setHeight(10)
											  .build());

		builder.setWeight(ItemWeight.builder().setWeight(1)
											  .build());
		builder.setAuthorityToLeave(false);
		builder.setAllowPartialDelivery(true);
		builder.addItemFeature(ItemFeatureTransitCover.builder().setTransitCoverAmount(1000)
													 			.build());		
		return builder.build();
	}
	
	private Item getItem2() {
		Item.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Item.builder();
		
		builder.setReference(ItemReference.builder().setValue("SKU-2")
													.build());
		builder.setProductId(AustraliaPostPostageProducts.eParcelStandard.getProductId());
		builder.setLength(ItemLength.builder().setLength(10)
											  .build());
		builder.setWidth(ItemWidth.builder().setWidth(10)
											  .build());
		builder.setHeight(ItemHeight.builder().setHeight(10)
											  .build());

		builder.setWeight(ItemWeight.builder().setWeight(1)
											  .build());
		builder.setAuthorityToLeave(false);
		builder.setAllowPartialDelivery(true);
		
		return builder.build();
	}
	
	private Item getItem3() {
		Item.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Item.builder();
		
		builder.setReference(ItemReference.builder().setValue("SKU-3")
													.build());
		builder.setProductId(AustraliaPostPostageProducts.eParcelStandard.getProductId());
		builder.setLength(ItemLength.builder().setLength(10)
											  .build());
		builder.setWidth(ItemWidth.builder().setWidth(10)
											  .build());
		builder.setHeight(ItemHeight.builder().setHeight(10)
											  .build());

		builder.setWeight(ItemWeight.builder().setWeight(1)
											  .build());
		builder.setAuthorityToLeave(false);
		builder.setAllowPartialDelivery(true);		
		
		return builder.build();
	}
	
	private void printErrorMessage(AusPostErrorCollection errors, String exceptionMsg) {		
		if(null != errors && errors.validate()) {
			for (AusPostError error : errors.getErrors()) {
				String msg = null;
				if(null == error) {
					msg = String.format("No error returned with exception. Msg = %s", exceptionMsg);
				}else {
					msg = String.format("Code = %s, Name = %s, Msg = %s", error.getCode(), error.getName(), error.getMessage());
				}
				
				System.err.println(msg);
			}				
		}				
	}
	
	private Shipment createShipment(Shipment data) throws EndpointException {		
		ShipmentEndpoint endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  .enableSandboxEnvironment()
															  .build();
		
		try {
			Shipment result = endpoint.create(data);			
			return result;
		}catch(EndpointException e) {
			this.printErrorMessage(endpoint.getOperationErrors(), e.getMessage());
			throw e;
		}
	}
}
