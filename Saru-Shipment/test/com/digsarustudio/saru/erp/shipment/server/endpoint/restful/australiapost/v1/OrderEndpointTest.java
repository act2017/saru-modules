/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.AustralianPostcode;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.AustralianState;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostConfig;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.environment.sandbox.tog.AusPostMerchant;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.environment.sandbox.tog.AustraliaPostPostageProducts;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureTransitCover;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemHeight;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemLength;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemWeight;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemWidth;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentAddressLines;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentBusinessName;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentEmail;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentPersonalName;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentPhoneNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentSuburb;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.SenderReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelBranded;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormat;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatGroup;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatLeftOffset;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatTopOffset;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.javabean.LabelPreference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.javabean.LabelPreferenceType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Consignor;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderReference;

/**
 * To test {@link OrderEndpoint}.<br>
 * <br>
 * The actuall data returned is as the follows:<br>
 * {
  "order": {
    "order_id": "TB00029763",
    "order_reference": "2018-06-06T13:14:28.731",
    "order_creation_date": "2018-06-06T13:14:50+10:00",
    "order_summary": {
      "total_cost": 100.16,
      "total_gst": 0,
      "status": "Initiated",
      "tracking_summary": {
        "Sealed": 6
      },
      "number_of_shipments": 2,
      "number_of_pickups": 0,
      "number_of_items": 6,
      "dangerous_goods_included": false,
      "total_weight": 6,
      "shipping_methods": {
        "7E55": 6
      }
    },
    "shipments": [
      {
        "shipment_id": "WoAK0EKz_WYAAAFjWIsbHdMW",
        "shipment_reference": "XYZ-001-01",
        "shipment_creation_date": "2018-06-06T13:14:20+10:00",
        "email_tracking_enabled": true,
        "items": [
          {
            "weight": 1,
            "authority_to_leave": false,
            "safe_drop_enabled": true,
            "allow_partial_delivery": true,
            "item_id": "tNUK0EKz5cQAAAFjWYsbHdMW",
            "item_reference": "SKU-1",
            "tracking_details": {
              "article_id": "111JD500728301000935101",
              "consignment_id": "111JD5007283",
              "barcode_id": "019931265099999891111JD500728301000935101|4202000|8008180606131422"
            },
            "product_id": "7E55",
            "item_summary": {
              "total_cost": 27.12,
              "total_gst": 0,
              "status": "Sealed"
            },
            "item_contents": []
          },
          {
            "weight": 1,
            "authority_to_leave": false,
            "safe_drop_enabled": true,
            "allow_partial_delivery": true,
            "item_id": "E4IK0EKzCXUAAAFjWosbHdMW",
            "item_reference": "SKU-2",
            "tracking_details": {
              "article_id": "111JD500728303000935105",
              "consignment_id": "111JD5007283",
              "barcode_id": "019931265099999891111JD500728303000935105|4202000|8008180606131422"
            },
            "product_id": "7E55",
            "item_summary": {
              "total_cost": 11.48,
              "total_gst": 0,
              "status": "Sealed"
            },
            "item_contents": []
          },
          {
            "weight": 1,
            "authority_to_leave": false,
            "safe_drop_enabled": true,
            "allow_partial_delivery": true,
            "item_id": "N2YK0EKze3wAAAFjW4sbHdMW",
            "item_reference": "SKU-3",
            "tracking_details": {
              "article_id": "111JD500728302000935108",
              "consignment_id": "111JD5007283",
              "barcode_id": "019931265099999891111JD500728302000935108|4202000|8008180606131422"
            },
            "product_id": "7E55",
            "item_summary": {
              "total_cost": 11.48,
              "total_gst": 0,
              "status": "Sealed"
            },
            "item_contents": []
          }
        ],
        "options": {},
        "shipment_summary": {
          "total_cost": 50.08,
          "total_gst": 0,
          "status": "Sealed",
          "tracking_summary": {
            "Sealed": 3
          },
          "number_of_items": 3
        },
        "movement_type": "DESPATCH"
      },
      {
        "shipment_id": "TGYK0E4nRTsAAAFjh5wbHNMW",
        "shipment_reference": "XYZ-001-01",
        "shipment_creation_date": "2018-06-06T13:14:24+10:00",
        "email_tracking_enabled": true,
        "items": [
          {
            "weight": 1,
            "authority_to_leave": false,
            "safe_drop_enabled": true,
            "allow_partial_delivery": true,
            "item_id": "ixEK0E4nUVcAAAFjiJwbHNMW",
            "item_reference": "SKU-1",
            "tracking_details": {
              "article_id": "111JD500728403000935102",
              "consignment_id": "111JD5007284",
              "barcode_id": "019931265099999891111JD500728403000935102|4202601|8008180606131425"
            },
            "product_id": "7E55",
            "item_summary": {
              "total_cost": 27.12,
              "total_gst": 0,
              "status": "Sealed"
            },
            "item_contents": []
          },
          {
            "weight": 1,
            "authority_to_leave": false,
            "safe_drop_enabled": true,
            "allow_partial_delivery": true,
            "item_id": "8t8K0E4nvC4AAAFjlJwbHNMW",
            "item_reference": "SKU-3",
            "tracking_details": {
              "article_id": "111JD500728402000935105",
              "consignment_id": "111JD5007284",
              "barcode_id": "019931265099999891111JD500728402000935105|4202601|8008180606131425"
            },
            "product_id": "7E55",
            "item_summary": {
              "total_cost": 11.48,
              "total_gst": 0,
              "status": "Sealed"
            },
            "item_contents": []
          },
          {
            "weight": 1,
            "authority_to_leave": false,
            "safe_drop_enabled": true,
            "allow_partial_delivery": true,
            "item_id": "IWYK0E4nOLAAAAFjk5wbHNMW",
            "item_reference": "SKU-2",
            "tracking_details": {
              "article_id": "111JD500728401000935108",
              "consignment_id": "111JD5007284",
              "barcode_id": "019931265099999891111JD500728401000935108|4202601|8008180606131425"
            },
            "product_id": "7E55",
            "item_summary": {
              "total_cost": 11.48,
              "total_gst": 0,
              "status": "Sealed"
            },
            "item_contents": []
          }
        ],
        "options": {},
        "shipment_summary": {
          "total_cost": 50.08,
          "total_gst": 0,
          "status": "Sealed",
          "tracking_summary": {
            "Sealed": 3
          },
          "number_of_items": 3
        },
        "movement_type": "DESPATCH"
      }
    ],
    "payment_method": "CHARGE_TO_ACCOUNT",
    "consignor": "QB-BOX"
  }
}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
class OrderEndpointTest {
	private static AustraliaPostConfig CONFIG = null;
	private static com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference PREFERENCE	= null;
	
	private String 					labelGenerationMsg 	= null;
	private String 					labelGenerationCode	= null;
	private AusPostErrorCollection	errors				= null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		CONFIG = AustraliaPostConfig.getInstance();		
		CONFIG.setMerchant(new AusPostMerchant());
		

		
		@SuppressWarnings("static-access")
		LabelFormat format = LabelFormat.builder().setGroup(LabelFormatGroup.ParcelPost)
												  .setLayout(LabelFormatGroup.ParcelPost.ThermalLableA6_1pp)
												  .setLabelBranded(LabelBranded.builder().setBranded()
														  								 .build())
												  
												  .setLeftOffset(LabelFormatLeftOffset.builder().setValue(0)
														  										.build())
												  .setTopOffset(LabelFormatTopOffset.builder().setValue(0)
														  									  .build())
												  .build();
		
		
		
		PREFERENCE = LabelPreference.builder().setType(LabelPreferenceType.Print)
											  .addLabelFormat(format)
											  .build();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.OrderEndpoint#enableSandboxEnvironment()}.
	 */
	@Test
	void testEnableSandboxEnvironment() {

		ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
								  .enableSandboxEnvironment()
								  .build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.OrderEndpoint#enableProductionEnvironment()}.
	 */
	@Test
	void testEnableProductionEnvironment() {

		ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
								  .enableProductionEnvironment()
								  .build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.OrderEndpoint#create(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderReference, com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection)}.
	 * @throws EndpointException 
	 * @throws InterruptedException 
	 */
	@Test
	void testCreate() throws EndpointException, InterruptedException {				
		try {
			List<Shipment> shipmets = this.generateShipments();
			ShipmentCollection.Builder createdShipmentsBuilder	= com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentCollection.builder();
			
			//Create Shipments and labels
			for (Shipment shipment : shipmets) {
				if(null == shipment || !shipment.validate()) {
					continue;
				}
				
				//Create Shipment
				Shipment result = this.createShipment(shipment);
				createdShipmentsBuilder.addShipment(result);
				
				assertNotNull(result);
				assertNotNull(result.getShipmentId());
				assertTrue(result.getShipmentId().validate());
				
				//Create Label
				List<Label> createdLabels = this.generateLabels(result);
				assertNotNull(createdLabels);
				assertFalse(createdLabels.isEmpty());
				
				for (Label label : createdLabels) {
					assertNotNull(label.getRequestId(), String.format("Msg: %s, Code: %s", this.labelGenerationMsg, this.labelGenerationCode));
					
					System.out.println("testCreate::Request Id: " + label.getRequestId().getValue());	
				}
				
				//Get label
				Thread.sleep(3000);				
				LabelEndpoint labelRetrieveEndpoint = LabelEndpoint.builder().setMerchant(CONFIG.getMerchant())
																			.enableSandboxEnvironment()
																			.build();
				LabelCollection labelsWithUrl = labelRetrieveEndpoint.get(createdLabels.get(0));
				for (Label label : labelsWithUrl.getLabels()) {
					assertNotNull(label.getRequestId(), String.format("Msg: %s, Code: %s", labelRetrieveEndpoint.getGenerationProgressMessage(), labelRetrieveEndpoint.getGenerationProgressCode()));
					
					System.out.println("Request Id: " + label.getRequestId().getValue());
					
					assertNotNull(labelsWithUrl);
					assertNotNull(labelsWithUrl.getLabels());				
					
					for (Label labelWithUrl : labelsWithUrl.getLabels()) {
						assertNotNull(labelWithUrl, "Reqeust Id = " + labelWithUrl.getRequestId().getValue());
						assertNotNull(labelWithUrl.getURL(), "Reqeust Id = " + labelWithUrl.getRequestId().getValue() + ", Status = " + labelWithUrl.getStatus().getValue());
						assertNotNull(labelWithUrl.getURL().getValue(), "Reqeust Id = " + labelWithUrl.getRequestId().getValue() + ", Status = " + labelWithUrl.getStatus().getValue());
						assertFalse(labelWithUrl.getURL().getValue().isEmpty(), "Reqeust Id = " + labelWithUrl.getRequestId().getValue() + ", Status = " + labelWithUrl.getStatus().getValue());
						
						System.out.println(String.format("testGet::Reqeust Id: %s, Url: %s", labelWithUrl.getRequestId().getValue(), labelWithUrl.getURL().getValue()));
					}
				}	
			}				
						
			//Create Order
			OrderEndpoint	orderEndpoint	= OrderEndpoint.builder().setMerchant(CONFIG.getMerchant())
																	.enableSandboxEnvironment()
																	.build();
			
			Order order = orderEndpoint.create(this.generateOrderReference(), this.getConsignor(), createdShipmentsBuilder.build());
			
			assertNotNull(order, orderEndpoint.getJsonResponse());
			assertNotNull(order.getOrderId(), orderEndpoint.getJsonResponse());
			assertTrue(order.getOrderId().validate(), orderEndpoint.getJsonResponse());				
			System.out.println(String.format("Order %s is created.", order.getOrderId().getValue()));
			
			assertTrue(order.getOrderSummary().validate(), orderEndpoint.getJsonResponse());
			assertTrue(order.getShipments().validate(), orderEndpoint.getJsonResponse());			
		}catch(EndpointException e) {
			this.printErrorMessage(this.errors, e.getMessage());
			
			
			throw e;
		}
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.OrderEndpoint#get(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order)}.
	 * @throws EndpointException 
	 * @throws InterruptedException 
	 */
	@Test
	void testGetOrder() throws EndpointException, InterruptedException {		
		try {
			List<Shipment> shipmets = this.generateShipments();
			ShipmentCollection.Builder createdShipmentsBuilder	= com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentCollection.builder();
			
			//Create Shipments and labels
			for (Shipment shipment : shipmets) {
				if(null == shipment || !shipment.validate()) {
					continue;
				}
				
				//Create Shipment
				Shipment result = this.createShipment(shipment);
				createdShipmentsBuilder.addShipment(result);
				
				assertNotNull(result);
				assertNotNull(result.getShipmentId());
				assertTrue(result.getShipmentId().validate());
				
				//Create Label
				List<Label> createdLabels = this.generateLabels(result);
				assertNotNull(createdLabels);
				assertFalse(createdLabels.isEmpty());
				
				for (Label label : createdLabels) {
					assertNotNull(label.getRequestId(), String.format("Msg: %s, Code: %s", this.labelGenerationMsg, this.labelGenerationCode));
					
					System.out.println("testCreate::Request Id: " + label.getRequestId().getValue());	
				}
				
				//Get label
				Thread.sleep(3000);				
				LabelEndpoint labelRetrieveEndpoint = LabelEndpoint.builder().setMerchant(CONFIG.getMerchant())
																			.enableSandboxEnvironment()
																			.build();
				LabelCollection labelsWithUrl = labelRetrieveEndpoint.get(createdLabels.get(0));
				for (Label label : labelsWithUrl.getLabels()) {
					assertNotNull(label.getRequestId(), String.format("Msg: %s, Code: %s", labelRetrieveEndpoint.getGenerationProgressMessage(), labelRetrieveEndpoint.getGenerationProgressCode()));
					
					System.out.println("Request Id: " + label.getRequestId().getValue());
					
					assertNotNull(labelsWithUrl);
					assertNotNull(labelsWithUrl.getLabels());				
					
					for (Label labelWithUrl : labelsWithUrl.getLabels()) {
						assertNotNull(labelWithUrl, "Reqeust Id = " + labelWithUrl.getRequestId().getValue());
						assertNotNull(labelWithUrl.getURL(), "Reqeust Id = " + labelWithUrl.getRequestId().getValue() + ", Status = " + labelWithUrl.getStatus().getValue());
						assertNotNull(labelWithUrl.getURL().getValue(), "Reqeust Id = " + labelWithUrl.getRequestId().getValue() + ", Status = " + labelWithUrl.getStatus().getValue());
						assertFalse(labelWithUrl.getURL().getValue().isEmpty(), "Reqeust Id = " + labelWithUrl.getRequestId().getValue() + ", Status = " + labelWithUrl.getStatus().getValue());
						
						System.out.println(String.format("testGet::Reqeust Id: %s, Url: %s", labelWithUrl.getRequestId().getValue(), labelWithUrl.getURL().getValue()));
					}
				}	
			}				
						
			//Create Order
			OrderEndpoint	orderEndpoint	= OrderEndpoint.builder().setMerchant(CONFIG.getMerchant())
																	.enableSandboxEnvironment()
																	.build();
			
			Order order = orderEndpoint.create(this.generateOrderReference(), this.getConsignor(), createdShipmentsBuilder.build());
			
			assertNotNull(order, orderEndpoint.getJsonResponse());
			assertNotNull(order.getOrderId(), orderEndpoint.getJsonResponse());
			assertTrue(order.getOrderId().validate(), orderEndpoint.getJsonResponse());				
			System.out.println(String.format("Order %s is created.", order.getOrderId().getValue()));
			
			assertTrue(order.getOrderSummary().validate(), orderEndpoint.getJsonResponse());
			assertTrue(order.getShipments().validate(), orderEndpoint.getJsonResponse());	
			
			//Get Order
			OrderEndpoint	getOrderEndpoint	= OrderEndpoint.builder().setMerchant(CONFIG.getMerchant())
																		 .enableSandboxEnvironment()
																		 .build();
			
			Order retrievedOrder = getOrderEndpoint.get(order);
			
			assertNotNull(retrievedOrder, getOrderEndpoint.getJsonResponse());
			assertNotNull(retrievedOrder.getOrderId(), getOrderEndpoint.getJsonResponse());
			assertTrue(retrievedOrder.getOrderId().validate(), getOrderEndpoint.getJsonResponse());				
			System.out.println(String.format("Order %s is available to print now.", retrievedOrder.getOrderId().getValue()));
			
			assertTrue(retrievedOrder.getOrderSummary().validate(), getOrderEndpoint.getJsonResponse());
			assertTrue(retrievedOrder.getShipments().validate(), getOrderEndpoint.getJsonResponse());	
		}catch(EndpointException e) {
			this.printErrorMessage(this.errors, e.getMessage());
			
			
			throw e;
		}
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.OrderEndpoint#get(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderId)}.
	 * @throws EndpointException 
	 * @throws InterruptedException 
	 */
	@Test
	void testGetOrderId() throws EndpointException, InterruptedException {
		try {
			List<Shipment> shipmets = this.generateShipments();
			ShipmentCollection.Builder createdShipmentsBuilder	= com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentCollection.builder();
			
			//Create Shipments and labels
			for (Shipment shipment : shipmets) {
				if(null == shipment || !shipment.validate()) {
					continue;
				}
				
				//Create Shipment
				Shipment result = this.createShipment(shipment);
				createdShipmentsBuilder.addShipment(result);
				
				assertNotNull(result);
				assertNotNull(result.getShipmentId());
				assertTrue(result.getShipmentId().validate());
				
				//Create Label
				List<Label> createdLabels = this.generateLabels(result);
				assertNotNull(createdLabels);
				assertFalse(createdLabels.isEmpty());
				
				for (Label label : createdLabels) {
					assertNotNull(label.getRequestId(), String.format("Msg: %s, Code: %s", this.labelGenerationMsg, this.labelGenerationCode));
					
					System.out.println("testCreate::Request Id: " + label.getRequestId().getValue());	
				}
				
				//Get label
				Thread.sleep(3000);				
				LabelEndpoint labelRetrieveEndpoint = LabelEndpoint.builder().setMerchant(CONFIG.getMerchant())
																			.enableSandboxEnvironment()
																			.build();
				LabelCollection labelsWithUrl = labelRetrieveEndpoint.get(createdLabels.get(0));
				for (Label label : labelsWithUrl.getLabels()) {
					assertNotNull(label.getRequestId(), String.format("Msg: %s, Code: %s", labelRetrieveEndpoint.getGenerationProgressMessage(), labelRetrieveEndpoint.getGenerationProgressCode()));
					
					System.out.println("Request Id: " + label.getRequestId().getValue());
					
					assertNotNull(labelsWithUrl);
					assertNotNull(labelsWithUrl.getLabels());				
					
					for (Label labelWithUrl : labelsWithUrl.getLabels()) {
						assertNotNull(labelWithUrl, "Reqeust Id = " + labelWithUrl.getRequestId().getValue());
						assertNotNull(labelWithUrl.getURL(), "Reqeust Id = " + labelWithUrl.getRequestId().getValue() + ", Status = " + labelWithUrl.getStatus().getValue());
						assertNotNull(labelWithUrl.getURL().getValue(), "Reqeust Id = " + labelWithUrl.getRequestId().getValue() + ", Status = " + labelWithUrl.getStatus().getValue());
						assertFalse(labelWithUrl.getURL().getValue().isEmpty(), "Reqeust Id = " + labelWithUrl.getRequestId().getValue() + ", Status = " + labelWithUrl.getStatus().getValue());
						
						System.out.println(String.format("testGet::Reqeust Id: %s, Url: %s", labelWithUrl.getRequestId().getValue(), labelWithUrl.getURL().getValue()));
					}
				}	
			}				
						
			//Create Order
			OrderEndpoint	orderEndpoint	= OrderEndpoint.builder().setMerchant(CONFIG.getMerchant())
																	.enableSandboxEnvironment()
																	.build();
			
			Order order = orderEndpoint.create(this.generateOrderReference(), this.getConsignor(), createdShipmentsBuilder.build());
			
			assertNotNull(order, orderEndpoint.getJsonResponse());
			assertNotNull(order.getOrderId(), orderEndpoint.getJsonResponse());
			assertTrue(order.getOrderId().validate(), orderEndpoint.getJsonResponse());				
			System.out.println(String.format("Order %s is created.", order.getOrderId().getValue()));
			
			assertTrue(order.getOrderSummary().validate(), orderEndpoint.getJsonResponse());
			assertTrue(order.getShipments().validate(), orderEndpoint.getJsonResponse());	
			
			//Get Order
			OrderEndpoint	getOrderEndpoint	= OrderEndpoint.builder().setMerchant(CONFIG.getMerchant())
																		 .enableSandboxEnvironment()
																		 .build();
			
			Order retrievedOrder = getOrderEndpoint.get(order.getOrderId());
			
			assertNotNull(retrievedOrder, getOrderEndpoint.getJsonResponse());
			assertNotNull(retrievedOrder.getOrderId(), getOrderEndpoint.getJsonResponse());
			assertTrue(retrievedOrder.getOrderId().validate(), getOrderEndpoint.getJsonResponse());				
			System.out.println(String.format("Order %s is available to print now.", retrievedOrder.getOrderId().getValue()));
			
			assertTrue(retrievedOrder.getOrderSummary().validate(), getOrderEndpoint.getJsonResponse());
			assertTrue(retrievedOrder.getShipments().validate(), getOrderEndpoint.getJsonResponse());	
		}catch(EndpointException e) {
			this.printErrorMessage(this.errors, e.getMessage());
			
			
			throw e;
		}
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.OrderEndpoint#insert(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Disabled
	@Test
	void testInsert() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.OrderEndpoint#update(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Disabled
	@Test
	void testUpdateOrderResponseCallbackOfOrder() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.OrderEndpoint#update(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)}.
	 */
	@Disabled
	@Test
	void testUpdateOrderResponseCallbackOfOrderInteger() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.OrderEndpoint#list(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Disabled
	@Test
	void testListOrderResponseCallbackOfCollectionResponseOfOrder() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.OrderEndpoint#list(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer, java.lang.Integer)}.
	 */
	@Disabled
	@Test
	void testListOrderResponseCallbackOfCollectionResponseOfOrderIntegerInteger() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.OrderEndpoint#delete(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Disabled
	@Test
	void testDeleteOrderResponseCallbackOfOrder() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.OrderEndpoint#delete(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)}.
	 */
	@Disabled
	@Test
	void testDeleteOrderResponseCallbackOfOrderInteger() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.OrderEndpoint#get(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Disabled
	@Test
	void testGetOrderResponseCallbackOfOrder() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.OrderEndpoint#builder()}.
	 */
	@Test
	void testBuilder() {
		LabelEndpoint.builder();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#setMerchant(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails)}.
	 */
	@Test
	void testSetMerchant() {

		LabelEndpoint.builder().setMerchant(CONFIG.getMerchant())
							  .enableSandboxEnvironment()
							  .build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#getOperationErrors()}.
	 */
	@Disabled
	@Test
	void testGetOperationErrors() {
		fail("Not yet implemented");
	}

	private ShipmentReference getShipmentReference() {
		ShipmentReference.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentReference.builder();
		
		builder.setValue("XYZ-001-01");
				
		return builder.build();
	}
	
	private SenderReferenceCollection getSenderReferences() {
		SenderReferenceCollection.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.SenderReferenceCollection.builder();
		
		builder.addSenderReference(SenderReference.builder().setReference("Order 001")
															.build());

		builder.addSenderReference(SenderReference.builder().setReference("SKU-1, SKU-2, SKU-3")
															.build());
		return builder.build();
	}
	
	private Boolean isEmailTracking() {
		return true;
	}
	
	private Sender getSender() {
		Sender.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Sender.builder();
		
		builder.setName(AusPostShipmentPersonalName.builder().setFullName("Otto Hung")
															 .build());
		
		builder.setAddressLines(AusPostShipmentAddressLines.builder().addAddress("2, 74 perrin drive")
																	 .build());
		builder.setSuburb(AusPostShipmentSuburb.builder().setName("MELBOURNE")
														 .build());
		builder.setState(AustralianState.Victoria);
		builder.setPostcode(AustralianPostcode.builder().setCode("3000")
														.build());
		builder.setPhone(AusPostShipmentPhoneNumber.builder().setNumber("0732723318")
															 .build());
		builder.setEmail(AusPostShipmentEmail.builder().setAddress("it@togauto.com.au")
													   .build());
		
		return builder.build();
	}
	
	private Receiver getReceiver() {
		Receiver.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Receiver.builder();
		
		builder.setName(AusPostShipmentPersonalName.builder().setFullName("Jane Smith")
															 .build());
		builder.setBusinessName(AusPostShipmentBusinessName.builder().setName("Smith Pty Ltd")
																	 .build());
		builder.setAddressLines(AusPostShipmentAddressLines.builder().addAddress("123 Centre Road")
																	 .build());
		builder.setSuburb(AusPostShipmentSuburb.builder().setName("Sydney")
														 .build());
		builder.setState(AustralianState.NewSouhtWales);
		builder.setPostcode(AustralianPostcode.builder().setCode("2000")
														.build());
		builder.setPhone(AusPostShipmentPhoneNumber.builder().setNumber("0412345678")
															 .build());
		builder.setEmail(AusPostShipmentEmail.builder().setAddress("jane.smith@smith.com")
													   .build());
		
		return builder.build();
	}
	
	private Receiver getReceiver1() {
		Receiver.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Receiver.builder();
		
		builder.setName(AusPostShipmentPersonalName.builder().setFullName("Black Smith")
															 .build());
		builder.setBusinessName(AusPostShipmentBusinessName.builder().setName("Smith Pty Ltd")
																	 .build());
		builder.setAddressLines(AusPostShipmentAddressLines.builder().addAddress("123 Left Road")
																	 .build());
		builder.setSuburb(AusPostShipmentSuburb.builder().setName("Canberra")
														 .build());
		builder.setState(AustralianState.AustralianCapitalTerritory);
		builder.setPostcode(AustralianPostcode.builder().setCode("2601")
														.build());
		builder.setPhone(AusPostShipmentPhoneNumber.builder().setNumber("0482345678")
															 .build());
		builder.setEmail(AusPostShipmentEmail.builder().setAddress("balck.smith@smith.com")
													   .build());
		
		return builder.build();
	}
	
	private Receiver getReceiver2() {
		Receiver.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Receiver.builder();
		
		builder.setName(AusPostShipmentPersonalName.builder().setFullName("Clock Smith")
															 .build());
		builder.setBusinessName(AusPostShipmentBusinessName.builder().setName("Smith Pty Ltd")
																	 .build());
		builder.setAddressLines(AusPostShipmentAddressLines.builder().addAddress("123 Right Road")
																	 .build());
		builder.setSuburb(AusPostShipmentSuburb.builder().setName("Brisbane")
														 .build());
		builder.setState(AustralianState.Queensland);
		builder.setPostcode(AustralianPostcode.builder().setCode("4000")
														.build());
		builder.setPhone(AusPostShipmentPhoneNumber.builder().setNumber("0411345678")
															 .build());
		builder.setEmail(AusPostShipmentEmail.builder().setAddress("clock.smith@smith.com")
													   .build());
		
		return builder.build();
	}
	
	private Receiver getReceiver3() {
		Receiver.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Receiver.builder();
		
		builder.setName(AusPostShipmentPersonalName.builder().setFullName("Stone Smith")
															 .build());
		builder.setBusinessName(AusPostShipmentBusinessName.builder().setName("Smith Pty Ltd")
																	 .build());
		builder.setAddressLines(AusPostShipmentAddressLines.builder().addAddress("123 Top Road")
																	 .build());
		builder.setSuburb(AusPostShipmentSuburb.builder().setName("Melbourne")
														 .build());
		builder.setState(AustralianState.Victoria);
		builder.setPostcode(AustralianPostcode.builder().setCode("3000")
														.build());
		builder.setPhone(AusPostShipmentPhoneNumber.builder().setNumber("0452345678")
															 .build());
		builder.setEmail(AusPostShipmentEmail.builder().setAddress("stone.smith@smith.com")
													   .build());
		
		return builder.build();
	}
	
	private Receiver getReceiver4() {
		Receiver.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Receiver.builder();
		
		builder.setName(AusPostShipmentPersonalName.builder().setFullName("House Smith")
															 .build());
		builder.setBusinessName(AusPostShipmentBusinessName.builder().setName("Smith Pty Ltd")
																	 .build());
		builder.setAddressLines(AusPostShipmentAddressLines.builder().addAddress("123 Down Road")
																	 .build());
		builder.setSuburb(AusPostShipmentSuburb.builder().setName("Perth")
														 .build());
		builder.setState(AustralianState.WesternAustralia);
		builder.setPostcode(AustralianPostcode.builder().setCode("6000")
														.build());
		builder.setPhone(AusPostShipmentPhoneNumber.builder().setNumber("0432345678")
															 .build());
		builder.setEmail(AusPostShipmentEmail.builder().setAddress("house.smith@smith.com")
													   .build());
		
		return builder.build();
	}
	
	private Receiver getReceiver5() {
		Receiver.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Receiver.builder();
		
		builder.setName(AusPostShipmentPersonalName.builder().setFullName("Boots Smith")
															 .build());
		builder.setBusinessName(AusPostShipmentBusinessName.builder().setName("Smith Pty Ltd")
																	 .build());
		builder.setAddressLines(AusPostShipmentAddressLines.builder().addAddress("123 Reverse Road")
																	 .build());
		builder.setSuburb(AusPostShipmentSuburb.builder().setName("hobart")
														 .build());
		builder.setState(AustralianState.Tasmania);
		builder.setPostcode(AustralianPostcode.builder().setCode("7000")
														.build());
		builder.setPhone(AusPostShipmentPhoneNumber.builder().setNumber("0402345678")
															 .build());
		builder.setEmail(AusPostShipmentEmail.builder().setAddress("boots.smith@smith.com")
													   .build());
		
		return builder.build();
	}
	
	private Receiver getReceiver6() {
		Receiver.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Receiver.builder();
		
		builder.setName(AusPostShipmentPersonalName.builder().setFullName("Boots Smith")
															 .build());
		builder.setBusinessName(AusPostShipmentBusinessName.builder().setName("Smith Pty Ltd")
																	 .build());
		builder.setAddressLines(AusPostShipmentAddressLines.builder().addAddress("123 Reverse Road")
																	 .build());
		builder.setSuburb(AusPostShipmentSuburb.builder().setName("adelaide")
														 .build());
		builder.setState(AustralianState.SouthAustralia);
		builder.setPostcode(AustralianPostcode.builder().setCode("5000")
														.build());
		builder.setPhone(AusPostShipmentPhoneNumber.builder().setNumber("0402345678")
															 .build());
		builder.setEmail(AusPostShipmentEmail.builder().setAddress("boots.smith@smith.com")
													   .build());
		
		return builder.build();
	}
	
	private Receiver getReceiver7() {
		Receiver.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Receiver.builder();
		
		builder.setName(AusPostShipmentPersonalName.builder().setFullName("Car Smith")
															 .build());
		builder.setBusinessName(AusPostShipmentBusinessName.builder().setName("Smith Pty Ltd")
																	 .build());
		builder.setAddressLines(AusPostShipmentAddressLines.builder().addAddress("123 Bottom Road")
																	 .build());
		builder.setSuburb(AusPostShipmentSuburb.builder().setName("Darwin")
														 .build());
		builder.setState(AustralianState.NorthernTerritory);
		builder.setPostcode(AustralianPostcode.builder().setCode("0800")
														.build());
		builder.setPhone(AusPostShipmentPhoneNumber.builder().setNumber("0492345678")
															 .build());
		builder.setEmail(AusPostShipmentEmail.builder().setAddress("car.smith@smith.com")
													   .build());
		
		return builder.build();
	}
	
	@SuppressWarnings("unused")
	private ItemCollection getItems() {
		ItemCollection.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemCollection.builder();
		
		builder.addItem(this.getItem1());
		builder.addItem(this.getItem2());
		builder.addItem(this.getItem3());
		
		return builder.build();
	}
	
	private Item getItem1() {
		Item.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Item.builder();
		
		builder.setReference(ItemReference.builder().setValue("SKU-1")
													.build());
		builder.setProductId(AustraliaPostPostageProducts.eParcelStandard.getProductId());
		builder.setLength(ItemLength.builder().setLength(10)
											  .build());
		builder.setWidth(ItemWidth.builder().setWidth(10)
											  .build());
		builder.setHeight(ItemHeight.builder().setHeight(10)
											  .build());

		builder.setWeight(ItemWeight.builder().setWeight(1)
											  .build());
		builder.setAuthorityToLeave(false);
		builder.setAllowPartialDelivery(true);
		builder.addItemFeature(ItemFeatureTransitCover.builder().setTransitCoverAmount(500)
													 			.build());		
		return builder.build();
	}
	
	@SuppressWarnings("unused")
	private Item getItem1OverTransitCoverAmount() {
		Item.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Item.builder();
		
		builder.setReference(ItemReference.builder().setValue("SKU-1")
													.build());
		builder.setProductId(AustraliaPostPostageProducts.eParcelStandard.getProductId());
		builder.setLength(ItemLength.builder().setLength(10)
											  .build());
		builder.setWidth(ItemWidth.builder().setWidth(10)
											  .build());
		builder.setHeight(ItemHeight.builder().setHeight(10)
											  .build());

		builder.setWeight(ItemWeight.builder().setWeight(1)
											  .build());
		builder.setAuthorityToLeave(false);
		builder.setAllowPartialDelivery(true);
		builder.addItemFeature(ItemFeatureTransitCover.builder().setTransitCoverAmount(1000)
													 			.build());		
		return builder.build();
	}
	
	private Item getItem2() {
		Item.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Item.builder();
		
		builder.setReference(ItemReference.builder().setValue("SKU-2")
													.build());
		builder.setProductId(AustraliaPostPostageProducts.eParcelStandard.getProductId());
		builder.setLength(ItemLength.builder().setLength(10)
											  .build());
		builder.setWidth(ItemWidth.builder().setWidth(10)
											  .build());
		builder.setHeight(ItemHeight.builder().setHeight(10)
											  .build());

		builder.setWeight(ItemWeight.builder().setWeight(1)
											  .build());
		builder.setAuthorityToLeave(false);
		builder.setAllowPartialDelivery(true);
		
		return builder.build();
	}
	
	private Item getItem3() {
		Item.Builder builder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Item.builder();
		
		builder.setReference(ItemReference.builder().setValue("SKU-3")
													.build());
		builder.setProductId(AustraliaPostPostageProducts.eParcelStandard.getProductId());
		builder.setLength(ItemLength.builder().setLength(10)
											  .build());
		builder.setWidth(ItemWidth.builder().setWidth(10)
											  .build());
		builder.setHeight(ItemHeight.builder().setHeight(10)
											  .build());

		builder.setWeight(ItemWeight.builder().setWeight(1)
											  .build());
		builder.setAuthorityToLeave(false);
		builder.setAllowPartialDelivery(true);		
		
		return builder.build();
	}
	
	private void printErrorMessage(AusPostErrorCollection errors, String exceptionMsg) {		
		if(null != errors && errors.validate()) {
			for (AusPostError error : errors.getErrors()) {
				String msg = null;
				if(null == error) {
					msg = String.format("No error returned with exception. Msg = %s", exceptionMsg);
				}else {
					msg = String.format("Code = %s, Name = %s, Msg = %s", error.getCode(), error.getName(), error.getMessage());
				}
				
				if(null != exceptionMsg) {
					msg += "\n" + exceptionMsg;
				}
				
				System.err.println(msg);
			}				
		}				
	}
	
	private Shipment createShipment(Shipment data) throws EndpointException {		
		ShipmentEndpoint endpoint = ShipmentEndpoint.builder().setMerchant(CONFIG.getMerchant())
															  .enableSandboxEnvironment()
															  .build();
		
		try {
			Shipment result = endpoint.create(data);			
			return result;
		}catch(EndpointException e) {
			this.printErrorMessage(endpoint.getOperationErrors(), e.getMessage());
			throw e;
		}
	}
	
	private List<Shipment> generateShipments(){
		List<Shipment> shipments = new ArrayList<>();
		
		Shipment.Builder shipmentBuilder = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.builder();
		shipmentBuilder.setShipmentReference(this.getShipmentReference());
		shipmentBuilder.setSenderReferences(this.getSenderReferences());
		shipmentBuilder.setEmailTracking(this.isEmailTracking());
		shipmentBuilder.setSender(this.getSender());
		shipmentBuilder.setReceiver(this.getReceiver());
		shipmentBuilder.addItem(this.getItem1());
		shipmentBuilder.addItem(this.getItem2());
		shipmentBuilder.addItem(this.getItem3());
		shipments.add(shipmentBuilder.build());
		
		Shipment.Builder shipmentBuilder1 = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.builder();
		shipmentBuilder1.setShipmentReference(this.getShipmentReference());
		shipmentBuilder1.setSenderReferences(this.getSenderReferences());
		shipmentBuilder1.setEmailTracking(this.isEmailTracking());
		shipmentBuilder1.setSender(this.getSender());
		shipmentBuilder1.setReceiver(this.getReceiver1());
		shipmentBuilder1.addItem(this.getItem1());
		shipmentBuilder1.addItem(this.getItem2());
		shipmentBuilder1.addItem(this.getItem3());
		shipments.add(shipmentBuilder1.build());
		
		Shipment.Builder shipmentBuilder2 = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.builder();
		shipmentBuilder2.setShipmentReference(this.getShipmentReference());
		shipmentBuilder2.setSenderReferences(this.getSenderReferences());
		shipmentBuilder2.setEmailTracking(this.isEmailTracking());
		shipmentBuilder2.setSender(this.getSender());
		shipmentBuilder2.setReceiver(this.getReceiver2());
		shipmentBuilder2.addItem(this.getItem1());
		shipmentBuilder2.addItem(this.getItem2());
		shipmentBuilder2.addItem(this.getItem3());
		shipments.add(shipmentBuilder2.build());
		
		Shipment.Builder shipmentBuilder3 = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.builder();
		shipmentBuilder3.setShipmentReference(this.getShipmentReference());
		shipmentBuilder3.setSenderReferences(this.getSenderReferences());
		shipmentBuilder3.setEmailTracking(this.isEmailTracking());
		shipmentBuilder3.setSender(this.getSender());
		shipmentBuilder3.setReceiver(this.getReceiver3());
		shipmentBuilder3.addItem(this.getItem1());
		shipmentBuilder3.addItem(this.getItem2());
		shipmentBuilder3.addItem(this.getItem3());
		shipments.add(shipmentBuilder3.build());
		
		Shipment.Builder shipmentBuilder4 = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.builder();
		shipmentBuilder4.setShipmentReference(this.getShipmentReference());
		shipmentBuilder4.setSenderReferences(this.getSenderReferences());
		shipmentBuilder4.setEmailTracking(this.isEmailTracking());
		shipmentBuilder4.setSender(this.getSender());
		shipmentBuilder4.setReceiver(this.getReceiver4());
		shipmentBuilder4.addItem(this.getItem1());
		shipmentBuilder4.addItem(this.getItem2());
		shipmentBuilder4.addItem(this.getItem3());
		shipments.add(shipmentBuilder4.build());
		
		Shipment.Builder shipmentBuilder5 = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.builder();
		shipmentBuilder5.setShipmentReference(this.getShipmentReference());
		shipmentBuilder5.setSenderReferences(this.getSenderReferences());
		shipmentBuilder5.setEmailTracking(this.isEmailTracking());
		shipmentBuilder5.setSender(this.getSender());
		shipmentBuilder5.setReceiver(this.getReceiver5());
		shipmentBuilder5.addItem(this.getItem1());
		shipmentBuilder5.addItem(this.getItem2());
		shipmentBuilder5.addItem(this.getItem3());
		shipments.add(shipmentBuilder5.build());
		
		Shipment.Builder shipmentBuilder6 = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.builder();
		shipmentBuilder6.setShipmentReference(this.getShipmentReference());
		shipmentBuilder6.setSenderReferences(this.getSenderReferences());
		shipmentBuilder6.setEmailTracking(this.isEmailTracking());
		shipmentBuilder6.setSender(this.getSender());
		shipmentBuilder6.setReceiver(this.getReceiver6());
		shipmentBuilder6.addItem(this.getItem1());
		shipmentBuilder6.addItem(this.getItem2());
		shipmentBuilder6.addItem(this.getItem3());
		shipments.add(shipmentBuilder6.build());
		
		Shipment.Builder shipmentBuilder7 = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.builder();
		shipmentBuilder7.setShipmentReference(this.getShipmentReference());
		shipmentBuilder7.setSenderReferences(this.getSenderReferences());
		shipmentBuilder7.setEmailTracking(this.isEmailTracking());
		shipmentBuilder7.setSender(this.getSender());
		shipmentBuilder7.setReceiver(this.getReceiver7());
		shipmentBuilder7.addItem(this.getItem1());
		shipmentBuilder7.addItem(this.getItem2());
		shipmentBuilder7.addItem(this.getItem3());
		shipments.add(shipmentBuilder7.build());
		
		return shipments;
	}
	
	private List<Label> generateLabels(Shipment shipment) throws EndpointException {		
		//Create label
		LabelEndpoint endpoint = LabelEndpoint.builder().setMerchant(CONFIG.getMerchant())
														.enableSandboxEnvironment()
														.build();
		
		try {
			LabelCollection labels = endpoint.create(shipment, PREFERENCE);
		
			this.labelGenerationMsg		= endpoint.getGenerationProgressMessage().getValue();
			this.labelGenerationCode	= endpoint.getGenerationProgressCode().getValue();
			this.errors					= endpoint.getOperationErrors();
			
			return labels.getLabels();
		}catch(EndpointException e) {
			this.printErrorMessage(endpoint.getOperationErrors(), e.getMessage());
			
			
			throw e;
		}
	}
	
	private OrderReference generateOrderReference() {
		LocalDateTime localDate = LocalDateTime.now();
		
		String no = localDate.toString();
		
		OrderReference ref = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.OrderReference.builder().setValue(no)
																																				 .build();
		return ref;
	}
	
	private Consignor getConsignor() {
		return com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.Consignor.builder().setValue("QB-BOX")
																															  .build();
	}
}
