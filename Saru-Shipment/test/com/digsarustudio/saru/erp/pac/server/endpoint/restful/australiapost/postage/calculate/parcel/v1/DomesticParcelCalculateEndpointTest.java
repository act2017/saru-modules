/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calculate.parcel.v1;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.digsarustudio.saru.erp.musaceae.shared.FreightSize;
import com.digsarustudio.saru.erp.musaceae.shared.Length;
import com.digsarustudio.saru.erp.musaceae.shared.Weight;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.AustralianPostcode;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.AuthenticationDetails;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.ResponseCallback;
import com.digsarustudio.saru.erp.musaceae.shared.javabean.UnitPrice;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.DefaultPACAuthKey;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.exception.UnknownError403Exception;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption;
import com.digsarustudio.saru.erp.pac.shared.javabean.FreightLength;
import com.digsarustudio.saru.erp.pac.shared.javabean.FreightWeight;

/**
 * To test {@link DomesticParcelCalculateEndpoint}<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
class DomesticParcelCalculateEndpointTest {
	private static AuthenticationDetails	AUTH_KEY = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		AUTH_KEY = new DefaultPACAuthKey();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#setSenderPostcode(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode)}.
	 */
	@Test
	void testSetSenderPostcode() {
		@SuppressWarnings("unused")
		DomesticParcelCalculateEndpoint endpoint = DomesticParcelCalculateEndpoint.builder().setSenderPostcode(AustralianPostcode.builder().setCode("4119")
																																	  .build())
																						.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#setReceiverPostcode(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode)}.
	 */
	@Test
	void testSetReceiverPostcode() {
		@SuppressWarnings("unused")
		DomesticParcelCalculateEndpoint endpoint = DomesticParcelCalculateEndpoint.builder().setReceiverPostcode(AustralianPostcode.builder().setCode("4000")
																																		.build())
																						.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#setFreightSize(com.digsarustudio.saru.erp.musaceae.shared.FreightSize)}.
	 */
	@Test
	void testSetFreightSize() {
		Length 		width	= FreightLength.builder().setValue("20").build();
		Length 		height	= FreightLength.builder().setValue("10").build();
		Length 		length	= FreightLength.builder().setValue("30").build();
		FreightSize	size	= com.digsarustudio.saru.erp.pac.shared.javabean.FreightSize.builder().setWidth(width)
																								  .setLength(length)
																								  .setHeight(height)
																								  .build();
		@SuppressWarnings("unused")
		DomesticParcelCalculateEndpoint endpoint = DomesticParcelCalculateEndpoint.builder().setFreightSize(size)
																						.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#setFreightWeight(com.digsarustudio.saru.erp.musaceae.shared.Weight)}.
	 */
	@Test
	void testSetFreightWeight() {
		Weight		weight	= FreightWeight.builder().setValue("4").build();
		
		@SuppressWarnings("unused")
		DomesticParcelCalculateEndpoint endpoint = DomesticParcelCalculateEndpoint.builder().setFreightWeight(weight)
																						.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#setParcelService(ParcelService)}.
	 */
	@Test
	void testSetParcelService() {		
		ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().setCode("AUS_SERVICE_OPTION_SIGNATURE_ON_DELIVERY")
																																							   .build();
		
		@SuppressWarnings("unused")
		DomesticParcelCalculateEndpoint endpoint = DomesticParcelCalculateEndpoint.builder().setParcelService(service)
																							.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#setExtraCover(com.digsarustudio.banana.erp.general.UnitPrice)}.
	 */
	@Test
	void testSetExtraCover() {
		UnitPrice extraCover	= UnitPrice.builder().setPrice("100")
													 .build();
		
		@SuppressWarnings("unused")
		DomesticParcelCalculateEndpoint endpoint = DomesticParcelCalculateEndpoint.builder().setExtraCover(extraCover)
																							.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#list(com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 * @throws EndpointException 
	 */
	@Test
	void testListResponseCallbackOfCollectionResponseOfPostageResult() throws EndpointException {
		Length 		width	= FreightLength.builder().setValue("20").build();
		Length 		height	= FreightLength.builder().setValue("10").build();
		Length 		length	= FreightLength.builder().setValue("30").build();
		FreightSize	size	= com.digsarustudio.saru.erp.pac.shared.javabean.FreightSize.builder().setWidth(width)
																								  .setLength(length)
																								  .setHeight(height)
																								  .build();
		Weight		weight	= FreightWeight.builder().setValue("4").build();
		
		ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().setCode("AUS_PARCEL_REGULAR")
																																							   .build();
		
		UnitPrice extraCover	= UnitPrice.builder().setPrice("10000")
													 .build();
		
		DomesticParcelCalculateEndpoint endpoint = DomesticParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																							.setSenderPostcode(AustralianPostcode.builder().setCode("4119")
																																		  .build())
																							.setReceiverPostcode(AustralianPostcode.builder().setCode("4000")
																																			.build())
																							.setFreightSize(size)
																							.setFreightWeight(weight)
																							.setParcelService(service)
																							.setExtraCover(extraCover)
																							.build();

		endpoint.list(new ResponseCallback<CollectionResponse<PostageResult>>() {

			@Override
			public void onSuccess(CollectionResponse<PostageResult> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertNotEquals(0, result.getItems());
				
				Iterator<PostageResult> iterator = result.getItems().iterator();
				while(iterator.hasNext()) {
					System.out.println(iterator.next().toString());
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get country from CountryEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#insert(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.PostageResult, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testInsert() {
		assertThrows(UnsupportedOperationException.class, () -> {

			DomesticParcelCalculateEndpoint endpoint = DomesticParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			PostageResult result = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResult.builder().build();
			endpoint.insert(result, new ResponseCallback<PostageResult>() {

				@Override
				public void onSuccess(PostageResult result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#update(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.PostageResult, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testUpdatePostageResultResponseCallbackOfPostageResult() {
		assertThrows(UnsupportedOperationException.class, () -> {
			DomesticParcelCalculateEndpoint endpoint = DomesticParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			PostageResult result = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResult.builder().build();
			endpoint.update(result, new ResponseCallback<PostageResult>() {

				@Override
				public void onSuccess(PostageResult result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#update(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.PostageResult, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)}.
	 */
	@Test
	void testUpdatePostageResultResponseCallbackOfPostageResultInteger() {
		assertThrows(UnsupportedOperationException.class, () -> {

			DomesticParcelCalculateEndpoint endpoint = DomesticParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			PostageResult result = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResult.builder().build();
			endpoint.update(result, new ResponseCallback<PostageResult>() {

				@Override
				public void onSuccess(PostageResult result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#list(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.PostageResult, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testListPostageResultByCondition() {
		assertThrows(UnsupportedOperationException.class, () -> {

			DomesticParcelCalculateEndpoint endpoint = DomesticParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			PostageResult result = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResult.builder().build();
			endpoint.list(result, new ResponseCallback<CollectionResponse<PostageResult>>() {

				@Override
				public void onSuccess(CollectionResponse<PostageResult> result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#list(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.PostageResult, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer, java.lang.Integer)}.
	 */
	@Test
	void testListPostageResultWithCursor() {

		assertThrows(UnsupportedOperationException.class, () -> {

			DomesticParcelCalculateEndpoint endpoint = DomesticParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			PostageResult result = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResult.builder().build();
			endpoint.list(result, new ResponseCallback<CollectionResponse<PostageResult>>() {

				@Override
				public void onSuccess(CollectionResponse<PostageResult> result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			}, 0, 0);
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#delete(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.PostageResult, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testDeletePostageResultResponseCallbackOfPostageResult() {

		assertThrows(UnsupportedOperationException.class, () -> {

			DomesticParcelCalculateEndpoint endpoint = DomesticParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			PostageResult result = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResult.builder().build();
			endpoint.delete(result, new ResponseCallback<PostageResult>() {

				@Override
				public void onSuccess(PostageResult result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#delete(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.PostageResult, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)}.
	 */
	@Test
	void testDeletePostageResultResponseCallbackOfPostageResultInteger() {

		assertThrows(UnsupportedOperationException.class, () -> {

			DomesticParcelCalculateEndpoint endpoint = DomesticParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			PostageResult result = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResult.builder().build();
			endpoint.delete(result, new ResponseCallback<PostageResult>() {

				@Override
				public void onSuccess(PostageResult result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#get(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.PostageResult, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testGet() {
		assertThrows(UnsupportedOperationException.class, () -> {
			DomesticParcelCalculateEndpoint endpoint = DomesticParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			PostageResult result = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResult.builder().build();
			endpoint.get(result, new ResponseCallback<PostageResult>() {

				@Override
				public void onSuccess(PostageResult result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#builder()}.
	 */
	@SuppressWarnings("unused")
	@Test
	public void testBuilder() {
		DomesticParcelCalculateEndpoint endpoint = DomesticParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																						.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint#setAuthKey(com.digsarustudio.banana.endpoint.AuthenticationDetails)}.
	 */
	@Test
	void testSetAuthKey() {
		@SuppressWarnings("unused")
		DomesticParcelCalculateEndpoint endpoint = DomesticParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY).build();
	}

	@Test
	public void testInvalidAuthKey() throws EndpointException {
		DomesticParcelCalculateEndpoint endpoint = DomesticParcelCalculateEndpoint.builder().build();
		
		endpoint.list(new ResponseCallback<CollectionResponse<PostageResult>>() {
			
			@Override
			public void onSuccess(CollectionResponse<PostageResult> result) {
				fail("This should cause an exception due to the auth-key has not been provided.");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				assertNotNull(caught);
				assertNotNull(caught.getMessage());
				assertFalse(caught.getMessage().isEmpty());
				assertTrue(caught instanceof UnknownError403Exception);
				
				System.err.println("testInvalidAuthKey(): " + caught.getMessage());
			}
		});
	}
	
	@Test
	public void testDomesticParcelCalculateWithEachParcelService() throws EndpointException {
		Length 		width	= FreightLength.builder().setValue("20").build();
		Length 		height	= FreightLength.builder().setValue("10").build();
		Length 		length	= FreightLength.builder().setValue("30").build();
		FreightSize	size	= com.digsarustudio.saru.erp.pac.shared.javabean.FreightSize.builder().setWidth(width)
																								  .setLength(length)
																								  .setHeight(height)
																								  .build();
		Weight		weight	= FreightWeight.builder().setValue("4").build();
		
		DomesticParcelServiceEndpoint endpoint = DomesticParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																						.setSenderPostcode(AustralianPostcode.builder().setCode("4119")
																																	  .build())
																						.setReceiverPostcode(AustralianPostcode.builder().setCode("4000")
																																		.build())
																						.setFreightSize(size)
																						.setFreightWeight(weight)
																						.build();
		//Note: Crash due to the mixed type of Parcel Service Option
		endpoint.list(new ResponseCallback<CollectionResponse<ParcelService>>() {

			@Override
			public void onSuccess(CollectionResponse<ParcelService> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertNotEquals(0, result.getItems());
				
				Iterator<ParcelService> iterator = result.getItems().iterator();
				while(iterator.hasNext()) {
					ParcelService service = iterator.next();
					
					if(!service.hasServiceOption()) {
						System.out.println("== service: " + service.getName() + " (" + service.getCode() + ") ==");
						try {
							testDomesticParcelCalculate(service);
						} catch (Exception e) {
							fail("should pass this test");
						}
						
					}else {						
						List<ParcelServiceOption> serviceOptions = service.getServiceOptionCollection().getOptionServices();
						while(!serviceOptions.isEmpty()) {
							System.out.println("== service: " + serviceOptions.get(0).getName() + " (" + serviceOptions.get(0).getCode() + ") ==");
							try {
								testDomesticParcelCalculate(service);
							} catch (EndpointException e) {
								fail("should pass this test");
							}
							
							serviceOptions.remove(0);
						}
					}
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get country from CountryEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
	}
	
	public void testDomesticParcelCalculate(ParcelService service) throws EndpointException {
		Length 		width	= FreightLength.builder().setValue("20").build();
		Length 		height	= FreightLength.builder().setValue("10").build();
		Length 		length	= FreightLength.builder().setValue("30").build();
		FreightSize	size	= com.digsarustudio.saru.erp.pac.shared.javabean.FreightSize.builder().setWidth(width)
																								  .setLength(length)
																								  .setHeight(height)
																								  .build();
		Weight		weight	= FreightWeight.builder().setValue("4").build();		
		UnitPrice extraCover	= UnitPrice.builder().setPrice("100")
													 .build();
		
		DomesticParcelCalculateEndpoint endpoint = DomesticParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																						.setSenderPostcode(AustralianPostcode.builder().setCode("4119")
																																	  .build())
																						.setReceiverPostcode(AustralianPostcode.builder().setCode("4000")
																																		.build())
																						.setFreightSize(size)
																						.setFreightWeight(weight)
																						.setParcelService(service)
																						.setExtraCover(extraCover)
																						.build();
		endpoint.list(new ResponseCallback<CollectionResponse<PostageResult>>() {

			@Override
			public void onSuccess(CollectionResponse<PostageResult> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertNotEquals(0, result.getItems());
				
				Iterator<PostageResult> iterator = result.getItems().iterator();
				while(iterator.hasNext()) {
					System.out.println(iterator.next().toString());
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get country from CountryEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
		
	}
}
