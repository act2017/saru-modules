/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calculate.parcel.v1;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.digsarustudio.saru.erp.musaceae.shared.Weight;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.AuthenticationDetails;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.ResponseCallback;
import com.digsarustudio.saru.erp.musaceae.shared.javabean.UnitPrice;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.DefaultPACAuthKey;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.exception.UnknownError403Exception;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.InternationalParcelCalculateEndpoint;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.country.v1.CountryEndpoint;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.InternationalParcelServiceEndpoint;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country.Country;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption;
import com.digsarustudio.saru.erp.pac.shared.javabean.FreightWeight;

/**
 * To test {@link InternationalParcelCalculateEndpoint}<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
class InternationalParcelCalculateEndpointTest {
	private static AuthenticationDetails	AUTH_KEY = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		AUTH_KEY = new DefaultPACAuthKey();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.InternationalParcelCalculateEndpoint#setDestinationCountryCode(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country.Country)}.
	 */
	@Test
	void testSetCountrycode() {
		@SuppressWarnings("unused")
		InternationalParcelCalculateEndpoint endpoint = InternationalParcelCalculateEndpoint.builder().setDestinationCountryCode(com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.country.v1.javabean.Country.builder().setCode("NZ")
																																				  																									.build())
																									  .build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.InternationalParcelCalculateEndpoint#setFreightWeight(com.digsarustudio.saru.erp.musaceae.shared.Weight)}.
	 */
	@Test
	void testSetFreightWeight() {
		Weight		weight	= FreightWeight.builder().setValue("4").build();
		
		@SuppressWarnings("unused")
		InternationalParcelCalculateEndpoint endpoint = InternationalParcelCalculateEndpoint.builder().setFreightWeight(weight)
																						.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.InternationalParcelCalculateEndpoint#setParcelService(ParcelService)}.
	 */
	@Test
	void testSetParcelService() {		
		ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().setCode("AUS_SERVICE_OPTION_SIGNATURE_ON_DELIVERY")
																																							   .build();
		
		@SuppressWarnings("unused")
		InternationalParcelCalculateEndpoint endpoint = InternationalParcelCalculateEndpoint.builder().setParcelService(service)
																							.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.InternationalParcelCalculateEndpoint#setExtraCover(com.digsarustudio.banana.erp.general.UnitPrice)}.
	 */
	@Test
	void testSetExtraCover() {
		UnitPrice extraCover	= UnitPrice.builder().setPrice("100")
													 .build();
		
		@SuppressWarnings("unused")
		InternationalParcelCalculateEndpoint endpoint = InternationalParcelCalculateEndpoint.builder().setExtraCover(extraCover)
																							.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.InternationalParcelCalculateEndpoint#list(com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 * @throws EndpointException 
	 */
	@Test
	void testListResponseCallbackOfCollectionResponseOfPostageResult() throws EndpointException {
		Country destinationCountry	= com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.country.v1.javabean.Country.builder().setCode("BA")
													   																									.build();
		Weight		weight	= FreightWeight.builder().setValue("4").build();
		
		ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().setCode("INT_PARCEL_EXP_OWN_PACKAGING")
																																							   .build();
		
		UnitPrice extraCover	= UnitPrice.builder().setPrice("10000")
													 .build();
		
		InternationalParcelCalculateEndpoint endpoint = InternationalParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																									  .setDestinationCountryCode(destinationCountry)
																									  .setFreightWeight(weight)
																									  .setParcelService(service)
																									  .setExtraCover(extraCover)
																  .build();
		endpoint.list(new ResponseCallback<CollectionResponse<PostageResult>>() {

			@Override
			public void onSuccess(CollectionResponse<PostageResult> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertNotEquals(0, result.getItems());
				
				Iterator<PostageResult> iterator = result.getItems().iterator();
				while(iterator.hasNext()) {
					System.out.println(iterator.next().toString());
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get country from CountryEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.InternationalParcelCalculateEndpoint#insert(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.PostageResult, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testInsert() {
		assertThrows(UnsupportedOperationException.class, () -> {

			InternationalParcelCalculateEndpoint endpoint = InternationalParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			PostageResult result = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResult.builder().build();
			endpoint.insert(result, new ResponseCallback<PostageResult>() {

				@Override
				public void onSuccess(PostageResult result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.InternationalParcelCalculateEndpoint#update(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.PostageResult, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testUpdatePostageResultResponseCallbackOfPostageResult() {
		assertThrows(UnsupportedOperationException.class, () -> {
			InternationalParcelCalculateEndpoint endpoint = InternationalParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			PostageResult result = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResult.builder().build();
			endpoint.update(result, new ResponseCallback<PostageResult>() {

				@Override
				public void onSuccess(PostageResult result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.InternationalParcelCalculateEndpoint#update(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.PostageResult, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)}.
	 */
	@Test
	void testUpdatePostageResultResponseCallbackOfPostageResultInteger() {
		assertThrows(UnsupportedOperationException.class, () -> {

			InternationalParcelCalculateEndpoint endpoint = InternationalParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			PostageResult result = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResult.builder().build();
			endpoint.update(result, new ResponseCallback<PostageResult>() {

				@Override
				public void onSuccess(PostageResult result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.InternationalParcelCalculateEndpoint#list(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.PostageResult, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testListPostageResultByCondition() {
		assertThrows(UnsupportedOperationException.class, () -> {

			InternationalParcelCalculateEndpoint endpoint = InternationalParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			PostageResult result = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResult.builder().build();
			endpoint.list(result, new ResponseCallback<CollectionResponse<PostageResult>>() {

				@Override
				public void onSuccess(CollectionResponse<PostageResult> result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.InternationalParcelCalculateEndpoint#list(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.PostageResult, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer, java.lang.Integer)}.
	 */
	@Test
	void testListPostageResultWithCursor() {

		assertThrows(UnsupportedOperationException.class, () -> {

			InternationalParcelCalculateEndpoint endpoint = InternationalParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			PostageResult result = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResult.builder().build();
			endpoint.list(result, new ResponseCallback<CollectionResponse<PostageResult>>() {

				@Override
				public void onSuccess(CollectionResponse<PostageResult> result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			}, 0, 0);
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.InternationalParcelCalculateEndpoint#delete(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.PostageResult, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testDeletePostageResultResponseCallbackOfPostageResult() {

		assertThrows(UnsupportedOperationException.class, () -> {

			InternationalParcelCalculateEndpoint endpoint = InternationalParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			PostageResult result = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResult.builder().build();
			endpoint.delete(result, new ResponseCallback<PostageResult>() {

				@Override
				public void onSuccess(PostageResult result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.InternationalParcelCalculateEndpoint#delete(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.PostageResult, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)}.
	 */
	@Test
	void testDeletePostageResultResponseCallbackOfPostageResultInteger() {

		assertThrows(UnsupportedOperationException.class, () -> {

			InternationalParcelCalculateEndpoint endpoint = InternationalParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			PostageResult result = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResult.builder().build();
			endpoint.delete(result, new ResponseCallback<PostageResult>() {

				@Override
				public void onSuccess(PostageResult result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.InternationalParcelCalculateEndpoint#get(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.PostageResult, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testGet() {
		assertThrows(UnsupportedOperationException.class, () -> {
			InternationalParcelCalculateEndpoint endpoint = InternationalParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			PostageResult result = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResult.builder().build();
			endpoint.get(result, new ResponseCallback<PostageResult>() {

				@Override
				public void onSuccess(PostageResult result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.InternationalParcelCalculateEndpoint#builder()}.
	 */
	@SuppressWarnings("unused")
	@Test
	public void testBuilder() {
		InternationalParcelCalculateEndpoint endpoint = InternationalParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																						.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint#setAuthKey(com.digsarustudio.banana.endpoint.AuthenticationDetails)}.
	 */
	@Test
	void testSetAuthKey() {
		@SuppressWarnings("unused")
		InternationalParcelCalculateEndpoint endpoint = InternationalParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY).build();
	}

	@Test
	public void testInvalidAuthKey() throws EndpointException {
		InternationalParcelCalculateEndpoint endpoint = InternationalParcelCalculateEndpoint.builder().build();
		
		endpoint.list(new ResponseCallback<CollectionResponse<PostageResult>>() {
			
			@Override
			public void onSuccess(CollectionResponse<PostageResult> result) {
				fail("This should cause an exception due to the auth-key has not been provided.");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				assertNotNull(caught);
				assertNotNull(caught.getMessage());
				assertFalse(caught.getMessage().isEmpty());
				assertTrue(caught instanceof UnknownError403Exception);
				
				System.err.println("testInvalidAuthKey(): " + caught.getMessage());
			}
		});
	}
	
	@Test
	public void testInternationalParcelCalculateWithEachCountry() throws EndpointException {
		CountryEndpoint endpoint = CountryEndpoint.builder().setAuthKey(AUTH_KEY)
															.build();
		
		endpoint.list(new ResponseCallback<CollectionResponse<Country>>() {

			@Override
			public void onSuccess(CollectionResponse<Country> result) {
				if(null == result || null == result.getItems() || result.getItems().isEmpty()) {
					return;
				}
				
				Iterator<Country> iterator = result.getItems().iterator();
				while(iterator.hasNext()) {
					Country country = iterator.next();
					
					System.out.println("== ship to " + country.getName() + " ==");
					try {
						testInternationalParcelCalculateWithEachParcelService(country);
					} catch (EndpointException e) {
						fail("Should pass this test");
					}
				}				 				
			}

			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get country from CountryEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);				
			}
		});
	}
	
	public void testInternationalParcelCalculateWithEachParcelService(final Country country) throws EndpointException {
		final Weight		weight	= FreightWeight.builder().setValue("4").build();
		final UnitPrice		extraCover	= UnitPrice.builder().setPrice("1000").build();
		
		InternationalParcelServiceEndpoint endpoint = InternationalParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																								  .setCountry(country)
																								  .setFreightWeight(weight)
																								  .build();
		//Note: Crash due to the mixed type of Parcel Service Option
		endpoint.list(new ResponseCallback<CollectionResponse<ParcelService>>() {

			@Override
			public void onSuccess(CollectionResponse<ParcelService> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertNotEquals(0, result.getItems());
				
				Iterator<ParcelService> iterator = result.getItems().iterator();
				while(iterator.hasNext()) {
					ParcelService service = iterator.next();
					
					if(!service.hasServiceOption()) {
						System.out.println("== service: " + service.getName() + " (" + service.getCode() + ") ==");
						try {
							testInternationalParcelCalculate(country, weight, extraCover, service);
						} catch (EndpointException e) {
							fail("Should pass this test");
						}
					}else {						
						List<ParcelServiceOption> serviceOptions = service.getServiceOptionCollection().getOptionServices();
						while(!serviceOptions.isEmpty()) {
							System.out.println("== service: " + serviceOptions.get(0).getName() + " (" + serviceOptions.get(0).getCode() + ") ==");
							try {
								testInternationalParcelCalculate(country, weight, extraCover, service);
							} catch (EndpointException e) {
								fail("Should pass this test");
							}
							
							serviceOptions.remove(0);
						}
					}
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get service for " + country.getName() + " from InternationalParcelServiceEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
	}
	
	public void testInternationalParcelCalculate(Country destinationCountry, Weight weight, UnitPrice extraCover, ParcelService service) throws EndpointException {
		InternationalParcelCalculateEndpoint endpoint = InternationalParcelCalculateEndpoint.builder().setAuthKey(AUTH_KEY)
																									  .setDestinationCountryCode(destinationCountry)
																									  .setFreightWeight(weight)
																									  .setParcelService(service)
																									  .setExtraCover(extraCover)
																									  .build();
		endpoint.list(new ResponseCallback<CollectionResponse<PostageResult>>() {

			@Override
			public void onSuccess(CollectionResponse<PostageResult> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertNotEquals(0, result.getItems());
				
				Iterator<PostageResult> iterator = result.getItems().iterator();
				while(iterator.hasNext()) {
					System.out.println(iterator.next().toString());
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get calculate the price for shipping to " + destinationCountry.getName() + " from InternationalParcelCalculateEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
		
	}
}
