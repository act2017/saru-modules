/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Iterator;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.digsarustudio.saru.erp.musaceae.shared.Weight;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.AuthenticationDetails;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.ResponseCallback;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.DefaultPACAuthKey;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.exception.UnknownError403Exception;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.country.v1.CountryEndpoint;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.InternationalParcelServiceEndpoint;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country.Country;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService;
import com.digsarustudio.saru.erp.pac.shared.javabean.FreightWeight;

/**
 * To test {@link InternationalParcelServiceEndpoint}<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
class InternationalParcelServiceEndpointTest {
	private static final String		DEFAULT_COUNTRY_CODE	= "US";
	
	private static AuthenticationDetails	AUTH_KEY = null;	

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		AUTH_KEY = new DefaultPACAuthKey();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.InternationalParcelServiceEndpoint#setCountry(Country)}.
	 */	
	@Test
	void testSetCountry() {
		Country		country = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.country.v1.javabean.Country.builder().setCode("US")
																																				.build();
		@SuppressWarnings("unused")
		InternationalParcelServiceEndpoint endpoint = InternationalParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																						.setCountry(country)
																						.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.InternationalParcelServiceEndpoint#setFreightWeight(com.digsarustudio.saru.erp.musaceae.shared.Weight)}.
	 */
	@Test
	void testSetFreightWeight() {
		Weight		weight	= FreightWeight.builder().setValue("4").build();
		
		@SuppressWarnings("unused")
		InternationalParcelServiceEndpoint endpoint = InternationalParcelServiceEndpoint.builder().setFreightWeight(weight)
																						.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.InternationalParcelServiceEndpoint#list(com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 * @throws EndpointException 
	 */
	@Test
	void testListResponseCallbackOfCollectionResponseOfParcelService() throws EndpointException {
		Country		country = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.country.v1.javabean.Country.builder().setCode(DEFAULT_COUNTRY_CODE)
																																				.build();
		
		testInternationalParcelServiceByCountry(country);
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.InternationalParcelServiceEndpoint#insert(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testInsert() {
		assertThrows(UnsupportedOperationException.class, () -> {

			InternationalParcelServiceEndpoint endpoint = InternationalParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().build();
			endpoint.insert(service, new ResponseCallback<ParcelService>() {

				@Override
				public void onSuccess(ParcelService result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.InternationalParcelServiceEndpoint#update(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testUpdateParcelServiceResponseCallbackOfParcelService() {
		assertThrows(UnsupportedOperationException.class, () -> {
			InternationalParcelServiceEndpoint endpoint = InternationalParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().build();
			endpoint.update(service, new ResponseCallback<ParcelService>() {

				@Override
				public void onSuccess(ParcelService result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.InternationalParcelServiceEndpoint#update(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)}.
	 */
	@Test
	void testUpdateParcelServiceResponseCallbackOfParcelServiceInteger() {
		assertThrows(UnsupportedOperationException.class, () -> {

			InternationalParcelServiceEndpoint endpoint = InternationalParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().build();
			endpoint.update(service, new ResponseCallback<ParcelService>() {

				@Override
				public void onSuccess(ParcelService result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.InternationalParcelServiceEndpoint#list(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testListParcelServiceByCondition() {
		assertThrows(UnsupportedOperationException.class, () -> {

			InternationalParcelServiceEndpoint endpoint = InternationalParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().build();
			endpoint.list(service, new ResponseCallback<CollectionResponse<ParcelService>>() {

				@Override
				public void onSuccess(CollectionResponse<ParcelService> result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.InternationalParcelServiceEndpoint#list(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer, java.lang.Integer)}.
	 */
	@Test
	void testListParcelServiceWithCursor() {

		assertThrows(UnsupportedOperationException.class, () -> {

			InternationalParcelServiceEndpoint endpoint = InternationalParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().build();
			endpoint.list(service, new ResponseCallback<CollectionResponse<ParcelService>>() {

				@Override
				public void onSuccess(CollectionResponse<ParcelService> result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			}, 0, 0);
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.InternationalParcelServiceEndpoint#delete(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testDeleteParcelServiceResponseCallbackOfParcelService() {

		assertThrows(UnsupportedOperationException.class, () -> {

			InternationalParcelServiceEndpoint endpoint = InternationalParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().build();
			endpoint.delete(service, new ResponseCallback<ParcelService>() {

				@Override
				public void onSuccess(ParcelService result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.InternationalParcelServiceEndpoint#delete(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)}.
	 */
	@Test
	void testDeleteParcelServiceResponseCallbackOfParcelServiceInteger() {

		assertThrows(UnsupportedOperationException.class, () -> {

			InternationalParcelServiceEndpoint endpoint = InternationalParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().build();
			endpoint.delete(service, new ResponseCallback<ParcelService>() {

				@Override
				public void onSuccess(ParcelService result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.InternationalParcelServiceEndpoint#get(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testGet() {
		assertThrows(UnsupportedOperationException.class, () -> {
			InternationalParcelServiceEndpoint endpoint = InternationalParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().build();
			endpoint.get(service, new ResponseCallback<ParcelService>() {

				@Override
				public void onSuccess(ParcelService result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.InternationalParcelServiceEndpoint#builder()}.
	 */
	@SuppressWarnings("unused")
	@Test
	public void testBuilder() {
		InternationalParcelServiceEndpoint endpoint = InternationalParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																						.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint#setAuthKey(com.digsarustudio.banana.endpoint.AuthenticationDetails)}.
	 */
	@Test
	void testSetAuthKey() {
		@SuppressWarnings("unused")
		InternationalParcelServiceEndpoint endpoint = InternationalParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY).build();
	}

	@Test
	public void testInvalidAuthKey() throws EndpointException {
		InternationalParcelServiceEndpoint endpoint = InternationalParcelServiceEndpoint.builder().build();
		
		endpoint.list(new ResponseCallback<CollectionResponse<ParcelService>>() {
			
			@Override
			public void onSuccess(CollectionResponse<ParcelService> result) {
				fail("This should cause an exception due to the auth-key has not been provided.");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				assertNotNull(caught);
				assertNotNull(caught.getMessage());
				assertFalse(caught.getMessage().isEmpty());
				assertTrue(caught instanceof UnknownError403Exception);
				
				System.err.println("testInvalidAuthKey(): " + caught.getMessage());
			}
		});
	}
	
	@Test
	public void testWithAllofTheCountries() throws EndpointException {

		CountryEndpoint endpoint = CountryEndpoint.builder().setAuthKey(AUTH_KEY)
															.build();
		
		endpoint.list(new ResponseCallback<CollectionResponse<Country>>() {
			
			@Override
			public void onSuccess(CollectionResponse<Country> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertNotEquals(0, result.getItems());
				
				Iterator<Country> iterator = result.getItems().iterator();
				while(iterator.hasNext()) {
					Country country = iterator.next();
					try {
						System.out.println("== To get parcel service for " + country.getName() + "==");
						try {
							testInternationalParcelServiceByCountry(country);
						} catch (EndpointException e) {
							fail("Should pass this test");
						}
					}catch(NullPointerException ex){
						System.err.println(country.getName() + "(" + country.getCode() + ") causes null pointer exception");
						throw ex;
					}
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get country from CountryEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
	}
	
	public void testInternationalParcelServiceByCountry(Country country) throws EndpointException {
		Weight		weight	= FreightWeight.builder().setValue("1").build();
		
		InternationalParcelServiceEndpoint endpoint = InternationalParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																						.setCountry(country)
																						.setFreightWeight(weight)
																						.build();
		endpoint.list(new ResponseCallback<CollectionResponse<ParcelService>>() {

			@Override
			public void onSuccess(CollectionResponse<ParcelService> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertNotEquals(0, result.getItems());
				
				Iterator<ParcelService> iterator = result.getItems().iterator();
				while(iterator.hasNext()) {
					System.out.println(iterator.next().toString());
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get the services from InternationalParcelServiceEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
		
	}
}
