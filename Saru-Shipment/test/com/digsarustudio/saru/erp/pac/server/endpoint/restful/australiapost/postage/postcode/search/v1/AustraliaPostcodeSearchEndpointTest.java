/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.digsarustudio.saru.erp.musaceae.shared.endpoint.AuthenticationDetails;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.ResponseCallback;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.DefaultPACAuthKey;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.AustralianState;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.Locality;

/**
 * To test {@link AustraliaPostcodeSearchEndpoint}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
class AustraliaPostcodeSearchEndpointTest {
	private static AuthenticationDetails	AUTH_KEY = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		AUTH_KEY = new DefaultPACAuthKey();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#searchByPostcode(java.lang.String)}.
	 * @throws EndpointException 
	 */
	@Test
	void testSearchByPostcode() throws EndpointException {
		AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																							.searchByPostcode("4119")
																							.build();
		
		endpoint.list(new ResponseCallback<CollectionResponse<Locality>>() {
			
			@Override
			public void onSuccess(CollectionResponse<Locality> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertFalse(result.getItems().isEmpty());				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get search result from AustraliaPostcodeSearchEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#searchByPostcode(java.lang.String)}.
	 * @throws EndpointException 
	 */
	@Test
	void testSearchByInvalidPostcode() throws EndpointException {
		AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																							.searchByPostcode("3441A")
																							.build();
		
		endpoint.list(new ResponseCallback<CollectionResponse<Locality>>() {
			
			@Override
			public void onSuccess(CollectionResponse<Locality> result) {
				assertNotNull(result);
				assertNull(result.getItems());
			}
			
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get search result from AustraliaPostcodeSearchEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#searchBySuburb(java.lang.String)}.
	 * @throws EndpointException 
	 */
	@Test
	void testSearchBySuburb() throws EndpointException {

		AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																							.searchBySuburb("Underwood")
																							.build();
		
		endpoint.list(new ResponseCallback<CollectionResponse<Locality>>() {
			
			@Override
			public void onSuccess(CollectionResponse<Locality> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertFalse(result.getItems().isEmpty());				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get search result from AustraliaPostcodeSearchEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
	}
	
	@Test
	void testSearchByInvalidSuburb() throws EndpointException {

		AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																							.searchBySuburb("CA")
																							.build();
		
		endpoint.list(new ResponseCallback<CollectionResponse<Locality>>() {
			
			@Override
			public void onSuccess(CollectionResponse<Locality> result) {
				assertNotNull(result);
				assertNull(result.getItems());	
			}
			
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get search result from AustraliaPostcodeSearchEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);				
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#searchByCity(java.lang.String)}.
	 * @throws EndpointException 
	 */
	@Test
	void testSearchByCity() throws EndpointException {

		AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																							.searchBySuburb("Brisbane")
																							.build();
		
		endpoint.list(new ResponseCallback<CollectionResponse<Locality>>() {
			
			@Override
			public void onSuccess(CollectionResponse<Locality> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertFalse(result.getItems().isEmpty());				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get search result from AustraliaPostcodeSearchEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
	}
	
	@Test
	void testSearchByInvalidCity() throws EndpointException {

		AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																							.searchBySuburb("Los Angles")
																							.build();
		
		endpoint.list(new ResponseCallback<CollectionResponse<Locality>>() {
			
			@Override
			public void onSuccess(CollectionResponse<Locality> result) {
				assertNotNull(result);
				assertNull(result.getItems());	
			}
			
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get search result from AustraliaPostcodeSearchEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
	}
	
	@Test
	void testNoSearchCriteria() throws EndpointException {

		AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
		
		endpoint.list(new ResponseCallback<CollectionResponse<Locality>>() {
			
			@Override
			public void onSuccess(CollectionResponse<Locality> result) {
				fail("testNoSearchCriteria() should go to onFailure()");	
			}
			
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get search result from AustraliaPostcodeSearchEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#setState(com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.State)}.
	 * @throws EndpointException 
	 */
	@Test
	void testSetState() throws EndpointException {
		AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																							.searchBySuburb("Brisbane")
																							.setState(AustralianState.Queensland)
																							.build();
		endpoint.list(new ResponseCallback<CollectionResponse<Locality>>() {
			
			@Override
			public void onSuccess(CollectionResponse<Locality> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertFalse(result.getItems().isEmpty());				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get search result from AustraliaPostcodeSearchEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#setExcludePOBox()}.
	 * @throws EndpointException 
	 */
	@Test
	void testSetExcludePOBox() throws EndpointException {

		AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																							.searchBySuburb("Brisbane")
																							.setState(AustralianState.Queensland)
																							.setExcludePOBox()
																							.build();
		
		endpoint.list(new ResponseCallback<CollectionResponse<Locality>>() {
			
			@Override
			public void onSuccess(CollectionResponse<Locality> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertFalse(result.getItems().isEmpty());				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get search result from AustraliaPostcodeSearchEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#setIncludePOBox()}.
	 * @throws EndpointException 
	 */
	@Test
	void testSetIncludePOBox() throws EndpointException {

		AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																							.searchBySuburb("Brisbane")
																							.setState(AustralianState.Queensland)
																							.setIncludePOBox()
																							.build();
		
		endpoint.list(new ResponseCallback<CollectionResponse<Locality>>() {
			
			@Override
			public void onSuccess(CollectionResponse<Locality> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertFalse(result.getItems().isEmpty());				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get search result from AustraliaPostcodeSearchEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#list(com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 * @throws EndpointException 
	 */
	@Test
	void testListResponseCallbackOfCollectionResponseOfLocality() throws EndpointException {

		AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																							.searchBySuburb("Brisbane")
																							.setState(AustralianState.Queensland)
																							.setIncludePOBox()
																							.build();
		
		endpoint.list(new ResponseCallback<CollectionResponse<Locality>>() {
			
			@Override
			public void onSuccess(CollectionResponse<Locality> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertFalse(result.getItems().isEmpty());				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get search result from AustraliaPostcodeSearchEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#insert(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.Locality, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testInsert() {
		assertThrows(UnsupportedOperationException.class, () -> {
			AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																								.searchBySuburb("Brisbane")
																								.setState(AustralianState.Queensland)
																								.setIncludePOBox()
																								.build();
			
			endpoint.insert(null, new ResponseCallback<Locality>() {
				
				@Override
				public void onSuccess(Locality result) {
					fail("not supported");			
				}
				
				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#update(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.Locality, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testUpdateLocalityResponseCallbackOfLocality() {

		assertThrows(UnsupportedOperationException.class, () -> {
			AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																								.searchBySuburb("Brisbane")
																								.setState(AustralianState.Queensland)
																								.setIncludePOBox()
																								.build();
			
			endpoint.update(null, new ResponseCallback<Locality>() {
				
				@Override
				public void onSuccess(Locality result) {
					fail("not supported");			
				}
				
				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#update(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.Locality, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)}.
	 */
	@Test
	void testUpdateLocalityResponseCallbackOfLocalityInteger() {
		assertThrows(UnsupportedOperationException.class, () -> {
			AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																								.searchBySuburb("Brisbane")
																								.setState(AustralianState.Queensland)
																								.setIncludePOBox()
																								.build();
			
			endpoint.update(null, new ResponseCallback<Locality>() {
				
				@Override
				public void onSuccess(Locality result) {
					fail("not supported");			
				}
				
				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#list(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.Locality, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testListLocalityResponseCallbackOfCollectionResponseOfLocality() {
		assertThrows(UnsupportedOperationException.class, () -> {
			AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																								.searchBySuburb("Brisbane")
																								.setState(AustralianState.Queensland)
																								.setIncludePOBox()
																								.build();
			
			endpoint.list(null, new ResponseCallback<CollectionResponse<Locality>>() {
				
				@Override
				public void onSuccess(CollectionResponse<Locality> result) {
					fail("not supported");		
				}
				
				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#list(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.Locality, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer, java.lang.Integer)}.
	 */
	@Test
	void testListLocalityResponseCallbackOfCollectionResponseOfLocalityIntegerInteger() {
		assertThrows(UnsupportedOperationException.class, () -> {
			AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																								.searchBySuburb("Brisbane")
																								.setState(AustralianState.Queensland)
																								.setIncludePOBox()
																								.build();
			
			endpoint.list(null, new ResponseCallback<CollectionResponse<Locality>>() {
				
				@Override
				public void onSuccess(CollectionResponse<Locality> result) {
					fail("not supported");			
				}
				
				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");
				}
			}, 0, 0);
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#delete(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.Locality, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testDeleteLocalityResponseCallbackOfLocality() {
		assertThrows(UnsupportedOperationException.class, () -> {
			AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																								.searchBySuburb("Brisbane")
																								.setState(AustralianState.Queensland)
																								.setIncludePOBox()
																								.build();
			
			endpoint.delete(null, new ResponseCallback<Locality>() {
				
				@Override
				public void onSuccess(Locality result) {
					fail("not supported");			
				}
				
				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#delete(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.Locality, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)}.
	 */
	@Test
	void testDeleteLocalityResponseCallbackOfLocalityInteger() {
		assertThrows(UnsupportedOperationException.class, () -> {
			AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																								.searchBySuburb("Brisbane")
																								.setState(AustralianState.Queensland)
																								.setIncludePOBox()
																								.build();
			
			endpoint.delete(null, new ResponseCallback<Locality>() {
				
				@Override
				public void onSuccess(Locality result) {
					fail("not supported");			
				}
				
				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");
				}
			}, 0);
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#get(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.Locality, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testGet() {
		assertThrows(UnsupportedOperationException.class, () -> {
			AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																								.searchBySuburb("Brisbane")
																								.setState(AustralianState.Queensland)
																								.setIncludePOBox()
																								.build();
			
			endpoint.get(null, new ResponseCallback<Locality>() {
				
				@Override
				public void onSuccess(Locality result) {
					fail("not supported");			
				}
				
				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#builder()}.
	 */
	@Test
	void testBuilder() {
		@SuppressWarnings("unused")
		AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																							.searchBySuburb("Brisbane")
																							.setState(AustralianState.Queensland)
																							.setIncludePOBox()
																							.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint#setAuthKey(com.digsarustudio.banana.endpoint.AuthenticationDetails)}.
	 */
	@Test
	void testSetAuthKey() {
		@SuppressWarnings("unused")
		AustraliaPostcodeSearchEndpoint endpoint = AustraliaPostcodeSearchEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
	}

}
