/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.digsarustudio.saru.erp.musaceae.shared.FreightSize;
import com.digsarustudio.saru.erp.musaceae.shared.Length;
import com.digsarustudio.saru.erp.musaceae.shared.Weight;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.AustralianPostcode;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.AuthenticationDetails;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.ResponseCallback;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.DefaultPACAuthKey;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.exception.UnknownError403Exception;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService;
import com.digsarustudio.saru.erp.pac.shared.javabean.FreightLength;
import com.digsarustudio.saru.erp.pac.shared.javabean.FreightWeight;

/**
 * To test {@link DomesticParcelServiceEndpoint}<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
class DomesticParcelServiceEndpointTest {
	private static AuthenticationDetails	AUTH_KEY = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		AUTH_KEY = new DefaultPACAuthKey();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint#setSenderPostcode(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode)}.
	 */
	@Test
	void testSetSenderPostcode() {
		@SuppressWarnings("unused")
		DomesticParcelServiceEndpoint endpoint = DomesticParcelServiceEndpoint.builder().setSenderPostcode(AustralianPostcode.builder().setCode("4119")
																																	  .build())
																						.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint#setReceiverPostcode(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode)}.
	 */
	@Test
	void testSetReceiverPostcode() {
		@SuppressWarnings("unused")
		DomesticParcelServiceEndpoint endpoint = DomesticParcelServiceEndpoint.builder().setReceiverPostcode(AustralianPostcode.builder().setCode("4000")
																																		.build())
																						.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint#setFreightSize(com.digsarustudio.saru.erp.musaceae.shared.FreightSize)}.
	 */
	@Test
	void testSetFreightSize() {
		Length 		width	= FreightLength.builder().setValue("20").build();
		Length 		height	= FreightLength.builder().setValue("10").build();
		Length 		length	= FreightLength.builder().setValue("30").build();
		FreightSize	size	= com.digsarustudio.saru.erp.pac.shared.javabean.FreightSize.builder().setWidth(width)
																								  .setLength(length)
																								  .setHeight(height)
																								  .build();
		@SuppressWarnings("unused")
		DomesticParcelServiceEndpoint endpoint = DomesticParcelServiceEndpoint.builder().setFreightSize(size)
																						.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint#setFreightWeight(com.digsarustudio.saru.erp.musaceae.shared.Weight)}.
	 */
	@Test
	void testSetFreightWeight() {
		Weight		weight	= FreightWeight.builder().setValue("4").build();
		
		@SuppressWarnings("unused")
		DomesticParcelServiceEndpoint endpoint = DomesticParcelServiceEndpoint.builder().setFreightWeight(weight)
																						.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint#list(com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 * @throws EndpointException 
	 */
	@Test
	void testListResponseCallbackOfCollectionResponseOfParcelService() throws EndpointException {
		Length 		width	= FreightLength.builder().setValue("20").build();
		Length 		height	= FreightLength.builder().setValue("10").build();
		Length 		length	= FreightLength.builder().setValue("30").build();
		FreightSize	size	= com.digsarustudio.saru.erp.pac.shared.javabean.FreightSize.builder().setWidth(width)
																								  .setLength(length)
																								  .setHeight(height)
																								  .build();
		Weight		weight	= FreightWeight.builder().setValue("4").build();
		
		DomesticParcelServiceEndpoint endpoint = DomesticParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																						.setSenderPostcode(AustralianPostcode.builder().setCode("4119")
																																	  .build())
																						.setReceiverPostcode(AustralianPostcode.builder().setCode("4000")
																																		.build())
																						.setFreightSize(size)
																						.setFreightWeight(weight)
																						.build();
		endpoint.list(new ResponseCallback<CollectionResponse<ParcelService>>() {

			@Override
			public void onSuccess(CollectionResponse<ParcelService> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertNotEquals(0, result.getItems());
				
				Iterator<ParcelService> iterator = result.getItems().iterator();
				while(iterator.hasNext()) {
					System.out.println(iterator.next().toString());
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get country from CountryEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
		
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint#insert(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testInsert() {
		assertThrows(UnsupportedOperationException.class, () -> {

			DomesticParcelServiceEndpoint endpoint = DomesticParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().build();
			endpoint.insert(service, new ResponseCallback<ParcelService>() {

				@Override
				public void onSuccess(ParcelService result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint#update(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testUpdateParcelServiceResponseCallbackOfParcelService() {
		assertThrows(UnsupportedOperationException.class, () -> {
			DomesticParcelServiceEndpoint endpoint = DomesticParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().build();
			endpoint.update(service, new ResponseCallback<ParcelService>() {

				@Override
				public void onSuccess(ParcelService result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint#update(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)}.
	 */
	@Test
	void testUpdateParcelServiceResponseCallbackOfParcelServiceInteger() {
		assertThrows(UnsupportedOperationException.class, () -> {

			DomesticParcelServiceEndpoint endpoint = DomesticParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().build();
			endpoint.update(service, new ResponseCallback<ParcelService>() {

				@Override
				public void onSuccess(ParcelService result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint#list(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testListParcelServiceByCondition() {
		assertThrows(UnsupportedOperationException.class, () -> {

			DomesticParcelServiceEndpoint endpoint = DomesticParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().build();
			endpoint.list(service, new ResponseCallback<CollectionResponse<ParcelService>>() {

				@Override
				public void onSuccess(CollectionResponse<ParcelService> result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint#list(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer, java.lang.Integer)}.
	 */
	@Test
	void testListParcelServiceWithCursor() {

		assertThrows(UnsupportedOperationException.class, () -> {

			DomesticParcelServiceEndpoint endpoint = DomesticParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().build();
			endpoint.list(service, new ResponseCallback<CollectionResponse<ParcelService>>() {

				@Override
				public void onSuccess(CollectionResponse<ParcelService> result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			}, 0, 0);
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint#delete(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testDeleteParcelServiceResponseCallbackOfParcelService() {

		assertThrows(UnsupportedOperationException.class, () -> {

			DomesticParcelServiceEndpoint endpoint = DomesticParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().build();
			endpoint.delete(service, new ResponseCallback<ParcelService>() {

				@Override
				public void onSuccess(ParcelService result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint#delete(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)}.
	 */
	@Test
	void testDeleteParcelServiceResponseCallbackOfParcelServiceInteger() {

		assertThrows(UnsupportedOperationException.class, () -> {

			DomesticParcelServiceEndpoint endpoint = DomesticParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().build();
			endpoint.delete(service, new ResponseCallback<ParcelService>() {

				@Override
				public void onSuccess(ParcelService result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint#get(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 */
	@Test
	void testGet() {
		assertThrows(UnsupportedOperationException.class, () -> {
			DomesticParcelServiceEndpoint endpoint = DomesticParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																							.build();
			
			ParcelService service = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.builder().build();
			endpoint.get(service, new ResponseCallback<ParcelService>() {

				@Override
				public void onSuccess(ParcelService result) {
					fail("not supported");
				}

				@Override
				public void onFailure(Throwable caught) {
					fail("not supported");				
				}
			});
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint#builder()}.
	 */
	@SuppressWarnings("unused")
	@Test
	public void testBuilder() {
		DomesticParcelServiceEndpoint endpoint = DomesticParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																						.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint#setAuthKey(com.digsarustudio.banana.endpoint.AuthenticationDetails)}.
	 */
	@Test
	void testSetAuthKey() {
		@SuppressWarnings("unused")
		DomesticParcelServiceEndpoint endpoint = DomesticParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY).build();
	}

	@Test
	public void testInvalidAuthKey() throws EndpointException {
		DomesticParcelServiceEndpoint endpoint = DomesticParcelServiceEndpoint.builder().build();
		
		endpoint.list(new ResponseCallback<CollectionResponse<ParcelService>>() {
			
			@Override
			public void onSuccess(CollectionResponse<ParcelService> result) {
				fail("This should cause an exception due to the auth-key has not been provided.");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				assertNotNull(caught);
				assertNotNull(caught.getMessage());
				assertFalse(caught.getMessage().isEmpty());
				assertTrue(caught instanceof UnknownError403Exception);
				
				System.err.println("testInvalidAuthKey(): " + caught.getMessage());
			}
		});
	}
	
	@Test
	public void testSynchronisedListMethod() throws EndpointException {
		Length 		width	= FreightLength.builder().setValue("20").build();
		Length 		height	= FreightLength.builder().setValue("10").build();
		Length 		length	= FreightLength.builder().setValue("30").build();
		FreightSize	size	= com.digsarustudio.saru.erp.pac.shared.javabean.FreightSize.builder().setWidth(width)
																								  .setLength(length)
																								  .setHeight(height)
																								  .build();
		Weight		weight	= FreightWeight.builder().setValue("4").build();
		
		DomesticParcelServiceEndpoint endpoint = DomesticParcelServiceEndpoint.builder().setAuthKey(AUTH_KEY)
																						.setSenderPostcode(AustralianPostcode.builder().setCode("4119")
																																	  .build())
																						.setReceiverPostcode(AustralianPostcode.builder().setCode("4000")
																																		.build())
																						.setFreightSize(size)
																						.setFreightWeight(weight)
																						.build();
		
		List<ParcelService> result = endpoint.list();
		
		assertNotNull(result);
		assertNotEquals(0, result.size());
		
		Iterator<ParcelService> iterator = result.iterator();
		while(iterator.hasNext()) {
			System.out.println(iterator.next().toString());
		}
	}
}
