/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.country.v1;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.saru.erp.musaceae.shared.endpoint.AuthenticationDetails;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.ResponseCallback;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.DefaultPACAuthKey;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.exception.UnknownError403Exception;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.country.v1.CountryEndpoint;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country.Country;

/**
 * The unit test for {@link CountryEndpoint}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class CountryEndpointTest {
	private static AuthenticationDetails	AUTH_KEY = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		AUTH_KEY = new DefaultPACAuthKey();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.country.v1.CountryEndpoint#list(com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 * @throws EndpointException 
	 */
	@Test
	public void testListResponseCallbackOfCollectionResponseOfCountry() throws EndpointException {

		CountryEndpoint endpoint = CountryEndpoint.builder().setAuthKey(AUTH_KEY)
															.build();

		endpoint.list(new ResponseCallback<CollectionResponse<Country>>() {
			
			@Override
			public void onSuccess(CollectionResponse<Country> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertNotEquals(0, result.getItems());
				
				Iterator<Country> iterator = result.getItems().iterator();
				while(iterator.hasNext()) {
					System.out.println(iterator.next().toString());
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get country from CountryEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.country.v1.CountryEndpoint#insert(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country.Country, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 * @throws EndpointException 
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void testInsert() throws EndpointException {
		CountryEndpoint endpoint = CountryEndpoint.builder().setAuthKey(AUTH_KEY)
															.build();
		
		endpoint.insert(null, new ResponseCallback<Country>() {
		
			@Override
			public void onSuccess(Country result) {
				fail("not supported");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				fail("not supported");
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.country.v1.CountryEndpoint#update(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country.Country, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 * @throws EndpointException 
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void testUpdateCountryResponseCallbackOfCountry() throws EndpointException {

		CountryEndpoint endpoint = CountryEndpoint.builder().setAuthKey(AUTH_KEY)
															.build();
		
		endpoint.update(null, new ResponseCallback<Country>() {
		
			@Override
			public void onSuccess(Country result) {
				fail("not supported");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				fail("not supported");
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.country.v1.CountryEndpoint#update(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country.Country, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)}.
	 * @throws EndpointException 
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void testUpdateCountryResponseCallbackOfCountryInteger() throws EndpointException {

		CountryEndpoint endpoint = CountryEndpoint.builder().setAuthKey(AUTH_KEY)
															.build();
		
		endpoint.update(null, new ResponseCallback<Country>() {
		
			@Override
			public void onSuccess(Country result) {
				fail("not supported");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				fail("not supported");
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.country.v1.CountryEndpoint#list(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country.Country, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 * @throws EndpointException 
	 */
	@Test
	public void testListCountryResponseCallbackOfCollectionResponseOfCountry() throws EndpointException {

		CountryEndpoint endpoint = CountryEndpoint.builder().setAuthKey(AUTH_KEY)
															.build();
		
		endpoint.list(null, new ResponseCallback<CollectionResponse<Country>>() {
			
			@Override
			public void onSuccess(CollectionResponse<Country> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertNotEquals(0, result.getItems());
				assertTrue( (result.getItems().size() > 1) );
			}
			
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get country from CountryEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		});
	}
	
	@Test
	public void testListCountryWithGivenCountryCode() throws EndpointException {

		CountryEndpoint endpoint = CountryEndpoint.builder().setAuthKey(AUTH_KEY)
															.build();
		
		Country target = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.country.v1.javabean.Country.builder().setCode("AU")
																														   .build();
		endpoint.list(target, new ResponseCallback<CollectionResponse<Country>>() {
			
			@Override
			public void onSuccess(CollectionResponse<Country> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertNotEquals(0, result.getItems());
				assertTrue( (result.getItems().size() == 1) );
				
				Country country = result.getItems().iterator().next();
				assertEquals("AU", country.getCode());
				assertEquals("Australia", country.getName());
			}
			
			@Override
			public void onFailure(Throwable caught) {
				fail("not supported");
			}
		});
	}	

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.country.v1.CountryEndpoint#list(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country.Country, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer, java.lang.Integer)}.
	 * @throws EndpointException 
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void testListCountryResponseCallbackOfCollectionResponseOfCountryIntegerInteger() throws EndpointException {

		CountryEndpoint endpoint = CountryEndpoint.builder().setAuthKey(AUTH_KEY)
															.build();
		
		endpoint.list(null, new ResponseCallback<CollectionResponse<Country>>() {
			
			@Override
			public void onSuccess(CollectionResponse<Country> result) {
				assertNotNull(result);
				assertNotNull(result.getItems());
				assertNotEquals(0, result.getItems());
				
				Iterator<Country> iterator = result.getItems().iterator();
				while(iterator.hasNext()) {
					System.out.println(iterator.next().toString());
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				String msg = "Failed to get country from CountryEndpoint. Error = " + caught.getMessage();
				System.err.println(msg);
				fail(msg);
			}
		}, 0, 0);
		
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.country.v1.CountryEndpoint#delete(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country.Country, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 * @throws EndpointException 
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void testDeleteCountryResponseCallbackOfCountry() throws EndpointException {

		CountryEndpoint endpoint = CountryEndpoint.builder().setAuthKey(AUTH_KEY)
															.build();
		
		endpoint.delete(null, new ResponseCallback<Country>() {
		
			@Override
			public void onSuccess(Country result) {
				fail("not supported");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				fail("not supported");
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.country.v1.CountryEndpoint#delete(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country.Country, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)}.
	 * @throws EndpointException 
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void testDeleteCountryResponseCallbackOfCountryInteger() throws EndpointException {

		CountryEndpoint endpoint = CountryEndpoint.builder().setAuthKey(AUTH_KEY)
															.build();
		
		endpoint.delete(null, new ResponseCallback<Country>() {
		
			@Override
			public void onSuccess(Country result) {
				fail("not supported");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				fail("not supported");
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.country.v1.CountryEndpoint#get(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country.Country, com.digsarustudio.banana.endpoint.ResponseCallback)}.
	 * @throws EndpointException 
	 */
	@Test
	public void testGet() throws EndpointException {
		CountryEndpoint endpoint = CountryEndpoint.builder().setAuthKey(AUTH_KEY)
															.build();
		
		Country target = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.country.v1.javabean.Country.builder().setCode("AU")
																														   .build();
		endpoint.get(target, new ResponseCallback<Country>() {
		
			@Override
			public void onSuccess(Country result) {
				assertEquals("AU", result.getCode());
				assertEquals("Australia", result.getName());
			}
			
			@Override
			public void onFailure(Throwable caught) {
				fail("not supported");
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.country.v1.CountryEndpoint#builder()}.
	 */
	@SuppressWarnings("unused")
	@Test
	public void testBuilder() {
		CountryEndpoint endpoint = CountryEndpoint.builder().setAuthKey(AUTH_KEY)
															.build();
	}

	/**
	 * Test method for {@link com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint#setAuthKey(com.digsarustudio.banana.endpoint.AuthenticationDetails)}.
	 */
	@Test
	public void testSetAuthKey() {
		@SuppressWarnings("unused")
		CountryEndpoint endpoint = CountryEndpoint.builder().setAuthKey(AUTH_KEY).build();
	}

	@Test
	public void testInvalidAuthKey() throws EndpointException {
		CountryEndpoint endpoint = CountryEndpoint.builder().build();
		
		endpoint.list(new ResponseCallback<CollectionResponse<Country>>() {
			
			@Override
			public void onSuccess(CollectionResponse<Country> result) {
				fail("This should cause an exception due to the auth-key has not been provided.");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				assertNotNull(caught);
				assertNotNull(caught.getMessage());
				assertFalse(caught.getMessage().isEmpty());
				assertTrue(caught instanceof UnknownError403Exception);
				
				System.err.println("testInvalidAuthKey(): " + caught.getMessage());
			}
		});
	}
}
