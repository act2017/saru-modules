/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint;

import com.digsarustudio.saru.erp.musaceae.shared.security.Credential;

/**
 * This interface provides a basic operation for the client code to append, update, delete, get, and list data through 
 * the API from service provider. The sub-type can add new method for a particular purpose if it is necessary.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 * 
 * @param		T The type of request object
 * @param		E The type of result object
 */
public interface Endpoint<T, E> {
	void setAPIToken(String token);

	void setCredential(Credential credential);

	Credential getCredential();
	
	String getAPIToken();
	
	/**
	 * To execute an insertion command on a particular data.<br>
	 *   
	 * @param data The data to be updated
	 * @param callback The callback indicating the result of request.
	 * 
	 * @throws EndpointException when the operation is failed due to the connection issue or 
	 * 			the restriction of the remote service. Please use {@link EndpointException#getCause()} 
	 * 			to retrieve the reason of the error.
	 * 
	 * @since 1.0.0
	 */
	void insert(T data, final ResponseCallback<E> callback) throws EndpointException;
	
	/**
	 * To execute a updating command on a particular data.<br>
	 *   
	 * @param data The data to be updated
	 * @param callback The callback indicating the result of request.
	 * 
	 * @throws EndpointException when the operation is failed due to the connection issue or 
	 * 			the restriction of the remote service. Please use {@link EndpointException#getCause()} 
	 * 			to retrieve the reason of the error. 
	 * 
	 * @since 1.0.0
	 */
	void update(T data, final ResponseCallback<E> callback) throws EndpointException;
	
	/**
	 * To execute a updating command on a particular data.<br>
	 *   
	 * @param data The data to be updated
	 * @param callback The callback indicating the result of request.
	 * @param maxRowCount The maximum row count to be delete
	 * 
	 * @throws EndpointException when the operation is failed due to the connection issue or 
	 * 			the restriction of the remote service. Please use {@link EndpointException#getCause()} 
	 * 			to retrieve the reason of the error. 
	 * 
	 * @since 1.0.0
	 */
	void update(T data, final ResponseCallback<E> callback, Integer maxRowCount) throws EndpointException;
	
	/**
	 * To execute a listing command on a particular data.<br>
	 * 
	 * @param data The data to be fetched
	 * @param callback The callback indicating the result of request.
	 * 
	 * @throws EndpointException when the operation is failed due to the connection issue or 
	 * 			the restriction of the remote service. Please use {@link EndpointException#getCause()} 
	 * 			to retrieve the reason of the error. 
	 * 
	 * @since 1.0.0
	 */
	void list(T data, final ResponseCallback<CollectionResponse<E>> callback) throws EndpointException;

	/**
	 * To execute a listing command on a particular data.<br>
	 *   
	 * @param data The data to be fetched
	 * @param callback The callback indicating the result of request.
	 * @param cursor The beginning row for the search.
	 * @param maxRowCount The maximum row count to be delete
	 * 
	 * @throws EndpointException when the operation is failed due to the connection issue or 
	 * 			the restriction of the remote service. Please use {@link EndpointException#getCause()} 
	 * 			to retrieve the reason of the error. 
	 * 
	 * @since 1.0.0
	 */
	void list(T data, final ResponseCallback<CollectionResponse<E>> callback, Integer cursor, Integer maxRowCount) throws EndpointException;
	
	/**
	 * To execute a deletion command on a particular data.<br>
	 * 
	 * @param data The data to be deleted
	 * @param callback The callback indicating the result of request.
	 * 
	 * @throws EndpointException when the operation is failed due to the connection issue or 
	 * 			the restriction of the remote service. Please use {@link EndpointException#getCause()} 
	 * 			to retrieve the reason of the error. 
	 * 
	 * @since 1.0.0
	 */
	void delete(T data, final ResponseCallback<E> callback) throws EndpointException;
	
	/**
	 * To execute a deletion command on a particular data.<br>
	 *   
	 * @param data The data to be deleted
	 * @param callback The callback indicating the result of request.
	 * @param maxRowCount The maximum row count to be delete
	 * 
	 * @throws EndpointException when the operation is failed due to the connection issue or 
	 * 			the restriction of the remote service. Please use {@link EndpointException#getCause()} 
	 * 			to retrieve the reason of the error. 
	 * 
	 * @since 1.0.0
	 */
	void delete(T data, final ResponseCallback<E> callback, Integer maxRowCount) throws EndpointException;
	
	/**
	 * To execute a fetching command on a particular data.<br>
	 *   
	 * @param data The data to be deleted
	 * @param callback The callback indicating the result of request.
	 * 
	 * @throws EndpointException when the operation is failed due to the connection issue or 
	 * 			the restriction of the remote service. Please use {@link EndpointException#getCause()} 
	 * 			to retrieve the reason of the error. 
	 * 
	 * @since 1.0.0
	 */
	void get(T data, final ResponseCallback<E> callback) throws EndpointException;
}
