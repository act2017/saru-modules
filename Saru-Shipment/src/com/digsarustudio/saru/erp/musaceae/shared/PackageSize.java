/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The size of package.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface PackageSize {
	/**
	 * 
	 * The builder for {@link PackageSize}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<PackageSize> {
		Builder setWidth(Length width);
		Builder setLength(Length length);
		Builder setHeight(Length height);
	}
	
	void setWidth(Length width);
	void setLength(Length length);
	void setHeight(Length height);
	
	Length getWidth();
	Length getLength();
	Length getHeight();
	
	PackageSize toCentimetre();
	
	Boolean validate();
}
