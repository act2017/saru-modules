/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.geoinfo;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type is a part of latitude.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Latitude {
	/**
	 * 
	 * The builder for {@link Latitude}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Latitude> {
		Builder setValue(Float value);
		Builder setValue(String value);
	}
	
	void setValue(Float value);
	void setValue(String value);
	
	String getValue();
	Float getValueFloat();
		
	Boolean validate();
}
