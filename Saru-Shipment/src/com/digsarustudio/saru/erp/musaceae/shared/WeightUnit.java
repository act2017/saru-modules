/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The unit of weight
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class WeightUnit implements IsSerializable {
	public static final WeightUnit	Gram		= new WeightUnit("g", "g");
	public static final WeightUnit	Kilogram	= new WeightUnit("kg", "kg");
	
	public static WeightUnit[] VALUES	= {
											Gram
											,Kilogram
	};		
	
	private String value = null;
	private String display = null;
	
	private WeightUnit() {
		
	}
	
	private WeightUnit(String value, String display) {
		this.value = value;
		this.display = display;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public String getDisplayString() {
		return this.display;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WeightUnit other = (WeightUnit) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	public static WeightUnit fromValue(String value) {
		WeightUnit target = null;
		
		for (WeightUnit unit : VALUES) {
			if( !value.equalsIgnoreCase(unit.getValue()) ) {
				continue;
			}
			
			target = unit;
		}
		
		return target;
	}
	
	public static WeightUnit getDefault() {
		return Kilogram;
	}
		
}
