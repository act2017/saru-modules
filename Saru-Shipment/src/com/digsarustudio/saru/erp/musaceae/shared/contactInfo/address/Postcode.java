/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The postcode interface for any address.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Postcode {
	/**
	 * 
	 * The builder for {@link Postcode}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Postcode> {
		Builder setCode(String code);
	}
	
	void setCode(String code);
	String getCode();
	
	Boolean validate();
}
