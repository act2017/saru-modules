/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.security.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.security.Credential;
import com.digsarustudio.saru.erp.musaceae.shared.time.Date;
import com.google.gwt.user.client.rpc.IsSerializable;
import com.digsarustudio.saru.erp.musaceae.shared.security.Token;


/**
 * The javabean of {@link com.digsarustudio.banana.erp.general.security.Credential}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ExpiryCredential implements Credential, IsSerializable {
	public static final String	ATTR_TOKEN				= "token";
	public static final String	ATTR_EXPIRE_DATE		= "expireDate";
	public static final String	ATTR_GENERATION_DATE	= "generationDate";
	
	/**
	 * 
	 * The object builder for {@link ExpiryCredential}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements Credential.Builder {
		private ExpiryCredential result = null;

		private Builder() {
			this.result = new ExpiryCredential();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.erp.general.security.Credential.Builder#setToken(com.digsarustudio.banana.erp.general.security.Token)
		 */
		@Override
		public Builder setToken(Token token) {
			this.result.setToken(token);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.erp.general.security.Credential.Builder#setExpireDate(com.digsarustudio.banana.erp.general.time.Date)
		 */
		@Override
		public Builder setExpireDate(Date date) {
			this.result.setExpireDate(date);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.security.Credential.Builder#setGenerationDate(com.digsarustudio.saru.erp.musaceae.shared.time.Date)
		 */
		@Override
		public Builder setGenerationDate(Date date) {
			this.result.setGenerationDate(date);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ExpiryCredential build() {
			return this.result;
		}

	}

	private Token 	token 			= null;
	private Date	expireDate		= null;
	private Date	generationDate	= null;
	
	/**
	 * 
	 */
	private ExpiryCredential() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.erp.general.security.Credential#setToken(com.digsarustudio.banana.erp.general.security.Token)
	 */
	@Override
	public void setToken(com.digsarustudio.saru.erp.musaceae.shared.security.Token token) {
		this.token = token;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.erp.general.security.Credential#getToken()
	 */
	@Override
	public Token getToken() {

		return this.token;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.erp.general.security.Credential#getExpireDate()
	 */
	@Override
	public Date getExpireDate() {

		return this.expireDate;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.erp.general.security.Credential#setExpireDate(com.digsarustudio.banana.erp.general.time.Date)
	 */
	@Override
	public void setExpireDate(Date date) {
		this.expireDate = date;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.security.Credential#setGenerationDate(com.digsarustudio.saru.erp.musaceae.shared.time.Date)
	 */
	@Override
	public void setGenerationDate(Date date) {
		this.generationDate = date;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.security.Credential#getGenerationDate()
	 */
	@Override
	public Date getGenerationDate() {
		return this.generationDate;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.erp.general.security.Credential#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.token || !this.token.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
