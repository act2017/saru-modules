/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.security;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * This sub-types represents a token which generated in any sort of form.<br>
 * Please use {@link #toString()} to retrieve the value of token in the string form.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Token {
	/**
	 * 
	 * The builder for {@link Token}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Token> {
		/**
		 * Assign a value for the token.<br>
		 * 
		 * @param value The value of the token.<br>
		 * 
		 * @return The instance of this builder
		 */
		Builder setValue(String value);
	}
	
	/**
	 * Assigns the value to this token.<br>
	 * 
	 * @param value The value for this token.
	 */
	void setValue(String value);
	
	/**
	 * Returns the value of this token.<br>
	 * 
	 * @return the value of this token.
	 */
	String getValue();
	
	Boolean validate();
}
