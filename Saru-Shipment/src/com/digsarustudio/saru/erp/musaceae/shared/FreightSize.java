/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The size of freight.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface FreightSize extends PackageSize {
	/**
	 * 
	 * The builder for {@link PackageSize}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<FreightSize> {
		Builder setWidth(Length width);
		Builder setLength(Length length);
		Builder setHeight(Length height);
	}
	
	FreightSize toCentimetre();
}
