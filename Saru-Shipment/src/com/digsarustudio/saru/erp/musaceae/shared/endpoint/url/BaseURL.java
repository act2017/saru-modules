/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint.url;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.utils.StringFormatter;

/**
 * The base class of {@link URL} which handles protocol, domain name, service path, and parameters.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public abstract class BaseURL implements URL {	
	protected ApplicationProtocolType	protocol	= null;
	protected String					domain		= null;
	protected String					path		= null;
	protected List<QueryParameter>		params		= null;
	protected List<URLParameter>		urlParams	= null;
	protected URLEncoder				encorder	= null;
	
	/**
	 * 
	 */
	public BaseURL(ApplicationProtocolType protocol) {
		this.protocol = protocol;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#getProtocol()
	 */
	@Override
	public ApplicationProtocolType getProtocol() {
		return this.protocol;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#setDomainName(java.lang.String)
	 */
	@Override
	public void setDomainName(String name) {
		this.domain = name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#getDomainName()
	 */
	@Override
	public String getDomainName() {

		return this.domain;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#getPath()
	 */
	@Override
	public String getPath() {

		return this.path;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#setPath(java.lang.String)
	 */
	@Override
	public void setPath(String path) {
		this.path = path;		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#addParameter(com.digsarustudio.musa.endpoint.RequestParameter)
	 */
	@Override
	public void addParameter(QueryParameter param) {
		if(null == this.params){
			this.params = new ArrayList<>();
		}
		
		this.params.add(param);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#setParameters(java.util.List)
	 */
	@Override
	public void setParameters(List<QueryParameter> params) {
		this.params = params;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.url.URL#addURLParameter(com.digsarustudio.banana.endpoint.url.URLParameter)
	 */
	@Override
	public void addURLParameter(URLParameter param) {
		if(null == this.urlParams) {
			this.urlParams = new ArrayList<>();
		}
		
		this.urlParams.add(param);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.url.URL#setURLParameters(java.util.List)
	 */
	@Override
	public void setURLParameters(List<URLParameter> params) {
		this.urlParams = params;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.url.URL#getURLParameters()
	 */
	@Override
	public List<URLParameter> getURLParameters() {
		return this.urlParams;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#getParameters()
	 */
	@Override
	public List<QueryParameter> getParameters() {

		return this.params;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#clearParameters()
	 */
	@Override
	public void clearParameters() {
		this.params.clear();
		this.params = null;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#getURL()
	 */
	@Override
	public String getURL() {
		return formatPathPart(this.protocol.getValue(), this.domain, this.path, this.urlParams);
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.url.URL#getEncodedURL()
	 */
	@Override
	public String getEncodedURL() {		
		return this.encorder.encodeUTF8(this.getURL(), this.params);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.url.URL#setEncoder(com.digsarustudio.banana.utils.URLEncoder)
	 */
	@Override
	public void setEncoder(URLEncoder encoder) {
		this.encorder = encoder;
	}
	
	protected URLEncoder getEncoder() {
		return this.encorder;
	}
	
	protected String getQueryStringPart(String url){
		String[] tmp = url.split(URL.QUERY_LINKER);
		
		return (null != tmp && tmp.length > 1) ? tmp[1] : null;
	}
	
	protected String getPathPart(String url){
		String[] tmp = url.split(URL.QUERY_LINKER);
		
		return (null != tmp && tmp.length > 0) ? tmp[0] : null;
	}
	
	protected String formatURL(String urlPath, String queryString){
		return StringFormatter.format("%s%s%s%s", urlPath, URL.QUERY_LINKER, queryString);
	}
	
	protected String formatPathPart(String protocol, String domain, String path, List<URLParameter> params){
		String newPath = path;
		StringBuffer buffer = new StringBuffer();
		if(null != params) {
			for (URLParameter param : params) {
				buffer.append(URL_DELIMITER);
				buffer.append(param.getValue());
			}
			
			newPath = StringFormatter.format("%s%s", path, buffer.toString());
		}
		
		return StringFormatter.format("%s://%s/%s", protocol, domain, newPath);
	}

	/**
	 * To decode the input URL and set those into the fields.
	 * 
	 * @param url The URL to be encoded
	 * 
	 * @throws URISyntaxException 
	 * 
	 */
	protected void parseURL(String url) throws URISyntaxException{
		String[] l1 = url.split(URL.QUERY_LINKER);
		
		String[] l2 = l1[0].split("://");
		if( l2.length < 2 ){
			throw new URISyntaxException("protocol is not contained in " + url);
		}
		
		this.protocol = ApplicationProtocolType.fromValue(l2[0]);
		String[] l3 = l2[1].split("/");
		if( l3.length < 2 ){
			this.domain = l2[1];
		}else{
			this.domain = l3[0];
			this.path	= l3[1];
		}
		
		if( l1.length > 1) {
			this.params = this.encorder.decodeUTF8(l1[1]);
		}
	}
	
}
