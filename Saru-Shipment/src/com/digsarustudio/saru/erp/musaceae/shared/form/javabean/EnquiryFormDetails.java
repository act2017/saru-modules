/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.form.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.InvalidDataException;
import com.digsarustudio.saru.erp.musaceae.shared.ValidationVisitor;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean form of {@link com.digsarustudio.saru.erp.musaceae.shared.form.EnquiryFormDetails}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @since		1.0.3
 * @version		1.0.4
 * <br>
 * Note:<br>
 * 				1.0.4	-> Add id for the admin to trace.<br>
 *
 */
public class EnquiryFormDetails implements com.digsarustudio.saru.erp.musaceae.shared.form.EnquiryFormDetails, IsSerializable {
	/**
	 * 
	 * The object builder for {@link EnquiryFormDetails}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.musaceae.shared.form.EnquiryFormDetails.Builder {
		private EnquiryFormDetails result = null;

		private Builder() {
			this.result = new EnquiryFormDetails();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.form.EnquiryFormDetails.Builder#setEnquirer(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName)
		 */
		@Override
		public Builder setEnquirer(PersonalName name) {
			this.result.setEnquirer(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.form.EnquiryFormDetails.Builder#setEmail(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress)
		 */
		@Override
		public Builder setEmail(EmailAddress email) {
			this.result.setEmail(email);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.form.EnquiryFormDetails.Builder#setMessage(java.lang.String)
		 */
		@Override
		public Builder setMessage(String msg) {
			this.result.setMessage(msg);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.form.EnquiryFormDetails.Builder#setId(java.lang.String)
		 */
		@Override
		public Builder setId(String id) {
			this.result.setId(id);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public EnquiryFormDetails build() {
			return this.result;
		}

	}

	private String			id			= null;
	private PersonalName	enquirer	= null;
	private EmailAddress	email		= null;
	private	String			message		= null;
	
	/**
	 * 
	 */
	public EnquiryFormDetails() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.form.EnquiryFormDetails#setId(java.lang.String)
	 */
	@Override
	public void setId(String id) {
		this.id = id;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.form.EnquiryFormDetails#getId()
	 */
	@Override
	public String getId() {
		return this.id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.form.EnquiryFormDetails#setEnquirer(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName)
	 */
	@Override
	public void setEnquirer(PersonalName name) {
		this.enquirer = name;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.form.EnquiryFormDetails#setEmail(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress)
	 */
	@Override
	public void setEmail(EmailAddress email) {
		this.email = email;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.form.EnquiryFormDetails#setMessage(java.lang.String)
	 */
	@Override
	public void setMessage(String msg) {
		this.message = msg;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.form.EnquiryFormDetails#getEnquirer()
	 */
	@Override
	public PersonalName getEnquirer() {
		
		return this.enquirer;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.form.EnquiryFormDetails#getEmail()
	 */
	@Override
	public EmailAddress getEmail() {
		
		return this.email;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.form.EnquiryFormDetails#getMessage()
	 */
	@Override
	public String getMessage() {
		
		return this.message;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Validatable#accept(com.digsarustudio.saru.erp.musaceae.shared.ValidationVisitor)
	 */
	@Override
	public void accept(ValidationVisitor<com.digsarustudio.saru.erp.musaceae.shared.form.EnquiryFormDetails> visitor) throws InvalidDataException {
		if(null == visitor) {
			return;
		}
		
		visitor.validate(this);
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
