/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.australia;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The state in Australia.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AustralianState implements State, IsSerializable {
	public final static AustralianState AustralianCapitalTerritory	= new AustralianState("ACT", "Australian Capital Territory");
	public final static AustralianState NewSouhtWales				= new AustralianState("NSW", "New South Wales");
	public final static AustralianState NorthernTerritory			= new AustralianState("NT", "Northern Territory");
	public final static AustralianState Tasmania					= new AustralianState("TAS", "Tasmania");
	public final static AustralianState Queensland					= new AustralianState("QLD", "Queensland");
	public final static AustralianState SouthAustralia				= new AustralianState("SA", "South Australia");
	public final static AustralianState	Victoria					= new AustralianState("VIC", "Victoria");
	public final static AustralianState	WesternAustralia			= new AustralianState("WA", "Western Australia");
	
	private final static Integer		MAX_CODE_LEN				= 3;
	
	public static AustralianState[] AUSTRALIA_STATES	= {
			 AustralianCapitalTerritory
			,NewSouhtWales
			,NorthernTerritory
			,Tasmania
			,Queensland
			,SouthAustralia
			,Victoria
			,WesternAustralia
	};

	private String name	= null;
	private String code	= null;
	
	/**
	 * 
	 */
	private AustralianState() {
		
	}
	
	private AustralianState(String code, String name) {
		this.code = code;
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.State#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.State#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.code = code;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.State#getName()
	 */
	@Override
	public String getName() {

		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.State#getCode()
	 */
	@Override
	public String getCode() {

		return this.code;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.State#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.code || this.code.isEmpty()) {
			return false;
		}
		
		if( this.code.length() > MAX_CODE_LEN ) {
			return false;
		}
		
		return true;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AustralianState other = (AustralianState) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

	public static AustralianState fromCode(String code) {
		AustralianState target = null;
		
		for (AustralianState state : AUSTRALIA_STATES) {
			if( !code.equalsIgnoreCase(state.getCode()) ) {
				continue;
			}
			
			target = state;
		}
		
		return target;
	}

	public static AustralianState fromName(String name) {
		AustralianState target = null;
		
		for (AustralianState state : AUSTRALIA_STATES) {
			if( !name.equalsIgnoreCase(state.getName()) ) {
				continue;
			}
			
			target = state;
		}
		
		return target;
	}
	
	public static AustralianState from(State state) {
		if(null == state) {
			return null;
		}
		
		String target = state.getCode();		
		AustralianState rtn = AustralianState.fromCode(target);
		if(null == rtn) {
			target = state.getName();
			rtn = AustralianState.fromName(target);
		}
		
		return rtn;
	}
}
