/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-types represent a HTTP header.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface HttpHeader{
	/**
	 * 
	 * The builder for {@link HttpHeader}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static interface Builder extends ObjectBuilder<HttpHeader> {		
		Builder setFields(List<HttpHeaderField> fields);
		Builder addField(HttpHeaderField field);
	}
	
	void setFields(List<HttpHeaderField> fields);
	void addField(HttpHeaderField field);
	
	HttpHeaderField getField(String name);
	List<HttpHeaderField> getFields();
	
	void setField(String name, HttpHeaderField field);
	void deleteField(String name);
	
	void clear();
}
