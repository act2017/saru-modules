/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type represents an identity of an entity.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface Identity {
	/**
	 * 
	 * The builder for {@link Identity}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Identity> {
		Builder setValue(String value);
	}
	
	void setValue(String value);
	String getValue();
	
	Boolean validate();
}
