/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint;

/**
 * This exception indicates an error occurred when the client tried to send a request to the remote service 
 * via an {@link Endpoint}.<br>
 * Please handle this exception if the interface of the endpoint did not work properly.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
@SuppressWarnings("serial")
public class EndpointException extends Exception {

	/**
	 * 
	 */
	public EndpointException() {

	}

	/**
	 * @param message
	 */
	public EndpointException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public EndpointException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public EndpointException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public EndpointException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
