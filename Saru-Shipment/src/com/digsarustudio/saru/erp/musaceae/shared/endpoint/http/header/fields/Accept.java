/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.fields;

import com.digsarustudio.banana.utils.StringFormatter;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.BaseHttpHeaderField;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.HttpHeaderField;

/**
 * <a href="https://en.wikipedia.org/wiki/Media_type">Media type(s)</a> that is(/are) acceptable for the response. 
 * See <a href="https://en.wikipedia.org/wiki/Content_negotiation">Content negotiation</a>.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public class Accept extends BaseHttpHeaderField implements HttpHeaderField {

	/**
	 * To indicates using a json string as the content to send to the remote service.
	 */
	public final static Accept	JSON		= new Accept("application", "json");
	
	private static final String HEADER = "Accept";
	
	private String type				= null;
	private String mediaTypeName	= null;
	
	private Accept(String type, String mediaTypeName) {
		super(HEADER);
		
		this.type = type;
		this.mediaTypeName = mediaTypeName;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.header.HTTPHeaderField#getValue()
	 */
	@Override
	public String getValue() {
		return StringFormatter.format("%s/%s", this.type, this.mediaTypeName);
	}

}
