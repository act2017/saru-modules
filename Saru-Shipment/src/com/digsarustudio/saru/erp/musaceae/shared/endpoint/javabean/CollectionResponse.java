/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint.javabean;

import java.util.ArrayList;
import java.util.Collection;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link CollectionResponse}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class CollectionResponse<T> implements com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse<T>
											, IsSerializable{
	/**
	 * 
	 * The object builder for {@link CollectionResponse<E>}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder<E> implements com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse.Builder<E> {
		private CollectionResponse<E> result = null;

		private Builder() {
			this.result = new CollectionResponse<E>();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse.Builder#setNextPageToken(java.lang.String)
		 */
		@Override
		public Builder<E> setNextPageToken(String token) {
			this.result.setNextPageToken(token);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse.Builder#setItems(java.util.Collection)
		 */
		@Override
		public Builder<E> setItems(Collection<E> items) {
			this.result.setItems(items);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse.Builder#addItem(java.lang.Object)
		 */
		@Override
		public Builder<E> addItem(E item) {
			this.result.addItem(item);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public CollectionResponse<E> build() {
			return this.result;
		}

	}
	
	private String nextPageToken = null;
	private Collection<T> items = null;
	
	private CollectionResponse() {

	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse#getNextPageToken()
	 */
	@Override
	public String getNextPageToken() {
		return this.nextPageToken;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse#getItems()
	 */
	@Override
	public Collection<T> getItems() {
		return this.items;
	}

	/*
	 * To create a new builder
	 */
	public static <T> Builder<T> builder() {
		return new Builder<T>();
	}
	
	protected void setNextPageToken(String token) {
		this.nextPageToken = token;
	}
	
	protected void addItem(T item) {
		if(null == this.items) {
			this.items = new ArrayList<>();
		}
		
		this.items.add(item);
	}
	
	protected void setItems(Collection<T> items) {
		this.items = items;
	}
}
