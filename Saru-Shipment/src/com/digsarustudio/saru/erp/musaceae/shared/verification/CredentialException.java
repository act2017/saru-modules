/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.verification;

/**
 * This exception indicates the credential is workable for the remote service.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.4
 *
 */
@SuppressWarnings("serial")
public class CredentialException extends Exception {

	/**
	 * 
	 */
	public CredentialException() {
		
	}

	/**
	 * @param message
	 */
	public CredentialException(String message) {
		super(message);
		
	}

	/**
	 * @param cause
	 */
	public CredentialException(Throwable cause) {
		super(cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 */
	public CredentialException(String message, Throwable cause) {
		super(message, cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public CredentialException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

}
