/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.fields;

import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.BaseHttpHeaderField;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.HttpHeaderField;

/**
 * This class represents the overriding of HTTP method used in HTTP header
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public class HttpMethodOverride extends BaseHttpHeaderField implements HttpHeaderField {
	/**
	 * To ask remote service to do a fetching by the request
	 */
	public final static HttpMethodOverride	GET		= new HttpMethodOverride("GET");
	
	/**
	 * To ask remote service to do a insertion by the request
	 */
	public final static HttpMethodOverride	POST	= new HttpMethodOverride("POST");
	
	/**
	 * To ask remote service to do a updating by the request
	 */
	public final static HttpMethodOverride	PUT		= new HttpMethodOverride("PUT");
	
	/**
	 * To ask remote service to do a deletion by the request 
	 */
	public final static HttpMethodOverride	DELETE	= new HttpMethodOverride("DELETE");
	
	
	private final static String HEADER = "X-HTTP-METHOD-OVERRIDE";
	
	private String method = null;
	
	/**
	 * @param header
	 */
	private HttpMethodOverride(String method) {
		super(HEADER);

		this.method = method;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.http.headers.HTTPHeader#getValue()
	 */
	@Override
	public String getValue() {
		return this.method;
	}

}
