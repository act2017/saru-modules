/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.verification;

/**
 * This exception indicates the operation from user is unauthenticated or the user is not allow to do this operation.<br> 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.4
 *
 */
@SuppressWarnings("serial")
public class UnauthenticatedOperationException extends CredentialException {

	/**
	 * 
	 */
	public UnauthenticatedOperationException() {

	}

	/**
	 * @param message
	 */
	public UnauthenticatedOperationException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public UnauthenticatedOperationException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnauthenticatedOperationException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public UnauthenticatedOperationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
