/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint.restful;

/**
 * This exception represents that the Remote RESTful connection has been cancelled.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class RemoteConnectionCancelledException extends Exception {

	/**
	 * 
	 */
	public RemoteConnectionCancelledException() {

	}

	/**
	 * @param message
	 */
	public RemoteConnectionCancelledException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public RemoteConnectionCancelledException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public RemoteConnectionCancelledException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public RemoteConnectionCancelledException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
