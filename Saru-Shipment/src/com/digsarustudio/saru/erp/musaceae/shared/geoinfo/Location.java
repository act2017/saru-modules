/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.geoinfo;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type represents the location object.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Location {
	/**
	 * 
	 * The builder for {@link Location}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Location> {
		Builder setName(String name); 
	}
	
	void setName(String name);
	String getName();
	
	Boolean validate();
}
