/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.time;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type represents the calendar date.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Date {
	public static final String DASH_DELIMITER		= "-";
	public static final String BACKSLASH_DELIMITER	= "/";
	
	/**
	 * 
	 * The builder for {@link Date}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Date> {
		Builder setValue(String value);
		Builder setYear(String year);
		Builder setMonth(String month);
		Builder setDay(String day);
	}
	
	void setValue(String value);
	
	void setYear(String year);
	void setMonth(String month);
	void setDay(String day);
	
	String getValue();
	
	String getYear();
	String getMonth();
	String getDay();
	
	String toMMYYYY();
	Timestamp toTimestamp();
	
	Boolean validate();
}
