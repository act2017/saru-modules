/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo;

/**
 * Thrown when the input email address are in an invalid format
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class EMailAddressFormatException extends Exception {

	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = -5222155032302590077L;

	/**
	 * 
	 */
	public EMailAddressFormatException() {

	}

	/**
	 * @param message
	 */
	public EMailAddressFormatException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public EMailAddressFormatException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public EMailAddressFormatException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public EMailAddressFormatException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
