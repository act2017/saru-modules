/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.form.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.ValidationVisitor;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName;
import com.digsarustudio.saru.erp.musaceae.shared.form.EnquiryFormDetails;

/**
 * This validator validates {@link EnquiryFormDetails}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class EnquiryFormDetailsValidator implements ValidationVisitor<EnquiryFormDetails> {

	/**
	 * 
	 */
	public EnquiryFormDetailsValidator() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.ValidationVisitor#validate(java.lang.Object)
	 */
	@Override
	public void validate(EnquiryFormDetails data) throws IllegalArgumentException {
		if( null == data ) {
			throw new IllegalArgumentException("Invalid enquiry form details");
		}

		this.validateEnquirer(data.getEnquirer());
		this.validateEmail(data.getEmail());
		this.validateMessage(data.getMessage());
	}

	protected void validateEnquirer(PersonalName name) {
		if( null == name || null == name.getFullName() || name.getFullName().isEmpty() ) {
			throw new IllegalArgumentException("The enquirer must be set to send the enquiry form.");
		}
	}

	protected void validateEmail(EmailAddress email) {
		if( null == email || !email.validate() ) {
			throw new IllegalArgumentException("The email must be set to send the enquiry form.");
		}
	}

	protected void validateMessage(String message) {
		if( null == message || message.isEmpty() ) {
			throw new IllegalArgumentException("The message must be set for enquiry form.");
		}
	}
}
