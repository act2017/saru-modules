/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.time.candidate;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-types represents as a date for some particular circumstance.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Date {
	public static final String	FORMAT_YEAR 		= "yyyy";
	public static final String	FORMAT_MONTH		= "mm";
	public static final String	FORMAT_DAY			= "dd";
	public static final String	FORMAT_DELIMITER	= "-";
	
	/**
	 * 
	 * The builder for {@link Date}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Date> {
		Builder setYear(String year);
		Builder setMonth(String month);
		Builder setDay(String day);
		Builder setFormat(String format);
		Builder setDate(String date);
	}
	
	void setYear(String year);
	void setMonth(String month);
	void setDay(String day);
	void setFormat(String format);
	void setDate(String date);
	
	String getYear();
	String getMonth();
	String getDay();
	String getDate();
	
	String getDatabaseDateTime();
	
	Boolean validate();
	String toString();
}
