/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.fields;

import com.digsarustudio.banana.utils.StringFormatter;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.BaseHttpHeaderField;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.HttpHeaderField;

/**
 * Authentication credentials for <a href="https://en.wikipedia.org/wiki/Basic_access_authentication">HTTP authentication</a>.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public final class Authorization extends BaseHttpHeaderField implements HttpHeaderField{
	/**
	 * 
	 * The object builder for {@link HttpHeaderField}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static class Builder implements HttpHeaderField.Builder {
		private Authorization result = null;

		private Builder() {
			this.result = new Authorization();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.endpoint.http.header.HTTPHeaderField.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public HttpHeaderField build() {
			return this.result;
		}

	}
	
	private static final String HEADER = "Authorization";
	
	private String encodedCredential	= null;
	
	/**
	 * 
	 */
	private Authorization() {
		super(HEADER);
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.http.HTTPHeader#getValue()
	 */
	@Override
	public String getValue() {
		return StringFormatter.format("Basic %s", this.encodedCredential);
	}

	protected void setValue(String value) {
		this.encodedCredential = value;
	}
}
