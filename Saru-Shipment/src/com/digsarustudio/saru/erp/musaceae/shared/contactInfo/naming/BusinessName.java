/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming;


/**
 * The sub-type represents a name for the business. For example, Google Pty. Ltd.<br>
 * This also provides the abbreviation name of it. 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface BusinessName extends Identity {
	/**
	 * 
	 * The builder for {@link Business}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static interface Builder extends Identity.Builder{
		Builder setName(String name);
		Builder setAbbreviation(String name);
	}
	
	
	void setAbbreviation(String name);
	void setName(String name);
	
	String getName();
	String getAbbreviation();
	
	Boolean validate();
}
