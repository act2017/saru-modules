/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Address;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Country;

/**
 * The base object of {@link Address}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public abstract class BaseAddress implements Address {
	protected Country	country	= null;
	
	/**
	 * The address for the other country or incoming with a non-parsable format.<br>
	 */
	protected String	address	= null;

	/**
	 * 
	 */
	public BaseAddress() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Address#setCountry(com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.Country)
	 */
	@Override
	public void setCountry(Country country) {
		this.country = country;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Address#getCountry()
	 */
	@Override
	public Country getCountry() {
		
		return this.country;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Address#setAddress(java.lang.String)
	 */
	@Override
	public void setAddress(String address) {
		this.address = address;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Address#getAddress()
	 */
	@Override
	public String getAddress() {
		return this.address;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Address#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.country || null == this.country.getCode() || this.country.getCode().isEmpty()) {
			return false;
		}
		
		if(null != this.address && this.address.isEmpty()) {
			return false;
		}
		
		return true;
	}

}
