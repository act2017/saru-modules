/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The length which contains the unit and the quantity.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Length {
	/**
	 * 
	 * The builder for {@link Length}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Length> {
		Builder setValue(String value);
		Builder setUnit(LengthUnit unit);
	}
	
	void setValue(String value);
	void setUnit(LengthUnit unit);
	
	String getValue();
	Integer getIntValue();
	LengthUnit getUnit();
	
	Boolean validate();
}
