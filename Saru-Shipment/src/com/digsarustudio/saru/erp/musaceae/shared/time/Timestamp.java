/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.time;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The timestamp of the date in second.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Timestamp {
	/**
	 * 
	 * The builder for {@link Timestamp}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Timestamp> {
		Builder setValue(String value);
		Builder setValue(Long value);
	}
	
	void setValue(String value);
	void setValue(Long value);
	
	Long getValue();
	
	Boolean validate();
}
