/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint.url;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type represents a Uniform Resource Locator, such as HTTP URL, HTTPS URL, FTP URL and so on.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface URL{
	/**
	 * Used to link parameter and parameter
	 */
	static final String PARAMETER_LINKER = "&";
	
	/**
	 * Used to link URL and parameters
	 */
	static final String QUERY_LINKER = "?";
	
	static final String	URL_DELIMITER	= "/";
	
	/**
	 * 
	 * The builder for {@link URL}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static interface Builder extends ObjectBuilder<URL> {
		/**
		 * Assigns a domain name to this URL
		 * 
		 * @param name A domain name for this URL
		 * 
		 * @return The instance of this builder
		 */
		Builder setDomainName(String name);
		
		/**
		 * Assigns a path of service under the domain
		 * 
		 * @param path A path of service under the domain
		 * 
		 * @return The instance of this builder
		 */
		Builder setPath(String path);
		
		/**
		 * Appends a request parameter to this URL
		 * 
		 * @param param A request parameter to this URL
		 * 
		 * @return The instance of this builder
		 */
		Builder addParameter(QueryParameter param);
		
		/**
		 * Assigns a list of request parameters to this URL
		 * 
		 * @param params A list of request parameters to this URL
		 * 
		 * @return The instance of this builder
		 */
		Builder setParameters(List<QueryParameter> params);
		
		/**
		 * Appends a request parameter attached to this URL
		 * 
		 * @param param A request parameter attached to this URL
		 */
		Builder addURLParameter(URLParameter param);
		
		/**
		 * Assigns a list of request parameters attached to this URL
		 * 
		 * @param params A list of request parameters attached to this URL
		 */
		Builder setURLParameters(List<URLParameter> params);
		
		/**
		 * Assigns an encoded URL to parse.
		 * 
		 * @param url An encoded URL to parse
		 * 
		 * @return The instance of this builder
		 * 
		 * @throws URISyntaxException if the format of incoming URL is not valid. 
		 */
		Builder setEncodedURL(String url) throws URISyntaxException;
		
		/**
		 * Assigns a plain URL to parse
		 * 
		 * @param url A plain URL to parse
		 * 
		 * @return The instance of this builder
		 * 
		 * @throws URISyntaxException if the format of incoming URL is not valid.
		 */
		Builder setPlainURL(String url) throws URISyntaxException;
		
		/**
		 * Assigns a URL encoder for the URL object to encode outgoing URL or decode incoming URL.
		 * 
		 * @param encoder The {@link URLEncoder}.
		 * 
		 * @return The instance of this builder
		 */
		Builder setEncoder(URLEncoder encoder);
	}
	
	/**
	 * Returns the application protocol this URL uses.
	 * 
	 * @return the application protocol this URL uses.
	 */
	ApplicationProtocolType getProtocol();
	
	/**
	 * Assigns a domain name to this URL
	 * 
	 * @param name A domain name for this URL
	 */
	void setDomainName(String name);
	
	/**
	 * Returns the domain name of this URL
	 * 
	 * @return The domain name of this URL
	 */
	String getDomainName();
	
	/**
	 * Assigns a path of service under the domain
	 * 
	 * @param path A path of service under the domain
	 */
	void setPath(String path);
	
	/**
	 * Returns the service path of this URL
	 * 
	 * @return The service path of this URL
	 */
	String getPath();
	
	/**
	 * Appends a request parameter to this URL
	 * 
	 * @param param A request parameter to this URL
	 */
	void addParameter(QueryParameter param);
	
	/**
	 * Assigns a list of request parameters to this URL
	 * 
	 * @param params A list of request parameters to this URL
	 */
	void setParameters(List<QueryParameter> params);
	
	/**
	 * Appends a request parameter attached to this URL
	 * 
	 * @param param A request parameter attached to this URL
	 */
	void addURLParameter(URLParameter param);
	
	/**
	 * Assigns a list of request parameters attached to this URL
	 * 
	 * @param params A list of request parameters attached to this URL
	 */
	void setURLParameters(List<URLParameter> params);
	/**
	 * Returns a list of parameters of this URL, null if there is no {@link QueryParameter} for this {@link URL}
	 * 
	 * @return a list of parameters of this URL, null if there is no {@link QueryParameter} for this {@link URL}
	 */
	List<URLParameter> getURLParameters();
	
	/**
	 * Assigns a URL encoder for the URL object to encode outgoing URL or decode incoming URL.
	 * 
	 * @param encoder The {@link URLEncoder}.
	 * 
	 * @return The instance of this builder
	 */
	void setEncoder(URLEncoder encoder);
	
	/**
	 * Returns a list of parameters of this URL, null if there is no {@link QueryParameter} for this {@link URL}
	 * 
	 * @return a list of parameters of this URL, null if there is no {@link QueryParameter} for this {@link URL}
	 */
	List<QueryParameter> getParameters();
	
	/**
	 * To clean stored parameters. Normally, it is called when the the request was sent.<br>
	 */
	void clearParameters();	
	
	/**
	 * Returns the URL in plain text
	 * 
	 * @return the URL in plain text
	 */
	String getURL();
	
	/**
	 * Returns the URL encoded, which replaced space as '%20' and so on.
	 * 
	 * @return the URL encoded, which replaced space as '%20' and so on.
	 */
	String getEncodedURL();
	
	URL copy();
}
