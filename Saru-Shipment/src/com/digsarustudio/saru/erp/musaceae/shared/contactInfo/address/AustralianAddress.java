/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address;


/**
 * The sub-type represents the address used in Australia.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface AustralianAddress extends Address {
	/**
	 * 
	 * The builder for {@link AustralianAddress}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends Address.Builder{
		Builder setAddressLine(AddressLines line);
		Builder setSuburb(Suburb suburb);
		Builder setState(State state);
		Builder setPostcode(Postcode postcode);
	}
	
	void setAddressLine(AddressLines line);
	void setSuburb(Suburb suburb);
	void setState(State state);
	void setPostcode(Postcode postcode);
	
	AddressLines getAddressLine();
	Suburb getSuburb();
	State getState();
	Postcode getPostcode();
	
	Boolean validate();
}
