/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The postcode used in Australia.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AustralianPostcode implements Postcode
										 , IsSerializable{
	private static final String REGEXP_POSTCODE	= "[0-9]{4}";
	
	/**
	 * 
	 * The object builder for {@link AustralianPostcode}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements Postcode.Builder {
		private AustralianPostcode result = null;

		private Builder() {
			this.result = new AustralianPostcode();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode.Builder#setCode(java.lang.String)
		 */
		@Override
		public Builder setCode(String code) {
			this.result.setCode(code);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AustralianPostcode build() {
			return this.result;
		}

	}	
	
	private String code	= null;
	
	/**
	 * 
	 */
	private AustralianPostcode() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.code = code;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode#getCode()
	 */
	@Override
	public String getCode() {

		return this.code;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.code || this.code.isEmpty()) {
			return false;
		}
		
		if( !this.code.matches(REGEXP_POSTCODE) ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
