/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type represents an address in a country.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Address {
	/**
	 * 
	 * The builder for {@link Address}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Address> {	
		Builder setAddress(String address);
		Builder setAddressLines(AddressLines lines);
		Builder setSuburb(Suburb suburb);
		Builder setState(State state);
		Builder setPostcode(Postcode postcode);
		Builder setCountry(Country country);
	}
	
	void setAddress(String address);
	void setAddressLines(AddressLines lines);
	void setSuburb(Suburb suburb);
	void setState(State state);
	void setPostcode(Postcode postcode);
	void setCountry(Country country);
		
	AddressLines	getAddressLines();
	Suburb			getSuburb();
	State 			getState();
	Postcode 		getPostcode();
	Country 		getCountry();
	String			getAddress();
		
	Boolean validate();
}
