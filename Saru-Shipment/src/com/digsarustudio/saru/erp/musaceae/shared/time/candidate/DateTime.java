/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.time.candidate;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Date and time.<br>
 * For example:<br>
 * 	2014-08-27T15:48:09+10:00
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface DateTime {
	public static final String DELIMITER_DATE_AND_TIME	= "T";
	public static final String DELIMITER_DATABASE_DATE_AND_TIME	= " ";
	
	/**
	 * 
	 * The builder for {@link DateTime}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<DateTime> {
		Builder setDate(Date date);
		Builder setTime(Time time);
		Builder setDateTime(String dateTime);
	}
	
	void setDate(Date date);
	void setTime(Time time);
	void setDateTime(String dateTime);
	
	Date getDate();
	Time getTime();
	
	String getDateTime();
	String getDatabaseDateTime();
	
	Boolean validate();
}
