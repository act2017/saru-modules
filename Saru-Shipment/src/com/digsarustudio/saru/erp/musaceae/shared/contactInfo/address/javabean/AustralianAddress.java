/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Country;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb;
import com.google.gwt.user.client.rpc.IsSerializable;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines;

/**
 * The javabean for {@link com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AustralianAddress}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AustralianAddress extends BaseAddress 
							   implements com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AustralianAddress
							   			, IsSerializable{
	public static final String ATTR_ADDRESS_LINES	= "lines";
	public static final String ATTR_SUBURB			= "suburb";
	public static final String ATTR_STATE			= "state";
	public static final String ATTR_COUNTRY			= "country";
	public static final String ATTR_POSTCODE		= "postcode";
	
	/**
	 * 
	 * The object builder for {@link AustralianAddress}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AustralianAddress.Builder{
		private AustralianAddress result = null;

		private Builder() {
			this.result = new AustralianAddress();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AustralianAddress.Builder#setAddressLine(java.lang.String)
		 */
		@Override
		public Builder setAddressLine(AddressLines line) {
			this.result.setAddressLine(line);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AustralianAddress.Builder#setSuburb(com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.Suburb)
		 */
		@Override
		public Builder setSuburb(Suburb suburb) {
			this.result.setSuburb(suburb);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AustralianAddress.Builder#setState(com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.State)
		 */
		@Override
		public Builder setState(State state) {
			this.result.setState(state);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AustralianAddress.Builder#setPostcode(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode)
		 */
		@Override
		public Builder setPostcode(Postcode postcode) {
			this.result.setPostcode(postcode);
			return this;
		}

		public Builder setCountry(Country country) {
			this.result.setCountry(country);
			return this;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Address.Builder#setAddressLines(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines)
		 */
		@Override
		public Builder setAddressLines(AddressLines lines) {
			this.result.setAddressLines(lines);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Address.Builder#setAddress(java.lang.String)
		 */
		@Override
		public Builder setAddress(String address) {
			this.result.setAddress(address);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AustralianAddress build() {
			return this.result;
		}

	}

	private AddressLines	lines		= null;
	private Suburb			suburb		= null;
	private State			state		= null;
	private Country			country		= null;
	private Postcode		postcode	= null;
	
	/**
	 * 
	 */
	public AustralianAddress() {
		this.setCountry(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.Country.builder().setName("Australia")
																												 .setCode("AU")
																												 .build());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Address#setCountry(com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.Country)
	 */
	@Override
	public void setCountry(Country country) {
		this.country = country;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Address#getCountry()
	 */
	@Override
	public Country getCountry() {

		return this.country;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AustralianAddress#setAddressLine(java.lang.String)
	 */
	@Override
	public void setAddressLine(AddressLines line) {
		this.lines = line;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AustralianAddress#setSuburb(com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.Suburb)
	 */
	@Override
	public void setSuburb(Suburb suburb) {
		this.suburb = suburb;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AustralianAddress#setState(com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.State)
	 */
	@Override
	public void setState(State state) {
		this.state = state;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AustralianAddress#setPostcode(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode)
	 */
	@Override
	public void setPostcode(Postcode postcode) {
		this.postcode = postcode;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AustralianAddress#getAddressLine()
	 */
	@Override
	public AddressLines getAddressLine() {

		return this.lines;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AustralianAddress#getSuburb()
	 */
	@Override
	public Suburb getSuburb() {

		return this.suburb;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AustralianAddress#getState()
	 */
	@Override
	public State getState() {

		return this.state;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AustralianAddress#getPostcode()
	 */
	@Override
	public Postcode getPostcode() {

		return this.postcode;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Address#setAddressLines(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines)
	 */
	@Override
	public void setAddressLines(AddressLines lines) {
		this.lines = lines;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Address#getAddressLines()
	 */
	@Override
	public AddressLines getAddressLines() {
		return this.lines;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.BaseAddress#getAddress()
	 */
	@Override
	public String getAddress() {
		String address = super.getAddress();
		
		if(null != address) {
			return address;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		if(null != this.lines && this.lines.validate()) {
			buffer.append(this.lines.getCombinedLines());
		}
		
		if(null != this.suburb && this.suburb.validate()) {
			buffer.append(", ");		
			buffer.append(this.suburb.getName());
		}
		
		if(null != this.state && this.state.validate()) {
			buffer.append(", ");
			buffer.append(this.state.getCode());
		}
		
		if(null != this.postcode && this.postcode.validate()) {
			buffer.append(", ");
			buffer.append(this.postcode.getCode());
		}
		
		if(null != this.country && this.country.validate()) {
			buffer.append(", ");
			buffer.append(this.country.getCode());
		}
		
		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AustralianAddress#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.lines || !this.lines.validate()) {
			return false;
		}
		
		if(null == this.suburb || !this.suburb.validate()) {
			return false;
		}
		
		if(null == this.state || !this.state.validate()) {
			return false;
		}
		
		if(null == this.country || !this.country.validate()) {
			return false;
		}
		
		if(null == this.postcode || !this.postcode.validate()) {
			return false;
		}

		return true;
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
