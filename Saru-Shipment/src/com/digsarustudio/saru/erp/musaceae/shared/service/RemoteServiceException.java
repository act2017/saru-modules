/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.service;

/**
 * This exception indicates the remote service cannot deal with the request well. 
 * All the exception thrown by remote service should be inherited this exception.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
@SuppressWarnings("serial")
public class RemoteServiceException extends Exception {
	/**
	 * 
	 */
	public RemoteServiceException() {
		
	}

	/**
	 * @param message
	 */
	public RemoteServiceException(String message) {
		super(message);
		
	}

	/**
	 * @param cause
	 */
	public RemoteServiceException(Throwable cause) {
		super(cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 */
	public RemoteServiceException(String message, Throwable cause) {
		super(message, cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public RemoteServiceException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

}
