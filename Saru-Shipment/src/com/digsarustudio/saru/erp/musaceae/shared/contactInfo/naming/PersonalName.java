/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming;


/**
 * The sub-type represents the name of a person including given name, middle name, and surname.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface PersonalName extends Identity {
	/**
	 * 
	 * The builder for {@link PersonalName}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static interface Builder extends Identity.Builder	{
		Builder setSurname(String name);
		Builder setMiddleName(String name);
		Builder setGivenName(String name);
		Builder setFullName(String name);
	}
	
	void setSurname(String name);
	void setMiddleName(String name);
	void setGivenName(String name);
	void setFullName(String name);
	
	String getSurname();
	String getMiddleName();
	String getGivenName();
	String getFullName();

	Boolean validate();
}
