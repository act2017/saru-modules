/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.time.candidate.javabean;

import com.digsarustudio.banana.utils.StringFormatter;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class Date implements com.digsarustudio.saru.erp.musaceae.shared.time.candidate.Date
						   , IsSerializable {
	@SuppressWarnings("unused")
	private static final String REG_YYYY_MM_DD	= "\\d{4}-\\d{2}-\\d{2}";
	private static final String FORMAT			= "%s-%s-%s";
	private static final String	DELIMITER		= "\\-";
	
	/**
	 * 
	 * The object builder for {@link Date}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.musaceae.shared.time.candidate.Date.Builder {
		private Date result = null;

		private Builder() {
			this.result = new Date();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date.Builder#setYear(java.lang.String)
		 */
		@Override
		public Builder setYear(String year) {
			this.result.setYear(year);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date.Builder#setMonth(java.lang.String)
		 */
		@Override
		public Builder setMonth(String month) {
			this.result.setMonth(month);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date.Builder#setDay(java.lang.String)
		 */
		@Override
		public Builder setDay(String day) {
			this.result.setDay(day);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date.Builder#setFormat(java.lang.String)
		 */
		@Override
		public Builder setFormat(String format) {
			this.result.setFormat(format);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date.Builder#setDate(java.lang.String)
		 */
		@Override
		public Builder setDate(String date) {
			this.result.setDate(date);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Date build() {
			return this.result;
		}

	}

	private String	year	= null;
	private String	month	= null;
	private String	day		= null;
	private String	format	= null;
	
	/**
	 * 
	 */
	private Date() {
		this.setFormat(FORMAT);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date#setYear(java.lang.String)
	 */
	@Override
	public void setYear(String year) {
		this.year = year;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date#setMonth(java.lang.String)
	 */
	@Override
	public void setMonth(String month) {
		this.month = month;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date#setDay(java.lang.String)
	 */
	@Override
	public void setDay(String day) {
		this.day = day;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date#setFormat(java.lang.String)
	 */
	@Override
	public void setFormat(String format) {
		this.format = format;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date#setDate(java.lang.String)
	 */
	@Override
	public void setDate(String date) {
		String[] items = date.split(DELIMITER);
		
		if( 3 != items.length) {
			throw new IllegalArgumentException("Invalid date format. The format should be matched yyyy-mm-dd. i.e. 2018-03-05");
		}

		this.year 	= items[0];
		this.month	= items[1];
		this.day	= items[2];
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date#getYear()
	 */
	@Override
	public String getYear() {

		return this.year;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date#getMonth()
	 */
	@Override
	public String getMonth() {

		return this.month;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date#getDay()
	 */
	@Override
	public String getDay() {

		return this.day;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date#getDate()
	 */
	@Override
	public String getDate() {
		
		return StringFormatter.format(this.format, this.year, this.month, this.day);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date#getDatabaseDateTime()
	 */
	@Override
	public String getDatabaseDateTime() {
		return StringFormatter.format(this.format, this.year, this.month, this.day);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.year || this.year.isEmpty()) {
			return false;
		}
		
		if(null == this.month || this.month.isEmpty()) {
			return false;
		}
		
		if(null == this.day || this.day.isEmpty()) {
			return false;
		}		

		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		
		return this.getDate();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
