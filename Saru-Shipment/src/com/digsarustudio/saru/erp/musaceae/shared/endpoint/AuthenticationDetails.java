/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type contains the key of the remote service API for authentication.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface AuthenticationDetails {
	/**
	 * 
	 * The builder for {@link AuthenticationDetails}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<AuthenticationDetails> {
		Builder setToken(String token);
	}
		
	void setToken(String token);
	String getToken();
	
	Boolean validate();
}
