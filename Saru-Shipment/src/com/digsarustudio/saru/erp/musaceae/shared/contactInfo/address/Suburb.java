/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type contains the name and code of a particular suburb.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Suburb {
	/**
	 * 
	 * The builder for {@link Suburb}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Suburb> {
		Builder setName(String name);
		Builder setCode(String code);
	}
	
	void setName(String name);
	void setCode(String code);
	
	String getName();
	String getCode();
	
	Boolean validate();
}
