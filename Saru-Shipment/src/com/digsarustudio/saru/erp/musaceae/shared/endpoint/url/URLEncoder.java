/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint.url;

import java.util.List;

/**
 * The sub-type can encode or decode a particular URL.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface URLEncoder {
	String encodeUTF8(String url);
	String encodeUTF8(String url, List<QueryParameter> queryParameters);
	
	List<QueryParameter> decodeUTF8(String queryParameterString);
}
