/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared;

/**
 * The sub-type handles the validation of a particular data.<br>
 * Please implements this interface to let the client code has the ability to check the consistency of the data.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.3
 * @since		1.0.0
 *
 * Note:
 * 				1.0.3	-> Replace {@link IllegalArgumentException} by {@link InvalidDataException} to enforce the 
 * 						   client code to handle the exception.<br>
 *
 */
public interface ValidationVisitor<T> {
	void validate(T data) throws InvalidDataException;
}
