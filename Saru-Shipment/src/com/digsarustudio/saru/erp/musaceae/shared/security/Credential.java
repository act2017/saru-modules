/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.security;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.shared.time.Date;
import com.digsarustudio.saru.erp.musaceae.shared.security.Token;

/**
 * The sub-type represents the credential for different circumstance.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Credential {
	/**
	 * 
	 * The builder for {@link Credential}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Credential> {
		Builder setToken(Token token);
		Builder setExpireDate(Date date);
		Builder setGenerationDate(Date date);
	}
	
	void setToken(Token token);
	Token getToken();
	
	Date getExpireDate();
	void setExpireDate(Date date);
	
	void setGenerationDate(Date date);
	Date getGenerationDate();
	
	Boolean validate();
}
