/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of the {@link com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class Suburb implements com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb
							,  IsSerializable{
	public static final String ATTR_NAME	= "name";
	public static final String ATTR_CODE	= "code";	
	
	/**
	 * 
	 * The object builder for {@link Suburb}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb.Builder {
		private Suburb result = null;

		private Builder() {
			this.result = new Suburb();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.Suburb.Builder#setName(java.lang.String)
		 */
		@Override
		public Builder setName(String name) {
			this.result.setName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.Suburb.Builder#setCode(java.lang.String)
		 */
		@Override
		public Builder setCode(String code) {
			this.result.setCode(code);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Suburb build() {
			return this.result;
		}

	}

	private String code = null;
	private String name	= null;
	
	/**
	 * 
	 */
	private Suburb() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.Suburb#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.Suburb#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.code = code;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.Suburb#getName()
	 */
	@Override
	public String getName() {
		
		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.Suburb#getCode()
	 */
	@Override
	public String getCode() {
		
		return this.code;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.Suburb#validate()
	 */
	@Override
	public Boolean validate() {
		if( !this.hasCode() && !this.hasName() ) {
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
//		return "Suburb [code=" + code + ", name=" + name + "]";
		buffer.append("{\n");
		buffer.append("\t\"code\": " + "\"" + this.getCode() + "\"");
		buffer.append("\t\"name\": " + "\"" + this.getName() + "\"");
		buffer.append("}");
		
		return buffer.toString();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	protected Boolean hasCode() {
		return (null != this.code && !this.code.isEmpty());
	}
	
	protected Boolean hasName() {
		return (null != this.name && !this.name.isEmpty());
	}
}
