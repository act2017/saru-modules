/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

/**
 * The base class of {@link HttpHeader}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public abstract class BaseHttpHeader implements HttpHeader {
	private Map<String, HttpHeaderField> fields = null;
	
	public BaseHttpHeader() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.header.HTTPHeader#setFields(java.util.List)
	 */
	@Override
	public void setFields(List<HttpHeaderField> fields) {
		this.fields = new HashMap<>();
		
		for (HttpHeaderField field : fields) {
			this.fields.put(field.getHeader(), field);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.header.HTTPHeader#addField(com.digsarustudio.banana.endpoint.http.header.HTTPHeaderField)
	 */
	@Override
	public void addField(HttpHeaderField field) {
		if(null == this.fields) {
			this.fields = new HashMap<>();
		}
		
		this.fields.put(field.getHeader(), field);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.header.HTTPHeader#getField(java.lang.String)
	 */
	@Override
	public HttpHeaderField getField(String name) {
		if(!this.fields.containsKey(name)) {
			return null;
		}
		
		return this.fields.get(name);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.header.HTTPHeader#getFields()
	 */
	@Override
	public List<HttpHeaderField> getFields() {
		if(null == this.fields) {
			return null;
		}
		
		List<HttpHeaderField> rtns = new ArrayList<>();
		
		Iterator<HttpHeaderField> iterator = this.fields.values().iterator();
		while(iterator.hasNext()) {
			rtns.add(iterator.next());
		}
		
		return rtns;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.header.HTTPHeader#setField(java.lang.String, com.digsarustudio.banana.endpoint.http.header.HTTPHeaderField)
	 */
	@Override
	public void setField(String name, HttpHeaderField field) {
		this.fields.put(name, field);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.header.HTTPHeader#deleteField(java.lang.String)
	 */
	@Override
	public void deleteField(String name) {
		if(!this.fields.containsKey(name)) {
			return;
		}
		
		this.fields.remove(name);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.header.HTTPHeader#clear()
	 */
	@Override
	public void clear() {
		this.fields.clear();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if(null == this.fields || this.fields.isEmpty()) {
			return "";
		}
		
		Integer count = 0;
		StringBuffer buffer = new StringBuffer();		
		Set<Entry<String, HttpHeaderField>> entries= this.fields.entrySet();
		for (Entry<String, HttpHeaderField> entry : entries) {
			if( 0 < count ++) {
				buffer.append("\n");
			}
			
			buffer.append(entry.toString());			
		}
		
		return buffer.toString();
	}
	
}
