/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header;

/**
 * The base class of {@link HttpHeaderField}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public abstract class BaseHttpHeaderField implements HttpHeaderField {

	private String header	= null;
	private String value	= null;
	
	/**
	 * 
	 */
	public BaseHttpHeaderField(String header) {
		this.header = header;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.http.HTTPHeader#getHeader()
	 */
	@Override
	public String getHeader() {
		return this.header;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.header.HttpHeaderField#getValue()
	 */
	@Override
	public String getValue() {
		return this.value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		
		buffer.append(this.getHeader());
		buffer.append(HttpHeaderField.DELIMITER);
		buffer.append(" ");
		buffer.append(this.getValue());
		
		return buffer.toString();
	}
	
	protected void setValue(String value) {
		this.value = value;
	}
}
