/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class EmailAddress implements com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress
								   , IsSerializable {
	public static final String	ATTR_ACCOUNT	= "account";
	public static final String	ATTR_DOMAIN		= "domain";
	
	/**
	 * 
	 * The object builder for {@link EmailAddress}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress.Builder {
		private EmailAddress result = null;

		private Builder() {
			this.result = new EmailAddress();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress.Builder#setAccountName(java.lang.String)
		 */
		@Override
		public Builder setAccountName(String name) {
			this.result.setAccountName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress.Builder#setDomainName(java.lang.String)
		 */
		@Override
		public Builder setDomainName(String name) {
			this.result.setDomainName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress.Builder#setAddress(java.lang.String)
		 */
		@Override
		public Builder setAddress(String address) {
			this.result.setAddress(address);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public EmailAddress build() {
			return this.result;
		}

	}

	private	String account	= null;
	private String domain	= null;
	
	/**
	 * 
	 */
	private EmailAddress() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress#setAccountName(java.lang.String)
	 */
	@Override
	public void setAccountName(String name) {
		this.account = name;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress#setDomainName(java.lang.String)
	 */
	@Override
	public void setDomainName(String name) {
		this.domain = name;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress#setAddress(java.lang.String)
	 */
	@Override
	public void setAddress(String address) {
		if(null == address) {
			return;
		}
		
		String[] format = address.split(ACCOUNT_DELIMITER);

		if( format.length > 0 ) {
			this.account = format[0];
		}
		
		if( format.length > 1 ) {
			this.domain = format[1];
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress#validate()
	 */
	@Override
	public Boolean validate() {
		if( null == this.account || this.account.isEmpty() || !this.account.matches(ACCOUNT_REGEX) ) {
			return false;
		}
		
		if( null == this.domain || this.domain.isEmpty() || !this.domain.matches(DOMAIN_REGEX) ) {
			return false;
		}

		return true;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress#getAccountName()
	 */
	@Override
	public String getAccountName() {

		return this.account;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress#getDomainName()
	 */
	@Override
	public String getDomainName() {

		return this.domain;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress#getAddress()
	 */
	@Override
	public String getAddress() {
		if( !this.validate() ) {
			return null;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append(this.account);
		buffer.append(ACCOUNT_DELIMITER);
		buffer.append(this.domain);
		
		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.getAddress();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
