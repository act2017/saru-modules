/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.Weight;
import com.digsarustudio.saru.erp.musaceae.shared.WeightUnit;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The base weight object for {@link Weight}.<br>
 * No static builder method for sub-type to implement it.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class BaseWeight implements Weight, IsSerializable {
	/**
	 * 
	 * The object builder for {@link BaseWeight}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements Weight.Builder {
		private BaseWeight result = null;

		public Builder() {
			this.result = new BaseWeight();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.Weight.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.Weight.Builder#setUnit(com.digsarustudio.saru.erp.musaceae.shared.WeightUnit)
		 */
		@Override
		public Builder setUnit(WeightUnit unit) {
			this.result.setUnit(unit);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public BaseWeight build() {
			return this.result;
		}

	}

	private String		value	= null;
	private WeightUnit	unit	= null;
	
	/**
	 * 
	 */
	protected BaseWeight() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Weight#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.value = value;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Weight#setUnit(com.digsarustudio.saru.erp.musaceae.shared.WeightUnit)
	 */
	@Override
	public void setUnit(WeightUnit unit) {
		this.unit = unit;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Weight#getValue()
	 */
	@Override
	public String getValue() {
		
		return this.value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Weight#getIntValue()
	 */
	@Override
	public Integer getIntValue() {
		
		return new Integer(this.value);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Weight#getUnit()
	 */
	@Override
	public WeightUnit getUnit() {
		
		return this.unit;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Weight#toKilograms()
	 */
	@Override
	public Weight toKilograms() {
		
		return (new BaseWeight.Builder()).setValue(this.getValue())
										 .setUnit(WeightUnit.Kilogram)
										 .build();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Weight#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.value || this.value.isEmpty()) {
			return false;
		}
		
		if(null == this.unit) {
			return false;
		}
		
		return true;
	}	
}
