/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The weight of an item.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Weight {
	/**
	 * 
	 * The builder for {@link Weight}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Weight> {
		Builder setValue(String value);
		Builder setUnit(WeightUnit unit);
	}
	
	void setValue(String value);
	void setUnit(WeightUnit unit);
	
	String getValue();
	Integer getIntValue();
	WeightUnit getUnit();
	
	Weight toKilograms();
	
	Boolean validate();
}
