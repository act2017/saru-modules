/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The java data object form of {@link com.digsarustudio.saru.erp.musaceae.shared.UnitPrice}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class UnitPrice implements com.digsarustudio.saru.erp.musaceae.shared.UnitPrice, IsSerializable {	
	public static final String	ATTR_PRICE			= "price";
	public static final String	ATTR_TAX			= "tax";
	public static final String	ATTR_INCLUDING_TAX	= "isIncludingTax";
	
	@SuppressWarnings("unused")
	private static final String REGEXP_PRICE		= "^(?!0\\.00)[1-9]\\d*(\\.\\d)*?$";
	
	/**
	 * 
	 * The object builder for {@link UnitPrice}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.musaceae.shared.UnitPrice.Builder {
		private UnitPrice result = null;

		public Builder() {
			this.result = new UnitPrice();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.banana.seed.shared.UnitPrice.Builder#setIncludingTax()
		 */
		@Override
		public Builder setIncludingTax() {
			this.result.setIncludingTax();
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.banana.seed.shared.UnitPrice.Builder#setExcludingTax()
		 */
		@Override
		public Builder setExcludingTax() {
			this.result.setExcludingTax();
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.banana.seed.shared.UnitPrice.Builder#setPrice(java.lang.String)
		 */
		@Override
		public Builder setPrice(String price) {
			this.result.setPrice(price);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.banana.seed.shared.UnitPrice.Builder#stTax(java.lang.String)
		 */
		@Override
		public Builder setTax(String tax) {
			this.result.setTax(tax);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public UnitPrice build() {
			return this.result;
		}

	}
	
	private String	price			= null;
	private String	tax				= null;
	private Boolean isIncludingTax	= false;
	
	/**
	 * 
	 */
	private UnitPrice() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.banana.seed.shared.UnitPrice#setIncludingTax()
	 */
	@Override
	public void setIncludingTax() {
		this.isIncludingTax = true;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.banana.seed.shared.UnitPrice#setExcludingTax()
	 */
	@Override
	public void setExcludingTax() {
		this.isIncludingTax = false;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.banana.seed.shared.UnitPrice#setPrice(java.lang.String)
	 */
	@Override
	public void setPrice(String price) {
		this.price = price;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.banana.seed.shared.UnitPrice#isIncludingTax()
	 */
	@Override
	public Boolean isIncludingTax() {

		return this.isIncludingTax;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.banana.seed.shared.UnitPrice#getPrice()
	 */
	@Override
	public String getPrice() {

		return this.price;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.banana.seed.shared.UnitPrice#setTax(java.lang.String)
	 */
	@Override
	public void setTax(String tax) {
		this.tax = tax;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.banana.seed.shared.UnitPrice#getTax()
	 */
	@Override
	public String getTax() {
		return this.tax;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.UnitPrice#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.price || this.price.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "UnitPrice [price=" + price + ", tax=" + tax + ", isIncludingTax=" + isIncludingTax + "]";
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
