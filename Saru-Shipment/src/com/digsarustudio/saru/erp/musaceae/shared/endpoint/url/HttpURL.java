/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint.url;

import java.util.List;

/**
 * This class represents a url using HTTP protocol to communicate with remote server.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public class HttpURL extends BaseURL implements URL {
	private static final ApplicationProtocolType PROTOCOL = ApplicationProtocolType.HTTP; 

	/**
	 * 
	 * The object builder for {@link HttpURL}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static class Builder implements URL.Builder {
		private HttpURL result = null;

		public Builder() {
			this.result = new HttpURL();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.url.URL.Builder#setDomainName(java.lang.String)
		 */
		@Override
		public Builder setDomainName(String name) {
			this.result.setDomainName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.url.URL.Builder#setPath(java.lang.String)
		 */
		@Override
		public Builder setPath(String path) {
			this.result.setPath(path);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.url.URL.Builder#addParameter(com.digsarustudio.musa.endpoint.RequestParameter)
		 */
		@Override
		public Builder addParameter(QueryParameter param) {
			this.result.addParameter(param);			
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.url.URL.Builder#setParameters(java.util.List)
		 */
		@Override
		public Builder setParameters(List<QueryParameter> params) {
			this.result.setParameters(params);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.endpoint.url.URL.Builder#addURLParameter(com.digsarustudio.banana.endpoint.url.URLParameter)
		 */
		@Override
		public Builder addURLParameter(URLParameter param) {
			this.result.addURLParameter(param);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.endpoint.url.URL.Builder#setURLParameters(java.util.List)
		 */
		@Override
		public Builder setURLParameters(List<URLParameter> params) {
			this.result.setURLParameters(params);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.url.URL.Builder#setEncodedURL(java.lang.String)
		 */
		@Override
		public Builder setEncodedURL(String url) throws URISyntaxException {
			this.result.parseURL(url);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.url.URL.Builder#setPlainURL(java.lang.String)
		 */
		@Override
		public Builder setPlainURL(String url) throws URISyntaxException {
			this.result.parseURL(url);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.endpoint.url.URL.Builder#setEncoder(com.digsarustudio.banana.endpoint.url.URLEncoder)
		 */
		@Override
		public Builder setEncoder(URLEncoder encoder) {
			this.result.setEncoder(encoder);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public URL build() {

			return this.result;
		}

	}
	
	/**
	 * 
	 */
	private HttpURL() {
		super(PROTOCOL);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.url.URL#copy()
	 */
	@Override
	public URL copy() {
		return HttpURL.builder().setDomainName(this.getDomainName())
								.setPath(this.getPath())
								.setParameters(this.getParameters())
								.setURLParameters(this.getURLParameters())
								.setEncoder(this.getEncoder())
								.build();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
