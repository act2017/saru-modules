/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint;

import com.digsarustudio.saru.erp.musaceae.client.endpoint.EndpointAsyncCallback;

/**
 * This interface used to tell client code the result of execution by {@link #onSuccess(Object)} 
 * when the target service has finished the request and returned a meaningful result, otherwise 
 * {@link #onFailure(Throwable)} called with exception which indicates what happened.<br>
 * Normally, this interface is used for RESTful connection, but it also can be used for RPC connection.<br>
 * <br>
 * If {@link EndpointAsyncCallback} works well for the both of connections, this interface can be deprecated.<br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface ResponseCallback<T> {
	void onSuccess(T result);
	void onFailure(Throwable caught);
}
