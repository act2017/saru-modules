/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The general phone number of {@link com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PhoneNumber implements com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber
								  , IsSerializable {

	/**
	 * 
	 * The object builder for {@link PhoneNumber}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber.Builder {
		private PhoneNumber result = null;

		private Builder() {
			this.result = new PhoneNumber();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber.Builder#setNumber(java.lang.String)
		 */
		@Override
		public Builder setNumber(String number) {
			this.result.setNumber(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public PhoneNumber build() {
			return this.result;
		}

	}
	
	private String number = null;
	
	/**
	 * 
	 */
	private PhoneNumber() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber#setNumber(java.lang.String)
	 */
	@Override
	public void setNumber(String number) {
		this.number = number;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber#getNumber()
	 */
	@Override
	public String getNumber() {

		return this.number;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.number || this.number.isEmpty()) {
			return false;
		}

		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
