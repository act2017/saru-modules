/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.form;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.shared.Validatable;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName;

/**
 * The sub-type contains the details of an enquiry form, such as name, email, and the message.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @since		1.0.3
 * @version		1.0.4
 * <br>
 * Note:<br>
 * 				1.0.4	-> Add id for the admin to trace.<br>
 *
 */
public interface EnquiryFormDetails extends Validatable<EnquiryFormDetails>{
	/**
	 * 
	 * The builder for {@link EnquiryFormDetails}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static interface Builder extends ObjectBuilder<EnquiryFormDetails> {
		Builder setId(String id);
		Builder setEnquirer(PersonalName name);
		Builder setEmail(EmailAddress email);
		Builder setMessage(String msg);
	}
	
	void setId(String id);
	void setEnquirer(PersonalName name);
	void setEmail(EmailAddress email);
	void setMessage(String msg);
	
	String			getId();
	PersonalName	getEnquirer();
	EmailAddress	getEmail();
	String			getMessage();
	
}
