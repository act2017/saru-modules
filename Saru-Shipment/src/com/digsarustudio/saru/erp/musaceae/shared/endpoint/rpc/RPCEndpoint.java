/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint.rpc;

import com.digsarustudio.saru.erp.musaceae.shared.endpoint.Endpoint;
import com.digsarustudio.saru.erp.musaceae.shared.security.Credential;
import com.digsarustudio.saru.erp.musaceae.shared.security.javabean.ExpiryCredential;
import com.digsarustudio.saru.erp.musaceae.shared.security.javabean.Token;

/**
 * The RPC endpoint which supports {@link Credential} data type.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public abstract class RPCEndpoint<T, E> implements Endpoint<T, E> {
	private Credential credential = null;
	
	/**
	 * 
	 */
	public RPCEndpoint() {
		
	}

	public void setAPIToken(String token) {
		this.setCredential(ExpiryCredential.builder().setToken(Token.builder().setValue(token)
																			  .build())
													.build());
		
	}

	public void setCredential(Credential credential) {
		this.credential = credential;
	}

	public Credential getCredential() {
		return this.credential;
	}
	
	public String getAPIToken() {
		if(null == this.credential) {
			return null;
		}
			
		return this.credential.getToken().getValue();
	}
}
