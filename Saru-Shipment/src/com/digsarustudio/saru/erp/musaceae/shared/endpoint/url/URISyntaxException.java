/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint.url;

import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;

/**
 * This exception thrown when the incoming URL is not in a valid format.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class URISyntaxException extends EndpointException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8037869970822936161L;

	/**
	 * 
	 */
	public URISyntaxException() {

	}

	/**
	 * @param arg0
	 */
	public URISyntaxException(String arg0) {
		super(arg0);

	}

	/**
	 * @param arg0
	 */
	public URISyntaxException(Throwable arg0) {
		super(arg0);

	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public URISyntaxException(String arg0, Throwable arg1) {
		super(arg0, arg1);

	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 */
	public URISyntaxException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);

	}

}
