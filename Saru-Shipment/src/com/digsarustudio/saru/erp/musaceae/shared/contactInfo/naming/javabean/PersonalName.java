/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @since		1.0.0
 * @version		1.0.5
 * <br>
 * 	Note:<br>
 * 				1.0.5	->	Bug Fixing: Crash when dispatch order.<br>
 * 
 */
public class PersonalName implements com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName
								   , IsSerializable {
	
	/**
	 * 
	 * The object builder for {@link PersonalName}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName.Builder {
		private PersonalName result = null;

		private Builder() {
			this.result = new PersonalName();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName.Builder#setSurname(java.lang.String)
		 */
		@Override
		public Builder setSurname(String name) {
			this.result.setSurname(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName.Builder#setMiddleName(java.lang.String)
		 */
		@Override
		public Builder setMiddleName(String name) {
			this.result.setMiddleName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName.Builder#setGivenName(java.lang.String)
		 */
		@Override
		public Builder setGivenName(String name) {
			this.result.setGivenName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName.Builder#setFullName(java.lang.String)
		 */
		@Override
		public Builder setFullName(String name) {
			this.result.setFullName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.Identity.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public PersonalName build() {
			return this.result;
		}

	}
	
	private String givenName	= null;
	private String middleName	= null;
	private String surname		= null;

	/**
	 * 
	 */
	protected PersonalName() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName#setSurname(java.lang.String)
	 */
	@Override
	public void setSurname(String name) {
		this.surname = name;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName#setMiddleName(java.lang.String)
	 */
	@Override
	public void setMiddleName(String name) {
		this.middleName = name;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName#setGivenName(java.lang.String)
	 */
	@Override
	public void setGivenName(String name) {
		this.givenName = name;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName#setFullName(java.lang.String)
	 */
	@Override
	public void setFullName(String name) {
		if(null == name || name.isEmpty()) {
			return;
		}
		
		String[] names = name.split(" ");
		
		this.givenName = names[0];
		
		if(name.length() == 2) {
			this.surname = names[1];
		}else if(name.length() >= 3){		
			/**
			 * Bug Fixing: Crash when dispatch order.<br>
			 * 
			 * @since 1.0.5
			 */
			this.surname = names[names.length-1];
			
			StringBuffer buffer = new StringBuffer();
			for (int i = 1; i < names.length - 1; i++) {
				buffer.append(names[i]);
			}
			
			this.middleName = buffer.toString();
		}else {
			//Do nothing
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName#getFullName()
	 */
	@Override
	public String getFullName() {
		if( (null == this.givenName || this.givenName.isEmpty())
		 && (null == this.surname || this.surname.isEmpty()) ) {
			return null;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		if(null != this.getGivenName() && !this.getGivenName().isEmpty()) {
			buffer.append(this.getGivenName());
			buffer.append(" ");
		}
		
		if(null != this.middleName) {
			buffer.append(this.middleName);
			buffer.append(" ");
		}
		
		if(null != this.getSurname() && !this.getSurname().isEmpty()) {
			buffer.append(this.surname);
		}

		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName#getSurname()
	 */
	@Override
	public String getSurname() {
		
		return this.surname;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName#getMiddleName()
	 */
	@Override
	public String getMiddleName() {
		
		return this.middleName;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName#getGivenName()
	 */
	@Override
	public String getGivenName() {
		
		return this.givenName;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.Identity#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.setFullName(value);
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.Identity#getValue()
	 */
	@Override
	public String getValue() {
		return this.getFullName();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.givenName || this.givenName.isEmpty()) {
			return false;
		}
		
		if(null == this.surname || this.surname.isEmpty()) {
			return false;
		}
		
		return true;
	}

	public static PersonalName copy(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName source) {
		if(null == source) {
			return null;
		}
		
		return PersonalName.builder().setGivenName(source.getGivenName())
									 .setMiddleName(source.getMiddleName())
									 .setSurname(source.getSurname())
									 .build();
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
