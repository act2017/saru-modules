/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean;

/**
 * The simple enumeration of {@link Country}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.2
 * @since		1.0.0
 *
 * Note:
 * 				1.0.2	-> Add value search.
 */
public class Countries {
	public static final Country	Australia	= Country.builder().setCode("AU").setName("Australia").build();

	/**
	 * 
	 */
	private Countries() {
	}


	private static final Country[] VALUES = { Australia };

	public Country[] values() {
		return VALUES;
	}

	public static Country fromValue(String value) {
		if (null == value) {
			return null;
		}

		for (Country type : VALUES) {
			if (!type.getCode().equalsIgnoreCase(value)) {
				continue;
			}

			return type;
		}

		return null;
	}
}
