/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.google.gson.GsonBuilder;

/**
 * The sub-type represents a price which is including tax or excluding tax.<br>
 * If the sub-type has to be used for Gson to convert between JSON string, please use 
 * {@link GsonBuilder#registerTypeAdapter(java.lang.reflect.Type, Object)} to register 
 * {@link UnitPriceDeserializer} for the customized Gson object.<br> 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface UnitPrice {
	/**
	 * 
	 * The builder for {@link UnitPrice}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<UnitPrice> {
		Builder setIncludingTax();
		Builder setExcludingTax();
		Builder setPrice(String price);
		Builder setTax(String tax);
	}
	
	void setIncludingTax();
	void setExcludingTax();
	void setPrice(String price);
	void setTax(String tax);
	
	Boolean isIncludingTax();
	String getPrice();
	String getTax();
	
	Boolean validate();
}
