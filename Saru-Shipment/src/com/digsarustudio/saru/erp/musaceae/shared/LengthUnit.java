/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared;

import java.util.HashMap;
import java.util.Map;

/**
 * The unit of the {@link Length}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LengthUnit {
	public static final LengthUnit CentiMetre	= new LengthUnit("cm", "cm");
	public static final LengthUnit Metre		= new LengthUnit("m", "m");
	public static final LengthUnit Kilometre	= new LengthUnit("km", "km");

	private static Map<String, LengthUnit> VALUES = new HashMap<>();
		
	static {
		VALUES.put(CentiMetre.getValue(), CentiMetre);
		VALUES.put(Metre.getValue(), Metre);
		VALUES.put(Kilometre.getValue(), Kilometre);
	}	
	
	private String value = null;
	private String display = null;
	
	private LengthUnit() {
		
	}
	
	private LengthUnit(String value, String display) {
		this.value = value;
		this.display = display;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public String getDisplayString() {
		return this.display;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LengthUnit other = (LengthUnit) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	public static LengthUnit fromValue(String value) {
		if (null == value || !VALUES.containsKey(value)) {
			return null;
		}
		
		return VALUES.get(value);
	}
	
	public static LengthUnit getDefault() {
		return CentiMetre;
	}
}
