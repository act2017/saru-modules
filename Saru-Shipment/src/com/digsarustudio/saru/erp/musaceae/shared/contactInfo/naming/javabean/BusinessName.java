/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class BusinessName implements com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName
								   , IsSerializable {
	
	/**
	 * 
	 * The object builder for {@link BusinessName}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName.Builder {
		private BusinessName result = null;

		private Builder() {
			this.result = new BusinessName();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName.Builder#setName(java.lang.String)
		 */
		@Override
		public Builder setName(String name) {
			this.result.setName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName.Builder#setAbbreviation(java.lang.String)
		 */
		@Override
		public Builder setAbbreviation(String name) {
			this.result.setAbbreviation(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.Identity.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public BusinessName build() {
			return this.result;
		}

	}
	
	private String name			= null;
	private String abbreviation	= null;

	/**
	 * 
	 */
	protected BusinessName() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName#setAbbreviation(java.lang.String)
	 */
	@Override
	public void setAbbreviation(String name) {
		this.abbreviation = name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName#getName()
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName#getAbbreviation()
	 */
	@Override
	public String getAbbreviation() {
		return this.abbreviation;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.Identity#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.setName(value);
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.Identity#getValue()
	 */
	@Override
	public String getValue() {
		return this.getName();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.name || this.name.isEmpty()) {
			return false;
		}
		
		if(null != this.abbreviation && this.abbreviation.isEmpty()) {
			return false;
		}
		
		return true;
	}

	public static BusinessName copy(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName source) {
		if(null == source) {
			return null;
		}
		
		return BusinessName.builder().setName(source.getName())
									 .setAbbreviation(source.getAbbreviation())
									 .build();
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
