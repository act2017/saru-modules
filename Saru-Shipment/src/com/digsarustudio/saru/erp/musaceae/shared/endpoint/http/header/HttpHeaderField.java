/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type of this interface represents a HTTP header object which contains the name of header and 
 * its own value.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface HttpHeaderField {
	public static final String DELIMITER = ":";
	
	/**
	 * 
	 * The builder for {@link HttpHeaderField}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static interface Builder extends ObjectBuilder<HttpHeaderField> {
		Builder setValue(String value);
	}
		
	/**
	 * Returns the name of this header
	 * 
	 * @return the name of this header
	 */
	String getHeader();
	
	/**
	 * Returns the value of this header
	 * 
	 * @return the value of this header
	 */
	String getValue();
}
