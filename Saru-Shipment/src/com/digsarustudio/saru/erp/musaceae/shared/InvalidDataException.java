/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared;

/**
 * This exception indicates the data validated by {@link ValidationVisitor} is not valid.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
@SuppressWarnings("serial")
public class InvalidDataException extends Exception {

	/**
	 * 
	 */
	public InvalidDataException() {
		
	}

	/**
	 * @param message
	 */
	public InvalidDataException(String message) {
		super(message);
		
	}

	/**
	 * @param cause
	 */
	public InvalidDataException(Throwable cause) {
		super(cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 */
	public InvalidDataException(String message, Throwable cause) {
		super(message, cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public InvalidDataException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

}
