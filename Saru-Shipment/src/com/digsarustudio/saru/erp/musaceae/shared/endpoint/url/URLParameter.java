/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint.url;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * This class represents the parameter of a URL.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public class URLParameter {
	public static final String DELIMITER	= "/";
	
	/**
	 * 
	 * The object builder for {@link URLParameter}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static class Builder implements ObjectBuilder<URLParameter> {
		private URLParameter result = null;

		public Builder() {
			this.result = new URLParameter();
		}

		/**
		 * @param value
		 * @see com.digsarustudio.QueryParameter.endpoint.connection.RequestParameter#setValue(java.lang.String)
		 */
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public URLParameter build() {
			return this.result;
		}

	}
	
	private String value	= null;
	
	/**
	 * 
	 */
	protected URLParameter() {
		
	}

	/**
	 * Returns the value of this parameter
	 * 
	 * @return the value of this parameter
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Assigns a value to this parameter
	 * 
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append(DELIMITER);
		buffer.append(this.value);
		
		return buffer.toString();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
