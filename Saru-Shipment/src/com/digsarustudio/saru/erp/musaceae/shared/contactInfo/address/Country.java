/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type represents a country
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Country {
	/**
	 * 
	 * The builder for {@link Country}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Country> {
		Builder setCode(String code);
		Builder setName(String name);
	}
	
	void setCode(String code);
	String getCode();
	
	void setName(String name);
	String getName();
	
	Boolean validate();
}
