/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.time.javabean;

import java.io.Serializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.musaceae.shared.time.Date}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
@SuppressWarnings("serial")
public class Date implements com.digsarustudio.saru.erp.musaceae.shared.time.Date, Serializable {
	/**
	 * 
	 * The object builder for {@link Date}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.musaceae.shared.time.Date.Builder {
		private Date result = null;

		private Builder() {
			this.result = new Date();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Date.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Date.Builder#setYear(java.lang.String)
		 */
		@Override
		public Builder setYear(String year) {
			this.result.setYear(year);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Date.Builder#setMonth(java.lang.String)
		 */
		@Override
		public Builder setMonth(String month) {
			this.result.setMonth(month);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Date.Builder#setDay(java.lang.String)
		 */
		@Override
		public Builder setDay(String day) {
			this.result.setDay(day);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Date build() {
			return this.result;
		}

	}
	
	private String value	= null;
	
	private String year		= null;
	private String month	= null;
	private String day		= null;

	/**
	 * 
	 */
	private Date() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Date#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.value = value;
		
		if( null != value && value.contains(BACKSLASH_DELIMITER) ) {
			String[] split = value.split(BACKSLASH_DELIMITER);
			
			this.day = split[0];
			this.month = split[1];
			this.year = split[2];
		}else if(null != value && value.contains(DASH_DELIMITER)){
			String[] split = value.split(DASH_DELIMITER);
			
			this.day = split[0];
			this.month = split[1];
			this.year = split[2];
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Date#getValue()
	 */
	@Override
	public String getValue() {

		return this.value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Date#setYear(java.lang.String)
	 */
	@Override
	public void setYear(String year) {
		this.year = year;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Date#setMonth(java.lang.String)
	 */
	@Override
	public void setMonth(String month) {
		this.month = month;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Date#setDay(java.lang.String)
	 */
	@Override
	public void setDay(String day) {
		this.day = day;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Date#getYear()
	 */
	@Override
	public String getYear() {
		return this.year;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Date#getMonth()
	 */
	@Override
	public String getMonth() {
		return this.month;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Date#getDay()
	 */
	@Override
	public String getDay() {
		return this.day;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Date#toMMYYYY()
	 */
	@Override
	public String toMMYYYY() {
		if(null != this.value) {
			return this.value;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		buffer.append(this.day);
		buffer.append(Date.BACKSLASH_DELIMITER);
		buffer.append(this.month);
		buffer.append(Date.BACKSLASH_DELIMITER);
		buffer.append(this.year);
		
		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Date#toTimestamp()
	 */
	@Override
	public Timestamp toTimestamp() {
		if(null == this.value || this.value.isEmpty()) {
			return null;
		}

		return Timestamp.builder().setValue(this.value)
								  .build();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Date#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.value || this.value.isEmpty()) {
			return false;
		}
		
		return true;		
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuffer buffer = new StringBuffer();
		buffer.append("date:");
		buffer.append(this.toMMYYYY());
		buffer.append("\ntimestamp:");
		buffer.append(this.toTimestamp().toString());
		
		return buffer.toString();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
