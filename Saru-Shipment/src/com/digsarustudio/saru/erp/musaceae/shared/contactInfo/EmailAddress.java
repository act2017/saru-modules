/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type represents a particular email address.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface EmailAddress {
	/**
	 * The hyphen connects account and domain
	 */
	public final String ACCOUNT_DELIMITER = "@";
	
	/**
	 * The regular expression for e-mail validation
	 */
	public final String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
	
	/**
	 * The regular expression for account name validation
	 */
	public final String ACCOUNT_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]$";
	
	/**
	 * The regular expression for domain name validation
	 */
	public final String DOMAIN_REGEX = "([\\w]+\\.)+[\\w]+[\\w]$";
	
	/**
	 * 
	 * The builder for {@link EmailAddress}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static interface Builder extends ObjectBuilder<EmailAddress> {
		Builder setAccountName(String name);
		Builder setDomainName(String name);	
		Builder setAddress(String address);
	}
	
	void setAccountName(String name);
	void setDomainName(String name);	
	void setAddress(String address);
		
	String getAccountName();
	String getDomainName();
	String getAddress();
	
	Boolean validate();	
}
