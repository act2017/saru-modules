/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.Length;
import com.digsarustudio.saru.erp.musaceae.shared.LengthUnit;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The implementation of {@link com.digsarustudio.saru.erp.musaceae.shared.Length}<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class BaseLength implements Length, IsSerializable {
	/**
	 * 
	 * The object builder for {@link BaseLength}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements Length.Builder {
		private BaseLength result = null;

		public Builder() {
			this.result = new BaseLength();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.Length.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.Length.Builder#setUnit(com.digsarustudio.saru.erp.musaceae.shared.LengthUnit)
		 */
		@Override
		public Builder setUnit(LengthUnit unit) {
			this.result.setUnit(unit);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public BaseLength build() {
			return this.result;
		}

	}

	private String		value	= null;
	private LengthUnit	unit	= null;
	
	/**
	 * 
	 */
	protected BaseLength() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Length#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.value = value;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Length#setUnit(com.digsarustudio.saru.erp.musaceae.shared.LengthUnit)
	 */
	@Override
	public void setUnit(LengthUnit unit) {
		this.unit = unit;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Length#getValue()
	 */
	@Override
	public String getValue() {
		
		return this.value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Length#getIntValue()
	 */
	@Override
	public Integer getIntValue() {
		
		return new Integer(this.value);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Length#getUnit()
	 */
	@Override
	public LengthUnit getUnit() {
		
		return this.unit;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Length#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.value || this.value.isEmpty()) {
			return false;
		}
		
		if(null == this.unit) {
			return false;
		}
		
		return true;
	}
}
