/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.time.javabean;

import java.io.Serializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.musaceae.shared.time.Timestamp}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
@SuppressWarnings("serial")
public class Timestamp implements com.digsarustudio.saru.erp.musaceae.shared.time.Timestamp, Serializable {
	/**
	 * 
	 * The object builder for {@link Timestamp}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.musaceae.shared.time.Timestamp.Builder {
		private Timestamp result = null;

		private Builder() {
			this.result = new Timestamp();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Timestamp.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Timestamp.Builder#setValue(java.lang.Long)
		 */
		@Override
		public Builder setValue(Long value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Timestamp build() {
			return this.result;
		}

	}

	private String value	= null;
	
	/**
	 * 
	 */
	private Timestamp() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Timestamp#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.value = value;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Timestamp#setValue(java.lang.Long)
	 */
	@Override
	public void setValue(Long value) {
		this.value = value.toString();

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Timestamp#getValue()
	 */
	@Override
	public Long getValue() {
		
		return Long.parseLong(this.value);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.time.Timestamp#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.value || this.value.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
