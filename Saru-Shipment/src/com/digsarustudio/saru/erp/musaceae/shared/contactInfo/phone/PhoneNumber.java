/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-types represents a particular phone number.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface PhoneNumber {
	/**
	 * 
	 * The builder for {@link PhoneNumber}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static interface Builder extends ObjectBuilder<PhoneNumber> {
		Builder setNumber(String number);
	}	
	
	void setNumber(String number);
	
	String getNumber();
	
	Boolean validate();
}
