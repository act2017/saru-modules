/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint.url;

/**
 * The protocol used by application which are HTTP, HTTPS, FTP and so on.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public enum ApplicationProtocolType {
	 HTTP("HTTP")
	,HTTPS("HTTPS")
	,FTP("FTP")
	;
	private String value = null;

	private static ApplicationProtocolType[] VALUES = values();

	private ApplicationProtocolType(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public static ApplicationProtocolType fromValue(String value) {
		ApplicationProtocolType rtn = null;

		for (ApplicationProtocolType target : VALUES) {
			if (!value.equals(target.getValue())) {
				continue;
			}

			rtn = target;
		}

		return rtn;
	}
}
