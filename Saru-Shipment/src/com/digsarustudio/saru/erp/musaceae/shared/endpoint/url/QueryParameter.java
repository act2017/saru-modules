/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.endpoint.url;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * This class contains a value with the key to represent as a parameter for the request action to the 
 * remote server.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public class QueryParameter {
	public static final String DELIMITER	= "=";
	
	/**
	 * 
	 * The object builder for {@link QueryParameter}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static class Builder implements ObjectBuilder<QueryParameter> {
		private QueryParameter result = null;

		public Builder() {
			this.result = new QueryParameter();
		}

		/**
		 * @param key
		 * @see com.digsarustudio.QueryParameter.endpoint.connection.RequestParameter#setKey(java.lang.String)
		 */
		public Builder setKey(String key) {
			this.result.setKey(key);
			return this;
		}

		/**
		 * @param value
		 * @see com.digsarustudio.QueryParameter.endpoint.connection.RequestParameter#setValue(java.lang.String)
		 */
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public QueryParameter build() {
			return this.result;
		}

	}
	
	private String key		= null;
	private String value	= null;
	
	/**
	 * 
	 */
	protected QueryParameter() {
		
	}

	/**
	 * Returns the key of this parameter
	 * 
	 * @return the key of this parameter
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Assigns a key to this parameter
	 * 
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * Returns the value of this parameter
	 * 
	 * @return the value of this parameter
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Assigns a value to this parameter
	 * 
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if(null == this.key || this.key.isEmpty()) {
			return "";
		}
		
		StringBuffer buffer = new StringBuffer();
		buffer.append(this.key);
		if(null != this.value) {
			buffer.append(DELIMITER);
			buffer.append(this.value);
		}
		
		return buffer.toString();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
