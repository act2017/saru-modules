/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.geoinfo;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type represents a longitude object.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Longitude {
	/**
	 * 
	 * The builder for {@link Longitude}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Longitude> {
		Builder setValue(Float value);
		Builder setValue(String value);
	}
	
	void setValue(Float value);
	void setValue(String value);
		
	String getValue();
	Float getValueFloat();
	
	Boolean validate();
}
