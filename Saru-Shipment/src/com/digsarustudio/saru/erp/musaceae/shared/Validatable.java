/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared;

/**
 * The sub-type has the ability to be validated by {@link ValidationVisitor}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.3
 * @since		1.0.0
 * 
 * @param		T	The data type to be validated.<br>
 *
 * Note:
 * 				1.0.3	-> Replace {@link IllegalArgumentException} by {@link InvalidDataException} to enforce the 
 * 						   client code to handle the exception.<br>
 */
public interface Validatable<T> {
	void accept(ValidationVisitor<T> visitor) throws InvalidDataException;
}
