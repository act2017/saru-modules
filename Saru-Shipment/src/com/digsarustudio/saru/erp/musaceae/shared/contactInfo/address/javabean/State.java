/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class State implements com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State
							, IsSerializable{
	public static final String ATTR_NAME	= "name";
	public static final String ATTR_CODE	= "code";
	
	/**
	 * 
	 * The object builder for {@link State}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State.Builder {
		private State result = null;

		private Builder() {
			this.result = new State();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.State.Builder#setName(java.lang.String)
		 */
		@Override
		public Builder setName(String name) {
			this.result.setName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.State.Builder#setCode(java.lang.String)
		 */
		@Override
		public Builder setCode(String code) {
			this.result.setCode(code);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public State build() {
			return this.result;
		}

	}

	private String name	= null;
	private String code	= null;
	
	/**
	 * 
	 */
	private State() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.State#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.State#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.code = code;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.State#getName()
	 */
	@Override
	public String getName() {
		
		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.State#getCode()
	 */
	@Override
	public String getCode() {
		
		return this.code;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.State#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.code || this.code.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
