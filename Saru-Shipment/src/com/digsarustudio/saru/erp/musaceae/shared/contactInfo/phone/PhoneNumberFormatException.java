/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone;

/**
 * Represents the format of phone number is not valid.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class PhoneNumberFormatException extends Exception {

	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = -1912763550570879118L;

	/**
	 * 
	 */
	public PhoneNumberFormatException() {

	}

	/**
	 * @param message
	 */
	public PhoneNumberFormatException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public PhoneNumberFormatException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public PhoneNumberFormatException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public PhoneNumberFormatException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
