/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Search event for widget
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SearchEvent extends AbstractCompositeEvent<SearchEvent.Handler> {
	/**
	 * 
	 * The event handler of search event
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public interface Handler extends EventHandler{
		void onSearch(SearchEvent event);
	}
	
	/**
	 * Constructs a search event
	 */
	public SearchEvent(){	
		super();
	}
	
	/**
	 * Constructs a search event with a particular data
	 * 
	 * @param data The data passed with event
	 */
	public SearchEvent(Object data){
		super(data);		
	}
	
	public static Type<SearchEvent.Handler> TYPE = new Type<SearchEvent.Handler>();

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.DomEvent#getAssociatedType()
	 */
	@Override
	public Type<SearchEvent.Handler> getAssociatedType() {
		return TYPE;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(Handler handler) {
		handler.onSearch(this);
	}
	
}
