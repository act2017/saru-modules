/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.polymer.dialog;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * The sub-type handles {@link DialogCancelActionHandler} to deal with the event from dialog.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface HasDialogCancelActionHandler {
	HandlerRegistration addDialogCancelActionHandler(DialogCancelActionHandler handler);
}
