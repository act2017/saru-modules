/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.utils;

import com.google.gwt.i18n.client.NumberFormat;

/**
 * This object can format Integer and Double into a formatted string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DataFormatter {
	private static DataFormatter instance;
	
	private final String DEFAULT_PRICE_PATTERN = "#,##0.00";
	
	/**
	 * Seal the constructor
	 */
	private DataFormatter() {
		
	}
	
	/**
	 * Get the Instance
	 * @return The instance of this object
	 */
	public static DataFormatter getInstance() {
		if (null == instance) {
			instance = new DataFormatter();
		}
	
		return instance;
	}
	
	/**
	 * Returns the formatted price. I.E. 1,234.22.
	 * 
	 * @param price The unformatted price string
	 * 
	 * @return the formatted price. I.E. 1,234.22.
	 */
	public String formatPrice(String price){
		if(null == price) {
			return "";
		}
		
		Double db = NumberFormat.getDecimalFormat().parse(price);
		
		return NumberFormat.getFormat(this.DEFAULT_PRICE_PATTERN).format(db);
	}
}
