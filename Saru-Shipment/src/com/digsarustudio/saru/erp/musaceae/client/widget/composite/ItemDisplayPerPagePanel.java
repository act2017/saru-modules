/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.composite;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * A panel to let users choose how many items they want to see on the page 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemDisplayPerPagePanel extends Composite implements HasChangeHandlers {

	private static HowManyPerPagePanelUiBinder uiBinder = GWT.create(HowManyPerPagePanelUiBinder.class);

	interface HowManyPerPagePanelUiBinder extends UiBinder<Widget, ItemDisplayPerPagePanel> {
	}

	@UiField ListBox	numberPerPage;
	@UiField Label		total;
	@UiField Label		totalSuffix;
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public ItemDisplayPerPagePanel() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public void setTotalItemCount(Integer count){
		if(count > 1){
			this.totalSuffix.setText("items");
		}
		
		this.total.setText(count.toString());
	}
	
	public String getNumberPerPage(){
		return this.numberPerPage.getSelectedValue();
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.HasChangeHandlers#addChangeHandler(com.google.gwt.event.dom.client.ChangeHandler)
	 */
	@Override
	public HandlerRegistration addChangeHandler(ChangeHandler handler) {
		return this.numberPerPage.addChangeHandler(handler);
	}

	
}
