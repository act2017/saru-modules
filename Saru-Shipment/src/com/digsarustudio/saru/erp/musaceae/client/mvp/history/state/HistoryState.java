/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.history.state;

import com.digsarustudio.saru.erp.musaceae.client.factory.AbstractDataModelBuilderSetFactory;
import com.digsarustudio.saru.erp.musaceae.client.mvp.history.HistoryTokenCollection;

/**
 * The sub-type can be a more specific history state for the app controller.
 * 
 * Please extends {@link AbstractHistoryState} for the state used by app controller.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface HistoryState {
	/**
	 * The name of key page
	 */
	public final String TOKEN_STATE	= "STATE";
	
	/**
	 * Returns a set of tokens which represents this History State
	 * 
	 * @return a set of tokens which represents this History State
	 */
	public HistoryTokenCollection getTokens();
	
	
	/**
	 * Returns the name of this state
	 * 
	 * @return the name of this state
	 */
	public String getStateName();
		
	void setDataModelBuilderSetFactory(AbstractDataModelBuilderSetFactory factory);
	
	/**
	 * To send a notification to the browser to change the current state.
	 * 
	 */
	void fireState();
	
	/**
	 * Assigns a state to the browser, but do not to execute it.
	 * 
	 */
	void setState();	
}
