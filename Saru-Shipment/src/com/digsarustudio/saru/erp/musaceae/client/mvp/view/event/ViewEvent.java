/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.view.event;

import com.digsarustudio.saru.erp.musaceae.client.mvp.view.SupervisingView;

/**
 * The subclass of this {@link ViewEvent} has the ability to carry a source data for the client code to 
 * handle an event passed from a {@link PassiveView} or {@link SupervisingView} widget.<br>
 * Normally, this type of event only used between {@link Presenter} and {@link PassiveView}.<br>
 * If you wanna pass the event to the {@link AppController}, please use the subclass of {@link EventBusEvent}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 * @deprecated	1.0.3	Please see the ReadMe file and use the event from com.digsarustudio.saru.erp.musaceae.client.widget.event
 */
public abstract class ViewEvent<T>{
	private T sourceData = null;
	
	/**
	 * 
	 */
	public ViewEvent() {
	}

	/**
	 * @return the source data
	 */
	public T getSourceData() {
		return sourceData;
	}

	/**
	 * @param source the source to set
	 */
	public void setSourceData(T source) {
		this.sourceData = source;
	}

}
