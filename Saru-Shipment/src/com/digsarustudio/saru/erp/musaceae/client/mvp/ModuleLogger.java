/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.digsarustudio.banana.utils.StringFormatter;

/**
 * The logger for the module in the front app to output a log on the browser console and to reduce the function call.<br> 
 * 
 * How to use:<br>
 * 	1. Add the following snippet in to the module configuration file (*.gwt.xml): <br>
 *   	<inherits name="com.google.gwt.logging.Logging"/> <br>
 *   	<!-- Setup properties of logger --> <br>
 *   	<!-- <set-property name="gwt.logging.logLevel" value="INFO"/>  --> <br>          
 *   	<set-property name="gwt.logging.enabled" value="TRUE"/> <br>
 *	<br>
 *  2. And inherit MVP module as following: <br>
 *  	<inherits name='com.digsarustudio.saru.erp.musaceae.Module_Musaceae'/> <br>
 *
 * 
 * It is a wrapper of {@link Logger}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ModuleLogger {
	private Logger logger = null;
	
	private Boolean isDebugging = false;
	
	/**
	 * 
	 */
	public ModuleLogger(String name) {
		this.logger = Logger.getLogger(name);
	}
	
	public void info(String msg) {
		this.logger.info(msg);
	}
	
	public void warning(String msg) {
		this.logger.warning(msg);
	}
	
	public void severe(String msg) {
		this.severe(msg, null);
	}
	
	public void severe(Throwable cause) {
		this.severe(null, cause);
	}
	
	public void severe(String msg, Throwable cause) {		
		this.logger.severe(StringFormatter.formatExceptionCauseMessage(msg, cause));
	}
	
	public void debug(String msg) {
		if( !this.isDebugging ) {
			return;
		}
		
		this.logger.info("DEBUG => " + msg);
	}

	public void offLogging() {
		this.logger.setLevel(Level.OFF);
		this.isDebugging = false;
	}
	
	public void logInfomationalMessage() {
		this.logger.setLevel(Level.INFO);
	}
	
	public void logWarningMessage() {
		this.logger.setLevel(Level.WARNING);
	}
	
	public void logSevereMessage() {
		this.logger.setLevel(Level.SEVERE);
	}
	
	public void logAllMessage() {
		this.logger.setLevel(Level.ALL);
	}
	
	public void logDebugMessage() {
		this.isDebugging = true;
	}
	
	public void notLogDebugMessage() {
		this.isDebugging = false;
	}
}
