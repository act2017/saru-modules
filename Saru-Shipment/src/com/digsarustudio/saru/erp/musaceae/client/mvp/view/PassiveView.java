/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.view;

import com.google.gwt.user.client.ui.Widget;

/**
 * The definitions of the basic view which can be operated with Display class.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.5
 * @since		1.0.0
 *
 */
public interface PassiveView{
	
	void clear();
	void reset();
	
	/**
	 * Returns the view itself as widget
	 * 
	 * @return the view itself as widget
	 */
	Widget asWidget();
}
