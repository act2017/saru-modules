/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.presenter;

import java.util.List;

import com.digsarustudio.saru.erp.musaceae.client.mvp.view.SupervisingView;
import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.dialog.HasDialogCancelActionHandler;
import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.dialog.HasDialogCloseActionHandler;
import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.dialog.HasDialogConfirmActionHandler;
import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.dialog.HasDialogNoActionHandler;
import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.dialog.HasDialogResetActionHandler;
import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.dialog.HasDialogYesActionHandler;

/**
 * The sub-type handles event and display with {@link PagerDataGrid}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface PagerDataGridHandlingPresenter<T> extends Presenter{
	public static final Integer DEFAULT_CURSOR = 0;
	public static final Integer DEFAULT_VISIBLE_ROW_COUNT = 25;
	
	/**
	 * The interface for this presenter to mutate and access the target view.<br> 
	 * The sub-type must handle a dialog for the editor to display.<br>
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public interface Display<T> extends SupervisingView, HasDialogConfirmActionHandler
													   , HasDialogCancelActionHandler
													   , HasDialogResetActionHandler
													   , HasDialogCloseActionHandler
													   , HasDialogYesActionHandler
													   , HasDialogNoActionHandler {
		/**
		 * To clear the contents of this display
		 */
		void clear();

		/**
		 * To reset the contents of this display to default value
		 */
		void reset();
		
		Integer getCurrentCursor();
		
		void setMaxRowPerPage(Integer count);
		
		void updatePageVisibleRange(Integer cursor, Integer length);
		
		void updateTotolCount(Integer total);
		
		void updateUI(List<T> data);
		
		Integer getDisplayPerPageCount();
		
		SupervisingView getEditorView();
		
		void showEditorDialog();
		void hideEditorDialog();
		
		void showDeleteConfirmDialog(String msg);
		void hideDelelteConfirmDialog();
		
		void hideApplyButton();
		void showApplyButton();
		void hideCancelButton();
		void showCancelButton();
		void hideResetButton();
		void showResetButton();
		void hideCloseButton();
		void showCloseButton();
	}
	
	void setCurrentCursor(Integer cursor);
	void setMaxRowPerPage(Integer count);
	
	void onPageChanged(Integer cursor);	
	void onColumnSorting(Integer columnIndex, Boolean isAscending, Integer cursor);
	void onMaxRowPerPageChanged(Integer count);
	
	void onCreate();
	void onEdit(final T target);
	void onView(final T target);
	void onDelete(final T target);
}
