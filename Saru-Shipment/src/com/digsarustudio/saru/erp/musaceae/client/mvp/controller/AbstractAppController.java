/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.controller;

import java.util.HashMap;
import java.util.Map;

import com.digsarustudio.banana.utils.InvalidTokenFormatException;
import com.digsarustudio.saru.erp.musaceae.client.factory.AbstractFactoryCollection;
import com.digsarustudio.saru.erp.musaceae.client.mvp.history.HistoryTokenCollection;
import com.digsarustudio.saru.erp.musaceae.client.mvp.history.state.HistoryState;
import com.digsarustudio.saru.erp.musaceae.client.mvp.history.state.HistoryStateBus;
import com.digsarustudio.saru.erp.musaceae.client.mvp.history.state.HistoryStateHandler;
import com.digsarustudio.saru.erp.musaceae.client.mvp.view.PassiveView;
import com.digsarustudio.saru.erp.musaceae.shared.NoSuchValueException;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.google.gwt.user.client.ui.HasWidgets;

/**
 * The base class of app controller
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public abstract class AbstractAppController implements AppController{
	/**
	 * The event bus used to control the events between the presenter
	 */
	protected EventBus eventBus;
	
	protected AbstractFactoryCollection factoryCollection = null;
	
	/**
	 * The main container used to be a root panel.
	 */
	protected HasWidgets mainContainer;
	
	
	
	private Map<Class<? extends HistoryState>, PassiveView> persistentViews = new HashMap<>();
		
	/**
	 * Constructs the app controller
	 * 
	 * @param eventBus The event handler
	 */
	public AbstractAppController(EventBus eventBus) {
		this.eventBus = eventBus;
		
		this.init();
		this.bind();
	}
	
	/**
	 * 
	 */
	public AbstractAppController(EventBus eventBus, AbstractFactoryCollection factory) {
		this.eventBus = eventBus;
		this.factoryCollection = factory;
		
		this.init();
		this.bind();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.tutorial.google.web.tooltkits.twoeight.client.AppController#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {
		this.mainContainer = container;
		this.mainContainer.clear();
		
		this.buildMainLayout();
	}
	
	/**
	 * Register this controller as the on value change event handler to History
	 * 
	 * Please implement this function in the sub-class in order to register the event handler to the event bus.
	 */
	protected void bind(){		
		this.bindEvents();
		this.bindHistoryState();
	}
	
	/**
	 * To register an event and its handler to the event bus
	 * 
	 * @param type The type of GWT Event
	 * @param handler The event handler of the target event
	 */
	protected <H extends EventHandler> void registerEventHandler(Type<H> type, H handler) {
		this.eventBus.addHandler(type, handler);
	}	
	
	/**
	 * To register a history state handler for the state changing notification
	 * 
	 * @param state The target state
	 * @param handler The handler which deals with the operation when this state occurred.
	 * 
	 * @since 1.0.5
	 */
	protected void registerHistoryStateHandler(String state, HistoryStateHandler handler){
		HistoryStateBus.addHistoryStateHandler(state, handler);
	}
	
	/**
	 * Returns the tokens of current history state
	 * 
	 * @return the tokens of current history state
	 * @throws InvalidTokenFormatException When the current token is not in a valid format.
	 * 
	 * @since 1.0.5
	 */
	protected HistoryTokenCollection getCurrentHistoryStateTokens() throws InvalidTokenFormatException{
		return HistoryStateBus.getCurrentState();
	}
	
	/**
	 * To add a persistent view called by a particular state
	 * 
	 * @param state The state operating the view
	 * @param view The view to be handled by the specific event
	 * 
	 */
	protected void addPersistentView(Class<? extends HistoryState> state, PassiveView view){
		if( this.persistentViews.containsKey(state) ){
			throw new IllegalArgumentException("Duplicated regsitering for view[" + view.getClass().getName() + "].");
		}
		
		this.persistentViews.put(state, view);
	}
	
	/**
	 * Returns a persistent view called by a particular state
	 * 
	 * @param state The state operating the view
	 * 
	 * @throws NoSuchValueException When the expecting view was not binded with the target event
	 * 
	 * @return The binded view for the target event
	 */
	@SuppressWarnings("unchecked")
	protected <T> T getPersistentView(Class<? extends HistoryState> state) throws NoSuchValueException{
		if( !this.persistentViews.containsKey(state) ){
			throw new NoSuchValueException("No match view for event " + state.toString() + ".");
		}
		
		return (T) this.persistentViews.get(state);
	}	
		
	protected AbstractFactoryCollection getFactoryCollection() {
		return this.factoryCollection;
	}

	/**
	 * To bind the events used in event bus
	 */
	protected abstract void bindEvents();
	
	/**
	 * To bind the history state used when the state of history changed
	 * 
	 * Please bind the history state with {@link HistoryStateBus.addHistoryStateHandler()}
	 */
	protected abstract void bindHistoryState();
	
	/**
	 * To initialize this app controller
	 */
	protected abstract void init();
	
	/**
	 * To build main layout. Please create the main layout object in this function 
	 * if the main layout has to be created dynamically.
	 */
	protected abstract void buildMainLayout();
}
