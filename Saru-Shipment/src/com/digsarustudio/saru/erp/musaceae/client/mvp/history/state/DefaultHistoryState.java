/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.history.state;

import com.digsarustudio.banana.utils.InvalidTokenFormatException;
import com.digsarustudio.saru.erp.musaceae.client.factory.AbstractDataModelBuilderSetFactory;
import com.digsarustudio.saru.erp.musaceae.client.mvp.history.HistoryToken;
import com.digsarustudio.saru.erp.musaceae.client.mvp.history.HistoryTokenCollection;

/**
 * The default history state object for the developer who doesn't want to create a new class for the history state.
 * 
 * If you want to have your own history state, please extends {@link AbstractHistoryState} and declare the static methods for fireState(HistoryTokenCollection)
 * and setState(HistoryTokenCollection).
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public abstract class DefaultHistoryState extends AbstractHistoryState implements HistoryState {

	/**
	 * @param name
	 */
	public DefaultHistoryState(String name) {
		super(name);
	}

	/**
	 * @param name
	 * @param tokens
	 */
	public DefaultHistoryState(String name, HistoryTokenCollection tokens, AbstractDataModelBuilderSetFactory factory) {
		super(name, tokens, factory);
	}	
	
	/**
	 * @param name
	 * @param tokenString
	 * @throws InvalidTokenFormatException
	 */
	public DefaultHistoryState(String name, String tokenString, AbstractDataModelBuilderSetFactory factory) throws InvalidTokenFormatException {
		super(name, tokenString, factory);
	}

	/**
	 * To fire a state to the history state bus and notify the registered client immediately
	 * 
	 * @param state The value of the state
	 * @param tokens The tokens to fire
	 */
	public static void fireState(String state, HistoryTokenCollection tokens){
		HistoryState hs = new DefaultHistoryState(state) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);					
				}
			}
		};
		
		hs.fireState();
	}
	
	/**
	 * To set a state to the history state bus but not notify the registered client
	 * 
	 * @param state The value of the state
	 * @param tokens The tokens to fire
	 */
	public static void setState(String state, HistoryTokenCollection tokens){
		HistoryState hs = new DefaultHistoryState(state) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);					
				}
			}
		};
		
		hs.setState();		
	}
}
