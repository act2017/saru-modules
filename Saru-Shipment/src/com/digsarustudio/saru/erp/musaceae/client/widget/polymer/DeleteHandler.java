/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.polymer;

import com.google.gwt.event.shared.EventHandler;

/**
 * The event handler of {@link DeleteEvent}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface DeleteHandler extends EventHandler {
	void onDelete(DeleteEvent event);
}
