/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.composite.list;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.digsarustudio.saru.erp.musaceae.client.widget.event.ChangeEvent;
import com.digsarustudio.saru.erp.musaceae.client.widget.event.ChangeHandler;
import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.DeleteEvent;
import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.DeleteHandler;
import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.HasDeleteHandler;
import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.dialog.SaruYesNoDialog;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * This panel handles {@link DeletableListItemPanel} to let the user has an ability to remove an unwanted item from his/her list.<br>
 * This panel calls back to its parent widget when the {@link DeletableListItemPanel} sent a delete event back and the user confirmed 
 * the action by {@link SaruYesNoDialog}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ListPanel<T> extends Composite implements HasDeleteHandler{
	Logger logger = Logger.getLogger(this.getClass().getName());
	
	private static ListPanelUiBinder uiBinder = GWT.create(ListPanelUiBinder.class);

	@SuppressWarnings("rawtypes")
	interface ListPanelUiBinder extends UiBinder<Widget, ListPanel> {
	}

	@UiField		Label			title;
	@UiField		VerticalPanel	mainCanvas;
	@UiField		SaruYesNoDialog	yesNoDialog;
	
	private DeleteHandler			deleteHandler	= null;
	private ListItemPanel.Builder<T>	itemPanelBuilder	= null;	
	
	private List<ListItemPanel<T>>	itemPanels		= new ArrayList<>();
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public ListPanel() {
		initWidget(uiBinder.createAndBindUi(this));
	}
		
	public void setTitle(String title) {
		this.title.setText(title);
	}
	
	/**
	 * To initialize the 1st item panel for select.<br>
	 * This function only can be called when the item panel builder has been established.<br>
	 */
	public void init() {
		if(this.itemPanels.isEmpty()) {
			this.buildItemPanel();
		}
	}
	
	public void setListItemPanelBuilder(ListItemPanel.Builder<T> builder) {
		this.itemPanelBuilder = builder;
	}
	
	/**
	 * set up a new item and create a new list item
	 * 
	 * @param item The item to be added into the target widget.<br>
	 */
	public void addItem(final T item) {
		Integer last = this.itemPanels.size() - 1 ;
		
		ListItemPanel<T> itemPanel = this.itemPanels.get(last);		
		itemPanel.setSourceData(item);
		
		this.buildItemPanel();
	}
	
	public T getItemValue(Integer index) {
		if(this.itemPanels.size() <= index) {
			throw new IndexOutOfBoundsException("the size of itemPanels is " + this.itemPanels.size() + ", but the input is " + index.toString());
		}
		
		return this.itemPanels.get(index).getValue();
	}
	
	public void reset() {
		this.mainCanvas.clear();
		this.itemPanels.clear();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.polymer.HasDeleteHandler#addDeleteHandler(com.digsarustudio.musa.widget.polymer.DeleteHandler)
	 */
	@Override
	public HandlerRegistration addDeleteHandler(DeleteHandler handler) {
		this.deleteHandler	= handler;
		
		return new HandlerRegistration() {
			
			@Override
			public void removeHandler() {
				deleteHandler = null;
			}
		};
	}

	private void showYesNoDialog(String msg, ClickHandler yesClickHandler) {
		this.yesNoDialog.setVisible(true);		
		this.yesNoDialog.setInfoMessage(msg);
		this.yesNoDialog.addYesClickHandler(yesClickHandler);
		this.yesNoDialog.addNoClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				yesNoDialog.close();
			}
		});
		
		this.yesNoDialog.open();
	}
	
	private void deleteItemPanel(ListItemPanel<T> itemPanel) {
		this.mainCanvas.remove(itemPanel);
		this.itemPanels.remove(itemPanel);
	}
	
	private ListItemPanel<T> buildItemPanel(){
		if(null == this.itemPanelBuilder) {
			return null;
		}
		
		final ListItemPanel<T> itemPanel = this.itemPanelBuilder.build();
		
		//The source data will be assign from the client code
		
		itemPanel.addDeleteHandler(new DeleteHandler() {
			
			@Override
			public void onDelete(DeleteEvent event) {
				@SuppressWarnings("unchecked")
				T item = (T) event.getSourceData();
				String name = "unknown";
				if(null != item) {
					name = item.toString();
				}else if( null != itemPanel.getValue() ) {
					name = itemPanel.getValue().toString();
				}
				
				StringBuffer buffer = new StringBuffer();
				buffer.append("Are you going to delete ");
				buffer.append(name);
				buffer.append("?\n\nThis can not be undo, if you are going to delete this item, please press Yes.\n"
							+ "Otherwise, please press No to leave it.");
				
				showYesNoDialog(buffer.toString(), new ClickHandler() {
					
					@Override
					public void onClick(ClickEvent event) {
						deleteItemPanel(itemPanel);
						
						if(null == deleteHandler) {
							return;
						}
						
						deleteHandler.onDelete(new DeleteEvent(item));
					}
				});
			}
		});
		
		itemPanel.addChangeHandler(new ChangeHandler() {
			
			@Override
			public void onChange(ChangeEvent event) {
				@SuppressWarnings("unchecked")
				ListItemPanel<T> panelEntity = (ListItemPanel<T>) event.getSourceData();

				T value = panelEntity.getSourceData();
				
				//append tail
				Integer index = itemPanels.indexOf(panelEntity);
				if( (itemPanels.size()-1) == index ) {
					buildItemPanel();
				}
				
				//delete the original data
				if(null != value) {
					deleteHandler.onDelete(new DeleteEvent(value));
				}
			}
		});
				
		this.itemPanels.add(itemPanel);
		this.mainCanvas.add(itemPanel);		
		
		return itemPanel;
	}
}
