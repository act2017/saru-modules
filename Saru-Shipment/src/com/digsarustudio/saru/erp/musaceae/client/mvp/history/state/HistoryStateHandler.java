/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.history.state;

import com.digsarustudio.saru.erp.musaceae.client.mvp.history.HistoryTokenCollection;

/**
 * A handler deals with the onValuChangedEvent.
 * It is a replacement of com.digsarustudio.musa.mvp.history.HistoryStateHandler
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface HistoryStateHandler {
	/**
	 * To execute what the state goes to do
	 * 
	 * @param collection The collection of history token.
	 */
	void execute(HistoryTokenCollection collection);
}
