/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.polymer;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * The sub-type can be assigned an {@link DeleteHandler} to handle {@link DeleteEvent}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface HasDeleteHandler {
	HandlerRegistration addDeleteHandler(final DeleteHandler handler);
}
