/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.google.gwt.user.client.ui.ListBox;

/**
 * This list box handle source data as an implementation of {@link IsBoxedItem} to display its caption on the {@link ListBox} and 
 * returns the value of source data.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class SaruListBox<T> extends ListBox {
	Logger logger = Logger.getLogger(this.getName());
	private Map<String, T> items = new HashMap<>();
	
	private ListBoxItem<T>	itemBox	= null;
	
	public SaruListBox(){
		
	}
	
	/**
	 * 
	 */
	public SaruListBox(ListBoxItem<T> item) {
		this.itemBox = item;
	}
	
	public void setItemBox(ListBoxItem<T> itemBox){
		this.itemBox = itemBox;
	}

	/**
	 * Return true if the data is existing, otherwise false returned.
	 * 
	 * @param item The delegate item object which contains the source data for this list box.
	 * 
	 * @return true if the data is existing, otherwise false returned.
	 */
	public Boolean isExisting(T item) {
		if(null == item){
			return false;
		}
		
		if(null == this.items){
			return false;
		}
		
		if( null == this.itemBox.getKey(item)){
			return false;
		}
		
		return this.items.containsKey(this.itemBox.getKey(item));
	}
	
	/**
	 * Assigns a source data as an item
	 * 
	 * @param item The delegate item object which contains the source data to add
	 */
	public void addItem(T item){
		if( this.isExisting(item) ){
			return;
		}
		
		String key = this.itemBox.getKey(item);
		this.addListBoxItem(key, item);		
		
		this.addItem(this.itemBox.getCaption(item), key);
	}
	
	/**
	 * Inserts a source data as an item
	 * 
	 * @param item The delegate item object which contains the source data to insert
	 * 
	 * @param index The index where the source data to insert.
	 */
	public void insertItem(T item, Integer index) {
		if( this.isExisting(item) ){
			return;
		}
		
		this.addListBoxItem(this.itemBox.getKey(item), item);
		
		this.insertItem(this.itemBox.getCaption(item), this.itemBox.getValue(item), index);
	}
	
	public void setSelectedItem(String caption) {
		Integer count = this.getItemCount();
		for (int i = 0; i < count; i++) {
			if( !caption.equals(this.getItemText(i)) ) {
				continue;
			}
			
			this.setSelectedIndex(i);
			break;
		}
	}
	
	/**
	 * Select an item by its own value
	 * 
	 * @param value
	 */
	public void setSelectedValue(String value) {
		Integer count = this.getItemCount();
		for (int i = 0; i < count; i++) {
			if( !value.equals(this.getValue(i)) ) {
				continue;
			}
			
			this.setSelectedIndex(i);
			break;
		}
		
	}
	
	/**
	 * Selects an item by the source data 
	 * 
	 * @param item The source data to be selected
	 */
	public void setSelectedItem(T item) {
		if(null == item) {
			return;
		}
		
		if(null == this.itemBox) {
			throw new IllegalArgumentException("Please set item box first");
		}
		
		String key = this.itemBox.getKey(item);
		if( !this.items.containsKey(key) ) {
			return;
		}
		
		this.setSelectedValue(this.itemBox.getValue(item));
	}
	
	/**
	 * Returns the source data of the item selected
	 * 
	 * @return the source data of the item selected
	 */
	public T getSelectedItem() {
		String key = this.getSelectedValue();
		
		return this.items.get(key);
	}
	
	/**
	 * Returns the source data according to its sequence in the list box
	 * 
	 * @param index The sequence of the source data in the list box to retrieve
	 * 
	 * @return the source data according to its sequence in the list box
	 */
	public T getSourceData(Integer index){
		String key = this.getValue(index);
		
		return this.getData(key);
	}
	
	/**
	 * Returns the source data according to its own value
	 * 
	 * @param value The value of the source data to retrieve
	 * 
	 * @return the source data according to its own value
	 */
	public T getSourceData(String value){
		String key = null;
		Integer count = this.getItemCount();
		for (int i = 0; i < count; i++) {
			if( !value.equals(this.getValue(i)) ){
				continue;
			}
			
			key = this.getValue(i);
			break;
		}
		
		return this.getData(key);
	}
	
	/**
	 * Returns the source data of selected item
	 * 
	 * @return the source data of selected item
	 */
	public T getSelectedItemSource(){
		String key = this.getSelectedValue();
		
		return this.getData(key);
	}
	
	public Integer getSourceDataCount() {
		return this.items.size();
	}
	
	public Integer getItemIndex(String value) {
		Integer index = null;
		Integer count = this.getItemCount();
		for (int i = 0; i < count; i++) {
			if( !value.equals(this.getValue(i)) ){
				continue;
			}
			
			index = i;
			break;
		}
		
		return index;
	}

	public void removeItem(String value) {
		Integer index = this.getItemIndex(value);
		if(null == index) {
			return;
		}
		
		this.removeItem(index);
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.ListBox#clear()
	 */
	@Override
	public void clear() {
		super.clear();
		
		this.clearSourceDataContainer();
	}	
	
	private void addListBoxItem(String key, T item){
		this.items.put(key, item);
	}
	
	private T getData(String key){
		if(null == key){
			return null;
		}
		
		return this.items.get(key);
	}
	
	private void clearSourceDataContainer(){
		this.items.clear();
	}
}
