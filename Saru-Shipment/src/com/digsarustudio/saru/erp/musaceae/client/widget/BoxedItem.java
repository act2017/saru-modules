/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget;

/**
 * This class represents an item which can be used in {@link SaruListBox} to provide caption and value for it.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public abstract class BoxedItem<T, C> {
	/**
	 * Constructs a boxed item for the Box component.<br>
	 * 
	 * @param source The source data for the Box component to display
	 */
	public BoxedItem() {
	}

	/**
	 * Returns the caption provided by {@link #source} for the Box component to use.<br>
	 * 
	 * @param source The source data to retrieve its own key
	 * 
	 * @return the caption provided by {@link #source} for the Box component to use.
	 */
	public abstract String getKey(T source);
	
	/**
	 * Returns the caption provided by {@link #source} for the Box component to display on the screen.<br>
	 * 
	 * @param source The source data to retrieve its own caption
	 * 
	 * @return the caption provided by {@link #source} for the Box component to display on the screen.
	 */
	public abstract String getCaption(T source);
	
	/**
	 * Returns the value provided by {@link #source} for the Box component.<br>
	 * 
	 * @param source The source data to retrieve its own value
	 * 
	 * @return the value provided by {@link #source} for the Box component.
	 */
	public abstract C getValue(T source);
}
