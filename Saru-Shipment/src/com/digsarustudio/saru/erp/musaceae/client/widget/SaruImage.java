/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget;

import com.google.gwt.dom.client.Element;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeUri;
import com.google.gwt.user.client.ui.Image;

/**
 * The extended image provides the interface for the client code to get original width and height.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SaruImage extends Image {
		
	/**
	 * 
	 */
	public SaruImage() {
	}

	/**
	 * @param resource
	 */
	public SaruImage(ImageResource resource) {
		super(resource);		
	}

	/**
	 * @param url
	 */
	public SaruImage(String url) {
		super(url);

	}

	/**
	 * @param url
	 */
	public SaruImage(SafeUri url) {
		super(url);

	}

	/**
	 * @param element
	 */
	public SaruImage(Element element) {
		super(element);

	}

	/**
	 * @param url
	 * @param left
	 * @param top
	 * @param width
	 * @param height
	 */
	public SaruImage(String url, int left, int top, int width, int height) {
		super(url, left, top, width, height);

	}

	/**
	 * @param url
	 * @param left
	 * @param top
	 * @param width
	 * @param height
	 */
	public SaruImage(SafeUri url, int left, int top, int width, int height) {
		super(url, left, top, width, height);

	}
	
	public Integer getOriginWidth(){
		return this.getOriginWidth(this.getElement());
	}
	
	public Integer getOriginHeight(){
		return this.getOriginHeight(this.getElement());
	}
	
	public String getImageContent() {
		return this.getImageContent(this.getElement());
	}
	
	public void setImageContent(String content) {
		if(null == content) {
			return;
		}
		this.setImageContent(this.getElement(), content);
	}
	
	public void clear(){
		this.getElement().setAttribute("src", "");
	}
	
	public Boolean isEmpty() {
		return (null == this.getElement().getAttribute("src"));
	}
		
	private final native int getOriginWidth(Element element) /*-{
		return element.naturalWidth;
	}-*/;
	
	private final native int getOriginHeight(Element element) /*-{
		return element.naturalHeight;
	}-*/;
		
	/**
	 * Returns the content of this image
	 * 
	 * @param source The image element in the DOM
	 * 
	 * @return the content of this image
	 */
	private final native String getImageContent(Element source) /*-{
		return source.src;		
	}-*/;
	
	/**
	 * Assigns the content of an image to this uploader
	 * 
	 * @param source The image element in the DOM
	 * 
	 * @param content THe content for this uploader
	 */
	private final native void setImageContent(Element source, String content) /*-{
		source.src = content;
	}-*/;
}
