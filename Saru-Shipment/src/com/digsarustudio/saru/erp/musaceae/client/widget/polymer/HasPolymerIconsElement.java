/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.polymer;

import java.util.List;

/**
 * The sub-type of this interface represents that the Polymer Element has been used by it.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public interface HasPolymerIconsElement {
	
	/**
	 * Returns a list of Polymer Icons Element references.<br>
	 * Normally, this method is called by the main process before the app controller has been initiated.<br>
	 *   
	 * @return
	 */
	List<String> getIconsElements();	
}
