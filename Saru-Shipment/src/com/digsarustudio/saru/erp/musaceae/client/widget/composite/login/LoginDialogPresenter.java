/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.composite.login;

import com.digsarustudio.saru.erp.musaceae.client.mvp.presenter.Presenter;
import com.digsarustudio.saru.erp.musaceae.client.mvp.view.SupervisingView;

/**
 * This presenter handles the operation of {@link LoginDialogView}.<br> 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface LoginDialogPresenter extends Presenter {
	public enum LoginStatus{
		 None
		,WrongPassword
		,PasswordExpired
	}
	
	/**
	 * The interface for this presenter to mutate and access the target view. 
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public interface Display extends SupervisingView {				
		/**
		 * The event handler method to tell user the login was failed.
		 * 
		 * @param message The message for the user to indicate what's going on the login.
		 */
		void onFailToLogin(String message);
		
		/**
		 * To show the dialog
		 */
		void showDialog();
		
		/**
		 * To hide the dialog
		 */
		void hideDialog();
		
		void showResetPasswordBtn();
		void hideResetPasswordBtn();
		
		void showForgotPasswordBtn();
		void hideForgotPasswordBtn();
		
		String getUserName();
		String getPassword();
		
		void setIsPasswordExpired();
		void setIsWrongPassword();
	}
	
	
	/**
	 * The user tries to log into the system.
	 * 
	 * @param userName The user name for the login
	 * @param password The password for the login
	 * 
	 */
	void onLogin(String userName, String password);
	
	void onCancel();
	
	void onResetPassword(String userName);
	void onForgotPassword(String userName);
	
	void setIsPasswordExpired();
	void setIsWrongPassword();
}
