/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.history.state;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * To make the sub-type be able to add the handler of history states
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface HasHistoryStateHandlers {
	
	/**
	 * To append a handler of history state for the notification while the history changed.
	 * 
	 * @param state The token of the state
	 * @param handler The handler to deal with the notification
	 * @return HandlerRegistration Used to deregister. {@link HandlerRegistration}
	 */
	HandlerRegistration addHistoryStateHandler(final String state, HistoryStateHandler handler);
}
