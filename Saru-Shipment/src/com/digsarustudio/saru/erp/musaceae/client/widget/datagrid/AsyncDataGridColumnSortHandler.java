/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.datagrid;

/**
 * This handler used by {@link SaruDataGrid} to return a data grid column sorting event to the client code.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface AsyncDataGridColumnSortHandler {
	/**
	 * User clicks the header to sort a column.
	 * 
	 * @param columnIndex
	 * @param isAscending
	 * @param cursor
	 */
	void onColumnSort(Integer columnIndex, Boolean isAscending, Integer cursor);
}
