/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.view;

import com.digsarustudio.saru.erp.musaceae.client.factory.AbstractDataModelBuilderSetFactory;

/**
 * The sub-type of this {@link FactoryView} can be set a {@link AbstractDataModelBuilderSetFactory} for the data model object generation.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface FactoryView extends SupervisingView {
	void setDataModelBuilderSetFactory(AbstractDataModelBuilderSetFactory factory);
}
