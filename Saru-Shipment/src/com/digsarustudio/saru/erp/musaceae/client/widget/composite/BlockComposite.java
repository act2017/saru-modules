/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.composite;

import com.digsarustudio.saru.erp.musaceae.client.widget.event.ClickEvent;
import com.digsarustudio.saru.erp.musaceae.client.widget.event.HasCompositeClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Composite;

/**
 * The base block for other block widget
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class BlockComposite extends Composite implements HasCompositeClickHandlers{

	protected ClickEvent.Handler widgetClickHandler;
	
	protected Object details;
	
	/**
	 * Constructs a block composite object
	 */
	public BlockComposite() {

	}

	/**
	 * @return the details
	 */
	public Object getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(Object details) {
		this.details = details;
	}
	
	public void onWidgetClickEvent(ClickEvent event){
		this.widgetClickHandler.onClick(event);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.HasClickHandlers#addClickHandler(com.google.gwt.event.dom.client.ClickHandler)
	 */
	@Override
	public HandlerRegistration addClickHandler(ClickEvent.Handler handler) {
		this.widgetClickHandler = handler;
		
		return new HandlerRegistration() {
			
			@Override
			public void removeHandler() {
				widgetClickHandler = null;
			}
		};
	}
	
}
