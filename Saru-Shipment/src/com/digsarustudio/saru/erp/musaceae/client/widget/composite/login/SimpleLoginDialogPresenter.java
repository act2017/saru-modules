/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.composite.login;

import com.digsarustudio.saru.erp.musaceae.client.mvp.presenter.AbstractPresenter;
import com.google.gwt.event.shared.EventBus;

/**
 * This presenter handles the user operation from {@link LoginDialogView}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public class SimpleLoginDialogPresenter extends AbstractPresenter<LoginDialogPresenter.Display> implements LoginDialogPresenter {

	private LoginCallback				loginCallback = null;
	
	public SimpleLoginDialogPresenter(LoginDialogPresenter.Display view) {
		super(null, view);
	}
	
	/**
	 * @param eventBus
	 * @param model
	 * @param view
	 */
	public SimpleLoginDialogPresenter(EventBus eventBus, LoginDialogPresenter.Display view) {
		super(eventBus, view);
	}

	/**
	 * @param loginCallback the loginCallback to set
	 */
	public void setLoginCallback(LoginCallback loginCallback) {
		this.loginCallback = loginCallback;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.LoginPresenter#onLogin(java.lang.String, java.lang.String)
	 */
	@Override
	public void onLogin(String userName, String password) {
		if(null == this.loginCallback) {
			return;
		}
		
		this.loginCallback.onLogin(userName, password);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.login.LoginDialogPresenter#onCancel()
	 */
	@Override
	public void onCancel() {
		if(null == this.loginCallback) {
			return;
		}
		
		this.loginCallback.onCancel();
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.login.LoginDialogPresenter#onResetPassword(java.lang.String)
	 */
	@Override
	public void onResetPassword(String userName) {
		if(null == this.loginCallback) {
			return;
		}
	
		this.loginCallback.onResetPassword(userName);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.login.LoginDialogPresenter#onForgotPassword(java.lang.String)
	 */
	@Override
	public void onForgotPassword(String userName) {
		if(null == this.loginCallback) {
			return;
		}
		
		this.loginCallback.onForgetPassword(userName);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.login.LoginDialogPresenter#setIsPasswordExpired()
	 */
	@Override
	public void setIsPasswordExpired() {
		this.getView().setIsPasswordExpired();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.login.LoginDialogPresenter#setIsWrongPassword()
	 */
	@Override
	public void setIsWrongPassword() {
		this.getView().setIsWrongPassword();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.mvp.presenter.AbstractPresenter#init()
	 */
	@Override
	protected void init() {
		this.getView().reset();
		this.getView().showDialog();
	}
}
