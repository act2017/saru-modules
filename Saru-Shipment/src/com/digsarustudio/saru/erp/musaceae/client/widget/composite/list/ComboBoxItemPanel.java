/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.composite.list;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.digsarustudio.saru.erp.musaceae.client.widget.ListBoxItem;
import com.digsarustudio.saru.erp.musaceae.client.widget.event.ChangeHandler;
import com.digsarustudio.saru.erp.musaceae.client.widget.event.HasChangeHandlers;
import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.DeleteHandler;
import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.HasDeleteHandler;
import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.SimpleComboBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

/**
 * The item panel contains {@link SimpleComboBox} for {@link ListPanel}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ComboBoxItemPanel<T> extends Composite implements HasDeleteHandler
															, HasChangeHandlers
															, ListItemPanel<T>{
	Logger logger = Logger.getLogger(this.getClass().getName());
	
	/**
	 * 
	 * The object builder for {@link ComboBoxItemPanel}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder<S> implements ListItemPanel.Builder<S> {		
		private List<S> 			dataList	 	= null;
		private ListBoxItem<S> 	itemBox		= null;

		public Builder() {
			
		}

		public Builder<S> setDataList(List<S> dataList){
			this.dataList = dataList;
			return this;
		}
		
		public Builder<S> setListItemBox(ListBoxItem<S> itemBox){
			this.itemBox = itemBox;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ListItemPanel<S> build() {
			ComboBoxItemPanel<S> result = new ComboBoxItemPanel<>();
			result.setListItemBox(this.itemBox);
			result.setDataList(this.dataList);
			
			return result;
		}

	}

	interface Styling extends CssResource{
		String comboList();
	}
	
	private static ComboBoxItemPanelUiBinder uiBinder = GWT.create(ComboBoxItemPanelUiBinder.class);

	@SuppressWarnings("rawtypes")
	interface ComboBoxItemPanelUiBinder extends UiBinder<Widget, ComboBoxItemPanel> {
	}

	@UiField DeletableListItemPanel<T>	deletePanel;
	@UiField SimpleComboBox<T>			comboBox;
	@UiField Styling						style;
	
	private	T							sourceData = null;
	
	private List<ChangeHandler>			changeHandlers	= null;
	private	Boolean						isChanged	 	= false;
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public ComboBoxItemPanel() {
		initWidget(uiBinder.createAndBindUi(this));
		
		this.init();
	}
	
	@UiHandler("comboBox")
	protected void onComboBoxChangeEvent(ChangeEvent event) {
		if(comboBox.getSelectedIndex() <= 0) {
			deletePanel.disableDeleteButton();
			return;
		}
		
		deletePanel.enableDeleteButton();
		this.isChanged = true;
		
		if(null != this.changeHandlers) {
//			com.digsarustudio.musa.widget.ChangeEvent changeEvent = new com.digsarustudio.musa.widget.ChangeEvent(this.getSelectedItem());
			com.digsarustudio.saru.erp.musaceae.client.widget.event.ChangeEvent changeEvent = new com.digsarustudio.saru.erp.musaceae.client.widget.event.ChangeEvent(this);
			for (ChangeHandler changeHandler : changeHandlers) {
				changeHandler.onChange(changeEvent);
			}
		}else {
			logger.warning("no change handler in " + this.getClass().getName());
		}
	}
	
	public void setDataList(List<T> dataList) {
		if(null == dataList) {
			logger.info("no data list provided");
			return;
		}
		
		for (T data : dataList) {
			this.comboBox.addItem(data);
		}
	}

	public void setListItemBox(ListBoxItem<T> itemBox) {
		this.comboBox.setItemBox(itemBox);
	}
	
	public Boolean isSelectedItemChanged() {
		return (!this.comboBox.getSelectedSourceData().equals(this.sourceData));
	}
	
	public T getSelectedItem() {
		return this.comboBox.getSelectedSourceData();
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.polymer.HasDeleteHandler#addDeleteHandler(com.digsarustudio.musa.widget.polymer.DeleteHandler)
	 */
	@Override
	public HandlerRegistration addDeleteHandler(DeleteHandler handler) {
		return this.deletePanel.addDeleteHandler(handler);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.HasChangeHandlers#addChangeHandler(com.digsarustudio.musa.widget.ChangeHandler)
	 */
	@Override
	public HandlerRegistration addChangeHandler(final ChangeHandler handler) {
		if(null == this.changeHandlers) {
			this.changeHandlers = new ArrayList<ChangeHandler>();
		}
		this.changeHandlers.add(handler);
		
		return new HandlerRegistration() {
			
			@Override
			public void removeHandler() {
				changeHandlers.remove(handler);
			}
		};
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.list.ListItemPanel#getSourceData()
	 */
	@Override
	public T getSourceData() {
		return this.sourceData;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.list.ListItemPanel#getValue()
	 */
	@Override
	public T getValue() {
		return this.isChanged ? this.comboBox.getSelectedSourceData() : null;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.list.ListItemPanel#setSourceData(java.lang.Object)
	 */
	@Override
	public void setSourceData(T source) {
		this.sourceData = source;
		
		if(null == source) {
			this.comboBox.reset();
		} else {
			this.comboBox.setSelectedItem(source);
			
			this.deletePanel.enableDeleteButton();
			this.deletePanel.setSourceData(source);
		}
	}
	
	private void init() {
		this.comboBox.addStyleName(this.style.comboList());
	}
}
