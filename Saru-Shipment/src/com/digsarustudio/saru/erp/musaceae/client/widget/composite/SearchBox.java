/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.composite;

import com.digsarustudio.musa.event.widget.composite.HasSearchEventHandler;
import com.digsarustudio.musa.event.widget.composite.SearchEvent;
import com.digsarustudio.musa.resources.ResourceManager;
import com.digsarustudio.musa.widget.HasHint;
import com.digsarustudio.saru.erp.musaceae.client.widget.SaruTextBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.ImageResource;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;

/**
 * A text box works a search box.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.1
 * @since		1.0.0
 *
 */
public class SearchBox extends Composite implements HasSearchEventHandler, HasHint{

	private static SearchBoxUiBinder uiBinder = GWT.create(SearchBoxUiBinder.class);

	interface SearchBoxUiBinder extends UiBinder<Widget, SearchBox> {
	}
		
	@UiField Image			icon;
	@UiField SaruTextBox	search;
	
	private SearchEvent.Handler searchEventHandler;
		
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public SearchBox() {
		initWidget(uiBinder.createAndBindUi(this));
		this.init();
	}
	
	@UiHandler("search")
	protected void onKeyDownEvent(KeyDownEvent event) {
		if(null == this.searchEventHandler){
			return;
		}else if( event.getNativeKeyCode() != KeyCodes.KEY_ENTER ){
			return;
		}
		
		this.searchEventHandler.onSearch(new SearchEvent(this.search.getText()));
	}
	
	@UiHandler("icon")
	protected void onSearchClickEvent(ClickEvent event) {
		if(null == this.searchEventHandler){
			return;
		}
		
		this.searchEventHandler.onSearch(new SearchEvent(this.search.getText()));
	}
	
	@UiHandler("search")
	protected void onTextFieldClickEvent(ClickEvent event){
		this.search.selectAll();
	}
	
	/**
	 * Clear the input text
	 */
	public void clear(){
		this.search.clear();
	}
	
	/**
	 * Returns the input text to search
	 * 
	 * @return the input text to search
	 */
	public String getText(){
		return this.search.getText();
	}
	
	/**
	 * Assign a URL of image for the icon
	 * 
	 * @param url The URL of image for the icon
	 */
	public void setIcon(String url){
		this.icon.setUrl(url);
	}
	
	/**
	 * Assign a image resource object for the icon
	 * 
	 * @param resource The resource object of target image
	 */
	public void setIcon(ImageResource resource){
		this.icon.setResource(resource);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.tutorial.google.gwt.te.client.testclass.HasSearchEventHandler#addSearchEventHandler(com.digsarustudio.musa.dom.event.SearchEvent.Handler)
	 */
	@Override
	public HandlerRegistration addSearchEventHandler(SearchEvent.Handler handler) {
		this.searchEventHandler = handler;
		
		return new HandlerRegistration() {
			@Override
			public void removeHandler() {
				searchEventHandler = null;
			}
		};
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.HasHint#setHint(java.lang.String)
	 */
	@Override
	public void setHint(String hint) {
		this.search.setPlaceHolder(hint);		
	}

	/**
	 * initialization
	 */
	private void init() {
		this.setIcon(ResourceManager.getInstance().getImagesBundle().getSearchIcon());
	}	
}
