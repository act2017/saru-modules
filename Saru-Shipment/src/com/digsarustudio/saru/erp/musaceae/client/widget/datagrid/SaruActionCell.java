/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.datagrid;

import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;

/**
 * The wrapper of {@link ActionCell} in order to support button disability.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class SaruActionCell<C> extends ActionCell<C> {
	
	public static interface Disability<T>{
		Boolean isDisabled(T object);
	}
	
	private SafeHtml message = null;
	private Disability<C> disability	= null;

  /**
   * Construct a new {@link SaruActionCell}.
   *
   * @param message the message to display on the button
   * @param delegate the delegate that will handle events
   */
	public SaruActionCell(SafeHtml message, Delegate<C> delegate) {
		super(message, delegate);
		
		this.message = message;
	}


  /**
   * Construct a new {@link SaruActionCell} with a text String that does not contain
   * HTML markup.
   *
   * @param text the text to display on the button
   * @param delegate the delegate that will handle events
   */
	public SaruActionCell(String text, Delegate<C> delegate) {
		super(text, delegate);
		
		this.message = SafeHtmlUtils.fromString(text);
	}	
	
  /**
   * Construct a new {@link SaruActionCell}.
   *
   * @param message the message to display on the button
   * @param delegate the delegate that will handle events
   * @param disability the disability used to get the disability of this button according to the source data
   */
	public SaruActionCell(SafeHtml message, Delegate<C> delegate, Disability<C> disability) {
		super(message, delegate);
		
		this.disability = disability;
		this.message = message;
	}


  /**
   * Construct a new {@link SaruActionCell} with a text String that does not contain
   * HTML markup.
   *
   * @param text the text to display on the button
   * @param delegate the delegate that will handle events
   * @param disability the disability used to get the disability of this button according to the source data
   */
	public SaruActionCell(String text, Delegate<C> delegate, Disability<C> disability) {
		super(text, delegate);
		
		this.disability = disability;
		this.message = SafeHtmlUtils.fromString(text);
	}


	/* (non-Javadoc)
	 * @see com.google.gwt.cell.client.ActionCell#render(com.google.gwt.cell.client.Cell.Context, java.lang.Object, com.google.gwt.safehtml.shared.SafeHtmlBuilder)
	 */
	@Override
	public void render(Context context, C value, SafeHtmlBuilder sb) {
		if(null == this.disability) {
			super.render(context, value, sb);
		}else {
			StringBuffer buffer = new StringBuffer();
			buffer.append("<button type=\"button\" tabindex=\"-1\"");
			
			if(this.disability.isDisabled(value)) {
				buffer.append(" disabled=\"true\"");
			}
			
			buffer.append(">");
			
			SafeHtml btnHTML = new SafeHtmlBuilder().appendHtmlConstant(buffer.toString())
													.append(this.message)
													.appendHtmlConstant("</button>")
													.toSafeHtml();	
			sb.append(btnHTML);
		}
	}

}
