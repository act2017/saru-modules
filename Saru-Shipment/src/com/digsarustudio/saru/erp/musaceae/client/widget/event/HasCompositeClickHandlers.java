/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.event;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * The composite can return the click events to the client code.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface HasCompositeClickHandlers {
	/**
	   * Adds a {@link ClickEvent} handler.
	   * 
	   * @param handler the click handler
	   * @return {@link HandlerRegistration} used to remove this handler
	   */
	  HandlerRegistration addClickHandler(ClickEvent.Handler handler);
}
