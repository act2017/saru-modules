/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.composite.list;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.digsarustudio.saru.erp.musaceae.client.widget.event.ChangeHandler;
import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.DeleteEvent;
import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.DeleteHandler;
import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.HasDeleteHandler;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiChild;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.polymer.paper.widget.PaperFab;

/**
 * This panel handles a widget and a delete button for the user to indicate that this list item should be 
 * removed from the list or not. It is a sub panel of {@link ListPanel}.<br>
 * <br>
 * When the user click delete button, this item panel will be deleted by {@link ListPanel} when a 
 * {@link DeleteHandler#onDelete(com.digsarustudio.musa.widget.polymer.DeleteEvent)} called by this panel.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DeletableListItemPanel<T> extends Composite implements HasDeleteHandler, ListItemPanel<T>{
	Logger logger = Logger.getLogger(this.getClass().getName());
	
	private static DeletableListItemPanelUiBinder uiBinder = GWT.create(DeletableListItemPanelUiBinder.class);

	@SuppressWarnings("rawtypes")
	interface DeletableListItemPanelUiBinder extends UiBinder<Widget, DeletableListItemPanel> {
	}

	@UiField		HTMLPanel		itemPanel;
	@UiField		HorizontalPanel	horizonItemPanel;
	@UiField		VerticalPanel	verticalItemPanel;
	@UiField		PaperFab		deleteBtn;
	
	private DeleteHandler		deleteHandler	= null;
	private List<ChangeHandler>	changeHandlers	= null;

	private T sourceData = null;
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public DeletableListItemPanel() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiHandler("deleteBtn")
	protected void onDeleteButtonClickEvent(ClickEvent event) {
		if(null == this.deleteHandler) {
			return;
		}
		
		this.deleteHandler.onDelete(new DeleteEvent(this.sourceData));
	}

	public void enableDeleteButton() {
		this.deleteBtn.setDisabled(false);
	}
	
	public void disableDeleteButton() {
		this.deleteBtn.setDisabled(true);
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.polymer.HasDeleteHandler#addDeleteHandler(com.digsarustudio.musa.widget.polymer.DeleteHandler)
	 */
	@Override
	public HandlerRegistration addDeleteHandler(final DeleteHandler handler) {
		this.deleteHandler = handler;
		
		return new HandlerRegistration() {
			
			@Override
			public void removeHandler() {
				deleteHandler = null;
			}
		};
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.HasChangeHandlers#addChangeHandler(com.digsarustudio.musa.widget.ChangeHandler)
	 */
	@Override
	public HandlerRegistration addChangeHandler(ChangeHandler handler) {
		if( null == this.changeHandlers ) {
			this.changeHandlers = new ArrayList<>();
		}
		this.changeHandlers.add(handler);
		
		return new HandlerRegistration() {
			
			@Override
			public void removeHandler() {
				changeHandlers.remove(handler);				
			}
		};
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.list.ListItemPanel#getSourceData()
	 */
	@Override
	public T getSourceData() {
		return this.sourceData;
	}

	@UiChild(tagname = "item")
	public void addItem(IsWidget widget) {
		if(!this.itemPanel.isVisible()) {
			this.itemPanel.setVisible(true);
		}
		this.itemPanel.add(widget);
	}
	
	@UiChild(tagname = "horizonItem")
	public void addHorizonItem(IsWidget widget) {
		if(!this.horizonItemPanel.isVisible()) {
			this.horizonItemPanel.setVisible(true);
		}
		
		this.horizonItemPanel.add(widget);
	}
	
	@UiChild(tagname = "verticalItem")
	public void addVerticalItem(IsWidget widget) {
		if(!this.verticalItemPanel.isVisible()) {
			this.verticalItemPanel.setVisible(true);
			this.verticalItemPanel.setSpacing(10);
		}
		
		this.verticalItemPanel.add(widget);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.list.ListItemPanel#setSourceData(java.lang.Object)
	 */
	@Override
	public void setSourceData(T source) {
		this.sourceData = source;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.list.ListItemPanel#getValue()
	 */
	@Override
	public T getValue() {
		return this.sourceData;
	}
	
}
