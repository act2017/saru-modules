/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.polymer;

import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.dialog.ApplyActionFailureEvent;
import com.google.gwt.event.shared.EventHandler;

/**
 * This handler callback to parent when the particular action has been completed.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface ApplyActionFailureHandler extends EventHandler{
	void onApplyActionFailure(ApplyActionFailureEvent event);
}
