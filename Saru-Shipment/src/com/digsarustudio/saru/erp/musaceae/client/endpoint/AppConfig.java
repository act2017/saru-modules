/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.endpoint;

import com.digsarustudio.saru.erp.musaceae.client.factory.AbstractFactoryCollection;
import com.digsarustudio.saru.erp.musaceae.client.mvp.AppLogger;
import com.digsarustudio.saru.erp.musaceae.client.mvp.ModuleLogger;
import com.digsarustudio.saru.erp.musaceae.client.verification.CredentialVerificationCallback;
import com.digsarustudio.saru.erp.musaceae.client.verification.InvalidCredentialException;
import com.digsarustudio.saru.erp.musaceae.client.widget.composite.login.LoginCallback;
import com.digsarustudio.saru.erp.musaceae.client.widget.composite.login.LoginDialogView;
import com.digsarustudio.saru.erp.musaceae.client.widget.composite.login.SimpleLoginDialogPresenter;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * This class stores credential for the user to reuse it as a fundamental config object.<br>
 * This can be wrapped by the client app for storing. some particular data.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public abstract class AppConfig {
	private static final String KEY_CURRENCY_FORMAT		= "CURRENCY_FORMAT";
	private static final String KEY_CREDENTIAL			= "CREDENTIAL";
	
	private static final String DEFAULT_CURRENCY_FORMAT = "##,##0.00";
	
	private LoginDialogView 			loginDialogView 	= null;
	private ModuleLogger 				logger 				= AppLogger.getInstance().get(this.getClass());
	private AbstractFactoryCollection	factoryCollection	= null;
	
	/**
	 * 
	 */
	protected AppConfig() {
	}

	public void init(AbstractFactoryCollection collection) {
		this.setFactoryCollection(collection);
	}
	
	public void setCredentialToken(String credential) {
		if(null == credential) {
			this.removeConfig(KEY_CREDENTIAL);
			return;
		}
		
		this.addConfig(KEY_CREDENTIAL, credential);
	}
	
	public void setCurrencyFormat(String format) {
		if(null == format) {
			this.removeConfig(DEFAULT_CURRENCY_FORMAT);
			return;
		}
		
		this.addConfig(DEFAULT_CURRENCY_FORMAT, format);
	}
	
	public String getCredentialToken() {
		return this.getConfig(KEY_CREDENTIAL);
	}
	
	public String getCurrencyFormat() {
		String format = this.getConfig(KEY_CURRENCY_FORMAT);
		
		if(null == format) {
			format = DEFAULT_CURRENCY_FORMAT;
		}
		
		return format;
	}
	
	public void addConfig(String key, String value) {
		Cookies.setCookie(key, value);
	}
	
	public String getConfig(String key) {
		return Cookies.getCookie(key);
	}
	
	public void removeConfig(String key) {
		Cookies.removeCookie(key);
	}
	
	/**
	 * To verify the user credential to see if it's still valid, otherwise show a login dialog to verify the user again.<br>
	 * A {@link com.digsarustudio.saru.erp.musaceae.client.verification.banana.ship.shared.exception.InvalidCredentialException} will be passed via {@link CredentialVerificationCallback#onFailure(Throwable)} 
	 * if the credential was expired.<br>
	 * 
	 * @param callback The callback object to let client code knows what's happened on the credential verification.
	 * 
	 */
	public void verifyCredential(final CredentialVerificationCallback callback) {
		this.verifyCredential(callback, false);
	}
	
	/**
	 * To show a login dialog.<br>
	 * 
	 * @param callback The callback object tells client code if the user cancelled the login or forgot password.<br>
	 */
	public void showLoginDialog(CredentialVerificationCallback callback) {
		this.openLoginDialog(null, callback);
	}
	
	public void showLoginDialog(String msg, CredentialVerificationCallback callback) {
		this.openLoginDialog(msg, callback);
	}
	
	/**
	 * To verify the user credential to see if it's still valid, otherwise show a login dialog to verify the user again.<br>
	 *  
	 * @param callback The callback object to let client code knows what's happened on the credential verification.
	 * @param isShowLoginDialog True if the client code wants to show a login dialog for the user; 
	 * 							False if the client code wants to handle the {@link com.digsarustudio.saru.erp.musaceae.client.verification.banana.ship.shared.exception.InvalidCredentialException} 
	 * 							passed via  {@link CredentialVerificationCallback#onFailure(Throwable)}.<br>
	 */
	public void verifyCredential(final CredentialVerificationCallback callback, Boolean isShowLoginDialog) {
		this.validateCredential(this.getCredentialToken(), new AsyncCallback<Boolean>() {

			@Override
			public void onFailure(Throwable caught) {
				if(null == callback) {
					return;
				}
				
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess(Boolean result) {
				//True, execute the callback.
				if(result) {
					if(null != callback) {
						callback.onSuccess();
					}
					
				//False, show login dialog if the client code needs to verify the user identity again.
				}else{
					if( !isShowLoginDialog ) {
						callback.onFailure(new InvalidCredentialException("The credential was expired"));
					}else {
						openLoginDialog("The credential is expired. Please login again.", callback);
					}
				}
			}
		});
	}
	
	/**
	 * To open a default login dialog implemented in this module.<br.
	 * If you want to implemented your own dialog, please override this method.<br>
	 * 
	 * @param errMsg
	 * @param callback
	 */
	protected void openLoginDialog(String errMsg, final CredentialVerificationCallback callback) {
		if(null == this.loginDialogView) {
			this.loginDialogView = new LoginDialogView();
		}else {
			this.loginDialogView.clear();
		}
		
		SimpleLoginDialogPresenter presenter = new SimpleLoginDialogPresenter(loginDialogView);
		loginDialogView.setPresenter(presenter);
		
		if(null != errMsg) {
			this.loginDialogView.setHint(errMsg);
		}
		
		presenter.setLoginCallback(new LoginCallback() {
			
			@Override
			public void onLogin(String accountName, String password) {
				onLogIn(accountName, password, new AsyncCallback<Boolean>() {

					@Override
					public void onFailure(Throwable caught) {
						if(null == callback) {
							return;
						}
						
						callback.onFailure(caught);
					}

					@Override
					public void onSuccess(Boolean result) {
						if(null == callback) {
							return;							
						}
						
						callback.onSuccess();
					}
				});
			}
			
			@Override
			public void onForgetPassword(String accountName) {
				logger.info("To resend a password reset link for " + accountName);
				
				if(null == callback) {
					return;
				}
				
				callback.onForgetPassword(accountName);
			}
			
			/* (non-Javadoc)
			 * @see com.digsarustudio.musa.widget.composite.login.LoginCallback#onResetPassword(java.lang.String)
			 */
			@Override
			public void onResetPassword(String accountName) {
				logger.info("To reset the password for " + accountName);
				
				if(null == callback) {
					return;
				}
				
				callback.onResetPassword(accountName);
				
			}

			@Override
			public void onCancel() {
				logger.info("The login has been cancelled");
				
				if(null == callback) {
					return;
				}
				
				callback.onCancel();
			}
		});
		presenter.go();
	}
	
	protected void setFactoryCollection(AbstractFactoryCollection collection) {
		this.factoryCollection = collection;
	}
	
	protected AbstractFactoryCollection getFactoryCollection() {
		return this.factoryCollection;
	}
	
	protected ModuleLogger getLogger() {
		return this.logger;
	}
	
	protected LoginDialogView getLoginDialog() {
		return this.loginDialogView;
	}

	abstract protected void validateCredential(String credential, AsyncCallback<Boolean> callback);
	
	abstract protected void onLogIn(String accountName, String password, final AsyncCallback<Boolean> callback);
}
