/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.datagrid;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.google.gwt.dom.client.Style.Unit;

/**
 * This object contains the details for {@link SaruDataGridColumn} to setup column and events.<br>
 * Some fields can be set in the sub-class if it is necessary.<br>
 * A Builder provided as well.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SaruDataGridColumn<T, S> {
	/**
	 * 
	 * The object builder for {@link SaruDataGridColumn<T, S>}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder<E, R> implements ObjectBuilder<SaruDataGridColumn<E, R>> {
		private SaruDataGridColumn<E, R> result = null;

		public Builder() {
			this.result = new SaruDataGridColumn<E, R>();
		}

		/**
		 * @param handler
		 * @see com.digsarustudio.musa.widget.datagrid.SaruDataGridColumn#setCellValueFetchHandler(com.digsarustudio.musa.widget.datagrid.DataGridCellValueFetchedHanlder)
		 */
		public Builder<E, R> setCellValueFetchHandler(DataGridCellValueFetchedHanlder<E, R> handler) {
			result.setCellValueFetchHandler(handler);
			return this;
		}

		/**
		 * @param type
		 * @see com.digsarustudio.musa.widget.datagrid.SaruDataGridColumn#setType(com.digsarustudio.musa.widget.datagrid.DataGridColumnTypes)
		 */
		public Builder<E, R> setType(DataGridColumnTypes type) {
			result.setType(type);
			return this;
		}

		/**
		 * @param cellCommandHandler
		 * @see com.digsarustudio.musa.widget.datagrid.SaruDataGridColumn#setCellCommandHandler(com.digsarustudio.musa.widget.datagrid.DataGridCellCommandHandler)
		 */
		public Builder<E, R> setCellCommandHandler(DataGridCellCommandHandler<E> cellCommandHandler) {
			result.setCellCommandHandler(cellCommandHandler);
			return this;
		}

		/**
		 * @param header
		 * @see com.digsarustudio.musa.widget.datagrid.SaruDataGridColumn#setHeader(java.lang.String)
		 */
		public Builder<E, R> setHeader(String header) {
			result.setHeader(header);
			return this;
		}

		/**
		 * @param width
		 * @see com.digsarustudio.musa.widget.datagrid.SaruDataGridColumn#setWidth(java.lang.Integer)
		 */
		public Builder<E, R> setWidth(Integer width) {
			result.setWidth(width);
			return this;
		}

		/**
		 * @param unit
		 * @see com.digsarustudio.musa.widget.datagrid.SaruDataGridColumn#setUnit(com.google.gwt.dom.client.Style.Unit)
		 */
		public Builder<E, R> setUnit(Unit unit) {
			result.setUnit(unit);
			return this;
		}

		/**
		 * @param isSortable
		 * @see com.digsarustudio.musa.widget.datagrid.SaruDataGridColumn#setSortable(java.lang.Boolean)
		 */
		public Builder<E, R> setSortable() {
			result.setSortable();
			return this;
		}

		/**
		 * @param cellDisabilityHandler
		 * @see com.digsarustudio.musa.widget.datagrid.SaruDataGridColumn#setCellDisabilityHandler(com.digsarustudio.musa.widget.datagrid.DataGridCellDisabilityHandler)
		 */
		public Builder<E, R> setCellDisabilityHandler(DataGridCellDisabilityHandler<E> cellDisabilityHandler) {
			result.setCellDisabilityHandler(cellDisabilityHandler);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public SaruDataGridColumn<E, R> build() {
			return this.result;
		}

	}
	
	private DataGridCellValueFetchedHanlder<T, S>	cellValueFetchHandler	= null;
	private DataGridCellCommandHandler<T>			cellCommandHandler		= null;
	private DataGridCellDisabilityHandler<T>			cellDisabilityHandler	= null;

	private DataGridColumnTypes	type			= null;
	private	String				header		= "";
	private Integer				width		= 200;
	private Unit					unit			= Unit.PX;
	private Boolean				isSortable	= false;
		
	
	/**
	 * To support new keyword
	 */
	public SaruDataGridColumn() {
	}

	public void setCellValueFetchHandler(DataGridCellValueFetchedHanlder<T, S> handler) {
		this.cellValueFetchHandler = handler;
	}

	/**
	 * @return the type
	 */
	public DataGridColumnTypes getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(DataGridColumnTypes type) {
		this.type = type;
	}

	/**
	 * @return the cellCommandHandler
	 */
	public DataGridCellCommandHandler<T> getCellCommandHandler() {
		return cellCommandHandler;
	}

	/**
	 * @param cellCommandHandler the cellCommandHandler to set
	 */
	public void setCellCommandHandler(DataGridCellCommandHandler<T> cellCommandHandler) {
		this.cellCommandHandler = cellCommandHandler;
	}

	/**
	 * @return the header
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * @param header the header to set
	 */
	public void setHeader(String header) {
		this.header = header;
	}

	/**
	 * @return the width
	 */
	public Integer getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(Integer width) {
		this.width = width;
	}

	/**
	 * @return the unit
	 */
	public Unit getUnit() {
		return unit;
	}

	/**
	 * @param unit the unit to set
	 */
	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	/**
	 * @return the cellValueFetchHandler
	 */
	public DataGridCellValueFetchedHanlder<T, S> getCellValueFetchHandler() {
		return cellValueFetchHandler;
	}	

	/**
	 * @return the isSortable
	 */
	public Boolean isSortable() {
		return isSortable;
	}

	/**
	 * @param isSortable the isSortable to set
	 */
	public void setSortable() {
		this.isSortable = true;
	}

	/**
	 * @return the cellDisabilityHandler
	 */
	public DataGridCellDisabilityHandler<T> getCellDisabilityHandler() {
		return cellDisabilityHandler;
	}

	/**
	 * @param cellDisabilityHandler the cellDisabilityHandler to set
	 */
	public void setCellDisabilityHandler(DataGridCellDisabilityHandler<T> cellDisabilityHandler) {
		this.cellDisabilityHandler = cellDisabilityHandler;
	}

	/*
	 * To create a new builder
	 */
	public static <T, S> Builder<T, S> builder() {
		return new Builder<T, S>();
	}
}
