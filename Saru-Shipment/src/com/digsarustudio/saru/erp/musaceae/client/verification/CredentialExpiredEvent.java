/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.verification;

import com.digsarustudio.musa.mvp.event.EventBusEvent;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.EventHandler;

/**
 * This event indicates the credential is expired.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public class CredentialExpiredEvent extends EventBusEvent<CredentialExpiredEvent.Handler> {
	/**
	 * 
	 * Event handler to handle CredentialExpiredEvent
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public interface Handler extends EventHandler {
		/*
		 * To handle CredentialExpiredEvent
		 */
		void onCredentialExpired(CredentialExpiredEvent event);
	}

	public static Type<Handler> TYPE = new Type<>();
	
	private CredentialVerificationCallback	credentialVerificationCallback	= null;
	private String							errorMsg							= null;
	
	public void setCredentialVerificationCallback(CredentialVerificationCallback	callback) {
		this.credentialVerificationCallback = callback;
	}
	
	public CredentialVerificationCallback getCredentialVerificationCallback() {
		return this.credentialVerificationCallback;
	}

	/**
	 * @return the error message
	 */
	public String getErrorMessage() {
		return errorMsg;
	}

	/**
	 * @param msg the msg to set
	 */
	public void setErrorMessage(String msg) {
		this.errorMsg = msg;
	}

	/**
	 * To register this event into an event bus
	 * 
	 * @param eventbus The event bus to register
	 * 
	 * @param handler The event handler of this event
	 */
	public static void register(EventBus eventbus, CredentialExpiredEvent.Handler handler) {
		eventbus.addHandler(TYPE, handler);
	}

	/**
	 * To fire event with event bus
	 * 
	 * @param eventBus The event handler between event sender and receiver
	 * @param others The other parameters if needs.
	 */
	public static void fireEvent(EventBus eventBus, CredentialVerificationCallback callback) {
		CredentialExpiredEvent event = new CredentialExpiredEvent();
		event.setCredentialVerificationCallback(callback);

		eventBus.fireEvent(event);
	}
	
	/**
	 * To fire event with event bus
	 * 
	 * @param eventBus The event handler between event sender and receiver
	 * @param errorMsg The error message shown on the login dialog
	 * @param callback The callback to notify client code the result of credential verification
	 * @param others The other parameters if needs.
	 */
	public static void fireEvent(EventBus eventBus, String errorMsg, CredentialVerificationCallback callback) {
		CredentialValidationFailureEvent event = new CredentialValidationFailureEvent();
		event.setCredentialVerificationCallback(callback);
		event.setErrorMessage(errorMsg);

		eventBus.fireEvent(event);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<Handler> getAssociatedType() {
		return TYPE;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(Handler handler) {
		handler.onCredentialExpired(this);
	}
}
