/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.polymer;

import com.google.gwt.event.shared.GwtEvent;

/**
 * The event indicates an item is going to be deleted from a list.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DeleteEvent extends GwtEvent<DeleteHandler> {
	public static Type<DeleteHandler> TYPE = new Type<>();
	
	private Object sourceData = null;
	
	
	/**
	 * 
	 */
	public DeleteEvent(Object source) {
		this.sourceData = source;
	}

	/**
	 * @return the sourceData
	 */
	public Object getSourceData() {
		return sourceData;
	}

	/**
	 * @param sourceData the sourceData to set
	 */
	public void setSourceData(Object sourceData) {
		this.sourceData = sourceData;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<DeleteHandler> getAssociatedType() {
		return TYPE;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(DeleteHandler handler) {
		handler.onDelete(this);

	}

}
