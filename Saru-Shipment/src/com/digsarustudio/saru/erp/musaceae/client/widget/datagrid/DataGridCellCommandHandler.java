/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.datagrid;

/**
 * This command handler handles the event from the button cell, text cell, or image cell.<br>
 *  
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface DataGridCellCommandHandler<T> {
	void execute(T source);
}
