/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.history.state;

import java.util.HashMap;
import java.util.Map;

import com.digsarustudio.banana.utils.InvalidTokenFormatException;
import com.digsarustudio.saru.erp.musaceae.client.mvp.history.HistoryToken;
import com.digsarustudio.saru.erp.musaceae.client.mvp.history.HistoryTokenCollection;
import com.digsarustudio.saru.erp.musaceae.client.mvp.history.HistoryTokenSet;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.History;

/**
 * This manager handles the changing of history state to dispatch the states to the correspondent controller.
 * Please fire the changing of state in the HisotryState object.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public class HistoryStateBus {
	private static class HistoryStateSource implements  HasHistoryStateHandlers
														, ValueChangeHandler<String>{
		//A map for this manager to store the target handler to execute a state change event 
		private Map< String, HistoryStateHandler> historyStatesChangeHandler = new HashMap<>();		
		
		public HistoryStateSource(){
			History.addValueChangeHandler(this);
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.mvp.historystate.HasHistoryStateHandlers#addHistoryStateHandler(java.lang.String, com.digsarustudio.musa.mvp.history.HistoryStateHandler)
		 */
		@Override
		public HandlerRegistration addHistoryStateHandler(final String state, HistoryStateHandler handler) {
			if( null == state || state.isEmpty() ){
				throw new NullPointerException("The page must be set");
			}
				
			if(null == handler){
				throw new NullPointerException("The handler must be set");
			}
			
			this.historyStatesChangeHandler.put(state, handler);
	
			return new HandlerRegistration() {
				
				@Override
				public void removeHandler() {
					historyStatesChangeHandler.remove(state);
				}
			};
		}

		/* To register the event handler for the history in order to be notified while any token value changed,
		 * whatever who the owner of the token is.
		 * 
		 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
		 */
		@Override
		public void onValueChange(ValueChangeEvent<String> event) {
			//To decode the parameters which encoded from the server side
			String rawToken = URL.decodeQueryString(event.getValue());

			try {
				HistoryTokenCollection tokenCollection = this.getTokenSet(rawToken);
				this.dispatchState(tokenCollection);
			} catch (InvalidTokenFormatException e) {
				//Do nothing because we don't have to deal with the the wrong token, just skip it.
				return;
			}
		}
		
		/**
		 * To send a notification to the browser to change the current state
		 * 
		 * @param state The state to change
		 */
		public void fireState(HistoryState state) {
			HistoryTokenCollection collection = state.getTokens();
			History.newItem(collection.toString());
		}

		/**
		 * Assigns a state to the browser, but do not to execute it.
		 * 
		 * @param state The state to set
		 */
		public void setState(HistoryState state) {
			HistoryTokenCollection collection = state.getTokens();
			History.newItem(collection.toString(), false);
		}
		
		/**
		 * Returns current state of history
		 * 
		 * @return The current history token collection
		 * @throws InvalidTokenFormatException When the token is not valid.
		 */
		public HistoryTokenCollection getCurrentState() throws InvalidTokenFormatException{
			String currentToken = History.getToken();
			
			return new HistoryTokenSet(currentToken);	
		}
		
		/**
		 * To check the current token has been set or not.
		 * If the token is not set, showing the main page. Otherwise showing page executed in current state.
		 * 
		 * @throws EmptyHistoryStateException When the incoming state is empty.
		 */
		public void checkCurrentState() throws EmptyHistoryStateException{
			if( History.getToken().equals("") ){
				throw new EmptyHistoryStateException();
			}else{
				History.fireCurrentHistoryState();
			}			
		}

		private HistoryTokenCollection getTokenSet(String token) throws InvalidTokenFormatException{
			return new HistoryTokenSet(token);					
		}
		
		private void dispatchState(HistoryTokenCollection collection){
			HistoryToken token = collection.getToken(HistoryState.TOKEN_STATE);
			if(null == token){
				return;
			}
			
			HistoryStateHandler handler = this.getHandler(token.getValue());
			if(null == handler){
				return;
			}
			
			handler.execute(collection);
		}
		
		private HistoryStateHandler getHandler(String page){
			if( !this.historyStatesChangeHandler.containsKey(page) ){
				return null;
			}
			
			return this.historyStatesChangeHandler.get(page);
		}
	}	
	
	/**
	 * The instance dealing with the operation of history
	 */
	private static final HistoryStateSource source = new HistoryStateSource();

	/**
	 * To append a handler of history state for the notification while the history changed.
	 * 
	 * @param state The token of the target state
	 * @param handler The handler while the target history state occurred
	 * 
	 * @return The handler registration object for the deregister.
	 * @see com.digsarustudio.musa.mvp.history.state.HistoryStateBus.HistoryStateSource#addHistoryStateHandler(java.lang.String, com.digsarustudio.musa.mvp.history.HistoryStateHandler)
	 */
	public static HandlerRegistration addHistoryStateHandler(String state, HistoryStateHandler handler) {
		return source.addHistoryStateHandler(state, handler);
	}

	/**
	 * @param state
	 * @see com.digsarustudio.musa.mvp.history.state.HistoryStateBus.HistoryStateSource#fireState(com.digsarustudio.musa.mvp.history.state.HistoryState)
	 */
	public static void fireState(HistoryState state) {
		source.fireState(state);
	}

	/**
	 * @param state
	 * @see com.digsarustudio.musa.mvp.history.state.HistoryStateBus.HistoryStateSource#setState(com.digsarustudio.musa.mvp.history.state.HistoryState)
	 */
	public static void setState(HistoryState state) {
		source.setState(state);
	}

	/**
	 * Returns current state of history
	 * 
	 * @return The token collection of history
	 * @throws InvalidTokenFormatException
	 * @see com.digsarustudio.musa.mvp.history.state.HistoryStateBus.HistoryStateSource#getCurrentState()
	 */
	public static HistoryTokenCollection getCurrentState() throws InvalidTokenFormatException {
		return source.getCurrentState();
	}

	/**
	 * To check the current token has been set or not.
	 * If the token is not set, showing the main page. Otherwise showing page executed in current state.
	 * 
	 * @throws EmptyHistoryStateException When the incoming state is empty. 
	 * 
	 * @see com.digsarustudio.musa.mvp.history.state.HistoryStateBus.HistoryStateSource#checkCurrentState()
	 */
	public static void checkCurrentState() throws EmptyHistoryStateException {
		source.checkCurrentState();
	} 	
	
}
