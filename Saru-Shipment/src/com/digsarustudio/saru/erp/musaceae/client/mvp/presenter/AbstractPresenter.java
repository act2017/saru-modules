/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.presenter;

import com.digsarustudio.saru.erp.musaceae.client.mvp.view.PassiveView;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.HasWidgets;

/**
 * The boilerplate of presenter
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.1
 * @since		1.0.0
 *
 */
public abstract class AbstractPresenter <T extends PassiveView> implements Presenter {
	private		PassiveView view		= null;
	protected	EventBus 	eventBus	= null;
	
	/**
	 * Construct the presenter object
	 * 
	 * @param eventBus The event handler manager
	 * @param model The data model
	 * @param view The view for display
	 * 
	 * @since 1.0.1
	 */ 
	public AbstractPresenter(EventBus eventBus, PassiveView view) {
		this.eventBus = eventBus;
		this.view = view;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.tutorial.gwt.twoeight.shared.mvp.presenter.Presenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(this.view.asWidget());
		
		this.init();
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.mvp.presenter.Presenter#go()
	 */
	@Override
	public void go() {		
		this.init();
	}
	
	protected EventBus getEventBus() {
		return this.eventBus;
	}
	
	/**
	 * 
	 * @return
	 * 
	 * @deprecated	Please use {@link #getDisplay()} instead
	 */
	protected T getView() {
		return (T) this.view;
	}
	
	@SuppressWarnings("unchecked")
	protected T getDisplay() {
		return (T) this.view;
	}

	/**
	 * Initialize this presenter
	 */
	protected abstract void init();
}
