/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget;

/**
 * The alignment style of a widget.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public enum WidgetAlignment {
	VerticalAlignment
	,HorizontalAlignment
	;
}
