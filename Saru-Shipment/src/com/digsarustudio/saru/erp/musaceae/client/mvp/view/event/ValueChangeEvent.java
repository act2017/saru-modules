/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.view.event;

/**
 * This event indicates the value of a widget has been changed and handles by {@link ValueChangeEventHandler}.<br>
 *
 * @param T The data type to carry.<br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 * @deprecated	1.0.3	Please see the ReadMe file and use the event from com.digsarustudio.saru.erp.musaceae.client.widget.event
 *
 */
public class ValueChangeEvent<T> extends ViewEvent<T>{
	
	public ValueChangeEvent(T source) {
		this.setSourceData(source);
	}	
}
