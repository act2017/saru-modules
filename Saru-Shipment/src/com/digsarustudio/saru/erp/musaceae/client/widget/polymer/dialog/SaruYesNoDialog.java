/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.polymer.dialog;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.polymer.iron.widget.IronIcon;
import com.vaadin.polymer.paper.widget.PaperButton;
import com.vaadin.polymer.paper.widget.PaperDialog;

/**
 * This dialog wraps {@link PaperDialog}.
 * 
 * Bug: When this widget has been used in more than 2 view, the second opened one cannot see the content in this dialog.<br>
 *      Failed in Chorme, but works fine in FireFox.<br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SaruYesNoDialog extends Composite implements HasYesNoDialogClickHandler{

	private static SaruYesNoDialogUiBinder uiBinder = GWT.create(SaruYesNoDialogUiBinder.class);

	interface SaruYesNoDialogUiBinder extends UiBinder<Widget, SaruYesNoDialog> {
	}

	@UiField PaperDialog		dialog;
	@UiField IronIcon			infoIcon;
	@UiField IronIcon			warningIcon;
	@UiField IronIcon			errorIcon;
	@UiField Label				msg;
	@UiField PaperButton		cancelBtn;
	@UiField PaperButton		applyBtn;

	private HandlerRegistration	yesActionHandlerRegistration	= null;
	private HandlerRegistration	noActionHandlerRegistration		= null;
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public SaruYesNoDialog() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public void setInfoMessage(String msg) {
		this.infoIcon.setVisible(true);
		this.warningIcon.setVisible(false);
		this.errorIcon.setVisible(false);
		
		this.msg.setText(msg);
	}
	
	public void setWarningMessage(String msg) {
		this.infoIcon.setVisible(false);
		this.warningIcon.setVisible(true);
		this.errorIcon.setVisible(false);
		
		this.msg.setText(msg);
	}
	
	public void setErrorMessage(String msg) {
		this.infoIcon.setVisible(false);
		this.warningIcon.setVisible(false);
		this.errorIcon.setVisible(true);
		
		this.msg.setText(msg);
	}
	
	/**
	 * 
	 * @see com.vaadin.polymer.paper.widget.PaperDialog#open()
	 */
	public void open() {
		dialog.open();
	}

	/**
	 * 
	 * @see com.vaadin.polymer.paper.widget.PaperDialog#center()
	 */
	public void center() {
		dialog.center();
	}

	/**
	 * 
	 * @see com.vaadin.polymer.paper.widget.PaperDialog#close()
	 */
	public void close() {
		dialog.close();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.polymer.dialog.HasYesNoDialogClickHandler#addYesClickHandler(com.google.gwt.event.dom.client.ClickHandler)
	 */
	@Override
	public HandlerRegistration addYesClickHandler(ClickHandler handler) {
		if(null != this.yesActionHandlerRegistration) {
			this.yesActionHandlerRegistration.removeHandler();			
		}
		
		this.yesActionHandlerRegistration = this.applyBtn.addClickHandler(handler);
		return this.yesActionHandlerRegistration;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.polymer.dialog.HasYesNoDialogClickHandler#addNoClickHandler(com.google.gwt.event.dom.client.ClickHandler)
	 */
	@Override
	public HandlerRegistration addNoClickHandler(ClickHandler handler) {
		if(null != this.noActionHandlerRegistration) {
			this.noActionHandlerRegistration.removeHandler();
		}
		
		this.noActionHandlerRegistration = this.cancelBtn.addClickHandler(handler);
		return this.noActionHandlerRegistration;
	}

}
