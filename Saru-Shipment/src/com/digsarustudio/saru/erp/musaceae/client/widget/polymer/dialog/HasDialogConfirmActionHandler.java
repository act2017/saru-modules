/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.polymer.dialog;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * The sub-type handles {@link DialogConfirmActionHandler} to deal with the event from dialog.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public interface HasDialogConfirmActionHandler {
	HandlerRegistration addDialogConfirmActionHandler(DialogConfirmActionHandler handler);
}
