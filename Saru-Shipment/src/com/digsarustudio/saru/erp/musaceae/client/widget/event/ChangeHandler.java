/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * This event handler handles {@link CompositeChangeEvent} when the value or status has been changed in the sub widget.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface ChangeHandler extends EventHandler {
	void onChange(ChangeEvent event);
}
