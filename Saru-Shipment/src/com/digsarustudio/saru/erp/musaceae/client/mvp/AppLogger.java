/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp;

import java.util.HashMap;
import java.util.Map;

/**
 * The logger for the front app to output a log on the browser console.<br> 
 * 
 * How to use:<br>
 * 	1. Add the following snippet in to the module configuration file (*.gwt.xml): <br>
 *   	<inherits name="com.google.gwt.logging.Logging"/> <br>
 *   	<!-- Setup properties of logger --> <br>
 *   	<!-- <set-property name="gwt.logging.logLevel" value="INFO"/>  --> <br>          
 *   	<set-property name="gwt.logging.enabled" value="TRUE"/> <br>
 *	<br>
 *  2. And inherit MVP module as following: <br>
 *  	<inherits name='com.digsarustudio.saru.erp.musaceae.Module_Musaceae'/> <br>
 *  
 *  
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AppLogger {
	private static AppLogger INSTANCE = null;

	private Map<Class<?>, ModuleLogger> loggers = null;
	
	/**
	 * 
	 */
	private AppLogger() {
	}

	public static AppLogger getInstance() {
		if(null == INSTANCE) {
			INSTANCE = new AppLogger();
			INSTANCE.init();
		}
		
		return INSTANCE;
	}
	
	public ModuleLogger get(Class<?> module) {
		ModuleLogger logger = null;
		if( !this.loggers.containsKey(module) ) {
			logger = new ModuleLogger(module.getName());
		}else {
			logger = this.loggers.get(module);
		}
		
		return logger;
	}
	
	public void info(Class<?> module, String msg) {
		ModuleLogger logger = this.get(module);
		logger.info(msg);
	}
	
	public void warning(Class<?> module, String msg) {
		ModuleLogger logger = this.get(module);
		logger.warning(msg);
	}
	
	public void severe(Class<?> module, String msg) {
		ModuleLogger logger = this.get(module);
		logger.severe(msg);
	}
	
	public void severe(Class<?> module, String msg, Throwable cause) {
		ModuleLogger logger = this.get(module);
		logger.severe(msg, cause);
	}
	
	private void init() {
		this.loggers = new HashMap<>();
	}
}
