/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.event;

import com.google.gwt.user.client.ui.Widget;

/**
 * The sub-type has the ability to contain the event source object or source data.
 * 
 * This interface is used to instead DOM event because the source object gotten from DOM Event cannot be casted as Composite widget.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface CompositeEvent {
	/**
	 * Assign a source widget for this event
	 * 
	 * @param widget The source widget
	 */
	void setSourceWidget(Widget widget);
	
	/**
	 * Returns the source widget put in this event
	 * 
	 * @return the source widget put in this event
	 */
	Widget getSourceWidget();

	/**
	 * Assign a source data for this event
	 * 
	 * @param data The source data to pass to other client code
	 */
	void setSourceData(Object data);
	
	/**
	 * Returns the source data put in this event
	 * 
	 * @return the source data put in this event
	 */
	Object getSourceData();
}
