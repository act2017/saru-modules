/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget;

/**
 * The sub-type can be set and retrieved with URL.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface HasUrl {
	void setUrl(String url);
	String getUrl();
}
