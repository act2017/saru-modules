/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.presenter;

import com.digsarustudio.saru.erp.musaceae.client.endpoint.AbstractEndpointSetFactory;
import com.digsarustudio.saru.erp.musaceae.client.factory.AbstractDataModelBuilderSetFactory;
import com.digsarustudio.saru.erp.musaceae.client.factory.AbstractFactoryCollection;
import com.digsarustudio.saru.erp.musaceae.client.mvp.view.SupervisingView;
import com.google.gwt.event.shared.EventBus;

/**
 * The base class of {@link FactoryPresenter}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public abstract class BaseFactoryPresenter<T extends SupervisingView> extends AbstractPresenter<T> implements FactoryPresenter {
	
	private AbstractFactoryCollection	factoryCollection = null;
	
	/**
	 * Creates a {@link BaseFactoryPresenter}
	 * 
	 * @param eventBus The event handler
	 * @param view The view to control
	 */
	public BaseFactoryPresenter(EventBus eventBus, SupervisingView view) {
		super(eventBus, view);
	}
	
	public BaseFactoryPresenter(EventBus eventBus, SupervisingView view, AbstractFactoryCollection factory) {
		super(eventBus, view);
		this.setFactoryCollection(factory);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.mvp.presenter.FactoryPresenter#setFactoryCollection(com.digsarustudio.musa.mvp.AbstractFactoryCollection)
	 */
	@Override
	public void setFactoryCollection(AbstractFactoryCollection factoryCollection) {
		this.factoryCollection = factoryCollection;
		
	}

	protected AbstractFactoryCollection getFactoryCollection() {
		return this.factoryCollection;
	}
	
	protected AbstractEndpointSetFactory getEndpointSetFactory() {
		return this.factoryCollection.getEndpointSetFactory();
	}
	
	protected AbstractDataModelBuilderSetFactory getDataModelBuilderSetFactory() {
		return this.factoryCollection.getDataModelBuilderSetFactory();
	}
}
