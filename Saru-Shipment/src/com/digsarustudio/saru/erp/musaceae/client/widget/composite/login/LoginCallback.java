/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.composite.login;

/**
 * The callback interface for the client code to handle login widget.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface LoginCallback {
	void onLogin(String accountName, String password);
	void onCancel();
	void onForgetPassword(String accountName);
	void onResetPassword(String accountName);
}
