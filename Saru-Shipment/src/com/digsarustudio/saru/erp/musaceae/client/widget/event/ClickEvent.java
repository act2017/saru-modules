/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.user.client.ui.Composite;

/**
 * The event for the Widgets and Composites to carry message to the receiver.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ClickEvent extends AbstractCompositeEvent<ClickEvent.Handler> {
	/**
	 * 
	 * Event handler of listing products
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public interface Handler extends EventHandler{
		/*
		 * On clicked
		 */
		void onClick(ClickEvent event);
	}
		
	public static Type<Handler> TYPE = new Type<>();			
	
	/**
	 * Constructs a click event 
	 */
	public ClickEvent() {
		super();
	}

	/**
	 * Constructs a click event with the source data
	 * 
	 * @param sourceData The click event with the source data
	 */
	public ClickEvent(Object sourceData) {
		super(sourceData);
	}

	/**
	 * Constructs a click event with the source widget
	 * 
	 * @param sourceWidget The click event with the source widget
	 */
	public ClickEvent(Composite sourceWidget) {
		super(sourceWidget);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<Handler> getAssociatedType() {
		return TYPE;
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(Handler handler) {
		handler.onClick(this);
	}
}
