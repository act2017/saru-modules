/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.verification;

/**
 * The sub-type tells the client code what's happened on the credential verification
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface CredentialVerificationCallback {
	void onSuccess();
	void onCancel();
	void onForgetPassword(String accountName);
	void onResetPassword(String accountName);
	void onFailure(Throwable caught);
}
