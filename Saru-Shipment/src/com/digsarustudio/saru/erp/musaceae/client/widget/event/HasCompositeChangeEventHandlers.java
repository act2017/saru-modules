/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.event;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * A widget that implements this interface provides registration for
 * {@link com.digsarustudio.frontend.togautogroup.client.ui.widget.event.ChangeEventHandler} instances.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface HasCompositeChangeEventHandlers {
	/**
	   * Adds a {@link CompositeChangeEvent} handler.
	   * 
	   * @param handler the click handler
	   * @return {@link HandlerRegistration} used to remove this handler
	   */
	  HandlerRegistration addChangeHandler(CompositeChangeEvent.Handler handler);
}
