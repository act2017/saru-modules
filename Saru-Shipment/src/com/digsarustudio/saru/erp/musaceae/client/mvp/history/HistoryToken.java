/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.history;

import com.digsarustudio.banana.utils.InvalidTokenFormatException;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * This token represents the status of history.  
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public class HistoryToken{
	public static class Builder implements ObjectBuilder<HistoryToken>{
		private String key = null;
		private String value = null;
		
		public Builder(String key){
			this.key = key;
		}
		
		/**
		 * @return the key
		 */
		private String getKey() {
			return key;
		}

		/**
		 * @param key the key to set
		 */
		public Builder setKey(String key) {
			this.key = key;
			return this;
		}

		/**
		 * @return the value
		 */
		private String getValue() {
			return value;
		}

		/**
		 * @param value the value to set
		 */
		public Builder setValue(String value) {
			this.value = value;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.cakemaker.IBuilder#build()
		 */
		@Override
		public HistoryToken build() {
			return new HistoryToken(this);
		}		
	}
	
	
	protected final String BINDER		= "=";
	protected final String LEFT_BRACKET	= "<";
	protected final String RIGHT_BRACKET= ">";
	
	private String key;
	private String value;
	
	public HistoryToken(String token) throws InvalidTokenFormatException{
		this.parse(token);
	}
	
	private HistoryToken(Builder builder){
		this.key	= builder.getKey();
		this.value	= builder.getValue();
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	public static Builder builder(String key){
		return new Builder(key);
	}

	@Override
	public String toString(){
		StringBuilder builder = new StringBuilder();
		
		builder.append(this.key);
		
		if(null != this.value){
			builder.append(this.BINDER);
			builder.append(this.LEFT_BRACKET);
			builder.append(this.value);
			builder.append(this.RIGHT_BRACKET);
		}
		
		return builder.toString();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoryToken other = (HistoryToken) obj;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	private void parse(String token) throws InvalidTokenFormatException{
		String[] keyVal = token.split(this.BINDER);
		
		if(keyVal.length < 1){
			throw new InvalidTokenFormatException(token + " is not a valid token string.");
		}		
		this.key = keyVal[0];
		
		if(keyVal.length < 2){
			return;
		}
		int start = keyVal[1].indexOf(this.LEFT_BRACKET);
		int end = keyVal[1].lastIndexOf(this.RIGHT_BRACKET);
		
		if( start != 0 ){
			throw new InvalidTokenFormatException();
		}
		
		if( end != (keyVal[1].length() - 1) ){
			throw new InvalidTokenFormatException();
		}		
		
		this.value = keyVal[1].substring(start+1, end);
	}
}
