/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.history;

/**
 * The collection of tokens which could be a set{@link HistoryTokenSet}.
 *  
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface HistoryTokenCollection extends Iterable<HistoryToken>  {
	/**
	 * 
	 * @return
	 * @since	1.0.0
	 */
	HistoryToken getToken(String key);
	
	/**
	 * 
	 * @since	1.0.0
	 */
	void addToken(HistoryToken token);
	
	/**
	 * 
	 * @since	1.0.0
	 */
	void removeToken(String key);
	
	/**
	 * 
	 * @since	1.0.0
	 */
	void removeToken(HistoryToken token);
}
