/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.presenter;

import com.digsarustudio.saru.erp.musaceae.client.factory.AbstractFactoryCollection;

/**
 * The sub-type can access an abstract factory and use it to generate a particular data model for the 
 * view, presenter, and endpoint.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface FactoryPresenter extends Presenter{								
	void setFactoryCollection(AbstractFactoryCollection factoryCollection);
}
