/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.presenter;

import com.google.gwt.user.client.ui.HasWidgets;

/**
 * The definitions of presenter
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Presenter {
	/**
	 * Representing the target view on the incoming layout panel
	 * 
	 * @param container The incoming layout panel
	 */
	void go(HasWidgets container);
	
	/**
	 * Representing the target view as an independent view.
	 */
	void go();
}
