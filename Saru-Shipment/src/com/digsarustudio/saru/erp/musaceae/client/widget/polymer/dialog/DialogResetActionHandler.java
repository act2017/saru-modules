/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.polymer.dialog;

import com.google.gwt.event.shared.EventHandler;

/**
 * This handler is used for the presenter of a {@link SaruDialog} to receive a reset action event.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface DialogResetActionHandler extends EventHandler{
	void onReset();
}
