/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.history.state;

import java.util.NoSuchElementException;
import java.util.logging.Logger;

import com.digsarustudio.banana.utils.InvalidTokenFormatException;
import com.digsarustudio.saru.erp.musaceae.client.factory.AbstractDataModelBuilderSetFactory;
import com.digsarustudio.saru.erp.musaceae.client.mvp.history.HistoryToken;
import com.digsarustudio.saru.erp.musaceae.client.mvp.history.HistoryTokenCollection;
import com.digsarustudio.saru.erp.musaceae.client.mvp.history.HistoryTokenSet;

/**
 * This state contains general data for the sub state
 * The tokens can be appended via addToken() for the getTokens().
 * 
 * If the {@link HistoryStateBus} is not the only one, refactor the constructor to receive the right history state bus.
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public abstract class AbstractHistoryState implements HistoryState {
	Logger logger = Logger.getLogger(this.getClass().getName());
	/**
	 * The name of this state
	 */
	private String stateName = null;
	
	/**
	 * The collection of history token
	 */
	protected HistoryTokenCollection tokens = null;
	
	protected AbstractDataModelBuilderSetFactory	modelBuilderSetFactory = null;
	
	/**
	 * Constructs a history state by name
	 * 
	 * @param name The name of this state
	 */
	public AbstractHistoryState(String name) {
		this.stateName = name;
		this.addToken(HistoryToken.builder(HistoryState.TOKEN_STATE)
								  .setValue(name)
								  .build());
	}
	
	/**
	 * Constructs a history state by name
	 * 
	 * @param name The name of this state
	 * @param tokens The token stack for this state
	 */
	public AbstractHistoryState(String name, HistoryTokenCollection tokens) {
		this(name);
		this.parse(tokens);
	}
	
	/**
	 * Constructs a history state by name
	 * 
	 * @param name The name of this state
	 * @param tokens The token stack for this state
	 */
	public AbstractHistoryState(String name, HistoryTokenCollection tokens, AbstractDataModelBuilderSetFactory factory) {
		this(name);
		this.setDataModelBuilderSetFactory(factory);
		this.parse(tokens);
	}
	
	/**
	 * Constructs a history state by name
	 * 
	 * @param name The name of this state
	 * @param tokenString The string contains token
	 * @throws InvalidTokenFormatException 
	 */
	public AbstractHistoryState(String name, String tokenString, AbstractDataModelBuilderSetFactory factory) throws InvalidTokenFormatException {
		this(name);
		this.setDataModelBuilderSetFactory(factory);
		this.parse(tokenString);
	}	

	/* (non-Javadoc)
	 * @see com.digsarustudio.frontend.togautogroup.client.historystate.HistoryState#getStateName()
	 */
	@Override
	public String getStateName() {
		return this.stateName;
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.mvp.historystate.HistoryState#fireState(com.digsarustudio.musa.mvp.historystate.HistoryState)
	 */
	@Override
	public void fireState() {
		HistoryStateBus.fireState(this);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.mvp.historystate.HistoryState#setState(com.digsarustudio.musa.mvp.historystate.HistoryState)
	 */
	@Override
	public void setState() {
		HistoryStateBus.setState(this);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.mvp.history.state.HistoryState#setDataModelBuilderFactory(com.digsarustudio.musa.mvp.AbstractDataModelBuilderFactory)
	 */
	@Override
	public void setDataModelBuilderSetFactory(AbstractDataModelBuilderSetFactory factory) {
		this.modelBuilderSetFactory = factory;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.mvp.history.state.HistoryState#getTokens()
	 */
	@Override
	public HistoryTokenCollection getTokens() {
		return this.tokens;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((stateName == null) ? 0 : stateName.hashCode());
		
		for (HistoryToken token : this.tokens) {
			result = prime * result + ((token == null) ? 0 : token.hashCode());
		}
		
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractHistoryState other = (AbstractHistoryState) obj;
		if (stateName == null) {
			if (other.stateName != null)
				return false;
		} else if (!stateName.equals(other.stateName))
			return false;
		
		
		
		return true;
	}

	/* (non-Javadoc)
	 * 
	 * Returns a formatted token string
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return  this.tokens.toString();
	}

	/**
	 * Return true if the value is not null and empty string for the key which needs to carry a value for this state, otherwise false returned.
	 * 
	 * @param value The value to check
	 * 
	 * @return true if the value is not null and empty string for the key which needs to carry a value for this state, otherwise false returned.
	 */
	protected Boolean isValidPresentValue(String value) {
		if(null == value){
			return false;
		}
		
		if(value.isEmpty()){
			return false;
		}
		
		return true;
	}

	/**
	 * Returns true if the incoming state name is not null and does equal to the name of this state, otherwise false returned.
	 * 
	 * @param stateName The name of incoming state
	 * 
	 * @return true if the incoming state name is not null and does equal to the name of this state, otherwise false returned.
	 */
	protected Boolean isValidIncomingStateName(String stateName) {
		if( null == stateName ){
			return false;
		}
			
		if( !stateName.equals(this.stateName)) {
			return false;
		}
			
		return true;
	}
	
	/**
	 * To append a parsed history token to this state.
	 * 
	 * @param token a parsed history token
	 */
	protected void addToken(HistoryToken token) {
		if(null == this.tokens){
			this.tokens = this.createTokenCollection();
		}
		
		this.tokens.addToken(token);
	}
	
	/**
	 * Returns a history token of the specific token name
	 * 
	 * @param tokenName The name of token to retrieve
	 * 
	 * @return The history token of the specific token name
	 * 
	 * @throws NoSuchElementException if the target token cannot be found.
	 */
	protected HistoryToken getToken(String tokenName) {
		HistoryToken token = this.tokens.getToken(tokenName);
		
		if(null == token){
			throw new NoSuchElementException("No such value for [" + tokenName +"]");
		}
		
		return token;
	}
	
	/**
	 * To validate the no value contained token and return it as the result
	 * 
	 * @param tokens The collection of tokens
	 * 
	 * @return The validated token
	 */
	protected HistoryToken validateToken(HistoryTokenCollection tokens, String key) {
		return this.validateToken(tokens, key, false);
	}
	
	/**
	 * To validate the value contained token and return it as the result
	 * 
	 * @param tokens The collection of tokens
	 * 
	 * @return The validated token
	 */
	protected HistoryToken validateToken(HistoryTokenCollection tokens, String key, Boolean validateValueAsWell) {
		HistoryToken token = tokens.getToken(key);
		if(null == token){
			throw new IllegalArgumentException("The incoming token is not correct."
											 + "\nPlease use Unit-Test to test [" + this.stateName +"]");
		}
		
		if( validateValueAsWell && !this.isValidPresentValue(token.getValue()) ){
			throw new IllegalArgumentException("The value of [" + key + "] must be set in [" + this.stateName +"].");
		}
		
		return token;
	}
	
	/**
	 * To create a token collection
	 * @return A token collection
	 */
	private HistoryTokenCollection createTokenCollection(){
		return new HistoryTokenSet();
	}
	
	/**
	 * To create a token collection
	 * @param tokenString the string contains tokens
	 * 
	 * @return A token collection
	 * @throws InvalidTokenFormatException 
	 */
	private HistoryTokenCollection createTokenCollection(String tokenString) throws InvalidTokenFormatException{
		return new HistoryTokenSet(tokenString);
	}
	
	
	/**
	 * To parse the incoming tokens, please add the parsed token into this state via addToken().
	 * And call {@link super.parse()} to validate the STATE token
	 * 
	 * @param tokens The incoming tokens
	 */
	protected void parse(HistoryTokenCollection tokens){
		this.validateState(tokens);
	}
	
	/**
	 * To parse the incoming token string 
	 * 
	 * @param tokens The incoming token string
	 * @throws InvalidTokenFormatException 
	 */
	protected void parse(String tokenString) throws InvalidTokenFormatException{
		HistoryTokenCollection collection = this.createTokenCollection(tokenString);
		this.parse(collection);		
	}
	
	protected AbstractDataModelBuilderSetFactory getDataModelBuilderSetFactory() {
		return this.modelBuilderSetFactory;
	}
	
	private void validateState(HistoryTokenCollection tokens){
		if(null == tokens){
			throw new IllegalArgumentException("No incoming tokens for [" + this.stateName + "]");
		}
		
		HistoryToken state = tokens.getToken(HistoryState.TOKEN_STATE);
		if(null == state){
			throw new IllegalArgumentException("The state token has not been set");
		}
		
		if( !this.isValidIncomingStateName(state.getValue()) ){
			throw new IllegalArgumentException("The state[" + state.getValue() + "] is not supported");
		}
	}	
}
