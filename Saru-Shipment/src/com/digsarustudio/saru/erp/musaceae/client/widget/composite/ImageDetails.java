/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.composite;


import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * This object is used for {@link ImageUpload} to update image.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ImageDetails {
	/**
	 * The type of photo source as following:
	 * 	1.URL
	 * 	2.Encoded file content
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public enum SourceTypes {
		/**
		 * To indicate the image provided from local file system.<br>
		 */
		  EncodedFileContent
		  
		  /**
		   * To indicate the image object contains a URL where the image stored.
		   */
		, URL
	}

	/**
	 * 
	 * The object builder for {@link ImageDetails}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<ImageDetails> {
		private ImageDetails result = null;

		public Builder() {
			this.result = new ImageDetails();
		}

		/**
		 * @param source
		 * @see com.digsarustudio.musa.widget.composite.ImageDetails#setSource(java.lang.String)
		 */
		public Builder setSource(String source) {
			result.setSource(source);
			return this;
		}

		/**
		 * @param type
		 * @see com.digsarustudio.musa.widget.composite.ImageDetails#setSourceType(com.digsarustudio.musa.widget.composite.ImageDetails.SourceTypes)
		 */
		public Builder setSourceType(SourceTypes type) {
			result.setSourceType(type);
			return this;
		}

		/**
		 * 
		 * @see com.digsarustudio.musa.widget.composite.ImageDetails#setSourceIsFromEncodedFile()
		 */
		public Builder setSourceIsFromEncodedFile() {
			result.setSourceIsFromEncodedFile();
			return this;
		}

		/**
		 * 
		 * @see com.digsarustudio.musa.widget.composite.ImageDetails#setSourceIsFromURL()
		 */
		public Builder setSourceIsFromURL() {
			result.setSourceIsFromURL();
			return this;
		}

		/**
		 * @param name
		 * @see com.digsarustudio.musa.widget.composite.ImageDetails#setName(java.lang.String)
		 */
		public Builder setName(String name) {
			result.setName(name);
			return this;
		}

		/**
		 * @param type
		 * @see com.digsarustudio.musa.widget.composite.ImageDetails#setType(java.lang.String)
		 */
		public Builder setType(String type) {
			result.setType(type);
			return this;
		}

		/**
		 * @param id
		 * @see com.digsarustudio.musa.widget.composite.ImageDetails#setId(java.lang.String)
		 */
		public Builder setId(String id) {
			result.setId(id);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ImageDetails build() {
			return this.result;
		}

	}
	
	/**
	 * The URL of the photo on the Internet or in the local file system.
	 * It could be the content of the photo as well.
	 * 
	 * Please check the format of this attribute before using it.
	 */
	private String		source		= null;
	private SourceTypes	sourceType	= null;
	private String 		name			= null;
	private String 		type			= null;
	private String 		id			= null;
	
	/**
	 * 
	 */
	private ImageDetails() {
	
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.model.photo.Photo#getSource()
	 */

	public String getSource() {
		return this.source;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.model.photo.Photo#setSource(java.lang.String)
	 */

	public void setSource(String source) {
		this.source = source;
	}

	public SourceTypes getSourceType() {
		return this.sourceType;
	}

	public void setSourceType(SourceTypes type) {
		this.sourceType = type;
		
	}
	
	public void setSourceIsFromEncodedFile() {
		this.sourceType = SourceTypes.EncodedFileContent;
	}
	
	public void setSourceIsFromURL() {
		this.sourceType = SourceTypes.URL;
	}
	
	public Boolean isEncodedFileContentType() {
		return (SourceTypes.EncodedFileContent == this.sourceType);
	}
	
	public Boolean isURLContentType() {
		return (SourceTypes.URL == this.sourceType);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		if(null == type) {
			return;
		}
		
//Sample:		
//		Type: image/png
		int position = type.indexOf("image/");		
		if( position < 0) {
			this.type = type;
		}else {
			this.type = type.substring(position + 6);
		}
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}	
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
