/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.composite;

import java.util.logging.Logger;

import com.digsarustudio.musa.event.widget.composite.ChangeEvent;
import com.digsarustudio.musa.mvp.event.DeleteEvent;
import com.digsarustudio.musa.mvp.event.DeleteEventHandler;
import com.digsarustudio.musa.mvp.event.HasDeleteHandler;
import com.digsarustudio.musa.resources.ResourceManager;
import com.digsarustudio.saru.erp.musaceae.client.widget.SaruFileUpload;
import com.digsarustudio.saru.erp.musaceae.client.widget.SaruImage;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Using to show what image is going to upload.
 * 
 * Do not use this widget to fetch the content of image programmingly.
 * 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.1
 * @since		1.0.0
 * 
 * TODO To make the client code can fetch the content of image programmingly.
 *
 */
public class ImageUpload extends Composite implements HasDeleteHandler{
	Logger logger = Logger.getLogger(this.getClass().getName());
	
	/**
	 * 
	 * The state indicates what sort of data this uploader imports.<br>
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	private enum UploadingState{
		/**
		 * Nothing in this uploader
		 */
		 Empty
		 
		 /**
		  * Default image has been set
		  */
		,Default
		
		/**
		 * A particular image object provided from remote service for this uploader to display and contain.
		 */
		,SourcedImage
		
		/**
		 * A particular image loaded from local file system.
		 */
		,LocalFile
		
		/**
		 * To display a particular image provided from the remote service only.
		 */
		,Display		
	}
	
	private static ImageUploaderUiBinder uiBinder = GWT.create(ImageUploaderUiBinder.class);

	interface ImageUploaderUiBinder extends UiBinder<Widget, ImageUpload> {
	}

	@UiField SaruImage			img;
	@UiField SaruFileUpload		fileUpload;
	@UiField Label				imgSize;
	@UiField HorizontalPanel		infoPanel;
	@UiField Label				fileNameInfo;
	
	
	private DeleteEventHandler	deleteEventHandler;

	private ImageDetails			imageDetails	= null;;
	private UploadingState		state	= UploadingState.Empty; 
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public ImageUpload() {
		initWidget(uiBinder.createAndBindUi(this));
		
		this.init();
	}

	@UiHandler("removeBtn")
	protected void onRemoveButtonClickEvent(ClickEvent event){
		if(null != this.deleteEventHandler){
			DeleteEvent deleteEvent = new DeleteEvent(this.imageDetails);
			this.deleteEventHandler.onDelete(deleteEvent);
		}
		
		this.clear();
		this.showFileUploadUI();
	}
	
	/**
	 * TODO BUG: The event called normally and the content of image is set correctly, however the image still cannot be shown on the UI.
	 * 
	 * @param event
	 */
	@UiHandler("fileUpload")
	protected void onFileUploadChangeEvent(ChangeEvent event) {		
		this.state = UploadingState.LocalFile;
		
		this.updateImage(this.fileUpload.getElement(), this.img.getElement());		
		this.updateFileName(this.getImageFileName());
		this.showFileInfoUI();		
	}
	
	/**
	 * Reset all the setting to show a file upload user interface
	 */
	public void clear(){
		this.fileUpload.clear();
		this.fileNameInfo.setText("");
		this.img.setResource(ResourceManager.getInstance().getImagesBundle().getSelectPhotoIcon());
		this.imgSize.setText("0 x 0");
		
		this.imageDetails = null;
		this.state = UploadingState.Default;
		
		this.showFileUploadUI();
	}
	
	/**
	 * Assign a URL of image to this widget for display only.<br>
	 * 
	 * @param url The URL of image
	 * 
	 * @deprecated Please use {@link #setImage(ImageDetails)} instead
	 */
	public void setURL(String url){
		if(null == url) {
			return;
		}
		
		this.updateFileName(url);
		
		this.img.setUrl(url);
		this.showFileInfoUI();
	}
	
	/**
	 * Assign a name to this widget for the form data transferring
	 * 
	 * @param name The name for the form data to transfer
	 */
	public void setName(String name) {
		if(null == name) {
			return;
		}
		
		this.fileUpload.setName(name);
	}
	
	/**
	 * Returns the content of this image
	 * 
	 * @return the content of this image
	 */
	public String getLocalImageContent(){
		if( !this.isImageSelected() ) {
			return null;
		}
		
		return this.img.getImageContent();
	}
	
	/**
	 * Returns the file name of this image, including the extension.
	 * 
	 * @return The file name of this image, including the extension.
	 */
	public String getLocalImageFileName(){
		if( !this.isImageSelected() ) {
			return null;
		}
		
		String filePath = this.fileUpload.getFilename();		
		
		return this.retrieveFileName(filePath);
	}
	
	/**
	 * Returns the type of this image
	 * 
	 * @return The type of this image
	 */
	public String getLocalImageType(){
		if( !this.isImageSelected() ) {
			return null;
		}
		
		return this.fileUpload.getFileType();
	}
	
	/**
	 * Returns the size of this image
	 * 
	 * @return The size of this image
	 */
	public Long getLocalImageSize(){
		if( !this.isImageSelected() ) {
			return 0L;
		}
		
		return new Long(this.fileUpload.getFileSize());
	}
		
	/**
	 * Returns true if there was an image set to this widget, otherwise false returned
	 * 
	 * @return true if there was an image set to this widget, otherwise false returned
	 */
	public Boolean isImageSelected() {
		switch(this.state) {
			case SourcedImage:
			case LocalFile:
				return true;
			default:
				return false;
		}
	}

	public Boolean isEmpty() {
		switch(this.state) {
			case Empty:
			case Default:
				return true;
			default:
				return false;
		}
	}
	
	public Boolean isLocalImage() {
		return (UploadingState.LocalFile == this.state);
	}
	
	public Boolean isSourcedImage() {
		return (UploadingState.SourcedImage == this.state);
	}
	
	public ImageDetails getImage() {
		return this.imageDetails;
	}
	
	public void setImage(ImageDetails details) {
		if( null == details || null == details.getSourceType() || null == details.getSource() ) {
			return;
		}
		
		switch( details.getSourceType() ) {
			case EncodedFileContent:
				this.img.setImageContent(details.getSource());
				break;
			case URL:
				this.img.setUrl(details.getSource());
				break;
			default:
				throw new UnsupportedOperationException("Unknown source type (" + details.getSourceType().toString() + ") set to ImageUpload");
		}
		
		this.imageDetails = details;
		this.state = UploadingState.SourcedImage;
		this.showFileInfoUI();
	}
	
	/**
	 * Returns the file name of this image, including the extension.
	 * 
	 * @return The file name of this image, including the extension.
	 */
	public String getImageFileName(){
		String filePath = this.fileUpload.getFilename();		
		
		return this.retrieveFileName(filePath);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.mvp.event.HasDeleteHandler#addDeleteEventHandler(com.digsarustudio.musa.mvp.event.DeleteEventHandler)
	 */
	@Override
	public HandlerRegistration addDeleteEventHandler(DeleteEventHandler handler) {
		this.deleteEventHandler = handler;
		
		return new HandlerRegistration() {			
			@Override
			public void removeHandler() {
				deleteEventHandler = null;
			}
		};
	}
	
	/**
	 * Initialization
	 */
	protected void init(){
		this.img.setResource(ResourceManager.getInstance().getImagesBundle().getSelectPhotoIcon());
		this.state = UploadingState.Default;
	}
	
	/**
	 * Show the file upload widget, but hide the details of image
	 */
	private void showFileUploadUI(){
		this.fileUpload.setVisible(true);
		this.infoPanel.setVisible(false);
	}
	
	/**
	 * Show the details of image, but hide the file upload widget
	 */
	private void showFileInfoUI(){
		this.fileUpload.setVisible(false);
		this.infoPanel.setVisible(true);
	}
	
	/**
	 * Update the name of file to the image details panel.
	 * 
	 * @param url The URL of image
	 */
	private void updateFileName(String url){
		String fileName = this.retrieveFileName(url);
		this.fileNameInfo.setText(fileName);
	}
	
	private String retrieveFileName(String url){
		int index = url.lastIndexOf("/");
		
		if( 0 > index ){
			index = url.lastIndexOf("\\");
		}
		
		return url.substring(index+1);
	}
	
	/**
	 * Show the selected image on the screen
	 * 
	 * @reference https://developer.mozilla.org/en/docs/Web/API/File
	 * @param element
	 */
	private final native void updateImage(Element sourceElement, final Element targetElement) /*-{
		var self = this;
		
		//Read file content from a specific file and set it into an Image widget to display.
		var reader = new FileReader();
		reader.onload=function(e){
			targetElement.src = e.target.result;
		};
		
		reader.readAsDataURL(sourceElement.files[0]);
	}-*/;
	
	/**
	 * Returns the content of this image
	 * 
	 * @param source The image element in the DOM
	 * 
	 * @return the content of this image
	 * 
	 * @deprecated Please use {@link SaruImage#getImageContent(String)} instead
	 */
	private final native String getImageContent(Element source) /*-{
		return source.src;		
	}-*/;
	
	/**
	 * Assigns the content of an image to this uploader
	 * 
	 * @param source The image element in the DOM
	 * 
	 * @param content THe content for this uploader
	 * 
	 * @deprecated Please use {@link SaruImage#setImageContent(String)} instead
	 */
	private final native void setImageContent(Element source, String content) /*-{
		source.src = content;
	}-*/;
}
