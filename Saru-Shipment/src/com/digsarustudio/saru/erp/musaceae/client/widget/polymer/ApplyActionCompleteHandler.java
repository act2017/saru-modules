/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.polymer;

import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.dialog.ApplyActionCompleteEvent;
import com.google.gwt.event.shared.EventHandler;

/**
 * This handler callback to parent when the particular action has been completed.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface ApplyActionCompleteHandler extends EventHandler{
	void onApplyActionComplete(ApplyActionCompleteEvent event);
}
