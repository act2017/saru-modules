/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.user.client.ui.Composite;

/**
 * The event indicating a change event occurred for the composite to carry message to the receiver 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class CompositeChangeEvent extends AbstractCompositeEvent<CompositeChangeEvent.Handler> {
	/**
	 * 
	 * Event handler of Change Event
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public interface Handler extends EventHandler{
		/*
		 * On Changed
		 */
		void onChanged(CompositeChangeEvent event);
	}
		
	public static Type<Handler> TYPE = new Type<>();			
	
	/**
	 * Constructs a click event 
	 */
	public CompositeChangeEvent() {
		super();
	}

	/**
	 * Constructs a click event with the source data
	 * 
	 * @param sourceData The click event with the source data
	 */
	public CompositeChangeEvent(Object sourceData) {
		super(sourceData);
	}

	/**
	 * Constructs a click event with the source widget
	 * 
	 * @param sourceWidget The click event with the source widget
	 */
	public CompositeChangeEvent(Composite sourceWidget) {
		super(sourceWidget);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<Handler> getAssociatedType() {
		return TYPE;
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(Handler handler) {
		handler.onChanged(this);
	}
}
