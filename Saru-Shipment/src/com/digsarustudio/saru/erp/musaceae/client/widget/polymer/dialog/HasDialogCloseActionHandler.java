/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.polymer.dialog;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * The sub-type can have and use {@link DialogCloseActionHandler} to close dialog.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface HasDialogCloseActionHandler {
	HandlerRegistration addDialogCloseActionHandler(DialogCloseActionHandler handler);
}
