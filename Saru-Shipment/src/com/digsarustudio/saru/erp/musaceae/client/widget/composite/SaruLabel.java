/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.composite;

import com.digsarustudio.saru.erp.musaceae.client.widget.HasFontStyle;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasAlignment;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * In this label, the text can be set at middle or center without css.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.1
 * @since		1.0.0
 *
 * Bug: the vertical alignment can not be set from UI binder.
 * Sol: set cell vertical alignment for the target widget.
 */
public class SaruLabel extends Composite implements HasText 
													, HasAlignment
													, HasClickHandlers
													, HasFontStyle													
													{

	private static SaruLabelUiBinder uiBinder = GWT.create(SaruLabelUiBinder.class);

	interface SaruLabelUiBinder extends UiBinder<Widget, SaruLabel> {
	}
	
	@UiField	HorizontalPanel	label;
	@UiField	Label			text;
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public SaruLabel() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	/**
	 * To set style to text. Please use this instead of CSS in UIbinder
	 * 
	 * @param style
	 */
	public void setTextStyle(String style) {
		this.text.setStyleName(style);
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasText#getText()
	 */
	@Override
	public String getText() {
		return this.text.getText();
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasText#setText(java.lang.String)
	 */
	@Override
	public void setText(String text) {
		this.text.setText(text);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasVerticalAlignment#getVerticalAlignment()
	 */
	@Override
	public VerticalAlignmentConstant getVerticalAlignment() {
		return this.label.getVerticalAlignment();
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasVerticalAlignment#setVerticalAlignment(com.google.gwt.user.client.ui.HasVerticalAlignment.VerticalAlignmentConstant)
	 */
	@Override
	public void setVerticalAlignment(VerticalAlignmentConstant align) {
		this.label.setCellVerticalAlignment(this.text, align);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasHorizontalAlignment#getHorizontalAlignment()
	 */
	@Override
	public HorizontalAlignmentConstant getHorizontalAlignment() {
		return this.label.getHorizontalAlignment();
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.HasHorizontalAlignment#setHorizontalAlignment(com.google.gwt.user.client.ui.HasHorizontalAlignment.HorizontalAlignmentConstant)
	 */
	@Override
	public void setHorizontalAlignment(HorizontalAlignmentConstant align) {
		this.label.setCellHorizontalAlignment(this.text, align);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.HasClickHandlers#addClickHandler(com.google.gwt.event.dom.client.ClickHandler)
	 */
	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		this.registerClickEvent(handler);		
		return this.text.addClickHandler(handler);
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.HasFontStyle#setFontSize(java.lang.String)
	 */
	@Override
	public void setFontSize(String size) {
		this.text.getElement().getStyle().setProperty("fontSize", size);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.HasFontStyle#setFontWeight(java.lang.String)
	 */
	@Override
	public void setFontWeight(String weight) {
		this.text.getElement().getStyle().setProperty("fontWeight", weight);
	}

	/**
	 * Pass the click handler to panel
	 * 
	 * @param handler The click handler
	 */
	private void registerClickEvent(ClickHandler handler){
		this.label.addDomHandler(handler, ClickEvent.getType());
	}
}
