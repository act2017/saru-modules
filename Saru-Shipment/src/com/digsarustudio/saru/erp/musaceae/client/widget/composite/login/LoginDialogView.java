/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.composite.login;

import com.digsarustudio.saru.erp.musaceae.client.mvp.presenter.Presenter;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

/**
 * The user login Dialog
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @since		1.0.3
 * @version		1.0.10
 * <br>
 * Note:<br>
 * 				1.0.10	->	Bug Fixing: fail to reset password.<br>
 *
 */
public class LoginDialogView extends DialogBox implements LoginDialogPresenter.Display{
	private final String WRONG_USERNAME_PASSWORD = "User name and password do not match. Please ensure the lowercase or uppercase of characters are right.";
	
	private static LoginDialogUiBinder uiBinder = GWT.create(LoginDialogUiBinder.class);

	interface LoginDialogUiBinder extends UiBinder<Widget, LoginDialogView> {
	}
	
	interface Styling extends CssResource{
		
	}

	@UiField Styling			style;
	@UiField TextBox			userName;
	@UiField PasswordTextBox	password;
	@UiField PushButton			loginBtn;
	@UiField PushButton			resetPwdBtn;
	@UiField PushButton			forgotPwdBtn;
	@UiField Label				hint;
	
	private LoginDialogPresenter presenter = null;
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public LoginDialogView() {
		setWidget(uiBinder.createAndBindUi(this));
		
		this.init();
	}

	@UiHandler("loginBtn")
	protected void onLoginClickEvent(ClickEvent event) {
		this.onLogin(this.userName.getText(), this.password.getText());
	}
	
	@UiHandler("password")
	protected void onButtonKeyDownClickEvent(KeyDownEvent event) {
		if( event.getNativeKeyCode() != KeyCodes.KEY_ENTER ){
			return;
		}
		
		this.onLogin(this.getUserName(), this.getPassword());
	}	
	
	@UiHandler("cancelBtn")
	protected void onLoginCancelClickEvent(ClickEvent event) {
		this.hide();
		this.presenter.onCancel();
	}
	
	@UiHandler("resetPwdBtn")
	protected void onResetPasswordClickEvent(ClickEvent event) {
		this.hide();
		this.presenter.onResetPassword(this.getUserName());
	}
	
	@UiHandler("forgotPwdBtn")
	protected void onForgotPasswordClickEvent(ClickEvent event) {
		this.hide();
		
		/**
		 * @since 1.0.10
		 */
		this.presenter.onForgotPassword(this.getUserName());
	}
	
	private Boolean isValidUserName(String userName){
		return true;
	}
	
	private Boolean isValidPassword(String password){
		return true;
	}
	
	private void onLogin(String userName, String password){
		if( !this.isValidUserName(this.userName.getText()) ){
			this.setHint(this.WRONG_USERNAME_PASSWORD);
			this.showHint();
			return;
		}
		
		if( !this.isValidPassword(this.password.getText()) ){
			this.setHint(this.WRONG_USERNAME_PASSWORD);
			this.showHint();
			return;
		}
		
		this.presenter.onLogin(this.userName.getText(), this.password.getText());
	}
	
	private void showHint(){
		this.hint.setVisible(true);
	}
	
	public void setHint(String hint){
		this.hint.setText(hint);		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.LoginPresenter.Display#clear()
	 */
	@Override
	public void clear() {
		this.userName.setText("");
		this.password.setText("");
		
		this.hint.setText("");
		
		this.resetButtonState();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.LoginPresenter.Display#reset()
	 */
	@Override
	public void reset() {
		this.userName.setText("");
		this.password.setText("");
		
		this.hint.setText("");
		
		this.resetButtonState();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.LoginPresenter.Display#onFailToLogin(java.lang.String)
	 */
	@Override
	public void onFailToLogin(String message) {
		this.setHint(message);
		this.hint.setVisible(true);
	}

	/* (non-Javadoc)
	 * 
	 */
	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter =(LoginDialogPresenter)presenter;
	}	

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.LoginDialogPresenter.Display#showDialog()
	 */
	@Override
	public void showDialog() {
		this.clear();
		this.center();
		this.show();		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.LoginDialogPresenter.Display#hideDialog()
	 */
	@Override
	public void hideDialog() {
		this.hide();
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.login.LoginDialogPresenter.Display#showResetPasswordBtn()
	 */
	@Override
	public void showResetPasswordBtn() {
		this.resetPwdBtn.setVisible(true);
		this.resetPwdBtn.setFocus(true);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.login.LoginDialogPresenter.Display#hideResetPasswordBtn()
	 */
	@Override
	public void hideResetPasswordBtn() {
		this.resetPwdBtn.setVisible(false);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.login.LoginDialogPresenter.Display#showForgotPasswordBtn()
	 */
	@Override
	public void showForgotPasswordBtn() {
		this.forgotPwdBtn.setVisible(true);
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.login.LoginDialogPresenter.Display#hideForgotPasswordBtn()
	 */
	@Override
	public void hideForgotPasswordBtn() {
		this.forgotPwdBtn.setVisible(false);
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.login.LoginDialogPresenter.Display#getUserName()
	 */
	@Override
	public String getUserName() {
		String name = this.userName.getText().trim();
		
		return name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.login.LoginDialogPresenter.Display#getPassword()
	 */
	@Override
	public String getPassword() {
		String pwd = this.password.getText().trim();
		
		return pwd;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.login.LoginDialogPresenter.Display#setIsPasswordExpired()
	 */
	@Override
	public void setIsPasswordExpired() {
		this.resetButtonState();
		
		this.showResetPasswordBtn();
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.login.LoginDialogPresenter.Display#setIsWrongPassword()
	 */
	@Override
	public void setIsWrongPassword() {
		this.resetButtonState();
		this.showForgotPasswordBtn();
	}

	private void init(){
		this.setAnimationEnabled(false);
		
		this.setText("Login");
		
		this.resetButtonState();
	}
	
	private void resetButtonState() {
		this.hideResetPasswordBtn();
		this.hideForgotPasswordBtn();
	}
}
