/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.polymer;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * The sub-type has the ability to handle {@link TextChangeHandler}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface HasTextChangeHandler {
	HandlerRegistration addTextChangeHandler(TextChangeHandler handler);
}
