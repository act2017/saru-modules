/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.composite;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * This pager provides adjustable number button for user to click a page the user wants to view.<br>
 * Also, a top page and bottom page button supplied.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @since		1.0.7
 * @version		1.0.0
 * <br>
 * Note:<br>
 *				1.0.7	-> Create
 */
public class SaruNumberPager extends Composite implements HasClickHandlers{
	private static SaruNumberPagerUiBinder uiBinder = GWT.create(SaruNumberPagerUiBinder.class);

	interface SaruNumberPagerUiBinder extends UiBinder<Widget, SaruNumberPager> {
	}
	
	interface Styling extends CssResource{
		String pageButton();
		String pagerButtonHover();
		String pagerButtonActive();
	}
	
	@UiField	Styling			style;
	@UiField	HorizontalPanel	buttonPanel;
	
	private		Integer			totalCount		= 0;
	private		Integer			noPerPage		= 0;
	private		Integer			cursor			= 0;
	private		Integer			leftBtnCount	= 5;
	private		Integer			rightBtnCount	= 3;
	
	private		List<ClickHandler>	clickHandlers	= null;

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public SaruNumberPager() {
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public void clear() {
		this.buttonPanel.clear();
	}
	
	public void reset() {
		this.clear();
	}
	
	public Integer getCursor() {
		return this.cursor;
	}
	
	public Integer getCurrentPage() {
		return (this.cursor / this.noPerPage) + 1;
	}
	
	public void setNumberPerPage(Integer number) {
		this.noPerPage = number;
	}
	
	public void setPages(Integer totalCount, Integer noPerPage) {
		this.totalCount = totalCount;
		this.noPerPage = noPerPage;
		
		if(null == this.cursor) {
			this.cursor = 0;
		}else if( totalCount < this.cursor ){
			this.cursor = 0;
		}
		
		this.clear();
		
		this.initButtons(totalCount, noPerPage);
	}
	
	public void setCurrentPage(Integer page) {
		if(null == page) {
			return;
		}
		
		this.cursor = page * noPerPage;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.HasClickHandlers#addClickHandler(com.google.gwt.event.dom.client.ClickHandler)
	 */
	@Override
	public HandlerRegistration addClickHandler(final ClickHandler handler) {
		if(null == handler) {
			return null;
		}else if(null == this.clickHandlers) {
			this.clickHandlers = new ArrayList<>();
		}
		
		this.clickHandlers.add(handler);
		
		return new HandlerRegistration() {
			
			@Override
			public void removeHandler() {
				clickHandlers.remove(handler);
			}
		};
	}

	private void initButtons(Integer totalCount, Integer noPerPage) {
		Integer totalPage = totalCount/noPerPage + ((0 == totalCount % noPerPage) ? 0 : 1);
		if(null == totalCount || 0 == totalCount) {
			totalPage = 1;
		}
		
		Boolean isOverDefaultCount = (totalPage > (this.leftBtnCount + this.rightBtnCount));
		
		//First button
		this.buttonPanel.add(this.getBack21stButton());
		
		//Previous button
		this.buttonPanel.add(this.getPreviousPageButton());
		
		//Left N button
		Integer left = isOverDefaultCount ? this.leftBtnCount : totalPage;
		for (int i = 0 ; i < left ; i++) {
			this.buttonPanel.add(this.getPageButton(i));
		}
		
		//Dot button
		if( isOverDefaultCount ) {
			this.buttonPanel.add(this.getDotButton());
		}
		
		//Right n button
		if( isOverDefaultCount ) {
			for (int i = totalPage - this.rightBtnCount ; i < totalPage; i++) {
				this.buttonPanel.add(this.getPageButton(i));
			}
		}
		
		//Next button
		this.buttonPanel.add(this.getNextPageButton());
		
		//Last button
		this.buttonPanel.add(this.getForward2LastButton());
	}
	
	private Label getPageButton(final Integer index) {
		final Integer page = index + 1;
		final Label btn = new Label(page.toString());		
		
		btn.setStyleName(this.style.pageButton());
		if(this.getCurrentPage() == page) {
			btn.addStyleName(this.style.pagerButtonActive());
		}
		
		btn.addMouseMoveHandler(new MouseMoveHandler() {
			
			@Override
			public void onMouseMove(MouseMoveEvent event) {
				btn.addStyleName(style.pagerButtonHover());
				
			}
		});
		btn.addMouseOutHandler(new MouseOutHandler() {
			@Override
			public void onMouseOut(MouseOutEvent event) {				
				btn.removeStyleName(style.pagerButtonHover());
				
			}
		});
		
		btn.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				cursor = index * noPerPage;
				dispatchClickEvent(event);
			}
		});
		
		return btn;
	}
	
	private Label getDotButton() {
		final Label btn = new Label("...");
		btn.setStyleName(this.style.pageButton());
		return btn;		
	}
	
	private Label getNextPageButton() {
		final Label btn = new Label(">");
		btn.setStyleName(this.style.pageButton());
		btn.addMouseMoveHandler(new MouseMoveHandler() {
			
			@Override
			public void onMouseMove(MouseMoveEvent event) {
				btn.addStyleName(style.pagerButtonHover());
				
			}
		});
		btn.addMouseOutHandler(new MouseOutHandler() {
			@Override
			public void onMouseOut(MouseOutEvent event) {
				btn.removeStyleName(style.pagerButtonHover());
				
			}
		});
		btn.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(totalCount < cursor + noPerPage) {
					return;
				}
				
				cursor += noPerPage;
				dispatchClickEvent(event);
			}
		});
		
		return btn;
		
	}
	
	private Label getPreviousPageButton() {
		final Label btn = new Label("<");
		btn.setStyleName(this.style.pageButton());
		btn.addMouseMoveHandler(new MouseMoveHandler() {
			
			@Override
			public void onMouseMove(MouseMoveEvent event) {
				btn.addStyleName(style.pagerButtonHover());
				
			}
		});
		btn.addMouseOutHandler(new MouseOutHandler() {
			@Override
			public void onMouseOut(MouseOutEvent event) {
				btn.removeStyleName(style.pagerButtonHover());
				
			}
		});
		btn.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(0 == cursor) {
					return;
				}
				
				cursor -= noPerPage;
				dispatchClickEvent(event);
			}
		});
		
		return btn;
		
	}
	
	private Label getBack21stButton() {
		final Label btn = new Label("<<");
		btn.setStyleName(this.style.pageButton());
		btn.addMouseMoveHandler(new MouseMoveHandler() {
			
			@Override
			public void onMouseMove(MouseMoveEvent event) {
				btn.addStyleName(style.pagerButtonHover());
				
			}
		});
		btn.addMouseOutHandler(new MouseOutHandler() {
			@Override
			public void onMouseOut(MouseOutEvent event) {
				btn.removeStyleName(style.pagerButtonHover());
				
			}
		});
		btn.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				cursor = 0;
				dispatchClickEvent(event);
			}
		});
		
		return btn;
	}
	
	private Label getForward2LastButton() {
		final Label btn = new Label(">>");
		btn.setStyleName(this.style.pageButton());
		btn.addMouseMoveHandler(new MouseMoveHandler() {
			
			@Override
			public void onMouseMove(MouseMoveEvent event) {
				btn.addStyleName(style.pagerButtonHover());
				
			}
		});
		btn.addMouseOutHandler(new MouseOutHandler() {
			@Override
			public void onMouseOut(MouseOutEvent event) {
				btn.removeStyleName(style.pagerButtonHover());
				
			}
		});
		btn.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				cursor = totalCount/noPerPage * noPerPage;
				dispatchClickEvent(event);
			}
		});
		
		return btn;
	}
	
	private void dispatchClickEvent(ClickEvent event) {
		if(null == this.clickHandlers) {
			return;
		}
		
		for (ClickHandler handler : this.clickHandlers) {
			handler.onClick(event);
		}
	}
}
