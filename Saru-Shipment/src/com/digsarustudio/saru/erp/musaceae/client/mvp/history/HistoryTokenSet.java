/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.history;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.digsarustudio.banana.utils.InvalidTokenFormatException;


/**
 * The set of history tokens. The key of the token cannot be duplicated.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public class HistoryTokenSet extends AbstractHistoryTokenCollection implements HistoryTokenCollection {
	private Set<HistoryToken> tokens = new HashSet<>();
	
	/**
	 * Constructs a history token set object
	 */
	public HistoryTokenSet(){		
	}
	
	/**
	 * Constructs a history token set object with a string presenting as a collection of tokens
	 * 
	 * @param tokenString The string presents as a collection of tokens
	 * 
	 * @throws InvalidTokenFormatException When the incoming string is not in a valid token string format 
	 * 
	 */
	public HistoryTokenSet(String tokenString) throws InvalidTokenFormatException {
		this.parse(tokenString);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.jarable.HistoryStack#getToken(java.lang.String)
	 */
	@Override
	public HistoryToken getToken(String key) {
		for (HistoryToken token : this.tokens) {
			if( !token.getKey().equals(key) ){
				continue;
			}
			
			return token;
		}		
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.jarable.HistoryStack#addToken(com.digsarustudio.togauto.shared.jarable.HistoryToken)
	 */
	@Override
	public void addToken(HistoryToken token) {
		if( this.tokens.contains(token) ){
			return;
		}
		
		this.tokens.add(token);		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.jarable.HistoryStack#removeToken(java.lang.String)
	 */
	@Override
	public void removeToken(String key) {
		HistoryToken target = null;
		for (HistoryToken token : tokens) {
			if( !token.getKey().equals(key) ){
				continue;
			}
			
			target = token;
			break;
		}
		
		this.tokens.remove(target);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.jarable.HistoryStack#removeToken(com.digsarustudio.togauto.shared.jarable.HistoryToken)
	 */
	@Override
	public void removeToken(HistoryToken token) {
		this.removeToken(token.getKey());
	}

	@Override
	protected void parse(String tokenString) throws InvalidTokenFormatException{
		if(null == tokenString || tokenString.isEmpty()){
			throw new IllegalArgumentException("The input token is null or empty");
		}
		
		String[] tokensStrings = tokenString.split(this.HYPHEN);
		
		for (String token : tokensStrings) {
			if(token.isEmpty()){
				continue;
			}
			
			this.tokens.add(new HistoryToken(token));
		}
	}

	@Override
	protected List<HistoryToken> getTokens() {
		List<HistoryToken> rtns = new ArrayList<>();
		
		for (HistoryToken historyToken : this.tokens) {
			rtns.add(historyToken);
		}
		
		return rtns;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tokens == null) ? 0 : tokens.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoryTokenSet other = (HistoryTokenSet) obj;
		if (tokens == null) {
			if (other.tokens != null)
				return false;
		} else if (!tokens.equals(other.tokens))
			return false;
		return true;
	}

}
