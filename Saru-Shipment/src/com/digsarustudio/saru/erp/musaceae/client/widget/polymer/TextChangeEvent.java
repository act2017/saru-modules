/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.polymer;

import com.google.gwt.event.shared.GwtEvent;

/**
 * This event fired when the value of a text box changed.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class TextChangeEvent extends GwtEvent<TextChangeHandler> {
	public static Type<TextChangeHandler> TYPE = new Type<>();
	
	private String value = null;
	
	public TextChangeEvent(String value){
		this.value = value;
	}
	
	public String getValue(){
		return this.value;
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.DomEvent#getAssociatedType()
	 */
	@Override
	public Type<TextChangeHandler> getAssociatedType() {
		return TYPE;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(TextChangeHandler handler) {
		handler.onTextChange(this);
		
	}

}
