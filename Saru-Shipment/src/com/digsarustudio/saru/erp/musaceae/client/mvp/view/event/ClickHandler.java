/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.view.event;

/**
 * This click handler is used to handle an event triggered by user through the user interface 
 * which has the ability to carry a source data for the client code to use. 
 * Normally, this handler is used with {@link ClickEvent}.<br>
 * If you want to handle the event from the browser only, 
 * please use {@link com.google.gwt.event.dom.client.ClickHandler}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 * @deprecated	1.0.3	Please see the ReadMe file and use the event from com.digsarustudio.saru.erp.musaceae.client.widget.event
 *
 */
public interface ClickHandler<T> {
	/**
	 * On a click event triggered.
	 * 
	 * @param event The triggered event
	 */
	void onClicked(ClickEvent<T> event);
}
