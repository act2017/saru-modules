/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.history.state;

/**
 * This exception indicates the there is not history state incoming when the web app executed.<br>
 * Please use this exception to handle the demonstration of home page or main page.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
@SuppressWarnings("serial")
public class EmptyHistoryStateException extends Exception {

	/**
	 * 
	 */
	public EmptyHistoryStateException() {

	}

	/**
	 * @param message
	 */
	public EmptyHistoryStateException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public EmptyHistoryStateException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public EmptyHistoryStateException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public EmptyHistoryStateException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
