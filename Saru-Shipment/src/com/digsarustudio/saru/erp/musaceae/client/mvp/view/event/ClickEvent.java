/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.view.event;

/**
 * This event triggered while a user tries to click a button.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 * @deprecated	1.0.3	Please see the ReadMe file and use the event from com.digsarustudio.saru.erp.musaceae.client.widget.event
 *
 */
public class ClickEvent<T> extends ViewEvent<T> {

	/**
	 * 
	 */
	public ClickEvent() {
	}
	
	public ClickEvent(T source) {
		this.setSourceData(source);
	}
}
