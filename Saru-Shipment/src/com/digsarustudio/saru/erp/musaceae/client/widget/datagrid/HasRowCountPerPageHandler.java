/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.datagrid;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * The sub-type can be added a {@link RowCountPerPageHandler} to handle row count per page event.<br>
 *
 * TODO use a generic handler instead of this.<br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface HasRowCountPerPageHandler {
	HandlerRegistration addRowCountPerPageHandler(final RowCountPerPageHandler handler);
}
