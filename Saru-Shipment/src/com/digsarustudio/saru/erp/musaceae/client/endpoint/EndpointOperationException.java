/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.endpoint;

import com.digsarustudio.saru.erp.musaceae.shared.endpoint.Endpoint;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;

/**
 * This exception indicates an error occurred when the client tried to send a request to the remote 
 * service via an {@link Endpoint}, but the {@link Endpoint} cannot handle it<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 * 
 * @deprecated	1.0.3	Please use {@link EndpointException} instead.<br>
 */
@SuppressWarnings("serial")
public class EndpointOperationException extends RuntimeException {

	/**
	 * 
	 */
	public EndpointOperationException() {

	}

	/**
	 * @param message
	 */
	public EndpointOperationException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public EndpointOperationException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public EndpointOperationException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public EndpointOperationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
