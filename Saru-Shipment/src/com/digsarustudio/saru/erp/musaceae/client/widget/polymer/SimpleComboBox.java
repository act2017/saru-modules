/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.polymer;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.digsarustudio.banana.utils.StringFormatter;
import com.digsarustudio.saru.erp.musaceae.client.widget.ListBoxItem;
import com.digsarustudio.saru.erp.musaceae.client.widget.SaruListBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.polymer.paper.widget.PaperFab;
import com.vaadin.polymer.paper.widget.PaperInput;

/**
 * A simple combo box for appending new item.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SimpleComboBox<T> extends Composite implements HasChangeHandlers
														 , HasTextChangeHandler
														 , HasDeleteHandler{
	
	Logger logger = Logger.getLogger(SimpleComboBox.class.getName());
	
	private static final String LIST_ITEM_OTHER	= "Other";
	private static final String LIST_VALUE_OTHER	= "OTHER";
	
	private static final String LIST_ITEM_HEADER		= "==  %s  ==";
	private static final String LIST_VALUE_HEADER	= "HEADER";

	private static ComboBoxUiBinder uiBinder = GWT.create(ComboBoxUiBinder.class);

	interface ComboBoxUiBinder extends UiBinder<Widget, SimpleComboBox<?>> {
	}
	
	@UiField Label			caption;
	@UiField SaruListBox<T>	list;
	@UiField PaperInput		other;
	@UiField PaperFab		deleteBtn;
	
	private Boolean				isOtherEnabled = true;
	private String				header			= null;
	
	private List<ChangeHandler>		changeEventHandlers	= new ArrayList<ChangeHandler>();
	private List<TextChangeHandler>	textChangeHandlers	= new ArrayList<TextChangeHandler>();
	private List<DeleteHandler>	deleteHandlers	= new ArrayList<>();

	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public SimpleComboBox() {
		initWidget(uiBinder.createAndBindUi(this));
		
		this.init();
	}
	
	@UiHandler("list")
	protected void onListValueChange(ChangeEvent event) {
		if( !this.getSelectedValue().equals(LIST_VALUE_OTHER) ) {
			this.other.setValue("");
		}
		
		this.checkOtherItemInList();
		
		for (ChangeHandler handler : changeEventHandlers) {
			handler.onChange(event);
		}
	}
	
	@UiHandler("other")
	protected void onOtherChangeEvent(com.vaadin.polymer.paper.widget.event.ChangeEvent event){
		for (TextChangeHandler handler : this.textChangeHandlers) {
			handler.onTextChange(new TextChangeEvent(this.other.getValue()));
		}
	}
	
	@UiHandler("deleteBtn")
	protected void onDeleteButtonClickEvent(ClickEvent event) {
		for(DeleteHandler handler : this.deleteHandlers) {
			handler.onDelete(new DeleteEvent(this.list.getSelectedItemSource()));
		}
	}
			
	public void setHeader(String header) {
		if(null != this.header){
			this.list.removeItem(0);
		}
		
		this.header = header;
		String formattedHeader = StringFormatter.format(LIST_ITEM_HEADER, header);
		this.list.insertItem(formattedHeader, LIST_VALUE_HEADER, 0);
		this.list.setSelectedIndex(0);
		
		this.checkOtherItemInList();
	}
	
	public void setItemBox(ListBoxItem<T> itemBox){
		this.list.setItemBox(itemBox);
	}
	
	public void addItem(String caption, String value) {
		Integer count = this.list.getItemCount();
		Integer index = this.isOtherEnabled ? (count - 1) : count;

		this.list.insertItem(caption, value, index);
	}
	
	public void addItem(T item) {
		Integer count = this.list.getItemCount();
		Integer index = this.isOtherEnabled ? (count - 1) : count;		
		
		this.list.insertItem(item, index);
	}

	public void setCaption(String caption) {
		this.caption.setText(caption);
	}
	
	public String getCaption() {
		return this.caption.getText();
	}
	
	public void emptyCaption() {
		this.caption.setText("");
	}
	
	public Integer getSelectedIndex() {
		return this.list.getSelectedIndex();
	}
	
	public String getItemCaption(Integer index) {
		return this.list.getItemText(index);
	}
	
	public String getItemValue(Integer index) {
		return this.list.getValue(index);
	}
	
	public String getSelectedItem() {
		return this.list.getSelectedItemText();
	}
	
	public void setSelectedItem(Integer index) {
		this.list.setSelectedIndex(index);
	}
	
	public void setSelectedItem(String item) {
		this.list.setSelectedItem(item);
	}
	
	public void setSelectedItem(T source) {
		this.list.setSelectedItem(source);
	}
	
	public void setSelectedValue(String value) {
		this.list.setSelectedValue(value);
	}
	
	public String getSelectedValue() {
		return this.list.getSelectedValue();
	}
	
	public Boolean isItemSelected() {
		Integer index = this.list.getSelectedIndex();

		if(index <= 0) {
			return false;
		}
		
		Integer lowbound = (null != this.header && !header.isEmpty()) ? 1 : 0;
		Integer upbound = this.list.getItemCount() - (this.isOtherEnabled ?  2 : 1);		
		Boolean isSelected = false;		
		if( lowbound <= index && index <= upbound ) {
			isSelected = true;
		}else if( index > upbound && this.isOtherEnabled && !this.other.getValue().isEmpty() ) {
			isSelected = true;
		}
		
		return isSelected;
	}
	
	public Integer getItemCount() {
		Integer count = null;
		
		if( null != this.header && !header.isEmpty() ){
			if( this.isOtherEnabled ){
				count = this.list.getItemCount() - 2;
			}else{
				count = this.list.getItemCount() - 1;
			}
		}else if(this.isOtherEnabled){
			count = this.list.getItemCount() - 1;
		}
		
		return count;
	}
	
	public void setEnabled() {
		this.list.setEnabled(true);
		
		if( null == this.header && this.header.isEmpty() && this.isOtherEnabled ){
			this.list.setSelectedValue(LIST_VALUE_OTHER);
		}else if( this.getItemCount() == 0 ) {
			this.list.setSelectedValue(LIST_VALUE_OTHER);
		}else{
			this.list.setSelectedIndex(0);
		}
		
		this.checkOtherItemInList();
	}
	
	public void setReadOnly() {
		this.list.setEnabled(false);
		this.other.setDisabled(true);
	}
	
	public void setWritable() {
		this.list.setEnabled(true);
		this.other.setDisabled(false);
	}
	
	public void setDisabled() {
		this.list.setSelectedIndex(0);
		this.list.setEnabled(false);
	}
	
	public void setOther(String other) {
		this.other.setValue(other);
	}
	
	public Boolean isValidatedValue() {
		return this.other.validate();
	}
	
	public String getOther() {
		return this.other.getValue();
	}
	
	public Boolean isOtherValue() {
		return (this.list.getSelectedValue().equals(LIST_VALUE_OTHER) && !this.getOther().isEmpty());
	}
	
	public T getSelectedSourceData() {
		return this.list.getSelectedItemSource();
	}
	
	public void setPlaceHolder(String text) {
		this.other.setLabel(text);
	}
	
	public void setPattern(String pattern){
		this.other.setPattern(pattern);
	}
	
	public void setErrorMessage(String message){
		this.other.setErrorMessage(message);
	}
	
	public void selectOther() {
		this.list.setSelectedValue(LIST_VALUE_OTHER);
	}
	
	public void clear() {
		this.list.clear();
		
		if(null != this.header) {
			this.list.addItem(StringFormatter.format(LIST_ITEM_HEADER, this.header), LIST_VALUE_HEADER);
			this.list.setSelectedIndex(0);
		}
		
		if(this.isOtherEnabled) {
			this.list.addItem(LIST_ITEM_OTHER, LIST_VALUE_OTHER);
		}		
		
		this.other.setValue("");
		this.checkOtherItemInList();
	}
	
	public void reset() {
		this.list.setSelectedIndex(0);
		this.checkOtherItemInList();
	}
	
	public void setDisableOtherField(Boolean disabled) {
		this.isOtherEnabled = !disabled;
		
		if(disabled) {
			this.list.removeItem(LIST_VALUE_OTHER);
		}else {
			this.list.addItem(LIST_ITEM_OTHER, LIST_VALUE_OTHER);
		}
		
		this.checkOtherItemInList();
	}

	public void setDeleteable() {
		this.deleteBtn.setVisible(true);
	}
	
	public void setUndeletable() {
		this.deleteBtn.setVisible(false);
	}
	
	public void setCaptionVisible(Boolean visible) {
		this.caption.setVisible(visible);
	}
	
	public void setListWidth(String width) {
		this.list.setWidth(width);
	}
	
	public void setListHeight(String height) {
		this.list.setHeight(height);
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.HasChangeHandlers#addChangeHandler(com.google.gwt.event.dom.client.ChangeHandler)
	 */
	@Override
	public HandlerRegistration addChangeHandler(final ChangeHandler handler) {
		this.changeEventHandlers.add(handler);
		return new HandlerRegistration() {
			
			@Override
			public void removeHandler() {
				changeEventHandlers.remove(handler);
			}
		};
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.onlinestore.console.client.widget.vehicle.event.HasTextChangeHandler#addTextChangeHandler(com.digsarustudio.saru.erp.onlinestore.console.client.widget.vehicle.event.TextChangeHandler)
	 */
	@Override
	public HandlerRegistration addTextChangeHandler(final TextChangeHandler handler) {
		this.textChangeHandlers.add(handler);
		
		return new HandlerRegistration() {
			
			@Override
			public void removeHandler() {
				textChangeHandlers.remove(handler);				
			}
		};
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.polymer.HasItemDeleteHandler#addItemDeleteHandler(com.digsarustudio.musa.widget.polymer.ItemDeleteHandler)
	 */
	@Override
	public HandlerRegistration addDeleteHandler(DeleteHandler handler) {
		this.deleteHandlers.add(handler);
		
		return new HandlerRegistration() {
			
			@Override
			public void removeHandler() {
				deleteHandlers.remove(handler);
			}
		};
	}

	private void init() {
		this.clear();
		
		this.checkOtherItemInList();
	}
	
	private void checkOtherItemInList() {
		if(!this.isOtherEnabled) {
			this.other.setVisible(false);
			this.other.setDisabled(true);
			return;
		}
		
		String selected = this.getSelectedValue();
		if( null == selected || !selected.equals(LIST_VALUE_OTHER)) {
			this.other.setVisible(false);
			this.other.setDisabled(true);
			return;
		}
		
		this.other.setVisible(true);
		this.other.setDisabled(false);
	}
}
