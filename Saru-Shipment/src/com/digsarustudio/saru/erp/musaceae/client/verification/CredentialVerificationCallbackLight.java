/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.verification;

/**
 * The light version of {@link CredentialVerificationCallback} for the {@link Presenter} to handle 
 * the result of credential verification from the app controller.<br> 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public abstract class CredentialVerificationCallbackLight implements CredentialVerificationCallback {
	public CredentialVerificationCallbackLight() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.banana.ship.client.CredentialVerificationCallback#onCancel()
	 */
	@Override
	public void onCancel() {
		// Omitted
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.banana.ship.client.CredentialVerificationCallback#onForgetPassword(java.lang.String)
	 */
	@Override
	public void onForgetPassword(String accountName) {
		// Omitted
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.banana.ship.client.CredentialVerificationCallback#onFailure(java.lang.Throwable)
	 */
	@Override
	public void onFailure(Throwable caught) {
		// Omitted
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.banana.ship.client.CredentialVerificationCallback#onResetPassword(java.lang.String)
	 */
	@Override
	public void onResetPassword(String accountName) {
		// Omitted
		
	}

}
