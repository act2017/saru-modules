/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.datagrid;

/**
 * The types of column for {@link SaruDataGrid}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public enum DataGridColumnTypes {
	 Text
	,Image
	,ActionBar
	,Button
	,Toggle
	,ActionButton
	;
}
