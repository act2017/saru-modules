/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget;

import com.google.gwt.resources.client.ImageResource;

/**
 * The sub-type can be set and retrieved the image resource.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface HasImageResource {
	void setResource(ImageResource resource);
}
