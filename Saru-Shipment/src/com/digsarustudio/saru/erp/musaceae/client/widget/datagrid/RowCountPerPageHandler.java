/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.datagrid;

/**
 * This handler handles row count per page change event.<br>
 *
 * TODO use a generic handler instead of this.<br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface RowCountPerPageHandler {
	void onRowCountPerPageChanged(Integer rowCount);
}
