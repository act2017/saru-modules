/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.datagrid;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent;
import com.google.gwt.user.cellview.client.ColumnSortEvent.AsyncHandler;
import com.google.gwt.user.cellview.client.DataGrid;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.DefaultSelectionEventManager;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.MultiSelectionModel;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.Range;
import com.google.gwt.view.client.SelectionModel;

/**
 * This implementation blocks the onRangeChanged() when onColumnSort called and make the onRangeChanged() can work properly.<br>
 * <br>
 * How to use this widget:<br>
 * 	1.Initiate {@link SaruDataGrid} before executing {@link #initWidget(Widget)}<br>
 * 	2.Setup properties of {@link SaruDataGrid}<br>
 *  3.Call {@link SaruDataGrid#init()} to apply settings to it.<br>
 *  4.Add {@link SaruDataGrid} to the parent container if it has been declared in java code.
 *  5.Set {@link SaruDataGrid} as "provided=true" if it has been declared in uiBinder. 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SaruDataGrid<T> extends DataGrid<T> {

	private AsyncDataGridColumnSortHandler		dataGridColumnSortHandler	= null;
	private AsyncPagerChangeHandler				pagerChangeHandler			= null;
	
	//Column config
	private List< SaruDataGridColumn<T, ?> >	columns						= null;
	
	private ProvidesKey<T>						keyProvider					= null;
	private AsyncDataProvider<T>				dataProvider				= null;
	private SelectionModel<T>					selectionModel				= null;
	
	//Used to skip onRangeChanged() call when a column has been sorted.
	private Range								previousPage				= null;
	private Boolean								isUsingCheckBox				= false;
	
	
	
	/**
	 * 
	 */
	public SaruDataGrid() {

	}

	/**
	 * @param pageSize
	 */
	public SaruDataGrid(int pageSize) {
		super(pageSize);

	}

	/**
	 * @param keyProvider
	 */
	public SaruDataGrid(ProvidesKey<T> keyProvider) {
		super(keyProvider);

		this.keyProvider = keyProvider;
	}

	/**
	 * @param pageSize
	 * @param keyProvider
	 */
	public SaruDataGrid(int pageSize, ProvidesKey<T> keyProvider) {
		super(pageSize, keyProvider);

		this.keyProvider = keyProvider;
	}

	/**
	 * @param pageSize
	 * @param resources
	 */
	public SaruDataGrid(int pageSize, Resources resources) {
		super(pageSize, resources);

	}

	/**
	 * @param pageSize
	 * @param resources
	 * @param keyProvider
	 */
	public SaruDataGrid(int pageSize, Resources resources, ProvidesKey<T> keyProvider) {		
		super(pageSize, resources, keyProvider);
		
		this.keyProvider = keyProvider;
	}

	/**
	 * @param pageSize
	 * @param resources
	 * @param keyProvider
	 * @param loadingIndicator
	 */
	public SaruDataGrid(int pageSize, Resources resources, ProvidesKey<T> keyProvider, Widget loadingIndicator) {
		super(pageSize, resources, keyProvider, loadingIndicator);

		this.keyProvider = keyProvider;
	}

	/**
	 * 
	 */
	public void setUsingCheckBox() {
		this.isUsingCheckBox = true;
	}
	
	public void addColumn(SaruDataGridColumn<T, ?> column) {
		if( null == this.columns ) {
			this.columns = new ArrayList<>();
		}
		
		this.columns.add(column);
	}

	/**
	 * @param size
	 * @param exact
	 * @see com.google.gwt.view.client.AsyncDataProvider#updateRowCount(int, boolean)
	 */
	public void updateRowCount(int size, boolean exact) {
		dataProvider.updateRowCount(size, exact);
		this.setRowCount(size, exact);
	}

	/**
	 * @param start
	 * @param values
	 * @see com.google.gwt.view.client.AsyncDataProvider#updateRowData(int, java.util.List)
	 */
	public void updateRowData(List<T> values) {
		Range range = this.getVisibleRange();
		
		dataProvider.updateRowData(range.getStart(), values);
	}

	public void init() {
		this.setAutoHeaderRefreshDisabled(true);
		this.setEmptyTableWidget(new Label("No Data"));
		
		AsyncHandler sorthandler = new AsyncHandler(this) {

			/* (non-Javadoc)
			 * @see com.google.gwt.user.cellview.client.ColumnSortEvent.AsyncHandler#onColumnSort(com.google.gwt.user.cellview.client.ColumnSortEvent)
			 */
			@Override
			public void onColumnSort(ColumnSortEvent event) {
				super.onColumnSort(event);
				
				if(null == dataGridColumnSortHandler) {
					return;
				}
				
				@SuppressWarnings("unchecked")
				int columnIndex = getColumnIndex((Column<T, ?>) event.getColumn());
				dataGridColumnSortHandler.onColumnSort(columnIndex, event.isSortAscending(), getVisibleRange().getStart());
			}			
		};
		
		this.addColumnSortHandler(sorthandler);		
		
		if(this.isUsingCheckBox) {
			this.addRowCheckBox();
		}
		
		for (SaruDataGridColumn<T, ?> column : columns) {
			switch(column.getType()){
				case Text:
					this.addTextColumn(column);
					break;
				case ActionBar:
					this.enableActionBar(column);
					break;

				case ActionButton:
					this.enableActionButton(column);
					break;
				case Image:
					this.addImageColumn(column);
					break;
				case Button:
					this.addButton(column);
					break;
				case Toggle:
					this.addToggle(column);
					break;
				default:
					continue;
			}
		}
		
		this.dataProvider = new AsyncDataProvider<T>() {

			@Override
			protected void onRangeChanged(HasData<T> display) {
				if(null == pagerChangeHandler) {
					return;
				}
				
				Range range = display.getVisibleRange();	
				
				int cursor = range.getStart();
				
				if( null == previousPage ) {
					previousPage = range;
					return;
				}else if( previousPage.getStart() == cursor ) {
					return;
				}
				previousPage = range;
				
				pagerChangeHandler.onPageChange(cursor);
			}
		};
		
		this.dataProvider.addDataDisplay(this);
	}
	
	/**
	 * @param dataGridColumnSortHandler the dataGridColumnSortHandler to set
	 */
	public void setDataGridColumnSortHandler(AsyncDataGridColumnSortHandler dataGridColumnSortHandler) {
		this.dataGridColumnSortHandler = dataGridColumnSortHandler;
	}
	
	/**
	 * @param pagerChangeHandler the pagerChangeHandler to set
	 */
	public void setPagerChangeHandler(AsyncPagerChangeHandler pagerChangeHandler) {
		this.pagerChangeHandler = pagerChangeHandler;
	}

	/**
	 * Show check box at the first column of the data grid. Please call this method at 1st place.
	 */
	private void addRowCheckBox() {
		this.selectionModel	= new MultiSelectionModel<>(this.keyProvider);
		this.setSelectionModel(selectionModel, DefaultSelectionEventManager.<T>createCheckboxManager());
		
		Column<T, Boolean> checkColumn = new Column<T, Boolean>(new CheckboxCell(true, false)) {

			@Override
			public Boolean getValue(T object) {
				return selectionModel.isSelected(object);
			}
		};
		
		this.addColumn(checkColumn, SafeHtmlUtils.fromSafeConstant("<br />"));
		this.setColumnWidth(checkColumn, 40, Unit.PX);
		
		
	}
	
	private void enableActionBar(final SaruDataGridColumn<T, ?> columnDetails) {
		throw new UnsupportedOperationException("not supported yet");
	}
	
	private void enableActionButton(final SaruDataGridColumn<T, ?> columnDetails) {
		SaruActionCell<T> cell = new SaruActionCell<T>(columnDetails.getHeader(), new SaruActionCell.Delegate<T>() {

			@Override
			public void execute(T object) {
				columnDetails.getCellCommandHandler().execute(object);				
			}
		}, new SaruActionCell.Disability<T>() {

			@Override
			public Boolean isDisabled(T object) {
				if(null == columnDetails.getCellDisabilityHandler()) {
					return false;
				}
				
				return columnDetails.getCellDisabilityHandler().isDisabled(object);
			}
		});		
		
		Column<T, T> column = new Column<T, T>(cell){

			@Override
			public T getValue(T object) {
				return object;
			}
			
		};
		
		this.addColumn(column, "");
		this.setColumnWidth(column, columnDetails.getWidth(), Unit.PX);
	}
	
	private void addButton(final SaruDataGridColumn<T, ?> columnDetails) {
		throw new UnsupportedOperationException("not supported yet");
	}
	
	private void addToggle(final SaruDataGridColumn<T, ?> columnDetails) {
		throw new UnsupportedOperationException("not supported yet");
	}
	
	private void addTextColumn(final SaruDataGridColumn<T, ?> columnDetails) {
		Column<T, String> column = new Column<T, String>(new TextCell()){

			@Override
			public String getValue(T object) {
				return (String) columnDetails.getCellValueFetchHandler().getValue(object);
			}
			
		};
		
		column.setSortable(columnDetails.isSortable());
		
		this.addColumn(column, columnDetails.getHeader());
		this.setColumnWidth(column, columnDetails.getWidth(), Unit.PX);
	}
	
	private void addImageColumn(final SaruDataGridColumn<T, ?> columnDetails) {
		//Photo column
		ButtonCell imageCell = new ButtonCell(){

			/* (non-Javadoc)
			 * @see com.google.gwt.cell.client.ImageCell#render(com.google.gwt.cell.client.Cell.Context, java.lang.String, com.google.gwt.safehtml.shared.SafeHtmlBuilder)
			 */
			@Override
			public void render(Context context, String value, SafeHtmlBuilder sb) {
				if(null == value){
					return;
				}
				
				final Image img = new Image(value);
				img.setWidth("50px");
				img.setHeight("50px");
								
				
				SafeHtml html = SafeHtmlUtils.fromTrustedString(img.toString());
				sb.append(html);			
			}

			/* (non-Javadoc)
			 * @see com.google.gwt.cell.client.ButtonCell#onBrowserEvent(com.google.gwt.cell.client.Cell.Context, com.google.gwt.dom.client.Element, java.lang.String, com.google.gwt.dom.client.NativeEvent, com.google.gwt.cell.client.ValueUpdater)
			 */
			@Override
			public void onBrowserEvent(Context context, Element parent, String value, NativeEvent event,
					ValueUpdater<String> valueUpdater) {				
				super.onBrowserEvent(context, parent, value, event, valueUpdater);
				
				switch( DOM.eventGetType((Event)event) ){
					case Event.ONCLICK:
						final PopupPanel imgPopup = new PopupPanel(true);
						imgPopup.setAnimationEnabled(true);
												
						imgPopup.setWidth("500px");
						imgPopup.setHeight("500px");
					
						
						Image largeImg = new Image(value);
						
						largeImg.addClickHandler(new ClickHandler() {				
							@Override
							public void onClick(ClickEvent event) {
								imgPopup.hide();					
							}
						});
						
						imgPopup.add(largeImg);
						
						imgPopup.center();						
					break;										
				}
			}			
		};
		
		Column<T, String> imageColumn = new Column<T, String>(imageCell) {
			@Override
			public String getValue(T object) {
				return (String) columnDetails.getCellValueFetchHandler().getValue(object);
			}			
		};
		this.addColumn(imageColumn, "Photo");
		this.setColumnWidth(imageColumn, columnDetails.getWidth(), Unit.PX);
	}
}
