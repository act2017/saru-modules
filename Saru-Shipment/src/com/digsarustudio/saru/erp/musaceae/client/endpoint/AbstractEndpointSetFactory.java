/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.endpoint;

import com.digsarustudio.banana.designpattern.abstractfactory.GenericAbstractFactory;

/**
 * The sub-type of this factory generates endpoint factories for the specific endpoint.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface AbstractEndpointSetFactory extends GenericAbstractFactory
												//For legancy support
												  , com.digsarustudio.musa.mvp.AbstractEndpointSetFactory{
}
