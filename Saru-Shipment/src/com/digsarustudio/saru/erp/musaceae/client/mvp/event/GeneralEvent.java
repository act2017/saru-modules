/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.event;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * The subclass of this type is working as the event transited between {@link AppController} and {@link Presenter} 
 * via {@link EventBus}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 */
public abstract class GeneralEvent<H extends EventHandler> extends GwtEvent<H> {
	protected EventBus eventBus	= null;
	
	public void setEventBus(EventBus eventBus) {
		this.eventBus = eventBus;
	}
	
	public EventBus getEventBus() {
		return this.eventBus;
	}
	
	public abstract void fire();
}
