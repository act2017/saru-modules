/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.datagrid;

/**
 * This handles page range changing event.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface AsyncPagerChangeHandler {
	/**
	 * The page change
	 * 
	 * @param cursor The location(index) of the first data to deal with.
	 */
	void onPageChange(Integer cursor);
}
