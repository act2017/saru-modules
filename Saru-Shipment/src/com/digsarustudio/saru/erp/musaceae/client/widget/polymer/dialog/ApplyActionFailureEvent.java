/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.polymer.dialog;

import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.ApplyActionFailureHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.GwtEvent;

/**
 * This event indicates an apply action cannot be completed.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ApplyActionFailureEvent extends GwtEvent<ApplyActionFailureHandler> {
	public static Type<ApplyActionFailureHandler> TYPE = new Type<>();

	private String data = null;
	
	public ApplyActionFailureEvent() {
		
	}
	
	public ApplyActionFailureEvent(String data) {
		this.setData(data);
	}
	
	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}
	/**
	 * To register this event into an event bus
	 * 
	 * @param eventbus The event bus to register
	 * 
	 * @param handler The event handler of this event
	 */
	public static void register(EventBus eventbus, ApplyActionFailureHandler handler) {
		eventbus.addHandler(TYPE, handler);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<ApplyActionFailureHandler> getAssociatedType() {
		return TYPE;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(ApplyActionFailureHandler handler) {
		handler.onApplyActionFailure(this);
		
	}
}
