/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.event;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * This exception event wraps {@link GwtEvent} for the client code to send an event to notify controller there is an exception happened and 
 * the controller has to handle it.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public abstract class ExceptionEvent<H extends EventHandler> extends GwtEvent<H> {
	protected Throwable	cause		= null;
	protected String	msg			= null;
	protected EventBus	eventBus	= null;
	
	public void setCause(Throwable cause) {
		this.cause = cause;
	}
	
	public Throwable getCause() {
		return this.cause;
	}
	
	public void setMessage(String msg) {
		this.msg = msg;
	}
	
	public String getMessage() {
		return this.msg;
	}
	
	public void setEventBus(EventBus eventBus) {
		this.eventBus = eventBus;
	}
	
	public EventBus getEventBus() {
		return this.eventBus;
	}
	
	public abstract void fire();
}
