/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget;

import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.TextBox;

/**
 * Represents a custom text box which can be operated with placeholder property.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SaruTextBox extends TextBox{

	/**
	 * Constructs a saru text box
	 */
	public SaruTextBox() {
	}

	/**
	 * Set the place holder to the custom text box
	 * 
	 * @param text The text for place holder as a hint
	 */
	public void setPlaceHolder(String text){
		String placeholder = (null != text) ? text : "";
		
		this.getElement().setPropertyString("placeholder", placeholder);
	}
	
	/**
	 * Set the value of this text box must be set
	 * 
	 * @param isRequired true if this text box must be set, otherwise false.
	 */
	public void setRequired(Boolean isRequired){
		if(!isRequired){
			return;
		}
		
		this.getElement().setPropertyBoolean("required", isRequired);
	}
	
	/**
	 * Clear the content
	 */
	public void clear(){
		this.getElement().setPropertyString("value", "");
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.ValueBoxBase#addChangeHandler(com.google.gwt.event.dom.client.ChangeHandler)
	 */
	@Override
	public HandlerRegistration addChangeHandler(ChangeHandler handler) {
		return super.addChangeHandler(handler);
	}	
}
