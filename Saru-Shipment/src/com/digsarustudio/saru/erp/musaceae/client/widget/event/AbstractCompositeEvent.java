/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.ui.Composite;

/**
 * Root of composite {@link Composite} event object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public abstract class AbstractCompositeEvent<H extends EventHandler> extends GwtEvent<H>{
	private Composite sourceWidget;
	private Object sourceData; 
	
	/**
	 * Construct an event object
	 */
	public AbstractCompositeEvent() {
	}

	/**
	 * Constructs an event with a source widget
	 * 
	 * @param sourceWidget The source widget to carry
	 */
	public AbstractCompositeEvent(Composite sourceWidget) {
		super();
		this.sourceWidget = sourceWidget;
	}

	/**
	 * Constructs an event with a source data
	 * @param sourceData The source data to carry
	 */
	public AbstractCompositeEvent(Object sourceData) {
		super();
		this.sourceData = sourceData;
	}

	/**
	 * @return the sourceWidget
	 */
	public Composite getSourceWidget() {
		return sourceWidget;
	}

	/**
	 * @param sourceWidget the sourceWidget to set
	 */
	public void setSourceWidget(Composite sourceWidget) {
		this.sourceWidget = sourceWidget;
	}

	/**
	 * @return the sourceData
	 */
	public Object getSourceData() {
		return sourceData;
	}

	/**
	 * @param sourceData the sourceData to set
	 */
	public void setSourceData(Object sourceData) {
		this.sourceData = sourceData;
	}	
}
