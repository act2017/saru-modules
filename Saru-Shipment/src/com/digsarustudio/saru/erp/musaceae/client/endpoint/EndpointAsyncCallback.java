/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.endpoint;

import java.util.logging.Logger;

import javax.security.auth.login.CredentialException;

import com.digsarustudio.saru.erp.musaceae.client.verification.CredentialExpiredEvent;
import com.digsarustudio.saru.erp.musaceae.client.verification.CredentialExpiredException;
import com.digsarustudio.saru.erp.musaceae.client.verification.CredentialVerificationCallback;
import com.digsarustudio.saru.erp.musaceae.client.verification.InvalidCredentialException;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The wrapper of {@link AsyncCallback} in order to handle {@link CredentialException} 
 * to prevent a user use the service without permission.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @since		1.0.3
 * @version		1.0.6
 * <br>
 * Note:<br>
 * 				1.0.6	->	To support com.digsarustudio.saru.erp.musaceae.shared.verification.InvalidCredentialException.
 *
 */
public abstract class EndpointAsyncCallback<T> implements AsyncCallback<T> {
	Logger logger = Logger.getLogger(this.getClass().getName());
	
	private EventBus						eventBus						= null;
	private CredentialVerificationCallback	credentialVerificationCallback	= null;
	
	public EndpointAsyncCallback() {
		
	}
	
	public EndpointAsyncCallback(EventBus eventBus, CredentialVerificationCallback credentialVerificationCallback){
		this.eventBus						= eventBus;
		this.credentialVerificationCallback	= credentialVerificationCallback;
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.rpc.AsyncCallback#onFailure(java.lang.Throwable)
	 */
	@Override
	public void onFailure(Throwable caught) {
		try {
			throw caught;
		}catch(CredentialExpiredException | InvalidCredentialException | com.digsarustudio.saru.erp.musaceae.shared.verification.CredentialException e) {
			if(null == this.eventBus || null == this.credentialVerificationCallback) {
				return;
			}
			
			CredentialExpiredEvent.fireEvent(eventBus, this.credentialVerificationCallback);
		}catch(Throwable e) {
			//Bug: this.onFailed won't call override function but call #onFailed instead.<br>
			logger.severe(e.getMessage());
		}		
	}
	
	/**
	 * To handle the rest of exception, please override this function instead of {@link #onFailure(Throwable)} if 
	 * you wanna handle more exception.<br>
	 * 
	 * @param caught The exception caught from remote service.
	 */
	public void onFailed(Throwable caught) {
		//Let the subclass do this.
	}
}
