/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.verification;

/**
 * This exception indicates the credential of user is expired.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 * @deprecated	1.0.4	please use {@link com.digsarustudio.saru.erp.musaceae.shared.verification.CredentialExpiredException} instead
 */
@SuppressWarnings("serial")
public class CredentialExpiredException extends Exception {

	/**
	 * 
	 */
	public CredentialExpiredException() {

	}

	/**
	 * @param message
	 */
	public CredentialExpiredException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public CredentialExpiredException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public CredentialExpiredException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public CredentialExpiredException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
