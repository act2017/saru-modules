/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.polymer.dialog;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;

/**
 * The sub-type has the ability to receive {@link ClickHandler} and call it when the dialog receive click, cancel or reset action.<br>
 * Normally, this handler is used for {@link SaruDialog} to return apply, cancel, and reset event from the event handler to the presenter.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface HasEditorDialogClickHandler {
	HandlerRegistration addApplyClickHandler(ClickHandler handler);
	HandlerRegistration addCancelClickHandler(ClickHandler handler);
	HandlerRegistration addCloseClickHandler(ClickHandler handler);
	HandlerRegistration addResetClickHandler(ClickHandler handler);
}
