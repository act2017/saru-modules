/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.polymer;

import com.google.gwt.event.shared.EventHandler;

/**
 * This handler handles {@link TextChangeEvent} for the text box-like widget.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface TextChangeHandler extends EventHandler {
	void onTextChange(TextChangeEvent event);
}
