/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.view;

import com.digsarustudio.saru.erp.musaceae.client.mvp.presenter.Presenter;

/**
 * In supervising view, it can fetch data from the model via a binding interface.
 * If you are considering to use UI Binder to implement the user interface, you might choose this interface.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.2
 * @since		1.0.0
 *
 */
public interface SupervisingView extends PassiveView{
	/**
	 * Assign a presenter for the view to handle the user events
	 * 
	 * @param presenter handling user events
	 */
	void setPresenter(Presenter presenter);
	
	/**
	 * To clear the data in this view
	 * 
	 * @since 1.0.1
	 */
	void clear();
	
	/**
	 * To rest the data to the default in this view
	 * 
	 * @since 1.0.1
	 */
	void reset();
}
