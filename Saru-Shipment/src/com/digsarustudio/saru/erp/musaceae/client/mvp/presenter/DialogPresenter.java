/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.presenter;

/**
 * This presenter receive {@link #onConfirmed()}, {@link #onCancelled()}, and {@link #onReset()} from the client code to 
 * deal with the event triggered by {@link SaruDialog}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface DialogPresenter extends Presenter {
	void onConfirmed();
	void onCancelled();
	void onReset();
	void onClose();
}
