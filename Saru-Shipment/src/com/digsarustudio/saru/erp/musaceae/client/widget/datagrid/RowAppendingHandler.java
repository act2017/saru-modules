/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.datagrid;

/**
 * The sub-type can handles the row appending behaviour.<br>
 *
 * TODO use a generic handler instead of this.<br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface RowAppendingHandler {
	void onAppendRow();
}
