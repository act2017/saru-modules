/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget;

import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.view.client.Range;

/**
 * This class wraps {@link SimplePager}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class SaruPager extends SimplePager {

	/**
	 * 
	 */
	public SaruPager() {
		this.setRangeLimited(true);
	}

	/**
	 * @param location
	 */
	public SaruPager(TextLocation location) {
		super(location);
		this.setRangeLimited(true);
	}

	/**
	 * @param location
	 * @param showFastForwardButton
	 * @param showLastPageButton
	 */
	public SaruPager(TextLocation location, boolean showFastForwardButton, boolean showLastPageButton) {
		super(location, showFastForwardButton, showLastPageButton);
		
		this.setRangeLimited(true);
	}

	/**
	 * @param location
	 * @param showFastForwardButton
	 * @param fastForwardRows
	 * @param showLastPageButton
	 */
	public SaruPager(TextLocation location, boolean showFastForwardButton, int fastForwardRows,
			boolean showLastPageButton) {
		super(location, showFastForwardButton, fastForwardRows, showLastPageButton);
		this.setRangeLimited(true);
	}

	/**
	 * @param location
	 * @param resources
	 * @param showFastForwardButton
	 * @param fastForwardRows
	 * @param showLastPageButton
	 */
	public SaruPager(TextLocation location, Resources resources, boolean showFastForwardButton, int fastForwardRows,
			boolean showLastPageButton) {
		super(location, resources, showFastForwardButton, fastForwardRows, showLastPageButton);
		this.setRangeLimited(true);
	}

	/**
	 * @param location
	 * @param resources
	 * @param showFastForwardButton
	 * @param fastForwardRows
	 * @param showLastPageButton
	 * @param imageButtonConstants
	 */
	public SaruPager(TextLocation location, Resources resources, boolean showFastForwardButton, int fastForwardRows,
			boolean showLastPageButton, ImageButtonsConstants imageButtonConstants) {
		super(location, resources, showFastForwardButton, fastForwardRows, showLastPageButton, imageButtonConstants);
		this.setRangeLimited(true);
	}

	/**
	 * @param location
	 * @param resources
	 * @param showFastForwardButton
	 * @param fastForwardRows
	 * @param showLastPageButton
	 * @param showFirstPageButton
	 * @param imageButtonConstants
	 */
	public SaruPager(TextLocation location, Resources resources, boolean showFastForwardButton, int fastForwardRows,
			boolean showLastPageButton, boolean showFirstPageButton, ImageButtonsConstants imageButtonConstants) {
		super(location, resources, showFastForwardButton, fastForwardRows, showLastPageButton, showFirstPageButton,
				imageButtonConstants);
		this.setRangeLimited(true);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.user.cellview.client.SimplePager#setPageStart(int)
	 */
	@Override
	public void setPageStart(int index) {
//Ref:https://stackoverflow.com/questions/6057141/simplepager-row-count-is-working-incorrectly		
	 if (this.getDisplay() != null) {
          Range range = getDisplay().getVisibleRange();
          int pageSize = range.getLength();
          if (!isRangeLimited() && getDisplay().isRowCountExact()) {
            index = Math.min(index, getDisplay().getRowCount() - pageSize);
          }
          index = Math.max(0, index);
          if (index != range.getStart()) {
            getDisplay().setVisibleRange(index, pageSize);
          }
        }  
	}

}
