/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.composite;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Widget;

/**
 * The FlexTable has been used to make the layout pretty
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class BlockTable extends Composite {

	private static BlockTableUiBinder uiBinder = GWT.create(BlockTableUiBinder.class);

	interface BlockTableUiBinder extends UiBinder<Widget, BlockTable> {
	}

	interface Styling extends CssResource{
		String cell();
	}
	
	@UiField Styling	style;
	@UiField FlexTable	table;
	
	//The columnCount cannot be 0 because this is the dividing number.
	private Integer columnCount = 4;
	private Integer index = 0;
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public BlockTable() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	
	public void add(BlockComposite widget){
		Integer row = this.index / this.columnCount;
		Integer col = this.index % this.columnCount;
		
		this.table.setWidget(row, col, widget);
		this.table.getCellFormatter().addStyleName(row, col, this.style.cell());
		
		this.index++;
	}
	
	public void clear(){
		this.index = 0;
		this.table.clear();
	}
	
	public void setColumnCount(Integer count){
		this.columnCount = count;
	}	
}
