/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.view.event;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * The sub-type can be add an event handler which handles {@link ValueChangeEvent}.<br>
 *
 * @param T The data type carried by {@link ValueChangeEvent}.<br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 * @deprecated	1.0.3	Please see the ReadMe file and use the event from com.digsarustudio.saru.erp.musaceae.client.widget.event
 *
 */
public interface HasValueChangeEventHandlers<T> {
	HandlerRegistration addValueChangeEventHandler(ValueChangeEventHandler<T> handler);
}
