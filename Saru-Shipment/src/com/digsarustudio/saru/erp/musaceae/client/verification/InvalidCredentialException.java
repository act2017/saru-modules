/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.verification;

/**
 * This exception indicates the credential of user is invalid.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 * @deprecated	1.0.4	please use {@link com.digsarustudio.saru.erp.musaceae.shared.verification.InvalidCredentialException} instead
 *
 */
@SuppressWarnings("serial")
public class InvalidCredentialException extends Exception {

	/**
	 * 
	 */
	public InvalidCredentialException() {

	}

	/**
	 * @param message
	 */
	public InvalidCredentialException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public InvalidCredentialException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public InvalidCredentialException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public InvalidCredentialException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
