/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.TextArea;

/**
 * This text area provides a placeholder for the client code to put it as comment.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.4
 *
 */
public class SaruTextArea extends TextArea {

	/**
	 * 
	 */
	public SaruTextArea() {

	}

	/**
	 * @param element
	 */
	public SaruTextArea(Element element) {
		super(element);

	}
	
	/**
	 * Clear the content
	 */
	public void clear(){
		this.getElement().setPropertyString("value", "");
	}

	/**
	 * Set the place holder to the custom text box.
	 * 
	 * @param text The text for place holder as a hint
	 */
	public void setPlaceHolder(String text){
		String placeholder = (null != text) ? text : "";
		
		this.getElement().setPropertyString("placeholder", placeholder);
	}
	
	/**
	 * Set the value of this text box must be set
	 * 
	 * @param isRequired true if this text box must be set, otherwise false.
	 */
	public void setRequired(Boolean isRequired){
		if(!isRequired){
			return;
		}
		
		this.getElement().setPropertyBoolean("required", isRequired);
	}
}
