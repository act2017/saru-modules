/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.datagrid;

/**
 * This handler returns the value of T which handles by client code.<br>
 *
 * @param T The data type for the grid cell to retrieve value.<br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface DataGridCellValueFetchedHanlder<T, S> {
	S getValue(T source);
}
