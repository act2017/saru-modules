/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.polymer.dialog;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiChild;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.polymer.paper.widget.PaperButton;
import com.vaadin.polymer.paper.widget.PaperDialog;

/**
 * This dialog wraps {@link PaperDialog}.<br>
 * 
 * Bug: When this widget has been used in more than 2 view, the second opened one cannot see the content in this dialog.<br>
 * 		Failed in Chorme, but works fine in FireFox.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SaruDialog extends Composite implements HasEditorDialogClickHandler{

	private static SaruDialogUiBinder uiBinder = GWT.create(SaruDialogUiBinder.class);

	interface SaruDialogUiBinder extends UiBinder<Widget, SaruDialog> {
	}

	@UiField PaperDialog		dialog;
	@UiField HTMLPanel		dialogCanvas;
	@UiField PaperButton		closeBtn;
	@UiField PaperButton		resetBtn;
	@UiField PaperButton		cancelBtn;
	@UiField PaperButton		applyBtn;
	@UiField ScrollPanel		mainScroll;

	private ClickHandler		cancelClickHandler	= null;
	private ClickHandler		closeClickHandler	= null;
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public SaruDialog() {
		initWidget(uiBinder.createAndBindUi(this));
		sinkEvents(Event.ONKEYDOWN);
	}

	public void showCloseButton() {
		this.closeBtn.setVisible(true);
	}
	
	public void hideCloseButton() {
		this.closeBtn.setVisible(false);
	}

	public void showResetButton() {
		this.resetBtn.setVisible(true);
	}
	
	public void hideResetButton() {
		this.resetBtn.setVisible(false);
	}

	public void showCancelButton() {
		this.cancelBtn.setVisible(true);
	}
	
	public void hideCancelButton() {
		this.cancelBtn.setVisible(false);
	}

	public void showApplyButton() {
		this.applyBtn.setVisible(true);
	}
	
	public void hideApplyButton() {
		this.applyBtn.setVisible(false);
	}
	
	@UiChild(tagname = "canvas")
	public void addCanvas(Widget widget) {	
//To let client code has the ability to handle multiple view in dialog
//		this.dialogCanvas.clear();
		this.dialogCanvas.add(widget);
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.Composite#onBrowserEvent(com.google.gwt.user.client.Event)
	 */
	@Override
	public void onBrowserEvent(Event event) {
		switch(DOM.eventGetType(event)) {
			case Event.ONKEYDOWN:
				if( event.getKeyCode() == KeyCodes.KEY_ESCAPE ) {
					if(null != this.cancelClickHandler) {
						this.cancelClickHandler.onClick(null);
					}
										
					if(null != this.closeClickHandler) {
						this.closeClickHandler.onClick(null);
					}
				}
				break;
		}
		
		super.onBrowserEvent(event);
	}

	/**
	 * 
	 * @see com.vaadin.polymer.paper.widget.PaperDialog#open()
	 */
	public void open() {
		this.scroll2TopLeft();
		dialog.open();
	}

	/**
	 * 
	 * @see com.vaadin.polymer.paper.widget.PaperDialog#center()
	 */
	public void center() {
		this.scroll2TopLeft();
		dialog.center();
	}

	/**
	 * 
	 * @see com.vaadin.polymer.paper.widget.PaperDialog#close()
	 */
	public void close() {
		dialog.close();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.onlinestore.console.client.widget.HasEditorDialogClickHandler#addApplyClickHandler(com.google.gwt.event.dom.client.ClickHandler)
	 */
	@Override
	public HandlerRegistration addApplyClickHandler(ClickHandler handler) {
		return this.applyBtn.addClickHandler(handler);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.onlinestore.console.client.widget.HasEditorDialogClickHandler#addCancelClickHandler(com.google.gwt.event.dom.client.ClickHandler)
	 */
	@Override
	public HandlerRegistration addCancelClickHandler(ClickHandler handler) {
		this.cancelClickHandler = handler;
		return this.cancelBtn.addClickHandler(handler);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.onlinestore.console.client.widget.HasEditorDialogClickHandler#addResetClickHandler(com.google.gwt.event.dom.client.ClickHandler)
	 */
	@Override
	public HandlerRegistration addResetClickHandler(ClickHandler handler) {
		return this.resetBtn.addClickHandler(handler);
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.polymer.dialog.HasEditorDialogClickHandler#addCloseClickHandler(com.google.gwt.event.dom.client.ClickHandler)
	 */
	@Override
	public HandlerRegistration addCloseClickHandler(ClickHandler handler) {
		this.closeClickHandler = handler;
		return this.closeBtn.addClickHandler(handler);
	}

	public void scroll2TopLeft() {
		this.mainScroll.scrollToTop();
		this.mainScroll.scrollToLeft();
	}
}
