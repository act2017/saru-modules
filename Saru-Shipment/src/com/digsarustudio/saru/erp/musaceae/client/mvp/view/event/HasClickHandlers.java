/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.view.event;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * This sub-type has the ability to handle {@link ClickEvent} and notify the client call.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 * @deprecated	1.0.3	Please see the ReadMe file and use the event from com.digsarustudio.saru.erp.musaceae.client.widget.event
 *
 */
public interface HasClickHandlers<T> {
	HandlerRegistration addClickHandler(final ClickHandler<T> handler);
}
