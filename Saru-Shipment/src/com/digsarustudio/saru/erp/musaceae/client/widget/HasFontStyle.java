/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget;

/**
 * The sub-type of this interface can be set font style
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface HasFontStyle {
	
	/**
	 * To set font size
	 * 
	 * @param size the size of font. It could be in PX or EM.
	 */
	void setFontSize(String size);
	
	/**
	 * To set the weight of font
	 * 
	 * @param weight This could be bold or normal.
	 */
	void setFontWeight(String weight);
}
