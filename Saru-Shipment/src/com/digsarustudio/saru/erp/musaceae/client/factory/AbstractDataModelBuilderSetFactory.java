/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.factory;

import com.digsarustudio.banana.designpattern.abstractfactory.GenericAbstractFactory;

/**
 * The sub-type of this factory generates the builders of specific data models for the endpoint to use.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface AbstractDataModelBuilderSetFactory extends GenericAbstractFactory{

}
