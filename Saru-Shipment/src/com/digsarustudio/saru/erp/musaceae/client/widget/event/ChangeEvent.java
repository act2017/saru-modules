/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * This event indicates the status or value of sub widget has been changed.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 */
public class ChangeEvent extends GwtEvent<ChangeHandler> {
	public static Type<ChangeHandler> TYPE	= new Type<>();
	
	private Object sourceData = null;
	
	/**
	 * 
	 */
	public ChangeEvent() {

	}
	
	public ChangeEvent(Object sourceData) {
		this.sourceData = sourceData;
	}

	/**
	 * @return the sourceData
	 */
	public Object getSourceData() {
		return sourceData;
	}

	/**
	 * @param sourceData the sourceData to set
	 */
	public void setSourceData(Object sourceData) {
		this.sourceData = sourceData;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<ChangeHandler> getAssociatedType() {

		return TYPE;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(ChangeHandler handler) {
		handler.onChange(this);

	}
}
