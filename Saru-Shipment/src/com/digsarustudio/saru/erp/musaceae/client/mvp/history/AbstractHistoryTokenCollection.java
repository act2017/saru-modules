/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.mvp.history;

import java.util.Iterator;
import java.util.List;

import com.digsarustudio.banana.utils.InvalidTokenFormatException;


/**
 * The base class of history token collection
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public abstract class AbstractHistoryTokenCollection implements HistoryTokenCollection{
	/**
	 * The hyphen between each token
	 */
	protected final String HYPHEN = ";";
	
	/**
	 * Constructs a history token collection
	 */
	public AbstractHistoryTokenCollection(){
		
	}
	
	/**
	 * @throws InvalidTokenFormatException 
	 * 
	 */
	public AbstractHistoryTokenCollection(String tokenString) throws InvalidTokenFormatException {
		this.parse(tokenString);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		List<HistoryToken> tokens = this.getTokens();
		
		StringBuilder builder = new StringBuilder();
		Integer counter = 0;
		
		Iterator<HistoryToken> iterator = tokens.iterator();
		
		while (iterator.hasNext()) {
			HistoryToken historyToken = iterator.next();
			
			if(counter++ != 0){
				builder.append(HYPHEN);
			}
			
			builder.append(historyToken);
		}		
		
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<HistoryToken> iterator() {
		return this.getTokens().iterator();
	}

	/**
	 * The sub-class has to implement this method for the toString() to get the details of tokens.
	 * 
	 * @return A list of tokens for toString()
	 */
	abstract protected List<HistoryToken> getTokens();
	
	/**
	 * The sub-class has to implement this method to parse incoming string
	 * 
	 * @param tokenString The string of incoming token
	 * 
	 * @throws InvalidTokenFormatException When the incoming string cannot be parsed as a history token collection.
	 */
	abstract protected void parse(String tokenString) throws InvalidTokenFormatException;
}
