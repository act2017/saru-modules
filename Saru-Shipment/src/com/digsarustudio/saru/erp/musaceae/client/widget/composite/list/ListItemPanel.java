/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.composite.list;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.client.widget.event.HasChangeHandlers;
import com.digsarustudio.saru.erp.musaceae.client.widget.polymer.HasDeleteHandler;
import com.google.gwt.user.client.ui.IsWidget;

/**
 * The sub-type can be added into {@link ListPanel} and cooperating with it.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 */
public interface ListItemPanel<T> extends HasDeleteHandler
									   , HasChangeHandlers
									   , IsWidget {
	/**
	 * 
	 * The builder for {@link ListItemPanel}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder<S> extends ObjectBuilder<ListItemPanel<S>>{
	
	}
	
	void setSourceData(T source);
	T getSourceData();
	
	/**
	 * Returns T if the value has been set or changed, otherwise null returned to indicate that 
	 * there is nothing different.<br>
	 * 
	 * @return T if the value has been set or changed, otherwise null returned to indicate that 
	 * 			 there is nothing different.
	 */
	T getValue();
}
