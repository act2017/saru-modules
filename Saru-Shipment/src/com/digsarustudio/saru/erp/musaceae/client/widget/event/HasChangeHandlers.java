/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.event;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * The sub-type has the ability to handle {@link CompositeChangeEvent}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface HasChangeHandlers {
	HandlerRegistration addChangeHandler(ChangeHandler handler);
}
