package com.digsarustudio.saru.erp.musaceae.client.mvp.controller;

import com.google.gwt.user.client.ui.HasWidgets;

/**
 * The definition of app controller
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface AppController {
	void go(HasWidgets container);
}
