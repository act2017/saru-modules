/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.composite;

import com.digsarustudio.saru.erp.musaceae.client.widget.SaruImage;
import com.digsarustudio.saru.erp.musaceae.client.widget.SaruVerticalPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * A composite block which represents with the icon and caption of target item
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class IconCaptionBlock extends BlockComposite implements HasClickHandlers{
	interface Styling extends CssResource{
		String mouseOver();
	}
	
	private static IconCaptionBlockUiBinder uiBinder = GWT.create(IconCaptionBlockUiBinder.class);

	interface IconCaptionBlockUiBinder extends UiBinder<Widget, IconCaptionBlock> {
	}

	@UiField Styling			style;
	@UiField SaruVerticalPanel	mainFrame;
	@UiField SaruImage			icon;
	@UiField Label				caption;
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public IconCaptionBlock() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiHandler("mainFrame")
	protected void onMouseOverEvent(MouseOverEvent event) {
		this.icon.addStyleName(this.style.mouseOver());
		this.caption.addStyleName(this.style.mouseOver());
	}
	
	@UiHandler("mainFrame")
	protected void onMouseOutEvent(MouseOutEvent event) {
		this.icon.removeStyleName(this.style.mouseOver());
		this.caption.removeStyleName(this.style.mouseOver());
	}
		
	public void setIcon(String url){
		if( null == url ) {
			return;
		}
		
		this.icon.setUrl(url);
	}
	
	public void setIconContent(String content) {
		if(null == content) {
			return;
		}
		
		this.icon.setImageContent(content);
	}
	
	public void setIcon(ImageResource resource){
		this.icon.setResource(resource);
	}
	
	public void setCaption(String caption){
		this.caption.setText(caption);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.dom.client.HasClickHandlers#addClickHandler(com.google.gwt.event.dom.client.ClickHandler)
	 */
	@Override
	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return this.mainFrame.addClickHandler(handler);
	}
	
}
