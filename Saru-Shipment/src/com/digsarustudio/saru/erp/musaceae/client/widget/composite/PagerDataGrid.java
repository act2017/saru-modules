/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.composite;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.saru.erp.musaceae.client.widget.SaruPager;
import com.digsarustudio.saru.erp.musaceae.client.widget.datagrid.AsyncDataGridColumnSortHandler;
import com.digsarustudio.saru.erp.musaceae.client.widget.datagrid.AsyncPagerChangeHandler;
import com.digsarustudio.saru.erp.musaceae.client.widget.datagrid.HasRowAppendingHandler;
import com.digsarustudio.saru.erp.musaceae.client.widget.datagrid.HasRowCountPerPageHandler;
import com.digsarustudio.saru.erp.musaceae.client.widget.datagrid.RowAppendingHandler;
import com.digsarustudio.saru.erp.musaceae.client.widget.datagrid.RowCountPerPageHandler;
import com.digsarustudio.saru.erp.musaceae.client.widget.datagrid.SaruDataGrid;
import com.digsarustudio.saru.erp.musaceae.client.widget.datagrid.SaruDataGridColumn;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.Range;
import com.vaadin.polymer.paper.widget.PaperFab;

/**
 * The data grid widget goes with pager and number of row displaying switch.<br>
 * <br>
 * How to use this widget:<br>
 * 	1.Initiate {@link PagerDataGrid} before executing {@link #initWidget(Widget)}<br>
 * 	2.Setup properties of {@link PagerDataGrid}<br>
 *  3.Call {@link PagerDataGrid#init()} to apply settings to it.<br>
 *  4.Add {@link PagerDataGrid} to the parent container if it has been declared in java code.
 *  5.Set {@link PagerDataGrid} as "provided=true" if it has been declared in uiBinder.
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PagerDataGrid<T> extends Composite implements HasRowCountPerPageHandler, HasRowAppendingHandler {

	private static PagerDataGridUiBinder uiBinder = GWT.create(PagerDataGridUiBinder.class);

	@SuppressWarnings("rawtypes")
	interface PagerDataGridUiBinder extends UiBinder<Widget, PagerDataGrid> {
	}

	@UiField					HorizontalPanel	countPerPagePanel;
	@UiField					Label			whatPerPage;
	@UiField					ListBox			countPerPage;
	@UiField					PaperFab		addBtn;
	
	@UiField(provided = true)	SaruDataGrid<T>	grid;
	@UiField(provided = true)	SaruPager		pager;
	
	
	private List<RowCountPerPageHandler>				rowCountPerPageHandlers	= null;
	private List<RowAppendingHandler>					rowAppendingHandlers	= null;
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public PagerDataGrid(List<SaruDataGridColumn<T, ?>> columns, AsyncDataGridColumnSortHandler sortHandler
															   , AsyncPagerChangeHandler pageChangeHandler) {
		this.initDataGrid(columns, sortHandler, pageChangeHandler);		
		this.initPager();
		
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@UiHandler("countPerPage")
	protected void onMaxRowPerPageClickEvent(ChangeEvent event) {
		if(null == this.rowCountPerPageHandlers) {
			return;
		}
		
		Integer rowPerPage = Integer.parseInt(this.countPerPage.getSelectedValue());
		
		for (RowCountPerPageHandler handler : this.rowCountPerPageHandlers) {
			handler.onRowCountPerPageChanged(rowPerPage);
		}		
	}
	
	@UiHandler("addBtn")
	protected void onAddButtonClickEvent(ClickEvent event) {
		if(null == this.rowAppendingHandlers) {
			return;
		}
		
		for (RowAppendingHandler handler : rowAppendingHandlers) {
			handler.onAppendRow();
		}
	}	
	
	public void setMaxRowPerPage(Integer count) {
		Integer itemCount = this.countPerPage.getItemCount();
		for (int i = 0; i < itemCount; i++) {
			if( Integer.parseInt(this.countPerPage.getValue(i)) != count ) {
				continue;
			}
			
			this.countPerPage.setItemSelected(i, true);
			break;
		}
	
		Range range = this.grid.getVisibleRange();
		this.grid.setVisibleRange(range.getStart(), count);
	}
	
	/**
	 * @param values
	 * @see com.digsarustudio.musa.widget.datagrid.SaruDataGrid#updateRowData(java.util.List)
	 */
	public void updateRowData(List<T> values) {
		grid.updateRowData(values);
	}
	
	public void showCountPerPage() {
		this.countPerPagePanel.setVisible(true);
	}
	
	public void hideCountPerPage() {
		this.countPerPagePanel.setVisible(false);
	}

	public Integer getDisplayPerPageCount() {
		return Integer.parseInt(this.countPerPage.getSelectedValue());
	}
	
	public void updateTotolCount(Integer total) {
		this.grid.updateRowCount(total, true);				
	}
	
	public void setEstimateRowCount(Integer count) {
		this.grid.updateRowCount(count, false);
	}
	
	public void updatePageVisibleRange(Integer cursor, Integer length) {
		Range range = new Range(cursor, length);
		this.grid.setVisibleRangeAndClearData(range, false);
	}	
	
	public Integer getCurrentCursor() {
		return this.grid.getVisibleRange().getStart();
	}
	
	public void setAddButtonVisible(Boolean visible) {
		this.addBtn.setVisible(visible);
	}
	
	public void setWhatPerPage(String name) {
		if(null == name) {
			return;
		}
		
		this.whatPerPage.setText(name);
	}
	
	public void hideAddButton() {
		this.addBtn.setVisible(false);
	}
	
	public void showAddButton() {
		this.addBtn.setVisible(true);
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.datagrid.HasRowCountPerPageHandler#addRowCountPerPageHandler(com.digsarustudio.musa.widget.datagrid.RowCountPerPageHandler)
	 */
	@Override
	public HandlerRegistration addRowCountPerPageHandler(final RowCountPerPageHandler handler) {
		if(null == this.rowCountPerPageHandlers) {
			this.rowCountPerPageHandlers = new ArrayList<>();
		}
		
		this.rowCountPerPageHandlers.add(handler);

		return new HandlerRegistration() {
			
			@Override
			public void removeHandler() {
				rowCountPerPageHandlers.remove(handler);
			}
		};
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.datagrid.HasRowAppendingHandler#addAppendRowHandler(com.digsarustudio.musa.widget.datagrid.RowAppendingHandler)
	 */
	@Override
	public HandlerRegistration addAppendRowHandler(final RowAppendingHandler handler) {
		if(null == this.rowAppendingHandlers) {
			this.rowAppendingHandlers = new ArrayList<>();
		}
		this.rowAppendingHandlers.add(handler);
		
		return new HandlerRegistration() {
			
			@Override
			public void removeHandler() {
				rowAppendingHandlers.remove(handler);				
			}
		};
	}

	private void initDataGrid(List<SaruDataGridColumn<T, ?>> columns, AsyncDataGridColumnSortHandler sortHandler
																	, AsyncPagerChangeHandler pageChangeHandler) {
		this.grid = new SaruDataGrid<>();
		this.grid.setDataGridColumnSortHandler(sortHandler);
		this.grid.setPagerChangeHandler(pageChangeHandler);
		
		for (SaruDataGridColumn<T, ?> column : columns) {
			this.grid.addColumn(column);
		}
		
		this.grid.init();
	}
	
	private void initPager() {
		SimplePager.Resources pagerResource = GWT.create(SaruPager.Resources.class);
		
		this.pager = new SaruPager(TextLocation.CENTER, pagerResource, true, 0, true);
		this.pager.setDisplay(this.grid);
	}

}
