/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.event;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * A widget that implements this interface provides registration for
 * {@link SearchEventHandler} instances.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface HasSearchEventHandler {
	/**
	   * Adds a {@link SearchEvent} handler.
	   * 
	   * @param handler the search event handler
	   * @return {@link HandlerRegistration} used to remove this handler
	   */
	HandlerRegistration addSearchEventHandler(SearchEvent.Handler handler);
}
