/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.client.widget.datagrid;

/**
 * The sub-type check the disability of the target value according to the result of checking.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public interface DataGridCellDisabilityHandler<T> {
	Boolean isDisabled(T value);
}
