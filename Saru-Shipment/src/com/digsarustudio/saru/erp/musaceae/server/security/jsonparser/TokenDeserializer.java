/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.security.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.musaceae.shared.security.javabean.Token;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * The deserializer for Gson to parse {@link com.digsarustudio.banana.erp.general.security.Token} from a JSON string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class TokenDeserializer implements JsonDeserializer<Token> {

	/**
	 * 
	 */
	public TokenDeserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public Token deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		String token = json.getAsString();
		
		return Token.builder().setValue(token)
							  .build();
	}

}
