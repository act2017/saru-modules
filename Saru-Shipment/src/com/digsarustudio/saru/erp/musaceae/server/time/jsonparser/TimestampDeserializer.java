/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.time.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.musaceae.shared.time.javabean.Timestamp;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * The deserializer for Gson to parse {@link com.digsarustudio.banana.erp.general.time.Timestamp} from the JSON string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class TimestampDeserializer implements JsonDeserializer<Timestamp> {

	/**
	 * 
	 */
	public TimestampDeserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public Timestamp deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		String timestamp = json.getAsString();
		
		return Timestamp.builder().setValue(timestamp)
								  .build();
	}

}
