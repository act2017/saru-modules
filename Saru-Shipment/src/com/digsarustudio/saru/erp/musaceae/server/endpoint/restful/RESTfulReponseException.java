/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.endpoint.restful;

import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;

/**
 * This exception thrown when the HTTP response status code indicating that there is an error taken place.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class RESTfulReponseException extends EndpointException {

	/**
	 * 
	 */
	public RESTfulReponseException() {
		
	}

	/**
	 * @param message
	 */
	public RESTfulReponseException(String message) {
		super(message);
		
	}

	/**
	 * @param cause
	 */
	public RESTfulReponseException(Throwable cause) {
		super(cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 */
	public RESTfulReponseException(String message, Throwable cause) {
		super(message, cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public RESTfulReponseException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

}
