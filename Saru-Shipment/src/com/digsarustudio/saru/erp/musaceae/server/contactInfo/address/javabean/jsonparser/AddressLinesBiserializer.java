/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.contactInfo.address.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.AddressLines;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The deserializer and serializer of {@link AddressLines}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AddressLinesBiserializer implements JsonDeserializer<AddressLines>
												,JsonSerializer<AddressLines>{

	/**
	 * 
	 */
	public AddressLinesBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public AddressLines deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		String[] lines = null;
		if( json.isJsonArray() ) {
			lines = context.deserialize(json, String[].class);
		}else {
			lines 		= new String[1];
			lines[0]	= json.getAsString();
		}		
		
		return AddressLines.builder().setAddresses(lines)
									 .build();
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(AddressLines src, Type typeOfSrc, JsonSerializationContext context) {
		if( null == src || !src.validate() ) {
			return null;
		}
		
		JsonArray array = new JsonArray();
		
		for (String line : src.getAddresses()) {
			array.add(line);
		}
		
		return array;
	}

}
