/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.endpoint.http;

/**
 * The callback to receive the HTTP response asynchronisedly.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface HttpResponseCallback {
	void onResponse(String statusCode, String mediaContent);
}
