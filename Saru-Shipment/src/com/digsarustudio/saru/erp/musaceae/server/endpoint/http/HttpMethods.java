/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.endpoint.http;

import java.io.Serializable;

/**
 * The enumeration of HTTP methods which are GET, POST, UPDATE, and DELETE.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class HttpMethods implements Serializable{
	public static final HttpMethods		GET		= new HttpMethods("GET", "Get");
	public static final HttpMethods		POST	= new HttpMethods("POST", "Post");
	public static final HttpMethods		PUT		= new HttpMethods("PUT", "Put");
	public static final HttpMethods		DELETE	= new HttpMethods("DELETE", "Delete");

	private String value = null;
	private String display = null;

	private static HttpMethods[] VALUES = {GET, POST, PUT, DELETE};

	private HttpMethods(String value, String display) {
		this.value = value;
		this.display = display;
	}

	public String getValue() {
		return this.value;
	}

	public String getDisplayString() {
		return this.display;
	}

	public static HttpMethods fromValue(String value) {
		if (null == value) {
			return null;
		}

		HttpMethods rtn = null;

		for (HttpMethods target : VALUES) {
			if (!value.equals(target.getValue())) {
				continue;
			}

			rtn = target;
		}

		return rtn;
	}
}
