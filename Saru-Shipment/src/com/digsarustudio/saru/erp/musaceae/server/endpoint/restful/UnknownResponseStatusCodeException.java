/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.endpoint.restful;

/**
 * This exception thrown when the HTTP response status code cannot be recognised.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class UnknownResponseStatusCodeException extends Exception {

	/**
	 * 
	 */
	public UnknownResponseStatusCodeException() {
		
	}

	/**
	 * @param message
	 */
	public UnknownResponseStatusCodeException(String message) {
		super(message);
		
	}

	/**
	 * @param cause
	 */
	public UnknownResponseStatusCodeException(Throwable cause) {
		super(cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnknownResponseStatusCodeException(String message, Throwable cause) {
		super(message, cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public UnknownResponseStatusCodeException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

}
