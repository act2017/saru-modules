/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.musaceae.shared.javabean.UnitPrice;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * The deserializer of {@link UnitPrice} for Gson to convert it.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class UnitPriceDeserializer implements JsonDeserializer<UnitPrice> {

	/**
	 * 
	 */
	public UnitPriceDeserializer() {

	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public UnitPrice deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		JsonObject jsonObj = null;
		
		JsonElement	priceElem	= null;
		JsonElement taxElem		= null;
		JsonElement incTaxElem	= null;
		
		String	price			= null;
		
		if(!json.isJsonObject()) {
			if(null != json && !json.isJsonNull()) {
				price = json.getAsString();
			}
		}else {
			jsonObj = json.getAsJsonObject();
			
			priceElem	= jsonObj.get(UnitPrice.ATTR_PRICE);
			taxElem		= jsonObj.get(UnitPrice.ATTR_TAX);
			incTaxElem	= jsonObj.get(UnitPrice.ATTR_INCLUDING_TAX);						
		}
		
		
		if(null != priceElem && !priceElem.isJsonNull()) {
			price = priceElem.getAsString();
		}else if(null != priceElem){
			price = jsonObj.getAsString();
		}
		
		String	tax				= null;
		if(null != taxElem && !taxElem.isJsonNull()) {
			tax = taxElem.getAsString();
		}

		Boolean isIncludingTax	= null;
		if(null != incTaxElem && !incTaxElem.isJsonNull()) {
			isIncludingTax = incTaxElem.getAsBoolean();
		}
		
		UnitPrice.Builder builder = UnitPrice.builder().setPrice(price)
								  					   .setTax(tax);
		
		if( null != isIncludingTax && !isIncludingTax) {
			builder.setExcludingTax();
		}else {			
			builder.setIncludingTax();
		}
		
		return builder.build();
	}

}
