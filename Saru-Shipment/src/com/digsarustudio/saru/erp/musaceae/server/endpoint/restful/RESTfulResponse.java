/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.endpoint.restful;

import com.digsarustudio.saru.erp.musaceae.server.endpoint.http.HttpResponse;

/**
 * The object contains the status code and media content as the response of a HTTP request.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public class RESTfulResponse implements HttpResponse {
	/**
	 * 
	 * The object builder for {@link RESTfulResponse}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static class Builder implements HttpResponse.Builder {
		private RESTfulResponse result = null;

		private Builder() {
			this.result = new RESTfulResponse();
		}

		/**
		 * @param statusCode
		 * @see com.digsarustudio.banana.endpoint.http.HttpResponse#setStatusCode(java.lang.String)
		 */
		public Builder setStatusCode(Integer statusCode) {
			this.result.setStatusCode(statusCode);
			return this;
		}

		/**
		 * @param mediaContent
		 * @see com.digsarustudio.banana.endpoint.http.HttpResponse#setMediaContent(java.lang.String)
		 */
		public Builder setMediaContent(String mediaContent) {
			this.result.setMediaContent(mediaContent);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public RESTfulResponse build() {
			return this.result;
		}

	}
	
	private Integer	statusCode		= null;
	private String 	mediaContent	= null;
	
	/**
	 * 
	 */
	private RESTfulResponse() {
		
	}

	/**
	 * @return the statusCode
	 */
	public Integer getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the mediaContent
	 */
	public String getMediaContent() {
		return mediaContent;
	}

	/**
	 * @param mediaContent the mediaContent to set
	 */
	public void setMediaContent(String mediaContent) {
		this.mediaContent = mediaContent;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
