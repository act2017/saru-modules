/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.contactInfo.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.javabean.EmailAddress;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserialzer for Gson to parse or compile {@link EmailAddress} to or from a JSON string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class EmailAddressBiserializer implements JsonSerializer<EmailAddress>
											   , JsonDeserializer<EmailAddress> {

	/**
	 * 
	 */
	public EmailAddressBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public EmailAddress deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json) {
			return null;
		}
		
		String email = json.getAsString();
		
		return EmailAddress.builder().setAddress(email)
									 .build();
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(EmailAddress src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}

		JsonPrimitive email = new JsonPrimitive(src.getAddress());
		
		return email;
	}

}
