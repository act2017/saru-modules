/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.endpoint.restful;

import java.util.List;

import com.digsarustudio.saru.erp.musaceae.server.endpoint.http.HttpConnection;
import com.digsarustudio.saru.erp.musaceae.server.endpoint.http.HttpException;
import com.digsarustudio.saru.erp.musaceae.server.endpoint.http.HttpResponse;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.Endpoint;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.ResponseCallback;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.HttpHeader;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.HttpHeaderField;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.QueryParameter;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.URL;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.URLParameter;
import com.digsarustudio.saru.erp.musaceae.shared.security.Credential;
import com.digsarustudio.saru.erp.musaceae.shared.security.javabean.ExpiryCredential;
import com.digsarustudio.saru.erp.musaceae.shared.security.javabean.Token;


/**
 * The RESTful endpoint used by servlet works as a delegation class.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public abstract class RESTfulEndpoint<T, E> implements Endpoint<T, E>{
	public static final String 	JSON_NULL	= "null";
	
	/**
	 * The entity of HTTP connection, the headers, URL, and query parameters must be set in the sub-class.<br>
	 */
	private HttpConnection	connection	= null;
	
	private Credential		credential	= null;
	
	/**
	 * 
	 */
	public RESTfulEndpoint() {
		this.connection = RESTfulConnection.builder()
										   .build();
	}
	
	protected void setURL(URL url) {
		this.connection.setURL(url);
	}

	protected void addHeader(HttpHeaderField field) {
		this.connection.addHeader(field);
	}
	
	protected void setHeaderFields(List<HttpHeaderField> fields) {
		this.connection.setHeaders(fields);
	}
	
	protected void setHeader(HttpHeader header) {
		this.connection.setHTTPHeader(header);
	}
	
	protected void addQueryParameter(QueryParameter param) {
		this.connection.addParameter(param);
	}
	
	protected void addQueryParameter(String name, String value) {
		this.connection.addParameter(QueryParameter.builder().setKey(name)
															 .setValue(value)
															 .build());
	}
	
	protected void addURLParameter(URLParameter param) {
		this.connection.addURLParameter(param);
	}
	
	protected void addURLParameter(String value) {
		this.connection.addURLParameter(URLParameter.builder().setValue(value)
															  .build());
	}
	
	protected QueryParameter getQueryParameter(String key) {
		return this.connection.getParameter(key);
	}
	
	protected void setQueryParameters(List<QueryParameter> params) {
		this.connection.setParameters(params);
	}
	
	protected void setMediaContent(String content) {
		this.connection.setMediaContent(content);
	}
	
	protected void clear() {
		this.connection.clearParameters();
		this.connection.clearURLParameters();
	}

	/**
	 * The interface to manipulate {@link HttpConnection} locally.<br>
	 * This method is available in {@link #sendHttpGetRequest(ResponseCallback)} only, 
	 * <b>DO NOT</b> call this method anywhere.<br>
	 * 
	 * @return the response from remote service
	 * @throws HttpException
	 * @see com.digsarustudio.banana.endpoint.http.HttpConnection#sendGetReqeust()
	 */
	protected HttpResponse sendGetRequest() throws HttpException {
		return this.connection.sendGetRequest();
	}
	
	/**
	 * To send RESTful Get request to remote service and receive the response via a callback interface.<br>
	 * Please call {@link #sendGetRequest()} to send GET request to remote service in order to receive response.<br>
	 * 
	 * @param callback The response callback interface.<br>
	 * @throws EndpointException When an error occurred in the client side.<br>
	 * 
	 */
	protected abstract <P> void sendRESTfulGetRequest(ResponseCallback<P> callback) throws EndpointException;
	
	/**
	 * To send RESTful Get request to remote service and wait for the response from remote service.<br>
	 * Please call {@link #sendGetRequest()} to send GET request to remote service in order to receive response.<br>
	 * 
	 * @return The response object from remote service.
	 * @throws EndpointException When an error occurred in the client side.<br>
	 */
	protected abstract <P> P sendRESTfulGetRequest() throws EndpointException;

	/**
	 * The interface to manipulate {@link HttpConnection} locally.<br>
	 * This method is available in {@link #sendHttpPostRequest(ResponseCallback)} only, 
	 * <b>DO NOT</b> call this method anywhere.<br>
	 * 
	 * @return
	 * @throws HttpException
	 * @see com.digsarustudio.banana.endpoint.http.HttpConnection#sendPostRequest()
	 */
	protected HttpResponse sendPostRequest() throws HttpException {
		return connection.sendPostRequest();
	}
	
	/**
	 * To send RESTful Post request to remote service and receive the response via a callback interface.<br>
	 * Please call {@link #sendPostRequest()} to send POST request to remote service in order to receive response.<br>
	 * 
	 * @param callback The response callback interface.<br>
	 * @throws EndpointException When an error occurred in the client side.<br>
	 * 
	 */
	protected abstract <P> void sendRESTfulPostRequest(ResponseCallback<P> callback) throws EndpointException;
	
	/**
	 * To send RESTful Post request to remote service and wait for the response from remote service.<br>
	 * Please call {@link #sendPostRequest()} to send GET request to remote service in order to receive response.<br>
	 * 
	 * @return The response object from remote service.
	 * @throws EndpointException When an error occurred in the client side.<br>
	 */
	protected abstract <P> P sendRESTfulPostRequest() throws EndpointException;

	/**
	 * The interface to manipulate {@link HttpConnection} locally.<br>
	 * This method is available in {@link #sendHttpPutRequest(ResponseCallback)} only, 
	 * <b>DO NOT</b> call this method anywhere.<br>
	 * 
	 * @return
	 * @throws HttpException
	 * @see com.digsarustudio.banana.endpoint.http.HttpConnection#sendPutRequest()
	 */
	protected HttpResponse sendPutRequest() throws HttpException {
		return connection.sendPutRequest();
	}
	
	/**
	 * To send RESTful Put request to remote service and receive the response via a callback interface.<br>
	 * Please call {@link #sendPutRequest()} to send PUT request to remote service in order to receive response.<br>
	 * 
	 * @param callback The response callback interface.<br>
	 * @throws EndpointException When an error occurred in the client side.<br>
	 * 
	 */
	protected abstract <P> void sendRESTfulPutRequest(ResponseCallback<P> callback) throws EndpointException;
	
	/**
	 * To send RESTful Put request to remote service and wait for the response from remote service.<br>
	 * Please call {@link #sendPutRequest()} to send GET request to remote service in order to receive response.<br>
	 * 
	 * @return The response object from remote service.
	 * @throws EndpointException When an error occurred in the client side.<br>
	 */
	protected abstract <P> P sendRESTfulPutRequest() throws EndpointException;

	/**
	 * The interface to manipulate {@link HttpConnection} locally.<br>
	 * This method is available in {@link #sendHttpDeleteRequest(ResponseCallback)} only, 
	 * <b>DO NOT</b> call this method anywhere.<br>
	 * 
	 * @return
	 * @throws HttpException
	 * @see com.digsarustudio.banana.endpoint.http.HttpConnection#sendDeleteReqeust()
	 */
	protected HttpResponse sendDeleteReqeust() throws HttpException {
		return connection.sendDeleteReqeust();
	}
	
	/**
	 * To send RESTful Put request to remote service and receive the response via a callback interface.<br>
	 * Please call {@link #sendDeleteRequest()} to send DELETE request to remote service in order to receive response.<br>
	 * 
	 * @param callback The response callback interface.<br>
	 * @throws EndpointException When an error occurred in the client side.<br>
	 * 
	 */
	protected abstract <P> void sendRESTfulDeleteRequest(ResponseCallback<P> callback) throws EndpointException;
	
	/**
	 * To send RESTful Delete request to remote service and wait for the response from remote service.<br>
	 * Please call {@link #sendDeleteRequest()} to send GET request to remote service in order to receive response.<br>
	 * 
	 * @return The response object from remote service.
	 * @throws EndpointException When an error occurred in the client side.<br>
	 */
	protected abstract <P> P sendRESTfulDeleteRequest() throws EndpointException;

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.endpoint.Endpoint#setAPIToken(java.lang.String)
	 */
	@Override
	public void setAPIToken(String token) {
		this.setCredential(ExpiryCredential.builder().setToken(Token.builder().setValue(token)
																	.build())
													 .build());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.endpoint.Endpoint#setCredential(com.digsarustudio.saru.erp.musaceae.shared.security.Credential)
	 */
	@Override
	public void setCredential(Credential credential) {
		this.credential = credential;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.endpoint.Endpoint#getCredential()
	 */
	@Override
	public Credential getCredential() {
		return this.credential;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.endpoint.Endpoint#getAPIToken()
	 */
	@Override
	public String getAPIToken() {
		if(null == this.credential || !this.credential.validate()) {
			return null;
		}
		
		return this.credential.getToken().getValue();
	}
	
	
}
