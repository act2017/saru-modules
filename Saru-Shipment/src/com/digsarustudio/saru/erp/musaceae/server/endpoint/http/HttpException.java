/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.endpoint.http;

/**
 * This exception indicates an error occurred when the user sent a request to remote service.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
@SuppressWarnings("serial")
public class HttpException extends Exception {
	private Integer statusCode	= null;
	private String	statusMsg	= null; 	
	
	/**
	 * 
	 */
	public HttpException() {

	}
	
	public HttpException(Integer statusCode, String statusMsg){
		super(statusCode.toString() + " : " + statusMsg);
		this.statusCode = statusCode;
		this.statusMsg = statusMsg;
	}

	/**
	 * @param message
	 */
	public HttpException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public HttpException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public HttpException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public HttpException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

	/**
	 * Returns the status code
	 * 
	 * @return the statusCode
	 */
	public Integer getStatusCode() {
		return statusCode;
	}

	/**
	 * Returns the status message
	 * 
	 * @return the statusMsg
	 */
	public String getStatusMsg() {
		return statusMsg;
	}

	
}
