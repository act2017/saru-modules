/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.contactInfo.address.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.AddressLines;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.AustralianAddress;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.AustralianPostcode;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.Country;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.State;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.Suburb;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The deserializer and serializer for Gson to parse {@link AustralianAddress} from a json string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AustralianAddressBiserializer implements JsonDeserializer<AustralianAddress>
													, JsonSerializer<AustralianAddress>{

	/**
	 * 
	 */
	public AustralianAddressBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public AustralianAddress deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject			root		= json.getAsJsonObject();
		
		JsonElement			linesElem	= root.get(AustralianAddress.ATTR_ADDRESS_LINES);
		String[]			linesArr	= context.deserialize(linesElem, String[].class);
		AddressLines		lines		= AddressLines.builder().setAddresses(linesArr)
															.build();
				
		JsonElement			suburbElem	= root.get(AustralianAddress.ATTR_SUBURB);
		Suburb				suburb		= context.deserialize(suburbElem, Suburb.class);
			
		JsonElement			stateElem	= root.get(AustralianAddress.ATTR_STATE);
		State				state		= context.deserialize(stateElem, State.class);
		
		JsonElement			postcodeElem= root.get(AustralianAddress.ATTR_POSTCODE);
		AustralianPostcode	postcode	= context.deserialize(postcodeElem, AustralianPostcode.class);
		
		JsonElement			countryElem	= root.get(AustralianAddress.ATTR_COUNTRY);
		Country				country		= context.deserialize(countryElem, Country.class);
		
		return AustralianAddress.builder().setAddressLine(lines)
										  .setSuburb(suburb)
										  .setState(state)
										  .setPostcode(postcode)
										  .setCountry(country)
										  .build();
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(AustralianAddress src, Type typeOfSrc, JsonSerializationContext context) {
		if( null == src || !src.validate() ) {
			return null;
		}
		
		JsonObject root = new JsonObject();
		
		root.add(AustralianAddress.ATTR_ADDRESS_LINES, context.serialize(src.getAddressLine(), AddressLines.class));
		root.add(AustralianAddress.ATTR_SUBURB, context.serialize(src.getSuburb(), Suburb.class));
		root.add(AustralianAddress.ATTR_STATE, context.serialize(src.getState(), State.class));
		root.add(AustralianAddress.ATTR_POSTCODE, context.serialize(src.getPostcode(), AustralianPostcode.class));
		
		//[Optional]
		JsonElement countryElem = context.serialize(src.getCountry(), Country.class);
		if(null != countryElem) {
			root.add(AustralianAddress.ATTR_COUNTRY, countryElem);
		}
		
		return root;
	}

}
