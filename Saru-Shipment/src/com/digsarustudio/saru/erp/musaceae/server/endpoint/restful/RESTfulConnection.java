/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.endpoint.restful;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.util.EntityUtils;

import com.digsarustudio.saru.erp.musaceae.server.endpoint.http.HttpConnection;
import com.digsarustudio.saru.erp.musaceae.server.endpoint.http.HttpException;
import com.digsarustudio.saru.erp.musaceae.server.endpoint.http.HttpResponse;
import com.digsarustudio.saru.erp.musaceae.server.endpoint.http.HttpResponseCallback;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.HttpHeader;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.HttpHeaderField;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.QueryParameter;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.URL;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.URLParameter;

/**
 * The connection object abided by REST convention to send and receive data which can be represented in 
 * JSON format from a HTTP server.<br> 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class RESTfulConnection implements HttpConnection {
	/**
	 * 
	 * The object builder for {@link HttpConnection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements HttpConnection.Builder {
		private HttpConnection result = null;

		private Builder() {
			this.result = new RESTfulConnection();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.endpoint.http.HttpConnection.Builder#setURL(com.digsarustudio.banana.endpoint.url.URL)
		 */
		@Override
		public Builder setURL(URL url) {
			this.result.setURL(url);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.endpoint.http.HttpConnection.Builder#setHeaders(java.util.List)
		 */
		@Override
		public Builder setHeaders(List<HttpHeaderField> headers) {
			this.result.setHeaders(headers);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.endpoint.http.HttpConnection.Builder#addHeaders(com.digsarustudio.banana.endpoint.http.header.HttpHeaderField)
		 */
		@Override
		public Builder addHeader(HttpHeaderField header) {
			this.result.addHeader(header);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.endpoint.http.HttpConnection.Builder#setHTTPHeader(com.digsarustudio.banana.endpoint.http.header.HttpHeader)
		 */
		@Override
		public Builder setHTTPHeader(HttpHeader header) {
			this.result.setHTTPHeader(header);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.endpoint.http.HttpConnection.Builder#setMediaContent(java.lang.String)
		 */
		@Override
		public Builder setMediaContent(String data) {
			this.result.setMediaContent(data);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.endpoint.http.HttpConnection.Builder#addParameter(com.digsarustudio.banana.endpoint.url.QueryParameter)
		 */
		@Override
		public Builder addParameter(QueryParameter param) {
			this.result.addParameter(param);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.endpoint.http.HttpConnection.Builder#setParameters(java.util.List)
		 */
		@Override
		public Builder setParameters(List<QueryParameter> params) {
			this.result.setParameters(params);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.endpoint.http.HttpConnection.Builder#addURLParameter(com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.URLParameter)
		 */
		@Override
		public Builder addURLParameter(URLParameter param) {
			this.result.addURLParameter(param);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.endpoint.http.HttpConnection.Builder#setURLParameters(java.util.List)
		 */
		@Override
		public Builder setURLParameters(List<URLParameter> params) {
			this.result.setURLParameters(params);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public HttpConnection build() {
			return this.result;
		}

	}
	
	private HttpHeader				header			= null;
	private List<QueryParameter>	queryParams		= null;
	private List<URLParameter>		urlParams		= null;
	private String					mediaContent	= null;
	private URL						url				= null;

	/**
	 * 
	 */
	private RESTfulConnection() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HttpConnection#sendGetRequest(com.digsarustudio.banana.endpoint.http.HttpResponseCallback)
	 */
	@Override
	public void sendGetRequest(HttpResponseCallback callback) throws HttpException {
		throw new UnsupportedOperationException("Please use #sendGetReqeust() instead");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HttpConnection#sendGetRequest()
	 */
	@Override
	public HttpResponse sendGetRequest() throws HttpException {
		if(null == this.url) {
			throw new HttpException("url must be set to send the request");
		}
		
		CloseableHttpClient httpClient	= HttpClients.createDefault();
		HttpGet				httpGet		= new HttpGet(this.getEncodedURL());
		
		if(null != this.header && null != this.header.getFields() && !this.header.getFields().isEmpty()) {
			List<HttpHeaderField> fields = this.header.getFields();
			for (HttpHeaderField field : fields) {
				httpGet.setHeader(field.getHeader(), field.getValue());
			}
		}

		// The underlying HTTP connection is still held by the response object
		// to allow the response content to be streamed directly from the network socket.
		// In order to ensure correct deallocation of system resources
		// the user MUST call CloseableHttpResponse#close() from a finally clause.
		// Please note that if response content is not fully consumed the underlying
		// connection cannot be safely re-used and will be shut down and discarded
		// by the connection manager.			
		CloseableHttpResponse response = null;
		HttpResponse.Builder builder = RESTfulResponse.builder();
		try {
			response = httpClient.execute(httpGet);
			
			HttpEntity entity = response.getEntity();
			
			//Deal with the response entity
			builder.setStatusCode(response.getStatusLine().getStatusCode());
			if(null != entity) {
				String content = EntityUtils.toString(entity);
				builder.setMediaContent(content);
			}			
			
			EntityUtils.consume(entity);
		} catch (IOException e) {
			throw new HttpException(e.getMessage(), e);
		}finally {
			try {
				response.close();				
				httpClient.close();
			} catch (IOException e) {
				throw new HttpException(e.getMessage(), e);
			}
		}
		
		return builder.build();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HttpConnection#sendPostRequest(com.digsarustudio.banana.endpoint.http.HttpResponseCallback)
	 */
	@Override
	public void sendPostRequest(HttpResponseCallback callback) throws HttpException {
		throw new UnsupportedOperationException("Please use #sendGetReqeust() instead");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HttpConnection#sendPostRequest()
	 */
	@Override
	public HttpResponse sendPostRequest() throws HttpException {
		if(null == this.url) {
			throw new HttpException("url must be set to send the request");
		}
		
		CloseableHttpClient httpClient	= HttpClients.createDefault();
		HttpPost			httpPost	= new HttpPost(this.getEncodedURL());
		
		if(null != this.header && null != this.header.getFields() && !this.header.getFields().isEmpty()) {
			List<HttpHeaderField> fields = this.header.getFields();
			for (HttpHeaderField field : fields) {
				httpPost.setHeader(field.getHeader(), field.getValue());
			}
		}
	
		CloseableHttpResponse response = null;
		HttpResponse.Builder builder = RESTfulResponse.builder();
		try {
			StringEntity requstEntity = new StringEntity(this.mediaContent);
			httpPost.setEntity(requstEntity);
			
			response = httpClient.execute(httpPost);
			
			//Deal with the response entity
			HttpEntity responseEntity = response.getEntity();
			
			builder.setStatusCode(response.getStatusLine().getStatusCode());
			if(null != responseEntity) {
				String content = EntityUtils.toString(responseEntity);
				builder.setMediaContent(content);
			}			
			
			EntityUtils.consume(responseEntity);
		} catch (IOException e) {
			throw new HttpException(e.getMessage(), e);
		} finally {
			try {
				response.close();				
				httpClient.close();
			} catch (IOException e) {
				throw new HttpException(e.getMessage(), e);
			}
		}		
		
		return builder.build();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HttpConnection#sendPutRequest(com.digsarustudio.banana.endpoint.http.HttpResponseCallback)
	 */
	@Override
	public void sendPutRequest(HttpResponseCallback callback) throws HttpException {
		throw new UnsupportedOperationException("Please use #sendGetReqeust() instead");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HttpConnection#sendPutRequest()
	 */
	@Override
	public HttpResponse sendPutRequest() throws HttpException {
		if(null == this.url) {
			throw new HttpException("url must be set to send the request");
		}
		
		CloseableHttpClient httpClient	= HttpClients.createDefault();
		HttpPut				httpPut		= new HttpPut(this.getEncodedURL());
		
		if(null != this.header && null != this.header.getFields() && !this.header.getFields().isEmpty()) {
			List<HttpHeaderField> fields = this.header.getFields();
			for (HttpHeaderField field : fields) {
				httpPut.setHeader(field.getHeader(), field.getValue());
			}
		}
		
		CloseableHttpResponse response = null;
		HttpResponse.Builder builder = RESTfulResponse.builder();
		try {
			StringEntity requstEntity = new StringEntity(this.mediaContent);
			httpPut.setEntity(requstEntity);
			
			response = httpClient.execute(httpPut);
			
			//Deal with the response entity
			HttpEntity responseEntity = response.getEntity();
			
			builder.setStatusCode(response.getStatusLine().getStatusCode());
			if(null != responseEntity) {
				String content = EntityUtils.toString(responseEntity);
				builder.setMediaContent(content);
			}			
			
			EntityUtils.consume(responseEntity);
		} catch (IOException e) {
			throw new HttpException(e.getMessage(), e);
		} finally {
			try {
				response.close();				
				httpClient.close();
			} catch (IOException e) {
				throw new HttpException(e.getMessage(), e);
			}
		}		
		
		return builder.build();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HttpConnection#sendDeleteRequest(com.digsarustudio.banana.endpoint.http.HttpResponseCallback)
	 */
	@Override
	public void sendDeleteRequest(HttpResponseCallback callback) throws HttpException {
		throw new UnsupportedOperationException("Please use #sendGetReqeust() instead");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HttpConnection#sendDeleteReqeust()
	 */
	@Override
	public HttpResponse sendDeleteReqeust() throws HttpException {

		if(null == this.url) {
			throw new HttpException("url must be set to send the request");
		}
		
		CloseableHttpClient httpClient	= HttpClients.createDefault();
		HttpDelete			httpMethod	= new HttpDelete(this.getEncodedURL());
		
		if(null != this.header && null != this.header.getFields() && !this.header.getFields().isEmpty()) {
			List<HttpHeaderField> fields = this.header.getFields();
			for (HttpHeaderField field : fields) {
				httpMethod.setHeader(field.getHeader(), field.getValue());
			}
		}
		
		CloseableHttpResponse response = null;
		HttpResponse.Builder builder = RESTfulResponse.builder();
		try {
			response = httpClient.execute(httpMethod);
			
			//Deal with the response entity
			HttpEntity responseEntity = response.getEntity();
			
			builder.setStatusCode(response.getStatusLine().getStatusCode());
			if(null != responseEntity) {
				String content = EntityUtils.toString(responseEntity);
				builder.setMediaContent(content);
			}			
			
			EntityUtils.consume(responseEntity);
		} catch (IOException e) {
			throw new HttpException(e.getMessage(), e);
		} finally {
			try {
				response.close();				
				httpClient.close();
			} catch (IOException e) {
				throw new HttpException(e.getMessage(), e);
			}
		}		
		
		return builder.build();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HTTPConnection#getURL()
	 */
	@Override
	public URL getURL() {
		return this.url;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HTTPConnection#setURL(com.digsarustudio.banana.endpoint.url.URL)
	 */
	@Override
	public void setURL(URL url) {
		this.url = url;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HTTPConnection#getHeaders()
	 */
	@Override
	public List<HttpHeaderField> getHeaders() {
		return this.header.getFields();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HTTPConnection#setHeaders(java.util.List)
	 */
	@Override
	public void setHeaders(List<HttpHeaderField> headers) {
		if(null == this.header) {
			this.header = new RESTfulHttpHeader();
		}
		
		this.header.setFields(headers);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HTTPConnection#addHeaders(com.digsarustudio.banana.endpoint.http.header.HttpHeaderField)
	 */
	@Override
	public void addHeader(HttpHeaderField header) {
		if(null == this.header) {
			this.header = new RESTfulHttpHeader();
		}
		
		this.header.addField(header);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HttpConnection#setHTTPHeader(com.digsarustudio.banana.endpoint.http.header.HttpHeader)
	 */
	@Override
	public void setHTTPHeader(HttpHeader header) {
		this.header = header;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HTTPConnection#getMediaContent()
	 */
	@Override
	public String getMediaContent() {
		return this.mediaContent;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HTTPConnection#setMediaContent(java.lang.String)
	 */
	@Override
	public void setMediaContent(String data) {
		this.mediaContent = data;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HTTPConnection#addParameter(com.digsarustudio.banana.endpoint.url.QueryParameter)
	 */
	@Override
	public void addParameter(QueryParameter param) {
		if(null == this.queryParams) {
			this.queryParams = new ArrayList<>();
		}
		
		this.queryParams.add(param);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HTTPConnection#setParameters(java.util.List)
	 */
	@Override
	public void setParameters(List<QueryParameter> params) {
		this.queryParams = params;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HTTPConnection#getParameters()
	 */
	@Override
	public List<QueryParameter> getParameters() {
		return this.queryParams;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HttpConnection#getParameter(java.lang.String)
	 */
	@Override
	public QueryParameter getParameter(String key) {
		if(null == this.queryParams) {
			return null;
		}
		
		QueryParameter rtn = null;
		for (QueryParameter param : this.queryParams) {
			if(!key.equals(param.getKey())) {
				continue;
			}
			
			rtn = param;
			break;
		}
		
		return rtn;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HTTPConnection#clearParameters()
	 */
	@Override
	public void clearParameters() {
		if(null != this.queryParams) {
			this.queryParams.clear();
		}
		
		this.mediaContent = null;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HttpConnection#addURLParameter(com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.URLParameter)
	 */
	@Override
	public void addURLParameter(URLParameter param) {
		if(null == param) {
			return;
		}else if(null == this.urlParams) {
			this.urlParams = new ArrayList<>();
		}
		
		this.urlParams.add(param);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HttpConnection#getURLParameter(java.lang.Integer)
	 */
	@Override
	public URLParameter getURLParameter(Integer index) {
		if(null == this.urlParams || this.urlParams.isEmpty() || null == index || index >= this.urlParams.size()) {
			return null;
		}

		return this.urlParams.get(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HttpConnection#setURLParameters(java.util.List)
	 */
	@Override
	public void setURLParameters(List<URLParameter> params) {
		this.urlParams = params;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HttpConnection#getURLParameters()
	 */
	@Override
	public List<URLParameter> getURLParameters() {
		return this.urlParams;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.HttpConnection#clearURLParameters()
	 */
	@Override
	public void clearURLParameters() {
		if(null != this.urlParams) {
			this.urlParams.clear();
		}
		
		this.mediaContent = null;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	/**
	 * Returns an encoded string that is the copy of {@link #url} and combined with {@link QueryParameter}
	 * @return
	 */
	protected String getEncodedURL() {
		URL compile = this.url.copy();
		
		if(null != this.urlParams) {
			for (URLParameter param : this.urlParams) {
				compile.addURLParameter(param);
			}
		}
		
		if(null != this.queryParams) {
			for (QueryParameter param : queryParams) {
				compile.addParameter(param);
			}
		}
		
		return compile.getEncodedURL();
	}	
	
	protected void setAsyncHttpRequest(final HttpResponseCallback callback) {
		//HttpAsyncClient
		CloseableHttpAsyncClient client = HttpAsyncClients.createDefault();
		client.start();
		
		final HttpGet requestGet = new HttpGet(this.getURL().getEncodedURL());
		
		client.execute(requestGet, new FutureCallback<org.apache.http.HttpResponse>() {
			
			@Override
			public void failed(Exception ex) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void completed(org.apache.http.HttpResponse result) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void cancelled() {
				// TODO Auto-generated method stub
				
			}
		});		
	}
}
