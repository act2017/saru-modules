/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.endpoint.http;

import java.util.List;

import com.digsarustudio.saru.erp.musaceae.shared.endpoint.Endpoint;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.HttpHeader;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.QueryParameter;

/**
 * The endpoint used to send Get, Post, Update, Delete HTTP Method to HTTP server.<br>
 *
 * @param		T The data type of incoming data
 * @param		E The data type of outgoing data
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 * 
 *
 */
public interface HttpEndpoint<T, E> extends Endpoint<T, E> {
	void setHTTPHeader(HttpHeader header);
	void setQueryParameters(List<QueryParameter> parameters);
	void addQueryParameter(QueryParameter parameter);
}
