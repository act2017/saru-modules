/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.endpoint.restful;

import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.BaseHttpHeader;

/**
 * The HTTP header used by a {@link RESTfulConnection}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class RESTfulHttpHeader extends BaseHttpHeader {

	/**
	 * 
	 */
	public RESTfulHttpHeader() {
	}

}
