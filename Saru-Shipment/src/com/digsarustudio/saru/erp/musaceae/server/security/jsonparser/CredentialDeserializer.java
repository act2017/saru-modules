/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.security.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.musaceae.shared.security.javabean.ExpiryCredential;
import com.digsarustudio.saru.erp.musaceae.shared.security.javabean.Token;
import com.digsarustudio.saru.erp.musaceae.shared.time.javabean.Date;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * The deserializer for Gson to parse {@link com.digsarustudio.banana.erp.general.security.Credential} from a JSON string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class CredentialDeserializer implements JsonDeserializer<ExpiryCredential> {

	/**
	 * 
	 */
	public CredentialDeserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public ExpiryCredential deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		JsonObject root = json.getAsJsonObject();
		
		JsonElement tokenElem = root.get(ExpiryCredential.ATTR_TOKEN);
		Token token = null;
		if(null != tokenElem) {
			if(tokenElem.isJsonObject()) {
				token = context.deserialize(tokenElem, Token.class);
			}else {
				token = Token.builder().setValue(tokenElem.getAsString())
									  .build();
			}
		}
	
		JsonElement expireDateElem = root.get(ExpiryCredential.ATTR_EXPIRE_DATE);
		Date expireDate = null;
		if(null != expireDateElem) {
			if(expireDateElem.isJsonObject()) {
				expireDate = context.deserialize(expireDateElem, Date.class);
			}else {
				expireDate = Date.builder().setValue(expireDateElem.getAsString())
										   .build();
			}
		}
		
		return ExpiryCredential.builder().setToken(token)
								   .setExpireDate(expireDate)
								   .build();
	}

}
