/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.endpoint.http;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The object contains the status code and media content as the response of a HTTP request.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface HttpResponse {
	/**
	 * 
	 * The builder for {@link HttpResponse}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static interface Builder extends ObjectBuilder<HttpResponse> {
		Builder setStatusCode(Integer statusCode);
		Builder setMediaContent(String content);
	}

	Integer getStatusCode();
	String getMediaContent();
}
