/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.contactInfo.address.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.Suburb;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link Suburb} from or to a JSON string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SuburbBiserializer implements JsonSerializer<Suburb>
										 , JsonDeserializer<Suburb> {

	/**
	 * 
	 */
	public SuburbBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public Suburb deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}

		JsonObject root = json.getAsJsonObject();
		
		return Suburb.builder().setCode(root.get(Suburb.ATTR_CODE).getAsString())
			 				   .setName(root.get(Suburb.ATTR_NAME).getAsString())
			 				   .build();
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(Suburb src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonObject suburb = new JsonObject();
		suburb.addProperty(Suburb.ATTR_CODE, src.getCode());
		suburb.addProperty(Suburb.ATTR_NAME, src.getName());
		
		return suburb;
	}

}
