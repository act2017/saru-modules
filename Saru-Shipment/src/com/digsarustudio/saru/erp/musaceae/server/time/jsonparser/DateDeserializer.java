/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.time.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.musaceae.shared.time.javabean.Date;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The deserializer for Gson to parse a {@link com.digsarustudio.banana.erp.general.time.Date} object from a JSON string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DateDeserializer implements JsonDeserializer<Date>
										,JsonSerializer<Date>{

	/**
	 * 
	 */
	public DateDeserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		String date = json.getAsString();
		
		return Date.builder().setValue(date)
							 .build();
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		
		return null;
	}
	
}
