/**
 * 
 */
package com.digsarustudio.saru.erp.musaceae.server.endpoint.http;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.HttpHeader;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.HttpHeaderField;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.QueryParameter;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.URL;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.URLParameter;

/**
 * The sub-type handles the data model and how to send a HTTP request to the remote service via a URL, and 
 * retrieve the result by {@link HttpResponseCallback} to the client code.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 *
 */
public interface HttpConnection {
	/**
	 * 
	 * The object builder for {@link HttpConnection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static interface Builder extends ObjectBuilder<HttpConnection> {
		/**
		 * Assigns a details of URL of a remote service
		 * 
		 * @param url the URL of a remote service to set
		 */
		Builder setURL(URL url);

		/**
		 * Assigns a list of {@link HttpHeaderField} for the request
		 * 
		 * @param headers the headers to set
		 */
		Builder setHeaders(List<HttpHeaderField> headers);

		/**
		 * Assigns a {@link HttpHeaderField} for the request
		 * 
		 * @param header The header to set
		 */
		Builder addHeader(HttpHeaderField header);
		
		/**
		 * Assigns a {@link HttpHeader} for the request.<br>
		 * 
		 * @param header The header to set
		 * 
		 * @return The instance of this builder
		 */
		Builder setHTTPHeader(HttpHeader header);
		
		/**
		 * Assigns the media content to carry to the remote service
		 * 
		 * @param data the media content to carry to the remote service
		 */
		Builder setMediaContent(String data);

		/**
		 * @param param
		 */
		Builder addParameter(QueryParameter param);

		/**
		 * @param params
		 */
		Builder setParameters(List<QueryParameter> params);
		
		
		Builder addURLParameter(URLParameter param);
		Builder setURLParameters(List<URLParameter> params);
	}	

	/**
	 * To send a GET request to the remote service
	 * 
	 * @param callback The callback for this request
	 * 
	 * @throws HttpException if the request cannot be sent or there is no response from remote service.<br>
	 */
	void sendGetRequest(HttpResponseCallback callback) throws HttpException;
	
	/**
	 * To send a GET request to the remote service.<br>
	 * 
	 * @return The response of this request from the remote service.
	 * 
	 * @throws HttpException when an error occurred.
	 */
	HttpResponse sendGetRequest() throws HttpException;
	
	/**
	 * To send a POST request to the remote service
	 * 
	 * @param callback The callback for this request
	 * 
	 * @throws HttpException if the request cannot be sent or there is no response from remote service.<br>
	 */
	void sendPostRequest(HttpResponseCallback callback) throws HttpException;
	
	/**
	 * To send a POST request to the remote service.<br>
	 * 
	 * @return The response of this request from the remote service.
	 * 
	 * @throws HttpException when an error occurred.
	 */
	HttpResponse sendPostRequest() throws HttpException;
	
	/**
	 * To send a PUT request to the remote service
	 * 
	 * @param callback The callback for this request
	 * 
	 * @throws HttpException if the request cannot be sent or there is no response from remote service.<br>
	 */
	void sendPutRequest(HttpResponseCallback callback) throws HttpException;
	
	/**
	 * To send a PUT request to the remote service.<br>
	 * 
	 * @return The response of this request from the remote service.
	 * 
	 * @throws HttpException when an error occurred.
	 */
	HttpResponse sendPutRequest() throws HttpException;
	
	/**
	 * To send a DELETE request to the remote service
	 * 
	 * @param callback The callback for this request
	 * 
	 * @throws HttpException if the request cannot be sent or there is no response from remote service.<br>
	 */
	void sendDeleteRequest(HttpResponseCallback callback) throws HttpException;
	
	/**
	 * To send a Delete request to the remote service.<br>
	 * 
	 * @return The response of this request from the remote service.
	 * 
	 * @throws HttpException when an error occurred.
	 */
	HttpResponse sendDeleteReqeust() throws HttpException;
	
	/**
	 * Returns the details of URL of a remote service
	 * 
	 * @return the details of URL of a remote service
	 */
	URL getURL();

	/**
	 * Assigns a details of URL of a remote service
	 * 
	 * @param url the URL of a remote service to set
	 */
	void setURL(URL url);

	/**
	 * Returns the {@link HttpHeaderField} for the request
	 * 
	 * @return the {@link HttpHeaderField} for the request
	 */
	List<HttpHeaderField> getHeaders() ;

	/**
	 * Assigns a list of {@link HttpHeaderField} for the request
	 * 
	 * @param headers the headers to set
	 */
	void setHeaders(List<HttpHeaderField> headers);

	/**
	 * Assigns a {@link HttpHeaderField} for the request
	 * 
	 * @param header The header to set
	 */
	void addHeader(HttpHeaderField header);
	
	/**
	 * Assigns a {@link HttpHeader} for the request.<br>
	 * 
	 * @param header The header to set
	 * 
	 * @return The instance of this builder
	 */
	void setHTTPHeader(HttpHeader header);
	
	/**
	 * Returns the media content to carry to the remote service
	 * 
	 * @return the media content to carry to the remote service
	 */
	String getMediaContent();

	/**
	 * Assigns the media content to carry to the remote service
	 * 
	 * @param data the media content to carry to the remote service
	 */
	void setMediaContent(String data);

	/**
	 * Append a query parameter
	 * 
	 * @param param The query parameter to append
	 */
	void addParameter(QueryParameter param);
	
	/**
	 * Returns a particular parameter according to its key.<br>
	 * 
	 * @param key The key of the particular parameter
	 * 
	 * @return The particular parameter according to its key.<br>
	 */
	QueryParameter getParameter(String key);

	/**
	 * To set a bulk of query parameters
	 * 
	 * @param params The list of query parameters
	 */
	void setParameters(List<QueryParameter> params);

	/**
	 * Returns the list of query parameters
	 * 
	 * @return The list of query parameters
	 */
	List<QueryParameter> getParameters();

	/**
	 * To clear query parameters
	 */
	void clearParameters();

	/**
	 * Append a query parameter
	 * 
	 * @param param The query parameter to append
	 */
	void addURLParameter(URLParameter param);
	
	/**
	 * Returns a particular parameter according to its key.<br>
	 * 
	 * @param key The key of the particular parameter
	 * 
	 * @return The particular parameter according to its key.<br>
	 */
	URLParameter getURLParameter(Integer index);

	/**
	 * To set a bulk of query parameters
	 * 
	 * @param params The list of query parameters
	 */
	void setURLParameters(List<URLParameter> params);

	/**
	 * Returns the list of query parameters
	 * 
	 * @return The list of query parameters
	 */
	List<URLParameter> getURLParameters();

	/**
	 * To clear query parameters
	 */
	void clearURLParameters();
}
