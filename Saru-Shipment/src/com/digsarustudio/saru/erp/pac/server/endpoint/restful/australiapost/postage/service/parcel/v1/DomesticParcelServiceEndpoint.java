/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.shared.FreightSize;
import com.digsarustudio.saru.erp.musaceae.shared.LengthUnit;
import com.digsarustudio.saru.erp.musaceae.shared.Weight;
import com.digsarustudio.saru.erp.musaceae.shared.WeightUnit;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.AuthenticationDetails;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.ResponseCallback;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.QueryParameter;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.RESTfulResponseStatusCodes;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.jsonparser.ParcelServiceCollectionDeserializer;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.jsonparser.ParcelServiceDeserializer;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.jsonparser.ParcelServiceOptionCollectionDeserializer;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.jsonparser.ParcelServiceOptionDeserializer;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.jsonparser.ParcelServiceResponseDeserializer;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelServiceCollection;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelServiceOption;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelServiceOptionCollection;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelServiceResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Returns the available services and additional options for a domestic parcel.
 * <h3>Resource Information</h3>
 * <table>
 * 	<tr>
 * 		<td>Rate Limited?</td><td>No</td><td>HTTP Methods</td><td>GET</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Requests per rate limit window</td><td>-</td><td>Resource family</td><td>parcel</td>
 * 	<tr>
 * 		<td>Authentication</td><td>API key</td><td>Response object</td><td>Postage</td>
 * 	<tr>
 * 		<td>Response Formats</td><td>json, xml</td><td>API Version</td><td>v1.0</td>
 * 	</tr>
 * </table>
 * <br>
 * <h3>Resource URL</h3>
 * https://digitalapi.auspost.com.au/postage/parcel/domestic/service
 * <h3>Headers</h3>
 * auth-key(required)	Your PAC API Key (HTTP Header)<br>
 * <br>
 * <h3>Parameters</h3>
 * from_postcode(required)	The postcode the parcel will be sent from.<br>
 * to_postcode(required)	The postcode the parcel will be sent to.<br>
 * length(required)			The parcel length in CMs.<br>
 * width(required)			The parcel width in CMs.<br>
 * height(required)			The parcel height in CMs<br>
 * weight(required)			The parcel weight in KGs<br>
 * <br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/postage-parcel-domestic-service">postage/parcel/domestic/service</a>
 *
 */
public class DomesticParcelServiceEndpoint extends AustraliaPostEndpoint<ParcelService, ParcelService> {		
	private static final String API_PATH				= "postage/parcel/domestic/service.json";
	private static final String	PARAM_SENDER_POSTCODE	= "from_postcode";
	private static final String	PARAM_RECEIVER_POSTCODE	= "to_postcode";
	private static final String	PARAM_LENGTH			= "length";
	private static final String	PARAM_WIDTH				= "width";
	private static final String	PARAM_HEIGHT			= "height";
	private static final String	PARAM_WEIGHT			= "weight";
	
	/**
	 * 
	 * The object builder for {@link DomesticParcelServiceEndpoint}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<DomesticParcelServiceEndpoint> {
		private DomesticParcelServiceEndpoint result = null;

		private Builder() {
			this.result = new DomesticParcelServiceEndpoint();
			this.result.setURL(API_PATH);
		}

		/**
		 * @param details
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint#setAuthKey(com.digsarustudio.banana.endpoint.AuthenticationDetails)
		 */
		public Builder setAuthKey(AuthenticationDetails details) {
			result.setAuthKey(details);
			return this;
		}

		/**
		 * @param postcode
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint#setSenderPostcode(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode)
		 */
		public Builder setSenderPostcode(Postcode postcode) {
			result.setSenderPostcode(postcode);
			return this;
		}

		/**
		 * @param postcode
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint#setReceiverPostcode(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode)
		 */
		public Builder setReceiverPostcode(Postcode postcode) {
			result.setReceiverPostcode(postcode);
			return this;
		}

		/**
		 * @param size
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint#setFreightSize(com.digsarustudio.saru.erp.musaceae.shared.FreightSize)
		 */
		public Builder setFreightSize(FreightSize size) {
			result.setFreightSize(size);
			return this;
		}

		/**
		 * @param weight
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.DomesticParcelServiceEndpoint#setFreightWeight(com.digsarustudio.saru.erp.musaceae.shared.Weight)
		 */
		public Builder setFreightWeight(Weight weight) {
			result.setFreightWeight(weight);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DomesticParcelServiceEndpoint build() {
			return this.result;
		}

	}
	
	private DomesticParcelServiceEndpoint() {
		
	}
	
	public void setSenderPostcode(Postcode postcode) {
		this.addQueryParameter(QueryParameter.builder().setKey(PARAM_SENDER_POSTCODE)
													   .setValue(postcode.getCode())
													   .build());
	}
	
	public void setReceiverPostcode(Postcode postcode) {
		this.addQueryParameter(QueryParameter.builder().setKey(PARAM_RECEIVER_POSTCODE)
													   .setValue(postcode.getCode())
													   .build());
	}
	
	public void setFreightSize(FreightSize size) {
		FreightSize confirmed = size;
		
		if(LengthUnit.CentiMetre != size.getLength().getUnit() 
		|| LengthUnit.CentiMetre != size.getWidth().getUnit()
		|| LengthUnit.CentiMetre != size.getHeight().getUnit()) {
			confirmed = size.toCentimetre();
		}
		
		this.addQueryParameter(QueryParameter.builder().setKey(PARAM_LENGTH)
													   .setValue(confirmed.getLength().getValue())
													   .build());
		
		this.addQueryParameter(QueryParameter.builder().setKey(PARAM_WIDTH)
													   .setValue(confirmed.getWidth().getValue())
													   .build());
		
		this.addQueryParameter(QueryParameter.builder().setKey(PARAM_HEIGHT)
													   .setValue(confirmed.getHeight().getValue())
													   .build());
	}
	
	public void setFreightWeight(Weight weight) {
		Weight confirmed = weight;
		
		if(WeightUnit.Kilogram != weight.getUnit()) {
			confirmed = weight.toKilograms();
		}
		
		this.addQueryParameter(QueryParameter.builder().setKey(PARAM_WEIGHT)
													   .setValue(confirmed.getValue())
													   .build());		
	}
	
	/**
	 * Returns a list of {@link ParcelService} asynchronizedly.
	 * @param callback
	 * @throws EndpointException 
	 */
	public void list(ResponseCallback<CollectionResponse<ParcelService>> callback) throws EndpointException {
		this.sendRESTfulGetRequest(new ResponseCallback<ParcelServiceResponse>() {

			@Override
			public void onSuccess(ParcelServiceResponse result) {
				CollectionResponse.Builder<ParcelService> builder = com.digsarustudio.saru.erp.musaceae.shared.endpoint.javabean.CollectionResponse.<ParcelService>builder();
				List<ParcelService> services = result.getParcelServices();
				builder.setItems(services);
				
				callback.onSuccess(builder.build());
				
			}

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
		});
	}
	
	/**
	 * Returns a list of {@link ParcelService} synchronisedly.<br>
	 * 
	 * @return
	 * @throws EndpointException 
	 */
	public List<ParcelService> list() throws EndpointException{
		ParcelServiceResponse result = this.sendRESTfulGetRequest();
		List<ParcelService> services = result.getParcelServices();
		
		return services;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#insert(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void insert(ParcelService data, ResponseCallback<ParcelService> callback) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support inert method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#update(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void update(ParcelService data, ResponseCallback<ParcelService> callback) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support update method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#update(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)
	 */
	@Override
	public void update(ParcelService data, ResponseCallback<ParcelService> callback, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support update method.");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#list(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void list(ParcelService data, ResponseCallback<CollectionResponse<ParcelService>> callback) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support conditioned list method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#list(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void list(ParcelService data, ResponseCallback<CollectionResponse<ParcelService>> callback, Integer cursor, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support conditioned list method with cursor.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#delete(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void delete(ParcelService data, ResponseCallback<ParcelService> callback) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support delete method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#delete(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)
	 */
	@Override
	public void delete(ParcelService data, ResponseCallback<ParcelService> callback, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support delete method with maximum quantity.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#get(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void get(ParcelService data, ResponseCallback<ParcelService> callback) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support get method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint#getSuccessCode()
	 */
	@Override
	protected RESTfulResponseStatusCodes getSuccessCode() {
		return RESTfulResponseStatusCodes.OK;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint#initGsonAndTypeToken()
	 */
	@Override
	protected void initGsonAndTypeToken() {
		// Due to the format of json string which contains mixed data type, so that providing a deserializer to handle it.
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(ParcelServiceResponse.class, new ParcelServiceResponseDeserializer());
		builder.registerTypeAdapter(ParcelServiceCollection.class, new ParcelServiceCollectionDeserializer());
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.class, new ParcelServiceDeserializer());
		builder.registerTypeAdapter(ParcelServiceOptionCollection.class, new ParcelServiceOptionCollectionDeserializer());
		builder.registerTypeAdapter(ParcelServiceOption.class, new ParcelServiceOptionDeserializer());
		
		Gson gson = builder.create();
		this.setGsonObject(gson);
		this.setResponseObjectType(ParcelServiceResponse.class);
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
