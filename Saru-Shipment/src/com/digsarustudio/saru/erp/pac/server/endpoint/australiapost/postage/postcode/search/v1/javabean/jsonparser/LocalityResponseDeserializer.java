/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.australiapost.postage.postcode.search.v1.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityCollection;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityResponse;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * The deserializer for Gson to parse {@link com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.LocalityResponse} from a JSON string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LocalityResponseDeserializer implements JsonDeserializer<LocalityResponse> {

	/**
	 * 
	 */
	public LocalityResponseDeserializer() {

	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public LocalityResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		JsonObject root = json.getAsJsonObject();
		JsonElement localiesElem	= root.get(LocalityResponse.ATTR_LOCALITIES);
		
		LocalityCollection collection = null;
		if( !localiesElem.isJsonNull() && !localiesElem.isJsonPrimitive()) {
			collection = context.deserialize(localiesElem, LocalityCollection.class);
		}		
		
		return LocalityResponse.builder().setLocalityCollection(collection)
										 .build();
	}

}
