/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost;

import com.digsarustudio.saru.erp.musaceae.shared.endpoint.AuthenticationDetails;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * This class contains the API key of Postage Assessment Calculator to use this service.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			{@link AuthenticationDetails}
 *
 */
public class PACAuthKey implements AuthenticationDetails, IsSerializable {
	/**
	 * 
	 * The object builder for {@link PACAuthKey}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements AuthenticationDetails.Builder {
		private PACAuthKey result = null;

		private Builder() {
			this.result = new PACAuthKey();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.endpoint.AuthenticationDetails.Builder#setToken(java.lang.String)
		 */
		@Override
		public Builder setToken(String token) {
			this.result.setToken(token);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public PACAuthKey build() {
			return this.result;
		}

	}

	private String	apiToken	= null;
	
	/**
	 * 
	 */
	protected PACAuthKey() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.AuthenticationDetails#setToken(java.lang.String)
	 */
	@Override
	public void setToken(String token) {
		this.apiToken = token;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.AuthenticationDetails#getToken()
	 */
	@Override
	public String getToken() {
		
		return this.apiToken;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.AuthenticationDetails#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.apiToken) {
			return false;
		}
		
		if(this.apiToken.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
