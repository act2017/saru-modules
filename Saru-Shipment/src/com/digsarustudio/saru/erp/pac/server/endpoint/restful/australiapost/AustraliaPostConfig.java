/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost;

import com.digsarustudio.saru.erp.musaceae.shared.endpoint.AuthenticationDetails;

/**
 * This data object contains the API key and account number.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AustraliaPostConfig {
	private static AustraliaPostConfig INSTANCE	= null;
	
	private AuthenticationDetails	authenticationDetails	= null;
	
	private AustraliaPostConfig() {}
	
	public static AustraliaPostConfig getInstance() {
		if(null == INSTANCE) {
			INSTANCE = new AustraliaPostConfig();
		}
		
		return INSTANCE;
	}
	
	public void setAuthentication(AuthenticationDetails details) {
		this.authenticationDetails = details;
	}
	
	public void setAPIKey(String apiKey) {
		this.authenticationDetails = PACAuthKey.builder().setToken(apiKey)
												   		 .build();
	}
	
	public AuthenticationDetails getAPIKey() {
		return this.authenticationDetails;
	}
}
