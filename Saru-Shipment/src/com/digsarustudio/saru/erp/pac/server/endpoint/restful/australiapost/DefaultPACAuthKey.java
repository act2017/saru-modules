/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost;

import com.digsarustudio.saru.erp.musaceae.shared.endpoint.AuthenticationDetails;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * This object contains the default details of {@link MerchantDetails}.<br>
 * just new this object.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DefaultPACAuthKey extends PACAuthKey implements AuthenticationDetails, IsSerializable {
	private final String API_KEY	= "ef099ee2-9685-4d36-97cf-3387fb284702";

	/**
	 * 
	 */
	public DefaultPACAuthKey() {
		super();
		
		this.setToken(API_KEY);
	}
}
