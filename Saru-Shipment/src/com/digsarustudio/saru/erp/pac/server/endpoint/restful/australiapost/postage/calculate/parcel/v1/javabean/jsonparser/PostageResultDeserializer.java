/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.musaceae.shared.javabean.UnitPrice;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.DeliveryTime;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCostCollection;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResult;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * The deserializer of {@link PostageResult}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PostageResultDeserializer implements JsonDeserializer<PostageResult> {

	/**
	 * 
	 */
	public PostageResultDeserializer() {
		
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public PostageResult deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		JsonObject	jsonObj = json.getAsJsonObject();
		
		ParcelService			service 		= context.deserialize(jsonObj.get(PostageResult.ATTR_SERVICE), com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.class);
		DeliveryTime			deliveryTime	= context.deserialize(jsonObj.get(PostageResult.ATTR_DELIVERY_TIME), com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.DeliveryTime.class);
		UnitPrice				totalCost		= context.deserialize(jsonObj.get(PostageResult.ATTR_TOTAL_COST), UnitPrice.class);
		PostageCostCollection	costCollection	= context.deserialize(jsonObj.get(PostageResult.ATTR_COSTS), com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageCostCollection.class);
				
		return PostageResult.builder().setService(service)
									  .setDeliveryTime(deliveryTime)
									  .setTotalCost(totalCost)
									  .setPostageCostCollection(costCollection)
									  .build();
	}

}
