/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.australiapost.postage.postcode.search.v1.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.Locality;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityCollection;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * The deserializer for Gson to parse {@link com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.LocalityCollection} from a JSON string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LocalityCollectionDeserializer implements JsonDeserializer<LocalityCollection> {

	/**
	 * 
	 */
	public LocalityCollectionDeserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public LocalityCollection deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		JsonObject root = json.getAsJsonObject();
		
		JsonElement localitiesElem = root.get(LocalityCollection.ATTR_LOCALITY);
		
		Locality[]	localities	= null;
		if(localitiesElem.isJsonArray()) {
			localities = context.deserialize(localitiesElem, Locality[].class);
		}else {
			localities 		= new Locality[1];
			localities[0]	= context.deserialize(localitiesElem, Locality.class);
		}
		
		return LocalityCollection.builder().setLocalities(localities)
										   .build();
	}

}
