/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.australiapost.postage.postcode.search.v1.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.AustralianPostcode;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.AustralianState;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.Locality;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityCategory;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityId;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityLatitude;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityLocation;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityLongitude;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * The deserializer for Gson to parse {@link Locality} from a JSON string.<br> 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LocalityDeserializer implements JsonDeserializer<Locality> {

	/**
	 * 
	 */
	public LocalityDeserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public Locality deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		JsonObject root = json.getAsJsonObject();
		
		LocalityCategory	category	= context.deserialize(root.get(Locality.ATTR_CATEGORY), LocalityCategory.class);
		LocalityId			id			= context.deserialize(root.get(Locality.ATTR_ID), LocalityId.class);
		LocalityLatitude	latitude	= context.deserialize(root.get(Locality.ATTR_LATITUDE), LocalityLatitude.class);
		LocalityLocation	location	= context.deserialize(root.get(Locality.ATTR_LOCATION), LocalityLocation.class);
		LocalityLongitude	longitude	= context.deserialize(root.get(Locality.ATTR_LONGITUDE), LocalityLongitude.class);
		AustralianPostcode	postcode	= context.deserialize(root.get(Locality.ATTR_POSTCODE), AustralianPostcode.class);
		AustralianState		state		= context.deserialize(root.get(Locality.ATTR_STATE), AustralianState.class);
		
		return Locality.builder().setCategory(category)
								 .setId(id)
								 .setLatitude(latitude)
								 .setLocation(location)
								 .setLongitude(longitude)
								 .setPostcode(postcode)
								 .setState(state)
								 .build();
	}

}
