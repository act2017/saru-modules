/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.exception.builder;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type has the ability to generate a new exception for a particular RESTful HTTP response code.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface RESTfulResponseExceptionBuilder<T extends Throwable> extends ObjectBuilder<T> {
	RESTfulResponseExceptionBuilder<T> setMessage(String msg);
	RESTfulResponseExceptionBuilder<T> setCause(Throwable cause);
}
