/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelServiceOption;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelServiceOptionCollection;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * The deserializer for {@link ParcelService} to parse json string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ParcelServiceOptionDeserializer implements JsonDeserializer<ParcelServiceOption> {
	/**
	 * 
	 */
	public ParcelServiceOptionDeserializer() {
	}
	
	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public ParcelServiceOption deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		JsonObject								jsonObj					= json.getAsJsonObject();
		
		String									code					= jsonObj.get(ParcelServiceOption.ATTR_CODE).getAsString();
		String									name					= jsonObj.get(ParcelServiceOption.ATTR_NAME).getAsString();
		
		JsonElement								maxExtraCoverElem		= jsonObj.get(ParcelServiceOption.ATTR_MAX_EXTRA_COVER);		
		//This field doesn't exist in ServiceOption right now.<br>		
		Integer									maxExtraCover			= null;
		if( null != maxExtraCoverElem && !maxExtraCoverElem.isJsonNull() ) {
			maxExtraCover = maxExtraCoverElem.getAsInt();
		}		
		
		JsonElement								subOptionElem			= jsonObj.get(ParcelServiceOption.ATTR_SUBOPTIONS);
		ParcelServiceOptionCollection			subOptions				= null;
		if(null != subOptionElem && !subOptionElem.isJsonNull()) {
			subOptions	= context.deserialize(subOptionElem, ParcelServiceOptionCollection.class);
		}
		
		ParcelServiceOption serviceOption = ParcelServiceOption.builder().setCode(code)
																		 .setName(name)
																		 .setMaxExtraCover(maxExtraCover)
																		 .build();
		if(null != subOptions) {
			subOptions.setParentParcelServiceOption(serviceOption);
		}
		serviceOption.setServiceSubOptionCollection(subOptions);
		
		return serviceOption;
	}

}
