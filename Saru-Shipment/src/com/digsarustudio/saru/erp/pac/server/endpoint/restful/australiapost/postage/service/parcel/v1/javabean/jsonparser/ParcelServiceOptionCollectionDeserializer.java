/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelServiceOption;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelServiceOptionCollection;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * The deserializer for {@link ParcelServiceOptionCollection} to parse json string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ParcelServiceOptionCollectionDeserializer implements JsonDeserializer<ParcelServiceOptionCollection> {
	
	/**
	 * 
	 */
	public ParcelServiceOptionCollectionDeserializer() {
	}
	
	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public ParcelServiceOptionCollection deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		JsonObject				jsonObj					= json.getAsJsonObject();
		JsonElement				jsonElem				= jsonObj.get(ParcelServiceOptionCollection.ATTR_OPTION);
		
		ParcelServiceOption[]	parcelServiceOptions	= null;
		if(jsonElem.isJsonArray()) {
			parcelServiceOptions	= context.deserialize(jsonObj.get(ParcelServiceOptionCollection.ATTR_OPTION), ParcelServiceOption[].class);
		}else {
			parcelServiceOptions	= new ParcelServiceOption[1];
			parcelServiceOptions[0] = context.deserialize(jsonObj.get(ParcelServiceOptionCollection.ATTR_OPTION), ParcelServiceOption.class);
		}		 
		
		return ParcelServiceOptionCollection.builder().setOptionServices(parcelServiceOptions)
											    	  .build();
	}

}
