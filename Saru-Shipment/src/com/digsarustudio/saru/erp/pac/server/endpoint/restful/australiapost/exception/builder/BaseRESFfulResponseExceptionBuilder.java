/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.exception.builder;

/**
 * The base class of {@link RESTfulResponseExceptionBuilder}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public abstract class BaseRESFfulResponseExceptionBuilder<T extends Throwable> implements RESTfulResponseExceptionBuilder<T> {
	private String	 	msg		= null;
	private Throwable	cause	= null;
	
	/**
	 * 
	 */
	public BaseRESFfulResponseExceptionBuilder() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder#setMessage(java.lang.String)
	 */
	@Override
	public RESTfulResponseExceptionBuilder<T> setMessage(String msg) {
		this.msg = msg;
		return this;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder#setCause(java.lang.Throwable)
	 */
	@Override
	public RESTfulResponseExceptionBuilder<T> setCause(Throwable cause) {
		this.cause = cause;
		return this;
	}

	protected String getMessage() {
		return this.msg;
	}
	
	protected Throwable getCause() {
		return this.cause;
	}
}
