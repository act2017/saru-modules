/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.exception.builder;

import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.exception.NotFoundException;

/**
 * The {@link NotFoundException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class NotFoundExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<NotFoundException> 
											 implements RESTfulResponseExceptionBuilder<NotFoundException> {
		
	/**
	 * 
	 */
	public NotFoundExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public NotFoundException build() {
		return new NotFoundException(this.getMessage(), this.getCause());
	}

}
