/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelServiceCollection;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelServiceResponse;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * The deserializer for {@link ParcelServiceResponse} to parse json string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ParcelServiceResponseDeserializer implements JsonDeserializer<ParcelServiceResponse> {

	/**
	 * 
	 */
	public ParcelServiceResponseDeserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public ParcelServiceResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		JsonObject				jsonObj					= json.getAsJsonObject();
		ParcelServiceCollection	parcelServiceCollection = context.deserialize(jsonObj.get(ParcelServiceResponse.ATTR_SERVICES), ParcelServiceCollection.class); 
		
		return ParcelServiceResponse.builder().setParcelServiceCollection(parcelServiceCollection)
											  .build();
	}

}
