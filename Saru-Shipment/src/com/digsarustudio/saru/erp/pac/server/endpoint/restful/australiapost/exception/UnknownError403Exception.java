/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.exception;

/**
 * Unknown Error 403 occurred when the client code sent a request to the PAC service.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class UnknownError403Exception extends Exception {

	/**
	 * 
	 */
	public UnknownError403Exception() {

	}

	/**
	 * @param message
	 */
	public UnknownError403Exception(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public UnknownError403Exception(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnknownError403Exception(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public UnknownError403Exception(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
