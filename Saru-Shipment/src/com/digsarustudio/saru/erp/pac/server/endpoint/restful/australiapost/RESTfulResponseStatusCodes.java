/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost;

import java.io.Serializable;

/**
 * This class contains the status code with the name and description provided by Australia Post API.<br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/restful">REST and Authentication</a>
 */
@SuppressWarnings("serial")
public class RESTfulResponseStatusCodes implements Serializable {
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 200</li>
	 * 	<li><b>Name</b>: OK</li>
	 * 	<li><b>Description:</b> Optional okay (success) message.</li>
	 * </ul>
	 */
	public static final RESTfulResponseStatusCodes OK				= new RESTfulResponseStatusCodes(200, "OK", "Optional okay (success) message.");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 403</li>
	 * 	<li><b>Name</b>: NotFound</li>
	 * 	<li><b>Description:</b> Not found</li>
	 * </ul>
	 */
	public static final RESTfulResponseStatusCodes	UnknownError403			= new RESTfulResponseStatusCodes(403, "UnknownError403", null);	
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 404</li>
	 * 	<li><b>Name</b>: NotFound</li>
	 * 	<li><b>Description:</b> Not found</li>
	 * </ul>
	 */
	public static final RESTfulResponseStatusCodes	NotFound			= new RESTfulResponseStatusCodes(404, "NotFound", null);
	

	private Integer code 		= null;
	private String name 		= null;
	private String description	= null;

	private static RESTfulResponseStatusCodes[] VALUES = { OK, NotFound
														 , UnknownError403
												  		 };

	private RESTfulResponseStatusCodes(Integer code, String name, String description) {
		this.code 		= code;
		this.name 		= name;
		this.description= description;
	}

	public Integer getCode() {
		return this.code;
	}

	public String getName() {
		return this.name;
	}
	
	public String getDescription() {
		return this.description;
	}

	public static RESTfulResponseStatusCodes fromCode(Integer code) {
		if (null == code) {
			return null;
		}

		RESTfulResponseStatusCodes rtn = null;

		for (RESTfulResponseStatusCodes target : VALUES) {
			if (!code.equals(target.getCode())) {
				continue;
			}

			rtn = target;
		}

		return rtn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RESTfulResponseStatusCodes other = (RESTfulResponseStatusCodes) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

}
