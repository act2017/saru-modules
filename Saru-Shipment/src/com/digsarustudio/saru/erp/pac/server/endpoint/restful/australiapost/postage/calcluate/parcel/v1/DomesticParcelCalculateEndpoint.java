/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.server.jsonparser.UnitPriceDeserializer;
import com.digsarustudio.saru.erp.musaceae.shared.FreightSize;
import com.digsarustudio.saru.erp.musaceae.shared.LengthUnit;
import com.digsarustudio.saru.erp.musaceae.shared.UnitPrice;
import com.digsarustudio.saru.erp.musaceae.shared.Weight;
import com.digsarustudio.saru.erp.musaceae.shared.WeightUnit;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.AuthenticationDetails;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.ResponseCallback;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.QueryParameter;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.RESTfulResponseStatusCodes;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.jsonparser.DeliveryTimeDeserializer;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.jsonparser.ParcelServiceDeserializer;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.jsonparser.PostageCostCollectionDeserializer;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.jsonparser.PostageCostDeserializer;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.jsonparser.PostageCostItemDeserializer;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.jsonparser.PostageResultDeserializer;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.jsonparser.PostageResultResponseDeserializer;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.DeliveryTime;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageCost;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageCostCollection;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageCostItem;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResultResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Returns the total postage cost for a domestic parcel based on speed of delivery selected, dimensions, 
 * weight and any included additional features such as proof of delivery and extra cover.
 * <h3>Resource Information</h3>
 * <table>
 * 	<tr>
 * 		<td>Rate Limited?</td><td>No</td><td>HTTP Methods</td><td>GET</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Requests per rate limit window</td><td>-</td><td>Resource family</td><td>parcel</td>
 * 	<tr>
 * 		<td>Authentication</td><td>API key</td><td>Response object</td><td>Postage</td>
 * 	<tr>
 * 		<td>Response Formats</td><td>json, xml</td><td>API Version</td><td>v1.0</td>
 * 	</tr>
 * </table>
 * <br>
 * <h3>Resource URL</h3>
 * https://digitalapi.auspost.com.au/postage/parcel/domestic/calculate
 * <h3>Headers</h3>
 * auth-key(required)	Your PAC API Key (HTTP Header)<br>
 * <br>
 * <h3>Parameters</h3>
 * from_postcode(required)	The postcode the parcel will be sent from.<br>
 * to_postcode(required)	The postcode the parcel will be sent to.<br>
 * length(required)			The parcel length in CMs.<br>
 * width(required)			The parcel width in CMs.<br>
 * height(required)			The parcel height in CMs<br>
 * weight(required)			The parcel weight in KGs<br>
 * service_code(required)	The chosen product / service code
 * option_code(optional)	The chosen option code for the product option
 * suboption_code(optional)	The chosen suboption code for the product suboption
 * extra_cover(optional)	The dollar amount of the extra cover required
 * <br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/postage-parcel-domestic-calculate">postage/parcel/domestic/calculator</a>
 *
 */
public class DomesticParcelCalculateEndpoint extends AustraliaPostEndpoint<PostageResult, PostageResult> {		
	private static final String API_PATH				= "postage/parcel/domestic/calculate.json";
	private static final String	PARAM_SENDER_POSTCODE	= "from_postcode";
	private static final String	PARAM_RECEIVER_POSTCODE	= "to_postcode";
	private static final String	PARAM_LENGTH			= "length";
	private static final String	PARAM_WIDTH				= "width";
	private static final String	PARAM_HEIGHT			= "height";
	private static final String	PARAM_WEIGHT			= "weight";
	private static final String	PARAM_SERVICE_CODE		= "service_code";
	private static final String	PARAM_OPTION_CODE		= "option_code";
	private static final String	PARAM_SUBOPTION_CODE	= "suboption_code";
	private static final String	PARAM_EXTRA_COVER		= "extra_cover";
	
	
	/**
	 * 
	 * The object builder for {@link DomesticParcelCalculateEndpoint}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<DomesticParcelCalculateEndpoint> {
		private DomesticParcelCalculateEndpoint result = null;

		private Builder() {
			this.result = new DomesticParcelCalculateEndpoint();
			this.result.setURL(API_PATH);
		}

		/**
		 * @param details
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint#setAuthKey(com.digsarustudio.banana.endpoint.AuthenticationDetails)
		 */
		public Builder setAuthKey(AuthenticationDetails details) {
			result.setAuthKey(details);
			return this;
		}

		/**
		 * @param postcode
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#setSenderPostcode(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode)
		 */
		public Builder setSenderPostcode(Postcode postcode) {
			result.setSenderPostcode(postcode);
			return this;
		}

		/**
		 * @param postcode
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#setReceiverPostcode(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode)
		 */
		public Builder setReceiverPostcode(Postcode postcode) {
			result.setReceiverPostcode(postcode);
			return this;
		}

		/**
		 * @param size
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#setFreightSize(com.digsarustudio.saru.erp.musaceae.shared.FreightSize)
		 */
		public Builder setFreightSize(FreightSize size) {
			result.setFreightSize(size);
			return this;
		}

		/**
		 * @param weight
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#setFreightWeight(com.digsarustudio.saru.erp.musaceae.shared.Weight)
		 */
		public Builder setFreightWeight(Weight weight) {
			result.setFreightWeight(weight);
			return this;
		}

		/**
		 * @param service
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#setParcelService(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService)
		 */
		public Builder setParcelService(ParcelService service) {
			result.setParcelService(service);
			return this;
		}

		/**
		 * @param extraCover
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calcluate.parcel.v1.DomesticParcelCalculateEndpoint#setExtraCover(com.digsarustudio.banana.erp.general.UnitPrice)
		 */
		public Builder setExtraCover(UnitPrice extraCover) {
			result.setExtraCover(extraCover);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DomesticParcelCalculateEndpoint build() {
			return this.result;
		}

	}
	
	/**
	 * This field used to make sure the extra cover the customer wants does not exceed its maximum.
	 */
	private ParcelService	parcelService	= null;
	private UnitPrice		extraCover		= null;
	
	private DomesticParcelCalculateEndpoint() {
		
	}
	
	public void setSenderPostcode(Postcode postcode) {
		this.addQueryParameter(QueryParameter.builder().setKey(PARAM_SENDER_POSTCODE)
													   .setValue(postcode.getCode())
													   .build());
	}
	
	public void setReceiverPostcode(Postcode postcode) {
		this.addQueryParameter(QueryParameter.builder().setKey(PARAM_RECEIVER_POSTCODE)
													   .setValue(postcode.getCode())
													   .build());
	}
	
	public void setFreightSize(FreightSize size) {
		FreightSize confirmed = size;
		
		if(LengthUnit.CentiMetre != size.getLength().getUnit() 
		|| LengthUnit.CentiMetre != size.getWidth().getUnit()
		|| LengthUnit.CentiMetre != size.getHeight().getUnit()) {
			confirmed = size.toCentimetre();
		}
		
		this.addQueryParameter(QueryParameter.builder().setKey(PARAM_LENGTH)
													   .setValue(confirmed.getLength().getValue())
													   .build());
		
		this.addQueryParameter(QueryParameter.builder().setKey(PARAM_WIDTH)
													   .setValue(confirmed.getWidth().getValue())
													   .build());
		
		this.addQueryParameter(QueryParameter.builder().setKey(PARAM_HEIGHT)
													   .setValue(confirmed.getHeight().getValue())
													   .build());
	}
	
	public void setParcelService(ParcelService service) {
		this.parcelService = service;
		
		if(null == this.parcelService || null == this.parcelService.getCode() || this.parcelService.getCode().isEmpty()) {
			return;
		}				
		
		this.addQueryParameter(QueryParameter.builder().setKey(PARAM_SERVICE_CODE)
													   .setValue(this.parcelService.getCode())
													   .build());
		
		if(!this.parcelService.hasServiceOption()) {
			//Check max extra cover
			if(null != this.extraCover) {
				//run the extra cover check again
				this.setExtraCover(this.extraCover);
			}
		}else {
			//Due to the incoming ParcelService only has one set of option service, so that just pick up the 1st one to calculate.
			ParcelServiceOption serviceOption = service.getServiceOptionCollection().getOptionService(0);
			
			this.addQueryParameter(QueryParameter.builder().setKey(PARAM_OPTION_CODE)
														   .setValue(serviceOption.getCode())
														   .build());
						
			if( serviceOption.hasSubOptions() ) {
				////Due to the incoming ParcelService only has one set of service sub option, so that just pick up the 1st one to calculate.
				ParcelServiceOption subOption = serviceOption.getServiceSubOptionCollection().getOptionService(0);
				
				this.addQueryParameter(QueryParameter.builder().setKey(PARAM_SUBOPTION_CODE)
															   .setValue(subOption.getCode())
															   .build());				
				//check max extra cover
				if(null != this.extraCover) {
					//run the extra cover check again
					this.setExtraCover(this.extraCover);
				}
			}
		}
	}
	
	public void setExtraCover(UnitPrice extraCover) {
		this.extraCover = extraCover;
		
		if(null == this.parcelService || null == this.parcelService.getCode() || this.parcelService.getCode().isEmpty()) {			
			return;
		}		
		
		Integer maxExtraCover = this.parcelService.getMaxExtraCover();
		if(this.parcelService.hasServiceOption()) {
			ParcelServiceOption serviceOption = this.parcelService.getServiceOptionCollection().getOptionService(0);
			
			if( null != serviceOption ) {
				if(null != serviceOption.getMaxExtraCover()) {
					maxExtraCover = serviceOption.getMaxExtraCover();
				}				
				
				if(serviceOption.hasSubOptions()) {
					ParcelServiceOption subOption = serviceOption.getSubOption(0);
					
					if(null != subOption && null != subOption.getMaxExtraCover()) {
						maxExtraCover = subOption.getMaxExtraCover();
					}
				}
			}
		}
		
		//The max extra over provided by an sub option of a service. This parameter doesn't need to be set if the sub option service is not the "max extra cover".
		if(null == maxExtraCover) {
			return;
		}
		
		Integer exactExtraCover = new Integer(extraCover.getPrice());
		if( maxExtraCover <= exactExtraCover) {
			exactExtraCover = maxExtraCover;
		}
		
		QueryParameter param = this.getQueryParameter(PARAM_EXTRA_COVER);
		if(null != param) {
			param.setValue(exactExtraCover.toString());
		}else {
			param = QueryParameter.builder().setKey(PARAM_EXTRA_COVER)
										    .setValue(exactExtraCover.toString())
										    .build();
		}
		
		this.addQueryParameter(param);
	}
	
	public void setFreightWeight(Weight weight) {
		Weight confirmed = weight;
		
		if(WeightUnit.Kilogram != weight.getUnit()) {
			confirmed = weight.toKilograms();
		}
		
		this.addQueryParameter(QueryParameter.builder().setKey(PARAM_WEIGHT)
													   .setValue(confirmed.getValue())
													   .build());		
	}
	
	public void list(ResponseCallback<CollectionResponse<PostageResult>> callback) throws EndpointException {
		this.sendRESTfulGetRequest(new ResponseCallback<PostageResultResponse>() {

			@Override
			public void onSuccess(PostageResultResponse result) {
				CollectionResponse.Builder<PostageResult> builder = com.digsarustudio.saru.erp.musaceae.shared.endpoint.javabean.CollectionResponse.<PostageResult>builder();
				List<PostageResult> postageResults = new ArrayList<>();
				postageResults.add(result.getPostageResult());
				builder.setItems(postageResults);
				
				callback.onSuccess(builder.build());				
			}

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
		});
	}
	
	public PostageResult calculatePostage() throws EndpointException{
		PostageResultResponse response = this.sendRESTfulGetRequest();
		if(null == response) {
			return null;
		}
		
		return response.getPostageResult();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#insert(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void insert(PostageResult data, ResponseCallback<PostageResult> callback) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support inert method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#update(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void update(PostageResult data, ResponseCallback<PostageResult> callback) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support update method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#update(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)
	 */
	@Override
	public void update(PostageResult data, ResponseCallback<PostageResult> callback, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support update method.");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#list(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void list(PostageResult data, ResponseCallback<CollectionResponse<PostageResult>> callback) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support conditioned list method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#list(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void list(PostageResult data, ResponseCallback<CollectionResponse<PostageResult>> callback, Integer cursor, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support conditioned list method with cursor.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#delete(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void delete(PostageResult data, ResponseCallback<PostageResult> callback) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support delete method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#delete(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)
	 */
	@Override
	public void delete(PostageResult data, ResponseCallback<PostageResult> callback, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support delete method with maximum quantity.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#get(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void get(PostageResult data, ResponseCallback<PostageResult> callback) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support get method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint#getSuccessCode()
	 */
	@Override
	protected RESTfulResponseStatusCodes getSuccessCode() {
		return RESTfulResponseStatusCodes.OK;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint#initGsonAndTypeToken()
	 */
	@Override
	protected void initGsonAndTypeToken() {
		// Due to the format of json string which contains mixed data type, so that providing a deserializer to handle it.
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(PostageResultResponse.class, new PostageResultResponseDeserializer());
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResult.class, new PostageResultDeserializer());
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService.class, new ParcelServiceDeserializer());
		builder.registerTypeAdapter(DeliveryTime.class, new DeliveryTimeDeserializer());
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.musaceae.shared.javabean.UnitPrice.class, new UnitPriceDeserializer());
		builder.registerTypeAdapter(PostageCostCollection.class, new PostageCostCollectionDeserializer());
		builder.registerTypeAdapter(PostageCost.class, new PostageCostDeserializer());
		builder.registerTypeAdapter(PostageCostItem.class, new PostageCostItemDeserializer());
		
		Gson gson = builder.create();
		this.setGsonObject(gson);
		this.setResponseObjectType(PostageResultResponse.class);
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
