/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.country.v1;

import java.util.Iterator;
import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.AuthenticationDetails;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.ResponseCallback;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.RESTfulResponseStatusCodes;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country.Country;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.country.v1.javabean.CountryResponse;

/**
 * This endpoint used to get a list of country from Australia Postage Calculation Assessment.<br>
 * <br>
 * Returns a list of countries which are accepted by the PAC API.<br>
 * <br>
 * <table>
 * 	<tr>
 * 		<td>Rate Limited?</td><td>No</td><td>HTTP Methods</td><td>GET</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Requests per rate limit window</td><td>-</td><td>Resource family</td><td>Data</td>
 * 	<tr>
 * 		<td>Authentication</td><td>API key</td><td>Response object</td><td>Postage</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Response Formats</td><td>json, xml</td><td>API Version</td><td>v1.0</td>
 * <br>
 * <h3>Resource URL</h3>
 * https://digitalapi.auspost.com.au/postage/country<br>
 * <br>
 * <h3>Headers</h3>
 * 	auth-key(required)		Your PAC API Key (HTTP Header)<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/country">country</a>
 *
 */
public class CountryEndpoint extends AustraliaPostEndpoint<Country, Country> {
	private static final String API_PATH = "postage/country.json";
	
	/**
	 * 
	 * The object builder for {@link CountryEndpoint}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<CountryEndpoint> {
		private CountryEndpoint result = null;

		private Builder() {
			this.result = new CountryEndpoint();
			this.result.setURL(CountryEndpoint.API_PATH);
		}

		/**
		 * @param details
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint#setAuthKey(com.digsarustudio.banana.endpoint.AuthenticationDetails)
		 */
		public Builder setAuthKey(AuthenticationDetails details) {
			result.setAuthKey(details);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public CountryEndpoint build() {
			return this.result;
		}

	}
	
	private CountryEndpoint() {
	}
	
	public void list(final ResponseCallback<CollectionResponse<Country>> callback) throws EndpointException{
		this.sendRESTfulGetRequest(new ResponseCallback<CountryResponse>() {

			@Override
			public void onSuccess(CountryResponse result) {
				CollectionResponse.Builder<Country> builder = com.digsarustudio.saru.erp.musaceae.shared.endpoint.javabean.CollectionResponse.<Country>builder();
				List<Country> countries = result.getCountries();
				builder.setItems(countries);
				
				callback.onSuccess(builder.build());
			}

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
		});
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#insert(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void insert(Country data, final ResponseCallback<Country> callback) throws EndpointException {
		throw new UnsupportedOperationException("This api doesn't support insert method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#update(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void update(Country data, final ResponseCallback<Country> callback) throws EndpointException {
		throw new UnsupportedOperationException("This api doesn't support update method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#update(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)
	 */
	@Override
	public void update(Country data, final ResponseCallback<Country> callback, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("This api doesn't support update method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#list(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void list(Country data, final ResponseCallback<CollectionResponse<Country>> callback) throws EndpointException {
		if(null == data) {
			this.list(callback);
		}else {
			this.get(data, new ResponseCallback<Country>() {

				@Override
				public void onSuccess(Country result) {
					CollectionResponse<Country> collection = com.digsarustudio.saru.erp.musaceae.shared.endpoint.javabean.CollectionResponse.<Country>builder().addItem(result)
																															  										   .build();
					callback.onSuccess(collection);
				}

				@Override
				public void onFailure(Throwable caught) {
					callback.onFailure(caught);
				}
			});
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#list(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void list(Country data, final ResponseCallback<CollectionResponse<Country>> callback, Integer cursor,Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("This api doesn't support list method with cursor.");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#delete(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void delete(Country data, final ResponseCallback<Country> callback) throws EndpointException {
		throw new UnsupportedOperationException("This api doesn't support delete method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#delete(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)
	 */
	@Override
	public void delete(Country data, final ResponseCallback<Country> callback, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("This api doesn't support delete method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#get(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void get(final Country data, final ResponseCallback<Country> callback) throws EndpointException {
		if(null == callback) {
			return;
		}
		
		this.list(new ResponseCallback<CollectionResponse<Country>>() {

			@Override
			public void onSuccess(CollectionResponse<Country> result) {
				if(null == result || null == result.getItems()) {
					callback.onSuccess(null);
					return;
				}
				
				Iterator<Country> iterator = result.getItems().iterator();
				while(iterator.hasNext()) {
					Country returned = iterator.next();
					if(null == returned) {
						continue;
					}
					
					if( data.equals(returned) ) {
						callback.onSuccess(returned);
						break;
					}
				}
			}

			@Override
			public void onFailure(Throwable caught) {			
				callback.onFailure(caught);
			}
		});
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint#getSuccessCode()
	 */
	@Override
	protected RESTfulResponseStatusCodes getSuccessCode() {

		return RESTfulResponseStatusCodes.OK;
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint#initGsonAndTypeToken()
	 */
	@Override
	protected void initGsonAndTypeToken() {
//Only workable for polymorphism of an interface.
//		TypeToken<CountryResponse> typeToken = new TypeToken<CountryResponse>() {
//		};		
//		this.setTypeToken(typeToken);
//		
//		RuntimeTypeAdapterFactory<CountryResponse> responseTypeFactory = RuntimeTypeAdapterFactory.of(CountryResponse.class)
//																						  		  .registerSubtype(com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.v1.javabean.CountryResponse.class);
//		
//		RuntimeTypeAdapterFactory<CountryCollection> collectionTypeFactory = RuntimeTypeAdapterFactory.of(CountryCollection.class)
//																									  .registerSubtype(com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.v1.javabean.CountryCollection.class);
//		
//		RuntimeTypeAdapterFactory<Country> countryTypeFactory = RuntimeTypeAdapterFactory.of(Country.class)
//																						 .registerSubtype(com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.v1.javabean.Country.class);
//		this.setGsonObject(new GsonBuilder().registerTypeAdapterFactory(responseTypeFactory)
////											.registerTypeAdapterFactory(collectionTypeFactory)
////											.registerTypeAdapterFactory(countryTypeFactory)
//											.create());
		
		this.setResponseObjectType(CountryResponse.class);
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
