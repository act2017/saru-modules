/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageCost;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageCostCollection;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * The json string deserializer of {@link PostageCostCollection}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PostageCostCollectionDeserializer implements JsonDeserializer<PostageCostCollection> {

	/**
	 * 
	 */
	public PostageCostCollectionDeserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public PostageCostCollection deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		JsonObject jsonObj = json.getAsJsonObject();		
		JsonElement	costElem	= jsonObj.get(PostageCostCollection.ATTR_COST);
		
		PostageCost[] postageCosts = null;
		if(costElem.isJsonArray()) {
			postageCosts = context.deserialize(costElem, PostageCost[].class);
		}else {
			postageCosts	= new PostageCost[1];
			postageCosts[0]	= context.deserialize(costElem, PostageCost.class); 
		}		
		
		return PostageCostCollection.builder().setPostageCosts(postageCosts)
											  .build();
	}
}
