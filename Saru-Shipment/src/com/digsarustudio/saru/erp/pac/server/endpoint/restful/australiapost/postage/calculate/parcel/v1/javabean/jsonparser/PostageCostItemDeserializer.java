/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageCostItem;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * The json string deserializer of {@link PostageCostItem}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PostageCostItemDeserializer implements JsonDeserializer<PostageCostItem> {

	/**
	 * 
	 */
	public PostageCostItemDeserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public PostageCostItem deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		return PostageCostItem.builder().setName(json.getAsString())
										.build();
	}

}
