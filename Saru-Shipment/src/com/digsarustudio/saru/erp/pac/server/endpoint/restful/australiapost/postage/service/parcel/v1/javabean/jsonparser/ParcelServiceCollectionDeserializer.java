/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelServiceCollection;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * The deserializer for {@link ParcelServiceCollection} to parse json string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ParcelServiceCollectionDeserializer implements JsonDeserializer<ParcelServiceCollection> {

	/**
	 * 
	 */
	public ParcelServiceCollectionDeserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public ParcelServiceCollection deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		JsonObject				jsonObj					= json.getAsJsonObject();
		JsonElement				parcelServiceElem		= jsonObj.get(ParcelServiceCollection.ATTR_SERVICE);
		
		ParcelService[]			parcelServices			= null;
		if(null != parcelServiceElem) {
			if(parcelServiceElem.isJsonArray()) {
				parcelServices			= context.deserialize(parcelServiceElem, ParcelService[].class);
			}else {
				parcelServices = new ParcelService[1];
				parcelServices[0] = context.deserialize(parcelServiceElem, ParcelService.class);
			}
		}
		
		return ParcelServiceCollection.builder().setParcelServices(parcelServices)
											    .build();
	}

}
