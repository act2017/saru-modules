/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.australiapost.postage.postcode.search.v1.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityLongitude;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * The deserializer for Gson to parse {@link LocalityLongitude} from a JSON string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LocalityLongitudeDeserializer implements JsonDeserializer<LocalityLongitude> {

	/**
	 * 
	 */
	public LocalityLongitudeDeserializer() {
		
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public LocalityLongitude deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		String longitude = json.getAsString();
		
		return LocalityLongitude.builder().setValue(longitude)
										  .build();
	}

}
