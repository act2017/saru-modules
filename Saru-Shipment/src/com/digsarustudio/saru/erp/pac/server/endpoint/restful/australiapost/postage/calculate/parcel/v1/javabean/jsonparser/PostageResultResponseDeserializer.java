/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResultResponse;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * To deserializer of {@link PostageResultResponse}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PostageResultResponseDeserializer implements JsonDeserializer<PostageResultResponse> {

	/**
	 * 
	 */
	public PostageResultResponseDeserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public PostageResultResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		JsonObject jsonObj	= json.getAsJsonObject();
		
		PostageResult postageResult = context.deserialize(jsonObj.get(PostageResultResponse.ATTR_POSTAGE_RESULT), com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageResult.class);
		
		return PostageResultResponse.builder().setPostageResult(postageResult)
										.build();
	}

}
