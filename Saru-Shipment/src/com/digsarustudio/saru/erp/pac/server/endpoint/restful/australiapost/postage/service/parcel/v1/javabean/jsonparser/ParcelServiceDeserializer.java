/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelService;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelServiceOptionCollection;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

/**
 * The deserializer for {@link ParcelService} to parse json string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ParcelServiceDeserializer implements JsonDeserializer<ParcelService> {

	/**
	 * 
	 */
	public ParcelServiceDeserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public ParcelService deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		JsonObject								jsonObj					= json.getAsJsonObject();
		
		String									code					= jsonObj.get(ParcelService.ATTR_CODE).getAsString();
		String									name					= jsonObj.get(ParcelService.ATTR_NAME).getAsString();
		String									price					= jsonObj.get(ParcelService.ATTR_PRICE).getAsString();
		
		//optional for some countries
		JsonElement								maxExtraCoverElem		= jsonObj.get(ParcelService.ATTR_MAX_EXTRA_COVER);
		Integer									maxExtraCover			= null;
		if( null != maxExtraCoverElem && !maxExtraCoverElem.isJsonNull() ) {
			maxExtraCover			= maxExtraCoverElem.getAsInt();	
		}
		
		ParcelServiceOptionCollection			parcelOptionServiceCollection	= context.deserialize(jsonObj.get(ParcelService.ATTR_OPTIONS), ParcelServiceOptionCollection.class); 
		
		ParcelService service	= ParcelService.builder().setCode(code)
									  					 .setName(name)
									  					 .setPrice(price)
									  					 .setMaxExtraCover(maxExtraCover)									  					 
									  					 .build();

		parcelOptionServiceCollection.setParentParcelService(service);
		service.setServiceOptionCollection(parcelOptionServiceCollection);
		
		return service;
	}

}
