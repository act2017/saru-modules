/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.header;

import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.BaseHttpHeaderField;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.HttpHeaderField;

/**
 * The header for the client code to pass Authentication key to PAC service.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AuthKey extends BaseHttpHeaderField implements HttpHeaderField {
	private static final String HEADER = "auth-key";

	/**
	 * 
	 * The object builder for {@link AuthKey}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements HttpHeaderField.Builder {
		private AuthKey result = null;

		private Builder() {
			this.result = new AuthKey();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.endpoint.http.header.HttpHeaderField.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AuthKey build() {
			return this.result;
		}

	}
	
	/**
	 * 
	 */
	public AuthKey() {
		super(HEADER);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.http.header.HttpHeaderField#getHeader()
	 */
	@Override
	public String getHeader() {
		return HEADER;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
