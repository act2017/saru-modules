/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost;

import java.lang.reflect.Type;
import java.util.Map;

import com.digsarustudio.banana.utils.StringFormatter;
import com.digsarustudio.saru.erp.musaceae.server.endpoint.http.HttpException;
import com.digsarustudio.saru.erp.musaceae.server.endpoint.http.HttpResponse;
import com.digsarustudio.saru.erp.musaceae.server.endpoint.restful.RESTfulEndpoint;
import com.digsarustudio.saru.erp.musaceae.server.endpoint.restful.UnknownResponseStatusCodeException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.AuthenticationDetails;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.Endpoint;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.ResponseCallback;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.fields.Accept;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.fields.ContentType;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.HttpsURL;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.URL;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.header.AuthKey;
import com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.v1.javabean.ErrorResponse;
import com.digsarustudio.saru.erp.shipment.server.utils.URLEncoder;
import com.google.gson.Gson;

/**
 * The endpoint for client code to send request and receive response from the RESTful service maintained by Australia Post.<br>
 * Please setup the path of service, declare incoming and outgoing data object and query parameter, 
 * and handle error code in the sub-types.
 * 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/docs/reference">Australia Post API Reference</a>
 * 
 * @param		T	The data type of incoming data
 * @param		E	The data type of outgoing data
 * @param		C	The data type of received data from server
 * 
 */
public abstract class AustraliaPostEndpoint<T, E> extends RESTfulEndpoint<T, E> implements Endpoint<T, E> {
	protected static final String BASE_URL		= "https://digitalapi.auspost.com.au";
	protected static final String DOMAIN_NAME	= "digitalapi.auspost.com.au";
	
	protected Map<Integer, ErrorCodes>	errorCodes	= null;
	
	/**
	 * Please generate the particular gson object for this class to parse the data type from sub-class.<br>
	 * 
	 * @see <a href="https://stackoverflow.com/questions/16396904/using-gson-with-interface-types">Using Gson with Interface Types</a>
	 */
	private Gson						gson		= null;
	
	private Type						responseObjType	= null;
	
	public AustraliaPostEndpoint() {
		super();
		
		this.initHeader();
		this.initGsonAndTypeToken();
	}
	
	/**
	 * Assign the details of a merchant.<br>
	 * 
	 * @param details The details of a merchant.<br>
	 */
	public void setAuthKey(AuthenticationDetails details) {
		this.addHeader(AuthKey.builder().setValue(details.getToken())
										.build());
	}
	
	protected void setURL(String functionPath) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(functionPath);
		
		URL url = HttpsURL.builder().setEncoder(new URLEncoder())
									.setDomainName(DOMAIN_NAME)									
									.setPath(buffer.toString())
									.build();
		
		this.setURL(url);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.RESTfulEndpoint#sendRESTfulGetRequest(com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	protected <P> void sendRESTfulGetRequest(ResponseCallback<P> callback) throws EndpointException{
		HttpResponse response = null;
		try {
			response = this.sendGetRequest();
		} catch (HttpException e) {
			throw new EndpointException(e.getMessage(), e);
		}
		
		this.parseResponse(response, callback);
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.RESTfulEndpoint#sendRESTfulPostRequest(com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	protected <P> void sendRESTfulPostRequest(ResponseCallback<P> callback) throws EndpointException {
		HttpResponse response = null;
		try {
			response = this.sendPostRequest();
		} catch (HttpException e) {
			throw new EndpointException(e.getMessage(), e);
		}
		
		this.parseResponse(response, callback);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.RESTfulEndpoint#sendRESTfulPutRequest(com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	protected <P> void sendRESTfulPutRequest(ResponseCallback<P> callback) throws EndpointException {
		HttpResponse response = null;
		try {
			response = this.sendPutRequest();
		} catch (HttpException e) {
			throw new EndpointException(e.getMessage(), e);
		}
		
		this.parseResponse(response, callback);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.RESTfulEndpoint#sendRESTfulDeleteRequest(com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	protected <P> void sendRESTfulDeleteRequest(ResponseCallback<P> callback) throws EndpointException {
		HttpResponse response = null;
		try {
			response = this.sendDeleteReqeust();
		} catch (HttpException e) {
			throw new EndpointException(e.getMessage(), e);
		}
		
		this.parseResponse(response, callback);
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.server.endpoint.restful.RESTfulEndpoint#sendRESTfulGetRequest()
	 */
	@Override
	protected <P> P sendRESTfulGetRequest() throws EndpointException {
		HttpResponse response = null;
		try {
			response = this.sendGetRequest();
			return this.parseResponse(response);
		} catch (Throwable e) {
			throw new EndpointException(e.getMessage(), e);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.server.endpoint.restful.RESTfulEndpoint#sendRESTfulPostRequest()
	 */
	@Override
	protected <P> P sendRESTfulPostRequest() throws EndpointException {
		HttpResponse response = null;
		try {
			response = this.sendPostRequest();
			return this.parseResponse(response);
		} catch (Throwable e) {
			throw new EndpointException(e.getMessage(), e);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.server.endpoint.restful.RESTfulEndpoint#sendRESTfulPutRequest()
	 */
	@Override
	protected <P> P sendRESTfulPutRequest() throws EndpointException {
		HttpResponse response = null;
		try {
			response = this.sendPutRequest();
			return this.parseResponse(response);
		} catch (Throwable e) {
			throw new EndpointException(e.getMessage(), e);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.server.endpoint.restful.RESTfulEndpoint#sendRESTfulDeleteRequest()
	 */
	@Override
	protected <P> P sendRESTfulDeleteRequest() throws EndpointException {
		HttpResponse response = null;
		try {
			response = this.sendDeleteReqeust();
			return this.parseResponse(response);
		} catch (Throwable e) {
			throw new EndpointException(e.getMessage(), e);
		}
	}

	/**
	 * Returns the success code of RESTful response defined by remote service API.
	 * 
	 * @return the success code of RESTful response defined by remote service API.
	 */
	protected abstract RESTfulResponseStatusCodes getSuccessCode();
	
	/**
	 * To setup gson object by a particular type token.<br>
	 * Only available for the polymorphism used by class, not interface.<br>
	 * Example:<br>
	 * final TypeToken<List<Request>> requestListTypeToken = new TypeToken<List<Request>>() {
	 * };<br>
	 * <br>
	 * final RuntimeTypeAdapterFactory<Request> typeFactory = RuntimeTypeAdapterFactory.of(Request.class, "type")
	 * 																					.registerSubtype(LoginRequest.class)
	 * 																					.registerSubtype(PingRequest.class);<br>
	 * <br>
	 * final Gson gson = new GsonBuilder().registerTypeAdapterFactory(typeFactory).create();
	 * <br>
	 * The <b>"type"</b> is a data member of a class which indicates what sort of sub-class used to be converted from a json string.<br>
	 */
	protected abstract void initGsonAndTypeToken();
	
	protected void setGsonObject(Gson gson) {
		this.gson = gson;
	}
	
	protected void setResponseObjectType(Type type) {
		this.responseObjType = type;
	}
	
	protected Type getResponseObjectType() {
		return this.responseObjType;
	}

	private void initHeader() {
		this.addHeader(ContentType.JSON);
		this.addHeader(Accept.JSON);
	}
	
	/**
	 * To parse {@link HttpResponse} and return it via {@link ResponseCallback} to client call.<br>
	 * 
	 * @param response
	 * @param callback
	 */
	private <P> void parseResponse(HttpResponse response, ResponseCallback<P> callback) {
		try {
			P result = this.parseResponse(response);
			callback.onSuccess(result);
		} catch (Throwable e) {			
			callback.onFailure(e);
		}
	}
	
	/**
	 * To parse {@link HttpResponse} and return it via {@link ResponseCallback} to client call directly.<br>
	 *  
	 * @param response
	 * @return
	 */
	private <P> P parseResponse(HttpResponse response) throws Throwable{
		RESTfulResponseStatusCodes statusCode = RESTfulResponseStatusCodes.fromCode(response.getStatusCode());
		
		if(null == statusCode) {
			String msg = StringFormatter.format("Unknown HTTP response status code[%d].", statusCode);
			throw new UnknownResponseStatusCodeException(msg);	
		}else if( this.getSuccessCode() == statusCode) {
			Gson jsonParser = (null != this.gson) ? this.gson : new Gson();
			P result = null;
			
			//No payload for this response.
			String content = response.getMediaContent();
			if(null == content || content.isEmpty()) {
				return null;
			}
			
			result = jsonParser.fromJson(content, this.getResponseObjectType());
			return result;
		}else {
			Gson gson = new Gson();
			ErrorResponse error = gson.fromJson(response.getMediaContent(), ErrorResponse.class);
			String msg = null;
			if(error.validate()) {
				msg = error.getErrorMesssage().getMessage();
			}
			
			Throwable exception = RESTfulResponseExceptions.getInstance().getException(statusCode, msg);
			throw exception;
		}		
	}
}
