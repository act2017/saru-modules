/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost;

import java.util.HashMap;
import java.util.Map;

import com.digsarustudio.banana.utils.StringFormatter;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.exception.builder.NotFoundExceptionBuilder;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.exception.builder.UnknownError403ExceptionBuilder;

/**
 * This singleton HTTP response status codes and exceptions manager provides the map of status codes and exceptions.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class RESTfulResponseExceptions {
	private static RESTfulResponseExceptions INSTANCE 	= null;
	
	private Map<Integer, RESTfulResponseExceptionBuilder<?>>		exceptions	= null;
	
	/**
	 * 
	 */
	private RESTfulResponseExceptions() {
	}

	public static RESTfulResponseExceptions getInstance() {
		if(null == INSTANCE) {
			INSTANCE = new RESTfulResponseExceptions();
			INSTANCE.buildReplationship();			
		}
		
		return INSTANCE;
	}
	
	public Throwable getException(RESTfulResponseStatusCodes code, String errorMessage) {
		return this.getException(code.getCode(), errorMessage);
	}
	
	public Throwable getException(Integer code) {
		return this.getException(code, null);
	}
	
	public Throwable getException(Integer code, String errorMsg) {
		if(!this.exceptions.containsKey(code)) {
			return null;
		}
		
		RESTfulResponseExceptionBuilder<?> builder = this.exceptions.get(code);
		
		RESTfulResponseStatusCodes statusCode = RESTfulResponseStatusCodes.fromCode(code);
		String msg = null;
		if(null != statusCode) {
			if( null != statusCode.getDescription() ) {
				msg = StringFormatter.format("%s: %s", statusCode.getName(), statusCode.getDescription());
			}else {
				msg = errorMsg;
			}
		}else {
			msg = StringFormatter.format("Unknown status code (%d)", code);
		}		
		
		return builder.setMessage(msg)
					  .build();
	}
	
	private void buildReplationship() {
		this.exceptions = new HashMap<Integer, RESTfulResponseExceptionBuilder<?>>();
		
		this.exceptions.put(RESTfulResponseStatusCodes.NotFound.getCode(), new NotFoundExceptionBuilder());
		this.exceptions.put(RESTfulResponseStatusCodes.UnknownError403.getCode(), new UnknownError403ExceptionBuilder());
	}
}
