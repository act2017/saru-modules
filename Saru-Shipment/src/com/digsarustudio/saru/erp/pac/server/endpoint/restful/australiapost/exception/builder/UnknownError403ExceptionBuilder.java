/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.exception.builder;

import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.exception.UnknownError403Exception;

/**
 * The {@link UnknownError403Exception} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class UnknownError403ExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<UnknownError403Exception> 
											 implements RESTfulResponseExceptionBuilder<UnknownError403Exception> {
		
	/**
	 * 
	 */
	public UnknownError403ExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public UnknownError403Exception build() {
		return new UnknownError403Exception(this.getMessage(), this.getCause());
	}

}
