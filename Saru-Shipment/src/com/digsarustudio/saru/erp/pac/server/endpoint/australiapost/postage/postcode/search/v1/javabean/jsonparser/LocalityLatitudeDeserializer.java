/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.australiapost.postage.postcode.search.v1.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityLatitude;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

/**
 * The deserializer for Gson to deserialize {@link LocalityLatitude} from a JSON string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LocalityLatitudeDeserializer implements JsonDeserializer<LocalityLatitude> {

	/**
	 * 
	 */
	public LocalityLatitudeDeserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public LocalityLatitude deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		String latitude = json.getAsString();
		
		return LocalityLatitude.builder().setValue(latitude)
										 .build();
	}
}
