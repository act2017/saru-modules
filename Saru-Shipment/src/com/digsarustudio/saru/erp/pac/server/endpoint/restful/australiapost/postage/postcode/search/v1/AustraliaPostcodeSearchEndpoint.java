/**
 * 
 */
package com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.server.contactInfo.address.javabean.jsonparser.AustralianPostcodeBiserializer;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.AustralianPostcode;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.AuthenticationDetails;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.ResponseCallback;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.QueryParameter;
import com.digsarustudio.saru.erp.pac.server.endpoint.australiapost.jsonparser.AustralianStateBiserializer;
import com.digsarustudio.saru.erp.pac.server.endpoint.australiapost.postage.postcode.search.v1.javabean.jsonparser.LocalityCategoryDeserializer;
import com.digsarustudio.saru.erp.pac.server.endpoint.australiapost.postage.postcode.search.v1.javabean.jsonparser.LocalityCollectionDeserializer;
import com.digsarustudio.saru.erp.pac.server.endpoint.australiapost.postage.postcode.search.v1.javabean.jsonparser.LocalityDeserializer;
import com.digsarustudio.saru.erp.pac.server.endpoint.australiapost.postage.postcode.search.v1.javabean.jsonparser.LocalityIdDeserializer;
import com.digsarustudio.saru.erp.pac.server.endpoint.australiapost.postage.postcode.search.v1.javabean.jsonparser.LocalityLatitudeDeserializer;
import com.digsarustudio.saru.erp.pac.server.endpoint.australiapost.postage.postcode.search.v1.javabean.jsonparser.LocalityLocationDeserializer;
import com.digsarustudio.saru.erp.pac.server.endpoint.australiapost.postage.postcode.search.v1.javabean.jsonparser.LocalityLongitudeDeserializer;
import com.digsarustudio.saru.erp.pac.server.endpoint.australiapost.postage.postcode.search.v1.javabean.jsonparser.LocalityResponseDeserializer;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint;
import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.RESTfulResponseStatusCodes;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.AustralianState;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.Locality;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityCategory;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityCollection;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityId;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityLatitude;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityLocation;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityLongitude;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Search the collection of domestic Postcode records for potential matches.<br>
 * The search criterion is location name or postcode with an optional state filter.<br>
 * <h3>Resource Information</h3>
 * <table>
 * 	<tr>
 * 		<td>Rate Limited?</td><td>No</td><td>HTTP Methods</td><td>GET</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Requests per rate limit window</td><td>-</td><td>Resource family</td><td>Data</td>
 * 	<tr>
 * 		<td>Authentication</td><td>API key</td><td>Response object</td><td>Postage</td>
 * 	<tr>
 * 		<td>Response Formats</td><td>json, xml</td><td>API Version</td><td>v1.0</td>
 * 	</tr>
 * </table>
 * <br>
 * <h3>Resource URL</h3>
 * https://digitalapi.auspost.com.au/postcode/search
 * <h3>Headers</h3>
 * auth-key(required)	Your PAC API Key (HTTP Header)<br>
 * <br>
 * <h3>Parameters</h3>
 * from_postcode(required)	The postcode the parcel will be sent from.<br>
 * to_postcode(required)	The postcode the parcel will be sent to.<br>
 * length(required)			The parcel length in CMs.<br>
 * width(required)			The parcel width in CMs.<br>
 * height(required)			The parcel height in CMs<br>
 * weight(required)			The parcel weight in KGs<br>
 * service_code(required)	The chosen product / service code
 * option_code(optional)	The chosen option code for the product option
 * suboption_code(optional)	The chosen suboption code for the product suboption
 * extra_cover(optional)	The dollar amount of the extra cover required
 * <br>
 * <h3>What can be the query criteria</h3>
 * <ul>
 * 	<li>postcode (recommended)</li>
 * 	<li>name of suburb (recommended)</li>
 * 	<li>name of city</li>
 * 	<li>name of town</li>
 * </ul>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/postage-parcel-domestic-calculate">postage/parcel/domestic/calculator</a>
 *
 */
public class AustraliaPostcodeSearchEndpoint extends AustraliaPostEndpoint<Locality, Locality> {		
	private static final String API_PATH				= "postcode/search.json";
	private static final String	PARAM_Q					= "q";
	private static final String	PARAM_STATE				= "state";
	private static final String	PARAM_EXCLUDE_POBOX		= "excludepostboxflag";
	
	
	/**
	 * 
	 * The object builder for {@link AustraliaPostcodeSearchEndpoint}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<AustraliaPostcodeSearchEndpoint> {
		private AustraliaPostcodeSearchEndpoint result = null;

		private Builder() {
			this.result = new AustraliaPostcodeSearchEndpoint();
			this.result.setURL(API_PATH);
		}

		/**
		 * @param details
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint#setAuthKey(com.digsarustudio.banana.endpoint.AuthenticationDetails)
		 */
		public Builder setAuthKey(AuthenticationDetails details) {
			result.setAuthKey(details);
			return this;
		}

		/**
		 * @param postcode
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#searchByPostcode(java.lang.String)
		 */
		public Builder searchByPostcode(String postcode) {
			result.searchByPostcode(postcode);
			return this;
		}

		/**
		 * @param suburb
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#searchBySuburb(java.lang.String)
		 */
		public Builder searchBySuburb(String suburb) {
			result.searchBySuburb(suburb);
			return this;
		}

		/**
		 * @param city
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#searchByCity(java.lang.String)
		 */
		public Builder searchByCity(String city) {
			result.searchByCity(city);
			return this;
		}

		/**
		 * @param state
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#setState(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State)
		 */
		public Builder setState(State state) {
			result.setState(state);
			return this;
		}

		/**
		 * 
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#setExcludePOBox()
		 */
		public Builder setExcludePOBox() {
			result.setExcludePOBox();
			return this;
		}

		/**
		 * 
		 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.postcode.search.v1.AustraliaPostcodeSearchEndpoint#setIncludePOBox()
		 */
		public Builder setIncludePOBox() {
			result.setIncludePOBox();
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AustraliaPostcodeSearchEndpoint build() {
			return this.result;
		}

	}
	
	private AustraliaPostcodeSearchEndpoint() {
		
	}
	
	public void searchByPostcode(String postcode) {
		QueryParameter search = this.getQueryParameter(PARAM_Q);
		
		if(null == search) {
			this.addQueryParameter(QueryParameter.builder().setKey(PARAM_Q)
														   .setValue(postcode)
														   .build());
		}
	}
	
	public void searchBySuburb(String suburb) {
		QueryParameter search = this.getQueryParameter(PARAM_Q);
		
		if(null == search) {
			this.addQueryParameter(QueryParameter.builder().setKey(PARAM_Q)
														   .setValue(suburb)
														   .build());
		}else {
			search.setValue(suburb);
		}
	}
	
	public void searchByCity(String city) {
		QueryParameter search = this.getQueryParameter(PARAM_Q);
		
		if(null == search) {
			this.addQueryParameter(QueryParameter.builder().setKey(PARAM_Q)
														   .setValue(city)
														   .build());
		}else {
			search.setValue(city);
		}
	}
	
	public void setState(State state) {
		this.addQueryParameter(QueryParameter.builder().setKey(PARAM_STATE)
													   .setValue(state.getCode())
													   .build());
	}
	
	public void setExcludePOBox() {
		this.addQueryParameter(QueryParameter.builder().setKey(PARAM_EXCLUDE_POBOX)
													   .setValue("true")
													   .build());
	}
	
	public void setIncludePOBox() {
		this.addQueryParameter(QueryParameter.builder().setKey(PARAM_EXCLUDE_POBOX)
				   									   .setValue("false")
				   									   .build());		
	}
	
	public void list(ResponseCallback<CollectionResponse<Locality>> callback) throws EndpointException {
		this.sendRESTfulGetRequest(new ResponseCallback<LocalityResponse>() {

			@Override
			public void onSuccess(LocalityResponse result) {
				CollectionResponse.Builder<Locality> builder = com.digsarustudio.saru.erp.musaceae.shared.endpoint.javabean.CollectionResponse.<Locality>builder();
				List<Locality> localities = result.getLocalities();
				builder.setItems(localities);
				
				callback.onSuccess(builder.build());				
			}

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
		});
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#insert(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void insert(Locality data, ResponseCallback<Locality> callback) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support inert method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#update(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void update(Locality data, ResponseCallback<Locality> callback) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support update method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#update(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)
	 */
	@Override
	public void update(Locality data, ResponseCallback<Locality> callback, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support update method.");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#list(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void list(Locality data, ResponseCallback<CollectionResponse<Locality>> callback) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support conditioned list method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#list(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void list(Locality data, ResponseCallback<CollectionResponse<Locality>> callback, Integer cursor, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support conditioned list method with cursor.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#delete(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void delete(Locality data, ResponseCallback<Locality> callback) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support delete method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#delete(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)
	 */
	@Override
	public void delete(Locality data, ResponseCallback<Locality> callback, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support delete method with maximum quantity.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#get(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void get(Locality data, ResponseCallback<Locality> callback) throws EndpointException {
		throw new UnsupportedOperationException("This API doesn't support get method.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint#getSuccessCode()
	 */
	@Override
	protected RESTfulResponseStatusCodes getSuccessCode() {
		return RESTfulResponseStatusCodes.OK;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.AustraliaPostEndpoint#initGsonAndTypeToken()
	 */
	@Override
	protected void initGsonAndTypeToken() {
		// Due to the format of json string which contains mixed data type, so that providing a deserializer to handle it.
		GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(LocalityResponse.class, new LocalityResponseDeserializer());
		builder.registerTypeAdapter(LocalityCollection.class, new LocalityCollectionDeserializer());
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.Locality.class, new LocalityDeserializer());
		builder.registerTypeAdapter(LocalityCategory.class, new LocalityCategoryDeserializer());
		builder.registerTypeAdapter(LocalityId.class, new LocalityIdDeserializer());
		builder.registerTypeAdapter(LocalityLatitude.class, new LocalityLatitudeDeserializer());
		builder.registerTypeAdapter(LocalityLocation.class, new LocalityLocationDeserializer());
		builder.registerTypeAdapter(LocalityLongitude.class, new LocalityLongitudeDeserializer());
		builder.registerTypeAdapter(AustralianPostcode.class, new AustralianPostcodeBiserializer());
		builder.registerTypeAdapter(AustralianState.class, new AustralianStateBiserializer());
		
		Gson gson = builder.create();
		this.setGsonObject(gson);
		this.setResponseObjectType(LocalityResponse.class);
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
