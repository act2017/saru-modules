/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean;

import com.digsarustudio.banana.utils.ObjectIdentifier;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The id object for {@link Locality}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LocalityId implements ObjectIdentifier, IsSerializable {
	/**
	 * 
	 * The object builder for {@link LocalityId}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectIdentifier.Builder {
		private LocalityId result = null;

		private Builder() {
			this.result = new LocalityId();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectIdentifier.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public LocalityId build() {
			return this.result;
		}

	}
	
	private String value = null;

	/**
	 * 
	 */
	private LocalityId() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectIdentifier#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.value = value;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectIdentifier#getValue()
	 */
	@Override
	public String getValue() {
		
		return this.value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectIdentifier#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.value || this.value.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
