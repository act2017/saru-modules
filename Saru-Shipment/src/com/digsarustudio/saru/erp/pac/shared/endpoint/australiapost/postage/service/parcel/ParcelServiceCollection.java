/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The collection of available services and additional options for a domestic/international parcels or letters.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/postage-parcel-domestic-service">postage/parcel/domestic/service</a>
 *
 */
public interface ParcelServiceCollection {
	/**
	 * 
	 * The builder for {@link ParcelServiceCollection}<br>
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ParcelServiceCollection> {
		Builder setParcelServices(List<ParcelService> services);
		Builder setParcelServices(ParcelService[] services);
	}
	
	void setParcelServices(List<ParcelService> services);
	void setParcelServices(ParcelService[] services);
	
	ParcelService getParcelService(Integer index);
	List<ParcelService> getParcelServices();
	
	Boolean validate();
}
