/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The collection of {@link Locality}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface LocalityCollection {
	/**
	 * 
	 * The builder for {@link LocalityCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<LocalityCollection> {
		Builder addLocality(Locality locality);
		Builder setLocalities(List<Locality> localities);
		Builder setLocalities(Locality[] localities);
	}
	
	void addLocality(Locality locality);
	void setLocalities(List<Locality> localities);
	void setLocalities(Locality[] localities);
	
	Locality getLocality(Integer index);
	List<Locality> getLocalities();
	
	Boolean validate();
}
