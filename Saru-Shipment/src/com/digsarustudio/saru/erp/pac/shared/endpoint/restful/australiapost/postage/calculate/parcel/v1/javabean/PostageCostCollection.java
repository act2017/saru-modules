/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCostCollection}.<br>
 * To convert this by Gson, please call {@link PostCostCollectionDeserializer}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PostageCostCollection implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCostCollection
											, IsSerializable {
	public static final String	ATTR_COST	= "cost";
	
	/**
	 * 
	 * The object builder for {@link PostageCostCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCostCollection.Builder {
		private PostageCostCollection result = null;

		private Builder() {
			this.result = new PostageCostCollection();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCostCollection.Builder#addPostageCost(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost)
		 */
		@Override
		public Builder addPostageCost(PostageCost cost) {
			this.result.addPostageCost(cost);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCostCollection.Builder#setPostageCosts(java.util.List)
		 */
		@Override
		public Builder setPostageCosts(List<PostageCost> costs) {
			this.result.setPostageCosts(costs);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCostCollection.Builder#setPostageCosts(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost[])
		 */
		@Override
		public Builder setPostageCosts(PostageCost[] costs) {
			this.result.setPostageCosts(costs);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public PostageCostCollection build() {
			return this.result;
		}

	}
	
	private List<PostageCost>	cost		= null;
		
	/**
	 * 
	 */
	private PostageCostCollection() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCostCollection#addPostageCost(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost)
	 */
	@Override
	public void addPostageCost(PostageCost cost) {
		if(null == this.cost) {
			this.cost = new ArrayList<PostageCost>();
		}

		this.cost.add(cost);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCostCollection#setPostageCosts(java.util.List)
	 */
	@Override
	public void setPostageCosts(List<PostageCost> costs) {
		this.cost = costs;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCostCollection#setPostageCosts(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost[])
	 */
	@Override
	public void setPostageCosts(PostageCost[] costs) {
		this.cost = new ArrayList<>(Arrays.asList(costs));

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCostCollection#getPostageCost(java.lang.Integer)
	 */
	@Override
	public PostageCost getPostageCost(Integer index) {
		if(null == this.cost || this.cost.size() <= index) {
			return null;
		}
		
		return this.cost.get(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCostCollection#getPostageCost()
	 */
	@Override
	public List<PostageCost> getPostageCost() {
		
		return this.cost;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCostCollection#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.cost) {
			return false;
		}
		
		for (PostageCost postageCost : this.cost) {
			if( !postageCost.validate() ) {
				return false;
			}				
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PostageCostCollection [cost=" + cost + "]";
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
