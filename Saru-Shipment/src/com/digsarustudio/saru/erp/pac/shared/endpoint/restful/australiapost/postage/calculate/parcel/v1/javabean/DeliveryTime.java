/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.DeliveryTime}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DeliveryTime implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.DeliveryTime
								   , IsSerializable {
	
	/**
	 * 
	 * The object builder for {@link DeliveryTime}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.DeliveryTime.Builder {
		private DeliveryTime result = null;

		private Builder() {
			this.result = new DeliveryTime();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.DeliveryTime.Builder#setDescription(java.lang.String)
		 */
		@Override
		public Builder setDescription(String desc) {
			this.result.setDescription(desc);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DeliveryTime build() {
			return this.result;
		}

	}
	
	private String	description	= null;

	/**
	 * 
	 */
	private DeliveryTime() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.DeliveryTime#setDescription(java.lang.String)
	 */
	@Override
	public void setDescription(String desc) {
		this.description = desc;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.DeliveryTime#getDescription()
	 */
	@Override
	public String getDescription() {
		
		return this.description;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.DeliveryTime#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.description || this.description.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DeliveryTime [description=" + description + "]";
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
