/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean;

import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResultResponse}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PostageResultResponse implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResultResponse
									  , IsSerializable {
	public static final String	ATTR_POSTAGE_RESULT	= "postage_result";

	/**
	 * 
	 * The object builder for {@link PostageResultResponse}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResultResponse.Builder {
		private PostageResultResponse result = null;

		private Builder() {
			this.result = new PostageResultResponse();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResponse.Builder#setPostageResult(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult)
		 */
		@Override
		public Builder setPostageResult(PostageResult result) {
			this.result.setPostageResult(result);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public PostageResultResponse build() {
			return this.result;
		}

	}
	
	private PostageResult	postage_result	= null;
	
	/**
	 * 
	 */
	private PostageResultResponse() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResponse#setPostageResult(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult)
	 */
	@Override
	public void setPostageResult(PostageResult result) {
		this.postage_result = result;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResponse#getPostageResult()
	 */
	@Override
	public PostageResult getPostageResult() {

		return this.postage_result;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResponse#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.postage_result) {
			return false;
		}
		
		if( !this.postage_result.validate() ) {
			return false;
		}

		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PostageResultResponse [postage_result=" + postage_result + "]";
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
