/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Latitude}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LocalityLatitude implements com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Latitude, IsSerializable {
	/**
	 * 
	 * The object builder for {@link LocalityLatitude}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Latitude.Builder {
		private LocalityLatitude result = null;

		private Builder() {
			this.result = new LocalityLatitude();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Latitude.Builder#setValue(java.lang.Float)
		 */
		@Override
		public Builder setValue(Float value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Latitude.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public LocalityLatitude build() {
			return this.result;
		}

	}
	
	private String value	= null;

	/**
	 * 
	 */
	private LocalityLatitude() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Latitude#setValue(java.lang.Float)
	 */
	@Override
	public void setValue(Float value) {
		this.value = value.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Latitude#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.value = value;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Latitude#getValue()
	 */
	@Override
	public String getValue() {
		
		return this.value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Latitude#getValueFloat()
	 */
	@Override
	public Float getValueFloat() {
		
		return Float.parseFloat(this.value);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Latitude#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.value || this.value.isEmpty()) {
			return false;
		}

		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
