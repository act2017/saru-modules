/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean;

import java.util.List;

import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The implementation of {@link com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService}<br>.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/postage-parcel-domestic-service">postage/parcel/domestic/service</a>
 *
 */
public class ParcelService implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService
									, IsSerializable {
	public static final String ATTR_CODE			= "code";
	public static final String ATTR_NAME			= "name";
	public static final String ATTR_PRICE			= "price";
	public static final String ATTR_MAX_EXTRA_COVER	= "max_extra_cover";
	public static final String ATTR_OPTIONS			= "options";
	
	/**
	 * 
	 * The object builder for {@link ParcelService}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService.Builder {
		private ParcelService result = null;

		private Builder() {
			this.result = new ParcelService();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService.Builder#setCode(java.lang.String)
		 */
		@Override
		public Builder setCode(String code) {
			this.result.setCode(code);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService.Builder#setName(java.lang.String)
		 */
		@Override
		public Builder setName(String name) {
			this.result.setName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService.Builder#setPrice(java.lang.String)
		 */
		@Override
		public Builder setPrice(String price) {
			this.result.setPrice(price);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService.Builder#setMaxExtraCover(java.lang.Integer)
		 */
		@Override
		public Builder setMaxExtraCover(Integer extraCover) {
			this.result.setMaxExtraCover(extraCover);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService.Builder#setServiceOptionCollection(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection)
		 */
		@Override
		public Builder setServiceOptionCollection(ParcelServiceOptionCollection collection) {
			this.result.setServiceOptionCollection(collection);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService.Builder#addServiceOption(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption)
		 */
		@Override
		public Builder addServiceOption(ParcelServiceOption option) {
			this.result.addServiceOption(option);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ParcelService build() {
			return this.result;
		}

	}	
	
	private String							code			= null;
	private String							name			= null;
	private String							price			= null;
	private Integer							max_extra_cover	= null;
	private ParcelServiceOptionCollection	options			= null;
	
	/**
	 * 
	 */
	private ParcelService() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.serivce.parcel.ParcelService#getCode()
	 */
	@Override
	public String getCode() {
		
		return this.code;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.serivce.parcel.ParcelService#getName()
	 */
	@Override
	public String getName() {
		
		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.serivce.parcel.ParcelService#getPrice()
	 */
	@Override
	public String getPrice() {
		
		return this.price;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.serivce.parcel.ParcelService#getMaxExtraCover()
	 */
	@Override
	public Integer getMaxExtraCover() {
		
		return this.max_extra_cover;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.serivce.parcel.ParcelService#getServiceOptionCollection()
	 */
	@Override
	public ParcelServiceOptionCollection getServiceOptionCollection() {
		
		return this.options;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.code = code;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService#setPrice(java.lang.String)
	 */
	@Override
	public void setPrice(String price) {
		this.price = price;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService#setMaxExtraCover(java.lang.Integer)
	 */
	@Override
	public void setMaxExtraCover(Integer extraCover) {
		this.max_extra_cover = extraCover;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService#setServiceOptionCollection(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection)
	 */
	@Override
	public void setServiceOptionCollection(ParcelServiceOptionCollection collection) {
		this.options = collection;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService#addServiceOption(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption)
	 */
	@Override
	public void addServiceOption(ParcelServiceOption option) {
		if(null == this.options) {
			this.options = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelServiceOptionCollection.builder().build();
		}
		
		this.options.addOptionService(option);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService#hasServiceOption()
	 */
	@Override
	public Boolean hasServiceOption() {
		if(null == this.options) {
			return false;
		}
		
		List<ParcelServiceOption> options = this.options.getOptionServices();
		if(null == options) {
			return false;
		}
		
		if(options.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.serivce.parcel.ParcelService#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.code || this.code.isEmpty()) {
			return false;
		}
		
		if(null == this.name || this.name.isEmpty()) {
			return false;
		}
		
		if(null == this.price || this.price.isEmpty()) {
			return false;
		}
		
		if(null != this.options && !this.options.validate()) {
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParcelService other = (ParcelService) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ParcelService [code=" + code + ", name=" + name + ", price=" + price + ", max_extra_cover="
				+ max_extra_cover + ", options=" + options + "]";
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
