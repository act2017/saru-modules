/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.v1.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The implementation of {@link com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.ErrorMessage}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ErrorMessage implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.ErrorMessage
									, IsSerializable {
	private String errorMessage	= null;
	
	/**
	 * 
	 */
	private ErrorMessage() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.ErrorMessage#getMessage()
	 */
	@Override
	public String getMessage() {
		
		return this.errorMessage;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.ErrorMessage#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.errorMessage || this.errorMessage.isEmpty()) {
			return false;
		}
		
		return true;
	}

}
