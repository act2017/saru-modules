/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean;

import java.util.List;

import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceCollection;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The implementation of {@link com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceResponse}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/postage-parcel-domestic-service">postage/parcel/domestic/service</a>
 *
 */
public class ParcelServiceResponse implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceResponse
											, IsSerializable {
	public static final String ATTR_SERVICES	= "services";
	
	/**
	 * 
	 * The object builder for {@link ParcelServiceResponse}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceResponse.Builder {
		private ParcelServiceResponse result = null;

		private Builder() {
			this.result = new ParcelServiceResponse();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceResponse.Builder#setParcelServices(java.util.List)
		 */
		@Override
		public Builder setParcelServices(List<ParcelService> services) {
			this.result.setParcelServices(services);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceResponse.Builder#setParcelServiceCollection(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceCollection)
		 */
		@Override
		public Builder setParcelServiceCollection(ParcelServiceCollection serviceCollection) {
			this.result.setParcelServiceCollection(serviceCollection);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ParcelServiceResponse build() {
			return this.result;
		}

	}
	
	private ParcelServiceCollection	services	= null;

	/**
	 * 
	 */
	private ParcelServiceResponse() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceResponse#setParcelServices(java.util.List)
	 */
	@Override
	public void setParcelServices(List<ParcelService> services) {
		if(null == this.services) {
			this.services = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelServiceCollection.builder().setParcelServices(services)
																																									 .build();
			return;
		}
		
		this.services.setParcelServices(services);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceResponse#setParcelServiceCollection(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceCollection)
	 */
	@Override
	public void setParcelServiceCollection(ParcelServiceCollection serviceCollection) {
		this.services = serviceCollection;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.serivce.parcel.ParcelServiceResponse#getParcelServiceCollection()
	 */
	@Override
	public ParcelServiceCollection getParcelServiceCollection() {
		
		return this.services;
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceResponse#getParcelServices()
	 */
	@Override
	public List<ParcelService> getParcelServices() {
		if(null == this.services) {
			return null;
		}

		return this.services.getParcelServices();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.serivce.parcel.ParcelServiceResponse#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.services) {
			return false;
		}
		
		return this.services.validate();
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		
		
		return "ParcelServiceResponse [services=" + services + "]";
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
