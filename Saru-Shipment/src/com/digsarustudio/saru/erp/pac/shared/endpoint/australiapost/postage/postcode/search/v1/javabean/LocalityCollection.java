/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.Locality;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.LocalityCollection}<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LocalityCollection implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.LocalityCollection,
										   IsSerializable {
	public static final String ATTR_LOCALITY	= "locality";
	
	/**
	 * 
	 * The object builder for {@link LocalityCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.LocalityCollection.Builder {
		private LocalityCollection result = null;

		private Builder() {
			this.result = new LocalityCollection();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityCollection.Builder#addLocality(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality)
		 */
		@Override
		public Builder addLocality(Locality locality) {
			this.result.addLocality(locality);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityCollection.Builder#setLocalities(java.util.List)
		 */
		@Override
		public Builder setLocalities(List<Locality> localities) {
			this.result.setLocalities(localities);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityCollection.Builder#setLocalities(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality[])
		 */
		@Override
		public Builder setLocalities(Locality[] localities) {
			this.result.setLocalities(localities);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public LocalityCollection build() {
			return this.result;
		}

	}
	
	
	private List<Locality> locality	= null;

	/**
	 * 
	 */
	private LocalityCollection() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityCollection#addLocality(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality)
	 */
	@Override
	public void addLocality(Locality locality) {
		if(null == this.locality) {
			this.locality = new ArrayList<>();
		}

		this.locality.add(locality);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityCollection#setLocalities(java.util.List)
	 */
	@Override
	public void setLocalities(List<Locality> localities) {
		this.locality = localities;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityCollection#setLocalities(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality[])
	 */
	@Override
	public void setLocalities(Locality[] localities) {
		this.setLocalities(Arrays.asList(localities));
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityCollection#getLocality(java.lang.Integer)
	 */
	@Override
	public Locality getLocality(Integer index) {
		if(null == this.locality) {
			return null;					
		}
		
		if(index >= this.locality.size()) {
			return null;
		}
		
		return this.locality.get(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityCollection#getLocalities()
	 */
	@Override
	public List<Locality> getLocalities() {
		
		return this.locality;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityCollection#validate()
	 */
	@Override
	public Boolean validate() {
		if( null == this.locality ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
