/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.javabean;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.shared.Weight;
import com.digsarustudio.saru.erp.musaceae.shared.WeightUnit;
import com.digsarustudio.saru.erp.musaceae.shared.javabean.BaseWeight;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The weight used by Australia Post.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class FreightWeight extends BaseWeight implements Weight, IsSerializable {

	/**
	 * 
	 * The object builder for {@link FreightWeight}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<FreightWeight> {
		private FreightWeight result = null;

		private Builder() {
			this.result = new FreightWeight();
		}

		/**
		 * @param value
		 * @see com.digsarustudio.saru.erp.musaceae.shared.javabean.BaseWeight#setValue(java.lang.String)
		 */
		public Builder setValue(String value) {
			result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public FreightWeight build() {
			return this.result;
		}
	}
	
	/**
	 * 
	 */
	private FreightWeight() {
		this.setUnit(WeightUnit.Kilogram);
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
