/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The collection contains a list of countries.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			{@link Country}
 */
public interface CountryCollection {
	/**
	 * 
	 * The builder for {@link CountryCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<CountryCollection> {
		Builder addCountry(Country country);
		Builder setCountries(List<Country> countries);
		Builder setCountries(Country[] countries);
	}
	
	Country getCountry(String code);
	List<Country> getCountries();
	
	Boolean validate();
}
