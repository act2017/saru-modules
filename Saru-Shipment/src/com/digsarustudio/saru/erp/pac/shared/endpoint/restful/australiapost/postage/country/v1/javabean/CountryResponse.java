/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.country.v1.javabean;

import java.util.List;

import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country.Country;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The implementation of {@link CountryResponse}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class CountryResponse implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country.CountryResponse
									  , IsSerializable {

	private CountryCollection countries	= null;
	
	/**
	 * 
	 */
	private CountryResponse() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.CountryResponse#getCountryCollection()
	 */
	@Override
	public CountryCollection getCountryCollection() {

		return this.countries;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.CountryResponse#getCountry(java.lang.String)
	 */
	@Override
	public Country getCountry(String code) {
		if(null == this.countries) {
			return null;
		}
		
		return this.countries.getCountry(code);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.CountryResponse#getCountries()
	 */
	@Override
	public List<Country> getCountries() {
		if(null == this.countries) {
			return null;
		}
		
		return this.countries.getCountries();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.CountryResponse#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.countries) {
			return false;
		}
		
		return this.countries.validate();
	}

}
