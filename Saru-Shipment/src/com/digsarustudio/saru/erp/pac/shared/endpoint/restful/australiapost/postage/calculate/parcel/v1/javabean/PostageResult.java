/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean;

import java.util.List;

import com.digsarustudio.saru.erp.musaceae.shared.UnitPrice;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.DeliveryTime;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCostCollection;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PostageResult implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult
									, IsSerializable {
	public final static String	ATTR_SERVICE		= "service";
	public final static String	ATTR_DELIVERY_TIME	= "delivery_time";
	public final static String	ATTR_TOTAL_COST		= "total_cost";
	public final static String	ATTR_COSTS			= "costs";
	
	/**
	 * 
	 * The object builder for {@link PostageResult}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult.Builder {
		private PostageResult result = null;

		private Builder() {
			this.result = new PostageResult();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult.Builder#setService(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService)
		 */
		@Override
		public Builder setService(ParcelService service) {
			this.result.setService(service);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult.Builder#setDeliveryTime(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.DeliveryTime)
		 */
		@Override
		public Builder setDeliveryTime(DeliveryTime deliveryTime) {
			this.result.setDeliveryTime(deliveryTime);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult.Builder#setTotalCost(com.digsarustudio.banana.erp.general.UnitPrice)
		 */
		@Override
		public Builder setTotalCost(UnitPrice cost) {
			this.result.setTotalCost(cost);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult.Builder#addPostageCost(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost)
		 */
		@Override
		public Builder addPostageCost(PostageCost cost) {
			this.result.addPostageCost(cost);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult.Builder#setPostageCosts(java.util.List)
		 */
		@Override
		public Builder setPostageCosts(List<PostageCost> costs) {
			this.result.setPostageCosts(costs);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult.Builder#setPostageCosts(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost[])
		 */
		@Override
		public Builder setPostageCosts(PostageCost[] costs) {
			this.result.setPostageCosts(costs);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult.Builder#setPostageCostCollection(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCostCollection)
		 */
		@Override
		public Builder setPostageCostCollection(PostageCostCollection collection) {
			this.result.setPostageCostCollection(collection);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public PostageResult build() {
			return this.result;
		}

	}

	private ParcelService			service			= null;
	private DeliveryTime			delivery_time	= null;
	private UnitPrice				total_cost		= null;
	private PostageCostCollection	costs			= null;	
	
	/**
	 * 
	 */
	private PostageResult() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult#setService(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService)
	 */
	@Override
	public void setService(ParcelService service) {
		this.service = service;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult#setDeliveryTime(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.DeliveryTime)
	 */
	@Override
	public void setDeliveryTime(DeliveryTime deliveryTime) {
		this.delivery_time = deliveryTime;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult#setTotalCost(com.digsarustudio.banana.erp.general.UnitPrice)
	 */
	@Override
	public void setTotalCost(UnitPrice cost) {
		this.total_cost = cost;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult#setPostageCostCollection(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCostCollection)
	 */
	@Override
	public void setPostageCostCollection(PostageCostCollection collection) {
		this.costs = collection;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult#getService()
	 */
	@Override
	public ParcelService getService() {
		
		return this.service;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult#getDeliveryTime()
	 */
	@Override
	public DeliveryTime getDeliveryTime() {
		
		return this.delivery_time;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult#getTotalCost()
	 */
	@Override
	public UnitPrice getTotalCost() {
		
		return this.total_cost;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult#addPostageCost(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost)
	 */
	@Override
	public void addPostageCost(PostageCost cost) {
		if(null == this.costs) {
			this.costs = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageCostCollection.builder().build(); 
		}
		
		this.costs.addPostageCost(cost);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult#setPostageCosts(java.util.List)
	 */
	@Override
	public void setPostageCosts(List<PostageCost> costs) {
		if(null == this.costs) {
			this.costs = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageCostCollection.builder().build();
		}
		
		this.costs.setPostageCosts(costs);
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult#setPostageCosts(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost[])
	 */
	@Override
	public void setPostageCosts(PostageCost[] costs) {
		if(null == this.costs) {
			this.costs = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean.PostageCostCollection.builder().build();
		}
		
		this.costs.setPostageCosts(costs);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult#getPostageCost(java.lang.Integer)
	 */
	@Override
	public PostageCost getPostageCost(Integer index) {
		if(null == this.costs) {
			return null;
		}

		return this.costs.getPostageCost(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult#getPostageCosts()
	 */
	@Override
	public List<PostageCost> getPostageCosts() {
		if(null == this.costs) {
			return null;
		}
		
		return this.costs.getPostageCost();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageResult#validate()
	 */
	@Override
	public Boolean validate() {
		if( null == this.service || !this.service.validate() ) {
			return false;
		}
		
		if( null == this.delivery_time || !this.delivery_time.validate() ) {
			return false;
		}
		
		if( null == this.total_cost || !this.total_cost.validate() ) {
			return false;
		}
		
		if( null == this.costs || !this.costs.validate() ) {
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PostageResult [service=" + service + ", delivery_time=" + delivery_time + ", total_cost=" + total_cost
				+ ", costs=" + costs + "]";
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
