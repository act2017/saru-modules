/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The available services and additional options for a domestic/international parcels or letters.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/postage-parcel-domestic-service">postage/parcel/domestic/service</a>
 *
 */
public interface ParcelServiceOptionCollection {
	/**
	 * 
	 * The builder for {@link ParcelServiceOptionCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ParcelServiceOptionCollection> {
		Builder addOptionService(ParcelServiceOption option);
		Builder setOptionServices(List<ParcelServiceOption> options);
		Builder setOptionServices(ParcelServiceOption[] options);
		
		Builder setParentParcelService(ParcelService parent);
		Builder setParentParcelServiceOption(ParcelServiceOption parent);
	}
	
	void addOptionService(ParcelServiceOption option);
	void setOptionServices(List<ParcelServiceOption> options);
	void setOptionServices(ParcelServiceOption[] options);
	
	void setParentParcelService(ParcelService parent);
	void setParentParcelServiceOption(ParcelServiceOption parent);
	
	ParcelService getParentParcelService();
	ParcelServiceOption getParentParcelServiceOption();
	
	ParcelServiceOption getOptionService(Integer index);
	List<ParcelServiceOption> getOptionServices();
	
	Boolean validate();
}
