/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean;

import com.digsarustudio.banana.utils.ObjectIdentifier;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State;
import com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Latitude;
import com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Location;
import com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Longitude;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.LocalityCategory;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.Locality}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class Locality implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.Locality,
								 IsSerializable {
	public static final String ATTR_CATEGORY	= "category";
	public static final String ATTR_ID			= "id";
	public static final String ATTR_LATITUDE	= "latitude";
	public static final String ATTR_LOCATION	= "location";
	public static final String ATTR_LONGITUDE	= "longitude";
	public static final String ATTR_POSTCODE	= "postcode";
	public static final String ATTR_STATE		= "state";
	
	/**
	 * 
	 * The object builder for {@link Locality}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.Locality.Builder {
		private Locality result = null;

		private Builder() {
			this.result = new Locality();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality.Builder#setCategory(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityCategory)
		 */
		@Override
		public Builder setCategory(LocalityCategory category) {
			this.result.setCategory(category);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality.Builder#setId(com.digsarustudio.banana.utils.ObjectIdentifier)
		 */
		@Override
		public Builder setId(ObjectIdentifier id) {
			this.result.setId(id);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality.Builder#setLatitude(com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Latitude)
		 */
		@Override
		public Builder setLatitude(Latitude latitude) {
			this.result.setLatitude(latitude);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality.Builder#setLocation(com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Location)
		 */
		@Override
		public Builder setLocation(Location location) {
			this.result.setLocation(location);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality.Builder#setLongitude(com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Longitude)
		 */
		@Override
		public Builder setLongitude(Longitude longitude) {
			this.result.setLongitude(longitude);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality.Builder#setPostcode(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode)
		 */
		@Override
		public Builder setPostcode(Postcode postcode) {
			this.result.setPostcode(postcode);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality.Builder#setState(com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.State)
		 */
		@Override
		public Builder setState(State state) {
			this.result.setState(state);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Locality build() {
			return this.result;
		}

	}

	private LocalityCategory	category	= null;
	private ObjectIdentifier	id			= null;
	private Latitude			latitude	= null;
	private Location			location	= null;
	private Longitude			longitude	= null;
	private Postcode			postcode	= null;
	private State				state		= null;
	
	/**
	 * 
	 */
	private Locality() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality#setCategory(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityCategory)
	 */
	@Override
	public void setCategory(LocalityCategory category) {
		this.category = category;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality#setLatitude(com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Latitude)
	 */
	@Override
	public void setLatitude(Latitude latitude) {
		this.latitude = latitude;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality#setLocation(com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Location)
	 */
	@Override
	public void setLocation(Location location) {
		this.location = location;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality#setId(com.digsarustudio.banana.utils.ObjectIdentifier)
	 */
	@Override
	public void setId(ObjectIdentifier id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality#getId()
	 */
	@Override
	public ObjectIdentifier getId() {
		return this.id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality#setLongitude(com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Longitude)
	 */
	@Override
	public void setLongitude(Longitude longitude) {
		this.longitude = longitude;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality#setPostcode(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode)
	 */
	@Override
	public void setPostcode(Postcode postcode) {
		this.postcode = postcode;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality#setState(com.digsarustudio.saru.erp.musaceae.shared.geoinfo.nation.State)
	 */
	@Override
	public void setState(State state) {
		this.state = state;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality#getCategory()
	 */
	@Override
	public LocalityCategory getCategory() {
		
		return this.category;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality#getLatitude()
	 */
	@Override
	public Latitude getLatitude() {
		
		return this.latitude;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality#getLocation()
	 */
	@Override
	public Location getLocation() {
		
		return this.location;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality#getLongitude()
	 */
	@Override
	public Longitude getLongitude() {
		
		return this.longitude;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality#getPostcode()
	 */
	@Override
	public Postcode getPostcode() {
		
		return this.postcode;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality#getState()
	 */
	@Override
	public State getState() {
		
		return this.state;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.category || !this.category.validate()) {
			return false;
		}
		
		if(null == this.id || !this.id.validate()) {
			return false;
		}
		
		if(null == this.latitude || !this.latitude.validate()) {
			return false;
		}
		
		if(null == this.location || !this.location.validate()) {
			return false;
		}
		
		if(null == this.longitude || !this.longitude.validate()) {
			return false;
		}
		
		if(null == this.postcode || !this.postcode.validate()) {
			return false;
		}
		
		if(null == this.state || !this.state.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
