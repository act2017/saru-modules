/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The response of available services and additional options for a domestic/international parcels or letters from PAC API.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/postage-parcel-domestic-service">postage/parcel/domestic/service</a>
 *
 */
public interface ParcelServiceResponse {
	/**
	 * 
	 * The builder for {@link ParcelServiceResponse}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ParcelServiceResponse> {
		Builder setParcelServices(List<ParcelService> services);
		Builder setParcelServiceCollection(ParcelServiceCollection serviceCollection);
	}
	
	void setParcelServices(List<ParcelService> services);
	void setParcelServiceCollection(ParcelServiceCollection serviceCollection);
	
	ParcelServiceCollection getParcelServiceCollection();
	
	List<ParcelService>	getParcelServices();
	
	Boolean validate();
}
