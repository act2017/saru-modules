/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.javabean.BaseLength;
import com.digsarustudio.saru.erp.musaceae.shared.Length;
import com.digsarustudio.saru.erp.musaceae.shared.LengthUnit;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The freight size for Australia Post to use.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class FreightSize implements com.digsarustudio.saru.erp.musaceae.shared.FreightSize, IsSerializable {
	/**
	 * 
	 * The object builder for {@link FreightSize}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.musaceae.shared.FreightSize.Builder {
		private FreightSize result = null;

		private Builder() {
			this.result = new FreightSize();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.FreightSize.Builder#setWidth(com.digsarustudio.saru.erp.musaceae.shared.Length)
		 */
		@Override
		public Builder setWidth(Length width) {
			this.result.setWidth(width);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.FreightSize.Builder#setLength(com.digsarustudio.saru.erp.musaceae.shared.Length)
		 */
		@Override
		public Builder setLength(Length length) {
			this.result.setLength(length);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.FreightSize.Builder#setHeight(com.digsarustudio.saru.erp.musaceae.shared.Length)
		 */
		@Override
		public Builder setHeight(Length height) {
			this.result.setHeight(height);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public FreightSize build() {
			return this.result;
		}

	}
	
	private Length	width	= null;
	private Length	length	= null;
	private Length	height	= null;

	/**
	 * 
	 */
	private FreightSize() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.PackageSize#setWidth(com.digsarustudio.saru.erp.musaceae.shared.Length)
	 */
	@Override
	public void setWidth(Length width) {
		this.width = width;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.PackageSize#setLength(com.digsarustudio.saru.erp.musaceae.shared.Length)
	 */
	@Override
	public void setLength(Length length) {
		this.length = length;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.PackageSize#setHeight(com.digsarustudio.saru.erp.musaceae.shared.Length)
	 */
	@Override
	public void setHeight(Length height) {
		this.height = height;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.PackageSize#getWidth()
	 */
	@Override
	public Length getWidth() {
		
		return this.width;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.PackageSize#getLength()
	 */
	@Override
	public Length getLength() {
		
		return this.length;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.PackageSize#getHeight()
	 */
	@Override
	public Length getHeight() {
		
		return this.height;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.PackageSize#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.length || !this.length.validate()) {
			return false;
		}
		
		if(null == this.width || !this.width.validate()) {
			return false;
		}
		
		if(null == this.height || !this.height.validate()) {
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.FreightSize#toCentimetre()
	 */
	@Override
	public com.digsarustudio.saru.erp.musaceae.shared.FreightSize toCentimetre() {
		Length confirmedWidth = (new BaseLength.Builder()).setValue(this.width.getValue())
														  .setUnit(LengthUnit.CentiMetre)
														  .build();

		Length confirmedLength = (new BaseLength.Builder()).setValue(this.length.getValue())
														  .setUnit(LengthUnit.CentiMetre)
														  .build();

		Length confirmedHeight = (new BaseLength.Builder()).setValue(this.height.getValue())
														  .setUnit(LengthUnit.CentiMetre)
														  .build();
				
		return FreightSize.builder().setLength(confirmedLength)
									.setWidth(confirmedWidth)
									.setHeight(confirmedHeight)
									.build();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
