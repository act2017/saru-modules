/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-types indicate how long it takes to ship the freights.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface DeliveryTime {
	/**
	 * 
	 * The builder for {@link DeliveryTiem}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<DeliveryTime> {
		Builder setDescription(String desc);
	}
	
	void setDescription(String desc);
	String getDescription();
	
	Boolean validate();
}
