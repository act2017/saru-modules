/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel;import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.shared.UnitPrice;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService;

/**
 * The result of postage calculation.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/postage-parcel-domestic-calculate">postage/domestic/calculate</a>
 */
public interface PostageResult {
	/**
	 * 
	 * The builder for {@link PostageResult}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<PostageResult> {
		Builder setService(ParcelService service);
		Builder setDeliveryTime(DeliveryTime deliveryTime);
		Builder setTotalCost(UnitPrice cost);
		Builder addPostageCost(PostageCost cost);
		Builder setPostageCosts(List<PostageCost> costs);
		Builder setPostageCosts(PostageCost[] costs);
		Builder setPostageCostCollection(PostageCostCollection collection);
	}
	
	void setService(ParcelService service);
	void setDeliveryTime(DeliveryTime deliveryTime);
	void setTotalCost(UnitPrice cost);
	void addPostageCost(PostageCost cost);
	void setPostageCosts(List<PostageCost> costs);
	void setPostageCosts(PostageCost[] costs);
	void setPostageCostCollection(PostageCostCollection collection);
	
	ParcelService getService();
	DeliveryTime getDeliveryTime();
	UnitPrice getTotalCost();
	PostageCost getPostageCost(Integer index);
	List<PostageCost> getPostageCosts();
	
	Boolean validate();
}
