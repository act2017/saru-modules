/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * A collection of cost of postage.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/postage-parcel-domestic-calculate">postage/domestic/calculate</a>
 */
public interface PostageCostCollection {
	/**
	 * 
	 * The builder for {@link PostageCostCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<PostageCostCollection> {
		Builder addPostageCost(PostageCost cost);
		Builder setPostageCosts(List<PostageCost> costs);
		Builder setPostageCosts(PostageCost[] costs);
	}

	void addPostageCost(PostageCost cost);
	void setPostageCosts(List<PostageCost> costs);
	void setPostageCosts(PostageCost[] costs);
	
	PostageCost getPostageCost(Integer index);
	List<PostageCost> getPostageCost();
	
	Boolean validate();
}
