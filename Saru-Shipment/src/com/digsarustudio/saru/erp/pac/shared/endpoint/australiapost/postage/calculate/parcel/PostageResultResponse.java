/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The response of postage calculation.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/postage-parcel-domestic-calculate">postage/domestic/calculate</a>
 */
public interface PostageResultResponse {
	/**
	 * 
	 * The builder for {@link PostageResultResponse}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<PostageResultResponse> {
		Builder setPostageResult(PostageResult result);
	}
	
	void setPostageResult(PostageResult result);
	PostageResult getPostageResult();
	
	Boolean validate();
}
