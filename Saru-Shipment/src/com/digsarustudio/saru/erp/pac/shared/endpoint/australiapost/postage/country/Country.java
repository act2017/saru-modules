/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The country used by Australia Post, which contains 2 digits codes and full name.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/country">country</a>
 */
public interface Country {
	/**
	 * 
	 * The builder for {@link Country}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Country> {
		Builder setCode(String code);
		Builder setName(String name);
	}
	
	void setCode(String code);
	void setName(String name);
	
	String getCode();
	String getName();
	
	Boolean validate();
}
