/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Location}<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LocalityLocation implements com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Location, IsSerializable {
	/**
	 * 
	 * The object builder for {@link LocalityLocation}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Location.Builder {
		private LocalityLocation result = null;

		private Builder() {
			this.result = new LocalityLocation();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Location.Builder#setName(java.lang.String)
		 */
		@Override
		public Builder setName(String name) {
			this.result.setName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public LocalityLocation build() {
			return this.result;
		}

	}

	private String name	= null;
	
	/**
	 * 
	 */
	private LocalityLocation() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Location#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Location#getName()
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Location#validate()
	 */
	@Override
	public Boolean validate() {

		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
