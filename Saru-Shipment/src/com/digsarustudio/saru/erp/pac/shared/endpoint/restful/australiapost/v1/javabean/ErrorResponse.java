/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.ErrorMessage;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The implementation of {@link com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.ErrorResponse}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ErrorResponse implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.ErrorResponse
									, IsSerializable {

	private com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.v1.javabean.ErrorMessage error	= null;
	
	/**
	 * 
	 */
	private ErrorResponse() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.ErrorResponse#getErrorMesssage()
	 */
	@Override
	public ErrorMessage getErrorMesssage() {
		return this.error;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.ErrorResponse#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.error) {
			return false;
		}
		
		return this.error.validate();
	}

}
