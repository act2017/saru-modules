/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The implementation of {@link com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceCollection}<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/postage-parcel-domestic-service">postage/parcel/domestic/service</a>
 */
public class ParcelServiceCollection implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceCollection
											  , IsSerializable {
	public static final String ATTR_SERVICE	= "service";
	
	/**
	 * 
	 * The object builder for {@link ParcelServiceCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceCollection.Builder {
		private ParcelServiceCollection result = null;

		private Builder() {
			this.result = new ParcelServiceCollection();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceCollection.Builder#setParcelServices(java.util.List)
		 */
		@Override
		public Builder setParcelServices(List<ParcelService> services) {
			this.result.setParcelServices(services);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceCollection.Builder#setParcelServices(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService[])
		 */
		@Override
		public Builder setParcelServices(ParcelService[] services) {
			this.result.setParcelServices(services);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ParcelServiceCollection build() {
			return this.result;
		}

	}
	
	private ParcelService[] service	= null;
	
	
	/**
	 * 
	 */
	private ParcelServiceCollection() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceCollection#setParcelServices(java.util.List)
	 */
	@Override
	public void setParcelServices(List<ParcelService> services) {
		this.setParcelServices(services.toArray(this.service));
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceCollection#setParcelServices(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService[])
	 */
	@Override
	public void setParcelServices(ParcelService[] services) {
		this.service = services;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.serivce.parcel.ParcelServiceCollection#getParcelService(java.lang.Integer)
	 */
	@Override
	public ParcelService getParcelService(Integer index) {
		if(null == this.service) {
			return null;
		}
		
		if(null == index || index >= this.service.length) {
			return null;
		}
		
		
		return this.service[index];
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.serivce.parcel.ParcelServiceCollection#getParcelServices()
	 */
	@Override
	public List<com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService> getParcelServices() {
		
		return new ArrayList<>(Arrays.asList(this.service));
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.serivce.parcel.ParcelServiceCollection#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == service || this.service.length <= 0) {
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ParcelServiceCollection [service=" + Arrays.toString(service) + "]";
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
