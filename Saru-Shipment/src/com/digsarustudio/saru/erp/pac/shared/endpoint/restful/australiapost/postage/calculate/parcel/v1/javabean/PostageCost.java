/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.calculate.parcel.v1.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.UnitPrice;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCostItem;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PostageCost implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost
								  , IsSerializable {
	public static final String	ATTR_ITEM	= "item";
	public static final String	ATTR_COST	= "cost";

	/**
	 * 
	 * The object builder for {@link PostageCost}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost.Builder {
		private PostageCost result = null;

		private Builder() {
			this.result = new PostageCost();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost.Builder#setItem(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCostItem)
		 */
		@Override
		public Builder setItem(PostageCostItem item) {
			this.result.setItem(item);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost.Builder#setCost(com.digsarustudio.banana.erp.general.UnitPrice)
		 */
		@Override
		public Builder setCost(UnitPrice cost) {
			this.result.setCost(cost);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public PostageCost build() {
			return this.result;
		}

	}
	
	private PostageCostItem	item	= null;
	private UnitPrice		cost	= null;
	
	/**
	 * 
	 */
	private PostageCost() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost#setItem(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCostItem)
	 */
	@Override
	public void setItem(PostageCostItem item) {
		this.item = item;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost#setCost(com.digsarustudio.banana.erp.general.UnitPrice)
	 */
	@Override
	public void setCost(UnitPrice cost) {
		this.cost = cost;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost#getItem()
	 */
	@Override
	public PostageCostItem getItem() {
		
		return this.item;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost#getCost()
	 */
	@Override
	public UnitPrice getCost() {
		
		return this.cost;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.PostageCost#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.item || !this.item.validate()) {
			return false;
		}
		
		if(null == this.cost || !this.cost.validate()) {
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PostageCost [item=" + item + ", cost=" + cost + "]";
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
