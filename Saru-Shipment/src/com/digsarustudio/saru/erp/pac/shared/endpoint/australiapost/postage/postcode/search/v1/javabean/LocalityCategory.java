/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.LocalityCategory}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LocalityCategory implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.LocalityCategory
									   , IsSerializable {
	/**
	 * 
	 * The object builder for {@link LocalityCategory}<br>
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.LocalityCategory.Builder {
		private LocalityCategory result = null;

		private Builder() {
			this.result = new LocalityCategory();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityCategory.Builder#setName(java.lang.String)
		 */
		@Override
		public Builder setName(String name) {
			this.result.setName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public LocalityCategory build() {
			return this.result;
		}

	}

	private String	name	= null;
	
	/**
	 * 
	 */
	private LocalityCategory() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityCategory#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityCategory#getName()
	 */
	@Override
	public String getName() {

		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityCategory#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.name || this.name.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
