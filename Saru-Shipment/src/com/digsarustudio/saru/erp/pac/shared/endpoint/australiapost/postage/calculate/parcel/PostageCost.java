/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.shared.UnitPrice;

/**
 * The cost of postage.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/postage-parcel-domestic-calculate">postage/domestic/calculate</a>
 */
public interface PostageCost {
	/**
	 * 
	 * The builder for {@link PostageCost}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<PostageCost> {
		Builder setItem(PostageCostItem item);
		Builder setCost(UnitPrice cost);
	}
	
	void setItem(PostageCostItem item);
	void setCost(UnitPrice cost);
	
	PostageCostItem getItem();
	UnitPrice getCost();
	
	Boolean validate();
}
