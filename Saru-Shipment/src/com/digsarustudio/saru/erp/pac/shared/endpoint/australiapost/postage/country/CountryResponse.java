/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country;

import java.util.List;

/**
 * The response object returned from <a href="https://developers.auspost.com.au/apis/pac/reference/country">PAC - country api</a> which contains an array of countries.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/country">PAC - country api</a>
 * @see			CountryCollection, {@link Country}
 *
 */
public interface CountryResponse {
	CountryCollection getCountryCollection();
	Country getCountry(String code);
	List<Country> getCountries();
	
	Boolean validate();
}
