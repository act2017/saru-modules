/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.javabean;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.shared.Length;
import com.digsarustudio.saru.erp.musaceae.shared.LengthUnit;
import com.digsarustudio.saru.erp.musaceae.shared.javabean.BaseLength;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The freight length used by Australia Post.<br>
 * No static builder method for sub-type to implement it.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class FreightLength extends BaseLength implements Length, IsSerializable {

	/**
	 * 
	 * The object builder for {@link FreightLength}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<FreightLength> {
		private FreightLength result = null;

		private Builder() {
			this.result = new FreightLength();
		}
		
		/**
		 * @param value
		 * @see com.digsarustudio.saru.erp.musaceae.shared.javabean.BaseLength#setValue(java.lang.String)
		 */
		public Builder setValue(String value) {
			result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public FreightLength build() {
			return this.result;
		}
	}
	
	/**
	 * 
	 */
	private FreightLength() {
		this.setUnit(LengthUnit.CentiMetre);
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
