/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost;

/**
 * The error message for PAC.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface ErrorMessage {
	String getMessage();
	
	Boolean validate();
}
