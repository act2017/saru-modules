/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The category for {@link Locality}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface LocalityCategory {
	/**
	 * 
	 * The builder for {@link LocalityCategory}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<LocalityCategory> {
		Builder setName(String name);
	}
	
	void setName(String name);
	
	String getName();
	
	Boolean validate();
}
