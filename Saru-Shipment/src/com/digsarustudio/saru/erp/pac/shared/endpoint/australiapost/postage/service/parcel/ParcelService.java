/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The available services and additional options for a domestic/international parcels or letters.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/postage-parcel-domestic-service">postage/parcel/domestic/service</a>
 *
 */
public interface ParcelService {
	/**
	 * 
	 * The builder for {@link ParcelService}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ParcelService> {
		Builder setCode(String code);
		Builder setName(String name);
		Builder setPrice(String price);
		Builder setMaxExtraCover(Integer extraCover);
		Builder	setServiceOptionCollection(ParcelServiceOptionCollection collection);
		Builder addServiceOption(ParcelServiceOption option);
	}
	
	void setCode(String code);
	void setName(String name);
	void setPrice(String price);
	void setMaxExtraCover(Integer extraCover);
	void setServiceOptionCollection(ParcelServiceOptionCollection collection);
	void addServiceOption(ParcelServiceOption option);
	
	String getCode();
	String getName();
	String getPrice();
	Integer getMaxExtraCover();
	
	ParcelServiceOptionCollection	getServiceOptionCollection();
	
	Boolean hasServiceOption();
	
	Boolean validate();
}
