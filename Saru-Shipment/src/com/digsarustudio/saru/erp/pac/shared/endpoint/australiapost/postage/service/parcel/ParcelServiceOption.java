/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The available services options and additional options for a domestic/international parcels or letters.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/postage-parcel-domestic-service">postage/parcel/domestic/service</a>
 *
 */
public interface ParcelServiceOption {
	/**
	 * 
	 * The builder for {@link ParcelServiceOption}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ParcelServiceOption> {
		Builder setCode(String code);
		Builder setName(String name);
		Builder setMaxExtraCover(Integer extraCover);
		
		Builder setServiceSubOptionCollection(ParcelServiceOptionCollection collection);
		Builder addSubOption(ParcelServiceOption subOption);
		Builder setSubOptions(List<ParcelServiceOption> subOptions);
		Builder setSubOptions(ParcelServiceOption[] subOptions);
		Builder setParentService(ParcelService parent);
		Builder setParentServiceOption(ParcelServiceOption parentOption);
	}
	
	void setCode(String code);
	void setName(String name);
	void setMaxExtraCover(Integer extraCover);
	
	void setServiceSubOptionCollection(ParcelServiceOptionCollection collection);
	void addSubOption(ParcelServiceOption subOption);
	void setSubOptions(List<ParcelServiceOption> subOptions);
	void setSubOptions(ParcelServiceOption[] subOptions);
	void setParentService(ParcelService parent);
	void setParentServiceOption(ParcelServiceOption parentOption);
	
	String getCode();
	String getName();
	Integer getMaxExtraCover();
	
	ParcelServiceOptionCollection	getServiceSubOptionCollection();
	ParcelServiceOption getSubOption(Integer index);
	List<ParcelServiceOption> getSubOptions();
	ParcelService getParentService();
	ParcelServiceOption getParentServiceOption();
	Boolean hasSubOptions();
	
	Boolean validate();
}
