/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean;

import java.util.List;

import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.Locality;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.LocalityCollection;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link LocalityResponse}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LocalityResponse implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.LocalityResponse
									   , IsSerializable {
	public static final String ATTR_LOCALITIES	= "localities";
	
	/**
	 * 
	 * The object builder for {@link LocalityResponse}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.LocalityResponse.Builder {
		private LocalityResponse result = null;

		private Builder() {
			this.result = new LocalityResponse();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityResponse.Builder#addLocality(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality)
		 */
		@Override
		public Builder addLocality(Locality locality) {
			this.result.addLocality(locality);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityResponse.Builder#setLocalities(java.util.List)
		 */
		@Override
		public Builder setLocalities(List<Locality> localities) {
			this.result.setLocalities(localities);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityResponse.Builder#setLocalities(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality[])
		 */
		@Override
		public Builder setLocalities(Locality[] localities) {
			this.result.setLocalities(localities);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityResponse.Builder#setLocalityCollection(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityCollection)
		 */
		@Override
		public Builder setLocalityCollection(LocalityCollection collection) {
			this.result.setLocalityCollection(collection);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public LocalityResponse build() {
			return this.result;
		}

	}

	private LocalityCollection	localities	= null;
	
	/**
	 * 
	 */
	private LocalityResponse() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityResponse#addLocality(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality)
	 */
	@Override
	public void addLocality(Locality locality) {
		if(null == this.localities) {
			this.localities = com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityCollection.builder().build();
		}
		
		this.localities.addLocality(locality);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityResponse#setLocalities(java.util.List)
	 */
	@Override
	public void setLocalities(List<Locality> localities) {
		if(null == this.localities) {
			this.localities = com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityCollection.builder().build();
		}

		this.localities.setLocalities(localities);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityResponse#setLocalities(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.Locality[])
	 */
	@Override
	public void setLocalities(Locality[] localities) {
		if(null == this.localities) {
			this.localities = com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search.v1.javabean.LocalityCollection.builder().build();
		}

		this.localities.setLocalities(localities);

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityResponse#setLocalityCollection(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityCollection)
	 */
	@Override
	public void setLocalityCollection(LocalityCollection collection) {
		this.localities = collection;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityResponse#getLocality(java.lang.Integer)
	 */
	@Override
	public Locality getLocality(Integer index) {
		if(null == this.localities) {
			return null;
		}

		return this.localities.getLocality(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityResponse#getLocalities()
	 */
	@Override
	public List<Locality> getLocalities() {
		if(null == this.localities) {
			return null;
		}

		return this.localities.getLocalities();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postcode.search.LocalityResponse#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.localities) {
			return false;
		}
		
		if( !this.localities.validate() ) {
			return false;
		}

		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
