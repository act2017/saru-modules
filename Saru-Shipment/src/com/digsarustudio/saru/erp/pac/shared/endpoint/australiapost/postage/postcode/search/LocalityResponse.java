/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type represents the response returned by domestic/postcode/search API.<br>  
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/postcode-search">domestic/postcode/search</a>
 *
 */
public interface LocalityResponse {
	/**
	 * 
	 * The builder for {@link LocalityCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<LocalityResponse> {
		Builder addLocality(Locality locality);
		Builder setLocalities(List<Locality> localities);
		Builder setLocalities(Locality[] localities);
		Builder setLocalityCollection(LocalityCollection collection);
	}
	
	void addLocality(Locality locality);
	void setLocalities(List<Locality> localities);
	void setLocalities(Locality[] localities);
	void setLocalityCollection(LocalityCollection collection);
	
	Locality getLocality(Integer index);
	List<Locality> getLocalities();
	
	Boolean validate();
}
