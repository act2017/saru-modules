/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.postcode.search;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.banana.utils.ObjectIdentifier;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State;
import com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Latitude;
import com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Location;
import com.digsarustudio.saru.erp.musaceae.shared.geoinfo.Longitude;

/**
 * The locality search API by local postcode provided by Australia Post.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/postcode-search">domestic/postcode/search</a>
 */
public interface Locality {
	/**
	 * 
	 * The builder for {@link Locality}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Locality> {
		Builder setCategory(LocalityCategory category);
		Builder setId(ObjectIdentifier id);
		Builder setLatitude(Latitude latitude);
		Builder setLocation(Location location);
		Builder setLongitude(Longitude longitude);
		Builder setPostcode(Postcode postcode);
		Builder setState(State state);
	}
	
	void setCategory(LocalityCategory category);
	void setId(ObjectIdentifier id);
	void setLatitude(Latitude latitude);
	void setLocation(Location location);
	void setLongitude(Longitude longitude);
	void setPostcode(Postcode postcode);
	void setState(State state);
	
	LocalityCategory getCategory();
	ObjectIdentifier getId();
	Latitude getLatitude();
	Location getLocation();
	Longitude getLongitude();
	Postcode getPostcode();
	State getState();
	
	Boolean validate();
}
