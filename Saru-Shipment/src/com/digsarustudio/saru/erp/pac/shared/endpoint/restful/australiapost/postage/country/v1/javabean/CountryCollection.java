/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.country.v1.javabean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.digsarustudio.saru.erp.pac.server.endpoint.restful.australiapost.postage.country.v1.CountryEndpoint;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The collection of country used by {@link CountryEndpoint}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/country">country</a>
 *
 */
public class CountryCollection implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country.CountryCollection
										, IsSerializable{

	private Country[] country	= null;
	
	/**
	 * 
	 */
	private CountryCollection() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.CountryCollection#getCountry(java.lang.String)
	 */
	@Override
	public Country getCountry(String code) {
		if(null == code) {
			return null;
		}
		
		for (Country country : country) {
			if(null == country || null == country.getCode()) {
				continue;
			}
			
			if( !code.equalsIgnoreCase(country.getCode()) ) {
				continue;
			}
			
			return country;
		}		
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.CountryCollection#getCountries()
	 */
	@Override
	public List<com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.country.Country> getCountries() {
		return new ArrayList<>(Arrays.asList(this.country));
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.CountryCollection#validate()
	 */
	@Override
	public Boolean validate() {
		if( null == this.country ) {
			return false;
		}
		
		if( this.country.length <= 0 ) {
			return false;
		}
		
		return true;
	}

}
