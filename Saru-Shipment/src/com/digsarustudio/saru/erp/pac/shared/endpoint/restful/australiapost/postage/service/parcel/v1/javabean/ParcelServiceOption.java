/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean;

import java.util.List;

import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The implementation of {@link com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/postage-parcel-domestic-service">postage/parcel/domestic/service</a>
 *
 */
public class ParcelServiceOption implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption
										  , IsSerializable {
	public static final String	ATTR_CODE				= "code";
	public static final String	ATTR_NAME				= "name";
	
	/**
	 * [Unknown] for option, but doesn't exist.<br>
	 */
	public static final String	ATTR_MAX_EXTRA_COVER	= "max_extra_cover";
	
	/**
	 * [Optional] for sub options.
	 */
	public static final String	ATTR_SUBOPTIONS			= "suboptions";
	
	/**
	 * 
	 * The object builder for {@link ParcelServiceOption}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption.Builder {
		private ParcelServiceOption result = null;

		private Builder() {
			this.result = new ParcelServiceOption();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption.Builder#setCode(java.lang.String)
		 */
		@Override
		public Builder setCode(String code) {
			this.result.setCode(code);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption.Builder#setName(java.lang.String)
		 */
		@Override
		public Builder setName(String name) {
			this.result.setName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption.Builder#setMaxExtraCover(java.lang.Integer)
		 */
		@Override
		public Builder setMaxExtraCover(Integer extraCover) {
			this.result.setMaxExtraCover(extraCover);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption.Builder#setServiceSubOptionCollection(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection)
		 */
		@Override
		public Builder setServiceSubOptionCollection(ParcelServiceOptionCollection collection) {
			this.result.setServiceSubOptionCollection(collection);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption.Builder#addSubOption(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption)
		 */
		@Override
		public Builder addSubOption(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption subOption) {
			this.result.addSubOption(subOption);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption.Builder#setSubOptions(java.util.List)
		 */
		@Override
		public Builder setSubOptions(List<com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption> subOptions) {
			this.result.setSubOptions(subOptions);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption.Builder#setSubOptions(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption[])
		 */
		@Override
		public Builder setSubOptions(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption[] subOptions) {
			this.result.setSubOptions(subOptions);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption.Builder#setParentService(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService)
		 */
		@Override
		public Builder setParentService(ParcelService parent) {
			this.result.setParentService(parent);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption.Builder#setParentServiceOption(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption)
		 */
		@Override
		public Builder setParentServiceOption(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption parentOption) {
			this.result.setParentServiceOption(parentOption);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ParcelServiceOption build() {
			return this.result;
		}

	}
	
	private String							code			= null;
	private String							name			= null;
	
	/**
	 * [Optional]<br>
	 * Doesn't exist in service options.<br>
	 */
	private Integer							max_extra_cover	= null;
	
	/**
	 * [Optional]<br>
	 * Doesn't exist in service sub options.<br>
	 */
	private ParcelServiceOptionCollection	suboptions		= null;
	
	/**
	 * [Optional]<br>
	 * This is the parent for tracing back. Only used by client app.<br> 
	 */
	private ParcelService					parentService	= null;

	/**
	 * [Optional]<br>
	 * This is the partent for tacing back. Only used by option service in client app.<br>
	 */
	private com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption parentOption	= null;
	
	/**
	 * 
	 */
	private ParcelServiceOption() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.serivce.parcel.ParcelServiceOption#getCode()
	 */
	@Override
	public String getCode() {

		return this.code;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.serivce.parcel.ParcelServiceOption#getName()
	 */
	@Override
	public String getName() {

		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.serivce.parcel.ParcelServiceOption#getMaxExtraCover()
	 */
	@Override
	public Integer getMaxExtraCover() {

		return this.max_extra_cover;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.serivce.parcel.ParcelServiceOption#getServiceSubOptionCollection()
	 */
	@Override
	public ParcelServiceOptionCollection getServiceSubOptionCollection() {

		return this.suboptions;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.code = code;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption#setMaxExtraCover(java.lang.Integer)
	 */
	@Override
	public void setMaxExtraCover(Integer extraCover) {
		this.max_extra_cover = extraCover;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption#setServiceSubOptionCollection(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection)
	 */
	@Override
	public void setServiceSubOptionCollection(ParcelServiceOptionCollection collection) {
		this.suboptions = collection;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption#hasSubOptions()
	 */
	@Override
	public Boolean hasSubOptions() {
		if(null == this.suboptions) {
			return false;
		}
		
		List<com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption> options = this.suboptions.getOptionServices();
		if(null == options) {
			return false;
		}
		
		if(options.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption#addSubOption(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption)
	 */
	@Override
	public void addSubOption(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption subOption) {
		if(null == this.suboptions) {
			this.suboptions = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelServiceOptionCollection.builder().build();
		}
		
		this.suboptions.addOptionService(subOption);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption#setSubOptions(java.util.List)
	 */
	@Override
	public void setSubOptions(List<com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption> subOptions) {
		if(null == this.suboptions) {
			this.suboptions = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelServiceOptionCollection.builder().build();
		}
		
		this.suboptions.setOptionServices(subOptions);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption#setSubOptions(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption[])
	 */
	@Override
	public void setSubOptions(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption[] subOptions) {
		if(null == this.suboptions) {
			this.suboptions = com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean.ParcelServiceOptionCollection.builder().build();
		}
		
		this.suboptions.setOptionServices(subOptions);		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption#setParentService(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService)
	 */
	@Override
	public void setParentService(ParcelService parent) {
		this.parentService = parent;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption#setParentServiceOption(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption)
	 */
	@Override
	public void setParentServiceOption(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption parentOption) {
		this.parentOption = parentOption;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption#getParentServiceOption()
	 */
	@Override
	public com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption getParentServiceOption() {
		return this.parentOption;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption#getSubOption(java.lang.Integer)
	 */
	@Override
	public com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption getSubOption(Integer index) {
		if(null == this.suboptions) {
			return null;
		}
		
		return this.suboptions.getOptionService(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption#getSubOptions()
	 */
	@Override
	public List<com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption> getSubOptions() {
		if(null == this.suboptions) {
			return null;
		}
		
		return this.suboptions.getOptionServices();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption#getParentService()
	 */
	@Override
	public ParcelService getParentService() {
		return this.parentService;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.serivce.parcel.ParcelServiceOption#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.code || this.code.isEmpty()) {
			return false;
		}
		
		if(null == this.name || this.name.isEmpty()) {
			return false;
		}
		
		if(null != this.suboptions && !this.suboptions.validate()) {
			return false;
		}

		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ParcelServiceOption [code=" + code + ", name=" + name + ", max_extra_cover=" + max_extra_cover
				+ ", suboptions=" + suboptions + "]";
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
