/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost;

/**
 * The error response from PAC when the service is not available.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface ErrorResponse {
	ErrorMessage getErrorMesssage();
	
	Boolean validate();
}
