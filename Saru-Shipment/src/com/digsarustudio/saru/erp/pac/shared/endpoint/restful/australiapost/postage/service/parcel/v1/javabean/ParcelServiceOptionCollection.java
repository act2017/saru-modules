/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.restful.australiapost.postage.service.parcel.v1.javabean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The implementation of {@link com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/pac/reference/postage-parcel-domestic-service">postage/parcel/domestic/service</a>
 *
 */
public class ParcelServiceOptionCollection implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection
													, IsSerializable {
	public static final String	ATTR_OPTION	= "option";
	
	/**
	 * 
	 * The object builder for {@link ParcelServiceOptionCollection}.<br>
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection.Builder {
		private ParcelServiceOptionCollection result = null;

		private Builder() {
			this.result = new ParcelServiceOptionCollection();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection.Builder#setOptionServices(java.util.List)
		 */
		@Override
		public Builder setOptionServices(List<ParcelServiceOption> options) {
			this.result.setOptionServices(options);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection.Builder#setOptionServices(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption[])
		 */
		@Override
		public Builder setOptionServices(ParcelServiceOption[] options) {
			this.result.setOptionServices(options);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection.Builder#addOptionService(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption)
		 */
		@Override
		public Builder addOptionService(ParcelServiceOption option) {
			this.result.addOptionService(option);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection.Builder#setParentParcelService(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService)
		 */
		@Override
		public Builder setParentParcelService(ParcelService parent) {
			this.result.setParentParcelService(parent);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection.Builder#setParentParcelServiceOption(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption)
		 */
		@Override
		public Builder setParentParcelServiceOption(ParcelServiceOption parent) {
			this.result.setParentParcelServiceOption(parent);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ParcelServiceOptionCollection build() {
			return this.result;
		}

	}
	
	/**
	 * This is a mixed type, it could be an object or an array.<br>
	 */
	private List<ParcelServiceOption>	option	= null;

	private ParcelService		parentService		= null;
	private ParcelServiceOption	parentServiceOption	= null;
	
	/**
	 * 
	 */
	private ParcelServiceOptionCollection() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.serivce.parcel.ParcelServiceOptionCollection#getOptionService(java.lang.Integer)
	 */
	@Override
	public ParcelServiceOption getOptionService(Integer index) {
		if(null == this.option || index >= this.option.size()) {
			return null;
		}
		
		return this.option.get(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.serivce.parcel.ParcelServiceOptionCollection#getOptionServices()
	 */
	@Override
	public List<com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption> getOptionServices() {
		
		return this.option;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection#setOptionServices(java.util.List)
	 */
	@Override
	public void setOptionServices(List<ParcelServiceOption> options) {
		this.option = options;
		
		for (ParcelServiceOption option : this.option) {
			option.setParentService(this.parentService);
			option.setParentServiceOption(this.parentServiceOption);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection#setOptionServices(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption[])
	 */
	@Override
	public void setOptionServices(ParcelServiceOption[] options) {
		this.setOptionServices(new ArrayList<>(Arrays.asList(options)));
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection#addOptionService(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption)
	 */
	@Override
	public void addOptionService(ParcelServiceOption option) {
		if(null == this.option) {
			this.option = new ArrayList<>();
		}
		
		option.setParentService(this.parentService);
		option.setParentServiceOption(this.parentServiceOption);
		this.option.add(option);		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection#setParentParcelService(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService)
	 */
	@Override
	public void setParentParcelService(ParcelService parent) {
		this.parentService = parent;
		
		for (ParcelServiceOption option : this.option) {
			option.setParentService(this.parentService);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection#setParentParcelServiceOption(com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOption)
	 */
	@Override
	public void setParentParcelServiceOption(ParcelServiceOption parent) {
		this.parentServiceOption = parent;
		
		for (ParcelServiceOption option : this.option) {
			option.setParentServiceOption(this.parentServiceOption);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection#getParentParcelService()
	 */
	@Override
	public ParcelService getParentParcelService() {
		
		return this.parentService;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelServiceOptionCollection#getParentParcelServiceOption()
	 */
	@Override
	public ParcelServiceOption getParentParcelServiceOption() {
		return this.parentServiceOption;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.serivce.parcel.ParcelServiceOptionCollection#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.option || this.option.size() <= 0) {
			return false;
		}		
		
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ParcelServiceOptionCollection [option=" + option + "]";
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
