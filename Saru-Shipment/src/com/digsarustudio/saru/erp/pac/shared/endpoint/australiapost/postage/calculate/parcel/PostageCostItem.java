/**
 * 
 */
package com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.service.parcel.ParcelService;

/**
 * The item used to describe a cost of service.<br>
 * This field looks like the option field in {@link ParcelService}, but to make this individually due to 
 * the field names are different.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface PostageCostItem {
	/**
	 * 
	 * The builder for {@link PostageCostItem}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<PostageCostItem> {
		Builder setName(String name);
	}
		
	void setName(String name);
	String getName();
	
	Boolean validate();
}
