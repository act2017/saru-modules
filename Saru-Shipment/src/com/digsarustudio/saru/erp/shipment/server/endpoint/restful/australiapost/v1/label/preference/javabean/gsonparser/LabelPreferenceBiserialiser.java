/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.javabean.LabelPreference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.javabean.LabelPreferenceType;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite LabelPreference from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelPreferenceBiserialiser implements JsonDeserializer<LabelPreference>, JsonSerializer<LabelPreference> {

	/**
	 * 
	 */
	public LabelPreferenceBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(LabelPreference src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonObject		root		= new JsonObject();
		
		JsonElement		typeElem	= context.serialize(src.getType(), LabelPreferenceType.class);
		root.add(LabelPreference.ATTR_TYPE, typeElem);
		
		JsonElement		groupsElem	= context.serialize(src.getLabelFormats(), LabelFormatCollection.class);
		root.add(LabelPreference.ATTR_GROUPS, groupsElem);
				
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public LabelPreference deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject root	= json.getAsJsonObject();
		
		JsonElement				typeElem	= root.get(LabelPreference.ATTR_TYPE);
		LabelPreferenceType		type		= context.deserialize(typeElem, LabelPreferenceType.class);
		
		JsonElement				groupsElem	= root.get(LabelPreference.ATTR_GROUPS);
		LabelFormatCollection	groups		= context.deserialize(groupsElem, LabelFormatCollection.class);
		
		return LabelPreference.builder().setType(type)
										.setLabelFormats(groups)
										.build();
	}

}
