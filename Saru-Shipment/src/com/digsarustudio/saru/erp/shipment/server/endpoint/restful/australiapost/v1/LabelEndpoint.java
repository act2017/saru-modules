/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1;


import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.ResponseCallback;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.RESTfulResponseStatusCodes;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemCollectionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemIdBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorCollectionBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorContextBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorContextPairBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorsResponseBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.DateTimeBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ShipmentCollectionBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ShipmentIdBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.CreateLabelRequestBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.CreateLabelResponseBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.GetLabelResponseBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.LabelBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.LabelCollectionBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.LabelGenerationProgressCodeBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.LabelGenerationProgressMessageBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.LabelGenerationRequestStatusBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.LabelRequestIdBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.LabelShipmentBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.LabelShipmentItemBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.LabelURLBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.format.javabean.gsonparser.LabelBrandedBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.format.javabean.gsonparser.LabelFormatBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.format.javabean.gsonparser.LabelFormatCollectionBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.format.javabean.gsonparser.LabelFormatGroupBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.format.javabean.gsonparser.LabelFormatLayoutBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.format.javabean.gsonparser.LabelFormatLeftOffsetBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.format.javabean.gsonparser.LabelFormatTopOffsetBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.javabean.gsonparser.LabelPreferenceBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.javabean.gsonparser.LabelPreferenceCollectionBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.javabean.gsonparser.LabelPreferenceTypeBiserialiser;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Item;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostError;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorContext;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorContextPair;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorsResponse;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.DateTime;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.CreateLabelRequest;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.CreateLabelResponse;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.GetLabelResponse;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelGenerationProgressCode;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelGenerationProgressMessage;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelGenerationRequestStatus;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelRequestId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelURL;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelBranded;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormat;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatGroup;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatLayout;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatLeftOffset;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatTopOffset;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.javabean.LabelPreference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.javabean.LabelPreferenceCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.javabean.LabelPreferenceType;
import com.google.gson.GsonBuilder;

/**
 * This service initiates the generation of labels for the requested shipments that have been previously created using the Create Shipments service.
 * <h3>Resource information</h3>
 * <table>
 * 	<tr>
 * 		<td>Rate Limited?</td><td>No</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Requests per rate limit window</td><td>-</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Authentication</td><td>Basic Auth</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Response Formats</td><td>json</td>
 * 	</tr>
 * 	<tr>
 * 		<td>HTTP Methods</td><td>POST</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Resource family</td><td>Labels</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Response object</td><td>Label</td>
 * 	</tr>
 * 	<tr>
 * 		<td>API Version</td><td>v1.0</td>
 * 	</tr>
 * </table>
 * 
 * <h3>Service capabilities</h3>
 * This service initiates the generation of labels for the requested shipments that have been previously created using the Create Shipments service.
 * 
 * <h3>Resource URL</h3>
 * <a href="https://digitalapi.auspost.com.au/shipping/v1/labels">https://digitalapi.auspost.com.au/shipping/v1/labels</a>
 * 
 * <h3>Headers</h3>
 * <table>
 * 	<tr>
 * 		<td>authentication (required)</td><td>Your username and password as a HTTP Basic Auth hash. See REST and Authentication.</td>
 * 	</tr>
 * 	<tr>
 * 		<td>account-number (required)</td><td>A 10 digit number for an Australia Post charge account, or an 8 digit number for a StarTrack account. See REST and Authentication.</td>
 * 	</tr>
 * </table>
 * <br>
 * 
 * <h3>Parameters</h3>
 * 	<li>
 * 		<ul>Get Label
 * 
 * 			<table>
 * 				<tr>
 * 					<td>request_id (Required)</td><td>The identifier for the label request generated by Create Labels service</td>
 * 				</tr>
 * 			</table>
 * 		</ul>
 * </li>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * 
 * 
 * @param		
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a><br>
 * 				<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/get-shipments">Get Shipments</a>
 */
public class LabelEndpoint extends AustraliaPostEndpoint<Label, Label> {
	
	
	private final String API_PATH = "shipping/v1/labels";
	
	/**
	 * 
	 * The object builder for {@link LabelEndpoint}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<LabelEndpoint> {
		private LabelEndpoint result = null;

		private Builder() {
			this.result = new LabelEndpoint();
		}

		/**
		 * 
		 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.LabelEndpoint#enableSandboxEnvironment()
		 */
		public Builder enableSandboxEnvironment() {
			result.enableSandboxEnvironment();
			return this;
		}

		/**
		 * 
		 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.LabelEndpoint#enableProductionEnvironment()
		 */
		public Builder enableProductionEnvironment() {
			result.enableProductionEnvironment();
			return this;
		}

		/**
		 * @param details
		 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#setMerchant(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails)
		 */
		public Builder setMerchant(MerchantDetails details) {
			result.setMerchant(details);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public LabelEndpoint build() {
			return this.result;
		}

	}

	private com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelGenerationProgressMessage	message	= null;
	private com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelGenerationProgressCode		code	= null;
	
	private LabelEndpoint() {

	}
	
	/**
	 * This service creates a shipment with items and returns a summary of the pricing for the items.<br>
	 * 
	 * If each item Ids have been specified or all of it are not given, it means to print labels for each item as the separate labels.
	 * Otherwise, just given 1 item id  to generate a label for all of the items.<br>
	 * 
	 * @param data The shipment to create
	 * 
	 * @return The shipment created by Australia Post Create Shipment API.
	 * @throws EndpointException 
	 */
	public com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection create(Shipment data
																													, com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference preference) throws EndpointException  {		
		if(null == data || null == data.getShipmentId() || !data.getShipmentId().validate()) {
			throw new IllegalArgumentException("The incoming shipment[" + data.getShipmentId().getValue() + "] contains invalid item id, please confirm before send it");
		}
		
		for (com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item item : data.getItems().getItems()) {
			if(null != item && (null == item.getItemId() || !item.getItemId().validate()) ) {
				throw new IllegalArgumentException("The incoming shipment contains invalid item id, please confirm before send it");	
			}
		}
		
		this.clear();
				
		CreateLabelRequest request = CreateLabelRequest.builder().addPreference(preference)
																 .addShipment(data)
																 .build();
		
		this.setSuccessCode(RESTfulResponseStatusCodes.OK);
		this.setFailureCode(RESTfulResponseStatusCodes.BadRequest);
		this.setResponseObjectType(CreateLabelResponse.class);
		
		this.setMediaContent(request);
		
		CreateLabelResponse response = this.sendRESTfulPostRequest();
		
		if(null == response) {
			return null;
		}
		
		this.message = response.getGenerationProgressMessage();
		this.code = response.getGenerationProgressCode();
		com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection labels = response.getLabels();
		
		return labels;
	}
	
	/**
	 * This service retrieves the information against shipments and the items contained within for shipments created using the Create Shipments service.<br>
	 * 
	 * @param data The shipment to get.
	 * 
	 * @return The shipment fetched by Australia Post Get Shipment API.
	 * @throws EndpointException 
	 */
	public com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection get(Label data) throws EndpointException {
		if( null == data ) {
			return null;
		}
		
		com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelRequestId	id = data.getRequestId();
		if(null == id || !id.validate()) {
			return null;
		}
		
		this.clear();

		this.setSuccessCode(RESTfulResponseStatusCodes.OK);
		this.setFailureCode(RESTfulResponseStatusCodes.NotFound);
		this.setResponseObjectType(GetLabelResponse.class);
		
		this.addURLParameter(id.getValue());
		
		GetLabelResponse response = this.sendRESTfulGetRequest();		
		if(null == response) {
			return null;
		}
		
		return response.getLabels();
	}
	
	public com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelGenerationProgressMessage getGenerationProgressMessage(){
		return this.message;
	}
	
	public com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelGenerationProgressCode getGenerationProgressCode(){
		return this.code;
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#enableSandboxEnvironment()
	 */
	@Override
	public void enableSandboxEnvironment() {
		this.enableSandboxEnvironment(API_PATH);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#enableProductionEnvironment()
	 */
	@Override
	public void enableProductionEnvironment() {
		this.enableProductionEnvironment(API_PATH);
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#insert(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void insert(Label data, ResponseCallback<Label> callback) throws EndpointException {
		throw new UnsupportedOperationException("Insert is not supported yet");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#update(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void update(Label data, ResponseCallback<Label> callback) throws EndpointException {
		throw new UnsupportedOperationException("Update is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#update(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)
	 */
	@Override
	public void update(Label data, ResponseCallback<Label> callback, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("Update is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#list(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void list(Label data, ResponseCallback<CollectionResponse<Label>> callback) throws EndpointException {
		throw new UnsupportedOperationException("List is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#list(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void list(Label data, ResponseCallback<CollectionResponse<Label>> callback, Integer cursor,
			Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("List is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#delete(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void delete(Label data, ResponseCallback<Label> callback) throws EndpointException {
		throw new UnsupportedOperationException("Delete is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#delete(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)
	 */
	@Override
	public void delete(Label data, ResponseCallback<Label> callback, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("Delete is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#get(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void get(Label data, ResponseCallback<Label> callback) throws EndpointException {
		throw new UnsupportedOperationException("Get is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#initGsonAndTypeToken()
	 */
	@Override
	protected void initJsonParserAndTypeToken() {		
		// Due to the format of json string which contains mixed data type, so that providing a deserializer to handle it.
		GsonBuilder builder = new GsonBuilder();
		
		this.updateCreateLabelJsonBiserialiser(builder);
		this.updateGetLabelJsonBiserialiser(builder);
		this.updateErrorMessageBiserialiser(builder);
		
		this.setJsonParserObject(builder.create());		
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	private void updateCreateLabelJsonBiserialiser(GsonBuilder builder) {
		//Request
		builder.registerTypeAdapter(CreateLabelRequest.class, new CreateLabelRequestBiserialiser());
		builder.registerTypeAdapter(LabelPreferenceCollection.class, new LabelPreferenceCollectionBiserialiser());
		builder.registerTypeAdapter(LabelPreference.class, new LabelPreferenceBiserialiser());
		builder.registerTypeAdapter(LabelPreferenceType.class, new LabelPreferenceTypeBiserialiser());
		builder.registerTypeAdapter(LabelFormatCollection.class, new LabelFormatCollectionBiserialiser());
		builder.registerTypeAdapter(LabelFormat.class, new LabelFormatBiserialiser());		
		builder.registerTypeAdapter(LabelFormatGroup.class, new LabelFormatGroupBiserialiser());
		builder.registerTypeAdapter(LabelFormatLayout.class, new LabelFormatLayoutBiserialiser());
		builder.registerTypeAdapter(LabelBranded.class, new LabelBrandedBiserialiser());
		builder.registerTypeAdapter(LabelFormatLeftOffset.class, new LabelFormatLeftOffsetBiserialiser());
		builder.registerTypeAdapter(LabelFormatTopOffset.class, new LabelFormatTopOffsetBiserialiser());
		
		builder.registerTypeAdapter(ShipmentCollection.class, new ShipmentCollectionBiserialiser());
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.class, new LabelShipmentBiserialiser());
		builder.registerTypeAdapter(ShipmentId.class, new ShipmentIdBiserialiser());
		builder.registerTypeAdapter(ItemCollection.class, new ItemCollectionBiserializer());
		builder.registerTypeAdapter(Item.class, new LabelShipmentItemBiserialiser());
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemId.class, new ItemIdBiserialiser());
		
		//Response
		builder.registerTypeAdapter(CreateLabelResponse.class, new CreateLabelResponseBiserialiser());
		builder.registerTypeAdapter(LabelGenerationProgressMessage.class, new LabelGenerationProgressMessageBiserialiser());
		builder.registerTypeAdapter(LabelGenerationProgressCode.class, new LabelGenerationProgressCodeBiserialiser());
		builder.registerTypeAdapter(LabelCollection.class, new LabelCollectionBiserialiser());
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.Label.class, new LabelBiserialiser());
		builder.registerTypeAdapter(LabelRequestId.class, new LabelRequestIdBiserialiser());
		builder.registerTypeAdapter(LabelGenerationRequestStatus.class, new LabelGenerationRequestStatusBiserialiser());
		builder.registerTypeAdapter(DateTime.class, new DateTimeBiserialiser());
	}
	
	private void updateGetLabelJsonBiserialiser(GsonBuilder builder) {		
		builder.registerTypeAdapter(GetLabelResponse.class, new GetLabelResponseBiserialiser());
		builder.registerTypeAdapter(LabelURL.class, new LabelURLBiserialiser());
	}
	
	private void updateErrorMessageBiserialiser(GsonBuilder builder) {
		//Error Response
		builder.registerTypeAdapter(AusPostErrorsResponse.class, new AusPostErrorsResponseBiserialiser());
		builder.registerTypeAdapter(AusPostErrorCollection.class, new AusPostErrorCollectionBiserialiser());
		builder.registerTypeAdapter(AusPostError.class, new AusPostErrorBiserializer());
		builder.registerTypeAdapter(AusPostErrorContext.class, new AusPostErrorContextBiserialiser());
		builder.registerTypeAdapter(AusPostErrorContextPair.class, new AusPostErrorContextPairBiserialiser());
	}
}
