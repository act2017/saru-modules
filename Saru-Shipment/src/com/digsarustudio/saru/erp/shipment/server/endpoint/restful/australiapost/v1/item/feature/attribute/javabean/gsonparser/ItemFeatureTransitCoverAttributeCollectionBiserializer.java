/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.ItemFeatureTransitCoverAttributeCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverAmount;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverIncluded;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverMaximum;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverRate;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureTransitCover;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;


/**
 * The serializer and deserializer for Gson to parse or composite ItemFeatureTransitCoverAttributeCollection from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemFeatureTransitCoverAttributeCollectionBiserializer implements JsonSerializer<ItemFeatureTransitCoverAttributeCollection>
																			 , JsonDeserializer<ItemFeatureTransitCoverAttributeCollection> {

	/**
	 * 
	 */
	public ItemFeatureTransitCoverAttributeCollectionBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public ItemFeatureTransitCoverAttributeCollection deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject			root			= json.getAsJsonObject();
		
		JsonElement			coverAmountElem	= root.get(ItemFeatureTransitCover.ATTRIBUTE_COVER_AMOUNT);		
		TransitCoverAmount	coverAmount		= context.deserialize(coverAmountElem, TransitCoverAmount.class);
		
		JsonElement			rateElem		= root.get(ItemFeatureTransitCover.ATTRIBUTE_RATE);
		TransitCoverRate	rate			= null;
		if(null != rateElem && !rateElem.isJsonNull()) {
			rate = context.deserialize(rateElem, TransitCoverRate.class);
		}
		
		JsonElement				includedCoverElem	= root.get(ItemFeatureTransitCover.ATTRIBUTE_INCLUDED_COVER);
		TransitCoverIncluded	includedCover		= null;
		if(null != includedCoverElem && !includedCoverElem.isJsonNull()) {
			includedCover = context.deserialize(includedCoverElem, TransitCoverIncluded.class);
		}
		
		JsonElement			maxCoverElem	= root.get(ItemFeatureTransitCover.ATTRIBUTE_MAXIMUM_COVER);
		TransitCoverMaximum	maxCover		= null;
		if(null != maxCoverElem && !maxCoverElem.isJsonNull()) {
			maxCover = context.deserialize(maxCoverElem, TransitCoverMaximum.class);
		}
		
		return ItemFeatureTransitCoverAttributeCollection.builder().setTransitCoverAmount(coverAmount)
																   .setRate(rate)
																   .setCoverIncluded(includedCover)
																   .setMaximumCover(maxCover)
																   .build();
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(ItemFeatureTransitCoverAttributeCollection src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonObject	root	= new JsonObject();
		
		JsonElement	coverAmountElem	= context.serialize(src.getTransitCoverAmount(), TransitCoverAmount.class);
		root.add(ItemFeatureTransitCover.ATTRIBUTE_COVER_AMOUNT, coverAmountElem);
		
		return root;
	}

}
