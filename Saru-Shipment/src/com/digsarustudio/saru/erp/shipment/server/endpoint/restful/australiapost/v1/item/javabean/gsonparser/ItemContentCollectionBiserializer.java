/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemContent;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemContentCollection;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link ItemContent} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemContentCollectionBiserializer implements JsonSerializer<ItemContentCollection>, JsonDeserializer<ItemContentCollection> {

	/**
	 * 
	 */
	public ItemContentCollectionBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public ItemContentCollection deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		List<com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent> contents = new ArrayList<>();
		JsonArray source = json.getAsJsonArray();
		for (int i = 0; i < source.size(); i++) {
			contents.add(context.deserialize(source.get(i), ItemContent.class));
		}
		
		return ItemContentCollection.builder().setItemCotents(contents)
											  .build();
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(ItemContentCollection src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonArray contents	= new JsonArray();
		
		for (com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent content : src.getItemContents()) {
			contents.add(context.serialize(content, ItemContent.class));
		}
		
		return contents;
	}
}
