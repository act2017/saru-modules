/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * A value for customer reference 1 or 2 exceeds the maximum limit of 50 characters. 
 * Please reduce the length of the value and submit the request again.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class MaxLengthCustomerReferenceTextException extends AustraliaPostErrorException {

	/**
	 * 
	 */
	public MaxLengthCustomerReferenceTextException() {

	}

	/**
	 * @param message
	 */
	public MaxLengthCustomerReferenceTextException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public MaxLengthCustomerReferenceTextException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public MaxLengthCustomerReferenceTextException(String message, Throwable cause, AusPostErrorCollection error) {
		super(message, cause, error);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public MaxLengthCustomerReferenceTextException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
