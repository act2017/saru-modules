/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.UnauthorisedException;

/**
 * The {@link UnauthorisedException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class UnauthorisedExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<UnauthorisedException> 
										implements RESTfulResponseExceptionBuilder<UnauthorisedException> {
		
	/**
	 * 
	 */
	public UnauthorisedExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public UnauthorisedException build() {
		return new UnauthorisedException(this.getMessage(), this.getCause());
	}

}
