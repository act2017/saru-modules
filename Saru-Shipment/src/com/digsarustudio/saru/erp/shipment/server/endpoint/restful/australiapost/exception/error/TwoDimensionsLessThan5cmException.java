/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * At least 2 dimensions must be 5 cm
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class TwoDimensionsLessThan5cmException extends AustraliaPostErrorException {

	/**
	 * 
	 */
	public TwoDimensionsLessThan5cmException() {

	}

	/**
	 * @param message
	 */
	public TwoDimensionsLessThan5cmException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public TwoDimensionsLessThan5cmException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public TwoDimensionsLessThan5cmException(String message, Throwable cause, AusPostErrorCollection errors) {
		super(message, cause, errors);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public TwoDimensionsLessThan5cmException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
