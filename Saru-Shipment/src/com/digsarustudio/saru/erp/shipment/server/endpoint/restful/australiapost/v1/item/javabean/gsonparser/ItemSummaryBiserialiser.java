/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemSummary;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.OrderShipmentItemStatus;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TotalCost;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TotalGst;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link ItemSummary} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemSummaryBiserialiser implements JsonDeserializer<ItemSummary>
											  , JsonSerializer<ItemSummary> {

	/**
	 * 
	 */
	public ItemSummaryBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(ItemSummary src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}

		JsonObject root = new JsonObject();
		
		JsonElement	totalCostElem	= context.serialize(src.getTotalCost(), TotalCost.class);
		root.add(ItemSummary.ATTR_TOTAL_COST, totalCostElem);
		
		JsonElement	totalGstElem	= context.serialize(src.getTotalGST(), TotalGst.class);
		root.add(ItemSummary.ATTR_TOTAL_GST, totalGstElem);
		
		JsonElement	statusElem	= context.serialize(src.getStatus(), OrderShipmentItemStatus.class);
		root.add(ItemSummary.ATTR_STATUS, statusElem);
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public ItemSummary deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if( null == json || json.isJsonNull() ) {
			return null;
		}
		
		JsonObject 	root 			= json.getAsJsonObject();
		
		JsonElement	totalCostElem	= root.get(ItemSummary.ATTR_TOTAL_COST);
		TotalCost	totalCost		= context.deserialize(totalCostElem, TotalCost.class);
		
		JsonElement	totalGstElem	= root.get(ItemSummary.ATTR_TOTAL_GST);
		TotalGst	totalGst		= context.deserialize(totalGstElem, TotalGst.class);
		
		JsonElement	statusElem			= root.get(ItemSummary.ATTR_STATUS);
		OrderShipmentItemStatus	status	= context.deserialize(statusElem, OrderShipmentItemStatus.class);	
		
		return ItemSummary.builder().setTotalCost(totalCost)
									.setTotalGST(totalGst)
									.setStatus(status)
									.build();
	}
	
}
