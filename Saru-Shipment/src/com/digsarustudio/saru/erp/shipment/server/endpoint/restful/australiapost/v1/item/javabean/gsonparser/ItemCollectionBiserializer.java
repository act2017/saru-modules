/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Item;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemCollection;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link ItemCollection} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemCollectionBiserializer implements JsonDeserializer<ItemCollection>
												 , JsonSerializer<ItemCollection> {

	/**
	 * 
	 */
	public ItemCollectionBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(ItemCollection src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonArray items = new JsonArray();
		
		for (com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item item : src.getItems()) {
			items.add(context.serialize(item, Item.class));
		}		
		
		return items;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public ItemCollection deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonArray source	= json.getAsJsonArray();
		List<com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item> items	= new ArrayList<>();
		
		for (int i = 0; i < source.size(); i++) {
			items.add(context.deserialize(source.get(i), Item.class));
		}
		
		return ItemCollection.builder().setItems(items)
										.build();
	}

}
