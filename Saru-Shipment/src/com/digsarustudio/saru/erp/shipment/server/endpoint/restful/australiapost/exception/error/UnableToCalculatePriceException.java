/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * The service CODE is not available based upon the submitted weight of WEIGHT kg.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class UnableToCalculatePriceException extends AustraliaPostErrorException {

	/**
	 * 
	 */
	public UnableToCalculatePriceException() {

	}

	/**
	 * @param message
	 */
	public UnableToCalculatePriceException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public UnableToCalculatePriceException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnableToCalculatePriceException(String message, Throwable cause, AusPostErrorCollection errors) {
		super(message, cause, errors);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public UnableToCalculatePriceException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
