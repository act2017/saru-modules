/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Discount;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.FreightCharge;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.FuelSurcharge;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.OrderShipmentItemStatus;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.SecuritySurcharge;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentSummary;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TotalCost;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TotalGst;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TrackingSummary;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TransitCover;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link ShipmentSummary} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ShipmentSummaryBiserialiser implements JsonDeserializer<ShipmentSummary>
												  , JsonSerializer<ShipmentSummary> {

	/**
	 * 
	 */
	public ShipmentSummaryBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(ShipmentSummary src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonObject	root	= new JsonObject();
		
		JsonElement	totalCost		= context.serialize(src.getTotalCost(), TotalCost.class);
		root.add(ShipmentSummary.ATTR_TOTAL_COST, totalCost);
		
		JsonElement	totalGst		= context.serialize(src.getTotalGST(), TotalGst.class);
		root.add(ShipmentSummary.ATTR_TOTAL_GST, totalGst);
		
		JsonElement	status			= context.serialize(src.getStatus(), OrderShipmentItemStatus.class);
		root.add(ShipmentSummary.ATTR_STATUS, status);
		
		JsonPrimitive	numberOfItems	= new JsonPrimitive(src.getNumberOfItems());
		root.add(ShipmentSummary.ATTR_NUMBER_OF_ITEMS, numberOfItems);
		
		JsonElement	trackingSummaries	= context.serialize(src.getTrackingSummaries(), TrackingSummary.class);
		root.add(ShipmentSummary.ATTR_TRACKING_SUMMARY, trackingSummaries);
		
		JsonElement	freightCharge			= context.serialize(src.getFreightCharge(), FreightCharge.class);
		root.add(ShipmentSummary.ATTR_FREIGHT_CHARGE, freightCharge);
		
		JsonElement	discount			= context.serialize(src.getDiscount(), Discount.class);
		root.add(ShipmentSummary.ATTR_DISCOUNT, discount);
		
		JsonElement	transitCover			= context.serialize(src.getTransitCover(), TransitCover.class);
		root.add(ShipmentSummary.ATTR_TRANSIT_COVER, transitCover);
		
		JsonElement	securitySurcharge		= context.serialize(src.getSecuritySurcharge(), SecuritySurcharge.class);
		root.add(ShipmentSummary.ATTR_SECURITY_SURCHARGE, securitySurcharge);
		
		JsonElement	fuelSurcharge		= context.serialize(src.getFuelSurcharge(), FuelSurcharge.class);
		root.add(ShipmentSummary.ATTR_FUEL_SURCHARGE, fuelSurcharge);
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public ShipmentSummary deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject					root				= json.getAsJsonObject();
		
		JsonElement					totalCostElem		= root.get(ShipmentSummary.ATTR_TOTAL_COST);
		TotalCost					totalCost			= context.deserialize(totalCostElem, TotalCost.class);
		
		JsonElement					totalGstElem		= root.get(ShipmentSummary.ATTR_TOTAL_GST);
		TotalGst					totalGst			= context.deserialize(totalGstElem, TotalGst.class);
		
		JsonElement					statusElem			= root.get(ShipmentSummary.ATTR_STATUS);
		OrderShipmentItemStatus		status				= context.deserialize(statusElem, OrderShipmentItemStatus.class);
		
		Integer						numberOfItems		= null;
		JsonElement					numberOfItemsElem	= root.get(ShipmentSummary.ATTR_NUMBER_OF_ITEMS);
		if(null != numberOfItemsElem && !numberOfItemsElem.isJsonNull()) {
			numberOfItems	= numberOfItemsElem.getAsInt();
		}
		
		JsonElement					trackingSummaryElem	= root.get(ShipmentSummary.ATTR_TRACKING_SUMMARY);
		TrackingSummary	trackingSummaries	= context.deserialize(trackingSummaryElem, TrackingSummary.class);
		
		JsonElement					freightChargeElem	= root.get(ShipmentSummary.ATTR_FREIGHT_CHARGE);
		FreightCharge				freightCharge		= context.deserialize(freightChargeElem, FreightCharge.class);
		
		JsonElement					discountElem		= root.get(ShipmentSummary.ATTR_DISCOUNT);
		Discount					discount			= context.deserialize(discountElem, Discount.class);
		
		JsonElement					transitCoverElem	= root.get(ShipmentSummary.ATTR_TRANSIT_COVER);
		TransitCover				transitCover		= context.deserialize(transitCoverElem, TransitCover.class);
		
		JsonElement					securitySurchargeElem	= root.get(ShipmentSummary.ATTR_SECURITY_SURCHARGE);
		SecuritySurcharge			securitySurcharge		= context.deserialize(securitySurchargeElem, SecuritySurcharge.class);
		
		JsonElement					fuelSurchargeElem		= root.get(ShipmentSummary.ATTR_FUEL_SURCHARGE);
		FuelSurcharge				fuelSurcharge			= context.deserialize(fuelSurchargeElem, FuelSurcharge.class);
			
		return ShipmentSummary.builder().setTotalCost(totalCost)
										.setTotalGST(totalGst)
										.setStatus(status)
										.setNumberOfItems(numberOfItems)
										.setTrackingSummaries(trackingSummaries)
										.setFreightCharge(freightCharge)
										.setDiscount(discount)
										.setTransitCover(transitCover)
										.setSecuritySurcharge(securitySurcharge)
										.setFuelSurcharge(fuelSurcharge)
										.build();
	}

}
