/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.content.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.javabean.CountryOfOrigin;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link CountryOfOrigin} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class CountryOfOriginBiserialiser implements JsonDeserializer<CountryOfOrigin>
												  , JsonSerializer<CountryOfOrigin> {

	/**
	 * 
	 */
	public CountryOfOriginBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(CountryOfOrigin src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonPrimitive code = new JsonPrimitive(src.getCountryOfOrigin());
		
		return code;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public CountryOfOrigin deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		String code = json.getAsString();
		
		return CountryOfOrigin.builder().setCountry(code)
										.build();
	}

}
