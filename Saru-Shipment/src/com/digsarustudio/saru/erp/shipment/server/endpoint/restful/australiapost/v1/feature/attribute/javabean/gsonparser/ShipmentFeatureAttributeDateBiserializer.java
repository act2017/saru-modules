/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.attribute.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.ShipmentFeatureAttributeDate;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Date;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link ShipmentFeatureAttributeDate} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
 */
public class ShipmentFeatureAttributeDateBiserializer implements JsonDeserializer<ShipmentFeatureAttributeDate>
															  , JsonSerializer<ShipmentFeatureAttributeDate> {

	/**
	 * 
	 */
	public ShipmentFeatureAttributeDateBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(ShipmentFeatureAttributeDate src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}

		JsonElement	date = context.serialize(src.getDate(), Date.class);
		JsonObject	root = new JsonObject();
		root.add(ShipmentFeatureAttributeDate.ATTR_DATE, date);
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public ShipmentFeatureAttributeDate deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject root = json.getAsJsonObject();
		
		JsonElement dateElem 	= root.get(ShipmentFeatureAttributeDate.ATTR_DATE);
		Date		date		= context.deserialize(dateElem, Date.class);
		
		return ShipmentFeatureAttributeDate.builder().setValue(date)
							 						 .build();
	}

	
}
