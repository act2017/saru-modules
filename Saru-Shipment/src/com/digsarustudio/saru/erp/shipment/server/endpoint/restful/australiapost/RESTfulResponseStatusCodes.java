/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost;

import java.io.Serializable;

/**
 * This class contains the status code with the name and description provided by Australia Post Shipment API.<br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/restful">REST and Authentication</a>
 */
@SuppressWarnings("serial")
public class RESTfulResponseStatusCodes implements Serializable {
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 200</li>
	 * 	<li><b>Name</b>: OK</li>
	 * 	<li><b>Description:</b> Optional okay (success) message.</li>
	 * </ul>
	 */
	public static final RESTfulResponseStatusCodes OK				= new RESTfulResponseStatusCodes(200, "OK", "Optional okay (success) message.");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 201</li>
	 * 	<li><b>Name</b>: Created</li>
	 * 	<li><b>Description:</b> The API has successfully created the object requested.</li>
	 * </ul>
	 */
	public static final RESTfulResponseStatusCodes	Created			= new RESTfulResponseStatusCodes(201, "Created", "The API has successfully created the object requested.");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 400</li>
	 * 	<li><b>Name</b>: Bad Request</li>
	 * 	<li><b>Description:</b> The input request is missing the mandatory header element with the name 
	 * 							account-number. Please resubmit the request including the required header 
	 * 							element and value.</li>
	 * </ul>
	 */
	public static final RESTfulResponseStatusCodes	BadRequest			= new RESTfulResponseStatusCodes(400, "Bad Request", "The input request is missing the mandatory header element with the name account-number. Please resubmit the request including the required header element and value.");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 401</li>
	 * 	<li><b>Name</b>: Unauthorised</li>
	 * 	<li><b>Description:</b> Sorry, you are not authorised to use this service.</li>
	 * </ul>
	 */
	public static final RESTfulResponseStatusCodes	Unauthorised		= new RESTfulResponseStatusCodes(401, "Unauthorised", "Sorry, you are not authorised to use this service.");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 404</li>
	 * 	<li><b>Name</b>: Unauthorised</li>
	 * 	<li><b>Description:</b> Sorry, you are not authorised to use this service.</li>
	 * </ul>
	 */
	public static final RESTfulResponseStatusCodes	NotFound		= new RESTfulResponseStatusCodes(404, "NotFound", "Target not found");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 406</li>
	 * 	<li><b>Name</b>: Not Acceptable</li>
	 * 	<li><b>Description:</b> Invalid version or invalid method.</li>
	 * </ul>
	 */
	public static final RESTfulResponseStatusCodes	NotAcceptable			= new RESTfulResponseStatusCodes(406, "Not Acceptable", "Invalid version or invalid method.");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 412</li>
	 * 	<li><b>Name</b>: Precondition Failed</li>
	 * 	<li><b>Description:</b> Precondition Failed.</li>
	 * </ul>
	 */
	public static final RESTfulResponseStatusCodes	PreconditionFailed		= new RESTfulResponseStatusCodes(412, "Precondition Failed", "Precondition Failed.");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 503</li>
	 * 	<li><b>Name</b>: Service unavailable</li>
	 * 	<li><b>Description:</b> Service is currently unavailable.</li>
	 * </ul>
	 */
	public static final RESTfulResponseStatusCodes	ServiceUnavailable			= new RESTfulResponseStatusCodes(503, "Service unavailable", "Service is currently unavailable.");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 500</li>
	 * 	<li><b>Name</b>: Server Error</li>
	 * 	<li><b>Description:</b> Error in processing, see detail description</li>
	 * </ul>
	 */
	public static final RESTfulResponseStatusCodes	ServerError			= new RESTfulResponseStatusCodes(500, "Server Error", "Error in processing, see detail description");

	private Integer code 		= null;
	private String name 		= null;
	private String description	= null;

	private static RESTfulResponseStatusCodes[] VALUES = { OK, Created
														 , BadRequest
														 , Unauthorised
														 , NotFound
														 , NotAcceptable
														 , PreconditionFailed
														 , ServiceUnavailable
														 , ServerError														 
												  		 };

	private RESTfulResponseStatusCodes(Integer code, String name, String description) {
		this.code 		= code;
		this.name 		= name;
		this.description= description;
	}

	public Integer getCode() {
		return this.code;
	}

	public String getName() {
		return this.name;
	}
	
	public String getDescription() {
		return this.description;
	}

	public static RESTfulResponseStatusCodes fromCode(Integer code) {
		if (null == code) {
			return null;
		}

		RESTfulResponseStatusCodes rtn = null;

		for (RESTfulResponseStatusCodes target : VALUES) {
			if (!code.equals(target.getCode())) {
				continue;
			}

			rtn = target;
		}

		return rtn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RESTfulResponseStatusCodes other = (RESTfulResponseStatusCodes) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}

}
