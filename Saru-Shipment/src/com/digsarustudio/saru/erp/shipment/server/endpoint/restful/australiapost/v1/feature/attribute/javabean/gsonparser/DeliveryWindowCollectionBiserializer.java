/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.attribute.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.DeliveryWindow;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.DeliveryWindowCollection;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link DeliveryWindowCollection} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
 */
public class DeliveryWindowCollectionBiserializer implements JsonDeserializer<DeliveryWindowCollection>
														   , JsonSerializer<DeliveryWindowCollection> {

	/**
	 * 
	 */
	public DeliveryWindowCollectionBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(DeliveryWindowCollection src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonObject 	root		= new JsonObject();
		JsonArray	windows		= new JsonArray();		
	
		for (com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.DeliveryWindow window : src.getDeliveryWindows()) {
			windows.add(context.serialize(window, DeliveryWindow.class));
		}
		
		root.add(DeliveryWindowCollection.ATTR_WINDOWS, windows);
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public DeliveryWindowCollection deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject 	root 		= json.getAsJsonObject();
		JsonElement	windowsElem	= root.get(DeliveryWindowCollection.ATTR_WINDOWS);
		
		DeliveryWindow[]	windows	= context.deserialize(windowsElem, DeliveryWindow[].class);
				
		return DeliveryWindowCollection.builder().setDeliveryWindows(windows)
												 .build();
	}

	
}
