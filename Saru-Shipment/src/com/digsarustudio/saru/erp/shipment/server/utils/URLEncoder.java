/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.utils;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

import com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.QueryParameter;


/**
 * This url encoder use {@link HttpClient} as the main encoder and decoder for the URL.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class URLEncoder implements com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.URLEncoder {

	/**
	 * 
	 */
	public URLEncoder() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.url.URLEncoder#encodeUTF8(java.lang.String)
	 */
	@Override
	public String encodeUTF8(String url) {		
		return this.encodeUTF8(url, null);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.url.URLEncoder#encodeUTF8(java.lang.String, java.util.List)
	 */
	@Override
	public String encodeUTF8(String url, List<QueryParameter> queryParameters) {				
		StringBuffer buffer = new StringBuffer();
		buffer.append(url);
		
		if(null != queryParameters) {
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			for (QueryParameter param : queryParameters) {
				NameValuePair nvPair = new BasicNameValuePair(param.getKey(), param.getValue());
				nameValuePairs.add(nvPair);
			}
			
			buffer.append("?");
			buffer.append(URLEncodedUtils.format(nameValuePairs, StandardCharsets.UTF_8));
		}
		
		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.url.URLEncoder#decodeUTF8(java.lang.String)
	 */
	@Override
	public List<QueryParameter> decodeUTF8(String queryParameterString) {
		if(null == queryParameterString || queryParameterString.isEmpty()) {
			return null;
		}
		
		List<NameValuePair> nvPairs = URLEncodedUtils.parse(queryParameterString, StandardCharsets.UTF_8);
		
		List<QueryParameter> params = new ArrayList<>();
		for (NameValuePair nvPair : nvPairs) {
			params.add(QueryParameter.builder().setKey(nvPair.getName())
											   .setValue(nvPair.getValue())
											   .build());
		}
		
		return params;
	}

}
