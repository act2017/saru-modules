/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.MaxLengthDestinationBusinessNameException;

/**
 * The {@link MaxLengthDestinationBusinessNameException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class MaxLengthDestinationBusinessNameExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<MaxLengthDestinationBusinessNameException> 
											 				implements RESTfulResponseExceptionBuilder<MaxLengthDestinationBusinessNameException> {
		
	/**
	 * 
	 */
	public MaxLengthDestinationBusinessNameExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public MaxLengthDestinationBusinessNameException build() {
		return new MaxLengthDestinationBusinessNameException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
