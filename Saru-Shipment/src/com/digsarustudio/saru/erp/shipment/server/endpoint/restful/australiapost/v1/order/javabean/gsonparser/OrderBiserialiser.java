/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.order.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.DateTime;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.Consignor;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.Order;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.OrderId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.OrderReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.OrderSummary;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.PaymentMethod;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link Order} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class OrderBiserialiser implements JsonDeserializer<Order>, JsonSerializer<Order> {

	/**
	 * 
	 */
	public OrderBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(Order src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;			
		}
		
		JsonObject	root	= new JsonObject();
		
		JsonElement	orderIdElem		= context.serialize(src.getOrderId(), OrderId.class);
		root.add(Order.ATTR_ORDER_ID, orderIdElem); 
		
		JsonElement	orderRefElem	= context.serialize(src.getOrderReference(), OrderReference.class);
		root.add(Order.ATTR_ORDER_REFERENCE, orderRefElem); 
		
		JsonElement		orderCreationDateElem	= context.serialize(src.getOrderCreationDate(), DateTime.class);
		root.add(Order.ATTR_ORDER_CREATION_DATE, orderCreationDateElem); 
		
		JsonElement		orderSummaryElem		= context.serialize(src.getOrderSummary(), OrderSummary.class);
		root.add(Order.ATTR_ORDER_SUMMARY, orderSummaryElem); 
		
		JsonElement		shipmentsElem		= context.serialize(src.getShipments(), ShipmentCollection.class);
		root.add(Order.ATTR_SHIPMENTS, shipmentsElem);
		
		JsonElement			paymentMethodElem	= context.serialize(src.getPaymentMethod(), PaymentMethod.class);
		root.add(Order.ATTR_PAYMENT_METHOD, paymentMethodElem);
		
		JsonElement			consignorElem	= context.serialize(src.getConsignor(), Consignor.class);
		root.add(Order.ATTR_CONSIGNOR, consignorElem); 
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public Order deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject	root	= json.getAsJsonObject();
		
		JsonElement	orderIdElem		= root.get(Order.ATTR_ORDER_ID);
		OrderId		orderId			= context.deserialize(orderIdElem, OrderId.class);
		
		JsonElement		orderRefElem	= root.get(Order.ATTR_ORDER_REFERENCE);
		OrderReference	orderRef		= context.deserialize(orderRefElem, OrderReference.class);
		
		JsonElement		orderCreationDateElem	= root.get(Order.ATTR_ORDER_CREATION_DATE);
		DateTime		orderCreationDate		= context.deserialize(orderCreationDateElem, DateTime.class);
		
		JsonElement		orderSummaryElem		= root.get(Order.ATTR_ORDER_SUMMARY);
		OrderSummary	orderSummary			= context.deserialize(orderSummaryElem, OrderSummary.class);
		
		JsonElement			shipmentsElem		= root.get(Order.ATTR_SHIPMENTS);
		ShipmentCollection	shipments			= context.deserialize(shipmentsElem, ShipmentCollection.class);
		
		JsonElement			paymentMethodElem	= root.get(Order.ATTR_PAYMENT_METHOD);
		PaymentMethod		paymentMethod		= context.deserialize(paymentMethodElem, PaymentMethod.class);
		
		JsonElement			consignorElem		= root.get(Order.ATTR_CONSIGNOR);
		Consignor			consignor			= context.deserialize(consignorElem, Consignor.class);
		
		return Order.builder().setOrderId(orderId)
							  .setConsignor(consignor)
							  .setOrderReference(orderRef)
							  .setOrderCreationDate(orderCreationDate)
							  .setOrderSummary(orderSummary)
							  .setShipments(shipments)
							  .setPaymentMethod(paymentMethod)
							  .build();
	}

}
