/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * Incomplete dimensions supplied - price will be calculated on weight.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class PartialDimensionsInShipmentWarningException extends AustraliaPostErrorException {

	/**
	 * 
	 */
	public PartialDimensionsInShipmentWarningException() {

	}

	/**
	 * @param message
	 */
	public PartialDimensionsInShipmentWarningException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public PartialDimensionsInShipmentWarningException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public PartialDimensionsInShipmentWarningException(String message, Throwable cause, AusPostErrorCollection errors) {
		super(message, cause, errors);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public PartialDimensionsInShipmentWarningException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
