/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.ContractNotValidException;

/**
 * The {@link ContractNotValidException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ContractNotValidExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<ContractNotValidException> 
											 implements RESTfulResponseExceptionBuilder<ContractNotValidException> {
		
	/**
	 * 
	 */
	public ContractNotValidExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public ContractNotValidException build() {
		return new ContractNotValidException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
