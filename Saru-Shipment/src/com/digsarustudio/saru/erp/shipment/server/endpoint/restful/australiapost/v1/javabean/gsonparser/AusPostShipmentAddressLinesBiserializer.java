/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentAddressLines;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The deserializer and serializer of {@link AusPostShipmentAddressLines}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AusPostShipmentAddressLinesBiserializer implements JsonDeserializer<AusPostShipmentAddressLines>
																,JsonSerializer<AusPostShipmentAddressLines>{

	/**
	 * 
	 */
	public AusPostShipmentAddressLinesBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public AusPostShipmentAddressLines deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		String[] lines = null;
		if( json.isJsonArray() ) {
			lines = context.deserialize(json, String[].class);
		}else {
			lines 		= new String[1];
			lines[0]	= json.getAsString();
		}		
		
		return AusPostShipmentAddressLines.builder().setAddresses(lines)
										  .build();
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(AusPostShipmentAddressLines src, Type typeOfSrc, JsonSerializationContext context) {
		if( null == src || !src.validate() ) {
			return null;
		}
		
		JsonArray array = new JsonArray();
		
		for (String line : src.getAddresses()) {
			array.add(line);
		}
		
		return array;
	}

}
