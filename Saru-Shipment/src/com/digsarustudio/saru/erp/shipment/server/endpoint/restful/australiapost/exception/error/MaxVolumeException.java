/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * Maximum weight must not exceed 10000 kg
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class MaxVolumeException extends AustraliaPostErrorException {

	/**
	 * 
	 */
	public MaxVolumeException() {

	}

	/**
	 * @param message
	 */
	public MaxVolumeException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public MaxVolumeException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public MaxVolumeException(String message, Throwable cause, AusPostErrorCollection error) {
		super(message, cause, error);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public MaxVolumeException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
