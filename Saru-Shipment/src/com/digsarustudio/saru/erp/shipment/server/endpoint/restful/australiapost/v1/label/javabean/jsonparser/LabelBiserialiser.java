/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.DateTime;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.Label;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelGenerationRequestStatus;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelRequestId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelURL;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link Label} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelBiserialiser implements JsonDeserializer<Label>, JsonSerializer<Label> {

	/**
	 * 
	 */
	public LabelBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(Label src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}

		JsonObject root = new JsonObject();
		
		JsonElement		requestIdElem		= context.serialize(src.getRequestId(), LabelRequestId.class);
		root.add(Label.ATTR_REQUEST_ID, requestIdElem);
		
		JsonElement		requestDateElem		= context.serialize(src.getRequestDate(), DateTime.class);
		root.add(Label.ATTR_REQUEST_DATE, requestDateElem);
		
		JsonElement		urlElem				= context.serialize(src.getURL(), LabelURL.class);
		root.add(Label.ATTR_URL, urlElem);
		
		JsonElement		urlCreationDateElem	= context.serialize(src.getURLCreationDate(), DateTime.class);
		root.add(Label.ATTR_URL_CREATION_DATE, urlCreationDateElem);
		
		JsonElement			shipmentsElem	= context.serialize(src.getShipments(), ShipmentCollection.class);
		root.add(Label.ATTR_SHIPMENTS, shipmentsElem);
				
		JsonElement	labelUrlElem	= context.serialize(src.getPrintableURL(), LabelURL.class);
		root.add(Label.ATTR_LABEL_URL, labelUrlElem);
		
		JsonElement	statusElem	= context.serialize(src.getStatus(), LabelGenerationRequestStatus.class);
		root.add(Label.ATTR_STATUS, statusElem);
		
		JsonElement	creationDateElem	= context.serialize(src.getCreationDate(), DateTime.class);
		root.add(Label.ATTR_LABEL_CREATION_DATE, creationDateElem);
		
		JsonElement	errorsElem	= context.serialize(src.getErrors(), AusPostErrorCollection.class);
		root.add(Label.ATTR_LABEL_ERRORS, errorsElem);		
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public Label deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject 		root				= json.getAsJsonObject();
		
		JsonElement		requestIdElem		= root.get(Label.ATTR_REQUEST_ID);
		LabelRequestId	requestId			= context.deserialize(requestIdElem, LabelRequestId.class);
		
		JsonElement		requestDateElem		= root.get(Label.ATTR_REQUEST_DATE);
		DateTime		requestDate			= context.deserialize(requestDateElem, DateTime.class);
		
		JsonElement		urlElem				= root.get(Label.ATTR_URL);
		LabelURL		url					= context.deserialize(urlElem, LabelURL.class);
		
		JsonElement		urlCreationDateElem	= root.get(Label.ATTR_URL_CREATION_DATE);
		DateTime		urlCreationDate		= context.deserialize(urlCreationDateElem, DateTime.class);
		
		JsonElement			shipmentsElem	= root.get(Label.ATTR_SHIPMENTS);
		ShipmentCollection	shipments		= context.deserialize(shipmentsElem, ShipmentCollection.class);
		
		JsonElement		lableUrlElem		= root.get(Label.ATTR_LABEL_URL);
		LabelURL		lableUrl			= context.deserialize(lableUrlElem, LabelURL.class);
		
		JsonElement		statusElem			= root.get(Label.ATTR_STATUS);
		LabelGenerationRequestStatus		status				= context.deserialize(statusElem, LabelGenerationRequestStatus.class);
		
		JsonElement		creationDateElem	= root.get(Label.ATTR_LABEL_CREATION_DATE);
		DateTime		creationDate		= context.deserialize(creationDateElem, DateTime.class);
		
		JsonElement				errorsElem	= root.get(Label.ATTR_LABEL_ERRORS);
		AusPostErrorCollection	errors		= context.deserialize(errorsElem, AusPostErrorCollection.class);

		return Label.builder().setRequestId(requestId)
							  .setRequestDate(requestDate)
							  .setURL(url)
							  .setURLCreationDate(urlCreationDate)
							  .setShipments(shipments)
							  .setPrintableURL(lableUrl)
							  .setStatus(status)
							  .setCreationDate(creationDate)
							  .setErrors(errors)
							  .build();
	}

}
