/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorContext;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostError;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or compile {@link AusPostError} from or to a JSON string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AusPostErrorBiserializer implements JsonDeserializer<AusPostError>
												, JsonSerializer<AusPostError> {

	/**
	 * 
	 */
	public AusPostErrorBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(AusPostError src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonObject	error	= new JsonObject();
		error.addProperty(AusPostError.ATTR_CODE, src.getCode());
		error.addProperty(AusPostError.ATTR_NAME, src.getName());
		error.addProperty(AusPostError.ATTR_MESSAGE, src.getMessage());
		
		JsonElement	contextElem	= context.serialize(src.getContext(), AusPostErrorContext.class);
		error.add(AusPostError.ATTR_CONTEXT, contextElem);

		return error;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public AusPostError deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject	root 		= json.getAsJsonObject();
		
		JsonElement codeElem	= root.get(AusPostError.ATTR_CODE);
		String		code		= codeElem.getAsString();
		
		JsonElement nameElem	= root.get(AusPostError.ATTR_NAME);
		String		name		= nameElem.getAsString();
		
		JsonElement	msgElem		= root.get(AusPostError.ATTR_MESSAGE);
		String		msg			= msgElem.getAsString();
		
		JsonElement		contextElem		= root.get(AusPostError.ATTR_CONTEXT);
		AusPostErrorContext	errorContext	= context.deserialize(contextElem, AusPostErrorContext.class);
				
		return AusPostError.builder().setCode(code)
									  .setName(name)
									  .setMessage(msg)
									  .setContext(errorContext)
									  .build();
	}

}
