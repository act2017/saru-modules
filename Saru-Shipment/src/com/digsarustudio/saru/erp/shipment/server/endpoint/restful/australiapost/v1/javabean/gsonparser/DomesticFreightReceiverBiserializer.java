/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.AustralianPostcode;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.Country;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.State;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.Suburb;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.javabean.PersonalName;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentAddressLines;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentAddressType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentBusinessName;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentDeliveryInstruction;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentEmail;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentPhoneNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AustraliaPostCustomerNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.DomesticFreightReceiver;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse and composite {@link DomesticFreightReceiver} from or into a JSON string for {@link Shipment}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DomesticFreightReceiverBiserializer implements JsonDeserializer<DomesticFreightReceiver>
														  , JsonSerializer<DomesticFreightReceiver> {

	/**
	 * 
	 */
	public DomesticFreightReceiverBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(DomesticFreightReceiver src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonObject	root			= new JsonObject();
		
		JsonElement	nameElem		= context.serialize(src.getName(), PersonalName.class);
		root.add(DomesticFreightReceiver.ATTR_NAME, nameElem);
		
		if( null != src.getAPCN() ) {
			JsonElement	apcnElem		= context.serialize(src.getAPCN(), AustraliaPostCustomerNumber.class);
			root.add(DomesticFreightReceiver.ATTR_APCN, apcnElem);
		}
		
		if( null != src.getBusinessName() ) {
			JsonElement	businessNameElem		= context.serialize(src.getBusinessName(), AusPostShipmentBusinessName.class);
			root.add(DomesticFreightReceiver.ATTR_BUSINESS_NAME, businessNameElem);
		}
		
		if( null != src.getAddressType() ) {
			JsonElement	typeElem		= context.serialize(src.getAddressType(), AusPostShipmentAddressType.class);
			root.add(DomesticFreightReceiver.ATTR_TYPE, typeElem);
		}
		
		JsonElement	linesElem		= context.serialize(src.getAddressLines(), AusPostShipmentAddressLines.class);
		root.add(DomesticFreightReceiver.ATTR_LINES, linesElem);
		
		JsonElement	suburbElem		= context.serialize(src.getSuburb(), Suburb.class);
		root.add(DomesticFreightReceiver.ATTR_SUBURB, suburbElem);
		
		JsonElement	stateElem		= context.serialize(src.getState(), State.class);
		root.add(DomesticFreightReceiver.ATTR_STATE, stateElem);
		
		JsonElement	postcodeElem		= context.serialize(src.getPostcode(), AustralianPostcode.class);
		root.add(DomesticFreightReceiver.ATTR_POSTCODE, postcodeElem);
		
		if( null != src.getCountry() ) {
			JsonElement	countryElem		= context.serialize(src.getCountry(), Country.class);
			root.add(DomesticFreightReceiver.ATTR_COUNRY, countryElem);
		}
		
		if( null != src.getPhone() ) {
			JsonElement	phoneElem		= context.serialize(src.getPhone(), AusPostShipmentPhoneNumber.class);
			root.add(DomesticFreightReceiver.ATTR_PHONE, phoneElem);
		}
		
		if( null != src.getEmail() ) {
			JsonElement	emailElem		= context.serialize(src.getEmail(), AusPostShipmentEmail.class);
			root.add(DomesticFreightReceiver.ATTR_EMAIL, emailElem);
		}
		
		if( null != src.getDeliveryInstructions() ) {
			JsonElement	deliveryInstructionElem		= context.serialize(src.getDeliveryInstructions(), AusPostShipmentDeliveryInstruction.class);
			root.add(DomesticFreightReceiver.ATTR_DELIVERY_INSTRUCTIONS, deliveryInstructionElem);
		}
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public DomesticFreightReceiver deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject					root 				= json.getAsJsonObject();
		
		JsonElement					nameElem			= root.get(DomesticFreightReceiver.ATTR_NAME);
		PersonalName				name				= context.deserialize(nameElem, PersonalName.class);
		
		JsonElement					apcnElem			= root.get(DomesticFreightReceiver.ATTR_APCN);
		AustraliaPostCustomerNumber	apcn				= context.deserialize(apcnElem, AustraliaPostCustomerNumber.class);
		
		JsonElement					businessNameElem	= root.get(DomesticFreightReceiver.ATTR_BUSINESS_NAME);
		AusPostShipmentBusinessName	businessName		= context.deserialize(businessNameElem, AusPostShipmentBusinessName.class);
		
		JsonElement					typeElem			= root.get(DomesticFreightReceiver.ATTR_TYPE);
		AusPostShipmentAddressType	type				= context.deserialize(typeElem, AusPostShipmentAddressType.class);
		
		JsonElement					linesElem			= root.get(DomesticFreightReceiver.ATTR_LINES);
		AusPostShipmentAddressLines	lines				= context.deserialize(linesElem, AusPostShipmentAddressLines.class);
		
		JsonElement					suburbElem			= root.get(DomesticFreightReceiver.ATTR_SUBURB);
		Suburb						suburb				= context.deserialize(suburbElem, Suburb.class);
		
		JsonElement					stateElem			= root.get(DomesticFreightReceiver.ATTR_STATE);
		State						state				= context.deserialize(stateElem, State.class);
		
		JsonElement					postcodeElem		= root.get(DomesticFreightReceiver.ATTR_POSTCODE);
		AustralianPostcode			postcode			= context.deserialize(postcodeElem, AustralianPostcode.class);
		
		JsonElement					countryElem			= root.get(DomesticFreightReceiver.ATTR_COUNRY);
		Country						country				= context.deserialize(countryElem, Country.class);
		
		JsonElement					phoneElem			= root.get(DomesticFreightReceiver.ATTR_PHONE);
		AusPostShipmentPhoneNumber	phone				= context.deserialize(phoneElem, AusPostShipmentPhoneNumber.class);
		
		JsonElement					emailElem			= root.get(DomesticFreightReceiver.ATTR_EMAIL);
		AusPostShipmentEmail		email				= context.deserialize(emailElem, AusPostShipmentEmail.class);
		
		JsonElement							deliveryInstructionElem	= root.get(DomesticFreightReceiver.ATTR_DELIVERY_INSTRUCTIONS);
		AusPostShipmentDeliveryInstruction	deliveryInstruction		= context.deserialize(deliveryInstructionElem, AusPostShipmentDeliveryInstruction.class);
		
		return DomesticFreightReceiver.builder().setName(name)
												.setAPCN(apcn)
												.setBusinessName(businessName)
												.setType(type)
												.setAddressLines(lines)
												.setSuburb(suburb)
												.setState(state)
												.setPostcode(postcode)
												.setCountry(country)
												.setPhone(phone)
												.setEmail(email)
												.setDeliveryInstructions(deliveryInstruction)
												.build();
	}

}
