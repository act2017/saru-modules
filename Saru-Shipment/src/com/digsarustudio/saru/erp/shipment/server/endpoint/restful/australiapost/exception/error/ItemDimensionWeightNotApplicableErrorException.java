/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * The product PRODUCT is a flat-rate product and must not include weight or dimensions.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class ItemDimensionWeightNotApplicableErrorException extends AustraliaPostErrorException {

	/**
	 * 
	 */
	public ItemDimensionWeightNotApplicableErrorException() {

	}

	/**
	 * @param message
	 */
	public ItemDimensionWeightNotApplicableErrorException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public ItemDimensionWeightNotApplicableErrorException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public ItemDimensionWeightNotApplicableErrorException(String message, Throwable cause, AusPostErrorCollection error) {
		super(message, cause, error);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public ItemDimensionWeightNotApplicableErrorException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
