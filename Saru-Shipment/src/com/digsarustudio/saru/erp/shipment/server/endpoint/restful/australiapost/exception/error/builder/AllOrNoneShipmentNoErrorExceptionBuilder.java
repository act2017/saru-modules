/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.AllOrNoneShipmentNoErrorException;

/**
 * The {@link AllOrNoneShipmentNoErrorException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AllOrNoneShipmentNoErrorExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<AllOrNoneShipmentNoErrorException> 
													  implements RESTfulResponseExceptionBuilder<AllOrNoneShipmentNoErrorException> {
		
	/**
	 * 
	 */
	public AllOrNoneShipmentNoErrorExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public AllOrNoneShipmentNoErrorException build() {
		return new AllOrNoneShipmentNoErrorException(this.getMessage(), this.getCause());
	}

}
