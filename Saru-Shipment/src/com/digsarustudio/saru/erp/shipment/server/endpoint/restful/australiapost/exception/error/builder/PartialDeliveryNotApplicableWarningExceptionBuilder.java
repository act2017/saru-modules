/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.PartialDeliveryNotApplicableWarningException;

/**
 * The {@link PartialDeliveryNotApplicableWarningException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PartialDeliveryNotApplicableWarningExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<PartialDeliveryNotApplicableWarningException> 
																implements RESTfulResponseExceptionBuilder<PartialDeliveryNotApplicableWarningException> {		
	/**
	 * 
	 */
	public PartialDeliveryNotApplicableWarningExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public PartialDeliveryNotApplicableWarningException build() {
		return new PartialDeliveryNotApplicableWarningException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
