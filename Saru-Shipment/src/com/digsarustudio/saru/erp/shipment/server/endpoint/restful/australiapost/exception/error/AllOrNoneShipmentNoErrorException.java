/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

/**
 * The request contains shipments with items that are missing barcode ids. 
 * Please add barcode ids to the items where the ids are missing, and resubmit your request.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class AllOrNoneShipmentNoErrorException extends Exception {

	/**
	 * 
	 */
	public AllOrNoneShipmentNoErrorException() {

	}

	/**
	 * @param message
	 */
	public AllOrNoneShipmentNoErrorException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public AllOrNoneShipmentNoErrorException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public AllOrNoneShipmentNoErrorException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public AllOrNoneShipmentNoErrorException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
