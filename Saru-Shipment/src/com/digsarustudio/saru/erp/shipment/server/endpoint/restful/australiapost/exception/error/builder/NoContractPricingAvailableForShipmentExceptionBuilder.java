/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.NoContractPricingAvailableForShipmentException;

/**
 * The {@link NoContractPricingAvailableForShipmentException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class NoContractPricingAvailableForShipmentExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<NoContractPricingAvailableForShipmentException> 
																	implements RESTfulResponseExceptionBuilder<NoContractPricingAvailableForShipmentException> {
		
	/**
	 * 
	 */
	public NoContractPricingAvailableForShipmentExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public NoContractPricingAvailableForShipmentException build() {
		return new NoContractPricingAvailableForShipmentException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
