/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentsRequest;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentCollection;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link ShipmentsRequest} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ShipmentsRequestBiserialiser implements JsonDeserializer<ShipmentsRequest>
														  , JsonSerializer<ShipmentsRequest> {

	/**
	 * 
	 */
	public ShipmentsRequestBiserialiser() {

	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(ShipmentsRequest src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}

		JsonObject	root	 = new JsonObject();
		
		JsonElement	shipmentsElem = context.serialize(src.getShipmentCollection(), ShipmentCollection.class);
		root.add(ShipmentsRequest.ATTR_SHIPMENTS, shipmentsElem);
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public ShipmentsRequest deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject	root = json.getAsJsonObject();
		
		JsonElement	shipmentsElem	= root.get(ShipmentsRequest.ATTR_SHIPMENTS);
		ShipmentCollection shipments = context.deserialize(shipmentsElem, ShipmentCollection.class);		
		
		return ShipmentsRequest.builder().setShipments(shipments)
												.build();
	}

}
