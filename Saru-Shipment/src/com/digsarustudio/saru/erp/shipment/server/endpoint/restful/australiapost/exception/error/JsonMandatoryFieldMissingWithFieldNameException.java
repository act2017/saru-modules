/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * The input request is missing the mandatory field with the name 'FIELD NAME'. 
 * Please resubmit the request including the required fields and values.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class JsonMandatoryFieldMissingWithFieldNameException extends AustraliaPostErrorException {

	/**
	 * 
	 */
	public JsonMandatoryFieldMissingWithFieldNameException() {

	}

	/**
	 * @param message
	 */
	public JsonMandatoryFieldMissingWithFieldNameException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public JsonMandatoryFieldMissingWithFieldNameException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public JsonMandatoryFieldMissingWithFieldNameException(String message, Throwable cause, AusPostErrorCollection error) {
		super(message, cause, error);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public JsonMandatoryFieldMissingWithFieldNameException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
