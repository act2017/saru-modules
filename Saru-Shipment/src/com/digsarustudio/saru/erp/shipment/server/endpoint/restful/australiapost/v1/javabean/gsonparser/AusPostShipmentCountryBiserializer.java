/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.Country;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link Country} from or into a json string for {@link Shipment}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AusPostShipmentCountryBiserializer implements JsonDeserializer<Country>
										  , JsonSerializer<Country> {

	/**
	 * 
	 */
	public AusPostShipmentCountryBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(Country src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonPrimitive country = new JsonPrimitive(src.getCode());
		
		return country;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public Country deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}	

		return Country.builder().setCode(json.getAsString())
							  .build();
	}

	
}
