/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.AustralianPostcode;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.Country;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.State;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.Suburb;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.javabean.PersonalName;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.DomesticFreightSender;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentAddressType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentAddressLines;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentEmail;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentPhoneNumber;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse and composite {@link DomesticFreightSender} to or from a json string for {@link Shipment}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class FreightSenderBiserializer implements JsonDeserializer<DomesticFreightSender>, JsonSerializer<DomesticFreightSender> {

	/**
	 * 
	 */
	public FreightSenderBiserializer() {

	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(DomesticFreightSender src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonObject root	= new JsonObject();
		
		JsonElement	nameElem	= context.serialize(src.getName(), PersonalName.class);
		root.add(DomesticFreightSender.ATTR_NAME, nameElem);
		
		//Optional
		if(null != src.getType()) {
			JsonElement	typeElem	= context.serialize(src.getType(), AusPostShipmentAddressType.class);
			root.add(DomesticFreightSender.ATTR_TYPE, typeElem);
		}
		
		JsonElement	linesElem	= context.serialize(src.getAddressLines(), AusPostShipmentAddressLines.class);
		root.add(DomesticFreightSender.ATTR_LINES, linesElem);
		
		JsonElement	suburbElem	= context.serialize(src.getSuburb(), Suburb.class);
		root.add(DomesticFreightSender.ATTR_SUBURB, suburbElem);
		
		JsonElement	stateElem	= context.serialize(src.getState(), State.class);
		root.add(DomesticFreightSender.ATTR_STATE, stateElem);
		
		JsonElement	postcodeElem= context.serialize(src.getPostcode(), AustralianPostcode.class);
		root.add(DomesticFreightSender.ATTR_POSTCODE, postcodeElem);
		
		//Optional
		if( null != src.getCountry() ) {
			JsonElement	countryElem	= context.serialize(src.getCountry(), Country.class);
			root.add(DomesticFreightSender.ATTR_COUNTRY, countryElem);
		}
		
		//Optional
		if( null != src.getPhone() ) {
			JsonElement	phoneElem	= context.serialize(src.getPhone(), AusPostShipmentPhoneNumber.class);
			root.add(DomesticFreightSender.ATTR_PHONE, phoneElem);
		}
		
		//Optional
		if(null == src.getEmail()) {
			JsonElement emailElem	= context.serialize(src.getEmail(), AusPostShipmentEmail.class);
			root.add(DomesticFreightSender.ATTR_EMAIL, emailElem);
		}		
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public DomesticFreightSender deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject					root		= json.getAsJsonObject();

		JsonElement					nameElem	= root.get(DomesticFreightSender.ATTR_NAME);
		PersonalName				name		= context.deserialize(nameElem, PersonalName.class);
		
		JsonElement					typeElem	= root.get(DomesticFreightSender.ATTR_TYPE);
		AusPostShipmentAddressType	type		= context.deserialize(typeElem, AusPostShipmentAddressType.class);
		
		JsonElement					linesElem	= root.get(DomesticFreightSender.ATTR_LINES);
		AusPostShipmentAddressLines	lines		= context.deserialize(linesElem, AusPostShipmentAddressLines.class);
		
		JsonElement					suburbElem	= root.get(DomesticFreightSender.ATTR_SUBURB);
		Suburb						suburb		= context.deserialize(suburbElem, Suburb.class);
		
		JsonElement					stateElem	= root.get(DomesticFreightSender.ATTR_STATE);
		State						state		= context.deserialize(stateElem, State.class);
		
		JsonElement					postcodeElem= root.get(DomesticFreightSender.ATTR_POSTCODE);
		AustralianPostcode			postcode	= context.deserialize(postcodeElem, AustralianPostcode.class);
		
		JsonElement					countryElem	= root.get(DomesticFreightSender.ATTR_COUNTRY);
		Country						country		= context.deserialize(countryElem, Country.class);
		
		JsonElement					phoneElem	= root.get(DomesticFreightSender.ATTR_PHONE);
		AusPostShipmentPhoneNumber	phone		= context.deserialize(phoneElem, AusPostShipmentPhoneNumber.class);

		JsonElement					emailElem	= root.get(DomesticFreightSender.ATTR_EMAIL);
		AusPostShipmentEmail		email		= context.deserialize(emailElem, AusPostShipmentEmail.class);
		
		
		return DomesticFreightSender.builder().setName(name)
							   .setType(type)
							   .setAddressLines(lines)
							   .setSuburb(suburb)
							   .setState(state)
							   .setPostcode(postcode)
							   .setCountry(country)
							   .setPhone(phone)
							   .setEmail(email)
							   .build();
	}
}
