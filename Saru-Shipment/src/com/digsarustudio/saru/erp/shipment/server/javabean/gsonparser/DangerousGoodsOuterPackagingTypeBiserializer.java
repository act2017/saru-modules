/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsOuterPackagingType;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse and composite {@link DangerousGoodsOuterPackagingType} 
 * from or into a JSON string used by {@link DangerousGoods}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public class DangerousGoodsOuterPackagingTypeBiserializer implements JsonDeserializer<DangerousGoodsOuterPackagingType>
																	,JsonSerializer<DangerousGoodsOuterPackagingType> {

	/**
	 * 
	 */
	public DangerousGoodsOuterPackagingTypeBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(DangerousGoodsOuterPackagingType src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonPrimitive type = new JsonPrimitive(src.getDescription());
		
		return type;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public DangerousGoodsOuterPackagingType deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		String type = json.getAsString();
		
		return DangerousGoodsOuterPackagingType.builder().setDescription(type)
														 .build();
	}

	
}
