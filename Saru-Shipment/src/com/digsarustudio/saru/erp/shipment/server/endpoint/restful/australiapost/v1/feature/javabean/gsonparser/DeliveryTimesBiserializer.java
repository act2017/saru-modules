/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.DeliveryWindowCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean.DeliveryTimes;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link DeliveryTimes} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DeliveryTimesBiserializer implements JsonDeserializer<DeliveryTimes>, JsonSerializer<DeliveryTimes> {

	/**
	 * 
	 */
	public DeliveryTimesBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(DeliveryTimes src, Type typeOfSrc, JsonSerializationContext context) {
		
		JsonElement	attrElem 	= context.serialize(src.getAttribute(), DeliveryWindowCollection.class);
		
		JsonObject	root		= new JsonObject();
		root.add(DeliveryTimes.ATTR_ATTRIBUTES, attrElem);
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public DeliveryTimes deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject 					root 		= json.getAsJsonObject();
		JsonElement					attrElem	= root.get(DeliveryTimes.ATTR_ATTRIBUTES);
		DeliveryWindowCollection	windows		= context.deserialize(attrElem, DeliveryWindowCollection.class);
		
		return (DeliveryTimes) DeliveryTimes.builder().setDeliveryWindows(windows)
									  				  .build();
	}
}
