/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.ServerErrorException;

/**
 * The {@link ServerErrorException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ServerErrorExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<ServerErrorException> 
										   		implements RESTfulResponseExceptionBuilder<ServerErrorException> {
		
	/**
	 * 
	 */
	public ServerErrorExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public ServerErrorException build() {
		return new ServerErrorException(this.getMessage(), this.getCause());
	}

}
