/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.UnableToCalculatePriceException;

/**
 * The {@link UnableToCalculatePriceException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class UnableToCalculatePriceExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<UnableToCalculatePriceException> 
											 		implements RESTfulResponseExceptionBuilder<UnableToCalculatePriceException> {
		
	/**
	 * 
	 */
	public UnableToCalculatePriceExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public UnableToCalculatePriceException build() {
		return new UnableToCalculatePriceException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
