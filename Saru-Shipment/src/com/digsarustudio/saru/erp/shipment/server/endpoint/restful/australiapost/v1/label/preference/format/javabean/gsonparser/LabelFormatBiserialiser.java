/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.format.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelBranded;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormat;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatGroup;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatLayout;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatLeftOffset;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatTopOffset;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link LabelFormat} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelFormatBiserialiser implements JsonDeserializer<LabelFormat>, JsonSerializer<LabelFormat> {

	/**
	 * 
	 */
	public LabelFormatBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(LabelFormat src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonObject		root		= new JsonObject();
		
		JsonElement		groupelem	= context.serialize(src.getGroup(), LabelFormatGroup.class);
		root.add(LabelFormat.ATTR_GROUP, groupelem);
		
		JsonElement		layoutElem	= context.serialize(src.getLayout(), LabelFormatLayout.class);
		root.add(LabelFormat.ATTR_LAYOUT, layoutElem);
		
		JsonElement		brandedElem	= context.serialize(src.getLabelBranded(), LabelBranded.class);
		root.add(LabelFormat.ATTR_BRANDED, brandedElem);
		
		JsonElement		leftOffsetElem	= context.serialize(src.getLeftOffset(), LabelFormatLeftOffset.class);
		root.add(LabelFormat.ATTR_LEFT_OFFSET, leftOffsetElem);
		
		JsonElement		topOffsetElem	= context.serialize(src.getTopOffset(), LabelFormatTopOffset.class);
		root.add(LabelFormat.ATTR_TOP_OFFSET, topOffsetElem);
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public LabelFormat deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject	root	= json.getAsJsonObject();
		
		JsonElement			groupElem	= root.get(LabelFormat.ATTR_GROUP);
		LabelFormatGroup	group		= context.deserialize(groupElem, LabelFormatGroup.class);
		
		JsonElement			layoutElem	= root.get(LabelFormat.ATTR_LAYOUT);
		LabelFormatLayout	layout		= context.deserialize(layoutElem, LabelFormatLayout.class);
		
		JsonElement			brandedElem	= root.get(LabelFormat.ATTR_BRANDED);
		LabelBranded		branded		= context.deserialize(brandedElem, LabelBranded.class);
		
		JsonElement				leftOffsetElem	= root.get(LabelFormat.ATTR_LEFT_OFFSET);
		LabelFormatLeftOffset	leftOffset		= context.deserialize(leftOffsetElem, LabelFormatLeftOffset.class);
		
		JsonElement				topOffsetElem	= root.get(LabelFormat.ATTR_TOP_OFFSET);
		LabelFormatTopOffset	topOffset		= context.deserialize(topOffsetElem, LabelFormatTopOffset.class);

		return LabelFormat.builder().setGroup(group)
									.setLayout(layout)
									.setLabelBranded(branded)
									.setLeftOffset(leftOffset)
									.setTopOffset(topOffset)
									.build();
	}

}
