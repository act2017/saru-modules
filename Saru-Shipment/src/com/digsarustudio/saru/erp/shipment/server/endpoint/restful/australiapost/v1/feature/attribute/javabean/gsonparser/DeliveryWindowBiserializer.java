/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.attribute.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.DeliveryWindow;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Time;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link DeliveryWindow} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DeliveryWindowBiserializer implements JsonDeserializer<DeliveryWindow>
												 , JsonSerializer<DeliveryWindow> {

	/**
	 * 
	 */
	public DeliveryWindowBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(DeliveryWindow src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate() ) {
			return null;
		}

		JsonObject root	 = new JsonObject();
		
		JsonElement	start	= context.serialize(src.getStartTime(), Time.class);
		root.add(DeliveryWindow.ATTR_START, start);
		
		JsonElement	end		= context.serialize(src.getEndTime(), Time.class);
		root.add(DeliveryWindow.ATTR_END, end);		
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public DeliveryWindow deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject	root		= new JsonObject();
		
		JsonElement	startElem	= root.get(DeliveryWindow.ATTR_START);
		Time		start		= context.deserialize(startElem, Time.class);
		
		JsonElement	endElem		= root.get(DeliveryWindow.ATTR_END);
		Time		end			= context.deserialize(endElem, Time.class);
		
		return DeliveryWindow.builder().setStartTime(start)
									   .setEndTime(end)
									   .build();
	}

}
