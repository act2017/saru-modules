/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.SenderReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.SenderReferenceCollection;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse and compile {@link SenderReferenceCollection} to or from a json string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SenderReferenceCollectionBiserializer implements JsonDeserializer<SenderReferenceCollection>
															, JsonSerializer<SenderReferenceCollection> {

	/**
	 * 
	 */
	public SenderReferenceCollectionBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(SenderReferenceCollection src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonArray references = new JsonArray();
		for (com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference reference : src.getSenderReferences()) {
			if(!reference.validate()) {
				continue;
			}
			
			references.add(reference.getReference());
		}
				
		return references;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public SenderReferenceCollection deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		SenderReference[]	references	= null;
		if( json.isJsonArray() ) {
			references	= context.deserialize(json, SenderReference[].class);
		}else {
			references = new SenderReference[1];
			
			String reference = json.getAsString();			
			references[0] 	 = SenderReference.builder().setReference(reference)
														.build();
		}

		return SenderReferenceCollection.builder().setSenderReferences(references)
												  .build();
	}

}
