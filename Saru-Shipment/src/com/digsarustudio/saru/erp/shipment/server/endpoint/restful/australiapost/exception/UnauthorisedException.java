/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception;

/**
 * This exception thrown when a unauthorised(401) returned from remote service.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class UnauthorisedException extends AusPostRESTfulResponseException {

	/**
	 * 
	 */
	public UnauthorisedException() {
		
	}

	/**
	 * @param message
	 */
	public UnauthorisedException(String message) {
		super(message);
		
	}

	/**
	 * @param cause
	 */
	public UnauthorisedException(Throwable cause) {
		super(cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnauthorisedException(String message, Throwable cause) {
		super(message, cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public UnauthorisedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

}
