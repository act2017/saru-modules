/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.environment.sandbox.tog;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.Merchant;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * This object contains the default details of {@link MerchantDetails}<br>
 * <br>
 * Email-address: it@togauto.com.au<br>
 * Name: Otto Hung<br>
 * Key(username): 445a7636-3f8c-4e5f-80c3-56b2877b2e72<br>
 * Password(secret): xf381b346c5edc8a60fc<br>
 * <br>
 * Testbed URL: https://digitalapi.auspost.com.au/test/shipping/v1<br>
 * Testbed URL (for use with PostMan collection): https://digitalapi.auspost.com.au/test<br>
 * <br>
 * Products: eParcel and International<br>
 * Account-Number: 1014608494<br>
 * <br>
 * Products: ST Premium and ST Express<br>
 * Account-Number: 00610494<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AusPostMerchant extends Merchant implements MerchantDetails, IsSerializable {
	private final String ACCOUNT_NO	= "1014608494"; 
	private final String API_KEY	= "445a7636-3f8c-4e5f-80c3-56b2877b2e72";
	private final String PASSWORD	= "xf381b346c5edc8a60fc";

	/**
	 * 
	 */
	public AusPostMerchant() {
		super();
		
		this.setAccountNumber(ACCOUNT_NO);
		this.setAPIKey(API_KEY);
		this.setPassword(PASSWORD);
	}
}
