/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentPagination;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link ShipmentPagination} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ShipmentPaginationBiserialiser implements JsonDeserializer<ShipmentPagination>
													 , JsonSerializer<ShipmentPagination> {

	/**
	 * 
	 */
	public ShipmentPaginationBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(ShipmentPagination src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}

		JsonObject	root = new JsonObject();
		
		JsonPrimitive	totalNo	= new JsonPrimitive(src.getTotalNumberOfRecords());
		root.add(ShipmentPagination.ATTR_TOTAL_NUMBER_OF_RECORDS, totalNo);
		
		JsonPrimitive	recordsPerPage	= new JsonPrimitive(src.getNumberOfRecordsPerPage());
		root.add(ShipmentPagination.ATTR_NUMBER_OF_RECORDS_PER_PAGE, recordsPerPage);
		
		JsonPrimitive	currentPageNo	= new JsonPrimitive(src.getCurrentPageNumber());
		root.add(ShipmentPagination.ATTR_CURRENT_PAGE_NUMBER, currentPageNo);		
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public ShipmentPagination deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}

		JsonObject root = json.getAsJsonObject();
		
		Integer 	totalNo 	= null;
		JsonElement	totalNoElem	= root.get(ShipmentPagination.ATTR_TOTAL_NUMBER_OF_RECORDS);
		if(null != totalNoElem && !totalNoElem.isJsonNull()) {
			totalNo 	= totalNoElem.getAsInt();
		}
		
		Integer 	recordsPerPage 		= null;
		JsonElement	recordsPerPageElem	= root.get(ShipmentPagination.ATTR_NUMBER_OF_RECORDS_PER_PAGE);
		if(null != recordsPerPageElem && !recordsPerPageElem.isJsonNull()) {
			recordsPerPage 		= recordsPerPageElem.getAsInt();
		}
		
		Integer 	currentPageNo 		= null;
		JsonElement	currentPageNoElem	= root.get(ShipmentPagination.ATTR_CURRENT_PAGE_NUMBER);
		if(null != currentPageNoElem && !currentPageNoElem.isJsonNull()) {
			currentPageNo 		= currentPageNoElem.getAsInt();
		}
		
		
		return ShipmentPagination.builder().setTotalNumberOfRecords(totalNo)
										   .setNumberOfRecordsPerPage(recordsPerPage)
										   .setCurrentPageNumber(currentPageNo)
										   .build();
	}

}
