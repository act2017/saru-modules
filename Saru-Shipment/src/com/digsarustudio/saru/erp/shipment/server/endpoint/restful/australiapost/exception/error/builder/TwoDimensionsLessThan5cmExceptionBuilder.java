/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.TwoDimensionsLessThan5cmException;

/**
 * The {@link TwoDimensionsLessThan5cmException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class TwoDimensionsLessThan5cmExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<TwoDimensionsLessThan5cmException> 
													  implements RESTfulResponseExceptionBuilder<TwoDimensionsLessThan5cmException> {
		
	/**
	 * 
	 */
	public TwoDimensionsLessThan5cmExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public TwoDimensionsLessThan5cmException build() {
		return new TwoDimensionsLessThan5cmException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
