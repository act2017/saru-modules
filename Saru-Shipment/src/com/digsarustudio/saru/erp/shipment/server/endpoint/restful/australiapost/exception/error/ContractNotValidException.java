/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * The contract for the charge account with number account number has expired or is not yet valid. 
 * For further assistance please contact your Account Manager or call Australia Post on 13 21 31.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class ContractNotValidException extends AustraliaPostErrorException {

	/**
	 * 
	 */
	public ContractNotValidException() {

	}

	/**
	 * @param message
	 */
	public ContractNotValidException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public ContractNotValidException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public ContractNotValidException(String message, Throwable cause, AusPostErrorCollection error) {
		super(message, cause, error);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public ContractNotValidException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
