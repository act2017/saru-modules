/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * A value for delivery instructions exceeds the maximum limit of 128 characters. 
 * Please reduce the length of the value and submit the request again.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class MaxLengthDeliveryInstructionException extends AustraliaPostErrorException {

	/**
	 * 
	 */
	public MaxLengthDeliveryInstructionException() {

	}

	/**
	 * @param message
	 */
	public MaxLengthDeliveryInstructionException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public MaxLengthDeliveryInstructionException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public MaxLengthDeliveryInstructionException(String message, Throwable cause, AusPostErrorCollection errors) {
		super(message, cause, errors);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public MaxLengthDeliveryInstructionException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
