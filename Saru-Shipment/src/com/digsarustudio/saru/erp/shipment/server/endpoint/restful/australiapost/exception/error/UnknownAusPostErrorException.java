/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * This exception represents the incoming error which has not been recorded and handled in the system.<br>
 * This may take place because of the update of API.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class UnknownAusPostErrorException extends AustraliaPostErrorException {

	/**
	 * 
	 */
	public UnknownAusPostErrorException() {

	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnknownAusPostErrorException(String message, AusPostErrorCollection errors) {
		super(message, errors);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnknownAusPostErrorException(Throwable cause, AusPostErrorCollection errors) {
		super(cause, errors);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnknownAusPostErrorException(String message, Throwable cause, AusPostErrorCollection errors) {
		super(message, cause, errors);

	}
}
