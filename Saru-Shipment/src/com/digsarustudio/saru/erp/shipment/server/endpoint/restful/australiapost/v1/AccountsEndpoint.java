/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.ResponseCallback;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.RESTfulResponseStatusCodes;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.accounts.javabean.gsonparser.OrderSummaryBiserialiser;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.accounts.javabean.OrderSummary;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderId;
import com.google.gson.GsonBuilder;

/**
 * This service is used to get order summary and the details of the merchant account.<br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * 
 * 
 * @param		
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-order-from-shipments">Create Order From Shipments</a><br>
 * 
 */
public class AccountsEndpoint extends AustraliaPostEndpoint<Order, Order> {
	private final String API_PATH 				= "shipping/v1/accounts";
	private final String API_PATH_PARAM_ORDERS	= "orders";
	private final String API_PATH_PARAM_SUMMARY	= "summary";
	
	/**
	 * 
	 * The object builder for {@link AccountsEndpoint}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<AccountsEndpoint> {
		private AccountsEndpoint result = null;

		private Builder() {
			this.result = new AccountsEndpoint();
		}

		/**
		 * 
		 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.AccountsEndpoint#enableSandboxEnvironment()
		 */
		public Builder enableSandboxEnvironment() {
			result.enableSandboxEnvironment();
			return this;
		}

		/**
		 * 
		 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.AccountsEndpoint#enableProductionEnvironment()
		 */
		public Builder enableProductionEnvironment() {
			result.enableProductionEnvironment();
			return this;
		}

		/**
		 * @param details
		 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#setMerchant(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails)
		 */
		public Builder setMerchant(MerchantDetails details) {
			result.setMerchant(details);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AccountsEndpoint build() {
			return this.result;
		}

	}

	private AccountsEndpoint() {

	}
	
	public com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.accounts.OrderSummary getOrderSummary(Order order) throws EndpointException {
		if(null == order || !order.validate()) {
			throw new IllegalArgumentException("The incoming order contains invalid id, please confirm before send it");
		}
		
		return this.getOrderSummary(order.getOrderId());
	}
	
	public com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.accounts.OrderSummary getOrderSummary(OrderId orderId) throws EndpointException {
		if(null == orderId || !orderId.validate()) {
			throw new IllegalArgumentException("The order shipments contains invalid id, please confirm before send it");
		}
		
		return this.getOrderSummary(orderId.getValue());
	}
	
	/**
	 * This service creates an order for the referenced shipments that have previously been created using the Create Shipments service.
	 * 
	 * @param data The shipment for order to create
	 * 
	 * @return The shipment created by Australia Post Create Orders API.
	 * @throws EndpointException 
	 */
	public com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.accounts.OrderSummary getOrderSummary(String orderId) throws EndpointException  {		
		if(null == orderId || orderId.isEmpty()) {
			throw new IllegalArgumentException("The order shipments contains invalid id, please confirm before send it");
		}
		
		this.clear();				
		
		this.setSuccessCode(RESTfulResponseStatusCodes.OK);
		this.setFailureCode(RESTfulResponseStatusCodes.BadRequest);
		
		this.addURLParameter(this.getMerchantDetails().getAccountNumber());
		this.addURLParameter(API_PATH_PARAM_ORDERS);
		this.addURLParameter(orderId);
		this.addURLParameter(API_PATH_PARAM_SUMMARY);		
		
		this.setResponseObjectType(String.class);
		
		String result = this.sendRESTfulGetRequest();
		
//		//Due to U+000A ('controlLF') is not available in this font's encoding: WinAnsiEncoding, replace new line characters.
//		result = result.replace("\n", "").replace("\r", "");
		
		return OrderSummary.builder().setPDFContent(result)
									 .setOrderId(orderId)									 
									 .build();
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#enableSandboxEnvironment()
	 */
	@Override
	public void enableSandboxEnvironment() {
		this.enableSandboxEnvironment(API_PATH);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#enableProductionEnvironment()
	 */
	@Override
	public void enableProductionEnvironment() {
		this.enableProductionEnvironment(API_PATH);
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#insert(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void insert(Order data, ResponseCallback<Order> callback) throws EndpointException {
		throw new UnsupportedOperationException("Insert is not supported yet");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#update(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void update(Order data, ResponseCallback<Order> callback) throws EndpointException {
		throw new UnsupportedOperationException("Update is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#update(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)
	 */
	@Override
	public void update(Order data, ResponseCallback<Order> callback, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("Update is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#list(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void list(Order data, ResponseCallback<CollectionResponse<Order>> callback) throws EndpointException {
		throw new UnsupportedOperationException("List is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#list(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void list(Order data, ResponseCallback<CollectionResponse<Order>> callback, Integer cursor,
			Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("List is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#delete(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void delete(Order data, ResponseCallback<Order> callback) throws EndpointException {
		throw new UnsupportedOperationException("Delete is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#delete(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)
	 */
	@Override
	public void delete(Order data, ResponseCallback<Order> callback, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("Delete is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#get(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void get(Order data, ResponseCallback<Order> callback) throws EndpointException {
		throw new UnsupportedOperationException("Get is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#initGsonAndTypeToken()
	 */
	@Override
	protected void initJsonParserAndTypeToken() {		
		// Due to the format of json string which contains mixed data type, so that providing a deserializer to handle it.
		GsonBuilder builder = new GsonBuilder();
		
		this.updateGetOrderSummaryJsonParse(builder);
				
		this.setJsonParserObject(builder.create());		
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	private void updateGetOrderSummaryJsonParse(GsonBuilder builder) {
		builder.registerTypeAdapter(OrderSummary.class, new OrderSummaryBiserialiser());
	}
}
