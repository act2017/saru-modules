/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.PartialDimensionsInShipmentWarningException;

/**
 * The {@link PartialDimensionsInShipmentWarningException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PartialDimensionsInShipmentWarningExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<PartialDimensionsInShipmentWarningException> 
																implements RESTfulResponseExceptionBuilder<PartialDimensionsInShipmentWarningException> {
		
	/**
	 * 
	 */
	public PartialDimensionsInShipmentWarningExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public PartialDimensionsInShipmentWarningException build() {
		return new PartialDimensionsInShipmentWarningException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
