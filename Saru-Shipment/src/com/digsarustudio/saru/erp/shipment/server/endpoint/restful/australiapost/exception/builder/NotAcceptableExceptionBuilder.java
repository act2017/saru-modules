/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.NotAcceptableException;

/**
 * The {@link NotAcceptableException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class NotAcceptableExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<NotAcceptableException> 
										   implements RESTfulResponseExceptionBuilder<NotAcceptableException> {
		
	/**
	 * 
	 */
	public NotAcceptableExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public NotAcceptableException build() {
		return new NotAcceptableException(this.getMessage(), this.getCause());
	}

}
