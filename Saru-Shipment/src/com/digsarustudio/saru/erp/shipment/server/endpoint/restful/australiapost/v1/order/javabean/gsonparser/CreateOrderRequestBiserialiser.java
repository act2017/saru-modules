/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.order.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.Consignor;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.CreateOrderRequest;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.OrderReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.PaymentMethod;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link CreateOrderRequest} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public class CreateOrderRequestBiserialiser implements JsonDeserializer<CreateOrderRequest>, JsonSerializer<CreateOrderRequest> {

	/**
	 * 
	 */
	public CreateOrderRequestBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(CreateOrderRequest src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}

		JsonObject	root	= new JsonObject();
		
		JsonElement	orderRefElem	= context.serialize(src.getOrderReference(), OrderReference.class);
		root.add(CreateOrderRequest.ATTR_ORDER_REFERENCE, orderRefElem);
		
		JsonElement			paymentMethodElem	= context.serialize(src.getPaymentMethod(), PaymentMethod.class);
		root.add(CreateOrderRequest.ATTR_PAYMENT_METHOD, paymentMethodElem);
		
		JsonElement			consignorElem	= context.serialize(src.getConsignor(), Consignor.class);
		root.add(CreateOrderRequest.ATTR_CONSIGNOR, consignorElem); 
		
		JsonElement		shipmentsElem		= context.serialize(src.getShipments(), ShipmentCollection.class);
		root.add(CreateOrderRequest.ATTR_SHIPMENTS, shipmentsElem);
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public CreateOrderRequest deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject	root	= json.getAsJsonObject();
			
		JsonElement		orderRefElem	= root.get(CreateOrderRequest.ATTR_ORDER_REFERENCE);
		OrderReference	orderRef		= context.deserialize(orderRefElem, OrderReference.class);
		
		JsonElement			consignorElem		= root.get(CreateOrderRequest.ATTR_CONSIGNOR);
		Consignor			consignor			= context.deserialize(consignorElem, Consignor.class);
		
		JsonElement			paymentMethodElem	= root.get(CreateOrderRequest.ATTR_PAYMENT_METHOD);
		PaymentMethod		paymentMethod		= context.deserialize(paymentMethodElem, PaymentMethod.class);
		
		JsonElement			shipmentsElem		= root.get(CreateOrderRequest.ATTR_SHIPMENTS);
		ShipmentCollection	shipments			= context.deserialize(shipmentsElem, ShipmentCollection.class);
		
		return CreateOrderRequest.builder().setOrderReference(orderRef)
										   .setConsignor(consignor)
										   .setPaymentMethod(paymentMethod)
										   .setShipments(shipments)
										   .build();
	}

}
