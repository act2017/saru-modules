/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.DangerousGoods;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsOuterPackagingQuantity;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link DangerousGoodsOuterPackagingQuantity} 
 * from or into a json string of {@link DangerousGoods} used by Australia Post Shipment API.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DangerousGoodsOuterPackagingQuantityBiserializer implements JsonDeserializer<DangerousGoodsOuterPackagingQuantity>
																	   , JsonSerializer<DangerousGoodsOuterPackagingQuantity> {

	/**
	 * 
	 */
	public DangerousGoodsOuterPackagingQuantityBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(DangerousGoodsOuterPackagingQuantity src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonPrimitive quantity = new JsonPrimitive(src.getValueString());
		
		return quantity;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public DangerousGoodsOuterPackagingQuantity deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		String quantity = json.getAsString();
		
		return DangerousGoodsOuterPackagingQuantity.builder().setValue(quantity)
															 .build();
	}

	
}
