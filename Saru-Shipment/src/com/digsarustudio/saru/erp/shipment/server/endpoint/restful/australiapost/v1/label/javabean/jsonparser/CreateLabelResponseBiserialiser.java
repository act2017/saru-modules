/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.CreateLabelResponse;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelGenerationProgressCode;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelGenerationProgressMessage;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link CreateLabelResponse} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class CreateLabelResponseBiserialiser implements JsonDeserializer<CreateLabelResponse>, JsonSerializer<CreateLabelResponse> {

	/**
	 * 
	 */
	public CreateLabelResponseBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(CreateLabelResponse src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonObject		root	= new JsonObject();
		
		JsonElement		msgElem	= context.serialize(src.getGenerationProgressMessage(), LabelGenerationProgressMessage.class);
		root.add(CreateLabelResponse.ATTR_MESSAGE, msgElem);
		
		JsonElement		codeElem	= context.serialize(src.getGenerationProgressCode(), LabelGenerationProgressCode.class);
		root.add(CreateLabelResponse.ATTR_CODE, codeElem);
		
		JsonElement		labelElem	= context.serialize(src.getLabels(), LabelCollection.class);
		root.add(CreateLabelResponse.ATTR_LABELS, labelElem);
		
		JsonElement		errElem	= context.serialize(src.getAusPostErrors(), AusPostErrorCollection.class);
		root.add(CreateLabelResponse.ATTR_ERRORS, errElem);
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public CreateLabelResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject 		root 		= json.getAsJsonObject();
		
		JsonElement						msgElem	= root.get(CreateLabelResponse.ATTR_MESSAGE);
		LabelGenerationProgressMessage	msg		= context.deserialize(msgElem, LabelGenerationProgressMessage.class);

		JsonElement						codeElem	= root.get(CreateLabelResponse.ATTR_CODE);
		LabelGenerationProgressCode		code		= context.deserialize(codeElem, LabelGenerationProgressCode.class);
		
		JsonElement						labelsElem	= root.get(CreateLabelResponse.ATTR_LABELS);
		LabelCollection					labels		= context.deserialize(labelsElem, LabelCollection.class);
		
		JsonElement						errorsElem	= root.get(CreateLabelResponse.ATTR_ERRORS);
		AusPostErrorCollection			errors		= context.deserialize(errorsElem, AusPostErrorCollection.class);
		
		return CreateLabelResponse.builder().setGenerationProgressMessage(msg)
											.setGenerationProgressCode(code)
											.setLabels(labels)
											.setAusPostErrors(errors)
											.build();
	}

}
