/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.NoPricesForProductException;

/**
 * The {@link NoPricesForProductException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class NoPricesForProductExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<NoPricesForProductException> 
											 	implements RESTfulResponseExceptionBuilder<NoPricesForProductException> {
		
	/**
	 * 
	 */
	public NoPricesForProductExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public NoPricesForProductException build() {
		return new NoPricesForProductException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
