/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.ShipmentFeatureAttributeTime;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean.PickupTime;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link PickupTime} from or into a json string. 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PickupTimeBiserializer implements JsonDeserializer<PickupTime>
											   , JsonSerializer<PickupTime> {

	/**
	 * 
	 */
	public PickupTimeBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(PickupTime src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonElement	timeElem	= context.serialize(src.getAttribute(), ShipmentFeatureAttributeTime.class);		
		
		JsonObject	root		= new JsonObject();
		root.add(PickupTime.ATTR_ATTRIBUTES, timeElem);
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public PickupTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject 							root		= json.getAsJsonObject();
		
		JsonElement							attrElem	= root.get(PickupTime.ATTR_ATTRIBUTES);		
		ShipmentFeatureAttributeTime		time		= context.deserialize(attrElem, ShipmentFeatureAttributeTime.class);
		
		return (PickupTime) PickupTime.builder().setTime(time)
									 	 		.build();
	}

}
