/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * Partial delivery is not applicable for this product.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class PartialDeliveryNotApplicableWarningException extends AustraliaPostErrorException {

	/**
	 * 
	 */
	public PartialDeliveryNotApplicableWarningException() {

	}

	/**
	 * @param message
	 */
	public PartialDeliveryNotApplicableWarningException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public PartialDeliveryNotApplicableWarningException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public PartialDeliveryNotApplicableWarningException(String message, Throwable cause, AusPostErrorCollection error) {
		super(message, cause, error);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public PartialDeliveryNotApplicableWarningException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
