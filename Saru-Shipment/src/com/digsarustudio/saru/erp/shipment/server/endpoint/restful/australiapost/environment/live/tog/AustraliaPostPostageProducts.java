/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.environment.live.tog;

import java.util.HashMap;
import java.util.Map;

import com.digsarustudio.saru.erp.shipment.shared.PostageProduct;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The services provided by Australia Post.<br>
 * The details should be provided from Database.<br>
 * <br>
 * The following codes are for TOG:<br>
 * <table>
 * 	<tr>
 * 		<td>Service Code</td><td>Description</td>
 * 	</tr>
 * 	<tr>
 * 		<td>7D85</td><td>PARCEL POST + SIGNATURE</td>
 * 	</tr>
 * 	<tr>
 * 		<td>7J55</td><td>EXPRESS POST + SIGNATURE</td>
 * 	</tr>
 * 	<tr>
 * 		<td>AIR8</td><td>INTL ECONOMY/AIRMAIL PARCELS</td>
 * 	</tr>
 * 	<tr>
 * 		<td>ARI</td><td>INTL RETURNS-STANDARD/AIRMAIL</td>
 * 	</tr>
 * 	<tr>
 * 		<td>CFR</td><td>EPARCEL CALL FOR RETURN</td>
 * 	</tr>
 * 	<tr>
 * 		<td>ECD8</td><td>INTL EXPRESS DOCS/ECI DOCS</td>
 * 	</tr>
 * 	<tr>
 * 		<td>ECM8</td><td>INTL EXPRESS MERCH/ECI MERCH</td>
 * 	</tr>
 * 	<tr>
 * 		<td>ERI</td><td>INTL RETURNS-EXPRESS/ECI</td>
 * 	</tr>
 * 	<tr>
 * 		<td>PR</td><td>EPARCEL POST RETURNS</td>
 * 	</tr>
 * 	<tr>
 * 		<td>PTI7</td><td>INT'L STANDARD WITH SIGNATURE</td>
 * 	</tr>
 * 	<tr>
 * 		<td>PTI8</td><td>INTL STANDARD/PACK & TRACK	N</td>
 * 	</tr>
 * 	<tr>
 * 		<td>RPI8</td><td>INTL ECONOMY W SOD/ REGD POST</td>
 * 	</tr>
 * 	<tr>
 * 		<td>XPR</td><td>EXPRESS EPARCEL POST RETURNS</td>
 * 	</tr>
 * </table>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AustraliaPostPostageProducts implements PostageProduct, IsSerializable {
	public static final AustraliaPostPostageProducts	eParcelStandard						= new AustraliaPostPostageProducts("7D85", "Parcel Post + Signature");
	public static final AustraliaPostPostageProducts	eParcelExpress						= new AustraliaPostPostageProducts("7J55", "Express Post + Signature");
	public static final AustraliaPostPostageProducts	InternationalExpressMerch			= new AustraliaPostPostageProducts("ECM8", "INTL EXPRESS MERCH/ECI MERCH");
	public static final AustraliaPostPostageProducts	InternationalPackAndTrack			= new AustraliaPostPostageProducts("PTI8", "INTL STANDARD/PACK & TRACK");
	public static final AustraliaPostPostageProducts	InternationalStandardWithSignature	= new AustraliaPostPostageProducts("PTI7", "INT'L STANDARD WITH SIGNATURE");

	private static Map<String, AustraliaPostPostageProducts> VALUES = new HashMap<>();
		
	static {
		VALUES.put(eParcelStandard.getValue(), eParcelStandard);
		VALUES.put(eParcelStandard.getValue(), eParcelExpress);
		VALUES.put(eParcelStandard.getValue(), InternationalExpressMerch);
		VALUES.put(eParcelStandard.getValue(), InternationalPackAndTrack);
		VALUES.put(eParcelStandard.getValue(), InternationalStandardWithSignature);
	}	
	
	private String value = null;
	private String display = null;
	
	private AustraliaPostPostageProducts() {
		
	}
	
	private AustraliaPostPostageProducts(String value, String display) {
		this.value = value;
		this.display = display;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public String getDisplayString() {
		return this.display;
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.PostageProduct#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.value = code;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.PostageProduct#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.display = name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.PostageProduct#getCode()
	 */
	@Override
	public String getCode() {
		return this.value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.PostageProduct#getName()
	 */
	@Override
	public String getName() {
		return this.display;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.PostageProduct#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.value || this.value.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AustraliaPostPostageProducts other = (AustraliaPostPostageProducts) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	public static AustraliaPostPostageProducts fromValue(String value) {
		if (null == value ) {
			return null;
		}else if(!VALUES.containsKey(value)) {
			new AustraliaPostPostageProducts(value, "UKNOWN CODE [" + value +"]");
		}
		
		return VALUES.get(value);
	}
	
	public static AustraliaPostPostageProducts getDefault() {
		return eParcelStandard;
	}
}
