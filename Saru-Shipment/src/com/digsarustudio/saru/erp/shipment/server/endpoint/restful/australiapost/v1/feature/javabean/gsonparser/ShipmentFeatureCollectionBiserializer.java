/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean.DeliveryDate;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean.DeliveryTimes;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean.PickupDate;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean.PickupTime;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean.ShipmentFeatureCollection;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link ShipmentFeatureCollection} from or into a json string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ShipmentFeatureCollectionBiserializer implements JsonSerializer<ShipmentFeatureCollection>
															, JsonDeserializer<ShipmentFeatureCollection> {

	/**
	 * 
	 */
	public ShipmentFeatureCollectionBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public ShipmentFeatureCollection deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject		root 				= json.getAsJsonObject();
		
		JsonElement		deliveryDateElem	= root.get(ShipmentFeatureCollection.ATTR_DELIVERY_DATE);
		DeliveryDate	deliveryDate		= null;
		if(null != deliveryDateElem) {
			deliveryDate = context.deserialize(deliveryDateElem, DeliveryDate.class);
		}
		
		JsonElement		deliveryTimesElem	= root.get(ShipmentFeatureCollection.ATTR_DELIVERY_TIMES);
		DeliveryTimes	deliveryTimes		= null;
		if(null != deliveryTimesElem) {
			deliveryTimes = context.deserialize(deliveryTimesElem, DeliveryTimes.class);
		}
		
		JsonElement		pickupDateElem		= root.get(ShipmentFeatureCollection.ATTR_PICKUP_DATE);
		PickupDate		pickupDate			= null;
		if(null != pickupDateElem) {
			pickupDate = context.deserialize(pickupDateElem, PickupDate.class);
		}
		
		JsonElement		pickupTimeElem		= root.get(ShipmentFeatureCollection.ATTR_PICKUP_TIME);
		PickupTime		pickupTime			= null;
		if(null != pickupTimeElem) {
			pickupTime = context.deserialize(pickupTimeElem, PickupTime.class);
		}
		
		return ShipmentFeatureCollection.builder().setDeliveryDate(deliveryDate)
												  .setDeliveryTimes(deliveryTimes)
												  .setPickupDate(pickupDate)
												  .setPickupTime(pickupTime)
												  .build();
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(ShipmentFeatureCollection src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonObject		root				= new JsonObject();
		JsonElement		deliveryDateElem	= context.serialize(src.getDeliveryDate(), DeliveryDate.class);
		if( null != deliveryDateElem ) {
			root.add(ShipmentFeatureCollection.ATTR_DELIVERY_DATE, deliveryDateElem);			
		}
		
		JsonElement		deliveryTimesElem	= context.serialize(src.getDeliveryTimes(), DeliveryTimes.class);
		if(null != deliveryTimesElem) {
			root.add(ShipmentFeatureCollection.ATTR_DELIVERY_TIMES, deliveryTimesElem);
		}
		
		JsonElement		pickupDateElem		= context.serialize(src.getPickupDate(), PickupDate.class);
		if(null != pickupDateElem) {
			root.add(ShipmentFeatureCollection.ATTR_PICKUP_DATE, pickupDateElem);
		}
		
		JsonElement		pickupTimeElem		= context.serialize(src.getPickupTime(), PickupTime.class);
		if(null != pickupTimeElem) {
			root.add(ShipmentFeatureCollection.ATTR_PICKUP_TIME, pickupTimeElem);
		}
		
		return root;
	}
}
