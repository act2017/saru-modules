/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * The postcode POSTCODE submitted for the sender "from" address does not match the lodgement postcode 
 * POSTCODE the contract. Please change the sender "from" postcode to match the lodgement postcode in 
 * the contract and attempt the request again.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class FromPostcodeDoesNotMatchContractOriginPostcodeException extends AustraliaPostErrorException {

	/**
	 * 
	 */
	public FromPostcodeDoesNotMatchContractOriginPostcodeException() {

	}

	/**
	 * @param message
	 */
	public FromPostcodeDoesNotMatchContractOriginPostcodeException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public FromPostcodeDoesNotMatchContractOriginPostcodeException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public FromPostcodeDoesNotMatchContractOriginPostcodeException(String message, Throwable cause, AusPostErrorCollection error) {
		super(message, cause, error);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public FromPostcodeDoesNotMatchContractOriginPostcodeException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
