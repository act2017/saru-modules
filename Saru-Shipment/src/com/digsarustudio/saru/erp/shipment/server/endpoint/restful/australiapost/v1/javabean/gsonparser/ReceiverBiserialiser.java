/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.AustralianPostcode;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.AustralianState;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentAddressLines;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentAddressType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentBusinessName;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentCountry;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentDeliveryInstruction;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentEmail;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentPersonalName;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentPhoneNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentSuburb;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AustraliaPostCustomerNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Receiver;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link Receiver} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ReceiverBiserialiser implements JsonDeserializer<Receiver>, JsonSerializer<Receiver> {

	/**
	 * 
	 */
	public ReceiverBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(Receiver src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;			
		}
		
		JsonObject root	= new JsonObject();
		
		JsonElement	nameElem	= context.serialize(src.getName(), AusPostShipmentPersonalName.class);
		root.add(Receiver.ATTR_NAME, nameElem);
		
		AustraliaPostCustomerNumber	apcn = (AustraliaPostCustomerNumber) src.getAPCN();
		if(null != apcn && apcn.validate()) {
			JsonElement apcnElem = context.serialize(apcn, AustraliaPostCustomerNumber.class);
			root.add(Receiver.ATTR_APCN, apcnElem);
		}
		
		//Business name - optional		
		AusPostShipmentBusinessName	businessName = (AusPostShipmentBusinessName) src.getBusinessName();
		if(null != businessName && businessName.validate()) {
			JsonElement businessNameElem = context.serialize(businessName, AusPostShipmentBusinessName.class);
			root.add(Receiver.ATTR_BUSINESS_NAME, businessNameElem);
		}
		
		AusPostShipmentAddressType type = (AusPostShipmentAddressType) src.getType();
		if(null != type && type.validate()) {
			JsonElement typeElem = context.serialize(type, AusPostShipmentAddressType.class);
			root.add(Receiver.ATTR_TYPE, typeElem);
		}
		
		JsonElement	linesElem		= context.serialize(src.getAddressLines(), AusPostShipmentAddressLines.class);
		root.add(Receiver.ATTR_LINES, linesElem);
		
		JsonElement	suburbElem		= context.serialize(src.getSuburb(), AusPostShipmentSuburb.class);
		root.add(Receiver.ATTR_SUBURB, suburbElem);
		
		JsonElement	stateElem		= context.serialize(src.getState(), AustralianState.class);
		root.add(Receiver.ATTR_STATE, stateElem);
		
		JsonElement	postcodeElem	= context.serialize(src.getPostcode(), AustralianPostcode.class);
		root.add(Receiver.ATTR_POSTCODE, postcodeElem);
		
		AusPostShipmentCountry country = (AusPostShipmentCountry) src.getCountry();
		if(null != country && country.validate()) {
			JsonElement	countryElem = context.serialize(country, AusPostShipmentCountry.class);
			root.add(Receiver.ATTR_COUNTRY, countryElem);
		}
		
		AusPostShipmentPhoneNumber	phone	= (AusPostShipmentPhoneNumber) src.getPhone();
		if(null != phone && phone.validate()) {
			JsonElement phoneElem	= context.serialize(phone, AusPostShipmentPhoneNumber.class);
			root.add(Receiver.ATTR_PHONE, phoneElem);
		}
		
		AusPostShipmentEmail email = (AusPostShipmentEmail) src.getEmail();
		if(null != email && email.validate()) {
			JsonElement emailElem	= context.serialize(email, AusPostShipmentEmail.class);
			root.add(Receiver.ATTR_EMAIL, emailElem);			
		}
		
		AusPostShipmentDeliveryInstruction	deliveryInstruction = (AusPostShipmentDeliveryInstruction) src.getDeliveryInstructions();
		if(null != deliveryInstruction && deliveryInstruction.validate()) {
			JsonElement deliveryInstructionElem = context.serialize(deliveryInstruction, AusPostShipmentDeliveryInstruction.class);
			root.add(Receiver.ATTR_DELIVERY_INSTRUCTION, deliveryInstructionElem);
		}
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public Receiver deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject 		root 		= json.getAsJsonObject();
		
		JsonElement						nameElem	= root.get(Receiver.ATTR_NAME);
		AusPostShipmentPersonalName		name		= context.deserialize(nameElem, AusPostShipmentPersonalName.class);
		
		JsonElement						apcnElem	= root.get(Receiver.ATTR_APCN);
		AustraliaPostCustomerNumber		apcn		= context.deserialize(apcnElem, AustraliaPostCustomerNumber.class);
		
		JsonElement						businessNameElem	= root.get(Receiver.ATTR_BUSINESS_NAME);
		AusPostShipmentBusinessName		businessName		= context.deserialize(businessNameElem, AusPostShipmentBusinessName.class);
				
		JsonElement						typeElem	= root.get(Receiver.ATTR_TYPE);
		AusPostShipmentAddressType		type		= context.deserialize(typeElem, AusPostShipmentAddressType.class);
		
		JsonElement						linesElem	= root.get(Receiver.ATTR_LINES);
		AusPostShipmentAddressLines		lines		= context.deserialize(linesElem, AusPostShipmentAddressLines.class);
		
		JsonElement						suburbElem	= root.get(Receiver.ATTR_SUBURB);
		AusPostShipmentSuburb			suburb		= context.deserialize(suburbElem, AusPostShipmentSuburb.class);
		
		JsonElement						stateElem	= root.get(Receiver.ATTR_STATE);
		AustralianState					state		= context.deserialize(stateElem, AustralianState.class);
		
		JsonElement						postcodeElem= root.get(Receiver.ATTR_POSTCODE);
		AustralianPostcode				postcode	= context.deserialize(postcodeElem, AustralianPostcode.class);
		
		JsonElement						countryElem	= root.get(Receiver.ATTR_COUNTRY);
		AusPostShipmentCountry			country		= context.deserialize(countryElem, AusPostShipmentCountry.class);
		
		JsonElement						phoneElem	= root.get(Receiver.ATTR_PHONE);
		AusPostShipmentPhoneNumber		phone		= context.deserialize(phoneElem, AusPostShipmentPhoneNumber.class);
		
		JsonElement						emailElem	= root.get(Receiver.ATTR_EMAIL);
		AusPostShipmentEmail			email		= context.deserialize(emailElem, AusPostShipmentEmail.class);
		
		//Delivery Instruction
		JsonElement							deliveryInstructionElem	= root.get(Receiver.ATTR_DELIVERY_INSTRUCTION);
		AusPostShipmentDeliveryInstruction	deliveryInstruction		= context.deserialize(deliveryInstructionElem, AusPostShipmentDeliveryInstruction.class);

		return Receiver.builder().setName(name)
								 .setAPCN(apcn)
								 .setBusinessName(businessName)
							     .setType(type)
							     .setAddressLines(lines)
							     .setSuburb(suburb)
							     .setState(state)
							     .setPostcode(postcode)
							     .setCountry(country)
							     .setPhone(phone)
							     .setEmail(email)
							     .setDeliveryInstructions(deliveryInstruction)							     
							     .build();
	}

}
