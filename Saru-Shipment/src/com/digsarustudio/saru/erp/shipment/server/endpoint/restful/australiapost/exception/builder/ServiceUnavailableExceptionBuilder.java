/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.ServiceUnavailabeException;

/**
 * The {@link ServiceUnavailabeException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ServiceUnavailableExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<ServiceUnavailabeException> 
										   		implements RESTfulResponseExceptionBuilder<ServiceUnavailabeException> {
		
	/**
	 * 
	 */
	public ServiceUnavailableExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public ServiceUnavailabeException build() {
		return new ServiceUnavailabeException(this.getMessage(), this.getCause());
	}

}
