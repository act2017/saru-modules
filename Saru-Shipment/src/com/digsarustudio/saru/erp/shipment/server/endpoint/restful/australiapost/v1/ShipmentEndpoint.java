/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.server.contactInfo.address.javabean.jsonparser.AustralianPostcodeBiserializer;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.AustralianPostcode;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.ResponseCallback;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.QueryParameter;
import com.digsarustudio.saru.erp.pac.server.endpoint.australiapost.jsonparser.AustralianStateBiserializer;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.AustralianState;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.RESTfulResponseStatusCodes;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.attribute.javabean.gsonparser.DeliveryWindowBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.attribute.javabean.gsonparser.DeliveryWindowCollectionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.attribute.javabean.gsonparser.ShipmentFeatureAttributeDateBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.attribute.javabean.gsonparser.ShipmentFeatureAttributeTimeBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.javabean.gsonparser.DeliveryDateBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.javabean.gsonparser.DeliveryTimesBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.javabean.gsonparser.PickupDateBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.javabean.gsonparser.PickupTimeBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.javabean.gsonparser.ShipmentFeatureCollectionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.content.javabean.gsonparser.CountryOfOriginBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.content.javabean.gsonparser.ItemContentDescriptionBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.content.javabean.gsonparser.ItemContentQuantityBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.content.javabean.gsonparser.ItemContentWeightBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.content.javabean.gsonparser.TariffCodeBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.gsonparser.ItemFeatureTransitCoverAttributeCollectionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.gsonparser.TransitCoverAmountBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.gsonparser.TransitCoverIncludedBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.gsonparser.TransitCoverMaximumBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.gsonparser.TransitCoverRateBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.javabean.gsonparser.ItemFeatureBundledBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.javabean.gsonparser.ItemFeatureCollectionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.javabean.gsonparser.ItemFeaturePriceBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.javabean.gsonparser.ItemFeatureTransitCoverBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.javabean.gsonparser.ItemFeatureTypeBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.CertificateNumberBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.CommentBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.CubicVolumeBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.DescriptionOfOtherBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ExportDeclarationNumberBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.GoodsClassificationTypeBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ImportReferenceNumberBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.InvoiceNumberBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemCollectionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemContentCollectionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemDescriptionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemHeightBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemIdBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemLengthBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemReferenceBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemSummaryBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemWeightBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemWidthBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.LicenceNumberBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.NonDeliveryActionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ProductIdBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ReasonForReturnBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.tracking.javabean.gsonparser.ArticleIdBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.tracking.javabean.gsonparser.BarcodeIdBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.tracking.javabean.gsonparser.ConsignmentIdBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.tracking.javabean.gsonparser.TrackingDetailsBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ATLNumberBiSerializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorCollectionBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorContextBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorContextPairBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorsResponseBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostShipmentAddressLinesBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostShipmentAddressTypeBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostShipmentBusinessNameBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostShipmentCountryBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostShipmentDeliveryInstructionBiSerializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostShipmentEmailBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostShipmentPersonalNameBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostShipmentPhoneNumberBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostShipmentSuburbBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AustraliaPostCustomerNumberBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AuthorisationNumberBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.DangerousGoodsBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.DateBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.DateTimeBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.DiscountBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.FreightChargeBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.FuelSurchargeBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.GoodsDescriptionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.GoodsDescriptionCollectionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.MovementTypeBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.OrderShipmentItemStatusBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.PackagingTypeBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.PackingGroupDesignatorBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ReceiverBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.SecuritySurchargeBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.SenderBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.SenderReferenceBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.SenderReferenceCollectionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ShipmentBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ShipmentCollectionBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ShipmentIdBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ShipmentOrderIdBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ShipmentPaginationBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ShipmentReferenceBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ShipmentSummaryBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ShipmentsRequestBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ShipmentsResponseBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.TotalCostBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.TotalGstBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.TrackingSummaryBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.TrackingSummaryPairBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.TransitCoverBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.LabelBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.LabelGenerationRequestStatusBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.LabelURLBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.javabean.gsonparser.DangerousGoodsClassDivisionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.javabean.gsonparser.DangerousGoodsNetWeightBiserializer;
import com.digsarustudio.saru.erp.shipment.server.javabean.gsonparser.DangerousGoodsOuterPackagingQuantityBiserializer;
import com.digsarustudio.saru.erp.shipment.server.javabean.gsonparser.DangerousGoodsOuterPackagingTypeBiserializer;
import com.digsarustudio.saru.erp.shipment.server.javabean.gsonparser.DangerousGoodsSubsidiaryRiskBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.javabean.gsonparser.DangerousGoodsTechnicalNameBiserializer;
import com.digsarustudio.saru.erp.shipment.server.javabean.gsonparser.UnNumberBiserializer;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.OrderShipmentItemStatuses;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.DeliveryWindow;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.DeliveryWindowCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.ShipmentFeatureAttributeDate;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.ShipmentFeatureAttributeTime;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean.DeliveryDate;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean.DeliveryTimes;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean.PickupDate;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean.PickupTime;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean.ShipmentFeatureCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.javabean.CountryOfOrigin;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.javabean.ItemContentDescription;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.javabean.ItemContentQuantity;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.javabean.ItemContentWeight;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.javabean.TariffCode;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.ItemFeaturePrice;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.ItemFeatureTransitCoverAttributeCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverAmount;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverIncluded;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverMaximum;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverRate;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureBundled;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureTransitCover;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.CertificateNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Comment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.CubicVolume;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.DescriptionOfOther;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ExportDeclarationNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.GoodsClassificationType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ImportReferenceNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.InvoiceNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Item;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemContentCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemDescription;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemHeight;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemLength;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemSummary;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemWeight;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemWidth;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.LicenceNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.NonDeliveryAction;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ProductId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ReasonForReturn;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.javabean.ArticleId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.javabean.BarcodeId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.javabean.ConsignmentId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.javabean.TrackingDetails;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ATLNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentAddressLines;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentAddressType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentBusinessName;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentCountry;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentDeliveryInstruction;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentEmail;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentPersonalName;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentPhoneNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentSuburb;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AustraliaPostCustomerNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AuthorisationNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentsResponse;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.DangerousGoods;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Date;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Discount;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorContext;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorContextPair;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorsResponse;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostError;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.FreightCharge;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.FuelSurcharge;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.GoodsDescription;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.GoodsDescriptionCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.MovementType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentOrderId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.OrderShipmentItemStatus;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.PackagingType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.PackingGroupDesignator;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Receiver;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.SecuritySurcharge;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Sender;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.SenderReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentPagination;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentSummary;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentsRequest;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TotalCost;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TotalGst;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TrackingSummaryPair;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TrackingSummary;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TransitCover;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.Label;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelGenerationRequestStatus;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelURL;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsClassDivision;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsNetWeight;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsOuterPackagingQuantity;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsOuterPackagingType;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsSubsidiaryRisk;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsTechnicalName;
import com.digsarustudio.saru.erp.shipment.shared.javabean.UnNumber;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gwt.thirdparty.guava.common.reflect.TypeToken;

/**
 * This service creates, query, update, and delete a shipment with items and returns a summary of the pricing for the items.
 * <h3>Resource information</h3>
 * <table>
 * 	<tr>
 * 		<td>Rate Limited?</td><td>No</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Requests per rate limit window</td><td>-</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Authentication</td><td>Secure API Key</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Response Formats</td><td>json</td>
 * 	</tr>
 * 	<tr>
 * 		<td>HTTP Methods</td><td>POST</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Resource family</td><td>Shipments</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Response object</td><td>Shipments</td>
 * 	</tr>
 * 	<tr>
 * 		<td>API Version</td><td>v1.0</td>
 * 	</tr>
 * </table>
 * 
 * <h3>Service capabilities</h3>
 * This interface creates a shipment with items and returns a summary of the pricing for the items. A shipment is a set of items associated to a charge account’s location to be delivered to the one physical address.
 * 
 * <h3>Resource URL</h3>
 * <a href="https://digitalapi.auspost.com.au/shipping/v1/shipments">https://digitalapi.auspost.com.au/shipping/v1/shipments</a>
 * 
 * <h3>Headers</h3>
 * <table>
 * 	<tr>
 * 		<td>authentication (required)</td><td>Your username and password as a HTTP Basic Auth hash. See REST and Authentication.</td>
 * 	</tr>
 * 	<tr>
 * 		<td>account-number (required)</td><td>A 10 digit number for an Australia Post charge account, or an 8 digit number for a StarTrack account. See REST and Authentication.</td>
 * 	</tr>
 * </table>
 * <br>
 * 
 * <h3>Parameters</h3>
 * 	<li>
 * 		<ul>Get Shipment
 * 
 * 			<table>
 * 				<tr>
 * 					<td>shipment_ids (Optional)</td><td>A comma separated list of shipment ids</td>
 * 				</tr>
 * 				<tr>
 * 					<td>offset (Optional)</td><td>The offset parameter value specifies the numeric value of the first record to be returned in the result, beginning at 0. If you specify the offset parameter, you must also specify the number_of_shipments parameter.</td>
 * 				</tr>
 * 				<tr>
 * 					<td>number_of_shipments (Optional)</td><td>The number_of_shipments parameter specifies the number of records to return in the result. If you specify the number_of_shipments parameter, you must also specify the offset parameter.</td>
 * 				</tr>
 * 				<tr>
 * 					<td>status (Optional)</td><td>Setting the status parameter will result in the returned list containing only shipments that are at the given status. The correct status names are provided on the <a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/statuses">statuses page</a>. To use a value such as "In transit", simply replace the space with a +.The parameter will accept one or more status values separated by a comma. E.g. status=Created,Initiated</td>
 * 				</tr>
 * 				<tr>
 * 					<td>despatch_date (Optional)</td><td>Setting the despatch_date parameter will result in the returned list containing only shipments that have one of the values as despatch_date. The parameter will accept one or more despatch_date values in the format of yyyy-MM-dd separated by a comma. E.g. despatch_date=2016-04-04,2015-12-31</td>
 * 				</tr>
 * 				<tr>
 * 					<td>sender_reference (Optional)</td><td>Setting the sender_reference parameter will result in the returned list containing only shipments that have one of the values as sender_reference.The parameter will accept one or more sender_reference values separated by a comma. E.g. sender_reference=MYREF-12345</td>
 * 				</tr>
 * 			</table>
 * 		</ul>
 * </li>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * 
 * 
 * @param		
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a><br>
 * 				<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/get-shipments">Get Shipments</a>
 */
public class ShipmentEndpoint extends AustraliaPostEndpoint<Shipment, Shipment> {
	
	//====== Get Shipment =====
	/**
	 * [Optional]<br>
	 * A comma separated list of shipment ids<br>
	 * <br>
	 * Type: parameter<br>
	 */
	public static final String	PARAM_SHIPMENT_ID			= "shipment_ids";
	
	/**
	 * [Optional]<br>
	 * The offset parameter value specifies the numeric value of the first record to be returned in the result, beginning at 0. 
	 * If you specify the offset parameter, you must also specify the number_of_shipments parameter.<br>
	 * <br>
	 * Type: parameter<br>
	 */
	public static final String	PARAM_OFFSET				= "offset";
	
	/**
	 * [Optional]<br>
	 * The number_of_shipments parameter specifies the number of records to return in the result. 
	 * If you specify the number_of_shipments parameter, you must also specify the offset parameter.<br>
	 * <br>
	 * Type: parameter<br>
	 */
	public static final String	PARAM_NUMBER_OF_SHIPMENTS	= "number_of_shipments";
	
	/**
	 * [Optional]<br>
	 * Setting the status parameter will result in the returned list containing only shipments that are at the given status. 
	 * The correct status names are provided on the <a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/statuses">statuses page</a>. 
	 * To use a value such as "In transit", simply replace the space with a +.
	 * The parameter will accept one or more status values separated by a comma. E.g. status=Created,Initiated<br>
	 * <br>
	 * Type: parameter<br>
	 */
	public static final String	PARAM_STATUS				= "status";
	
	/**
	 * [Optional]<br>
	 * Setting the despatch_date parameter will result in the returned list containing only shipments that have one of the values as despatch_date. 
	 * The parameter will accept one or more despatch_date values in the format of yyyy-MM-dd separated by a comma. E.g. despatch_date=2016-04-04,2015-12-31<br>
	 * <br>
	 * Type: parameter<br>
	 */
	public static final String	PARAM_DESPATCH_DATE			= "despatch_date";
	
	/**
	 * [Optional]<br>
	 * Setting the sender_reference parameter will result in the returned list containing only shipments that have one of the values as sender_reference.
	 * The parameter will accept one or more sender_reference values separated by a comma. E.g. sender_reference=MYREF-12345<br>
	 * <br>
	 * Type: parameter<br>
	 */
	public static final String	PARAM_SENDER_REFERENCE		= "sender_reference";
	
	
	private final String API_PATH = "shipping/v1/shipments";
	private final String SENDER_REFERENCE_DELIMITER	= ",";
	
	/**
	 * 
	 * The object builder for {@link ShipmentEndpoint}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<ShipmentEndpoint> {
		private ShipmentEndpoint result = null;

		private Builder() {
			this.result = new ShipmentEndpoint();
		}

		/**
		 * 
		 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#enableSandboxEnvironment()
		 */
		public Builder enableSandboxEnvironment() {
			result.enableSandboxEnvironment();
			return this;
		}

		/**
		 * 
		 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint#enableProductionEnvironment()
		 */
		public Builder enableProductionEnvironment() {
			result.enableProductionEnvironment();
			return this;
		}

		/**
		 * @param details
		 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#setMerchant(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails)
		 */
		public Builder setMerchant(MerchantDetails details) {
			result.setMerchant(details);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ShipmentEndpoint build() {
			return this.result;
		}

	}

	private ShipmentEndpoint() {

	}
	
	/**
	 * This service creates a shipment with items and returns a summary of the pricing for the items.<br>
	 * 
	 * @param data The shipment to create
	 * 
	 * @return The shipment created by Australia Post Create Shipment API.
	 * @throws EndpointException 
	 */
	public Shipment create(Shipment data) throws EndpointException  {
		if(null == data || !data.validate()) {
			throw new IllegalArgumentException("The incoming shipment contains invalid data, please confirm before send it");
		}
		
		this.clear();
		
		ShipmentsRequest request = ShipmentsRequest.builder().addShipment(data)
															 .build();
		
		this.setSuccessCode(RESTfulResponseStatusCodes.Created);
		this.setFailureCode(RESTfulResponseStatusCodes.BadRequest);
		this.setResponseObjectType(ShipmentsResponse.class);
		
		this.setMediaContent(request);
		
		ShipmentsResponse response = this.sendRESTfulPostRequest();
		
		if(null == response) {
			return null;
		}
		
		com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection shipments = response.getShipmentCollection();
		if(null == shipments) {
			return null;
		}
		
		return shipments.getShipment(0);
	}
	
	/**
	 * This service retrieves the information against shipments and the items contained within for shipments created using the Create Shipments service.<br>
	 * 
	 * @param data The shipment to get.
	 * 
	 * @return The shipment fetched by Australia Post Get Shipment API.
	 * @throws EndpointException 
	 */
	public Shipment get(Shipment data) throws EndpointException {
		if( null == data ) {
			return null;
		}
		
		com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentId id = data.getShipmentId();
		if(null == id || !id.validate()) {
			return null;
		}

		this.clear();
		
		this.setSuccessCode(RESTfulResponseStatusCodes.OK);
		this.setFailureCode(RESTfulResponseStatusCodes.BadRequest);
		this.setResponseObjectType(ShipmentsResponse.class);
		
		this.addQueryParameter(QueryParameter.builder().setKey(PARAM_SHIPMENT_ID)
													   .setValue(id.getValue())
													   .build());
		
		ShipmentsResponse response = this.sendRESTfulGetRequest();
		
		if(null == response) {
			return null;
		}
		
		com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection shipments = response.getShipmentCollection();
		if(null == shipments) {
			return null;
		}
		
		return shipments.getShipment(0);
	}	
	
	/**
	 * This service retrieves the information against shipments and the items contained within for shipments created using the Create Shipments service.<br>
	 * 
	 * @param offset 			The offset parameter value specifies the numeric value of the first record to be returned in the result, beginning at 0. If you specify the offset parameter, you must also specify the number_of_shipments parameter.
	 * @param noOfShipments 	The number_of_shipments parameter specifies the number of records to return in the result. If you specify the number_of_shipments parameter, you must also specify the offset parameter.
	 * 
	 * @return					The results of shipments match the query criteria
	 * @throws EndpointException 
	 */
	public ShipmentCollection list(Integer offset, Integer noOfShipments) throws EndpointException {
		return this.list(offset, noOfShipments, null, null, null);
	}	
	
	/**
	 * This service retrieves the information against shipments and the items contained within for shipments created using the Create Shipments service.<br>
	 * 
	 * @param offset 			The offset parameter value specifies the numeric value of the first record to be returned in the result, beginning at 0. If you specify the offset parameter, you must also specify the number_of_shipments parameter.
	 * @param noOfShipments 	The number_of_shipments parameter specifies the number of records to return in the result. If you specify the number_of_shipments parameter, you must also specify the offset parameter.
	 * @param status			Setting the status parameter will result in the returned list containing only shipments that are at the given status. The correct status names are provided on the <a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/statuses">statuses page</a>. To use a value such as "In transit", simply replace the space with a +.The parameter will accept one or more status values separated by a comma. E.g. status=Created,Initiated
	 * 
	 * @return					The results of shipments match the query criteria
	 * @throws EndpointException 
	 */
	public ShipmentCollection list(Integer offset, Integer noOfShipments, OrderShipmentItemStatuses status) throws EndpointException {
		return this.list(offset, noOfShipments, status, null, null);
	}	
	
	/**
	 * This service retrieves the information against shipments and the items contained within for shipments created using the Create Shipments service.<br>
	 * 
	 * @param offset 			The offset parameter value specifies the numeric value of the first record to be returned in the result, beginning at 0. If you specify the offset parameter, you must also specify the number_of_shipments parameter.
	 * @param noOfShipments 	The number_of_shipments parameter specifies the number of records to return in the result. If you specify the number_of_shipments parameter, you must also specify the offset parameter.
	 * @param despatchDate		Setting the despatch_date parameter will result in the returned list containing only shipments that have one of the values as despatch_date. The parameter will accept one or more despatch_date values in the format of yyyy-MM-dd separated by a comma. E.g. despatch_date=2016-04-04,2015-12-31
	 * 
	 * @return					The results of shipments match the query criteria
	 * @throws EndpointException 
	 */
	public ShipmentCollection list(Integer offset, Integer noOfShipments
								, com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date despatchDate) throws EndpointException {
		return this.list(offset, noOfShipments, null, despatchDate, null);
	}	
	
	/**
	 * This service retrieves the information against shipments and the items contained within for shipments created using the Create Shipments service.<br>
	 * 
	 * @param offset 			The offset parameter value specifies the numeric value of the first record to be returned in the result, beginning at 0. If you specify the offset parameter, you must also specify the number_of_shipments parameter.
	 * @param noOfShipments 	The number_of_shipments parameter specifies the number of records to return in the result. If you specify the number_of_shipments parameter, you must also specify the offset parameter.
	 * @param senderReferences	Setting the sender_reference parameter will result in the returned list containing only shipments that have one of the values as sender_reference.The parameter will accept one or more sender_reference values separated by a comma. E.g. sender_reference=MYREF-12345
	 * 
	 * @return					The results of shipments match the query criteria
	 * @throws EndpointException 
	 */
	public ShipmentCollection list(Integer offset, Integer noOfShipments, SenderReferenceCollection senderReferences) throws EndpointException {
		return this.list(offset, noOfShipments, null, null, senderReferences);
	}	
	
	/**
	 * This service retrieves the information against shipments and the items contained within for shipments created using the Create Shipments service.<br>
	 * 
	 * @param offset 			The offset parameter value specifies the numeric value of the first record to be returned in the result, beginning at 0. If you specify the offset parameter, you must also specify the number_of_shipments parameter.
	 * @param noOfShipments 	The number_of_shipments parameter specifies the number of records to return in the result. If you specify the number_of_shipments parameter, you must also specify the offset parameter.
	 * @param status			Setting the status parameter will result in the returned list containing only shipments that are at the given status. The correct status names are provided on the <a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/statuses">statuses page</a>. To use a value such as "In transit", simply replace the space with a +.The parameter will accept one or more status values separated by a comma. E.g. status=Created,Initiated
	 * @param senderReferences	Setting the sender_reference parameter will result in the returned list containing only shipments that have one of the values as sender_reference.The parameter will accept one or more sender_reference values separated by a comma. E.g. sender_reference=MYREF-12345
	 * 
	 * @return					The results of shipments match the query criteria
	 * @throws EndpointException 
	 */	
	public ShipmentCollection list(Integer offset, Integer noOfShipments, OrderShipmentItemStatuses status
								 , SenderReferenceCollection senderReferences) throws EndpointException {
		return this.list(offset, noOfShipments, status, null, senderReferences);
	}	
	
	/**
	 * This service retrieves the information against shipments and the items contained within for shipments created using the Create Shipments service.<br>
	 * 
	 * @param offset 			The offset parameter value specifies the numeric value of the first record to be returned in the result, beginning at 0. If you specify the offset parameter, you must also specify the number_of_shipments parameter.
	 * @param noOfShipments 	The number_of_shipments parameter specifies the number of records to return in the result. If you specify the number_of_shipments parameter, you must also specify the offset parameter.
	 * @param status			Setting the status parameter will result in the returned list containing only shipments that are at the given status. The correct status names are provided on the <a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/statuses">statuses page</a>. To use a value such as "In transit", simply replace the space with a +.The parameter will accept one or more status values separated by a comma. E.g. status=Created,Initiated
	 * @param despatchDate		Setting the despatch_date parameter will result in the returned list containing only shipments that have one of the values as despatch_date. The parameter will accept one or more despatch_date values in the format of yyyy-MM-dd separated by a comma. E.g. despatch_date=2016-04-04,2015-12-31
	 * 
	 * @return					The results of shipments match the query criteria
	 * @throws EndpointException 
	 */
	
	public ShipmentCollection list(Integer offset, Integer noOfShipments, OrderShipmentItemStatuses status
								 , com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date despatchDate) throws EndpointException {
		return this.list(offset, noOfShipments, status, despatchDate, null);
	}	
	
	/**
	 * This service retrieves the information against shipments and the items contained within for shipments created using the Create Shipments service.<br>
	 * 
	 * @param offset 			The offset parameter value specifies the numeric value of the first record to be returned in the result, beginning at 0. If you specify the offset parameter, you must also specify the number_of_shipments parameter.
	 * @param noOfShipments 	The number_of_shipments parameter specifies the number of records to return in the result. If you specify the number_of_shipments parameter, you must also specify the offset parameter.
	 * @param status			Setting the status parameter will result in the returned list containing only shipments that are at the given status. The correct status names are provided on the <a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/statuses">statuses page</a>. To use a value such as "In transit", simply replace the space with a +.The parameter will accept one or more status values separated by a comma. E.g. status=Created,Initiated
	 * @param despatchDate		Setting the despatch_date parameter will result in the returned list containing only shipments that have one of the values as despatch_date. The parameter will accept one or more despatch_date values in the format of yyyy-MM-dd separated by a comma. E.g. despatch_date=2016-04-04,2015-12-31
	 * @param senderReferences	Setting the sender_reference parameter will result in the returned list containing only shipments that have one of the values as sender_reference.The parameter will accept one or more sender_reference values separated by a comma. E.g. sender_reference=MYREF-12345
	 * 
	 * @return					The results of shipments match the query criteria
	 * @throws EndpointException 
	 */
	public ShipmentCollection list(Integer offset, Integer noOfShipments, OrderShipmentItemStatuses status
								 , com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date despatchDate, SenderReferenceCollection senderReferences) throws EndpointException {
		if(null == offset || 0 > offset) {
			throw new IllegalArgumentException("offset must be set to fetch shipments");
		}
		
		if(null == noOfShipments || 0 > noOfShipments) {
			throw new IllegalArgumentException("number of shipments must be set to fetch shipments");
		}
		
		this.clear();
		
		this.setSuccessCode(RESTfulResponseStatusCodes.OK);
		this.setFailureCode(RESTfulResponseStatusCodes.BadRequest);
		this.setResponseObjectType(ShipmentsResponse.class);
		
		this.addQueryParameter(PARAM_OFFSET, offset.toString());
		this.addQueryParameter(PARAM_NUMBER_OF_SHIPMENTS, noOfShipments.toString());
		
		if(null != status) {
			this.addQueryParameter(PARAM_STATUS, status.getValue());
		}
		
		if(null != despatchDate) {
			this.addQueryParameter(PARAM_DESPATCH_DATE, despatchDate.getDate());
		}
		
		if(null != senderReferences) {
			StringBuffer buffer = new StringBuffer();
			Integer count = 0;
			for (com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference reference : senderReferences.getSenderReferences()) {
				if( 0 < count++) {
					buffer.append(SENDER_REFERENCE_DELIMITER);
				}
				
				buffer.append(reference.getReference());
			}
			
			this.addQueryParameter(PARAM_SENDER_REFERENCE, buffer.toString());
		}
		
		ShipmentsResponse response = this.sendRESTfulGetRequest();
		
		if(null == response) {
			return null;
		}
		
		ShipmentCollection shipments = response.getShipmentCollection();
		
		return shipments;
	}
	
	/**
	 * This service updates an existing shipment that has previously been created using the Create Shipment service.<br>
	 * 
	 * @param data The details of shipment to update.<br>
	 * @throws EndpointException 
	 */
	public void update(Shipment data) throws EndpointException {		
		if(null == data || !data.validate() || null == data.getShipmentId() || !data.getShipmentId().validate()) {
			throw new IllegalArgumentException("The incoming shipment contains invalid data, please confirm before send it");
		}
		
		this.clear();
		
		this.setMediaContent(data, com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.class);
		
		this.setSuccessCode(RESTfulResponseStatusCodes.Created);
		this.setFailureCode(RESTfulResponseStatusCodes.BadRequest);
		
		this.addURLParameter(data.getShipmentId().getValue());
		
		//No payload returned
		this.sendRESTfulPutRequest();
	}
	
	/**
	 * This service deletes an existing shipment(s) that has previously been created using the Create Shipment service.
	 * 
	 * @param data The details of shipment to delete.<br>
	 * @throws EndpointException 
	 */
	public void delete(Shipment data) throws EndpointException {
		if(null == data || null == data.getShipmentId() || !data.getShipmentId().validate()) {
			throw new IllegalArgumentException("the shipment id must be set to update the shipment");
		}
		
		this.clear();
		
		this.setSuccessCode(RESTfulResponseStatusCodes.OK);
		this.setFailureCode(RESTfulResponseStatusCodes.NotFound);
		
		this.addURLParameter(data.getShipmentId().getValue());
		
		this.sendRESTfulDeleteRequest();		
	}
	
	/**
	 * This service deletes an existing shipment(s) that has previously been created using the Create Shipment service.
	 * 
	 * @param data The details of shipment to delete.<br>
	 * @throws EndpointException 
	 */
	public void delete(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentId id) throws EndpointException {
		if(null == id || !id.validate()) {
			throw new IllegalArgumentException("the shipment id must be set to update the shipment");
		}
		
		this.delete(id.getValue());
	}
	
	/**
	 * This service deletes an existing shipment(s) that has previously been created using the Create Shipment service.
	 * 
	 * @param data The details of shipment to delete.<br>
	 * @throws EndpointException 
	 */
	public void delete(String id) throws EndpointException {
		if(null == id || id.isEmpty()) {
			throw new IllegalArgumentException("the shipment id must be set to update the shipment");
		}
		
		this.setSuccessCode(RESTfulResponseStatusCodes.OK);
		this.setFailureCode(RESTfulResponseStatusCodes.NotFound);
		
		this.addURLParameter(id);
		
		//No payload returned
		this.sendRESTfulDeleteRequest();		
	}
	
	
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#enableSandboxEnvironment()
	 */
	@Override
	public void enableSandboxEnvironment() {
		this.enableSandboxEnvironment(API_PATH);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#enableProductionEnvironment()
	 */
	@Override
	public void enableProductionEnvironment() {
		this.enableProductionEnvironment(API_PATH);
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#insert(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void insert(Shipment data, ResponseCallback<Shipment> callback) throws EndpointException {
		Gson gson = new Gson();
		String dataString = gson.toJson(data);
		
		this.setMediaContent(dataString);
		this.sendRESTfulPostRequest(new ResponseCallback<ShipmentsResponse>() {

			@Override
			public void onSuccess(ShipmentsResponse result) {
				if( null == result || null == result.getShipments() || result.getShipments().isEmpty() ) {
					return;
				}
				
				callback.onSuccess((Shipment) result.getShipment(0));
			}

			@Override
			public void onFailure(Throwable caught) {
				callback.onFailure(caught);
			}
		});
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#update(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void update(Shipment data, ResponseCallback<Shipment> callback) throws EndpointException {
		throw new UnsupportedOperationException("Update is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#update(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)
	 */
	@Override
	public void update(Shipment data, ResponseCallback<Shipment> callback, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("Update is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#list(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void list(Shipment data, ResponseCallback<CollectionResponse<Shipment>> callback) throws EndpointException {
		throw new UnsupportedOperationException("List is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#list(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void list(Shipment data, ResponseCallback<CollectionResponse<Shipment>> callback, Integer cursor,
			Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("List is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#delete(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void delete(Shipment data, ResponseCallback<Shipment> callback) throws EndpointException {
		throw new UnsupportedOperationException("Delete is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#delete(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)
	 */
	@Override
	public void delete(Shipment data, ResponseCallback<Shipment> callback, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("Delete is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#get(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void get(Shipment data, ResponseCallback<Shipment> callback) throws EndpointException {
		throw new UnsupportedOperationException("Get is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#initGsonAndTypeToken()
	 */
	@SuppressWarnings("serial")
	@Override
	protected void initJsonParserAndTypeToken() {
		this.setResponseObjectType(new TypeToken<ShipmentsResponse>(){}.getType());
		
		// Due to the format of json string which contains mixed data type, so that providing a deserializer to handle it.
		GsonBuilder builder = new GsonBuilder();
		
		//Create Shipments Response
		builder.registerTypeAdapter(ShipmentsResponse.class, new ShipmentsResponseBiserialiser());
		builder.registerTypeAdapter(ShipmentsRequest.class, new ShipmentsRequestBiserialiser());
		
		//Shipment collection
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentCollection.class, new ShipmentCollectionBiserialiser());
				
		//Shipment
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.class, new ShipmentBiserialiser());
		
		//Shipment Reference
		builder.registerTypeAdapter(ShipmentReference.class, new ShipmentReferenceBiserializer());
		
		//Sender References
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.SenderReferenceCollection.class, new SenderReferenceCollectionBiserializer());
		builder.registerTypeAdapter(SenderReference.class, new SenderReferenceBiserializer());
		
		//Goods Descriptions
		builder.registerTypeAdapter(GoodsDescriptionCollection.class, new GoodsDescriptionCollectionBiserializer());
		builder.registerTypeAdapter(GoodsDescription.class, new GoodsDescriptionBiserializer());
		
		//Despatch Date
		builder.registerTypeAdapter(Date.class, new DateBiserializer());
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.DateTime.class, new DateTimeBiserialiser());
		
		//From
		builder.registerTypeAdapter(Sender.class, new SenderBiserialiser());
		builder.registerTypeAdapter(AusPostShipmentPersonalName.class, new AusPostShipmentPersonalNameBiserialiser());
		builder.registerTypeAdapter(AusPostShipmentAddressType.class, new AusPostShipmentAddressTypeBiserialiser());
		builder.registerTypeAdapter(AusPostShipmentAddressLines.class, new AusPostShipmentAddressLinesBiserializer());
		builder.registerTypeAdapter(AusPostShipmentSuburb.class, new AusPostShipmentSuburbBiserialiser());
		builder.registerTypeAdapter(AustralianState.class, new AustralianStateBiserializer());
		builder.registerTypeAdapter(AusPostShipmentPhoneNumber.class, new AusPostShipmentPhoneNumberBiserializer());
		builder.registerTypeAdapter(AusPostShipmentCountry.class, new AusPostShipmentCountryBiserialiser());
		builder.registerTypeAdapter(AustralianPostcode.class, new AustralianPostcodeBiserializer());
		builder.registerTypeAdapter(AusPostShipmentEmail.class, new AusPostShipmentEmailBiserializer());
		
		//To
		builder.registerTypeAdapter(Receiver.class, new ReceiverBiserialiser());
		builder.registerTypeAdapter(AustraliaPostCustomerNumber.class, new AustraliaPostCustomerNumberBiserializer());
		builder.registerTypeAdapter(AusPostShipmentBusinessName.class, new AusPostShipmentBusinessNameBiserialiser());
		builder.registerTypeAdapter(AusPostShipmentDeliveryInstruction.class, new AusPostShipmentDeliveryInstructionBiSerializer());
		
		//Dangerous Goods
		builder.registerTypeAdapter(DangerousGoods.class, new DangerousGoodsBiserializer());
		builder.registerTypeAdapter(UnNumber.class, new UnNumberBiserializer());
		builder.registerTypeAdapter(DangerousGoodsTechnicalName.class, new DangerousGoodsTechnicalNameBiserializer());
		builder.registerTypeAdapter(DangerousGoodsNetWeight.class, new DangerousGoodsNetWeightBiserializer());
		builder.registerTypeAdapter(DangerousGoodsClassDivision.class, new DangerousGoodsClassDivisionBiserializer());
		builder.registerTypeAdapter(DangerousGoodsSubsidiaryRisk.class, new DangerousGoodsSubsidiaryRiskBiserialiser());
		builder.registerTypeAdapter(PackingGroupDesignator.class, new PackingGroupDesignatorBiserializer());
		builder.registerTypeAdapter(DangerousGoodsOuterPackagingType.class, new DangerousGoodsOuterPackagingTypeBiserializer());
		builder.registerTypeAdapter(DangerousGoodsOuterPackagingQuantity.class, new DangerousGoodsOuterPackagingQuantityBiserializer());
		
		//Movement Type
		builder.registerTypeAdapter(MovementType.class, new MovementTypeBiserialiser());
		
		//Features
		builder.registerTypeAdapter(ShipmentFeatureCollection.class, new ShipmentFeatureCollectionBiserializer());
		builder.registerTypeAdapter(DeliveryDate.class, new DeliveryDateBiserializer());
		builder.registerTypeAdapter(DeliveryTimes.class, new DeliveryTimesBiserializer());
		builder.registerTypeAdapter(PickupDate.class, new PickupDateBiserializer());
		builder.registerTypeAdapter(PickupTime.class, new PickupTimeBiserializer());
		builder.registerTypeAdapter(ShipmentFeatureAttributeDate.class, new ShipmentFeatureAttributeDateBiserializer());
		builder.registerTypeAdapter(ShipmentFeatureAttributeTime.class, new ShipmentFeatureAttributeTimeBiserializer());
		builder.registerTypeAdapter(DeliveryWindow.class, new DeliveryWindowBiserializer());
		builder.registerTypeAdapter(DeliveryWindowCollection.class, new DeliveryWindowCollectionBiserializer());
		
		//Authorisation Number
		builder.registerTypeAdapter(AuthorisationNumber.class, new AuthorisationNumberBiserializer());
		
		//Items
		builder.registerTypeAdapter(ItemCollection.class, new ItemCollectionBiserializer());
		builder.registerTypeAdapter(Item.class, new ItemBiserialiser());
		builder.registerTypeAdapter(ItemId.class, new ItemIdBiserialiser());
		builder.registerTypeAdapter(ItemReference.class, new ItemReferenceBiserializer());
		builder.registerTypeAdapter(ProductId.class, new ProductIdBiserializer());
		builder.registerTypeAdapter(ItemDescription.class, new ItemDescriptionBiserializer());
		builder.registerTypeAdapter(ItemLength.class, new ItemLengthBiserializer());
		builder.registerTypeAdapter(ItemWidth.class, new ItemWidthBiserializer());
		builder.registerTypeAdapter(ItemHeight.class, new ItemHeightBiserializer());
		builder.registerTypeAdapter(CubicVolume.class, new CubicVolumeBiserializer());
		builder.registerTypeAdapter(ItemWeight.class, new ItemWeightBiserialiser());
		builder.registerTypeAdapter(ReasonForReturn.class, new ReasonForReturnBiserializer());
		builder.registerTypeAdapter(PackagingType.class, new PackagingTypeBiserializer());
		builder.registerTypeAdapter(ATLNumber.class, new ATLNumberBiSerializer());
			//Features
		builder.registerTypeAdapter(ItemFeatureCollection.class, new ItemFeatureCollectionBiserializer());
		builder.registerTypeAdapter(ItemFeatureTransitCover.class, new ItemFeatureTransitCoverBiserializer());
		builder.registerTypeAdapter(ItemFeatureTransitCoverAttributeCollection.class, new ItemFeatureTransitCoverAttributeCollectionBiserializer());
		builder.registerTypeAdapter(TransitCoverAmount.class, new TransitCoverAmountBiserializer());
		builder.registerTypeAdapter(ItemFeatureBundled.class, new ItemFeatureBundledBiserializer());
		builder.registerTypeAdapter(ItemFeatureType.class, new ItemFeatureTypeBiserializer());
		builder.registerTypeAdapter(ItemFeaturePrice.class, new ItemFeaturePriceBiserialiser());
		
			//Tracking Details
		builder.registerTypeAdapter(TrackingDetails.class, new TrackingDetailsBiserializer());
		builder.registerTypeAdapter(ConsignmentId.class, new ConsignmentIdBiserializer());
		builder.registerTypeAdapter(ArticleId.class, new ArticleIdBiserializer());
		builder.registerTypeAdapter(BarcodeId.class, new BarcodeIdBiserializer());
		
		//Export declaration number
		builder.registerTypeAdapter(ExportDeclarationNumber.class, new ExportDeclarationNumberBiserializer());
		
		//Import reference number
		builder.registerTypeAdapter(ImportReferenceNumber.class, new ImportReferenceNumberBiserializer());
		
		//Goods classification type
		builder.registerTypeAdapter(GoodsClassificationType.class, new GoodsClassificationTypeBiserializer());
		
		//Description of other
		builder.registerTypeAdapter(DescriptionOfOther.class, new DescriptionOfOtherBiserializer());
		
		//Non delivery action
		builder.registerTypeAdapter(NonDeliveryAction.class, new NonDeliveryActionBiserializer());
		
		//Certificate number
		builder.registerTypeAdapter(CertificateNumber.class, new CertificateNumberBiserializer());
		
		//Licence number
		builder.registerTypeAdapter(LicenceNumber.class, new LicenceNumberBiserializer());
		
		//Invoice Number
		builder.registerTypeAdapter(InvoiceNumber.class, new InvoiceNumberBiserializer());
		
		//Comment
		builder.registerTypeAdapter(Comment.class, new CommentBiserializer());
		
		//Item Contents
		builder.registerTypeAdapter(ItemContentCollection.class, new ItemContentCollectionBiserializer());
		builder.registerTypeAdapter(ItemContentDescription.class, new ItemContentDescriptionBiserialiser());
		builder.registerTypeAdapter(ItemContentQuantity.class, new ItemContentQuantityBiserialiser());
		builder.registerTypeAdapter(ItemContentWeight.class, new ItemContentWeightBiserialiser());
		builder.registerTypeAdapter(TariffCode.class, new TariffCodeBiserialiser());
		builder.registerTypeAdapter(CountryOfOrigin.class, new CountryOfOriginBiserialiser());
		
		//==== Response object ====
		//Shipment id
		builder.registerTypeAdapter(ShipmentId.class, new ShipmentIdBiserialiser());
		
		//Shipment creation date
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.DateTime.class, new DateTimeBiserialiser());
		
		//Items
		builder.registerTypeAdapter(ItemSummary.class, new ItemSummaryBiserialiser());
		
			//features
		builder.registerTypeAdapter(TransitCoverIncluded.class, new TransitCoverIncludedBiserialiser());
		builder.registerTypeAdapter(TransitCoverRate.class, new TransitCoverRateBiserialiser());
		builder.registerTypeAdapter(TransitCoverMaximum.class, new TransitCoverMaximumBiserialiser());
		
			//item summary
		builder.registerTypeAdapter(TotalCost.class, new TotalCostBiserialiser());
		builder.registerTypeAdapter(TotalGst.class, new TotalGstBiserialiser());
		builder.registerTypeAdapter(OrderShipmentItemStatus.class, new OrderShipmentItemStatusBiserialiser());
		
		//Shipment summary
		builder.registerTypeAdapter(ShipmentSummary.class, new ShipmentSummaryBiserialiser());
		builder.registerTypeAdapter(OrderShipmentItemStatus.class, new OrderShipmentItemStatusBiserialiser());
		builder.registerTypeAdapter(TrackingSummary.class, new TrackingSummaryBiserialiser());
		builder.registerTypeAdapter(TrackingSummaryPair.class, new TrackingSummaryPairBiserialiser());
		builder.registerTypeAdapter(FreightCharge.class, new FreightChargeBiserialiser());
		builder.registerTypeAdapter(Discount.class, new DiscountBiserialiser());
		builder.registerTypeAdapter(TransitCover.class, new TransitCoverBiserialiser());
		builder.registerTypeAdapter(SecuritySurcharge.class, new SecuritySurchargeBiserialiser());
		builder.registerTypeAdapter(FuelSurcharge.class, new FuelSurchargeBiserialiser());
		
		this.updateGetShipmentJsonBiserialiser(builder);
		this.updateErrorMessageBiserialiser(builder);
		
		this.setJsonParserObject(builder.create());		
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	private void updateGetShipmentJsonBiserialiser(GsonBuilder builder) {
		//order id
		builder.registerTypeAdapter(ShipmentOrderId.class, new ShipmentOrderIdBiserialiser());
		
		//item feature bundled
		builder.registerTypeAdapter(ItemFeatureBundled.class, new ItemFeatureBundledBiserializer());
		
		//label
		builder.registerTypeAdapter(Label.class, new LabelBiserialiser());
		builder.registerTypeAdapter(LabelURL.class, new LabelURLBiserialiser());
		builder.registerTypeAdapter(LabelGenerationRequestStatus.class, new LabelGenerationRequestStatusBiserialiser());
		//label creation date are updated by #updateShipment()
		
		//Errors are updated by #updateErrorMessageBiserialiser()
		
		//pagination
		builder.registerTypeAdapter(ShipmentPagination.class, new ShipmentPaginationBiserialiser());
	}
	
	private void updateErrorMessageBiserialiser(GsonBuilder builder) {
		//Error Response
		builder.registerTypeAdapter(AusPostErrorsResponse.class, new AusPostErrorsResponseBiserialiser());
		builder.registerTypeAdapter(AusPostErrorCollection.class, new AusPostErrorCollectionBiserialiser());
		builder.registerTypeAdapter(AusPostError.class, new AusPostErrorBiserializer());
		builder.registerTypeAdapter(AusPostErrorContext.class, new AusPostErrorContextBiserialiser());
		builder.registerTypeAdapter(AusPostErrorContextPair.class, new AusPostErrorContextPairBiserialiser());
	}
}
