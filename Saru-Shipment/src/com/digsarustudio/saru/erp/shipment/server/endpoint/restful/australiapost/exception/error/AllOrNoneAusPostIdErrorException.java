/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * The request contains shipments with items that are missing a tracking identifier field. 
 * Please ensure that each item contains values for the fields consignment_id, article_id, and barcode_id, 
 * and resubmit your request.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class AllOrNoneAusPostIdErrorException extends AustraliaPostErrorException {

	/**
	 * 
	 */
	public AllOrNoneAusPostIdErrorException() {

	}

	/**
	 * @param message
	 */
	public AllOrNoneAusPostIdErrorException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public AllOrNoneAusPostIdErrorException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public AllOrNoneAusPostIdErrorException(String message, Throwable cause, AusPostErrorCollection error) {
		super(message, cause, error);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public AllOrNoneAusPostIdErrorException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
