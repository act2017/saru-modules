/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.AccountNotFoundException;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.UnknownAusPostErrorException;

/**
 * The {@link AccountNotFoundException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class UnknownAusPostErrorExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<UnknownAusPostErrorException> 
											 	 implements RESTfulResponseExceptionBuilder<UnknownAusPostErrorException> {
		
	/**
	 * 
	 */
	public UnknownAusPostErrorExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public UnknownAusPostErrorException build() {
		return new UnknownAusPostErrorException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
