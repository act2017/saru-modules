/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.header;

import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.BaseHttpHeaderField;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.HttpHeaderField;

/**
 * The account number of merchant used for the HTTP headers.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AccountNumber extends BaseHttpHeaderField implements HttpHeaderField {
	private static final String HEADER = "Account-Number";
	
	/**
	 * 
	 * The object builder for {@link HttpHeaderField}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements HttpHeaderField.Builder {
		private AccountNumber result = null;

		private Builder() {
			this.result = new AccountNumber();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.endpoint.http.header.HttpHeaderField.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public HttpHeaderField build() {
			return this.result;
		}

	}
		
	/**
	 * @param header
	 */
	public AccountNumber() {
		super(HEADER);
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
