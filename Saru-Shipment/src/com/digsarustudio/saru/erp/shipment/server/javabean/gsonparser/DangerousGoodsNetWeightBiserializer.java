/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsNetWeight;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link DangerousGoodsNetWeight} from or into a json string by Australia Post API.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DangerousGoodsNetWeightBiserializer implements JsonDeserializer<DangerousGoodsNetWeight>
														  , JsonSerializer<DangerousGoodsNetWeight> {

	/**
	 * 
	 */
	public DangerousGoodsNetWeightBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(DangerousGoodsNetWeight src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonPrimitive weight = new JsonPrimitive(src.getValue());

		return weight;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public DangerousGoodsNetWeight deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}

		String weight = json.getAsString();
		
		return DangerousGoodsNetWeight.builder().setValue(weight)
												.build();
	}

	
}
