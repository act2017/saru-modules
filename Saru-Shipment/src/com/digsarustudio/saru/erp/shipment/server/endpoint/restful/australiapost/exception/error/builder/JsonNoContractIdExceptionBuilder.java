/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.JsonNoContractIdException;

/**
 * The {@link JsonNoContractIdException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class JsonNoContractIdExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<JsonNoContractIdException> 
											  implements RESTfulResponseExceptionBuilder<JsonNoContractIdException> {
		
	/**
	 * 
	 */
	public JsonNoContractIdExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public JsonNoContractIdException build() {
		return new JsonNoContractIdException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
