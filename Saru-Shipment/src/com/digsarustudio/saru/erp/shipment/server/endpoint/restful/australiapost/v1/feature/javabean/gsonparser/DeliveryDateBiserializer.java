/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.ShipmentFeatureAttributeDate;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean.DeliveryDate;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link DeliveryDate} from or into a json string. 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DeliveryDateBiserializer implements JsonDeserializer<DeliveryDate>
											   , JsonSerializer<DeliveryDate> {

	/**
	 * 
	 */
	public DeliveryDateBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(DeliveryDate src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonElement	dateElem	= context.serialize(src.getAttribute(), ShipmentFeatureAttributeDate.class);		
		
		JsonObject	root		= new JsonObject();
		root.add(DeliveryDate.ATTR_ATTRIBUTES, dateElem);
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public DeliveryDate deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject root = json.getAsJsonObject();
		
		JsonElement							attrElem	= root.get(DeliveryDate.ATTR_ATTRIBUTES);		
		ShipmentFeatureAttributeDate		date		= context.deserialize(attrElem, ShipmentFeatureAttributeDate.class);
		
		return (DeliveryDate) DeliveryDate.builder().setDate(date)
									 	 			.build();
	}

}
