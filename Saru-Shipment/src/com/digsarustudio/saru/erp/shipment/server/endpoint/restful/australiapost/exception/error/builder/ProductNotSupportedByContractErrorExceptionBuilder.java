/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.ProductNotSupportedByContractErrorException;

/**
 * The {@link ProductNotSupportedByContractErrorException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ProductNotSupportedByContractErrorExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<ProductNotSupportedByContractErrorException> 
																implements RESTfulResponseExceptionBuilder<ProductNotSupportedByContractErrorException> {
		
	/**
	 * 
	 */
	public ProductNotSupportedByContractErrorExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public ProductNotSupportedByContractErrorException build() {
		return new ProductNotSupportedByContractErrorException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
