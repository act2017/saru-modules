/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception;

/**
 * This exception thrown when a service unavailable(503) returned from remote service.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class ServiceUnavailabeException extends AusPostRESTfulResponseException {

	/**
	 * 
	 */
	public ServiceUnavailabeException() {
		
	}

	/**
	 * @param message
	 */
	public ServiceUnavailabeException(String message) {
		super(message);
		
	}

	/**
	 * @param cause
	 */
	public ServiceUnavailabeException(Throwable cause) {
		super(cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ServiceUnavailabeException(String message, Throwable cause) {
		super(message, cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public ServiceUnavailabeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

}
