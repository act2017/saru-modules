/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.DangerousGoodsNotSupportedByProductErrorException;

/**
 * The {@link DangerousGoodsNotSupportedByProductErrorException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DangerousGoodsNotSupportedByProductErrorExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<DangerousGoodsNotSupportedByProductErrorException> 
											 						  implements RESTfulResponseExceptionBuilder<DangerousGoodsNotSupportedByProductErrorException> {
		
	/**
	 * 
	 */
	public DangerousGoodsNotSupportedByProductErrorExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public DangerousGoodsNotSupportedByProductErrorException build() {
		return new DangerousGoodsNotSupportedByProductErrorException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
