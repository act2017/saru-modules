/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost;

import com.digsarustudio.banana.encryption.EncryptionTool;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails;

/**
 * The merchant used in back-end service running with Java.<br>
 * The API key will be encoded into Base64 format when the client code needs it.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class Merchant implements MerchantDetails {
	/**
	 * 
	 * The object builder for {@link MerchantDetails}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements MerchantDetails.Builder {
		private MerchantDetails result = null;

		private Builder() {
			this.result = new Merchant();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails.Builder#setAccountNumber(java.lang.String)
		 */
		@Override
		public Builder setAccountNumber(String number) {
			this.result.setAccountNumber(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails.Builder#setAPIKey(java.lang.String)
		 */
		@Override
		public Builder setAPIKey(String key) {
			this.result.setAPIKey(key);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails.Builder#setPassword(java.lang.String)
		 */
		@Override
		public Builder setPassword(String password) {
			this.result.setPassword(password);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public MerchantDetails build() {
			return this.result;
		}

	}
	
	private String accountNo	= null;
	private String apiKey		= null;
	private String password		= null;

	/**
	 * 
	 */
	protected Merchant() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails#setAccountNumber(java.lang.String)
	 */
	@Override
	public void setAccountNumber(String number) {
		this.accountNo = number;
	}

	/* 
	 * (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails#setAPIKey(java.lang.String)
	 */
	@Override
	public void setAPIKey(String key) {
		this.apiKey = key;		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails#getAccountNumber()
	 */
	@Override
	public String getAccountNumber() {
		
		return this.accountNo;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails#getAPIKey()
	 */
	@Override
	public String getAPIKey() {		
		return this.apiKey;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails#setPassword(java.lang.String)
	 */
	@Override
	public void setPassword(String password) {
		this.password = password;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails#getPassword()
	 */
	@Override
	public String getPassword() {
		return this.password;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails#getBase64Authorisation()
	 */
	@Override
	public String getBase64Authorisation() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(this.apiKey);
		buffer.append(PASSWORD_DELIMITER);
		buffer.append(this.password);
		
		return EncryptionTool.toBase64String(buffer.toString());
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
