/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost;

import java.util.HashMap;
import java.util.Map;

import com.digsarustudio.banana.utils.StringFormatter;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.UnknownRESTfulResponseException;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BadRequestExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.NotAcceptableExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.NotFoundExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.PreconditionFailedExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.ServerErrorExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.ServiceUnavailableExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.UnauthorisedExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * This singleton HTTP response status codes and exceptions manager provides the map of status codes and exceptions.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class RESTfulResponseExceptions {
	private static RESTfulResponseExceptions INSTANCE 	= null;
	
	private Map<Integer, RESTfulResponseExceptionBuilder<?>>		exceptions	= null;
	
	/**
	 * 
	 */
	private RESTfulResponseExceptions() {
	}

	public static RESTfulResponseExceptions getInstance() {
		if(null == INSTANCE) {
			INSTANCE = new RESTfulResponseExceptions();
			INSTANCE.buildReplationship();			
		}
		
		return INSTANCE;
	}
	
	public Throwable getException(RESTfulResponseStatusCodes code, AusPostErrorCollection incomingErrs) {
		return this.getException(code.getCode(), incomingErrs);
	}
	
	public Throwable getException(Integer code, AusPostErrorCollection incomingErrs) {
		if(!this.exceptions.containsKey(code)) {
			String message = StringFormatter.format("Unknown RESTful Response Code[%d]", code);
			return new UnknownRESTfulResponseException(message);
		}
		
		RESTfulResponseExceptionBuilder<?> builder = this.exceptions.get(code);
		
		RESTfulResponseStatusCodes statusCode = RESTfulResponseStatusCodes.fromCode(code);
		String msg = null;
		if(null != statusCode) {
			String desc = statusCode.getDescription();
			
			if(null != incomingErrs) {
				StringBuffer buffer = new StringBuffer();
				for (AusPostError err : incomingErrs.getErrors()) {
					buffer.append(StringFormatter.format("%s[%s]: %s", err.getName(), err.getCode(), err.getMessage()));
				}
				
				desc = buffer.toString();
			}
			
			msg = StringFormatter.format("%s[%s]: %s", statusCode.getName(), statusCode.getCode().toString(), desc);
		}else if (null != incomingErrs && incomingErrs.validate()){
			StringBuffer buffer = new StringBuffer();
			for (AusPostError err : incomingErrs.getErrors()) {
				buffer.append(StringFormatter.format("%s[%s]: %s", err.getName(), err.getCode(), err.getMessage()));
			}
			
			msg = buffer.toString();
		}else {
			msg = StringFormatter.format("Unknown status code (%d)", code);
		}		
		
		return builder.setMessage(msg)
					  .setErrors(incomingErrs)
					  .build();
	}
	
	private void buildReplationship() {
		this.exceptions = new HashMap<Integer, RESTfulResponseExceptionBuilder<?>>();
		
		this.exceptions.put(RESTfulResponseStatusCodes.BadRequest.getCode(), new BadRequestExceptionBuilder());
		this.exceptions.put(RESTfulResponseStatusCodes.Unauthorised.getCode(), new UnauthorisedExceptionBuilder());
		this.exceptions.put(RESTfulResponseStatusCodes.NotFound.getCode(), new NotFoundExceptionBuilder());
		this.exceptions.put(RESTfulResponseStatusCodes.NotAcceptable.getCode(), new NotAcceptableExceptionBuilder());
		this.exceptions.put(RESTfulResponseStatusCodes.PreconditionFailed.getCode(), new PreconditionFailedExceptionBuilder());
		this.exceptions.put(RESTfulResponseStatusCodes.ServiceUnavailable.getCode(), new ServiceUnavailableExceptionBuilder());
		this.exceptions.put(RESTfulResponseStatusCodes.ServerError.getCode(), new ServerErrorExceptionBuilder());
	}
}
