/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1;


import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.server.contactInfo.address.javabean.jsonparser.AustralianPostcodeBiserializer;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.AustralianPostcode;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.ResponseCallback;
import com.digsarustudio.saru.erp.pac.server.endpoint.australiapost.jsonparser.AustralianStateBiserializer;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.AustralianState;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.RESTfulResponseStatusCodes;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.attribute.javabean.gsonparser.DeliveryWindowBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.attribute.javabean.gsonparser.DeliveryWindowCollectionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.attribute.javabean.gsonparser.ShipmentFeatureAttributeDateBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.attribute.javabean.gsonparser.ShipmentFeatureAttributeTimeBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.javabean.gsonparser.DeliveryDateBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.javabean.gsonparser.DeliveryTimesBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.javabean.gsonparser.PickupDateBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.javabean.gsonparser.PickupTimeBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.javabean.gsonparser.ShipmentFeatureCollectionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.content.javabean.gsonparser.CountryOfOriginBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.content.javabean.gsonparser.ItemContentDescriptionBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.content.javabean.gsonparser.ItemContentQuantityBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.content.javabean.gsonparser.ItemContentWeightBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.content.javabean.gsonparser.TariffCodeBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.gsonparser.ItemFeatureTransitCoverAttributeCollectionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.gsonparser.TransitCoverAmountBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.gsonparser.TransitCoverIncludedBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.gsonparser.TransitCoverMaximumBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.gsonparser.TransitCoverRateBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.javabean.gsonparser.ItemFeatureBundledBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.javabean.gsonparser.ItemFeatureCollectionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.javabean.gsonparser.ItemFeaturePriceBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.javabean.gsonparser.ItemFeatureTransitCoverBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.javabean.gsonparser.ItemFeatureTypeBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.CertificateNumberBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.CommentBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.CubicVolumeBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.DescriptionOfOtherBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ExportDeclarationNumberBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.GoodsClassificationTypeBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ImportReferenceNumberBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.InvoiceNumberBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemCollectionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemContentCollectionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemDescriptionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemHeightBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemIdBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemLengthBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemReferenceBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemSummaryBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemWeightBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemWidthBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.LicenceNumberBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.NonDeliveryActionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ProductIdBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ReasonForReturnBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.tracking.javabean.gsonparser.ArticleIdBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.tracking.javabean.gsonparser.BarcodeIdBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.tracking.javabean.gsonparser.ConsignmentIdBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.tracking.javabean.gsonparser.TrackingDetailsBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ATLNumberBiSerializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorCollectionBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorContextBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorContextPairBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorsResponseBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostShipmentAddressLinesBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostShipmentAddressTypeBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostShipmentBusinessNameBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostShipmentCountryBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostShipmentDeliveryInstructionBiSerializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostShipmentEmailBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostShipmentPersonalNameBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostShipmentPhoneNumberBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostShipmentSuburbBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AustraliaPostCustomerNumberBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AuthorisationNumberBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.DangerousGoodsBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.DateBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.DateTimeBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.DiscountBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.FreightChargeBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.FuelSurchargeBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.GoodsDescriptionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.GoodsDescriptionCollectionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.MovementTypeBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.OrderShipmentItemStatusBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.PackagingTypeBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.PackingGroupDesignatorBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ReceiverBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.SecuritySurchargeBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.SenderBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.SenderReferenceBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.SenderReferenceCollectionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ShipmentCollectionBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ShipmentIdBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ShipmentOrderIdBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ShipmentReferenceBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ShipmentSummaryBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.TotalCostBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.TotalGstBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.TrackingSummaryBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.TrackingSummaryPairBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.TransitCoverBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.GetLabelResponseBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.LabelBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.LabelCollectionBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.LabelGenerationRequestStatusBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.LabelRequestIdBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.LabelURLBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.format.javabean.gsonparser.LabelBrandedBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.format.javabean.gsonparser.LabelFormatBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.format.javabean.gsonparser.LabelFormatCollectionBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.format.javabean.gsonparser.LabelFormatGroupBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.format.javabean.gsonparser.LabelFormatLayoutBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.format.javabean.gsonparser.LabelFormatLeftOffsetBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.format.javabean.gsonparser.LabelFormatTopOffsetBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.javabean.gsonparser.LabelPreferenceBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.javabean.gsonparser.LabelPreferenceCollectionBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.preference.javabean.gsonparser.LabelPreferenceTypeBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.order.javabean.gsonparser.ConsignorBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.order.javabean.gsonparser.CreateOrderRequestBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.order.javabean.gsonparser.CreateOrderResponseBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.order.javabean.gsonparser.OrderBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.order.javabean.gsonparser.OrderIdBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.order.javabean.gsonparser.OrderReferenceBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.order.javabean.gsonparser.OrderShipmentBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.order.javabean.gsonparser.OrderSummaryBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.order.javabean.gsonparser.PaymentMethodBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.javabean.gsonparser.DangerousGoodsClassDivisionBiserializer;
import com.digsarustudio.saru.erp.shipment.server.javabean.gsonparser.DangerousGoodsNetWeightBiserializer;
import com.digsarustudio.saru.erp.shipment.server.javabean.gsonparser.DangerousGoodsOuterPackagingQuantityBiserializer;
import com.digsarustudio.saru.erp.shipment.server.javabean.gsonparser.DangerousGoodsOuterPackagingTypeBiserializer;
import com.digsarustudio.saru.erp.shipment.server.javabean.gsonparser.DangerousGoodsSubsidiaryRiskBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.javabean.gsonparser.DangerousGoodsTechnicalNameBiserializer;
import com.digsarustudio.saru.erp.shipment.server.javabean.gsonparser.UnNumberBiserializer;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.DeliveryWindow;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.DeliveryWindowCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.ShipmentFeatureAttributeDate;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.ShipmentFeatureAttributeTime;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean.DeliveryDate;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean.DeliveryTimes;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean.PickupDate;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean.PickupTime;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean.ShipmentFeatureCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.javabean.CountryOfOrigin;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.javabean.ItemContentDescription;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.javabean.ItemContentQuantity;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.javabean.ItemContentWeight;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.javabean.TariffCode;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.ItemFeaturePrice;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.ItemFeatureTransitCoverAttributeCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverAmount;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverIncluded;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverMaximum;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverRate;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureBundled;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureTransitCover;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.CertificateNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Comment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.CubicVolume;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.DescriptionOfOther;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ExportDeclarationNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.GoodsClassificationType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ImportReferenceNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.InvoiceNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Item;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemContentCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemDescription;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemHeight;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemLength;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemSummary;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemWeight;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemWidth;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.LicenceNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.NonDeliveryAction;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ProductId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ReasonForReturn;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.javabean.ArticleId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.javabean.BarcodeId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.javabean.ConsignmentId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.javabean.TrackingDetails;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ATLNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostError;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorContext;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorContextPair;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorsResponse;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentAddressLines;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentAddressType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentBusinessName;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentCountry;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentDeliveryInstruction;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentEmail;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentPersonalName;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentPhoneNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentSuburb;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AustraliaPostCustomerNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AuthorisationNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.DangerousGoods;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Date;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.DateTime;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Discount;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.FreightCharge;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.FuelSurcharge;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.GoodsDescription;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.GoodsDescriptionCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.MovementType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.OrderShipmentItemStatus;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.PackagingType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.PackingGroupDesignator;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Receiver;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.SecuritySurcharge;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Sender;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.SenderReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentOrderId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentSummary;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TotalCost;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TotalGst;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TrackingSummary;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TrackingSummaryPair;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TransitCover;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.GetLabelResponse;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelGenerationRequestStatus;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelRequestId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelURL;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelBranded;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormat;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatGroup;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatLayout;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatLeftOffset;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatTopOffset;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.javabean.LabelPreference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.javabean.LabelPreferenceCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.javabean.LabelPreferenceType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Consignor;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.CreateOrderRequest;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.CreateOrderResponse;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.OrderSummary;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.PaymentMethod;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.paymentmethod.ChargeToAccount;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsClassDivision;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsNetWeight;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsOuterPackagingQuantity;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsOuterPackagingType;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsSubsidiaryRisk;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsTechnicalName;
import com.digsarustudio.saru.erp.shipment.shared.javabean.UnNumber;
import com.google.gson.GsonBuilder;

/**
 * This service creates an order for the referenced shipments that have previously been created using the Create Shipments service.
 * <h3>Resource information</h3>
 * <table>
 * 	<tr>
 * 		<td>Rate Limited?</td><td>No</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Requests per rate limit window</td><td>-</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Authentication</td><td>Secure API Key</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Response Formats</td><td>json</td>
 * 	</tr>
 * 	<tr>
 * 		<td>HTTP Methods</td><td>PUT</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Resource family</td><td>Orders</td>
 * 	</tr>
 * 	<tr>
 * 		<td>Response object</td><td>Order</td>
 * 	</tr>
 * 	<tr>
 * 		<td>API Version</td><td>v1.0</td>
 * 	</tr>
 * </table>
 * 
 * <h3>HTTP Method</h3>
 * Note that this service has the same endpoint as Create Order Including Shipments. The distinguishing feature is the HTTP Method. For this service the method is PUT.
 * 
 * <h3>Service capabilities</h3>
 * This service creates an order for the referenced shipments that have previously been created using the Create Shipments service.
 * 
 * <h3>Resource URL</h3>
 * <a href="https://digitalapi.auspost.com.au/shipping/v1/orders">https://digitalapi.auspost.com.au/shipping/v1/orders</a>
 * 
 * <h3>Headers</h3>
 * <table>
 * 	<tr>
 * 		<td>authentication (required)</td><td>Your username and password as a HTTP Basic Auth hash. See REST and Authentication.</td>
 * 	</tr>
 * 	<tr>
 * 		<td>account-number (required)</td><td>A 10 digit number for an Australia Post charge account, or an 8 digit number for a StarTrack account. See REST and Authentication.</td>
 * 	</tr>
 * </table>
 * <br>
 * 
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * 
 * 
 * @param		
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-order-from-shipments">Create Order From Shipments</a><br>
 * 
 */
public class OrderEndpoint extends AustraliaPostEndpoint<Order, Order> {
	private final String API_PATH = "shipping/v1/orders";
	
	/**
	 * 
	 * The object builder for {@link OrderEndpoint}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<OrderEndpoint> {
		private OrderEndpoint result = null;

		private Builder() {
			this.result = new OrderEndpoint();
		}

		/**
		 * 
		 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.OrderEndpoint#enableSandboxEnvironment()
		 */
		public Builder enableSandboxEnvironment() {
			result.enableSandboxEnvironment();
			return this;
		}

		/**
		 * 
		 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.OrderEndpoint#enableProductionEnvironment()
		 */
		public Builder enableProductionEnvironment() {
			result.enableProductionEnvironment();
			return this;
		}

		/**
		 * @param details
		 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#setMerchant(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails)
		 */
		public Builder setMerchant(MerchantDetails details) {
			result.setMerchant(details);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public OrderEndpoint build() {
			return this.result;
		}

	}

	private OrderEndpoint() {

	}
	
	public Order create(OrderReference reference, Consignor consignor, List<Shipment> shipments) throws EndpointException  {
		ShipmentCollection collection = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentCollection.builder().setShipments(shipments)
																																						  .build();
		
		return this.create(reference, consignor, collection);
	}
	
	/**
	 * This service creates an order for the referenced shipments that have previously been created using the Create Shipments service.
	 * 
	 * @param data The shipment for order to create
	 * 
	 * @return The shipment created by Australia Post Create Orders API.
	 * @throws EndpointException 
	 */
	public Order create(OrderReference reference, Consignor consignor, ShipmentCollection shipments) throws EndpointException  {		
		if(null == shipments || !shipments.validate()) {
			throw new IllegalArgumentException("The incoming shipments contains invalid id, please confirm before send it");
		}
		
		this.clear();
				
		CreateOrderRequest	request = CreateOrderRequest.builder().setShipments(shipments)
																  .setConsignor(consignor)
																  .setOrderReference(reference)
																  .setPaymentMethod(new ChargeToAccount())
																  .build();
		
		this.setSuccessCode(RESTfulResponseStatusCodes.Created);
		this.setFailureCode(RESTfulResponseStatusCodes.BadRequest);
		this.setResponseObjectType(CreateOrderResponse.class);
		
		this.setMediaContent(request);
		
		CreateOrderResponse response = this.sendRESTfulPutRequest();
		
		if(null == response) {
			return null;
		}
				
		return response.getOrder();
	}
	
	/**
	 * This service returns information related to the order previously created using the Create Bulk Order or Create Order services.
	 * 
	 * @param data The shipment to get.
	 * 
	 * @return The shipment fetched by Australia Post Get Shipment API.
	 * @throws EndpointException 
	 */
	public Order get(Order data) throws EndpointException {
		if( null == data ) {
			return null;
		}
		
		return this.get(data.getOrderId());
	}
	
	public Order get(OrderId id) throws EndpointException {		
		if(null == id || !id.validate()) {
			return null;
		}
		
		this.clear();

		this.setSuccessCode(RESTfulResponseStatusCodes.OK);
		this.setFailureCode(RESTfulResponseStatusCodes.BadRequest);
		this.setResponseObjectType(CreateOrderResponse.class);
		
		this.addURLParameter(id.getValue());
		
		CreateOrderResponse response = this.sendRESTfulGetRequest();		
		if(null == response) {
			return null;
		}
		
		return response.getOrder();
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#enableSandboxEnvironment()
	 */
	@Override
	public void enableSandboxEnvironment() {
		this.enableSandboxEnvironment(API_PATH);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#enableProductionEnvironment()
	 */
	@Override
	public void enableProductionEnvironment() {
		this.enableProductionEnvironment(API_PATH);
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#insert(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void insert(Order data, ResponseCallback<Order> callback) throws EndpointException {
		throw new UnsupportedOperationException("Insert is not supported yet");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#update(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void update(Order data, ResponseCallback<Order> callback) throws EndpointException {
		throw new UnsupportedOperationException("Update is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#update(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)
	 */
	@Override
	public void update(Order data, ResponseCallback<Order> callback, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("Update is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#list(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void list(Order data, ResponseCallback<CollectionResponse<Order>> callback) throws EndpointException {
		throw new UnsupportedOperationException("List is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#list(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void list(Order data, ResponseCallback<CollectionResponse<Order>> callback, Integer cursor,
			Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("List is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#delete(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void delete(Order data, ResponseCallback<Order> callback) throws EndpointException {
		throw new UnsupportedOperationException("Delete is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#delete(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)
	 */
	@Override
	public void delete(Order data, ResponseCallback<Order> callback, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("Delete is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#get(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void get(Order data, ResponseCallback<Order> callback) throws EndpointException {
		throw new UnsupportedOperationException("Get is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#initGsonAndTypeToken()
	 */
	@Override
	protected void initJsonParserAndTypeToken() {		
		// Due to the format of json string which contains mixed data type, so that providing a deserializer to handle it.
		GsonBuilder builder = new GsonBuilder();
		
		this.updateCreateOrderJsonBiserialiser(builder);
		this.updateGetOrderJsonBiserialiser(builder);
		this.updateLabelBiserialiser(builder);
		this.updateErrorMessageBiserialiser(builder);
		
		this.setJsonParserObject(builder.create());		
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	private void updateCreateOrderJsonBiserialiser(GsonBuilder builder) {
		//Request
		builder.registerTypeAdapter(CreateOrderRequest.class, new CreateOrderRequestBiserialiser());
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.OrderReference.class, new OrderReferenceBiserialiser());
		builder.registerTypeAdapter(PaymentMethod.class, new PaymentMethodBiserialiser());
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.Consignor.class, new ConsignorBiserialiser());
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentCollection.class, new ShipmentCollectionBiserialiser());
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment.class, new OrderShipmentBiserialiser());
		builder.registerTypeAdapter(ShipmentId.class, new ShipmentIdBiserialiser());
		
		//Response
		builder.registerTypeAdapter(CreateOrderResponse.class, new CreateOrderResponseBiserialiser());
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.Order.class, new OrderBiserialiser());
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.OrderId.class, new OrderIdBiserialiser());
		builder.registerTypeAdapter(OrderSummary.class, new OrderSummaryBiserialiser());
		builder.registerTypeAdapter(TotalCost.class, new TotalCostBiserialiser());
		builder.registerTypeAdapter(TotalGst.class, new TotalGstBiserialiser());
		builder.registerTypeAdapter(OrderShipmentItemStatus.class, new OrderShipmentItemStatusBiserialiser());
		builder.registerTypeAdapter(TrackingSummary.class, new TrackingSummaryBiserialiser());
		builder.registerTypeAdapter(TrackingSummaryPair.class, new TrackingSummaryPairBiserialiser());
		builder.registerTypeAdapter(ShipmentReference.class, new ShipmentReferenceBiserializer());
		builder.registerTypeAdapter(ShipmentSummary.class, new ShipmentSummaryBiserialiser());
		builder.registerTypeAdapter(MovementType.class, new MovementTypeBiserialiser());
		
		builder.registerTypeAdapter(ItemCollection.class, new ItemCollectionBiserializer());
		builder.registerTypeAdapter(Item.class, new ItemBiserialiser());
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemId.class, new ItemIdBiserialiser());
		builder.registerTypeAdapter(ItemReference.class, new ItemReferenceBiserializer());
		builder.registerTypeAdapter(ProductId.class, new ProductIdBiserializer());
		builder.registerTypeAdapter(TrackingDetails.class, new TrackingDetailsBiserializer());
		builder.registerTypeAdapter(ConsignmentId.class, new ConsignmentIdBiserializer());
		builder.registerTypeAdapter(ArticleId.class, new ArticleIdBiserializer());
		builder.registerTypeAdapter(BarcodeId.class, new BarcodeIdBiserializer());
		builder.registerTypeAdapter(ItemSummary.class, new ItemSummaryBiserialiser());
		builder.registerTypeAdapter(ItemWeight.class, new ItemWeightBiserialiser());
		builder.registerTypeAdapter(ItemContentCollection.class, new ItemContentCollectionBiserializer());
		builder.registerTypeAdapter(ItemContentDescription.class, new ItemContentDescriptionBiserialiser());
		builder.registerTypeAdapter(ItemContentQuantity.class, new ItemContentQuantityBiserialiser());
		builder.registerTypeAdapter(ItemContentWeight.class, new ItemContentWeightBiserialiser());
		builder.registerTypeAdapter(TariffCode.class, new TariffCodeBiserialiser());
		builder.registerTypeAdapter(CountryOfOrigin.class, new CountryOfOriginBiserialiser());
		
		builder.registerTypeAdapter(DateTime.class, new DateTimeBiserialiser());
	}
	
	private void updateGetOrderJsonBiserialiser(GsonBuilder builder) {
		//Shipment collection
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentCollection.class, new ShipmentCollectionBiserialiser());
						
		//Shipment Reference
		builder.registerTypeAdapter(ShipmentReference.class, new ShipmentReferenceBiserializer());
		
		//Sender References
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.SenderReferenceCollection.class, new SenderReferenceCollectionBiserializer());
		builder.registerTypeAdapter(SenderReference.class, new SenderReferenceBiserializer());
		
		//Goods Descriptions
		builder.registerTypeAdapter(GoodsDescriptionCollection.class, new GoodsDescriptionCollectionBiserializer());
		builder.registerTypeAdapter(GoodsDescription.class, new GoodsDescriptionBiserializer());
		
		//Despatch Date
		builder.registerTypeAdapter(Date.class, new DateBiserializer());
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.DateTime.class, new DateTimeBiserialiser());
		
		//From
		builder.registerTypeAdapter(Sender.class, new SenderBiserialiser());
		builder.registerTypeAdapter(AusPostShipmentPersonalName.class, new AusPostShipmentPersonalNameBiserialiser());
		builder.registerTypeAdapter(AusPostShipmentAddressType.class, new AusPostShipmentAddressTypeBiserialiser());
		builder.registerTypeAdapter(AusPostShipmentAddressLines.class, new AusPostShipmentAddressLinesBiserializer());
		builder.registerTypeAdapter(AusPostShipmentSuburb.class, new AusPostShipmentSuburbBiserialiser());
		builder.registerTypeAdapter(AustralianState.class, new AustralianStateBiserializer());
		builder.registerTypeAdapter(AusPostShipmentPhoneNumber.class, new AusPostShipmentPhoneNumberBiserializer());
		builder.registerTypeAdapter(AusPostShipmentCountry.class, new AusPostShipmentCountryBiserialiser());
		builder.registerTypeAdapter(AustralianPostcode.class, new AustralianPostcodeBiserializer());
		builder.registerTypeAdapter(AusPostShipmentEmail.class, new AusPostShipmentEmailBiserializer());
		
		//To
		builder.registerTypeAdapter(Receiver.class, new ReceiverBiserialiser());
		builder.registerTypeAdapter(AustraliaPostCustomerNumber.class, new AustraliaPostCustomerNumberBiserializer());
		builder.registerTypeAdapter(AusPostShipmentBusinessName.class, new AusPostShipmentBusinessNameBiserialiser());
		builder.registerTypeAdapter(AusPostShipmentDeliveryInstruction.class, new AusPostShipmentDeliveryInstructionBiSerializer());
		
		//Dangerous Goods
		builder.registerTypeAdapter(DangerousGoods.class, new DangerousGoodsBiserializer());
		builder.registerTypeAdapter(UnNumber.class, new UnNumberBiserializer());
		builder.registerTypeAdapter(DangerousGoodsTechnicalName.class, new DangerousGoodsTechnicalNameBiserializer());
		builder.registerTypeAdapter(DangerousGoodsNetWeight.class, new DangerousGoodsNetWeightBiserializer());
		builder.registerTypeAdapter(DangerousGoodsClassDivision.class, new DangerousGoodsClassDivisionBiserializer());
		builder.registerTypeAdapter(DangerousGoodsSubsidiaryRisk.class, new DangerousGoodsSubsidiaryRiskBiserialiser());
		builder.registerTypeAdapter(PackingGroupDesignator.class, new PackingGroupDesignatorBiserializer());
		builder.registerTypeAdapter(DangerousGoodsOuterPackagingType.class, new DangerousGoodsOuterPackagingTypeBiserializer());
		builder.registerTypeAdapter(DangerousGoodsOuterPackagingQuantity.class, new DangerousGoodsOuterPackagingQuantityBiserializer());
		
		//Movement Type
		builder.registerTypeAdapter(MovementType.class, new MovementTypeBiserialiser());
		
		//Features
		builder.registerTypeAdapter(ShipmentFeatureCollection.class, new ShipmentFeatureCollectionBiserializer());
		builder.registerTypeAdapter(DeliveryDate.class, new DeliveryDateBiserializer());
		builder.registerTypeAdapter(DeliveryTimes.class, new DeliveryTimesBiserializer());
		builder.registerTypeAdapter(PickupDate.class, new PickupDateBiserializer());
		builder.registerTypeAdapter(PickupTime.class, new PickupTimeBiserializer());
		builder.registerTypeAdapter(ShipmentFeatureAttributeDate.class, new ShipmentFeatureAttributeDateBiserializer());
		builder.registerTypeAdapter(ShipmentFeatureAttributeTime.class, new ShipmentFeatureAttributeTimeBiserializer());
		builder.registerTypeAdapter(DeliveryWindow.class, new DeliveryWindowBiserializer());
		builder.registerTypeAdapter(DeliveryWindowCollection.class, new DeliveryWindowCollectionBiserializer());
		
		//Authorisation Number
		builder.registerTypeAdapter(AuthorisationNumber.class, new AuthorisationNumberBiserializer());
		
		//Items
		builder.registerTypeAdapter(ItemCollection.class, new ItemCollectionBiserializer());
		builder.registerTypeAdapter(Item.class, new ItemBiserialiser());
		builder.registerTypeAdapter(ItemId.class, new ItemIdBiserialiser());
		builder.registerTypeAdapter(ItemReference.class, new ItemReferenceBiserializer());
		builder.registerTypeAdapter(ProductId.class, new ProductIdBiserializer());
		builder.registerTypeAdapter(ItemDescription.class, new ItemDescriptionBiserializer());
		builder.registerTypeAdapter(ItemLength.class, new ItemLengthBiserializer());
		builder.registerTypeAdapter(ItemWidth.class, new ItemWidthBiserializer());
		builder.registerTypeAdapter(ItemHeight.class, new ItemHeightBiserializer());
		builder.registerTypeAdapter(CubicVolume.class, new CubicVolumeBiserializer());
		builder.registerTypeAdapter(ItemWeight.class, new ItemWeightBiserialiser());
		builder.registerTypeAdapter(ReasonForReturn.class, new ReasonForReturnBiserializer());
		builder.registerTypeAdapter(PackagingType.class, new PackagingTypeBiserializer());
		builder.registerTypeAdapter(ATLNumber.class, new ATLNumberBiSerializer());
			//Features
		builder.registerTypeAdapter(ItemFeatureCollection.class, new ItemFeatureCollectionBiserializer());
		builder.registerTypeAdapter(ItemFeatureTransitCover.class, new ItemFeatureTransitCoverBiserializer());
		builder.registerTypeAdapter(ItemFeatureTransitCoverAttributeCollection.class, new ItemFeatureTransitCoverAttributeCollectionBiserializer());
		builder.registerTypeAdapter(TransitCoverAmount.class, new TransitCoverAmountBiserializer());
		builder.registerTypeAdapter(ItemFeatureBundled.class, new ItemFeatureBundledBiserializer());
		builder.registerTypeAdapter(ItemFeatureType.class, new ItemFeatureTypeBiserializer());
		builder.registerTypeAdapter(ItemFeaturePrice.class, new ItemFeaturePriceBiserialiser());
		
			//Tracking Details
		builder.registerTypeAdapter(TrackingDetails.class, new TrackingDetailsBiserializer());
		builder.registerTypeAdapter(ConsignmentId.class, new ConsignmentIdBiserializer());
		builder.registerTypeAdapter(ArticleId.class, new ArticleIdBiserializer());
		builder.registerTypeAdapter(BarcodeId.class, new BarcodeIdBiserializer());
		
		//Export declaration number
		builder.registerTypeAdapter(ExportDeclarationNumber.class, new ExportDeclarationNumberBiserializer());
		
		//Import reference number
		builder.registerTypeAdapter(ImportReferenceNumber.class, new ImportReferenceNumberBiserializer());
		
		//Goods classification type
		builder.registerTypeAdapter(GoodsClassificationType.class, new GoodsClassificationTypeBiserializer());
		
		//Description of other
		builder.registerTypeAdapter(DescriptionOfOther.class, new DescriptionOfOtherBiserializer());
		
		//Non delivery action
		builder.registerTypeAdapter(NonDeliveryAction.class, new NonDeliveryActionBiserializer());
		
		//Certificate number
		builder.registerTypeAdapter(CertificateNumber.class, new CertificateNumberBiserializer());
		
		//Licence number
		builder.registerTypeAdapter(LicenceNumber.class, new LicenceNumberBiserializer());
		
		//Invoice Number
		builder.registerTypeAdapter(InvoiceNumber.class, new InvoiceNumberBiserializer());
		
		//Comment
		builder.registerTypeAdapter(Comment.class, new CommentBiserializer());
		
		//Item Contents
		builder.registerTypeAdapter(ItemContentCollection.class, new ItemContentCollectionBiserializer());
		builder.registerTypeAdapter(ItemContentDescription.class, new ItemContentDescriptionBiserialiser());
		builder.registerTypeAdapter(ItemContentQuantity.class, new ItemContentQuantityBiserialiser());
		builder.registerTypeAdapter(ItemContentWeight.class, new ItemContentWeightBiserialiser());
		builder.registerTypeAdapter(TariffCode.class, new TariffCodeBiserialiser());
		builder.registerTypeAdapter(CountryOfOrigin.class, new CountryOfOriginBiserialiser());
		
		//==== Response object ====
		//Shipment id
		builder.registerTypeAdapter(ShipmentId.class, new ShipmentIdBiserialiser());
		
		//Shipment creation date
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.DateTime.class, new DateTimeBiserialiser());
		
		//Items
		builder.registerTypeAdapter(ItemSummary.class, new ItemSummaryBiserialiser());
		
			//features
		builder.registerTypeAdapter(TransitCoverIncluded.class, new TransitCoverIncludedBiserialiser());
		builder.registerTypeAdapter(TransitCoverRate.class, new TransitCoverRateBiserialiser());
		builder.registerTypeAdapter(TransitCoverMaximum.class, new TransitCoverMaximumBiserialiser());
		
			//item summary
		builder.registerTypeAdapter(TotalCost.class, new TotalCostBiserialiser());
		builder.registerTypeAdapter(TotalGst.class, new TotalGstBiserialiser());
		builder.registerTypeAdapter(OrderShipmentItemStatus.class, new OrderShipmentItemStatusBiserialiser());
		
		//Shipment summary
		builder.registerTypeAdapter(ShipmentSummary.class, new ShipmentSummaryBiserialiser());
		builder.registerTypeAdapter(OrderShipmentItemStatus.class, new OrderShipmentItemStatusBiserialiser());
		builder.registerTypeAdapter(TrackingSummary.class, new TrackingSummaryBiserialiser());
		builder.registerTypeAdapter(TrackingSummaryPair.class, new TrackingSummaryPairBiserialiser());
		builder.registerTypeAdapter(FreightCharge.class, new FreightChargeBiserialiser());
		builder.registerTypeAdapter(Discount.class, new DiscountBiserialiser());
		builder.registerTypeAdapter(TransitCover.class, new TransitCoverBiserialiser());
		builder.registerTypeAdapter(SecuritySurcharge.class, new SecuritySurchargeBiserialiser());
		builder.registerTypeAdapter(FuelSurcharge.class, new FuelSurchargeBiserialiser());
		
		builder.registerTypeAdapter(ShipmentOrderId.class, new ShipmentOrderIdBiserialiser());
	}
	
	private void updateLabelBiserialiser(GsonBuilder builder) {
		builder.registerTypeAdapter(LabelPreferenceCollection.class, new LabelPreferenceCollectionBiserialiser());
		builder.registerTypeAdapter(LabelPreference.class, new LabelPreferenceBiserialiser());
		builder.registerTypeAdapter(LabelPreferenceType.class, new LabelPreferenceTypeBiserialiser());
		builder.registerTypeAdapter(LabelFormatCollection.class, new LabelFormatCollectionBiserialiser());
		builder.registerTypeAdapter(LabelFormat.class, new LabelFormatBiserialiser());		
		builder.registerTypeAdapter(LabelFormatGroup.class, new LabelFormatGroupBiserialiser());
		builder.registerTypeAdapter(LabelFormatLayout.class, new LabelFormatLayoutBiserialiser());
		builder.registerTypeAdapter(LabelBranded.class, new LabelBrandedBiserialiser());
		builder.registerTypeAdapter(LabelFormatLeftOffset.class, new LabelFormatLeftOffsetBiserialiser());
		builder.registerTypeAdapter(LabelFormatTopOffset.class, new LabelFormatTopOffsetBiserialiser());
		
		//Response
		builder.registerTypeAdapter(LabelCollection.class, new LabelCollectionBiserialiser());
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.Label.class, new LabelBiserialiser());
		builder.registerTypeAdapter(LabelRequestId.class, new LabelRequestIdBiserialiser());
		builder.registerTypeAdapter(LabelGenerationRequestStatus.class, new LabelGenerationRequestStatusBiserialiser());
		builder.registerTypeAdapter(DateTime.class, new DateTimeBiserialiser());
		
		builder.registerTypeAdapter(GetLabelResponse.class, new GetLabelResponseBiserialiser());
		builder.registerTypeAdapter(LabelURL.class, new LabelURLBiserialiser());
	}
	
	private void updateErrorMessageBiserialiser(GsonBuilder builder) {
		//Error Response
		builder.registerTypeAdapter(AusPostErrorsResponse.class, new AusPostErrorsResponseBiserialiser());
		builder.registerTypeAdapter(AusPostErrorCollection.class, new AusPostErrorCollectionBiserialiser());
		builder.registerTypeAdapter(AusPostError.class, new AusPostErrorBiserializer());
		builder.registerTypeAdapter(AusPostErrorContext.class, new AusPostErrorContextBiserialiser());
		builder.registerTypeAdapter(AusPostErrorContextPair.class, new AusPostErrorContextPairBiserialiser());
	}
}
