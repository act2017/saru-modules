/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.CollectionResponse;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.ResponseCallback;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.RESTfulResponseStatusCodes;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.address.javabean.gsonparser.SuburbValidationResponseBiserialiser;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationResponse;
import com.google.gson.GsonBuilder;

/**
 * This service validates a suburb, state, and postcode combination for an Australian postal address 
 * and returns the valid suburbs for the given state and postcode.<br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * 
 * 
 * @param		
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-order-from-shipments">Create Order From Shipments</a><br>
 * 
 */
public class AddressEndpoint extends AustraliaPostEndpoint<SuburbValidationRequest, SuburbValidationResponse> {
	private final String API_PATH 				= "shipping/v1/address";
	
	private final String PARAM_SUBURB			= "suburb";
	private final String PARAM_STATE			= "state";
	private final String PARAM_POSTCODE			= "postcode";
	
	/**
	 * 
	 * The object builder for {@link AddressEndpoint}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<AddressEndpoint> {
		private AddressEndpoint result = null;

		private Builder() {
			this.result = new AddressEndpoint();
		}

		/**
		 * 
		 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.AddressEndpoint#enableSandboxEnvironment()
		 */
		public Builder enableSandboxEnvironment() {
			result.enableSandboxEnvironment();
			return this;
		}

		/**
		 * 
		 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.AddressEndpoint#enableProductionEnvironment()
		 */
		public Builder enableProductionEnvironment() {
			result.enableProductionEnvironment();
			return this;
		}

		/**
		 * @param details
		 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#setMerchant(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails)
		 */
		public Builder setMerchant(MerchantDetails details) {
			result.setMerchant(details);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AddressEndpoint build() {
			return this.result;
		}

	}

	private AddressEndpoint() {

	}
	
	/**
	 * To validate the incoming suburb is under the given postcode and state.<br>
	 * If the suburb is under the given postcode and state, the {@link SuburbValidationResponse#isFound()} will 
	 * return true, otherwise false returned.<br>
	 * However, the {@link SuburbValidationResponse#getSuburbs()} and {@link SuburbValidationResponse#getSuburbsString()} 
	 * will return a list of correct suburbs under the given postcode and state which are matched, otherwise, it returns 
	 * null to indicate the postcode and state are not matched.<br> 
	 * 
	 * @param suburb	The suburb to validate
	 * @param postcode	The postcode of suburb to validate
	 * @param state		The state of suburb to validate
	 * @return			The result of validation contains a boolean to indicate the suburb is correct or not and with a 
	 * 					list of suburbs under this postcode.
	 * @throws EndpointException
	 */
	public SuburbValidationResponse validate(Suburb suburb, Postcode postcode, State state) throws EndpointException {
		if(null == suburb || !suburb.validate()) {
			throw new IllegalArgumentException("The incoming suburb is invalid. msg = " + suburb);
		}
		
		if(null == postcode || !postcode.validate()) {
			throw new IllegalArgumentException("The incoming postcode is invalid. msg = " + postcode);
		}
		
		if(null == state || !state.validate()) {
			throw new IllegalArgumentException("The incoming state is invalid. msg = " + state);
		}
		
		this.clear();
		
		this.setSuccessCode(RESTfulResponseStatusCodes.OK);
		this.setFailureCode(RESTfulResponseStatusCodes.BadRequest);
		this.setResponseObjectType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.javabean.SuburbValidationResponse.class);
		
		this.addQueryParameter(PARAM_SUBURB, suburb.getName());
		this.addQueryParameter(PARAM_STATE, state.getCode());
		this.addQueryParameter(PARAM_POSTCODE, postcode.getCode());
		
		return this.sendRESTfulGetRequest();
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#enableSandboxEnvironment()
	 */
	@Override
	public void enableSandboxEnvironment() {
		this.enableSandboxEnvironment(API_PATH);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#enableProductionEnvironment()
	 */
	@Override
	public void enableProductionEnvironment() {
		this.enableProductionEnvironment(API_PATH);
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#insert(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void insert(SuburbValidationRequest data, ResponseCallback<SuburbValidationResponse> callback) throws EndpointException {
		throw new UnsupportedOperationException("Insert is not supported yet");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#update(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void update(SuburbValidationRequest data, ResponseCallback<SuburbValidationResponse> callback) throws EndpointException {
		throw new UnsupportedOperationException("Update is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#update(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)
	 */
	@Override
	public void update(SuburbValidationRequest data, ResponseCallback<SuburbValidationResponse> callback, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("Update is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#list(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void list(SuburbValidationRequest data, ResponseCallback<CollectionResponse<SuburbValidationResponse>> callback) throws EndpointException {
		throw new UnsupportedOperationException("List is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#list(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void list(SuburbValidationRequest data, ResponseCallback<CollectionResponse<SuburbValidationResponse>> callback, Integer cursor,
			Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("List is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#delete(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void delete(SuburbValidationRequest data, ResponseCallback<SuburbValidationResponse> callback) throws EndpointException {
		throw new UnsupportedOperationException("Delete is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#delete(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback, java.lang.Integer)
	 */
	@Override
	public void delete(SuburbValidationRequest data, ResponseCallback<SuburbValidationResponse> callback, Integer maxRowCount) throws EndpointException {
		throw new UnsupportedOperationException("Delete is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.endpoint.Endpoint#get(java.lang.Object, com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	public void get(SuburbValidationRequest data, ResponseCallback<SuburbValidationResponse> callback) throws EndpointException {
		throw new UnsupportedOperationException("Get is not supported yet");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.AustraliaPostEndpoint#initGsonAndTypeToken()
	 */
	@Override
	protected void initJsonParserAndTypeToken() {		
		// Due to the format of json string which contains mixed data type, so that providing a deserializer to handle it.
		GsonBuilder builder = new GsonBuilder();
		
		this.updateSuburbValidateJsonParse(builder);
				
		this.setJsonParserObject(builder.create());		
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	private void updateSuburbValidateJsonParse(GsonBuilder builder) {
		builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.javabean.SuburbValidationResponse.class, new SuburbValidationResponseBiserialiser());
	}
}
