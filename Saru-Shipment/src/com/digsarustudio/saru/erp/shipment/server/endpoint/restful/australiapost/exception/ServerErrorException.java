/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception;

/**
 * This exception thrown when a server error(500) returned from remote service.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class ServerErrorException extends AusPostRESTfulResponseException {

	/**
	 * 
	 */
	public ServerErrorException() {
		
	}

	/**
	 * @param message
	 */
	public ServerErrorException(String message) {
		super(message);
		
	}

	/**
	 * @param cause
	 */
	public ServerErrorException(Throwable cause) {
		super(cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ServerErrorException(String message, Throwable cause) {
		super(message, cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public ServerErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

}
