/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.ItemFeaturePrice;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TotalCost;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TotalGst;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link ItemFeaturePrice} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemFeaturePriceBiserialiser implements JsonDeserializer<ItemFeaturePrice>, JsonSerializer<ItemFeaturePrice> {

	/**
	 * 
	 */
	public ItemFeaturePriceBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(ItemFeaturePrice src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonObject root = new JsonObject();
		
		JsonElement calculatedPriceElem = context.serialize(src.getCalculatedPrice(), TotalCost.class);
		root.add(ItemFeaturePrice.ATTR_CALCULATED_PRICE, calculatedPriceElem);
		
		JsonElement calculatedGstElem = context.serialize(src.getCalculatedGst(), TotalGst.class);
		root.add(ItemFeaturePrice.ATTR_CALCULATED_GST, calculatedGstElem);
		
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public ItemFeaturePrice deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject root = json.getAsJsonObject();
		
		JsonElement	calculatedPriceElem	= root.get(ItemFeaturePrice.ATTR_CALCULATED_PRICE);
		TotalCost	calculatedPrice		= context.deserialize(calculatedPriceElem, TotalCost.class);
		
		JsonElement	calculatedGstElem	= root.get(ItemFeaturePrice.ATTR_CALCULATED_GST);
		TotalGst	calculatedGst		= context.deserialize(calculatedGstElem, TotalGst.class);
		
		return ItemFeaturePrice.builder().setCalculatedGst(calculatedGst)
										 .setCalculatedPrice(calculatedPrice)
										 .build();
	}

}
