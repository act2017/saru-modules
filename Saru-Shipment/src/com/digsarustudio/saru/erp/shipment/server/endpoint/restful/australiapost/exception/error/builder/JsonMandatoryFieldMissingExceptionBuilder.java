/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.JsonMandatoryFieldMissingException;

/**
 * The {@link JsonMandatoryFieldMissingException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class JsonMandatoryFieldMissingExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<JsonMandatoryFieldMissingException> 
											 			implements RESTfulResponseExceptionBuilder<JsonMandatoryFieldMissingException> {
		
	/**
	 * 
	 */
	public JsonMandatoryFieldMissingExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public JsonMandatoryFieldMissingException build() {
		return new JsonMandatoryFieldMissingException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
