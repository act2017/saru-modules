/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.feature.attribute.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.ShipmentFeatureAttributeTime;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Time;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link ShipmentFeatureAttributeTime} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
 */
public class ShipmentFeatureAttributeTimeBiserializer implements JsonDeserializer<ShipmentFeatureAttributeTime>
															  , JsonSerializer<ShipmentFeatureAttributeTime> {

	/**
	 * 
	 */
	public ShipmentFeatureAttributeTimeBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(ShipmentFeatureAttributeTime src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}

		JsonElement	date = context.serialize(src.getTime(), Time.class);
		JsonObject	root = new JsonObject();
		root.add(ShipmentFeatureAttributeTime.ATTR_TIME, date);
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public ShipmentFeatureAttributeTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject root = json.getAsJsonObject();
		
		JsonElement timeElem 	= root.get(ShipmentFeatureAttributeTime.ATTR_TIME);
		Time		date		= context.deserialize(timeElem, Time.class);
		
		return ShipmentFeatureAttributeTime.builder().setValue(date)
							 						 .build();
	}

	
}
