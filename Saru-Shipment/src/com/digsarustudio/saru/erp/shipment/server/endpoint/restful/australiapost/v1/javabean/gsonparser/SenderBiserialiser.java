/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.AustralianPostcode;
import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.AustralianState;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentAddressLines;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentAddressType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentCountry;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentEmail;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentPhoneNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentPersonalName;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostShipmentSuburb;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Sender;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link Sender} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SenderBiserialiser implements JsonSerializer<Sender>
										 , JsonDeserializer<Sender> {

	/**
	 * 
	 */
	public SenderBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public Sender deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject 		root 		= json.getAsJsonObject();
		
		JsonElement						nameElem	= root.get(Sender.ATTR_NAME);
		AusPostShipmentPersonalName		name		= context.deserialize(nameElem, AusPostShipmentPersonalName.class);
		
		JsonElement						typeElem	= root.get(Sender.ATTR_TYPE);
		AusPostShipmentAddressType		type		= context.deserialize(typeElem, AusPostShipmentAddressType.class);
		
		JsonElement						linesElem	= root.get(Sender.ATTR_LINES);
		AusPostShipmentAddressLines		lines		= context.deserialize(linesElem, AusPostShipmentAddressLines.class);
		
		JsonElement						suburbElem	= root.get(Sender.ATTR_SUBURB);
		AusPostShipmentSuburb			suburb		= context.deserialize(suburbElem, AusPostShipmentSuburb.class);
		
		JsonElement						stateElem	= root.get(Sender.ATTR_STATE);
		AustralianState					state		= context.deserialize(stateElem, AustralianState.class);
		
		JsonElement						postcodeElem= root.get(Sender.ATTR_POSTCODE);
		AustralianPostcode				postcode	= context.deserialize(postcodeElem, AustralianPostcode.class);
		
		JsonElement						countryElem	= root.get(Sender.ATTR_COUNTRY);
		AusPostShipmentCountry			country		= context.deserialize(countryElem, AusPostShipmentCountry.class);
		
		JsonElement						phoneElem	= root.get(Sender.ATTR_PHONE);
		AusPostShipmentPhoneNumber		phone		= context.deserialize(phoneElem, AusPostShipmentPhoneNumber.class);
		
		JsonElement						emailElem	= root.get(Sender.ATTR_EMAIL);
		AusPostShipmentEmail			email		= context.deserialize(emailElem, AusPostShipmentEmail.class);		

		return Sender.builder().setName(name)
							   .setType(type)
							   .setAddressLines(lines)
							   .setSuburb(suburb)
							   .setState(state)
							   .setPostcode(postcode)
							   .setCountry(country)
							   .setPhone(phone)
							   .setEmail(email)
							   .build();
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(Sender src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;			
		}
		
		JsonObject root	= new JsonObject();
		
		JsonElement	nameElem	= context.serialize(src.getName(), AusPostShipmentPersonalName.class);
		root.add(Sender.ATTR_NAME, nameElem);
		
		AusPostShipmentAddressType type = (AusPostShipmentAddressType) src.getType();
		if(null != type && type.validate()) {
			JsonElement typeElem = context.serialize(type, AusPostShipmentAddressType.class);
			root.add(Sender.ATTR_TYPE, typeElem);
		}
		
		JsonElement	linesElem		= context.serialize(src.getAddressLines(), AusPostShipmentAddressLines.class);
		root.add(Sender.ATTR_LINES, linesElem);
		
		JsonElement	suburbElem		= context.serialize(src.getSuburb(), AusPostShipmentSuburb.class);
		root.add(Sender.ATTR_SUBURB, suburbElem);
		
		JsonElement	stateElem		= context.serialize(src.getState(), AustralianState.class);
		root.add(Sender.ATTR_STATE, stateElem);
		
		JsonElement	postcodeElem	= context.serialize(src.getPostcode(), AustralianPostcode.class);
		root.add(Sender.ATTR_POSTCODE, postcodeElem);
		
		AusPostShipmentCountry country = (AusPostShipmentCountry) src.getCountry();
		if(null != country && country.validate()) {
			JsonElement	countryElem = context.serialize(country, AusPostShipmentCountry.class);
			root.add(Sender.ATTR_COUNTRY, countryElem);
		}
		
		AusPostShipmentPhoneNumber	phone	= (AusPostShipmentPhoneNumber) src.getPhone();
		if(null != phone && phone.validate()) {
			JsonElement phoneElem	= context.serialize(phone, AusPostShipmentPhoneNumber.class);
			root.add(Sender.ATTR_PHONE, phoneElem);
		}
		
		AusPostShipmentEmail email = (AusPostShipmentEmail) src.getEmail();
		if(null != email && email.validate()) {
			JsonElement emailElem	= context.serialize(email, AusPostShipmentEmail.class);
			root.add(Sender.ATTR_EMAIL, emailElem);			
		}
		
		return root;
	}

}
