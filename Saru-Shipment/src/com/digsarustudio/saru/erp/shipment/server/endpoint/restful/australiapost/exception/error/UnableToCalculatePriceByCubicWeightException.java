/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * The service CODE is not available based upon the calculated cubic weight of WEIGHT kg 
 * for the submitted dimensions and weight.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class UnableToCalculatePriceByCubicWeightException extends AustraliaPostErrorException {

	/**
	 * 
	 */
	public UnableToCalculatePriceByCubicWeightException() {

	}

	/**
	 * @param message
	 */
	public UnableToCalculatePriceByCubicWeightException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public UnableToCalculatePriceByCubicWeightException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnableToCalculatePriceByCubicWeightException(String message, Throwable cause, AusPostErrorCollection errors) {
		super(message, cause, errors);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public UnableToCalculatePriceByCubicWeightException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
