/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost;

import java.io.Serializable;

/**
 * The error codes thrown back if the request contains invalid data.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/restful">REST and Authentication</a>
 *
 */
@SuppressWarnings("serial")
public class ErrorCodes implements Serializable{
	//==== REST and Authentication ====
	//Ref:https://developers.auspost.com.au/apis/shipping-and-tracking/reference/restful
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 40001</li>
	 * 	<li><b>Name</b>: JSON_NO_CONTRACT_ID</li>
	 * 	<li><b>Description:</b> A location with the charge account number of account number cannot be found. 
	 * 							Please check that the identifier is correct and submit the request again.</li>
	 * </ul>
	 */
	public static final ErrorCodes JsonNoContractID			= new ErrorCodes(40001, "JSON_NO_CONTRACT_ID");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 40002</li>
	 * 	<li><b>Name</b>: JSON_MANDATORY_FIELD_MISSING</li>
	 * 	<li><b>Description:</b> The input request is missing the mandatory field with the name element name. 
	 * 							Please resubmit the request including the required fields and values.</li>
	 * </ul>
	 */
	public static final ErrorCodes JsonMandatoryFieldMissing	= new ErrorCodes(40002, "JSON_MANDATORY_FIELD_MISSING");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 41001</li>
	 * 	<li><b>Name</b>: CUSTOMER_NOT_FOUND</li>
	 * 	<li><b>Description:</b> A location with the charge account number of account number cannot be found. 
	 * 							Please check that the identifier is correct and submit the request again.</li>
	 * </ul>
	 */
	public static final ErrorCodes CustomerNotFound			= new ErrorCodes(41001, "CUSTOMER_NOT_FOUND");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 41002</li>
	 * 	<li><b>Name</b>: ACCOUNT_NOT_FOUND</li>
	 * 	<li><b>Description:</b> An account with the id of account number cannot be found. 
	 * 							Please check that the identifier is correct and submit the request again.</li>
	 * </ul>
	 */
	public static final ErrorCodes AccountNotFound			= new ErrorCodes(41002, "ACCOUNT_NOT_FOUND");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 41003</li>
	 * 	<li><b>Name</b>: CONTRACT_NOT_VALID_ERROR</li>
	 * 	<li><b>Description:</b> The contract for the charge account with number account number has expired or is not yet valid. 
	 * 							For further assistance please contact your Account Manager or call Australia Post on 13 21 31.</li>
	 * </ul>
	 */
	public static final ErrorCodes ContractNotValidError	= new ErrorCodes(41003, "CONTRACT_NOT_VALID_ERROR");
	
	//==== Create Shipments ====
	//Ref: https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 41007</li>
	 * 	<li><b>Name</b>: FROM_POSTCODE_DOES_NOT_MATCH_CONTRACT_ORIGIN_POSTCODE</li>
	 * 	<li><b>Description:</b> The postcode POSTCODE submitted for the sender "from" address does not match the lodgement postcode POSTCODE the contract. 
	 * 							Please change the sender "from" postcode to match the lodgement postcode in the contract and attempt the request again.</li>
	 * </ul>
	 */
	public static final ErrorCodes FromPostcodeDoesNotMatchContractOriginPostcode	= new ErrorCodes(41007,	"FROM_POSTCODE_DOES_NOT_MATCH_CONTRACT_ORIGIN_POSTCODE");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 42002</li>
	 * 	<li><b>Name</b>: NO_PRICES_FOR_PRODUCT</li>
	 * 	<li><b>Description:</b> The service CODE is not available based upon the information submitted.</li>
	 * </ul>
	 */
	public static final ErrorCodes NoPricesForProduct	= new ErrorCodes(42002, "NO_PRICES_FOR_PRODUCT");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 42006</li>
	 * 	<li><b>Name</b>: MAX_WIDTH</li>
	 * 	<li><b>Description:</b> Maximum width must not exceed 105 cm</li>
	 * </ul>
	 */
	public static final ErrorCodes MaxWidth	= new ErrorCodes(42006,	"MAX_WIDTH");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 42007</li>
	 * 	<li><b>Name</b>: MAX_HEIGHT</li>
	 * 	<li><b>Description:</b> Maximum height must not exceed 105 cm</li>
	 * </ul>
	 */
	public static final ErrorCodes MaxHeight	= new ErrorCodes(42007,	"MAX_HEIGHT");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 42008</li>
	 * 	<li><b>Name</b>: MAX_LENGTH</li>
	 * 	<li><b>Description:</b> Maximum length must not exceed 105 cm</li>
	 * </ul>
	 */
	public static final ErrorCodes MaxLength	= new ErrorCodes(42008,	"MAX_LENGTH");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 42009</li>
	 * 	<li><b>Name</b>: MAX_VOLUME</li>
	 * 	<li><b>Description:</b> Maximum volume must not exceed 0.25 m3</li>
	 * </ul>
	 */
	public static final ErrorCodes MaxVolumne	= new ErrorCodes(42009,	"MAX_VOLUME");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 42010</li>
	 * 	<li><b>Name</b>: MAX_WEIGHT</li>
	 * 	<li><b>Description:</b> Maximum weight must not exceed 10000 kg</li>
	 * </ul>
	 */
	public static final ErrorCodes MaxWeight	= new ErrorCodes(42010,	"MAX_WEIGHT");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 42011</li>
	 * 	<li><b>Name</b>: TWO_DIMENSIONS_LESS_THAN_5CM</li>
	 * 	<li><b>Description:</b> At least 2 dimensions must be 5 cm</li>
	 * </ul>
	 */
	public static final ErrorCodes TwoDimensionsLessThan5cm	= new ErrorCodes(42011,	"TWO_DIMENSIONS_LESS_THAN_5CM");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 42012</li>
	 * 	<li><b>Name</b>: PRODUCT_NOT_SUPPORTED_BY_CONTRACT_ERROR</li>
	 * 	<li><b>Description:</b> The product CODE you have entered is not available on your contract ACCOUNT for the destination postcode POSTCODE. 
	 * 							For further assistance, please contact your Account Manager or call Australia Post on 13 21 31.</li>
	 * </ul>
	 */
	public static final ErrorCodes ProductNotSupportedByContractError = new ErrorCodes(42012, "PRODUCT_NOT_SUPPORTED_BY_CONTRACT_ERROR");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 43003</li>
	 * 	<li><b>Name</b>: UNABLE_TO_CALCULATE_PRICE</li>
	 * 	<li><b>Description:</b> The service CODE is not available based upon the submitted weight of WEIGHT kg.</li>
	 * </ul>
	 */
	public static final ErrorCodes UnableToCalculatePrice	= new ErrorCodes(43003,	"UNABLE_TO_CALCULATE_PRICE");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 43004</li>
	 * 	<li><b>Name</b>: UNABLE_TO_CALCULATE_PRICE</li>
	 * 	<li><b>Description:</b> The service CODE is not available based upon the calculated cubic weight of WEIGHT kg for the submitted dimensions and weight.</li>
	 * </ul>
	 */
	public static final ErrorCodes UnableToCalculatePriceByCubicWeight	= new ErrorCodes(43004,	"UNABLE_TO_CALCULATE_PRICE");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 43008</li>
	 * 	<li><b>Name</b>: MAX_LENGTH_DESTINATION_BUSINESS_NAME</li>
	 * 	<li><b>Description:</b> The shipment with reference REFERENCE includes a business name for the "to" address that exceeds the maximum limit of 50 characters. Please change the business name to have less than 50 characters and submit the request again.</li>
	 * </ul>
	 */
	public static final ErrorCodes MaxLengthDestinationBusinessName	= new ErrorCodes(43008,	"MAX_LENGTH_DESTINATION_BUSINESS_NAME");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 43009</li>
	 * 	<li><b>Name</b>: MAX_LENGTH_CUSTOMER_REFERENCE_TEXT</li>
	 * 	<li><b>Description:</b> The shipment with reference REFERENCE includes a business name for the "to" address that exceeds the maximum limit of 50 characters. Please change the business name to have less than 50 characters and submit the request again.</li>
	 * </ul>
	 */
	public static final ErrorCodes MaxLengthCustomerReferenceText	= new ErrorCodes(43009,	"MAX_LENGTH_CUSTOMER_REFERENCE_TEXT");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 43011</li>
	 * 	<li><b>Name</b>: MAX_LENGTH_DELIVERY_INSTRUCTION</li>
	 * 	<li><b>Description:</b> A value for delivery instructions exceeds the maximum limit of 128 characters. Please reduce the length of the value and submit the request again.</li>
	 * </ul>
	 */
	public static final ErrorCodes MaxLengthDeliveryInstruction	= new ErrorCodes(43011,	"MAX_LENGTH_DELIVERY_INSTRUCTION");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 43017</li>
	 * 	<li><b>Name</b>: ALL_OR_NONE_SHIPMENT_NO_ERROR</li>
	 * 	<li><b>Description:</b> The request contains shipments with items that are missing barcode ids. 
	 * 							Please add barcode ids to the items where the ids are missing, and resubmit your request.</li>
	 * </ul>
	 */
	public static final ErrorCodes AllOrNoneShipmentNoError = new ErrorCodes(43017,	"ALL_OR_NONE_SHIPMENT_NO_ERROR");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 44002</li>
	 * 	<li><b>Name</b>: ALL_OR_NONE_AUSPOST_ID_ERROR</li>
	 * 	<li><b>Description:</b> The request contains shipments with items that are missing a tracking identifier field. 
	 * 							Please ensure that each item contains values for the fields consignment_id, article_id, and barcode_id, and resubmit your request.</li>
	 * </ul>
	 */
	public static final ErrorCodes AllOrNoneAusPostIdError	= new ErrorCodes(44002,	"ALL_OR_NONE_AUSPOST_ID_ERROR");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 44003</li>
	 * 	<li><b>Name</b>: DANGEROUS_GOODS_NOT_SUPPORTED_BY_PRODUCT_ERROR</li>
	 * 	<li><b>Description:</b> The product CODE specified in an item has indicated that dangerous goods will be included in the parcel, 
	 * 							however, the product does not allow dangerous goods to be sent using the service. 
	 * 							Please choose a product that allows dangerous goods to be included within the parcel to be sent.</li>
	 * </ul>
	 */
	public static final ErrorCodes DangerousGoodsNotSupportedByProductError	= new ErrorCodes(44003,	"DANGEROUS_GOODS_NOT_SUPPORTED_BY_PRODUCT_ERROR");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 44010</li>
	 * 	<li><b>Name</b>: MAX_LENGTH_MERCHANT_REFERENCE_TEXT</li>
	 * 	<li><b>Description:</b> A value for merchant reference exceeds the maximum limit of 50 characters. Please reduce the length of the value and submit the request again.</li>
	 * </ul>
	 */
	public static final ErrorCodes MaxLengthMerchantReferenceText	= new ErrorCodes(44010,	"MAX_LENGTH_MERCHANT_REFERENCE_TEXT");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 44013</li>
	 * 	<li><b>Name</b>: SHIPMENT_NOT_FOUND_ERROR</li>
	 * 	<li><b>Description:</b> The shipment with shipment id SHIPMENT you requested can not be found. Please check the shipment id requested and submit the request again.</li>
	 * </ul>
	 */
	public static final ErrorCodes ShipmentNotFoundError	= new ErrorCodes(44013,	"SHIPMENT_NOT_FOUND_ERROR");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 44015</li>
	 * 	<li><b>Name</b>: NO_SHIPMENT_TO_DISPATCH</li>
	 * 	<li><b>Description:</b> There are no shipments in the request to dispatch.</li>
	 * </ul>
	 */
	public static final ErrorCodes NoShipmentToDispatch	= new ErrorCodes(44015,	"NO_SHIPMENT_TO_DISPATCH");	
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 44016</li>
	 * 	<li><b>Name</b>: SHIPMENT_INITIATED</li>
	 * 	<li><b>Description:</b> The shipment SHIPMENT cannot be modified or included in a new order, as the shipment is already contained in an existing order.</li>
	 * </ul>
	 */
	public static final ErrorCodes ShipmentInitiated	= new ErrorCodes(44016,	"SHIPMENT_INITIATED");	
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 44017</li>
	 * 	<li><b>Name</b>: SHIPMENT_IN_PROGRESS</li>
	 * 	<li><b>Description:</b> The shipment SHIPMENT cannot be modified or included in a new order, as the shipment is contained in an order currently being finalized.</li>
	 * </ul>
	 */
	public static final ErrorCodes ShipmentInProgress	= new ErrorCodes(44017,	"SHIPMENT_IN_PROGRESS");	
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 44023</li>
	 * 	<li><b>Name</b>: NO_SHIPMENT_TO_DELETE</li>
	 * 	<li><b>Description:</b> The Request you have entered does not contain any shipments.</li>
	 * </ul>
	 */
	public static final ErrorCodes NoShipmentToDelete	= new ErrorCodes(44023,	"NO_SHIPMENT_TO_DELETE");	
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 44031</li>
	 * 	<li><b>Name</b>: MAX_ITEM_COUNT_ERROR</li>
	 * 	<li><b>Description:</b> Maximum number of item in one manifest request must not exceed 2000.</li>
	 * </ul>
	 */
	public static final ErrorCodes MaxItemCountError	= new ErrorCodes(44031,	"MAX_ITEM_COUNT_ERROR");	
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 44033</li>
	 * 	<li><b>Name</b>: NO_SHIPMENT_IN_REQUEST_ERROR</li>
	 * 	<li><b>Description:</b> The Request you have entered does not contain any shipments to process.</li>
	 * </ul>
	 */
	public static final ErrorCodes NoShipmentInRequestError	= new ErrorCodes(44033,	"NO_SHIPMENT_IN_REQUEST_ERROR");	
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 44038</li>
	 * 	<li><b>Name</b>: SHIPMENT_NOT_WITH_CONTRACT_ERROR</li>
	 * 	<li><b>Description:</b> The shipment with shipment id SHIPMENT does not belong to the contract with account number ACCOUNT. Please process the shipment which belongs to only one contract defined by the account number in the request..</li>
	 * </ul>
	 */
	public static final ErrorCodes ShipmentNotWithContractError	= new ErrorCodes(44038,	"SHIPMENT_NOT_WITH_CONTRACT_ERROR");	
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 44042</li>
	 * 	<li><b>Name</b>: TRANSIT_COVER_AMOUNT_LARGER_THAN_AVT</li>
	 * 	<li><b>Description:</b> Article transit cover amount 1000 is greater than the value threshold 500.0. A recipient signature is needed for the delivery of this article.</li>
	 * </ul>
	 */
	public static final ErrorCodes TransitCoverAmountLagrgerThanAVT	= new ErrorCodes(44042,	"TRANSIT_COVER_AMOUNT_LARGER_THAN_AVT");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 44043</li>
	 * 	<li><b>Name</b>: ITEM_DIMENSION_WEIGHT_NOT_APPLICABLE_ERROR</li>
	 * 	<li><b>Description:</b> The product PRODUCT is a flat-rate product and must not include weight or dimensions.</li>
	 * </ul>
	 */
	public static final ErrorCodes ItemDimensionWeightNotApplicableError	= new ErrorCodes(44043,	"ITEM_DIMENSION_WEIGHT_NOT_APPLICABLE_ERROR");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 51015</li>
	 * 	<li><b>Name</b>: CREATE_LABEL_ERROR_DIFFERENT_PRINTING_SERVICE</li>
	 * 	<li><b>Description:</b> These labels have been generated via a different service. You are only able to reprint these labels using the same service.<br>
	 * 							This error means the API has detected that you've created labels using a system other than the API (for example your own system). 
	 * 							If you provide tracking_details when you create a shipment, the API assumes you have created labels using a different service.
	 * 	</li>
	 * </ul>
	 */
	public static final ErrorCodes CreateLabelErrorDifferentPrintingService	= new ErrorCodes(51015,	"CREATE_LABEL_ERROR_DIFFERENT_PRINTING_SERVICE");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 51020</li>
	 * 	<li><b>Name</b>: LABEL_REQUEST_NOT_FOUND_ERROR</li>
	 * 	<li><b>Description:</b> The request id xxxxxxxxxxxxxxxxxxxxxxxxxxx can not be found. Please check the request id and retry.</li>
	 * </ul>
	 */
	public static final ErrorCodes LabelRequestNotFoundError	= new ErrorCodes(51020,	"LABEL_REQUEST_NOT_FOUND_ERROR");
	
	
	
	//====  ====
	//Ref:

	private Integer code = null;
	private String name = null;
										//==== Authorisation ====
	private static ErrorCodes[] VALUES = {JsonNoContractID, JsonMandatoryFieldMissing, CustomerNotFound, AccountNotFound, ContractNotValidError
										//==== Create Shipment ====
										 ,ProductNotSupportedByContractError, AllOrNoneShipmentNoError, AllOrNoneAusPostIdError, FromPostcodeDoesNotMatchContractOriginPostcode,
										  FromPostcodeDoesNotMatchContractOriginPostcode, NoPricesForProduct, UnableToCalculatePrice, UnableToCalculatePriceByCubicWeight,
										  MaxWidth, MaxHeight, MaxLength, MaxVolumne, MaxWeight, TwoDimensionsLessThan5cm, MaxLengthMerchantReferenceText, DangerousGoodsNotSupportedByProductError,
										  MaxLengthCustomerReferenceText, MaxLengthDeliveryInstruction, MaxLengthDestinationBusinessName,
										  ItemDimensionWeightNotApplicableError
										  
										 //==== Get Shpment ====
										 ,ShipmentNotFoundError, ShipmentNotWithContractError
										  };

	private ErrorCodes(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return this.code;
	}

	public String getName() {
		return this.name;
	}

	public static ErrorCodes fromValue(Integer code) {
		if (null == code) {
			return null;
		}

		ErrorCodes rtn = null;

		for (ErrorCodes target : VALUES) {
			if (!code.equals(target.getCode())) {
				continue;
			}

			rtn = target;
		}

		return rtn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ErrorCodes other = (ErrorCodes) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}
	
}
