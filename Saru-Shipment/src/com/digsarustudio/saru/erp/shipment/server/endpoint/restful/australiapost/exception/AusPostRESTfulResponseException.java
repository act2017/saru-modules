/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception;

import com.digsarustudio.saru.erp.musaceae.server.endpoint.restful.RESTfulReponseException;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.RESTfulResponseStatusCodes;

/**
 * The base class of the exception to handle response code to string message.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class AusPostRESTfulResponseException extends RESTfulReponseException {

	/**
	 * 
	 */
	public AusPostRESTfulResponseException() {
		
	}
	
	public AusPostRESTfulResponseException(RESTfulResponseStatusCodes code) {
		super(String.format("%s[%d]: %s", code.getName(), code.getCode(), code.getDescription()));
		
	}

	/**
	 * @param message
	 */
	public AusPostRESTfulResponseException(String message) {
		super(message);
		
	}

	/**
	 * @param cause
	 */
	public AusPostRESTfulResponseException(Throwable cause) {
		super(cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 */
	public AusPostRESTfulResponseException(String message, Throwable cause) {
		super(message, cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public AusPostRESTfulResponseException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

}
