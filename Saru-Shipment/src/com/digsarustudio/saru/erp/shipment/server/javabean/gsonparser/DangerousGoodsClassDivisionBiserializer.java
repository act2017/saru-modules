/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsClassDivision;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse {@link DangerousGoodsClassDivision} from or into a JSON string used by {@link Shipment} for Australia Post API.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DangerousGoodsClassDivisionBiserializer implements JsonDeserializer<DangerousGoodsClassDivision>
															  , JsonSerializer<DangerousGoodsClassDivision> {

	/**
	 * 
	 */
	public DangerousGoodsClassDivisionBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(DangerousGoodsClassDivision src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonPrimitive classDivision	= new JsonPrimitive(src.getValue());
		
		return classDivision;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public DangerousGoodsClassDivision deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}

		String classDivision = json.getAsString();
		
		return DangerousGoodsClassDivision.builder().setValue(classDivision)
													.build();
	}

	
}
