/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.environment.sandbox.tog;

import java.util.HashMap;
import java.util.Map;

import com.digsarustudio.saru.erp.shipment.shared.PostageProduct;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ProductId;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The services provided by Australia Post.<br>
 * The details should be provided from Database.<br>
 * <br>
 * The following codes are for TOG:<br>
 * <table>
 * 	<tr>
 * 		<td>Service Code</td><td>Description</td>
 * 	</tr>
 * 	<tr>
 * 		<td>RPI8</td><td>REGISTERED POST INT'L 8</td>
 * 	</tr>
 * 	<tr>
 * 		<td>PTI8</td><td>PACK & TRACK INT'L 8</td>
 * 	</tr>
 * 	<tr>
 * 		<td>ECM8</td><td>EXPRESS COURIER INT'L MERCH 8Z</td>
 * 	</tr>
 * 	<tr>
 * 		<td>AIR8</td><td>INTERNATIONAL AIRMAIL 8Z</td>
 * 	</tr>
 * 	<tr>
 * 		<td>7E55</td><td>PARCEL POST + SIGNATURE</td>
 * 	</tr>
 * 	<tr>
 * 		<td>3K55</td><td>EXPRESS POST + SIGNATURE</td>
 * 	</tr>
 * </table>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AustraliaPostPostageProducts implements PostageProduct, IsSerializable {
	/**
	 * Code: 7E55
	 */
	public static final AustraliaPostPostageProducts	eParcelStandard						= new AustraliaPostPostageProducts("7E55", "Parcel Post + Signature");
	
	/**
	 * Code: 3K55
	 */
	public static final AustraliaPostPostageProducts	eParcelExpress						= new AustraliaPostPostageProducts("3K55", "Express Post + Signature");
	
	/**
	 * Code: ECM8
	 */
	public static final AustraliaPostPostageProducts	InternationalExpressMerch			= new AustraliaPostPostageProducts("ECM8", "INTL EXPRESS MERCH/ECI MERCH");
	
	/**
	 * Code: PTI8
	 */
	public static final AustraliaPostPostageProducts	InternationalPackAndTrack			= new AustraliaPostPostageProducts("PTI8", "PACK & TRACK INT'L 8");
	
	/**
	 * Code: RPI8
	 */
	public static final AustraliaPostPostageProducts	InternationalRegisteredPost8		= new AustraliaPostPostageProducts("RPI8", "REGISTERED POST INT'L 8");
	
	/**
	 * Code: AIR8
	 */
	public static final AustraliaPostPostageProducts	InternationalAirMail8				= new AustraliaPostPostageProducts("AIR8", "INTERNATIONAL AIRMAIL 8Z");

	private static Map<String, AustraliaPostPostageProducts> VALUES = new HashMap<>();
		
	static {
		VALUES.put(eParcelStandard.getValue(), eParcelStandard);
		VALUES.put(eParcelStandard.getValue(), eParcelExpress);
		VALUES.put(eParcelStandard.getValue(), InternationalExpressMerch);
		VALUES.put(eParcelStandard.getValue(), InternationalPackAndTrack);
		VALUES.put(eParcelStandard.getValue(), InternationalRegisteredPost8);
		VALUES.put(eParcelStandard.getValue(), InternationalAirMail8);
	}	
	
	private String value = null;
	private String display = null;
	
	private AustraliaPostPostageProducts() {
		
	}
	
	private AustraliaPostPostageProducts(String value, String display) {
		this.value = value;
		this.display = display;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public String getDisplayString() {
		return this.display;
	}
	
	public ProductId getProductId() {
		return com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ProductId.builder().setValue(this.value)
																															 .build();
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.PostageProduct#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.value = code;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.PostageProduct#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.display = name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.PostageProduct#getCode()
	 */
	@Override
	public String getCode() {
		return this.value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.PostageProduct#getName()
	 */
	@Override
	public String getName() {
		return this.display;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.PostageProduct#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.value || this.value.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AustraliaPostPostageProducts other = (AustraliaPostPostageProducts) obj;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	public static AustraliaPostPostageProducts fromValue(String value) {
		if (null == value ) {
			return null;
		}else if(!VALUES.containsKey(value)) {
			new AustraliaPostPostageProducts(value, "UKNOWN CODE [" + value +"]");
		}
		
		return VALUES.get(value);
	}
	
	public static AustraliaPostPostageProducts getDefault() {
		return eParcelStandard;
	}
}
