/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.order.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.OrderShipmentItemStatus;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TotalCost;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TotalGst;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TrackingSummary;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.OrderSummary;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link OrderSummary} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class OrderSummaryBiserialiser implements JsonDeserializer<OrderSummary>
												  , JsonSerializer<OrderSummary> {

	/**
	 * 
	 */
	public OrderSummaryBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(OrderSummary src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonObject	root	= new JsonObject();
		
		JsonElement	totalCost		= context.serialize(src.getTotalCost(), TotalCost.class);
		root.add(OrderSummary.ATTR_TOTAL_COST, totalCost);
		
		JsonElement	totalGst		= context.serialize(src.getTotalGST(), TotalGst.class);
		root.add(OrderSummary.ATTR_TOTAL_GST, totalGst);
		
		JsonElement	status			= context.serialize(src.getStatus(), OrderShipmentItemStatus.class);
		root.add(OrderSummary.ATTR_STATUS, status);
		
		JsonPrimitive	numberOfShipments	= new JsonPrimitive(src.getNumberOfShipments());
		root.add(OrderSummary.ATTR_NUMBER_OF_SHIPMENTS, numberOfShipments);
		
		JsonElement	trackingSummaries	= context.serialize(src.getTrackingSummaries(), TrackingSummary.class);
		root.add(OrderSummary.ATTR_TRACKING_SUMMARY, trackingSummaries);
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public OrderSummary deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject					root				= json.getAsJsonObject();
		
		JsonElement					totalCostElem		= root.get(OrderSummary.ATTR_TOTAL_COST);
		TotalCost					totalCost			= context.deserialize(totalCostElem, TotalCost.class);
		
		JsonElement					totalGstElem		= root.get(OrderSummary.ATTR_TOTAL_GST);
		TotalGst					totalGst			= context.deserialize(totalGstElem, TotalGst.class);
		
		JsonElement					statusElem			= root.get(OrderSummary.ATTR_STATUS);
		OrderShipmentItemStatus		status				= context.deserialize(statusElem, OrderShipmentItemStatus.class);
		
		Integer						numberOfShipments		= null;
		JsonElement					numberOfShipmentsElem	= root.get(OrderSummary.ATTR_NUMBER_OF_SHIPMENTS);
		if(null != numberOfShipmentsElem && !numberOfShipmentsElem.isJsonNull()) {
			numberOfShipments	= numberOfShipmentsElem.getAsInt();
		}
		
		JsonElement					trackingSummaryElem	= root.get(OrderSummary.ATTR_TRACKING_SUMMARY);
		TrackingSummary	trackingSummaries	= context.deserialize(trackingSummaryElem, TrackingSummary.class);
			
		return OrderSummary.builder().setTotalCost(totalCost)
										.setTotalGST(totalGst)
										.setStatus(status)
										.setNumberOfShipments(numberOfShipments)
										.setTrackingSummaries(trackingSummaries)
										.build();
	}

}
