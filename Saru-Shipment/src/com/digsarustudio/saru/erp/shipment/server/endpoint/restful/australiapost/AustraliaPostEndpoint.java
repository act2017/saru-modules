/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.digsarustudio.banana.utils.StringFormatter;
import com.digsarustudio.saru.erp.musaceae.server.endpoint.http.HttpException;
import com.digsarustudio.saru.erp.musaceae.server.endpoint.http.HttpResponse;
import com.digsarustudio.saru.erp.musaceae.server.endpoint.restful.RESTfulEndpoint;
import com.digsarustudio.saru.erp.musaceae.server.endpoint.restful.UnknownResponseStatusCodeException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.Endpoint;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.EndpointException;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.ResponseCallback;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.fields.Accept;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.fields.Authorization;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.http.header.fields.ContentType;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.HttpsURL;
import com.digsarustudio.saru.erp.musaceae.shared.endpoint.url.URL;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.header.AccountNumber;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorBiserializer;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorCollectionBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorContextBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorContextPairBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.AusPostErrorsResponseBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.utils.URLEncoder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorContext;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorContextPair;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorsResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * The endpoint for client code to send request and receive response from the RESTful service maintained by Australia Post.<br>
 * Please setup the path of service, declare incoming and outgoing data object and query parameter, 
 * and handle error code in the sub-types.
 * 
 * @param	T	Inward data type
 * @param	E	Outward data type
 * @param	P	Response Object data type
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/docs/reference">Australia Post API Reference</a>
 * 
 */
public abstract class AustraliaPostEndpoint<T, E> extends RESTfulEndpoint<T, E> implements Endpoint<T, E> {
	Logger logger = Logger.getLogger(this.getClass().getName());
	
	protected static final String BASE_URL		= "https://digitalapi.auspost.com.au";
	protected static final String DOMAIN_NAME	= "digitalapi.auspost.com.au";
	protected static final String SANDBOX_PATH	= "test";
	
	protected Map<Integer, ErrorCodes>	errorCodes	= null;
	
	/**
	 * Please generate the particular gson object for this class to parse the data type from sub-class.<br>
	 * 
	 * @see <a href="https://stackoverflow.com/questions/16396904/using-gson-with-interface-types">Using Gson with Interface Types</a>
	 */
	private Gson						gson			= null;
	
	private RESTfulResponseStatusCodes	successCode		= null;
	private RESTfulResponseStatusCodes	failureCode		= null;
	
	private com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection				operationErrors	= null;
	private RESTfulResponseStatusCodes	responsedCode	= null;
	
	private Type						responseObjType	= null;
	
	private String						jsonResponse	= null;
	private String						jsonRequest		= null;
	
	private MerchantDetails				merchantDetails	= null;
	
	/**
	 * This constructor only available for the incoming and outgoing data working as class.<br>
	 */
	public AustraliaPostEndpoint() {
		super();
		
		this.initHeader();
		this.initJsonParserAndTypeToken();
		this.initSuccessStatusCode();
		this.initFailureStatusCode();
	}
	
	/**
	 * Assign the details of a merchant.<br>
	 * 
	 * @param details The details of a merchant.<br>
	 */
	public void setMerchant(MerchantDetails details) {
		this.merchantDetails = details;
		
		this.addHeader(AccountNumber.builder().setValue(details.getAccountNumber())
											  .build());
		
		this.addHeader(Authorization.builder().setValue(details.getBase64Authorisation())
											  .build());
	}
	
	public com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection getOperationErrors() {
		return this.operationErrors;
	}
	
	public String getJsonResponse() {
		return this.jsonResponse;
	}
	
	public String getJsonRequest() {
		return this.jsonRequest;
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.server.endpoint.restful.RESTfulEndpoint#clear()
	 */
	@Override
	protected void clear() {
		super.clear();
		
		this.jsonRequest 	= null;
		this.jsonResponse	= null;
	}

	public abstract void enableSandboxEnvironment();
	public abstract void enableProductionEnvironment();
	
	protected MerchantDetails getMerchantDetails() {
		return this.merchantDetails;
	}
	
	protected void enableSandboxEnvironment(String functionPath) {
		StringBuffer buffer = new StringBuffer();
		buffer.append(SANDBOX_PATH);
		buffer.append("/");
		buffer.append(functionPath);
		
		URL url = HttpsURL.builder().setEncoder(new URLEncoder())
									.setDomainName(DOMAIN_NAME)									
									.setPath(buffer.toString())
									.build();
		
		this.setURL(url);
	}
	
	protected void enableProductionEnvironment(String functionPath) {		
		URL url = HttpsURL.builder().setEncoder(new URLEncoder())
									.setDomainName(DOMAIN_NAME)									
									.setPath(functionPath)
									.build();
		
		this.setURL(url);
	}
		
	protected <A> void setMediaContent(A content) {
		this.setMediaContent(content, null);
	}
	
	protected <A, C> void setMediaContent(A content, Class<C> type) {
		if(null == content) {
			return;
		}
		
		
		String contentString = null;
		if(null == type) {
			contentString = this.gson.toJson(content, new TypeToken<A>(){}.getType());
		}else {
			contentString = this.gson.toJson(content, type);
		}
		this.jsonRequest = contentString;
		
		if(null == contentString || contentString.isEmpty() || contentString.equals(JSON_NULL)) {
			throw new IllegalArgumentException("The media content is invalid.");
		}
		
		this.setMediaContent(contentString);
	}
	
	protected void setResponseObjectType(Type type) {
		this.responseObjType = type;
	}
	
	protected Type getResponseObjectType() {
		return this.responseObjType;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.RESTfulEndpoint#sendRESTfulGetRequest(com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	protected <P> void sendRESTfulGetRequest(ResponseCallback<P> callback) throws EndpointException{
		HttpResponse response = null;
		try {
			response = this.sendGetRequest();
		} catch (HttpException e) {
			throw new EndpointException(e.getMessage(), e);
		}
		
		this.parseResponse(response, callback);
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.RESTfulEndpoint#sendRESTfulPostRequest(com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	protected <P> void sendRESTfulPostRequest(ResponseCallback<P> callback) throws EndpointException {
		HttpResponse response = null;
		try {
			response = this.sendPostRequest();
		} catch (HttpException e) {
			throw new EndpointException(e.getMessage(), e);
		}
		
		this.parseResponse(response, callback);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.RESTfulEndpoint#sendRESTfulPutRequest(com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	protected <P> void sendRESTfulPutRequest(ResponseCallback<P> callback) throws EndpointException {
		HttpResponse response = null;
		try {
			response = this.sendPutRequest();
		} catch (HttpException e) {
			throw new EndpointException(e.getMessage(), e);
		}
		
		this.parseResponse(response, callback);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.server.endpoint.restful.RESTfulEndpoint#sendRESTfulDeleteRequest(com.digsarustudio.banana.endpoint.ResponseCallback)
	 */
	@Override
	protected <P> void sendRESTfulDeleteRequest(ResponseCallback<P> callback) throws EndpointException {
		HttpResponse response = null;
		try {
			response = this.sendDeleteReqeust();
		} catch (HttpException e) {
			throw new EndpointException(e.getMessage(), e);
		}
		
		this.parseResponse(response, callback);
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.server.endpoint.restful.RESTfulEndpoint#sendRESTfulGetRequest()
	 */
	@Override
	protected <P> P sendRESTfulGetRequest() throws EndpointException {
		HttpResponse response = null;
		try {
			response = this.sendGetRequest();
			return this.parseResponse(response);
		} catch (Throwable e) {
			throw new EndpointException(e.getMessage(), e);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.server.endpoint.restful.RESTfulEndpoint#sendRESTfulPostRequest()
	 */
	@Override
	protected <P> P sendRESTfulPostRequest() throws EndpointException {
		HttpResponse response = null;
		try {
			response = this.sendPostRequest();
			return this.parseResponse(response);
		} catch (Throwable e) {
			throw new EndpointException(e.getMessage(), e);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.server.endpoint.restful.RESTfulEndpoint#sendRESTfulPutRequest()
	 */
	@Override
	protected <P> P sendRESTfulPutRequest() throws EndpointException {
		HttpResponse response = null;
		try {
			response = this.sendPutRequest();
			return this.parseResponse(response);
		} catch (Throwable e) {
			throw new EndpointException(e.getMessage(), e);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.server.endpoint.restful.RESTfulEndpoint#sendRESTfulDeleteRequest()
	 */
	@Override
	protected <P> P sendRESTfulDeleteRequest() throws EndpointException {
		HttpResponse response = null;
		try {
			response = this.sendDeleteReqeust();
			return this.parseResponse(response);
		} catch (Throwable e) {
			throw new EndpointException(e.getMessage(), e);
		}
	}

	/**
	 * Returns the success code of RESTful response defined by remote service API.
	 * 
	 * @return the success code of RESTful response defined by remote service API.
	 */
	protected RESTfulResponseStatusCodes getSuccessCode() {
		return this.successCode;
	}
	
	protected RESTfulResponseStatusCodes getFailureCode() {
		return this.failureCode;
	}

	/**
	 * Please set status code if the interface handles different response code.<br>
	 * Generally, the status code is {@link RESTfulResponseStatusCodes#Created}<br>
	 * 
	 * @param code
	 */
	protected void setSuccessCode(RESTfulResponseStatusCodes code) {
		this.successCode = code;
	}
	
	protected void setFailureCode(RESTfulResponseStatusCodes code) {
		this.failureCode = code;
	}
	
	/**
	 * To setup gson object by a particular type token.<br>
	 * Only available for the polymorphism used by class, not interface.<br>
	 * Example:<br>
	 * final TypeToken<List<Request>> requestListTypeToken = new TypeToken<List<Request>>() {
	 * };<br>
	 * <br>
	 * final RuntimeTypeAdapterFactory<Request> typeFactory = RuntimeTypeAdapterFactory.of(Request.class, "type")
	 * 																					.registerSubtype(LoginRequest.class)
	 * 																					.registerSubtype(PingRequest.class);<br>
	 * <br>
	 * final Gson gson = new GsonBuilder().registerTypeAdapterFactory(typeFactory).create();
	 * <br>
	 * The <b>"type"</b> is a data member of a class which indicates what sort of sub-class used to be converted from a json string.<br>
	 */
	protected abstract void initJsonParserAndTypeToken();
	
	protected void setJsonParserObject(Gson gson) {
		this.gson = gson;
	}
	
	protected void setOperationErrors(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection errors) {
		this.operationErrors = errors;
	}

	private void initHeader() {
		this.addHeader(ContentType.JSON);
		this.addHeader(Accept.JSON);
	}
	
	private void initSuccessStatusCode() {
		this.setSuccessCode(RESTfulResponseStatusCodes.Created);
	}
	
	private void initFailureStatusCode() {
		this.setFailureCode(RESTfulResponseStatusCodes.BadRequest);
	}
	
	/**
	 * To parse {@link HttpResponse} and return it via {@link ResponseCallback} to client call.<br>
	 * 
	 * @param response
	 * @param callback
	 */
	private <P> void parseResponse(HttpResponse response, ResponseCallback<P> callback) {
		try {
			P result = this.parseResponse(response);
			callback.onSuccess(result);
		} catch (Throwable e) {			
			callback.onFailure(e);
		}
	}
	
	/**
	 * To parse {@link HttpResponse} and return it via {@link ResponseCallback} to client call directly.<br>
	 *  
	 * @param response
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private <P> P parseResponse(HttpResponse response) throws Throwable{
		this.responsedCode = RESTfulResponseStatusCodes.fromCode(response.getStatusCode());
		//Clean
		this.jsonResponse = null;
		
		if(null == this.responsedCode) {
			String msg = StringFormatter.format("Unknown HTTP response status code[%d].", response.getStatusCode());
			msg += "\nContent: " + response.getMediaContent();
			throw new UnknownResponseStatusCodeException(msg);
		}else if( this.getSuccessCode() == this.responsedCode) {
			Gson jsonParser = (null != this.gson) ? this.gson : new Gson();
			P result = null;
			
			//No payload for this response.
			this.jsonResponse = response.getMediaContent();
			if(null == this.jsonResponse || this.jsonResponse.isEmpty()) {
				return null;
			}else if(this.jsonResponse.contains("PDF")) {
				return (P) this.jsonResponse;
			}else {
				result = jsonParser.fromJson(this.jsonResponse, this.getResponseObjectType());
			}
			return result;
		}else {
			logger.warning("Fail to manipulate AusPost Shipping and Tracking API. Error: " + response.getMediaContent());
			
			Gson errorGson = null;
			if(null != this.gson) {
				errorGson = this.gson;
			}else {
				GsonBuilder builder = new GsonBuilder();

				//Error Response
				builder.registerTypeAdapter(AusPostErrorsResponse.class, new AusPostErrorsResponseBiserialiser());
				builder.registerTypeAdapter(AusPostErrorCollection.class, new AusPostErrorCollectionBiserialiser());
				builder.registerTypeAdapter(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostError.class, new AusPostErrorBiserializer());
				builder.registerTypeAdapter(AusPostErrorContext.class, new AusPostErrorContextBiserialiser());
				builder.registerTypeAdapter(AusPostErrorContextPair.class, new AusPostErrorContextPairBiserialiser());
						
				//Error Context Pair
				builder.registerTypeAdapter(AusPostErrorContextPair.class, new AusPostErrorContextPairBiserialiser());
				
				errorGson = builder.create();
			}
			
			AusPostErrorsResponse errorResponse = errorGson.fromJson(response.getMediaContent(), AusPostErrorsResponse.class);
			List<AusPostError> errors = errorResponse.getErrors();
			if(null != errors && !errors.isEmpty()) {
				this.setOperationErrors(errorResponse.getErrorCollecion());
			}else {
				com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostError error = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostError.builder().setCode(this.responsedCode.toString())
																																												 				.setName("No Errors Provided")
																																												 				.setMessage("No errors provided")
																																												 				.build();				
				AusPostErrorCollection unknownErrors = AusPostErrorCollection.builder().addError(error)
																				.build();
				this.setOperationErrors(unknownErrors);
			}
			
			Throwable exception = RESTfulResponseExceptions.getInstance().getException(this.responsedCode, this.operationErrors);
			throw exception;
		}		
	}
}
