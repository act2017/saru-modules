/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean.ShipmentFeatureCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AuthorisationNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.DangerousGoods;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Date;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.DateTime;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.GoodsDescriptionCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.MovementType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentOrderId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Receiver;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Sender;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.SenderReferenceCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentPagination;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentSummary;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link Shipment} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ShipmentBiserialiser implements JsonDeserializer<Shipment>
										   , JsonSerializer<Shipment> {

	/**
	 * 
	 */
	public ShipmentBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(Shipment src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonObject root = new JsonObject();
		
		ShipmentId			shipmentId = (ShipmentId) src.getShipmentId();
		if(null != shipmentId && shipmentId.validate()) {
			JsonElement shipmentIdElem	= context.serialize(shipmentId, ShipmentId.class);
			root.add(Shipment.ATTR_SHIPMENT_ID, shipmentIdElem);
		}
		
		ShipmentReference	shipmentRef	= (ShipmentReference) src.getShipmentReference();
		if(null != shipmentRef && shipmentRef.validate()) {
			JsonElement shipmentRefElem	= context.serialize(shipmentRef, ShipmentReference.class);
			root.add(Shipment.ATTR_SHIPMENT_REFERENCE, shipmentRefElem);
		}
		
		SenderReferenceCollection	senderRefs	= (SenderReferenceCollection) src.getSenderReferences();
		if(null != senderRefs && senderRefs.validate()) {
			JsonElement	senderRefsElem = context.serialize(senderRefs, SenderReferenceCollection.class);
			root.add(Shipment.ATTR_SENDER_REFERENCE, senderRefsElem);
		}
		
		GoodsDescriptionCollection	goodsDesces	= (GoodsDescriptionCollection) src.getGoodsDescriptions();
		if(null != goodsDesces) {
			JsonElement goodsDescesElem = context.serialize(goodsDesces, GoodsDescriptionCollection.class);
			root.add(Shipment.ATTR_GOODS_DESCRIPTIONS, goodsDescesElem);
		}
		
		Date	despatchDate	= (Date) src.getDespatchDate();
		if(null != despatchDate && despatchDate.validate()) {
			JsonElement	despatchDateElem	= context.serialize(despatchDate, Date.class);
			root.add(Shipment.ATTR_DESPATCH_DATE, despatchDateElem);
		}
		
		Boolean consolidate = src.isConsolidate();
		if(null != consolidate) {
			JsonPrimitive consolidateElem	= new JsonPrimitive(consolidate);
			root.add(Shipment.ATTR_CONSOLIDATE, consolidateElem);
		}
		
		Boolean emailTacking = src.isEmailTrackingEnabled();
		if(null != emailTacking) {
			JsonPrimitive	emailTrackingElem	= new JsonPrimitive(emailTacking);
			root.add(Shipment.ATTR_EMAIL_TRACKING_ENABLED, emailTrackingElem);
		}
		
		JsonElement	fromElem	= context.serialize(src.getSender(), Sender.class);
		root.add(Shipment.ATTR_FROM, fromElem);
		
		JsonElement	toElem		= context.serialize(src.getReceiver(), Receiver.class);
		root.add(Shipment.ATTR_TO, toElem);
		
		DangerousGoods	dangerousGoods	= (DangerousGoods) src.getDangerousGoods();
		if(null != dangerousGoods && dangerousGoods.validate()) {
			JsonElement dangerousGoodsElem = context.serialize(dangerousGoods, DangerousGoods.class);
			root.add(Shipment.ATTR_DANGEROUS_GOODS, dangerousGoodsElem);
		}
		
		MovementType	movementType	= (MovementType) src.getMovementType();
		if(null != movementType && movementType.validate()) {
			JsonElement movementTypeElem =  context.serialize(movementType, MovementType.class);
			root.add(Shipment.ATTR_MOVEMENT_TYPE, movementTypeElem);
		}
		
		ShipmentFeatureCollection	features = (ShipmentFeatureCollection) src.getShipmentFeatures();
		if(null != features && features.validate()) {
			JsonElement featuresElem = context.serialize(features, ShipmentFeatureCollection.class);
			root.add(Shipment.ATTR_FEATURES, featuresElem);
		}
		
		AuthorisationNumber	authorisationNo = (AuthorisationNumber) src.getAuthorisationNumber();
		if(null != authorisationNo && authorisationNo.validate()) {
			JsonElement authorisationNoElem = context.serialize(authorisationNo, AuthorisationNumber.class);
			root.add(Shipment.ATTR_AUTHORISATION_NUMBER, authorisationNoElem);
		}
		
		JsonElement	itemsElem		= context.serialize(src.getItems(), ItemCollection.class);
		root.add(Shipment.ATTR_ITEMS, itemsElem);
		
		//=== Get Shipment ===
		ShipmentOrderId orderId = (ShipmentOrderId) src.getOrderId();
		if(null != orderId && orderId.validate()) {
			JsonElement orderIdElem = context.serialize(orderId, ShipmentOrderId.class);
			root.add(Shipment.ATTR_ORDER_ID, orderIdElem);			
		}

		ShipmentPagination pagination = (ShipmentPagination) src.getPagination();
		if(null != pagination && pagination.validate()) {
			JsonElement paginationElem = context.serialize(pagination, ShipmentOrderId.class);
			root.add(Shipment.ATTR_PAGINATION, paginationElem);			
		}
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public Shipment deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject	root	= json.getAsJsonObject();
		
		JsonElement			shipmentIdElem	= root.get(Shipment.ATTR_SHIPMENT_ID);
		ShipmentId			shipmentId		= null;
		if(null != shipmentIdElem && !shipmentIdElem.isJsonNull()) {
			shipmentId		= context.deserialize(shipmentIdElem, ShipmentId.class);
		}
		
		JsonElement			shipmentRefElem	= root.get(Shipment.ATTR_SHIPMENT_REFERENCE);
		ShipmentReference	shipmentRef		= null;
		if(null != shipmentRefElem && !shipmentRefElem.isJsonNull()) {
			shipmentRef		= context.deserialize(shipmentRefElem, ShipmentReference.class);
		}
		
		JsonElement			shipmentCreationDateElem	= root.get(Shipment.ATTR_SHIPMENT_CREATION_DATE);
		DateTime			shipmentCreationDate		= null;
		if(null != shipmentCreationDateElem && !shipmentCreationDateElem.isJsonNull()) {
			shipmentCreationDate		= context.deserialize(shipmentCreationDateElem, DateTime.class);
		}
		
		Boolean				isEmailTrackingEnabled		= null;
		JsonElement			emailTrackingEnabledElem	= root.get(Shipment.ATTR_EMAIL_TRACKING_ENABLED);
		if(null != emailTrackingEnabledElem && !emailTrackingEnabledElem.isJsonNull()) {
			isEmailTrackingEnabled		= emailTrackingEnabledElem.getAsBoolean();
		}
		
		Boolean				isConsolicate = null;
		JsonElement			consolidateElem				= root.get(Shipment.ATTR_CONSOLIDATE);
		if(null != consolidateElem && !consolidateElem.isJsonNull()) {
			isConsolicate = consolidateElem.getAsBoolean();
		}
		
		JsonElement			itemsElem					= root.get(Shipment.ATTR_ITEMS);		
		ItemCollection		items						= null;
		if(null != itemsElem && !itemsElem.isJsonNull()) {
			items = context.deserialize(itemsElem, ItemCollection.class);
		}
		
		JsonElement			movementTypeElem			= root.get(Shipment.ATTR_MOVEMENT_TYPE);
		MovementType		movementType				= null;
		if(null != movementTypeElem && !movementTypeElem.isJsonNull()) {
			movementType = context.deserialize(movementTypeElem, MovementType.class);
		}
		
		JsonElement			authorisationNoElem			= root.get(Shipment.ATTR_AUTHORISATION_NUMBER);
		AuthorisationNumber	authorisationNo				= null;
		if(null != authorisationNoElem && !authorisationNoElem.isJsonNull()) {
			authorisationNo	= context.deserialize(authorisationNoElem, AuthorisationNumber.class);
		}
		
		JsonElement			shipmentSummaryElem			= root.get(Shipment.ATTR_SHIPMENT_SUMMARY);
		ShipmentSummary		shipmentSummary				= null;
		if(null != shipmentSummaryElem && !shipmentSummaryElem.isJsonNull()) {
			shipmentSummary				= context.deserialize(shipmentSummaryElem, ShipmentSummary.class);
		}
		
		//==== Get Shipment	====
		JsonElement					senderRefsElem		= root.get(Shipment.ATTR_SENDER_REFERENCE);
		SenderReferenceCollection	senderRefs			= null;
		if(null != senderRefsElem && !senderRefsElem.isJsonNull()) {
			senderRefs			= context.deserialize(senderRefsElem, SenderReferenceCollection.class);
		}
		
		JsonElement					goodsDescesElem		= root.get(Shipment.ATTR_GOODS_DESCRIPTIONS);
		GoodsDescriptionCollection	goodsDesces			= null;
		if(null != goodsDescesElem && !goodsDescesElem.isJsonNull()) {
			goodsDesces			= context.deserialize(goodsDescesElem, GoodsDescriptionCollection.class);
		}
		
		JsonElement					senderElem			= root.get(Shipment.ATTR_FROM);
		Sender						sender				= null;
		if(null != senderElem && !senderElem.isJsonNull()) {
			sender				= context.deserialize(senderElem, Sender.class);
		}
		
		JsonElement					receiverElem		= root.get(Shipment.ATTR_TO);
		Receiver					receiver			= null;
		if(null != receiverElem && !receiverElem.isJsonNull()) {
			receiver = context.deserialize(receiverElem, Receiver.class);
		}
		
		JsonElement					featuresElem		= root.get(Shipment.ATTR_FEATURES);
		ShipmentFeatureCollection	features			= null;
		if(null != featuresElem && !featuresElem.isJsonNull()) {
			features = context.deserialize(featuresElem, ShipmentFeatureCollection.class);
		}
		
		JsonElement					dangerousGoodsElem	= root.get(Shipment.ATTR_DANGEROUS_GOODS);
		DangerousGoods				dangerousGoods		= null;
		if(null != dangerousGoodsElem && !dangerousGoodsElem.isJsonNull()) {
			dangerousGoods		= context.deserialize(dangerousGoodsElem, DangerousGoods.class);
		}
		
		JsonElement			orderIdElem					= root.get(Shipment.ATTR_ORDER_ID);
		ShipmentOrderId		orderId						= null;
		if(null != orderIdElem && !orderIdElem.isJsonNull()) {
			orderId = context.deserialize(orderIdElem, ShipmentOrderId.class);
		}
		
		JsonElement			paginationElem				= root.get(Shipment.ATTR_PAGINATION);
		ShipmentPagination	pagination					= null;
		if(null != paginationElem && !paginationElem.isJsonNull()) {
			pagination	= context.deserialize(paginationElem, ShipmentPagination.class);
		}
		
		return Shipment.builder().setShipmentId(shipmentId)
								 .setShipmentReference(shipmentRef)
								 .setShipmentCreationDate(shipmentCreationDate)
								 .setEmailTracking(isEmailTrackingEnabled)
								 .setConsolidate(isConsolicate)								 
								 .setItems(items)
								 .setMovementType(movementType)
								 .setAuthorisationNumber(authorisationNo)
								 .setShipmentSummary(shipmentSummary)
								 .setSenderReferences(senderRefs)
								 .setGoodsDescriptions(goodsDesces)
								 .setSender(sender)
								 .setReceiver(receiver)
								 .setShipmentFeatures(features)
								 .setDangerousGoods(dangerousGoods)
								 .setOrderId(orderId)
								 .setPagination(pagination)
								 .build();
	}

}
