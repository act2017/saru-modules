/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.DangerousGoods;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.PackingGroupDesignator;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsClassDivision;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsNetWeight;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsOuterPackagingQuantity;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsOuterPackagingType;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsSubsidiaryRisk;
import com.digsarustudio.saru.erp.shipment.shared.javabean.DangerousGoodsTechnicalName;
import com.digsarustudio.saru.erp.shipment.shared.javabean.UnNumber;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link DangerousGoods} from or into a json string  
 * for the {@link Shipment}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DangerousGoodsBiserializer implements JsonDeserializer<DangerousGoods>
												 , JsonSerializer<DangerousGoods> {

	/**
	 * 
	 */
	public DangerousGoodsBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(DangerousGoods src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonObject		root			= new JsonObject();
		JsonElement		unNumberElem	= context.serialize(src.getUnNumber(), UnNumber.class);
		root.add(DangerousGoods.ATTR_UN_NUMBER, unNumberElem);
		
		JsonElement		technicalNameElem	= context.serialize(src.getTechnicalName(), DangerousGoodsTechnicalName.class);
		root.add(DangerousGoods.ATTR_TECHICAL_NAME, technicalNameElem);
		
		JsonElement		netWeightElem	= context.serialize(src.getNetWeight(), DangerousGoodsNetWeight.class);
		root.add(DangerousGoods.ATTR_NET_WEIGHT, netWeightElem);
		
		JsonElement		classDivisionElem	= context.serialize(src.getClassDivision(), DangerousGoodsClassDivision.class);
		root.add(DangerousGoods.ATTR_CLASS_DIVISION, classDivisionElem);
		
		JsonElement		subsidiaryRiskElem	= context.serialize(src.getSubsidiaryRisk(), DangerousGoodsSubsidiaryRisk.class);
		if(null != subsidiaryRiskElem) {
			root.add(DangerousGoods.ATTR_SUBSIDIARY_RISK, subsidiaryRiskElem);
		}
		
		JsonElement		packingGroupDesignatorElem	= context.serialize(src.getPackingGroupDesinator(), PackingGroupDesignator.class);
		if(null != packingGroupDesignatorElem) {
			root.add(DangerousGoods.ATTR_PACKING_GROUP_DESIGNATOR, packingGroupDesignatorElem);
		}
		
		JsonElement		outerPackagingTypeElem	= context.serialize(src.getOuterPackingType(), DangerousGoodsOuterPackagingType.class);
		root.add(DangerousGoods.ATTR_OUTER_PACKAGING_TYPE, outerPackagingTypeElem);
		
		JsonElement		outerPackagingQuantityElem	= context.serialize(src.getOuterPackaginggQuantity(), DangerousGoodsOuterPackagingQuantity.class);
		root.add(DangerousGoods.ATTR_OUTER_PACKAGING_QUANTITY, outerPackagingQuantityElem);
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public DangerousGoods deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}

		JsonObject								root					= json.getAsJsonObject();
		
		JsonElement								unNumberElem			= root.get(DangerousGoods.ATTR_UN_NUMBER);
		UnNumber								unNumber				= context.deserialize(unNumberElem, UnNumber.class);
		
		JsonElement								technicalNameElem		= root.get(DangerousGoods.ATTR_TECHICAL_NAME);
		DangerousGoodsTechnicalName				technicalName			= context.deserialize(technicalNameElem, DangerousGoodsTechnicalName.class);
		
		JsonElement								netWeightElem			= root.get(DangerousGoods.ATTR_NET_WEIGHT);
		DangerousGoodsNetWeight					netWeight				= context.deserialize(netWeightElem, DangerousGoodsNetWeight.class);
		
		JsonElement								classDivisionElem		= root.get(DangerousGoods.ATTR_CLASS_DIVISION);
		DangerousGoodsClassDivision				classDivision			= context.deserialize(classDivisionElem, DangerousGoodsClassDivision.class);
		
		JsonElement								subsidiaryRiskElem		= root.get(DangerousGoods.ATTR_SUBSIDIARY_RISK);
		DangerousGoodsSubsidiaryRisk			subsidiaryRisk			= context.deserialize(subsidiaryRiskElem, DangerousGoodsSubsidiaryRisk.class);
		
		JsonElement								packingGroupDesignatorElem	= root.get(DangerousGoods.ATTR_PACKING_GROUP_DESIGNATOR);
		PackingGroupDesignator					packingGroupDesignator	= context.deserialize(packingGroupDesignatorElem, PackingGroupDesignator.class);
		
		JsonElement								outerPackagingTypeElem	= root.get(DangerousGoods.ATTR_OUTER_PACKAGING_TYPE);
		DangerousGoodsOuterPackagingType		outerPackagingType		= context.deserialize(outerPackagingTypeElem, DangerousGoodsOuterPackagingType.class);
		
		JsonElement								outerPackagingQuantityElem	= root.get(DangerousGoods.ATTR_OUTER_PACKAGING_QUANTITY);
		DangerousGoodsOuterPackagingQuantity	outerPackagingQuantity		= context.deserialize(outerPackagingQuantityElem, DangerousGoodsOuterPackagingQuantity.class);
		
		return DangerousGoods.builder().setUnNumber(unNumber)
									   .setTechnicalName(technicalName)
									   .setNetWeight(netWeight)
									   .setClassDivision(classDivision)
									   .setSubsidiaryRisk(subsidiaryRisk)
									   .setPackingGroupDesignator(packingGroupDesignator)
									   .setOuterPackingType(outerPackagingType)
									   .setOuterPackagingQuantity(outerPackagingQuantity)
									   .build();
	}

}
