/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * The base class represents the error thrown from Australia Post Shipment API.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class AustraliaPostErrorException extends Exception {
	protected AusPostErrorCollection	errors	= null;

	/**
	 * 
	 */
	public AustraliaPostErrorException() {

	}

	/**
	 * @param message
	 */
	public AustraliaPostErrorException(String message) {
		super(message);

	}

	/**
	 * @param message
	 */
	public AustraliaPostErrorException(String message, AusPostErrorCollection errors) {
		this(message, null, errors);
	}

	/**
	 * @param cause
	 */
	public AustraliaPostErrorException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 */
	public AustraliaPostErrorException(Throwable cause, AusPostErrorCollection errors) {
		this(null, cause, errors);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public AustraliaPostErrorException(String message, Throwable cause, AusPostErrorCollection  errors) {
		super(message, cause);

		this.errors = errors;
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public AustraliaPostErrorException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

	public void setErrors(AusPostErrorCollection errors) {
		this.errors = errors;
	}
	
	public AusPostErrorCollection getErrors() {
		return this.errors;
	}
}
