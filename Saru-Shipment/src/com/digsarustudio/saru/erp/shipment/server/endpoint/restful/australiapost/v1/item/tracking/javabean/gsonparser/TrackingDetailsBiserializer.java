/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.tracking.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.javabean.ArticleId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.javabean.BarcodeId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.javabean.ConsignmentId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.javabean.TrackingDetails;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link TrackingDetails} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class TrackingDetailsBiserializer implements JsonSerializer<TrackingDetails>
												  , JsonDeserializer<TrackingDetails> {

	/**
	 * 
	 */
	public TrackingDetailsBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public TrackingDetails deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject		root				= json.getAsJsonObject();
		
		JsonElement		consignmentIdElem	= root.get(TrackingDetails.ATTR_CONSIGNMENT_ID);
		ConsignmentId	consignmentId		= context.deserialize(consignmentIdElem, ConsignmentId.class);
		
		JsonElement		articleIdElem		= root.get(TrackingDetails.ATTR_ARTICLE_ID);
		ArticleId		articleId			= context.deserialize(articleIdElem, ArticleId.class);
		
		JsonElement		barcodeIdElem		= root.get(TrackingDetails.ATTR_BARCODE_ID);
		BarcodeId		barcodeId			= context.deserialize(barcodeIdElem, BarcodeId.class);
		
		return TrackingDetails.builder().setConsignmentId(consignmentId)
										.setArticleId(articleId)
										.setBarcodeId(barcodeId)
										.build();
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(TrackingDetails src, Type typeOfSrc, JsonSerializationContext context) {
		if(null  == src || !src.validate()) {
			return null;
		}
		
		JsonObject	root	= new JsonObject();
		
		JsonElement	consignmentIdElem	= context.serialize(src.getConsignmentId(), ConsignmentId.class);
		root.add(TrackingDetails.ATTR_CONSIGNMENT_ID, consignmentIdElem);
		
		JsonElement	articleIdElem		= context.serialize(src.getArticleId(), ArticleId.class);
		root.add(TrackingDetails.ATTR_ARTICLE_ID, articleIdElem);
		
		//This is the mandatory field for json object, but it is not thrown by Get Shipment API.<br>		
		JsonElement	barcodeIdElem		= context.serialize(src.getBarcodeId(), BarcodeId.class);
		root.add(TrackingDetails.ATTR_BARCODE_ID, barcodeIdElem);
		
		return root;
	}

}
