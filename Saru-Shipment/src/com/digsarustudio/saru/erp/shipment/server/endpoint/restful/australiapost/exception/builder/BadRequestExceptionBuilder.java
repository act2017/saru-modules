/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.BadRequestException;

/**
 * The {@link BadRequestException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class BadRequestExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<BadRequestException> 
										implements RESTfulResponseExceptionBuilder<BadRequestException> {
		
	/**
	 * 
	 */
	public BadRequestExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public BadRequestException build() {
		return new BadRequestException(this.getMessage(), this.getCause());
	}

}
