/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.address.javabean.gsonparser;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.javabean.SuburbValidationResponse;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link SuburbValidationResponse} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SuburbValidationResponseBiserialiser implements JsonDeserializer<SuburbValidationResponse>
														  , JsonSerializer<SuburbValidationResponse> {

	/**
	 * 
	 */
	public SuburbValidationResponseBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(SuburbValidationResponse src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonObject	root	= new JsonObject();
		
		root.add(SuburbValidationResponse.ATTR_FOUND, new JsonPrimitive(src.isFound()));
		
		JsonArray results = new JsonArray();
		for (Suburb suburb : src.getSuburbs()) {
			results.add(suburb.getName());
		}
		
		root.add(SuburbValidationResponse.ATTR_RESULTS, results);
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public SuburbValidationResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject root = json.getAsJsonObject();
		
		JsonElement	foundElem	= root.get(SuburbValidationResponse.ATTR_FOUND);
		Boolean isFound = null;
		if(null != foundElem && !foundElem.isJsonNull()) {
			isFound = foundElem.getAsBoolean();
		}

		List<String> suburbs = null;
		JsonElement	resultsElem	= root.get(SuburbValidationResponse.ATTR_RESULTS);
		if(null != resultsElem && !resultsElem.isJsonNull()) {
			JsonArray results = resultsElem.getAsJsonArray();
			
			suburbs = new ArrayList<>();
			for (int i = 0; i < results.size() ; i++) {
				suburbs.add(results.get(i).getAsString());
			}
		}
		
		SuburbValidationResponse.Builder builder = SuburbValidationResponse.builder();
		
		if(isFound) {
			builder.setFound();
		}else {
			builder.setNotFound();
		}
		
		builder.setSuburbs(suburbs);		
		
		return builder.build();
	}

}
