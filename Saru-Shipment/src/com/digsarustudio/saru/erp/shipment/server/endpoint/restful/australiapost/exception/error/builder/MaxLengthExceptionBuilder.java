/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.MaxLengthException;

/**
 * The {@link MaxLengthException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class MaxLengthExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<MaxLengthException> 
									   implements RESTfulResponseExceptionBuilder<MaxLengthException> {
		
	/**
	 * 
	 */
	public MaxLengthExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public MaxLengthException build() {
		return new MaxLengthException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
