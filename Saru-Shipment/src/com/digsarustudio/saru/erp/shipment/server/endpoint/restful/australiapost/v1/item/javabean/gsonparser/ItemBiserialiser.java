/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.GoodsClassificationTypes;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.CertificateNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Comment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.CubicVolume;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.DescriptionOfOther;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ExportDeclarationNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.GoodsClassificationType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ImportReferenceNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.InvoiceNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Item;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemContentCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemDescription;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemHeight;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemLength;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemSummary;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemWeight;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemWidth;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.LicenceNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.NonDeliveryAction;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ProductId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ReasonForReturn;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.javabean.TrackingDetails;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ATLNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.PackagingType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.Label;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link Item} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemBiserialiser implements JsonDeserializer<Item>, JsonSerializer<Item> {

	/**
	 * 
	 */
	public ItemBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(Item src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return  null;
		}
		
		JsonObject	root	= new JsonObject();
		
		ItemId	id = (ItemId) src.getItemId();
		if(null != id && id.validate()) {
			JsonElement	itemIdElem		= context.serialize(id, ItemId.class);
			root.add(Item.ATTR_ITEM_ID, itemIdElem);
		}
				
		ItemReference reference = (ItemReference) src.getItemReference();
		if(null != reference && reference.validate()) {
			JsonElement	itemReferenceElem		= context.serialize(reference, ItemReference.class);
			root.add(Item.ATTR_ITEM_REFERENCE, itemReferenceElem);
		}
		
		JsonElement	productIdElem			= context.serialize(src.getProductId(), ProductId.class);
		root.add(Item.ATTR_PRODUCT_ID, productIdElem);
		
		ItemDescription itemDesc	= (ItemDescription) src.getDescription();
		if(null != itemDesc && itemDesc.validate()) {
			JsonElement	itemDescElem			= context.serialize(itemDesc, ItemDescription.class);
			root.add(Item.ATTR_ITEM_DESCRIPTION, itemDescElem);
		}
		
		JsonElement	lengthElem				= context.serialize(src.getLenth(), ItemLength.class);
		root.add(Item.ATTR_LENGTH, lengthElem);
		
		JsonElement	widthElem				= context.serialize(src.getWidth(), ItemWidth.class);
		root.add(Item.ATTR_WIDTH, widthElem);
		
		JsonElement	heightElem				= context.serialize(src.getHeight(), ItemHeight.class);
		root.add(Item.ATTR_HEIGHT, heightElem);
		
		CubicVolume cubicVolume = (CubicVolume) src.getCubicVolume();
		if(null != cubicVolume && cubicVolume.validate()) {
			JsonElement	cubicVolumeElem			= context.serialize(cubicVolume, CubicVolume.class);
			root.add(Item.ATTR_CUBIC_VOLUME, cubicVolumeElem);
		}
		
		JsonElement	weightElem				= context.serialize(src.getWeight(), ItemWeight.class);
		root.add(Item.ATTR_WEIGHT, weightElem);
		
		if(null != src.isContainingDangerousGoods()) {
			JsonPrimitive	containsDangerousGoods	= new JsonPrimitive(src.isContainingDangerousGoods());
			root.add(Item.ATTR_CONTAIN_DANGEROUS_GOODS, containsDangerousGoods);
		}
		
		if(null != src.isAuthorityToLeave()) {
			JsonPrimitive	authorityToLeave		= new JsonPrimitive(src.isAuthorityToLeave());
			root.add(Item.ATTR_AUTHORITY_TO_LEAVE, authorityToLeave);
		}
		
		if(null != src.isSafeDropEnabled()) {
			JsonPrimitive	safeDropEnabled			= new JsonPrimitive(src.isSafeDropEnabled());
			root.add(Item.ATTR_SAFE_DROP_ENABLED, safeDropEnabled);
		}
		
		ReasonForReturn reasonForReturn = (ReasonForReturn) src.getReasonForReturn();
		if(null != reasonForReturn && reasonForReturn.validate()) {
			JsonElement		reasonForReturnElem		= context.serialize(reasonForReturn, ReasonForReturn.class);
			root.add(Item.ATTR_REASON_FOR_RETURN, reasonForReturnElem);
		}
		
		if( null != src.isAllowingPartialDelivery() ) {
			JsonPrimitive	allowPartialDelivery	= new JsonPrimitive(src.isAllowingPartialDelivery());
			root.add(Item.ATTR_ALLOW_PARTIAL_DELIVERY, allowPartialDelivery);
		}
		
		PackagingType	packagingType	= (PackagingType) src.getPackagingType();
		if(null != packagingType && packagingType.validate()) {
			JsonElement		packagingTypeElem		= context.serialize(packagingType, PackagingType.class);
			root.add(Item.ATTR_PACKAGING_TYPE, packagingTypeElem);
		}
		
		ATLNumber		atlNo	= (ATLNumber) src.getATLNumber();
		if(null != atlNo && atlNo.validate()) {
			JsonElement		atlNumberElem			= context.serialize(atlNo, ATLNumber.class);
			root.add(Item.ATTR_ATL_NUMBER, atlNumberElem);
		}
		
		ItemFeatureCollection features = (ItemFeatureCollection) src.getFeatures();
		if(null != features && features.validate()) {
			JsonElement		featuresElem			= context.serialize(features, ItemFeatureCollection.class);
			root.add(Item.ATTR_FEATURES, featuresElem);
		}
		
//This field generated by Australia Post, so that do not modified it.		
//		TrackingDetails trackingDetails = (TrackingDetails) src.getTrackingDetails();
//		if(null != trackingDetails && trackingDetails.validate()) {
//			JsonElement		trackingDetailsElem		= context.serialize(trackingDetails, TrackingDetails.class);
//			root.add(Item.ATTR_TRACKING_DETAILS, trackingDetailsElem);
//		}
		
		if( null != src.hasComercialValue() ) {
			JsonPrimitive	commercialValue			= new JsonPrimitive(src.hasComercialValue());
			root.add(Item.ATTR_COMMERCIAL_VALUE, commercialValue);
		}
		
		ExportDeclarationNumber exportDeclarationNo = (ExportDeclarationNumber) src.getExportDeclarationNumber();
		if(null != exportDeclarationNo && exportDeclarationNo.validate()) {
			JsonElement		exportDeclarationNoElem	= context.serialize(exportDeclarationNo, ExportDeclarationNumber.class);
			root.add(Item.ATTR_EXPORT_DECLARATION_NUMBER, exportDeclarationNoElem);
		}		
		
		ImportReferenceNumber importReferenceNo	= (ImportReferenceNumber) src.getImportReferenceNumber();
		if(null != importReferenceNo && importReferenceNo.validate()) {
			JsonElement		importReferenceNoElem	= context.serialize(importReferenceNo, ImportReferenceNumber.class);
			root.add(Item.ATTR_IMPORT_REFERENCE_NUMBER, importReferenceNoElem);
		}
		
		GoodsClassificationType classificationType = (GoodsClassificationType) src.getClassificationType();
		if(null != classificationType && classificationType.validate()) {
			JsonElement		classificationTypeElem	= context.serialize(classificationType, GoodsClassificationType.class);
			root.add(Item.ATTR_CLASSIFICATION_TYPE, classificationTypeElem);
		}
		
		DescriptionOfOther descOfOther = (DescriptionOfOther) src.getDescriptionOfOther();
		if( GoodsClassificationTypes.Other == src.getClassificationType() 
		|| (null != descOfOther && descOfOther.validate()) ) {
			JsonElement		descriptionOfOther		= context.serialize(descOfOther, DescriptionOfOther.class);
			root.add(Item.ATTR_DESCRIPTION_OF_OTHER, descriptionOfOther);
		}
		
		NonDeliveryAction nonDeliveryAction = (NonDeliveryAction) src.getNonDeliveryAction();
		if( null != nonDeliveryAction && nonDeliveryAction.validate() ) {
			JsonElement		nonDeliveryActionElem		= context.serialize(nonDeliveryAction, NonDeliveryAction.class);
			root.add(Item.ATTR_NON_DELIVERY_ACTION, nonDeliveryActionElem);
		}
		
		CertificateNumber certificateNo	= (CertificateNumber) src.getCertificateNumber();
		if(null != certificateNo && certificateNo.validate()) {
			JsonElement		certificateNumberElem		= context.serialize(certificateNo, CertificateNumber.class);
			root.add(Item.ATTR_CERTIFICATE_NUMBER, certificateNumberElem);
		}
		
		LicenceNumber	licenceNo	= (LicenceNumber) src.getLicenceNumber();
		if(null != licenceNo && licenceNo.validate()) {
			JsonElement		licenceNumberElem			= context.serialize(licenceNo, LicenceNumber.class);
			root.add(Item.ATTR_LICENCE_NUMBER, licenceNumberElem);
		}
		
		InvoiceNumber	invoiceNo	= (InvoiceNumber) src.getInvoiceNumber();
		if(null != invoiceNo && invoiceNo.validate() ) {
			JsonElement		invoiceNumberElem			= context.serialize(invoiceNo, InvoiceNumber.class);
			root.add(Item.ATTR_INVOICE_NUMBER, invoiceNumberElem);
		}
		
		Comment comment = (Comment) src.getComment();
		if( null != comment && comment.validate() ) {
			JsonElement		commetElem					= context.serialize(comment, Comment.class);
			root.add(Item.ATTR_COMMENT, commetElem);
		}
		
		ItemContentCollection itemContents = (ItemContentCollection) src.getItemContents();
		if( null != itemContents && itemContents.validate() ) {
			JsonElement		itemContentsElem			= context.serialize(itemContents, ItemContentCollection.class);
			root.add(Item.ATTR_ITEM_CONTENTS, itemContentsElem);		
		}

		//Label is from Australia Post API.
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public Item deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject 	root = json.getAsJsonObject();
		
		JsonElement			idElem			= root.get(Item.ATTR_ITEM_ID);
		ItemId				id				= null;
		if(null != idElem && !idElem.isJsonNull()) {
			id	= context.deserialize(idElem, ItemId.class);
		}
		
		JsonElement			referenceElem	= root.get(Item.ATTR_ITEM_REFERENCE);
		ItemReference		reference		= null;
		if(null != referenceElem && !referenceElem.isJsonNull()) {
			reference = context.deserialize(referenceElem, ItemReference.class);
		}
		
		JsonElement			productIdElem	= root.get(Item.ATTR_PRODUCT_ID);
		ProductId			productId		= null;
		if(null != productIdElem && !productIdElem.isJsonNull()) {
			productId = context.deserialize(productIdElem, ProductId.class);
		}
		
		JsonElement			trackingDetailsElem	= root.get(Item.ATTR_TRACKING_DETAILS);
		TrackingDetails		trackingDetails		= null;
		if(null != trackingDetailsElem && !trackingDetailsElem.isJsonNull()) {
			trackingDetails	= context.deserialize(trackingDetailsElem, TrackingDetails.class);
		}
		
		JsonElement			itemSummaryElem		= root.get(Item.ATTR_ITEM_SUMMARY);
		ItemSummary			itemSummary			= null;
		if(null != itemSummaryElem && !itemSummaryElem.isJsonNull()) {
			itemSummary	= context.deserialize(itemSummaryElem, ItemSummary.class);
		}
		
		JsonElement			itemDescElem		= root.get(Item.ATTR_ITEM_DESCRIPTION);
		ItemDescription		itemDescription		= null;
		if(null != itemDescElem && !itemDescElem.isJsonNull()) {
			itemDescription		= context.deserialize(itemDescElem, ItemDescription.class);
		}
		
		JsonElement			heightElem			= root.get(Item.ATTR_HEIGHT);
		ItemHeight			height				= null;
		if(null != heightElem && !heightElem.isJsonNull()) {
			height = context.deserialize(heightElem, ItemHeight.class);
		}

		JsonElement			lengthElem			= root.get(Item.ATTR_LENGTH);
		ItemLength			length				= null;
		if(null != lengthElem && !lengthElem.isJsonNull()) {
			length				= context.deserialize(lengthElem, ItemLength.class);
		}
		
		JsonElement			widthElem			= root.get(Item.ATTR_WIDTH);
		ItemWidth			width				= null;
		if(null != widthElem && !widthElem.isJsonNull()) {
			width = context.deserialize(widthElem, ItemWidth.class);
		}
		
		JsonElement			cubicVolumeElem		= root.get(Item.ATTR_CUBIC_VOLUME);
		CubicVolume			cubicVolume			= null;
		if(null != cubicVolumeElem && !cubicVolumeElem.isJsonNull()) {
			cubicVolume	= context.deserialize(cubicVolumeElem, CubicVolume.class);
		}
		
		JsonElement			weightElem			= root.get(Item.ATTR_WEIGHT);
		ItemWeight			weight				= null;
		if(null != weightElem && !weightElem.isJsonNull()) {
			weight				= context.deserialize(weightElem, ItemWeight.class);
		}
		
		JsonElement			packagingTypeElem	= root.get(Item.ATTR_PACKAGING_TYPE);
		PackagingType		packagingType		= null;
		if(null != packagingTypeElem && !packagingTypeElem.isJsonNull()) {
			packagingType		= context.deserialize(packagingTypeElem, PackagingType.class);
		}
		
		JsonElement			atlNumberElem		= root.get(Item.ATTR_ATL_NUMBER);
		ATLNumber			atlNumber			= null;
		if(null != atlNumberElem && !atlNumberElem.isJsonNull()) {
			atlNumber			= context.deserialize(atlNumberElem, ATLNumber.class);
		}
		
		Boolean				isContainsDangerousGoods	= null;
		JsonElement			containsDangerousGoodsElem	= root.get(Item.ATTR_CONTAIN_DANGEROUS_GOODS);		
		if(null != containsDangerousGoodsElem && !containsDangerousGoodsElem.isJsonNull()) {
			isContainsDangerousGoods	= containsDangerousGoodsElem.getAsBoolean();
		}

		Boolean				isAuthorityToLeave			= null;
		JsonElement			authorityToLeaveElem		= root.get(Item.ATTR_AUTHORITY_TO_LEAVE);
		if(null != authorityToLeaveElem && !authorityToLeaveElem.isJsonNull()) {
			isAuthorityToLeave			= authorityToLeaveElem.getAsBoolean();
		}
		
		Boolean				safeDropEnabled				= null;
		JsonElement			safeDropEnabledElem			= root.get(Item.ATTR_SAFE_DROP_ENABLED);
		if(null != safeDropEnabledElem && !safeDropEnabledElem.isJsonNull()) {
			safeDropEnabled				= safeDropEnabledElem.getAsBoolean();
		}
		
		Boolean				isAllowPartialDelivery		= null;
		JsonElement			allowPartialDeliveryElem	= root.get(Item.ATTR_ALLOW_PARTIAL_DELIVERY);
		if(null != allowPartialDeliveryElem && !allowPartialDeliveryElem.isJsonNull()) {
			isAllowPartialDelivery		= allowPartialDeliveryElem.getAsBoolean();
		}
		
		JsonElement				featuresElem				= root.get(Item.ATTR_FEATURES);
		ItemFeatureCollection	features					= null;
		if(null != featuresElem && !featuresElem.isJsonNull()) {
			features = context.deserialize(featuresElem, ItemFeatureCollection.class);
		}
		
		JsonElement				labelElem					= root.get(Item.ATTR_LABEL);
		Label					label						= null;
		if(null != labelElem && !labelElem.isJsonNull()) {
			label = context.deserialize(labelElem, Label.class);
		}
		
		return Item.builder().setItemId(id)
							 .setReference(reference)
							 .setProductId(productId)
							 .setTrackingDetails(trackingDetails)
							 .setItemSummary(itemSummary)
							 .setDescription(itemDescription)
							 .setHeight(height)
							 .setLength(length)
							 .setWidth(width)
							 .setCubicVolume(cubicVolume)
							 .setWeight(weight)
							 .setPackagingType(packagingType)
							 .setATLNumber(atlNumber)
							 .setContainsDangerousGoods(isContainsDangerousGoods)
							 .setAuthorityToLeave(isAuthorityToLeave)
							 .setSafeDropEnabled(safeDropEnabled)
							 .setAllowPartialDelivery(isAllowPartialDelivery)
							 .setItemFeatures(features)
							 .setLabel(label)
							 .build();
	}

}
