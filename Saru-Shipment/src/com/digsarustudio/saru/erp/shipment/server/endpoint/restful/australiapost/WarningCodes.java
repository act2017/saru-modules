/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost;

import java.io.Serializable;

/**
 * The warning codes used by Australia Post API.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class WarningCodes implements Serializable {
	//==== Create Shipments ====
	//Ref: https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 66004</li>
	 * 	<li><b>Name</b>: NO_CONTRACT_PRICING_AVAILABLE_FOR_SHIPMENT</li>
	 * 	<li><b>Description:</b> Your account currently does not have a contracted rate for this product, 
	 * 							"from" and "to" lane combination in shipment SHIPMENT ID - 
	 * 							please contact your account manager for further information.</li>
	 * </ul>
	 */
	public static final WarningCodes		NoContractPricingAvailableForShipment = new WarningCodes(66004,	"NO_CONTRACT_PRICING_AVAILABLE_FOR_SHIPMENT");
		
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 44062</li>
	 * 	<li><b>Name</b>: PARTIAL_DIMENSIONS_IN_SHIPMENT_WARNING</li>
	 * 	<li><b>Description:</b> Incomplete dimensions supplied - price will be calculated on weight.</li>
	 * </ul>
	 */
	public static final WarningCodes		PartialDimesionsInShipmentWarning		= new WarningCodes(44062, "PARTIAL_DIMENSIONS_IN_SHIPMENT_WARNING");
	
	/**
	 * <ul>
	 * 	<li><b>Code:</b> 44061</li>
	 * 	<li><b>Name</b>: PARTIAL_DELIVERY_NOT_APPLICABLE_WARNING</li>
	 * 	<li><b>Description:</b> Partial delivery is not applicable for this product.</li>
	 * </ul>
	 */
	public static final WarningCodes		PartialDeliveryNotApplicableWarning		= new WarningCodes(44061, "PARTIAL_DELIVERY_NOT_APPLICABLE_WARNING");		
	
	//====  ====
	//Ref:
	
	
	private Integer code = null;
	private String name = null;

	private static WarningCodes[] VALUES = { NoContractPricingAvailableForShipment, PartialDimesionsInShipmentWarning
										   , PartialDeliveryNotApplicableWarning};

	private WarningCodes(Integer code, String name) {
		this.code = code;
		this.name = name;
	}

	public Integer getCode() {
		return this.code;
	}

	public String getDisplayString() {
		return this.name;
	}

	public static WarningCodes fromValue(String code) {
		if (null == code) {
			return null;
		}

		WarningCodes rtn = null;

		for (WarningCodes target : VALUES) {
			if (!code.equals(target.getCode())) {
				continue;
			}

			rtn = target;
		}

		return rtn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WarningCodes other = (WarningCodes) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		return true;
	}
	
}
