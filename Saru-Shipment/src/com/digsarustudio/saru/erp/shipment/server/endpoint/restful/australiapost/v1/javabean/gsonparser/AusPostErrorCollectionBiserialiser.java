/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostError;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorCollection;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link AusPostErrorCollection} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AusPostErrorCollectionBiserialiser implements JsonSerializer<AusPostErrorCollection>, JsonDeserializer<AusPostErrorCollection> {

	/**
	 * 
	 */
	public AusPostErrorCollectionBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public AusPostErrorCollection deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonArray root = json.getAsJsonArray();
		List<com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError> errors	= new ArrayList<>();
		
		for (int i = 0; i < root.size(); i++) {
			AusPostError error = context.deserialize(root.get(i), AusPostError.class);
			if(null == error) {
				continue;
			}
			
			errors.add(error);
		}
		
		return AusPostErrorCollection.builder().setErrors(errors)
											   .build();
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(AusPostErrorCollection src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonArray root = new JsonArray();
		
		List<com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError> errors = src.getErrors();
		for (com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError ausPostError : errors) {
			root.add(context.serialize(ausPostError, AusPostError.class));
		}
		
		return root;
	}

}
