/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MerchantDetails;

/**
 * This data object contains the API key and account number.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AustraliaPostConfig {
	private static AustraliaPostConfig INSTANCE	= null;
	
	private MerchantDetails	merchantDetails	= null;
	
	private AustraliaPostConfig() {}
	
	public static AustraliaPostConfig getInstance() {
		if(null == INSTANCE) {
			INSTANCE = new AustraliaPostConfig();
		}
		
		return INSTANCE;
	}
	
	public void setMerchant(MerchantDetails merchant) {
		this.merchantDetails = merchant;
	}
	
	public void setMerchant(String accountNo, String apiKey) {
		this.merchantDetails = Merchant.builder().setAccountNumber(accountNo)
												 .setAPIKey(apiKey)
												 .build();
	}
	
	public MerchantDetails getMerchant() {
		return this.merchantDetails;
	}
}
