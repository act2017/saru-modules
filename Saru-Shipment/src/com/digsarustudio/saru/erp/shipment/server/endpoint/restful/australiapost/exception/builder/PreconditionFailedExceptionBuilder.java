/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.PreconditionFailedException;

/**
 * The {@link PreconditionFailedException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PreconditionFailedExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<PreconditionFailedException> 
										   		implements RESTfulResponseExceptionBuilder<PreconditionFailedException> {
		
	/**
	 * 
	 */
	public PreconditionFailedExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public PreconditionFailedException build() {
		return new PreconditionFailedException(this.getMessage(), this.getCause());
	}

}
