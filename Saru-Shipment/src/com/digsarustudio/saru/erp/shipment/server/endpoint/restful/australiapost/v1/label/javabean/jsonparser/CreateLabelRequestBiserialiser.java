/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.CreateLabelRequest;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.javabean.LabelPreferenceCollection;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link CreateLabelRequest} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public class CreateLabelRequestBiserialiser implements JsonDeserializer<CreateLabelRequest>, JsonSerializer<CreateLabelRequest> {

	/**
	 * 
	 */
	public CreateLabelRequestBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(CreateLabelRequest src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}

		JsonObject	root	= new JsonObject();
		
		JsonElement	refsElem		= context.serialize(src.getPreferences(), LabelPreferenceCollection.class);
		root.add(CreateLabelRequest.ATTR_PREFERENCES, refsElem);
		
		JsonElement	shipmentsElem	= context.serialize(src.getShipments(), ShipmentCollection.class);
		root.add(CreateLabelRequest.ATTR_SHIPMENTS, shipmentsElem);
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public CreateLabelRequest deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject	root	= json.getAsJsonObject();
		
		JsonElement					refsElem	= root.get(CreateLabelRequest.ATTR_PREFERENCES);
		LabelPreferenceCollection	refs		= context.deserialize(refsElem, LabelPreferenceCollection.class);
		
		JsonElement					shipmentsElem	= root.get(CreateLabelRequest.ATTR_SHIPMENTS);
		ShipmentCollection			shipments		= context.deserialize(shipmentsElem, ShipmentCollection.class);
		
		return CreateLabelRequest.builder().setPreferences(refs)
										   .setShipments(shipments)
										   .build();
	}

}
