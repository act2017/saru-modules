/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.gsonparser;

import java.lang.reflect.Type;
import java.math.BigDecimal;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverAmount;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite TransitCoverAmount from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class TransitCoverAmountBiserializer implements JsonDeserializer<TransitCoverAmount>
													 , JsonSerializer<TransitCoverAmount> {

	/**
	 * 
	 */
	public TransitCoverAmountBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(TransitCoverAmount src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonPrimitive amount = new JsonPrimitive(new BigDecimal(src.getValue()));
		
		return amount;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public TransitCoverAmount deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}

		String amount = json.getAsString();
		
		return TransitCoverAmount.builder().setValue(amount)
										   .build();
	}

}
