/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.ItemDimensionWeightNotApplicableErrorException;

/**
 * The {@link ItemDimensionWeightNotApplicableErrorException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemDimensionWeightNotApplicableErrorExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<ItemDimensionWeightNotApplicableErrorException> 
											 implements RESTfulResponseExceptionBuilder<ItemDimensionWeightNotApplicableErrorException> {
		
	/**
	 * 
	 */
	public ItemDimensionWeightNotApplicableErrorExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public ItemDimensionWeightNotApplicableErrorException build() {
		return new ItemDimensionWeightNotApplicableErrorException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
