/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.UnableToCalculatePriceByCubicWeightException;

/**
 * The {@link UnableToCalculatePriceByCubicWeightException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class UnableToCalculatePriceByCubicWeightExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<UnableToCalculatePriceByCubicWeightException> 
																 implements RESTfulResponseExceptionBuilder<UnableToCalculatePriceByCubicWeightException> {
		
	/**
	 * 
	 */
	public UnableToCalculatePriceByCubicWeightExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public UnableToCalculatePriceByCubicWeightException build() {
		return new UnableToCalculatePriceByCubicWeightException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
