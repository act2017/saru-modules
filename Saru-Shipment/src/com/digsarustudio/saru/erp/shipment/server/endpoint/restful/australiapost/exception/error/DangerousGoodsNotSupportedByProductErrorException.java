/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * The product CODE specified in an item has indicated that dangerous goods will be included in the parcel, 
 * however, the product does not allow dangerous goods to be sent using the service. 
 * Please choose a product that allows dangerous goods to be included within the parcel to be sent.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class DangerousGoodsNotSupportedByProductErrorException extends AustraliaPostErrorException {

	/**
	 * 
	 */
	public DangerousGoodsNotSupportedByProductErrorException() {

	}

	/**
	 * @param message
	 */
	public DangerousGoodsNotSupportedByProductErrorException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public DangerousGoodsNotSupportedByProductErrorException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public DangerousGoodsNotSupportedByProductErrorException(String message, Throwable cause, AusPostErrorCollection error) {
		super(message, cause, error);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public DangerousGoodsNotSupportedByProductErrorException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
