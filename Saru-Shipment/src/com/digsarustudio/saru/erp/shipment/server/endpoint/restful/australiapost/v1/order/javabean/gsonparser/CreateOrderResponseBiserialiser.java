/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.order.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.CreateOrderResponse;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.Order;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link CreateOrderResponse} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class CreateOrderResponseBiserialiser implements JsonDeserializer<CreateOrderResponse>
													  , JsonSerializer<CreateOrderResponse> {

	/**
	 * 
	 */
	public CreateOrderResponseBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(CreateOrderResponse src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonObject	root	= new JsonObject();
		
		JsonElement	orderElem	= context.serialize(src.getOrder(), Order.class);
		root.add(CreateOrderResponse.ATTR_ORDER, orderElem);
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public CreateOrderResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull() ) {
			return null;
		}
		
		JsonObject root = json.getAsJsonObject();
		
		JsonElement	orderElem	= root.get(CreateOrderResponse.ATTR_ORDER);
		Order		order		= context.deserialize(orderElem, Order.class);
		
		
		return CreateOrderResponse.builder().setOrder(order)
											.build();
	}

}
