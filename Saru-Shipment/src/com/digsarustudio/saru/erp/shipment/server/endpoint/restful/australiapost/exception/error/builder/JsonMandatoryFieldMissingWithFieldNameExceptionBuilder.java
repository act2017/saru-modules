/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.JsonMandatoryFieldMissingWithFieldNameException;

/**
 * The {@link JsonMandatoryFieldMissingWithFieldNameException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class JsonMandatoryFieldMissingWithFieldNameExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<JsonMandatoryFieldMissingWithFieldNameException> 
											 			implements RESTfulResponseExceptionBuilder<JsonMandatoryFieldMissingWithFieldNameException> {
		
	/**
	 * 
	 */
	public JsonMandatoryFieldMissingWithFieldNameExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public JsonMandatoryFieldMissingWithFieldNameException build() {
		return new JsonMandatoryFieldMissingWithFieldNameException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
