/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * Your account currently does not have a contracted rate for this product, "from" and "to" lane 
 * combination in shipment SHIPMENT ID - please contact your account manager for further information.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class NoContractPricingAvailableForShipmentException extends AustraliaPostErrorException {

	/**
	 * 
	 */
	public NoContractPricingAvailableForShipmentException() {

	}

	/**
	 * @param message
	 */
	public NoContractPricingAvailableForShipmentException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public NoContractPricingAvailableForShipmentException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public NoContractPricingAvailableForShipmentException(String message, Throwable cause, AusPostErrorCollection error) {
		super(message, cause, error);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public NoContractPricingAvailableForShipmentException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
