/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorsResponse;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link AusPostErrorsResponse} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AusPostErrorsResponseBiserialiser implements JsonSerializer<AusPostErrorsResponse>, JsonDeserializer<AusPostErrorsResponse> {

	/**
	 * 
	 */
	public AusPostErrorsResponseBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public AusPostErrorsResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject	root 		= json.getAsJsonObject();
		
		JsonElement				errorsElem	= root.get(AusPostErrorsResponse.ATTR_ERRORS);
		AusPostErrorCollection	errors		= context.deserialize(errorsElem, AusPostErrorCollection.class);
		
		return AusPostErrorsResponse.builder().setErrors(errors)
											  .build();
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(AusPostErrorsResponse src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}
		
		JsonObject root = new JsonObject();
		
		JsonElement		errorsElem	= context.serialize(src.getErrorCollecion(), AusPostErrorCollection.class);
		root.add(AusPostErrorsResponse.ATTR_ERRORS, errorsElem);
		
		return root;
	}

}
