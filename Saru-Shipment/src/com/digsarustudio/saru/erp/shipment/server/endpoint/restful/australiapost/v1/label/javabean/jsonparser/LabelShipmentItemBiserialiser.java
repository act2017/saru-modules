/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Item;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemId;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link Item} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelShipmentItemBiserialiser implements JsonDeserializer<Item>, JsonSerializer<Item> {

	/**
	 * 
	 */
	public LabelShipmentItemBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(Item src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || null == src.getItemId() || !src.getItemId().validate()) {
			return null;
		}
		
		JsonObject 	root = new JsonObject();
		
		JsonElement	idElem	= context.serialize(src.getItemId(), ItemId.class);
		root.add(Item.ATTR_ITEM_ID, idElem);
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public Item deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject	root			= json.getAsJsonObject();
		
		JsonElement	idElem	= root.get(Item.ATTR_ITEM_ID);
		ItemId	id		= context.deserialize(idElem, ItemId.class);
		
		return Item.builder().setItemId(id)
								 .build();
	}
}
