/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * The shipment with reference REFERENCE includes a business name for the "to" address that exceeds 
 * the maximum limit of 50 characters. Please change the business name to have less than 50 characters 
 * and submit the request again.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class MaxLengthDestinationBusinessNameException extends AustraliaPostErrorException {

	/**
	 * 
	 */
	public MaxLengthDestinationBusinessNameException() {

	}

	/**
	 * @param message
	 */
	public MaxLengthDestinationBusinessNameException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public MaxLengthDestinationBusinessNameException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public MaxLengthDestinationBusinessNameException(String message, Throwable cause, AusPostErrorCollection error) {
		super(message, cause, error);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public MaxLengthDestinationBusinessNameException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
