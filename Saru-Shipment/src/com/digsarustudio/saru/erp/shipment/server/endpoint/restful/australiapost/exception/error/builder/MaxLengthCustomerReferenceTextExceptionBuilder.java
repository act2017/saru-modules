/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.MaxLengthCustomerReferenceTextException;

/**
 * The {@link MaxLengthCustomerReferenceTextException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class MaxLengthCustomerReferenceTextExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<MaxLengthCustomerReferenceTextException> 
											 				implements RESTfulResponseExceptionBuilder<MaxLengthCustomerReferenceTextException> {
		
	/**
	 * 
	 */
	public MaxLengthCustomerReferenceTextExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public MaxLengthCustomerReferenceTextException build() {
		return new MaxLengthCustomerReferenceTextException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
