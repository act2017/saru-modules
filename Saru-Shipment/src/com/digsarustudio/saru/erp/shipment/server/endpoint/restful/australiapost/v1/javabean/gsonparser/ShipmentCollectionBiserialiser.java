/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentCollection;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link ShipmentCollection} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ShipmentCollectionBiserialiser implements JsonDeserializer<ShipmentCollection>
													 , JsonSerializer<ShipmentCollection> {

	/**
	 * 
	 */
	public ShipmentCollectionBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(ShipmentCollection src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}

		JsonArray root = new JsonArray();
		for (com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment shipment : src.getShipments()) {
			root.add(context.serialize(shipment, Shipment.class));
		}
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public ShipmentCollection deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}

		JsonArray root = json.getAsJsonArray();
		List<com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment> shipments = new ArrayList<>();
		
		for (int i = 0; i < root.size() ; i++) {
			shipments.add(context.deserialize(root.get(i), Shipment.class));
		}
		
		return ShipmentCollection.builder().setShipments(shipments)
										   .build();
	}

}
