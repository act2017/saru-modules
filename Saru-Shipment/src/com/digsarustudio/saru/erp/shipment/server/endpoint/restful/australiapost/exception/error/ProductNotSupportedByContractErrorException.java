/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * The product CODE you have entered is not available on your contract ACCOUNT for the destination postcode 
 * POSTCODE. For further assistance, please contact your Account Manager or call Australia Post on 13 21 31.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class ProductNotSupportedByContractErrorException extends AustraliaPostErrorException {

	/**
	 * 
	 */
	public ProductNotSupportedByContractErrorException() {

	}

	/**
	 * @param message
	 */
	public ProductNotSupportedByContractErrorException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public ProductNotSupportedByContractErrorException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public ProductNotSupportedByContractErrorException(String message, Throwable cause, AusPostErrorCollection errors) {
		super(message, cause, errors);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public ProductNotSupportedByContractErrorException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
