/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.GoodsDescription;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.GoodsDescriptionCollection;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link GoodsDescriptionCollection} from or to a JSON string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class GoodsDescriptionCollectionBiserializer implements JsonDeserializer<GoodsDescriptionCollection>
															 , JsonSerializer<GoodsDescriptionCollection> {

	/**
	 * 
	 */
	public GoodsDescriptionCollectionBiserializer() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(GoodsDescriptionCollection src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || !src.validate()) {
			return null;
		}

		JsonArray descriptions = new JsonArray();
		for (com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescription desc : src.getDescriptions()) {
			descriptions.add(desc.getDescription());
		}
		
		return descriptions;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public GoodsDescriptionCollection deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		GoodsDescription[] descriptions = context.deserialize(json, GoodsDescription[].class);
		
		return GoodsDescriptionCollection.builder().setDescriptions(descriptions)
												   .build();
	}

}
