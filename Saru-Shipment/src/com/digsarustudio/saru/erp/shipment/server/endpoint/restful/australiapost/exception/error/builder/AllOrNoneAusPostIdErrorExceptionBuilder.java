/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.AllOrNoneAusPostIdErrorException;

/**
 * The {@link AllOrNoneAusPostIdErrorException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AllOrNoneAusPostIdErrorExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<AllOrNoneAusPostIdErrorException> 
											 		 implements RESTfulResponseExceptionBuilder<AllOrNoneAusPostIdErrorException> {
		
	/**
	 * 
	 */
	public AllOrNoneAusPostIdErrorExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public AllOrNoneAusPostIdErrorException build() {
		return new AllOrNoneAusPostIdErrorException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
