/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.builder;

import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.BaseRESFfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.builder.RESTfulResponseExceptionBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error.MaxLengthDeliveryInstructionException;

/**
 * The {@link MaxLengthDeliveryInstructionException} builder.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class MaxLengthDeliveryInstructionExceptionBuilder extends BaseRESFfulResponseExceptionBuilder<MaxLengthDeliveryInstructionException> 
											 				implements RESTfulResponseExceptionBuilder<MaxLengthDeliveryInstructionException> {
		
	/**
	 * 
	 */
	public MaxLengthDeliveryInstructionExceptionBuilder() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
	 */
	@Override
	public MaxLengthDeliveryInstructionException build() {
		return new MaxLengthDeliveryInstructionException(this.getMessage(), this.getCause(), this.getErrors());
	}

}
