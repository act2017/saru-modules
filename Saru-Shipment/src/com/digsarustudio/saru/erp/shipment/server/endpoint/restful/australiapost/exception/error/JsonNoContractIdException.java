/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception.error;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * This exception thrown when A location with the charge account number of account number cannot be found. 
 * Please check that the identifier is correct and submit the request again.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class JsonNoContractIdException extends AustraliaPostErrorException {

	/**
	 * 
	 */
	public JsonNoContractIdException() {

	}

	/**
	 * @param message
	 */
	public JsonNoContractIdException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public JsonNoContractIdException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public JsonNoContractIdException(String message, Throwable cause, AusPostErrorCollection error) {
		super(message, cause, error);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public JsonNoContractIdException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
