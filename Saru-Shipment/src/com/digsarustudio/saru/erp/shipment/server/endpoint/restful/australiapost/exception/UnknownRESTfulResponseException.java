/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.exception;

/**
 * This exception represents the RESTful response code which has not been recorded and handled in the system.<br>
 * This may be taken place by the updating of the API.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("serial")
public class UnknownRESTfulResponseException extends AusPostRESTfulResponseException {

	/**
	 * 
	 */
	public UnknownRESTfulResponseException() {

	}

	/**
	 * @param message
	 */
	public UnknownRESTfulResponseException(String message) {
		super(message);
		
	}

	/**
	 * @param cause
	 */
	public UnknownRESTfulResponseException(Throwable cause) {
		super(cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnknownRESTfulResponseException(String message, Throwable cause) {
		super(message, cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public UnknownRESTfulResponseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}
}
