/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser;

import java.lang.reflect.Type;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentId;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

/**
 * The serializer and deserializer for Gson to parse or composite {@link Shipment} from or into a json string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelShipmentBiserialiser implements JsonDeserializer<Shipment>, JsonSerializer<Shipment> {

	/**
	 * 
	 */
	public LabelShipmentBiserialiser() {
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonSerializer#serialize(java.lang.Object, java.lang.reflect.Type, com.google.gson.JsonSerializationContext)
	 */
	@Override
	public JsonElement serialize(Shipment src, Type typeOfSrc, JsonSerializationContext context) {
		if(null == src || null == src.getShipmentId() || !src.getShipmentId().validate()) {
			return null;
		}
		
		JsonObject 	root = new JsonObject();
		
		JsonElement	idElem	= context.serialize(src.getShipmentId(), ShipmentId.class);
		root.add(Shipment.ATTR_SHIPMENT_ID, idElem);
		
		JsonElement	itemsElem	= context.serialize(src.getItems(), ItemCollection.class);
		root.add(Shipment.ATTR_ITEMS, itemsElem);
		
		return root;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.JsonDeserializer#deserialize(com.google.gson.JsonElement, java.lang.reflect.Type, com.google.gson.JsonDeserializationContext)
	 */
	@Override
	public Shipment deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		if(null == json || json.isJsonNull()) {
			return null;
		}
		
		JsonObject	root			= json.getAsJsonObject();
		
		JsonElement	idElem	= root.get(Shipment.ATTR_SHIPMENT_ID);
		ShipmentId	id		= context.deserialize(idElem, ShipmentId.class);
		
		JsonElement		itemsElem	= root.get(Shipment.ATTR_ITEMS);
		ItemCollection	items		= context.deserialize(itemsElem, ItemCollection.class);
		
		return Shipment.builder().setShipmentId(id)
								 .setItems(items)
								 .build();
	}
}
