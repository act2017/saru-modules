/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Label information for each label created.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface LabelCollection {
	/**
	 * 
	 * The builder for {@link LabelCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<LabelCollection> {
		Builder addLabel(Label label);
		Builder setLabels(List<Label> labels);
	}

	void addLabel(Label label);
	void setLabels(List<Label> labels);
	
	Label 		getLabel(Integer index);
	List<Label> getLabels();
	
	Boolean validate();
}
