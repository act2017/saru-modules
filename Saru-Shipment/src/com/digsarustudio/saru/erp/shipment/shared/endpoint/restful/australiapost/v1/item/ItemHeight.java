/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The height of the parcel in centimetres. If volumetric pricing applies then this is mandatory.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface ItemHeight {
	public static final String DECIMAL_DELIMITER	= "\\.";
	public static final String DECIMAL_GLUE			= ".";
	
	/**
	 * 
	 * The builder for {@link ItemHeight}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemHeight> {
		Builder setHeight(Integer height);
		Builder setHeight(Integer height, Integer precision);
		Builder setHeight(String length);
		Builder setHeight(String height, String precision);
	}
	
	void setHeight(Integer height);
	void setHeight(Integer height, Integer precision);
	void setHeight(String length);
	void setHeight(String height, String precision);
	
	Integer getHeight();
	Integer getPrecision();
	String getDecimalHeight();
	
	Boolean validate();
}
