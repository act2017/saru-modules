/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The postage product of a courier company.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface PostageProduct {
	/**
	 * 
	 * The builder for {@link PostageProduct}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<PostageProduct> {
		Builder setCode(String code);
		Builder setName(String name);
	}
	
	void setCode(String code);
	void setName(String name);
	
	String getCode();
	String getName();
	
	Boolean validate();
}
