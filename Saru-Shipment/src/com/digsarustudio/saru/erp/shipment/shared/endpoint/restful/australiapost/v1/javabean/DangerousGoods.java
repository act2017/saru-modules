/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.Weight;
import com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsClassDivision;
import com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsPackagingType;
import com.digsarustudio.saru.erp.shipment.shared.ProperShippingName;
import com.digsarustudio.saru.erp.shipment.shared.Quantity;
import com.digsarustudio.saru.erp.shipment.shared.SubsidiaryRisk;
import com.digsarustudio.saru.erp.shipment.shared.UnNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackingGroupDesignator;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods} for {@link Shipment}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DangerousGoods implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods
									 , IsSerializable {
	/**
	 * [Mandatory]<br>
	 * Type: String
	 */
	public static final String	ATTR_UN_NUMBER					= "un_number";
	
	/**
	 * [Mandatory]<br>
	 * Type: String
	 */
	public static final String	ATTR_TECHICAL_NAME				= "technical_name";
	
	/**
	 * [Mandatory]<br>
	 * Type: String
	 */
	public static final String	ATTR_NET_WEIGHT					= "net_weight";
	
	/**
	 * [Mandatory]<br>
	 * Type: String
	 */
	public static final String	ATTR_CLASS_DIVISION				= "class_division";
	
	/**
	 * [Optional]<br>
	 * Type: String
	 */
	public static final String	ATTR_SUBSIDIARY_RISK			= "subsidiary_risk";
	
	/**
	 * [Optional]<br>
	 * Type: String
	 */
	public static final String	ATTR_PACKING_GROUP_DESIGNATOR	= "packing_group_designator";
	
	/**
	 * [Mandatory]<br>
	 * Type: String
	 */
	public static final String	ATTR_OUTER_PACKAGING_TYPE		= "outer_packaging_type";
	
	/**
	 * [Mandatory]<br>
	 * Type: String
	 */
	public static final String	ATTR_OUTER_PACKAGING_QUANTITY	= "outer_packaging_quantity";
	
	/**
	 * 
	 * The object builder for {@link DangerousGoods}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods.Builder {
		private DangerousGoods result = null;

		private Builder() {
			this.result = new DangerousGoods();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods.Builder#setUnNumber(com.digsarustudio.saru.erp.shipment.shared.UnNumber)
		 */
		@Override
		public Builder setUnNumber(UnNumber number) {
			this.result.setUnNumber(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods.Builder#setTechnicalName(com.digsarustudio.saru.erp.shipment.shared.ProperShippingName)
		 */
		@Override
		public Builder setTechnicalName(ProperShippingName name) {
			this.result.setTechnicalName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods.Builder#setNetWeight(com.digsarustudio.saru.erp.musaceae.shared.Weight)
		 */
		@Override
		public Builder setNetWeight(Weight weight) {
			this.result.setNetWeight(weight);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods.Builder#setClassDivision(com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsClassDivision)
		 */
		@Override
		public Builder setClassDivision(DangerousGoodsClassDivision classDivision) {
			this.result.setClassDivision(classDivision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods.Builder#setSubsidiaryRisk(com.digsarustudio.saru.erp.shipment.shared.SubsidiaryRisk)
		 */
		@Override
		public Builder setSubsidiaryRisk(SubsidiaryRisk subsidiaryRisk) {
			this.result.setSubsidiaryRisk(subsidiaryRisk);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods.Builder#setPackingGroupDesignator(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackingGroupDesignator)
		 */
		@Override
		public Builder setPackingGroupDesignator(PackingGroupDesignator designator) {
			this.result.setPackingGroupDesignator(designator);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods.Builder#setOuterPackingType(com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsPackagingType)
		 */
		@Override
		public Builder setOuterPackingType(DangerousGoodsPackagingType type) {
			this.result.setOuterPackingType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods.Builder#setOuterPackagingQuantity(com.digsarustudio.saru.erp.shipment.shared.Quantity)
		 */
		@Override
		public Builder setOuterPackagingQuantity(Quantity quantity) {
			this.result.setOuterPackagingQuantity(quantity);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DangerousGoods build() {
			return this.result;
		}

	}
	
	private UnNumber						un_number					= null;
	private ProperShippingName				technical_name				= null;
	private Weight							net_weight					= null;
	private DangerousGoodsClassDivision		class_division				= null;
	private SubsidiaryRisk					subsidiary_risk				= null;
	private PackingGroupDesignator			packing_group_designator	= null;
	private DangerousGoodsPackagingType		outer_packaging_type		= null;
	private Quantity						outer_packaging_quantity	= null;
	
	/**
	 * 
	 */
	private DangerousGoods() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods#setUnNumber(com.digsarustudio.saru.erp.shipment.shared.UnNumber)
	 */
	@Override
	public void setUnNumber(UnNumber number) {
		this.un_number = number;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods#getUnNumber()
	 */
	@Override
	public UnNumber getUnNumber() {
		
		return this.un_number;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods#setTechnicalName(com.digsarustudio.saru.erp.shipment.shared.ProperShippingName)
	 */
	@Override
	public void setTechnicalName(ProperShippingName name) {
		this.technical_name = name;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods#getTechnicalName()
	 */
	@Override
	public ProperShippingName getTechnicalName() {
		
		return this.technical_name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods#setNetWeight(com.digsarustudio.saru.erp.musaceae.shared.Weight)
	 */
	@Override
	public void setNetWeight(Weight weight) {
		this.net_weight = weight;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods#getNetWeight()
	 */
	@Override
	public Weight getNetWeight() {
		
		return this.net_weight;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods#setClassDivision(com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsClassDivision)
	 */
	@Override
	public void setClassDivision(DangerousGoodsClassDivision classDivision) {
		this.class_division = classDivision;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods#getClassDivision()
	 */
	@Override
	public DangerousGoodsClassDivision getClassDivision() {
		
		return this.class_division;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods#setSubsidiaryRisk(com.digsarustudio.saru.erp.shipment.shared.SubsidiaryRisk)
	 */
	@Override
	public void setSubsidiaryRisk(SubsidiaryRisk subsidiaryRisk) {
		this.subsidiary_risk = subsidiaryRisk;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods#getSubsidiaryRisk()
	 */
	@Override
	public SubsidiaryRisk getSubsidiaryRisk() {
		
		return this.subsidiary_risk;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods#setPackingGroupDesignator(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackingGroupDesignator)
	 */
	@Override
	public void setPackingGroupDesignator(PackingGroupDesignator designator) {
		this.packing_group_designator = designator;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods#getPackingGroupDesinator()
	 */
	@Override
	public PackingGroupDesignator getPackingGroupDesinator() {
		
		return this.packing_group_designator;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods#setOuterPackingType(com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsPackagingType)
	 */
	@Override
	public void setOuterPackingType(DangerousGoodsPackagingType type) {
		this.outer_packaging_type = type;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods#getOuterPackingType()
	 */
	@Override
	public DangerousGoodsPackagingType getOuterPackingType() {
		
		return this.outer_packaging_type;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods#setOuterPackagingQuantity(com.digsarustudio.saru.erp.shipment.shared.Quantity)
	 */
	@Override
	public void setOuterPackagingQuantity(Quantity quantity) {
		this.outer_packaging_quantity = quantity;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods#getOuterPackaginggQuantity()
	 */
	@Override
	public Quantity getOuterPackaginggQuantity() {
		
		return this.outer_packaging_quantity;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.un_number || !this.un_number.validate()) {
			return false;
		}
		
		if(null == this.technical_name || !this.technical_name.validate()) {
			return false;
		}
		
		if(null == this.net_weight || !this.net_weight.validate()) {
			return false;
		}
		
		if(null == this.class_division || !this.class_division.validate()) {
			return false;
		}
		
		if(null != this.subsidiary_risk && !this.subsidiary_risk.validate()) {
			return false;
		}
		
		if(null != this.packing_group_designator && !this.packing_group_designator.validate()) {
			return false;
		}
		
		if(null == this.outer_packaging_type || !this.outer_packaging_type.validate()) {
			return false;
		}
		
		if(null == this.outer_packaging_quantity || !this.outer_packaging_quantity.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
