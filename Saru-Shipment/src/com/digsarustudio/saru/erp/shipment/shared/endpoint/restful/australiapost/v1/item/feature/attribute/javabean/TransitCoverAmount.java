/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeature;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttribute;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The transit cover amount used by {@link ItemFeature} as an {@link ItemFeatureAttribute}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class TransitCoverAmount implements ItemFeatureAttribute, IsSerializable {
	public static final Integer MAX_LEN_MAIN 		= 4;
	public static final Integer	MAX_LEN_PRECISION	= 2;
	public static final String	DECIMAL_DELIMITER	= "\\.";
	public static final String	DECIMAL_GLUE		= ".";
	public static final String	NAME				= "cover_amount";

	/**
	 * 
	 * The object builder for {@link TransitCoverAmount}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ItemFeatureAttribute.Builder {
		private TransitCoverAmount result = null;

		private Builder() {
			this.result = new TransitCoverAmount();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/**
		 * @param main
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverAmount#setCoverAmount(java.lang.Integer)
		 */
		public Builder setCoverAmount(Integer main) {
			result.setCoverAmount(main);
			return this;
		}

		/**
		 * @param main
		 * @param precision
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverAmount#setCoverAmount(java.lang.Integer, java.lang.Integer)
		 */
		public Builder setCoverAmount(Integer main, Integer precision) {
			result.setCoverAmount(main, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute.Builder#setName(java.lang.String)
		 */
		@Override
		public Builder setName(String name) {
			this.result.setName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public TransitCoverAmount build() {
			return this.result;
		}

	}
	
	private String main			= null;
	private String precision	= null;
	private String name			= null;

	/**
	 * 
	 */
	private TransitCoverAmount() {
		this.name = NAME;
	}
	
	public void setCoverAmount(Integer main) {
		this.setValue(main.toString());
	}
	
	public void setCoverAmount(Integer main, Integer precision) {
		if(null == main || null == precision) {
			return;
		}
		
		this.main		= main.toString();
		this.precision	= precision.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		if(null == value || value.isEmpty()) {
			return;
		}
		
		String[] digits = value.split(DECIMAL_DELIMITER);

		this.main = digits[0];
		if( 2 == digits.length) {
			this.precision = digits[1];
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute#getValue()
	 */
	@Override
	public String getValue() {
		if(null == this.main && null == this.precision) {
			return null;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		if(null != this.main && !this.main.isEmpty() ) {
			buffer.append(this.main);
		}else {
			buffer.append("0");
		}
		
		if(null != this.precision) {
			buffer.append(DECIMAL_GLUE);
			buffer.append(this.precision);
		}

		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute#getName()
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.main && null == this.precision) {
			return false;
		}
		
		if(this.main.isEmpty() && this.precision.isEmpty()) {
			return false;
		}
		
		if( null != this.main 
			&& ( MAX_LEN_MAIN < this.main.length() ) ) {
			return false;
		}
		
		if( null != this.precision 
			&& (MAX_LEN_PRECISION < this.precision.length() ) ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
