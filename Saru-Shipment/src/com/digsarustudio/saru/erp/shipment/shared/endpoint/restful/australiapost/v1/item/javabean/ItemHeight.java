/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemHeight}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemHeight implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemHeight
								 , IsSerializable {
	public static final Integer	MAX_LEN_MAIN		= 4;
	public static final Integer	MAX_LEN_PRECISION	= 1;
	
	/**
	 * 
	 * The object builder for {@link ItemHeight}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemHeight.Builder {
		private ItemHeight result = null;

		private Builder() {
			this.result = new ItemHeight();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemHeight.Builder#setHeight(java.lang.Integer)
		 */
		@Override
		public Builder setHeight(Integer length) {
			this.result.setHeight(length);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemHeight.Builder#setHeight(java.lang.Integer, java.lang.Integer)
		 */
		@Override
		public Builder setHeight(Integer length, Integer precision) {
			this.result.setHeight(length, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemHeight.Builder#setHeight(java.lang.String)
		 */
		@Override
		public Builder setHeight(String length) {
			this.result.setHeight(length);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemHeight.Builder#setHeight(java.lang.String, java.lang.String)
		 */
		@Override
		public Builder setHeight(String height, String precision) {
			this.result.setHeight(height, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemHeight build() {
			return this.result;
		}

	}
	
	private String main			= null;
	private String precision	= null;
	
	/**
	 * 
	 */
	public ItemHeight() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemHeight#setHeight(java.lang.Integer)
	 */
	@Override
	public void setHeight(Integer length) {
		this.main = length.toString();
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemHeight#setHeight(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void setHeight(Integer length, Integer precision) {
		this.main		= length.toString();
		this.precision	= precision.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemHeight#setHeight(java.lang.String)
	 */
	@Override
	public void setHeight(String length) {
		if(null == length) {
			return;
		}
		
		String[] weights = length.split(DECIMAL_DELIMITER);
		
		String main = weights[0];
		String pre	= null;
		
		if(2 == weights.length) {
			pre = weights[1];
		}
		
		this.setHeight(main, pre);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemHeight#setHeight(java.lang.String, java.lang.String)
	 */
	@Override
	public void setHeight(String height, String precision) {
		this.main		= height;
		this.precision	= precision;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemHeight#getHeight()
	 */
	@Override
	public Integer getHeight() {
		return Integer.parseInt(this.main);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemHeight#getPrecision()
	 */
	@Override
	public Integer getPrecision() {
		return Integer.parseInt(this.precision);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemHeight#getDecimalHeight()
	 */
	@Override
	public String getDecimalHeight() {
		if(null == this.main && null == this.precision) {
			return null;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		if(null != this.main && !this.main.isEmpty()) {
			buffer.append(this.main);
		}else {
			buffer.append("0");
		}
		
		if(null != this.precision) {
			buffer.append(DECIMAL_GLUE);
			buffer.append(this.precision);
		}

		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemHeight#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.main && null == this.precision) {
			return false;
		}
		
		if(this.main.isEmpty() && this.precision.isEmpty()) {
			return false;
		}
		
		if( null != this.main 
			&& ( MAX_LEN_MAIN < this.main.length()) ) {
			return false;
		}
		
		if( null != this.precision 
			&& (MAX_LEN_PRECISION < this.precision.length()) ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
