/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorContext;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorContextPair;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The implementation of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError} 
 * in JDO form.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AusPostError implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError
									, IsSerializable{

	public static final String ATTR_CODE	= "code";
	public static final String ATTR_NAME	= "name";
	public static final String ATTR_MESSAGE	= "message";
	public static final String ATTR_CONTEXT	= "context";
	
	/**
	 * 
	 * The object builder for {@link AusPostError}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError.Builder {
		private AusPostError result = null;

		private Builder() {
			this.result = new AusPostError();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorResponse.Builder#setCode(java.lang.String)
		 */
		@Override
		public Builder setCode(String code) {
			this.result.setCode(code);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorResponse.Builder#setName(java.lang.String)
		 */
		@Override
		public Builder setName(String name) {
			this.result.setName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorResponse.Builder#setMessage(java.lang.String)
		 */
		@Override
		public Builder setMessage(String message) {
			this.result.setMessage(message);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorResponse.Builder#addContextPair(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorContextPair)
		 */
		@Override
		public Builder addContextPair(AusPostErrorContextPair pair) {
			this.result.addContextPair(pair);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorResponse.Builder#setContext(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorContext)
		 */
		@Override
		public Builder setContext(AusPostErrorContext context) {
			this.result.setContext(context);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AusPostError build() {
			return this.result;
		}

	}	
	
	private String			code		= null;
	private String			name		= null;
	private String			message		= null;
	private AusPostErrorContext	context		= null;
	
	/**
	 * 
	 */
	public AusPostError() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorResponse#getCode()
	 */
	@Override
	public String getCode() {

		return this.code;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorResponse#getName()
	 */
	@Override
	public String getName() {

		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorResponse#getMessage()
	 */
	@Override
	public String getMessage() {

		return this.message;
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorResponse#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.code = code;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorResponse#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorResponse#setMessage(java.lang.String)
	 */
	@Override
	public void setMessage(String message) {
		this.message = message;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorResponse#addContextPair(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorContextPair)
	 */
	@Override
	public void addContextPair(AusPostErrorContextPair pair) {
		if(null == pair) {
			return;
		}
		
		if(null == this.context) {
			this.context = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorContext.builder().build();
		}
		
		this.context.addPair(pair);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorResponse#setContext(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorContext)
	 */
	@Override
	public void setContext(AusPostErrorContext context) {
		this.context = context;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorResponse#getContext()
	 */
	@Override
	public AusPostErrorContext getContext() {
		return this.context;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorResponse#validate()
	 */
	@Override
	public Boolean validate() {
		if( null == this.code || this.code.isEmpty() ) {
			return false;
		}
		
		return true;
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
