/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains the dangerous goods subsidiary risk of sub class. This represent the subsidiary risk of a substance, 
 * for example, a product may have a primary risk of toxic (class 6.1) with a subsidiary risk of flammable liquid (class 3). 
 * This field is a decimal value, but must be passed as a string.<br>
 * Normally, the details of this subsidiary risk are from database.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://auspost.com.au/content/dam/auspost_corp/media/documents/dangerous-and-prohibited-goods-guide.pdf">Dangerous Goods Guide</a>
 *
 */
public class DangerousGoodsSubsidiaryRisks {
	public static final String CLASS_DELIMITER	= ".";
	
	public static final DangerousGoodsSubsidiaryRisks	FlammableLiquid	= new DangerousGoodsSubsidiaryRisks("3", "FlammableLiquid");

	
	private static Map<String, DangerousGoodsSubsidiaryRisks> VALUES = new HashMap<>();
		
	static {
		VALUES.put(FlammableLiquid.getClassification(), FlammableLiquid);
	}	
	
	private String 		 classification	= null;
	private String 		 name			= null;
	
	private DangerousGoodsSubsidiaryRisks() {
		
	}
	
	private DangerousGoodsSubsidiaryRisks(String classification, String name) {
		this.classification = classification;
		this.name			= name;
	}	
	
	/**
	 * @return the classification
	 */
	public String getClassification() {
		return classification;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	public static DangerousGoodsSubsidiaryRisks fromValue(String value) {
		if (null == value ) {
			return null;
		}
		
		if (!VALUES.containsKey(value)) {
			return new DangerousGoodsSubsidiaryRisks(value, "Unknown classification");
		}
		
		return VALUES.get(value);
	}
	
	public static DangerousGoodsSubsidiaryRisks getDefault() {
		return FlammableLiquid;
	}
		
}
