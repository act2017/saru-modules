/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContentCollection}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemContentCollection implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContentCollection
											, IsSerializable {
	public static final Integer	MAX_COUNT	= 20;
	
	
	/**
	 * 
	 * The object builder for {@link ItemContentCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContentCollection.Builder {
		private ItemContentCollection result = null;

		private Builder() {
			this.result = new ItemContentCollection();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContentCollection.Builder#addItemContent(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent)
		 */
		@Override
		public Builder addItemContent(ItemContent content) {
			this.result.addItemContent(content);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContentCollection.Builder#setItemCotents(java.util.List)
		 */
		@Override
		public Builder setItemCotents(List<ItemContent> contents) {
			this.result.setItemCotents(contents);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemContentCollection build() {
			return this.result;
		}

	}

	private List<ItemContent>	itemContents	= null;
	
	/**
	 * 
	 */
	private ItemContentCollection() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContentCollection#addItemContent(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent)
	 */
	@Override
	public void addItemContent(ItemContent content) {
		if(null == this.itemContents) {
			this.itemContents = new ArrayList<ItemContent>();
		}

		this.itemContents.add(content);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContentCollection#setItemCotents(java.util.List)
	 */
	@Override
	public void setItemCotents(List<ItemContent> contents) {
		this.itemContents = contents;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContentCollection#getItemContent(java.lang.Integer)
	 */
	@Override
	public ItemContent getItemContent(Integer index) {
		if(null == this.itemContents || index >= this.itemContents.size()) {
			return null;
		}
		
		return this.itemContents.get(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContentCollection#getItemContents()
	 */
	@Override
	public List<ItemContent> getItemContents() {

		return this.itemContents;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContentCollection#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.itemContents || this.itemContents.isEmpty()) {
			return false;
		}

		if(MAX_COUNT < this.itemContents.size()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
