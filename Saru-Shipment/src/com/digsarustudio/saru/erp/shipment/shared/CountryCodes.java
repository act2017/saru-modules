/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared;

import java.util.HashMap;
import java.util.Map;

/**
 * The 2 digits code for the countries.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class CountryCodes {
	public static final CountryCodes	Australia	= new CountryCodes("AU", "Australia");

	private static Map<String, CountryCodes> VALUES = new HashMap<>();
		
		static {
			VALUES.put(Australia.getValue(), Australia);
		}	
		
		private String value = null;
		private String name = null;
		
		private CountryCodes() {
			
		}
		
		private CountryCodes(String value, String name) {
			this.value = value;
			this.name = name;
		}
		
		public String getValue() {
			return this.value;
		}
		
		public String getDisplayString() {
			return this.name;
		}
		
		public static CountryCodes fromValue(String value) {
			if (null == value || !VALUES.containsKey(value)) {
				return null;
			}
			
			return VALUES.get(value);
		}
		
		public static CountryCodes fromName(String name) {
			CountryCodes found = null;
			
			for (Map.Entry<String, CountryCodes> code : VALUES.entrySet()) {
				if(null == code || null == code.getValue() || null == code.getValue().getDisplayString() || code.getValue().getDisplayString().isEmpty()) {
					continue;
				}
				
				found = code.getValue();
				break;
			}
			
			return found;
		}
}
