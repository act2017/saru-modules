/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelFormatLayouts;

/**
 * The label layout printed in A4 paper and 1 printed per page in landscape.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelFormatLayoutA4_1pp_Landscape extends LabelFormatLayout {

	/**
	 * 
	 */
	public LabelFormatLayoutA4_1pp_Landscape() {
		this.setLayout(LabelFormatLayouts.A4_1pp_Landscape);
	}

}
