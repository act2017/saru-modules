/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.TariffCode}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class TariffCode implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.TariffCode
								 , IsSerializable {
	public static final Integer	MIN_LEN	= 6;
	public static final Integer	MAX_LEN	= 12;
	
	/**
	 * 
	 * The object builder for {@link TariffCode}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.TariffCode.Builder {
		private TariffCode result = null;

		private Builder() {
			this.result = new TariffCode();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.TariffCode.Builder#setCode(java.lang.Integer)
		 */
		@Override
		public Builder setCode(Integer code) {
			this.result.setCode(code);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.TariffCode.Builder#setCode(java.lang.String)
		 */
		@Override
		public Builder setCode(String code) {
			this.result.setCode(code);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public TariffCode build() {
			return this.result;
		}

	}
	
	private String code	= null;
	
	/**
	 * 
	 */
	private TariffCode() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.TariffCode#setCode(java.lang.Integer)
	 */
	@Override
	public void setCode(Integer code) {
		if(null == code) {
			return;
		}
		
		this.setCode(code.toString());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.TariffCode#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.code = code;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.TariffCode#getCodeString()
	 */
	@Override
	public String getCodeString() {
		return this.code;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.TariffCode#getCode()
	 */
	@Override
	public Integer getCode() {
		if(null == this.code) {
			return null;
		}
		
		return Integer.parseInt(this.code);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.TariffCode#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.code || this.code.isEmpty()) {
			return false;
		}
		
		if( MIN_LEN > this.code.length() ) {
			return false;
		}
		
		if( MAX_LEN < this.code.length() ) {
			return false;
		}
		
		return null;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
