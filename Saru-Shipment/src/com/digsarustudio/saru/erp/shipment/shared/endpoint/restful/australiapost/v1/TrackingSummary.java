/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import java.util.Map;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The number of items in the shipment for each delivery status. 
 * The delivery status will be listed with the number of items in the shipment where the delivery status has been applied.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface TrackingSummary {
	/**
	 * 
	 * The builder for {@link TrackingSummary}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<TrackingSummary> {
		Builder addPair(TrackingSummaryPair summary);
		Builder addPair(String key, String value);
	}
	
	void addPair(TrackingSummaryPair summary);
	void addPair(String key, String value);
	
	TrackingSummaryPair getPair(String key);
	Map<String, TrackingSummaryPair> getPairs();
	
	Boolean validate();
}
