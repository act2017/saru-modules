/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean;

import com.digsarustudio.saru.erp.pac.shared.endpoint.australiapost.postage.calculate.parcel.DeliveryTime;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The window used to indicate the start time and the end time of the {@link DeliveryTime}.<br>
 * start - Start time of window. Time should be supplied in the following format hh24:mm:ss and should be before the end time.<br>
 * end - End time of window. Time should be supplied in the following format hh24:mm:ss and should be after the start time.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
 */
public class DeliveryWindow implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.DeliveryWindow
									 , IsSerializable {
	public static final String ATTR_START	= "start";
	public static final String ATTR_END		= "end";
	
	/**
	 * 
	 * The object builder for {@link DeliveryWindow}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.DeliveryWindow.Builder {
		private DeliveryWindow result = null;

		private Builder() {
			this.result = new DeliveryWindow();
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.DeliveryWindow.Builder#setStartTime(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time)
		 */
		@Override
		public Builder setStartTime(Time time) {
			this.result.setStartTime(time);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.DeliveryWindow.Builder#setEndTime(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time)
		 */
		@Override
		public Builder setEndTime(Time time) {
			this.result.setEndTime(time);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DeliveryWindow build() {
			return this.result;
		}

	}

	private Time	start	= null;
	private Time	end		= null;
	
	/**
	 * 
	 */
	private DeliveryWindow() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.DeliveryWindow#setStartTime(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time)
	 */
	@Override
	public void setStartTime(Time time) {
		this.start = time;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.DeliveryWindow#setEndTime(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time)
	 */
	@Override
	public void setEndTime(Time time) {
		this.end = time;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.DeliveryWindow#getStartTime()
	 */
	@Override
	public Time getStartTime() {
		
		return this.start;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.DeliveryWindow#getEndTime()
	 */
	@Override
	public Time getEndTime() {
		
		return this.end;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.DeliveryWindow#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.start || !this.start.validate()) {
			return false;
		}
		
		if(null == this.end || !this.end.validate()) {
			return false;
		}
		
		return true;
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
