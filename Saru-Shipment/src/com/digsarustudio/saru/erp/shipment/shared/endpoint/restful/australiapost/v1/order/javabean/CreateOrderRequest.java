/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean;

import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Consignor;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.PaymentMethod;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class CreateOrderRequest implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest
										  , IsSerializable {
	public static final	String	ATTR_ORDER_REFERENCE	= "order_reference";
	public static final	String	ATTR_PAYMENT_METHOD		= "payment_method";
	public static final	String	ATTR_CONSIGNOR			= "consignor";
	public static final	String	ATTR_SHIPMENTS			= "shipments";

	/**
	 * 
	 * The object builder for {@link CreateOrderRequest}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest.Builder {
		private CreateOrderRequest result = null;

		private Builder() {
			this.result = new CreateOrderRequest();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest.Builder#setOrderReference(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderReference)
		 */
		@Override
		public Builder setOrderReference(OrderReference reference) {
			this.result.setOrderReference(reference);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest.Builder#setPaymentMethod(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.PaymentMethod)
		 */
		@Override
		public Builder setPaymentMethod(PaymentMethod method) {
			this.result.setPaymentMethod(method);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest.Builder#setConsignor(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Consignor)
		 */
		@Override
		public Builder setConsignor(Consignor consignor) {
			this.result.setConsignor(consignor);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest.Builder#addShipment(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment)
		 */
		@Override
		public Builder addShipment(Shipment shipment) {
			this.result.addShipment(shipment);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest.Builder#setShipments(java.util.List)
		 */
		@Override
		public Builder setShipments(List<Shipment> shipments) {
			this.result.setShipments(shipments);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest.Builder#setShipments(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection)
		 */
		@Override
		public Builder setShipments(ShipmentCollection shipments) {
			this.result.setShipments(shipments);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public CreateOrderRequest build() {
			return this.result;
		}

	}
	
	private	OrderReference		order_reference	= null;
	private PaymentMethod		payment_method	= null;
	private Consignor			consignor		= null;
	private ShipmentCollection	shipments		= null;
	
	/**
	 * 
	 */
	private CreateOrderRequest() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest#setOrderReference(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderReference)
	 */
	@Override
	public void setOrderReference(OrderReference reference) {
		this.order_reference = reference;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest#setPaymentMethod(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.PaymentMethod)
	 */
	@Override
	public void setPaymentMethod(PaymentMethod method) {
		this.payment_method = method;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest#setConsignor(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Consignor)
	 */
	@Override
	public void setConsignor(Consignor consignor) {
		this.consignor = consignor;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest#addShipment(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment)
	 */
	@Override
	public void addShipment(Shipment shipment) {
		if(null == shipment) {
			return;
		}else if(null == this.shipments) {
			this.shipments = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentCollection.builder().build();
		}

		this.shipments.addShipment(shipment);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest#setShipments(java.util.List)
	 */
	@Override
	public void setShipments(List<Shipment> shipments) {
		if(null == shipments) {
			return;
		}

		for (Shipment shipment : shipments) {
			this.addShipment(shipment);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest#setShipments(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection)
	 */
	@Override
	public void setShipments(ShipmentCollection shipments) {
		this.shipments = shipments;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest#getOrderReference()
	 */
	@Override
	public OrderReference getOrderReference() {
		
		return this.order_reference;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest#getPaymentMethod()
	 */
	@Override
	public PaymentMethod getPaymentMethod() {
		
		return this.payment_method;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest#getConsignor()
	 */
	@Override
	public Consignor getConsignor() {
		
		return this.consignor;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest#getShipment(java.lang.Integer)
	 */
	@Override
	public Shipment getShipment(Integer index) {
		if(null == this.shipments || null == index || index >= this.shipments.size()) {
			return null;
		}
		
		return this.shipments.getShipment(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest#getShipments()
	 */
	@Override
	public ShipmentCollection getShipments() {
		
		return this.shipments;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest#validate()
	 */
	@Override
	public Boolean validate() {
		if(null != this.order_reference && !this.order_reference.validate()) {
			return false;
		}
		
		if(null != this.payment_method && !this.payment_method.validate()) {
			return false;
		}
		
		if(null != this.consignor && !this.consignor.validate()) {
			return false;
		}
		
		if(null == this.shipments ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
