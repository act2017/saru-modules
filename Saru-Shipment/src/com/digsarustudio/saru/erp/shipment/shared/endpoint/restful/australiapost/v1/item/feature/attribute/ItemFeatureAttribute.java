/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeature;

/**
 * The attribute of {@link ItemFeature}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface ItemFeatureAttribute {
	/**
	 * 
	 * The builder for {@link ItemFeatureAttribute}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemFeatureAttribute> {
		Builder setValue(String value);
		Builder setName(String name);
	}
		
	void setValue(String value);
	String getValue();
	
	void setName(String name);
	String getName();
	
	Boolean validate();
}
