/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.NonDeliveryActions;

/**
 * Your desired action if the delivery cannot be completed. 
 * You can specify "RETURN" or "ABANDONED". 
 * The default if not specified is to return the item by the most economical route or by air.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface NonDeliveryAction {
	/**
	 * 
	 * The builder for {@link NonDeliveryAction}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<NonDeliveryAction> {
		Builder setAction(NonDeliveryActions action);
		Builder setAction(String action);
	}
	
	void setAction(NonDeliveryActions action);
	void setAction(String action);
	
	String getAction();
	NonDeliveryActions getNonDeliveryAction();
	
	Boolean validate();
}
