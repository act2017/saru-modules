/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.PackingGroupDesignators;

/**
 * The packaging type for this item. Some example values are: CTN, PAL, SAT, BAG, ENV, ITM, JIF, SKI
 * Note, for StarTrack products, the packaging type (refereed to as a unit type by StarTrack) is mandatory 
 * and has a maximum length of 3.<br>
 * <br>
 * The type might be passed from database.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 */
public interface PackingGroupDesignator {
	/**
	 * 
	 * The builder for {@link PackingGroupDesignator}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<PackingGroupDesignator> {
		Builder setValue(PackingGroupDesignators value);
		Builder setValue(String value);
	}
	
	void setValue(PackingGroupDesignators value);
	void setValue(String value);
	
	PackingGroupDesignators getPackingGroupDesignator();
	String getValue();
	
	Boolean validate();
}
