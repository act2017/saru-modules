/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * If the shipment is part of an order, The order id will be populated.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface OrderId {
	/**
	 * 
	 * The builder for {@link OrderId}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<OrderId> {
		Builder setValue(String id);
	}
	
	void setValue(String id);
	String getValue();
	
	Boolean validate();
}
