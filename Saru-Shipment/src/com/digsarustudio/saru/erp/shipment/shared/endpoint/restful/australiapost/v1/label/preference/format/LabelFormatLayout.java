/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelFormatLayouts;

/**
 * The layout specifies the number of labels per page and the size of the page. 
 * The layout must be valid for the product types of the items in the shipment. 
 * Please refer to the <a href="https://developers.auspost.com.au/apis/shipping-and-tracking/info/api-resources/labels">Labels page</a> 
 * in our API Resources section.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface LabelFormatLayout {
	/**
	 * 
	 * The builder for {@link LabelFormatLayout}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<LabelFormatLayout> {
		Builder setLayout(String layout);
		Builder setLayout(LabelFormatLayouts layout);
	}
	
	void setLayout(String layout);
	void setLayout(LabelFormatLayouts layout);
	
	String getLayout();
	LabelFormatLayouts getLabelFormatLayouts();
	
	Boolean validate();
}
