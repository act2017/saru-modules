/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * A code related to the generation progress. Currently, the only code returned is 'Request actioned'.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface LabelGenerationProgressCode {
	/**
	 * 
	 * The builder for {@link LabelGenerationProgressCode}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<LabelGenerationProgressCode> {
		Builder setValue(String value);
	}
	
	void setValue(String value);
	String getValue();
	
	Boolean validate();

}
