/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelBranded;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatGroup;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatLayout;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatOffset;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelFormat implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat
								  , IsSerializable {
	public static final String	ATTR_GROUP		= "group";
	public static final String	ATTR_LAYOUT		= "layout";
	public static final String	ATTR_BRANDED	= "branded";
	public static final String	ATTR_LEFT_OFFSET= "left_offset";
	public static final String	ATTR_TOP_OFFSET	= "top_offset";
	
	/**
	 * 
	 * The object builder for {@link LabelFormat}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat.Builder {
		private LabelFormat result = null;

		private Builder() {
			this.result = new LabelFormat();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat.Builder#setGroup(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatGroup)
		 */
		@Override
		public Builder setGroup(LabelFormatGroup group) {
			this.result.setGroup(group);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat.Builder#setLayout(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatLayout)
		 */
		@Override
		public Builder setLayout(LabelFormatLayout layout) {
			this.result.setLayout(layout);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat.Builder#setLabelBranded(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelBranded)
		 */
		@Override
		public Builder setLabelBranded(LabelBranded branded) {
			this.result.setLabelBranded(branded);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat.Builder#setLeftOffset(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatOffset)
		 */
		@Override
		public Builder setLeftOffset(LabelFormatOffset offset) {
			this.result.setLeftOffset(offset);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat.Builder#setTopOffset(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatOffset)
		 */
		@Override
		public Builder setTopOffset(LabelFormatOffset offset) {
			this.result.setTopOffset(offset);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public LabelFormat build() {
			return this.result;
		}

	}

	private LabelFormatGroup	group		= null;
	private LabelFormatLayout	layout		= null;
	private	LabelBranded		branded		= null;
	private	LabelFormatOffset	left_offset	= null;
	private	LabelFormatOffset	top_offset	= null;
	
	/**
	 * 
	 */
	private LabelFormat() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat#setGroup(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatGroup)
	 */
	@Override
	public void setGroup(LabelFormatGroup group) {
		this.group = group;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat#setLayout(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatLayout)
	 */
	@Override
	public void setLayout(LabelFormatLayout layout) {
		this.layout = layout;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat#setLabelBranded(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelBranded)
	 */
	@Override
	public void setLabelBranded(LabelBranded branded) {
		this.branded = branded;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat#setLeftOffset(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatOffset)
	 */
	@Override
	public void setLeftOffset(LabelFormatOffset offset) {
		this.left_offset = offset;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat#setTopOffset(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatOffset)
	 */
	@Override
	public void setTopOffset(LabelFormatOffset offset) {
		this.top_offset = offset;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat#getGroup()
	 */
	@Override
	public LabelFormatGroup getGroup() {
		
		return this.group;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat#getLayout()
	 */
	@Override
	public LabelFormatLayout getLayout() {
		
		return this.layout;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat#getLabelBranded()
	 */
	@Override
	public LabelBranded getLabelBranded() {
		
		return this.branded;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat#getLeftOffset()
	 */
	@Override
	public LabelFormatOffset getLeftOffset() {
		
		return this.left_offset;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat#getTopOffset()
	 */
	@Override
	public LabelFormatOffset getTopOffset() {
		
		return this.top_offset;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.group || !this.group.validate()) {
			return false;
		}
		
		if(null == this.layout || !this.layout.validate()) {
			return false;
		}
		
		if(null == this.branded || !this.branded.validate()) {
			return false;
		}
		
		if(null == this.left_offset || !this.left_offset.validate()) {
			return false;
		}
		
		if(null == this.top_offset || !this.top_offset.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
