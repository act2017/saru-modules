/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentQuantity}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemContentQuantity implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentQuantity
										  , IsSerializable {
	/**
	 * 
	 * The object builder for {@link ItemContentQuantity}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentQuantity.Builder {
		private ItemContentQuantity result = null;

		private Builder() {
			this.result = new ItemContentQuantity();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentQuantity.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentQuantity.Builder#setValue(java.lang.Integer)
		 */
		@Override
		public Builder setValue(Integer value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemContentQuantity build() {
			return this.result;
		}

	}
	
	private Integer	quantity	= null;
	
	/**
	 * 
	 */
	private ItemContentQuantity() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentQuantity#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		if(null == value) {
			return;
		}
		
		this.setValue(Integer.parseInt(value));
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentQuantity#setValue(java.lang.Integer)
	 */
	@Override
	public void setValue(Integer value) {
		this.quantity = value;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentQuantity#getValue()
	 */
	@Override
	public Integer getValue() {

		return this.quantity;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentQuantity#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.quantity) {
			return false;
		}
		
		return true;
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}

}
