/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference;

import java.util.HashMap;
import java.util.Map;

/**
 * For printing preferences the type is "PRINT".
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelPreferenceTypes {
	public static final LabelPreferenceTypes	Print	= new LabelPreferenceTypes("PRINT", "Print");

	private static Map<String, LabelPreferenceTypes> VALUES = new HashMap<>();
		
	static {
		VALUES.put(Print.getValue(), Print);
	}	
	
	private String value = null;
	private String display = null;
	
	private LabelPreferenceTypes() {
		
	}
	
	private LabelPreferenceTypes(String value, String display) {
		this.value = value;
		this.display = display;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public String getDisplayString() {
		return this.display;
	}
	
	public static LabelPreferenceTypes fromValue(String value) {
		if (null == value || !VALUES.containsKey(value)) {
			return null;
		}
		
		return VALUES.get(value);
	}
	
	public static LabelPreferenceTypes getDefault() {
		return Print;
	}
		

}
