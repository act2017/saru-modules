/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentPagination}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ShipmentPagination implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentPagination
										 , IsSerializable {
	/**
	 * The total number of shipment records that can be returned.<br>
	 */
	public static final String	ATTR_TOTAL_NUMBER_OF_RECORDS	= "total_number_of_records";
	
	/**
	 * The number of shipment records per page, that you requested.<br>
	 */
	public static final String	ATTR_NUMBER_OF_RECORDS_PER_PAGE	= "number_of_records_per_page";
	
	/**
	 * 	The current page number you requested.<br>
	 */
	public static final String	ATTR_CURRENT_PAGE_NUMBER		= "current_page_number";
	
	/**
	 * 
	 * The object builder for {@link ShipmentPagination}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentPagination.Builder {
		private ShipmentPagination result = null;

		private Builder() {
			this.result = new ShipmentPagination();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentPagination.Builder#setTotalNumberOfRecords(java.lang.Integer)
		 */
		@Override
		public Builder setTotalNumberOfRecords(Integer number) {
			this.result.setTotalNumberOfRecords(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentPagination.Builder#setNumberOfRecordsPerPage(java.lang.Integer)
		 */
		@Override
		public Builder setNumberOfRecordsPerPage(Integer number) {
			this.result.setNumberOfRecordsPerPage(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentPagination.Builder#setCurrentPageNumber(java.lang.Integer)
		 */
		@Override
		public Builder setCurrentPageNumber(Integer number) {
			this.result.setCurrentPageNumber(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ShipmentPagination build() {
			return this.result;
		}

	}

	private	Integer	total_number_of_records		= null;
	private	Integer	number_of_records_per_page	= null;
	private	Integer	current_page_number			= null;
	
	/**
	 * 
	 */
	private ShipmentPagination() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentPagination#setTotalNumberOfRecords(java.lang.Integer)
	 */
	@Override
	public void setTotalNumberOfRecords(Integer number) {
		this.total_number_of_records = number;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentPagination#getTotalNumberOfRecords()
	 */
	@Override
	public Integer getTotalNumberOfRecords() {
		
		return this.total_number_of_records;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentPagination#setNumberOfRecordsPerPage(java.lang.Integer)
	 */
	@Override
	public void setNumberOfRecordsPerPage(Integer number) {
		this.number_of_records_per_page = number;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentPagination#getNumberOfRecordsPerPage()
	 */
	@Override
	public Integer getNumberOfRecordsPerPage() {
		
		return this.number_of_records_per_page;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentPagination#setCurrentPageNumber(java.lang.Integer)
	 */
	@Override
	public void setCurrentPageNumber(Integer number) {
		this.current_page_number = number;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentPagination#getCurrentPageNumber()
	 */
	@Override
	public Integer getCurrentPageNumber() {
		
		return this.current_page_number;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentPagination#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.total_number_of_records && null == this.number_of_records_per_page && null == this.current_page_number) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
