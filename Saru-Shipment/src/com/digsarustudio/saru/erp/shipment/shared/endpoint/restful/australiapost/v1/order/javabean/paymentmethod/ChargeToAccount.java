/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.paymentmethod;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.PaymentMethods;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean.PaymentMethod;

/**
 * The payment method for the Australia Post Merchant to pay by an account.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ChargeToAccount extends PaymentMethod {

	/**
	 * 
	 */
	public ChargeToAccount() {
		this.setValue(PaymentMethods.ChargeToAccount.getValue());
	}

}
