/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalGst;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummary;

/**
 * 	Summary pricing and shipment information contained in the order..<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-order-from-shipments">Create Order From Shipments</a>
 *
 */
public interface OrderSummary {
	/**
	 * 
	 * The builder for {@link OrderSummary}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<OrderSummary> {
		Builder setTotalCost(TotalCost cost);
		Builder setTotalGST(TotalGst gst);
		Builder setStatus(OrderShipmentItemStatus status);
		Builder setNumberOfShipments(Integer number);
		Builder setTrackingSummaries(TrackingSummary summaries);
	}
	
	
	/**
	 * <ul>
	 * 	<li>Type: Decimal</li>
	 * 	<li>Length: N.2</li>
	 * </ul>
	 * <br>
	 * The total price of the shipment, including goods and services tax (G.S.T).<br>
	 * 
	 * @return The total price of the shipment, including goods and services tax (G.S.T).
	 */
	TotalCost getTotalCost();
	void setTotalCost(TotalCost cost);
	
	/**
	 * <ul>
	 * 	<li>Type: Decimal</li>
	 * 	<li>Length: N.2</li>
	 * </ul>
	 * <br>
	 * The goods and services tax amount included in the total cost.<br>
	 * 
	 * @return The goods and services tax amount included in the total cost.
	 */
	TotalGst getTotalGST();
	void setTotalGST(TotalGst gst);
	
	/**
	 * The status of the item.<br>
	 * 
	 * @return The status of the item.
	 */
	OrderShipmentItemStatus getStatus();
	void setStatus(OrderShipmentItemStatus status);
	
	/**
	 * The number of shipments within the order.
	 * 
	 * @return The number of shipments within the order.
	 */
	Integer getNumberOfShipments();
	void setNumberOfShipments(Integer number);
	
	/**
	 * The number of items in the shipment for each delivery status. 
	 * The delivery status will be listed with the number of items in the shipment where the delivery status 
	 * has been applied.
	 * 
	 * @return The number of items in the shipment for each delivery status.
	 */
	TrackingSummary getTrackingSummaries();
	void setTrackingSummaries(TrackingSummary summaries);
	
	Boolean validate();
}
