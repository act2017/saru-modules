/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelFormatGroups;

/**
 * This class represents the label format - Australia Post Returns.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelFormatGroupAustraliaPostReturns extends LabelFormatGroup {
	public static final LabelFormatLayout	A4_4pp				= new LabelFormatLayoutA4_4pp();
	public static final LabelFormatLayout	A4_1pp				= new LabelFormatLayoutA4_1pp();
	public static final LabelFormatLayout	A6_1pp				= new LabelFormatLayoutA6_1pp();
	
	public LabelFormatGroupAustraliaPostReturns() {
		this.setValue(LabelFormatGroups.AustraliaPostReturns);
	}
}
