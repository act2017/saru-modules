/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.AustralianPostcode;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The jvavabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SuburbValidationRequest implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest
											  , IsSerializable {
	/**
	 * 
	 * The object builder for {@link SuburbValidationRequest}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest.Builder {
		private SuburbValidationRequest result = null;

		private Builder() {
			this.result = new SuburbValidationRequest();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest.Builder#setSuburb(java.lang.String)
		 */
		@Override
		public Builder setSuburb(String suburb) {
			this.result.setSuburb(suburb);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest.Builder#setSuburb(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb)
		 */
		@Override
		public Builder setSuburb(Suburb suburb) {
			this.result.setSuburb(suburb);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest.Builder#setState(java.lang.String)
		 */
		@Override
		public Builder setState(String state) {
			this.result.setState(state);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest.Builder#setState(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State)
		 */
		@Override
		public Builder setState(State state) {
			this.result.setState(state);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest.Builder#setPostcode(java.lang.String)
		 */
		@Override
		public Builder setPostcode(String postcode) {
			this.result.setPostcode(postcode);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest.Builder#setPostcode(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode)
		 */
		@Override
		public Builder setPostcode(Postcode postcode) {
			this.result.setPostcode(postcode);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public SuburbValidationRequest build() {
			return this.result;
		}

	}
	
	private String		suburb	= null;
	private String		state	= null;
	private	String		postcode	= null;
	
	/**
	 * 
	 */
	private SuburbValidationRequest() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest#setSuburb(java.lang.String)
	 */
	@Override
	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest#setSuburb(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb)
	 */
	@Override
	public void setSuburb(Suburb suburb) {
		if(null == suburb || !suburb.validate()) {
			return;
		}

		this.suburb = suburb.getName();
		if(null == this.suburb || this.suburb.isEmpty()) {
			this.suburb = suburb.getCode();
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest#setState(java.lang.String)
	 */
	@Override
	public void setState(String state) {
		this.state = state;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest#setState(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State)
	 */
	@Override
	public void setState(State state) {
		if(null == state || !state.validate()) {
			return;
		}

		this.state = state.getCode();
		if(null == this.state || this.state.isEmpty()) {
			this.state = state.getName();
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest#setPostcode(java.lang.String)
	 */
	@Override
	public void setPostcode(String postcode) {
		this.postcode = postcode;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest#setPostcode(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode)
	 */
	@Override
	public void setPostcode(Postcode postcode) {
		if(null == postcode || !postcode.validate()) {
			return;
		}

		this.postcode = postcode.getCode();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest#getSuburb()
	 */
	@Override
	public Suburb getSuburb() {
		if(!this.hasSuburb()) {
			return null;
		}
		
		return com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.Suburb.builder().setName(this.suburb)
																									   .build();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest#getSuburbString()
	 */
	@Override
	public String getSuburbString() {		
		return this.suburb;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest#getState()
	 */
	@Override
	public State getState() {
		if(!this.hasState()) {
			return null;
		}
		
		
		return com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.State.builder().setCode(this.state)
																									  .build();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest#getStateString()
	 */
	@Override
	public String getStateString() {
		return this.state;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest#getPostcode()
	 */
	@Override
	public Postcode getPostcode() {
		if(!this.hasPostcode()) {
			return null;
		}
		
		return AustralianPostcode.builder().setCode(this.postcode)
										   .build();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest#getPostcodeString()
	 */
	@Override
	public String getPostcodeString() {
		return this.postcode;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationRequest#validate()
	 */
	@Override
	public Boolean validate() {
		if(!this.hasSuburb() || !this.hasState() || !this.hasPostcode()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	private Boolean hasSuburb() {
		return (null != this.suburb && !this.suburb.isEmpty());
	}
	
	private Boolean hasState() {
		return (null != this.state && !this.state.isEmpty());
	}
	
	private Boolean hasPostcode() {
		return (null != this.postcode && !this.postcode.isEmpty());
	}
	
}
