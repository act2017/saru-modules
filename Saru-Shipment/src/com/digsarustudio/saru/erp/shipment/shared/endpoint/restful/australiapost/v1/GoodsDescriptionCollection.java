/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Description of the goods. This is printed on the auxiliary label. 
 * A maximum of 3 lines, each with a maximum of 40 characters.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface GoodsDescriptionCollection {
	/**
	 * 
	 * The builder for {@link GoodsDescriptionCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<GoodsDescriptionCollection> {
		Builder addDescription(GoodsDescription description);
		Builder setDescriptions(List<GoodsDescription> descriptions);
		Builder setDescriptions(GoodsDescription[] descriptions);
	}
	
	void addDescription(GoodsDescription description);
	void setDescriptions(List<GoodsDescription> descriptions);
	void setDescriptions(GoodsDescription[] descriptions);
	
	GoodsDescription getDescription(Integer index);
	List<GoodsDescription> getDescriptions();
	Integer size();
	
	Boolean validate();
}
