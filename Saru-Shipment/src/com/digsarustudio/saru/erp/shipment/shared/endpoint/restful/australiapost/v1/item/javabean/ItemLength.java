/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemLength}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemLength implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemLength
								 , IsSerializable {
	public static final Integer	MAX_LEN_MAIN		= 4;
	public static final Integer	MAX_LEN_PRECISION	= 1;
	
	/**
	 * 
	 * The object builder for {@link ItemLength}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemLength.Builder {
		private ItemLength result = null;

		private Builder() {
			this.result = new ItemLength();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemLength.Builder#setLength(java.lang.Integer)
		 */
		@Override
		public Builder setLength(Integer length) {
			this.result.setLength(length);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemLength.Builder#setLength(java.lang.Integer, java.lang.Integer)
		 */
		@Override
		public Builder setLength(Integer length, Integer precision) {
			this.result.setLength(length, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemLength.Builder#setLength(java.lang.String)
		 */
		@Override
		public Builder setLength(String length) {
			this.result.setLength(length);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemLength.Builder#setLength(java.lang.String, java.lang.String)
		 */
		@Override
		public Builder setLength(String length, String precision) {
			this.result.setLength(length, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemLength build() {
			return this.result;
		}

	}
	
	private String main			= null;
	private String precision	= null;
	
	/**
	 * 
	 */
	public ItemLength() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemLength#setLength(java.lang.Integer)
	 */
	@Override
	public void setLength(Integer length) {
		this.setLength(length, null);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemLength#setLength(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void setLength(Integer length, Integer precision) {
		String main = (null == length) ? null : length.toString();
		String pre	= (null == precision) ? null : precision.toString();
		
		this.setLength(main, pre);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemLength#setLength(java.lang.String)
	 */
	@Override
	public void setLength(String length) {
		if(null == length) {
			return;
		}
		
		String[] weights = length.split(DECIMAL_DELIMITER);
		
		String main = weights[0];
		String pre	= null;
		
		if(2 == weights.length) {
			pre = weights[1];
		}
		
		this.setLength(main, pre);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemLength#setLength(java.lang.String, java.lang.String)
	 */
	@Override
	public void setLength(String length, String precision) {
		this.main = length;
		this.precision = precision;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemLength#getLength()
	 */
	@Override
	public Integer getLength() {
		if(null == this.main) {
			return null;
		}
		
		return Integer.parseInt(this.main);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemLength#getPrecision()
	 */
	@Override
	public Integer getPrecision() {
		if(null == this.precision) {
			return null;
		}
		
		return Integer.parseInt(this.precision);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemLength#getDecimalLength()
	 */
	@Override
	public String getDecimalLength() {
		if(null == this.main && null == this.precision) {
			return null;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		if(null != this.main && !this.main.isEmpty()) {
			buffer.append(this.main);
		}else {
			buffer.append("0");
		}
		
		if(null != this.precision) {
			buffer.append(DECIMAL_GLUE);
			buffer.append(this.precision);
		}

		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemLength#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.main && null == this.precision) {
			return false;
		}
		
		if(this.main.isEmpty() && this.precision.isEmpty()) {
			return false;
		}
		
		if( null != this.main 
			&& ( MAX_LEN_MAIN < this.main.length()) ) {
			return false;
		}
		
		if( null != this.precision 
			&& (MAX_LEN_PRECISION < this.precision.length()) ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
