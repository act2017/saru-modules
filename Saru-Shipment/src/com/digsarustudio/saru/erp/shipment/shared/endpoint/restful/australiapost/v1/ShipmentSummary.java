/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Summary pricing information for the shipment.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface ShipmentSummary {
	/**
	 * 
	 * The builder for {@link ShipmentSummary}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ShipmentSummary> {
		Builder setTotalCost(TotalCost cost);
		Builder setTotalGST(TotalGst gst);
		Builder setStatus(OrderShipmentItemStatus status);
		Builder setNumberOfItems(Integer number);
		Builder setTrackingSummaries(TrackingSummary summaries);
		Builder setFreightCharge(FreightCharge charge);
		Builder setDiscount(Discount discount);
		Builder setTransitCover(TransitCover cover);
		Builder setSecuritySurcharge(SecuritySurcharge charge);
		Builder setFuelSurcharge(FuelSurcharge charge);
	}
	
	
	/**
	 * <ul>
	 * 	<li>Type: Decimal</li>
	 * 	<li>Length: N.2</li>
	 * </ul>
	 * <br>
	 * The total price of the shipment, including goods and services tax (G.S.T).<br>
	 * 
	 * @return The total price of the shipment, including goods and services tax (G.S.T).
	 */
	TotalCost getTotalCost();
	void setTotalCost(TotalCost cost);
	
	/**
	 * <ul>
	 * 	<li>Type: Decimal</li>
	 * 	<li>Length: N.2</li>
	 * </ul>
	 * <br>
	 * The goods and services tax amount included in the total cost.<br>
	 * 
	 * @return The goods and services tax amount included in the total cost.
	 */
	TotalGst getTotalGST();
	void setTotalGST(TotalGst gst);
	
	/**
	 * The status of the item.<br>
	 * 
	 * @return The status of the item.
	 */
	OrderShipmentItemStatus getStatus();
	void setStatus(OrderShipmentItemStatus status);
	
	/**
	 * The number of items within the shipment.
	 * 
	 * @return The number of items within the shipment.
	 */
	Integer getNumberOfItems();
	void setNumberOfItems(Integer number);
	
	/**
	 * The number of items in the shipment for each delivery status. 
	 * The delivery status will be listed with the number of items in the shipment where the delivery status 
	 * has been applied.
	 * 
	 * @return The number of items in the shipment for each delivery status.
	 */
	TrackingSummary getTrackingSummaries();
	void setTrackingSummaries(TrackingSummary summaries);
	
	/**
	 * <ul>
	 * 	<li>Type: Decimal</li>
	 * 	<li>Length: N.2</li>
	 * </ul>
	 * <br>
	 * The freight charge for this shipment.<br>
	 * 
	 * @return The freight charge for this shipment.
	 */
	FreightCharge getFreightCharge();
	void setFreightCharge(FreightCharge charge);
	
	/**
	 * <ul>
	 * 	<li>Type: Decimal</li>
	 * 	<li>Length: N.2</li>
	 * </ul>
	 * <br>
	 * The discount for this shipment.<br>
	 * 
	 * @return The discount for this shipment.
	 */
	Discount getDiscount();
	void setDiscount(Discount discount);
	
	/**
	 * <ul>
	 * 	<li>Type: Decimal</li>
	 * 	<li>Length: N.2</li>
	 * </ul>
	 * <br>
	 * The transit cover charge for this shipment.
	 * This field may be used to specify Transit Cover, Extra Cover or Transit Warranty for Contract, 
	 * Non-Contract or StarTrack products respectively.
	 * 
	 * @return The transit cover charge for this shipment.
	 */
	TransitCover getTransitCover();
	void setTransitCover(TransitCover cover);
	
	/**
	 * <ul>
	 * 	<li>Type: Decimal</li>
	 * 	<li>Length: N.2</li>
	 * </ul>
	 * <br>
	 * The security surcharge for this shipment.
	 * 
	 * @return The security surcharge for this shipment.
	 */	
	SecuritySurcharge getSecuritySurcharge();
	void setSecuritySurcharge(SecuritySurcharge charge);
	
	/**
	 * <ul>
	 * 	<li>Type: Decimal</li>
	 * 	<li>Length: N.2</li>
	 * </ul>
	 * <br>
	 * The fuel surcharge for this shipment.	
	 * 
	 * @return The fuel surcharge for this shipment.
	 */
	FuelSurcharge getFuelSurcharge();
	void setFuelSurcharge(FuelSurcharge charge);
	
	Boolean validate();
}
