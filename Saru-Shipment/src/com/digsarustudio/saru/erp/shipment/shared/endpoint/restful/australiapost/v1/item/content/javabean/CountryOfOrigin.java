/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.javabean;

import com.digsarustudio.saru.erp.shipment.shared.CountryCodes;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.CountryOfOrigin}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class CountryOfOrigin implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.CountryOfOrigin
									  , IsSerializable {
	public static final Integer	MAX_LEN	= 2;
	
	/**
	 * 
	 * The object builder for {@link CountryOfOrigin}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.CountryOfOrigin.Builder {
		private CountryOfOrigin result = null;

		private Builder() {
			this.result = new CountryOfOrigin();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.CountryOfOrigin.Builder#setCountry(com.digsarustudio.saru.erp.shipment.shared.CountryCodes)
		 */
		@Override
		public Builder setCountry(CountryCodes code) {
			this.result.setCountry(code);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.CountryOfOrigin.Builder#setCountry(java.lang.String)
		 */
		@Override
		public Builder setCountry(String country) {
			this.result.setCountry(country);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public CountryOfOrigin build() {
			return this.result;
		}

	}
	
	private String country	= null;
	
	
	/**
	 * 
	 */
	private CountryOfOrigin() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.CountryOfOrigin#setCountry(com.digsarustudio.saru.erp.shipment.shared.CountryCodes)
	 */
	@Override
	public void setCountry(CountryCodes code) {
		this.setCountry(code.getValue());

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.CountryOfOrigin#setCountry(java.lang.String)
	 */
	@Override
	public void setCountry(String country) {
		this.country = country;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.CountryOfOrigin#getCountryOfOrigin()
	 */
	@Override
	public String getCountryOfOrigin() {
		
		return this.country;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.CountryOfOrigin#getCountryCode()
	 */
	@Override
	public CountryCodes getCountryCode() {
		return CountryCodes.fromValue(this.country);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.CountryOfOrigin#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.country || this.country.isEmpty()) {
			return false;
		}
		
		if(MAX_LEN < this.country.length()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
