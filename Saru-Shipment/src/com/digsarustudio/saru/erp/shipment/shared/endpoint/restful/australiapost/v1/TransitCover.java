/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The transit cover charge for this shipment.
 * This field may be used to specify Transit Cover, Extra Cover or Transit Warranty for Contract, 
 * Non-Contract or StarTrack products respectively.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface TransitCover {
	/**
	 * 
	 * The builder for {@link Discount}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<TransitCover> {
		Builder setValue(Integer cover);
		Builder setValue(Integer cover, Integer precision);

		Builder setValue(String cover);
		Builder setValue(String cover, String precision);
	}
	
	void setValue(Integer cover);
	void setValue(Integer cover, Integer precision);

	void setValue(String cover);
	void setValue(String cover, String precision);
	
	String getTransitCover();
	String getPrcision();
	String getDecimalTransitCover();
	
	Boolean validate();
}
