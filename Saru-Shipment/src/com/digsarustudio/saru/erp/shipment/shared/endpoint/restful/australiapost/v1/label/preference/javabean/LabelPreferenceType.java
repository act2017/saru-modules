/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceType}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelPreferenceType implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceType
										  , IsSerializable {
	public static final LabelPreferenceType	Print	= new LabelPreferenceTypePrint();
	
	/**
	 * 
	 * The object builder for {@link LabelPreferenceType}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceType.Builder {
		private LabelPreferenceType result = null;

		private Builder() {
			this.result = new LabelPreferenceType();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceType.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String type) {
			this.result.setValue(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public LabelPreferenceType build() {
			return this.result;
		}

	}
	
	private String	type	= null;
	
	/**
	 * 
	 */
	protected LabelPreferenceType() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceType#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String type) {
		this.type = type;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceType#getValue()
	 */
	@Override
	public String getValue() {
		
		return this.type;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceType#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.type || this.type.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
