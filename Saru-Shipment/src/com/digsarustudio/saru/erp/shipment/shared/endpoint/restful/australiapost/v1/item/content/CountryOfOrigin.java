/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.CountryCodes;

/**
 * The country in which this described content was manufactured. 
 * It is only mandatory if commercial_value is "true", otherwise it is optional.<br>
 * The country code might returned from server side.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface CountryOfOrigin {
	/**
	 * 
	 * The builder for {@link CountryOfOrigin}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<CountryOfOrigin> {
		Builder setCountry(CountryCodes code);
		Builder setCountry(String country);
	}
	
	void setCountry(CountryCodes code);
	void setCountry(String country);
	
	String			getCountryOfOrigin();
	CountryCodes	getCountryCode();
	
	Boolean validate();
}
