/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelFormatLayouts;

/**
 * The label layout printed in A6 thermal paper and 1 printed per page.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelFormatLayoutThermalLabel_A6_1pp extends LabelFormatLayout {

	/**
	 * 
	 */
	public LabelFormatLayoutThermalLabel_A6_1pp() {
		this.setLayout(LabelFormatLayouts.ThermalLableA6_1pp);
	}

}
