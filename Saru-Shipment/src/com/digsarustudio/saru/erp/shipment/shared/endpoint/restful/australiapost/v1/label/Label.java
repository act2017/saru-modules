/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection;

/**
 * Information related to the generated labels for the item.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Label {
	/**
	 * 
	 * The builder for {@link Label}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Label> {
		Builder setRequestId(LabelRequestId id);
		Builder setRequestDate(DateTime date);
		Builder addShipment(Shipment shipment);
		Builder setShipments(List<Shipment> shipments);
		Builder setShipments(ShipmentCollection shipments);
		
		Builder	setURL(LabelURL url);
		Builder setURLCreationDate(DateTime date);
		
		Builder setPrintableURL(LabelURL url);
		Builder setContnent(String content);
		Builder setStatus(LabelGenerationRequestStatus status);
		Builder setCreationDate(DateTime date);
		Builder setErrors(AusPostErrorCollection errors);
	}
	
	void setRequestId(LabelRequestId id);
	void setRequestDate(DateTime date);
	void addShipment(Shipment shipment);
	void setShipments(List<Shipment> shipments);
	void setShipments(ShipmentCollection shipments);
	
	void setURL(LabelURL url);
	void setURLCreationDate(DateTime date);
	void setContnent(String content);
	
	void setPrintableURL(LabelURL url);
	void setStatus(LabelGenerationRequestStatus status);
	void setCreationDate(DateTime date);
	void setErrors(AusPostErrorCollection errors);
	
	LabelRequestId 					getRequestId();
	DateTime						getRequestDate();
	Shipment 						getShipment(Integer index);
	ShipmentCollection 				getShipments();
	
	LabelURL						getURL();
	DateTime 						getURLCreationDate();
	String							getContent();
	
	LabelURL 						getPrintableURL();
	LabelGenerationRequestStatus	getStatus();
	DateTime						getCreationDate();
	AusPostErrorCollection			getErrors();
	
	Boolean validate();
}
