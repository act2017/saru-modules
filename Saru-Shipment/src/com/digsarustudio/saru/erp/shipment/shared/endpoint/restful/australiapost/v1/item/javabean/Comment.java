/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Comment}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class Comment implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Comment
							  , IsSerializable {
	public static final Integer	MAX_LEN	= 120;
	
	/**
	 * 
	 * The object builder for {@link Comment}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Comment.Builder {
		private Comment result = null;

		private Builder() {
			this.result = new Comment();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Comment.Builder#setComment(java.lang.String)
		 */
		@Override
		public Builder setComment(String comment) {
			this.result.setComment(comment);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Comment build() {
			return this.result;
		}

	}
	
	private String comment	= null;

	/**
	 * 
	 */
	private Comment() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Comment#setComment(java.lang.String)
	 */
	@Override
	public void setComment(String comment) {
		this.comment = comment;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Comment#getComment()
	 */
	@Override
	public String getComment() {
		
		return this.comment;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Comment#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.comment || this.comment.isEmpty()) {
			return false;
		}
		
		if(MAX_LEN < this.comment.length()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
