/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.accounts;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type stores the details of order summary that contains a charges breakdown of the articles in the order..<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface OrderSummary {
	/**
	 * 
	 * The builder for {@link OrderSummary}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<OrderSummary> {
		Builder setOrderId(String id);
		Builder setPDFContent(String content);
	}
	
	void setOrderId(String id);
	void setPDFContent(String content);
	
	String getOrderId();
	String getPDFContent();
	
	Boolean valdiate();
}
