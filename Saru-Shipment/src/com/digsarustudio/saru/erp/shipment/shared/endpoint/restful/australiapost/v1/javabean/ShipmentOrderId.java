/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderId}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ShipmentOrderId implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderId
							  , IsSerializable {
	/**
	 * 
	 * The object builder for {@link ShipmentOrderId}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderId.Builder {
		private ShipmentOrderId result = null;

		private Builder() {
			this.result = new ShipmentOrderId();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderId.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String id) {
			this.result.setValue(id);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ShipmentOrderId build() {
			return this.result;
		}

	}
	
	private String	id	= null;

	/**
	 * 
	 */
	private ShipmentOrderId() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderId#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String id) {
		this.id = id;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderId#getValue()
	 */
	@Override
	public String getValue() {
		
		return this.id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderId#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.id || this.id.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
