/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ShipmentBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ShipmentCollectionBiserialiser;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatCollection;

/**
 * The required preference for label printing.<br>
 * If you wanna to parse this object from or into json string, please use .... to do it.<br>
 * Do not use {@link ShipmentCollectionBiserialiser} and {@link ShipmentBiserialiser}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface LabelPreference {
	/**
	 * 
	 * The builder for {@link LabelPreference}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<LabelPreference> {
		Builder setType(LabelPreferenceType type);
		Builder setLabelFormats(LabelFormatCollection formats);
		Builder addLabelFormat(LabelFormat format);
	}

	void setType(LabelPreferenceType type);
	void setLabelFormats(LabelFormatCollection formats);
	void addLabelFormat(LabelFormat format);
		
	LabelPreferenceType 	getType();
	LabelFormatCollection	getLabelFormats();
	LabelFormat				getLabelFormat(Integer index);
	
	Boolean validate();
}
