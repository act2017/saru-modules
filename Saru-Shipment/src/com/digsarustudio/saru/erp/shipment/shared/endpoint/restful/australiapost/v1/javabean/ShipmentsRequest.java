/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentsRequest}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ShipmentsRequest implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentsRequest
											  , IsSerializable {
	public static final String ATTR_SHIPMENTS	= "shipments";
	
	/**
	 * 
	 * The object builder for {@link ShipmentsRequest}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentsRequest.Builder {
		private ShipmentsRequest result = null;

		private Builder() {
			this.result = new ShipmentsRequest();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.CreateShipmentsResponse.Builder#addShipment(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment)
		 */
		@Override
		public Builder addShipment(Shipment shipment) {
			this.result.addShipment(shipment);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.CreateShipmentsResponse.Builder#setShipments(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection)
		 */
		@Override
		public Builder setShipments(ShipmentCollection shipments) {
			this.result.setShipments(shipments);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.CreateShipmentsResponse.Builder#setShipments(java.util.List)
		 */
		@Override
		public Builder setShipments(List<Shipment> shipments) {
			this.result.setShipments(shipments);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.CreateShipmentsResponse.Builder#setShipments(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment[])
		 */
		@Override
		public Builder setShipments(Shipment[] shipments) {
			this.result.setShipments(shipments);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ShipmentsRequest build() {
			return this.result;
		}

	}

	private ShipmentCollection	shipments	= null;
	
	/**
	 * 
	 */
	private ShipmentsRequest() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.CreateShipmentsResponse#addShipment(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment)
	 */
	@Override
	public void addShipment(Shipment shipment) {
		if(null == shipment) {
			return;
		}
		
		if(null == this.shipments) {
			this.shipments = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentCollection.builder().build();
		}

		this.shipments.addShipment(shipment);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.CreateShipmentsResponse#setShipments(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection)
	 */
	@Override
	public void setShipments(ShipmentCollection shipments) {
		this.shipments = shipments;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.CreateShipmentsResponse#getShipment(java.lang.Integer)
	 */
	@Override
	public Shipment getShipment(Integer index) {
		if(null == this.shipments || null == index || index >= this.shipments.size()) {
			return null;
		}
		
		return this.shipments.getShipment(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.CreateShipmentsResponse#getShipments()
	 */
	@Override
	public List<Shipment> getShipments() {
		if(null == this.shipments) {
			return null;
		}
		
		return this.shipments.getShipments();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.CreateShipmentsResponse#setShipments(java.util.List)
	 */
	@Override
	public void setShipments(List<Shipment> shipments) {
		for (Shipment shipment : shipments) {
			this.addShipment(shipment);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.CreateShipmentsResponse#setShipments(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment[])
	 */
	@Override
	public void setShipments(Shipment[] shipments) {
		for (Shipment shipment : shipments) {
			this.addShipment(shipment);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.CreateShipmentsResponse#getShipmentCollection()
	 */
	@Override
	public ShipmentCollection getShipmentCollection() {

		return this.shipments;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.CreateShipmentsResponse#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.shipments || !this.shipments.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
