/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AustraliaPostCustomerNumber}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AustraliaPostCustomerNumber implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AustraliaPostCustomerNumber
												  , IsSerializable {
	public static final Integer MAX_LEN	= 10;

	/**
	 * 
	 * The object builder for {@link AustraliaPostCustomerNumber}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AustraliaPostCustomerNumber.Builder {
		private AustraliaPostCustomerNumber result = null;

		private Builder() {
			this.result = new AustraliaPostCustomerNumber();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AustraliaPostCustomerNumber.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AustraliaPostCustomerNumber build() {
			return this.result;
		}

	}
	
	private String value	= null;
	
	/**
	 * 
	 */
	private AustraliaPostCustomerNumber() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AustraliaPostCustomerNumber#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.value = value;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AustraliaPostCustomerNumber#getValue()
	 */
	@Override
	public String getValue() {
		
		return this.value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AustraliaPostCustomerNumber#validate()
	 */
	@Override
	public Boolean validate() {
		if( null == this.value || this.value.isEmpty() ) {
			return false;
		}

		if( MAX_LEN < this.value.length() ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
