/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ArticleId}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ArticleId implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ArticleId
								, IsSerializable {
	public static final Integer MAX_LEN	= 100;

	/**
	 * 
	 * The object builder for {@link ArticleId}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ArticleId.Builder {
		private ArticleId result = null;

		private Builder() {
			this.result = new ArticleId();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ArticleId.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String id) {
			this.result.setValue(id);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ArticleId build() {
			return this.result;
		}

	}
	
	private String id	= null;
	
	/**
	 * 
	 */
	private ArticleId() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ArticleId#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String id) {
		this.id = id;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ArticleId#getValue()
	 */
	@Override
	public String getValue() {
		
		return this.id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ArticleId#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.id || this.id.isEmpty()) {
			return false;
		}
		
		if(MAX_LEN < this.id.length()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
