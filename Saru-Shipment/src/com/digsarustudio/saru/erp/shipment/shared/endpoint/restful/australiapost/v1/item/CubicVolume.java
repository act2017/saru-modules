/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The volume of the item in cubic metres. If volumetric pricing applies, then volume is mandatory. 
 * To specify volume, you can use the cubic_volume field or the length/width/height fields, but not both. 
 * Note that the cubic_volume field is only available for StarTrack products.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface CubicVolume {
	/**
	 * 
	 * The builder for {@link CubicVolume}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<CubicVolume> {
		Builder setValue(Integer value);
		Builder setValue(Integer main, Integer precision);
		Builder setValue(String value);
		Builder setValue(String main, String precision);
	}	

	void setValue(Integer value);
	void setValue(Integer main, Integer precision);
	void setValue(String value);	
	void setValue(String main, String precision);
	
	String getValue();
	
	Boolean validate();
}
