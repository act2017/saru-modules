/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.UnNumber}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class UnNumber implements com.digsarustudio.saru.erp.shipment.shared.UnNumber, IsSerializable {
	public static final Integer MAX_LEN	= 4;
	
	/**
	 * 
	 * The object builder for {@link UnNumber}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.UnNumber.Builder {
		private UnNumber result = null;

		private Builder() {
			this.result = new UnNumber();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.UnNumber.Builder#setValue(java.lang.Integer)
		 */
		@Override
		public Builder setValue(Integer value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.UnNumber.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public UnNumber build() {
			return this.result;
		}

	}
	
	private String	value	= null;

	/**
	 * 
	 */
	private UnNumber() {
		
	}

	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.UnNumber#setValue(java.lang.Integer)
	 */
	@Override
	public void setValue(Integer value) {
		this.value = value.toString();
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.UnNumber#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.value = value;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.UnNumber#getValue()
	 */
	@Override
	public Integer getValue() {
		
		return Integer.parseInt(this.value);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.UnNumber#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.value || this.value.isEmpty()) {
			return false;
		}
		
		if( MAX_LEN < this.value.length() ) {
			return false;
		}

		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
