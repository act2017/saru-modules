/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatCollection;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelPreference implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference
									  , IsSerializable {
	public static final String	ATTR_TYPE	= "type";
	public static final String	ATTR_GROUPS	= "groups";
	
	/**
	 * 
	 * The object builder for {@link LabelPreference}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference.Builder {
		private LabelPreference result = null;

		private Builder() {
			this.result = new LabelPreference();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference.Builder#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceType)
		 */
		@Override
		public Builder setType(LabelPreferenceType type) {
			this.result.setType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference.Builder#setLabelFormats(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatCollection)
		 */
		@Override
		public Builder setLabelFormats(LabelFormatCollection formats) {
			this.result.setLabelFormats(formats);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference.Builder#addLabelFormat(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat)
		 */
		@Override
		public Builder addLabelFormat(LabelFormat format) {
			this.result.addLabelFormat(format);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public LabelPreference build() {
			return this.result;
		}

	}
	
	private LabelPreferenceType		type	= null;
	private	LabelFormatCollection	groups	= null;
	
	/**
	 * 
	 */
	private LabelPreference() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceType)
	 */
	@Override
	public void setType(LabelPreferenceType type) {
		this.type = type;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference#setLabelFormats(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatCollection)
	 */
	@Override
	public void setLabelFormats(LabelFormatCollection formats) {
		this.groups = formats;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference#addLabelFormats(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat)
	 */
	@Override
	public void addLabelFormat(LabelFormat format) {
		if(null == this.groups) {
			this.groups = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean.LabelFormatCollection.builder().build();
		}
		
		this.groups.addFormat(format);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference#getType()
	 */
	@Override
	public LabelPreferenceType getType() {
		
		return this.type;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference#getLabelFormats()
	 */
	@Override
	public LabelFormatCollection getLabelFormats() {
		
		return this.groups;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference#getLabelFormat(java.lang.Integer)
	 */
	@Override
	public LabelFormat getLabelFormat(Integer index) {
		if(null == this.groups || null == index || index >= this.groups.size()) {
			return null;
		}
		
		return this.groups.getFormat(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.type || !this.type.validate()) {
			return false;
		}
		
		if(null == this.groups || !this.groups.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
