/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The weight in kilograms of this described content. 
 * If you provide a weight for one item_contents element, you must provide weights for all item_contents elements.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface ItemContentWeight {
	/**
	 * 
	 * The builder for {@link ItemContentWeight}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemContentWeight> {
		Builder setWeight(Integer weight);
		Builder setWeight(Integer weight, Integer precision);
		Builder setWeight(String weight);
		Builder setWeight(String weight, String precision);
	}
	
	
	void setWeight(Integer weight);
	void setWeight(Integer weight, Integer precision);
	void setWeight(String weight);
	void setWeight(String weight, String precision);
	
	Integer getWeight();
	Integer getPrecision();
	
	String getDecimalWeight();
	
	Boolean validate();
}
