/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The implementation of {@link com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber} for {@link Shipment} to use.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AusPostShipmentPhoneNumber implements PhoneNumber
								  		 , IsSerializable {
	public static final Integer MIN_LEN	= 10;
	public static final Integer	MAX_LEN	= 20;
	
	/**
	 * 
	 * The object builder for {@link AusPostShipmentPhoneNumber}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements PhoneNumber.Builder {
		private AusPostShipmentPhoneNumber result = null;

		private Builder() {
			this.result = new AusPostShipmentPhoneNumber();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber.Builder#setNumber(java.lang.String)
		 */
		@Override
		public Builder setNumber(String number) {
			this.result.setNumber(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AusPostShipmentPhoneNumber build() {
			return this.result;
		}

	}
	
	private String number	= null;
	
	/**
	 * 
	 */
	private AusPostShipmentPhoneNumber() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber#setNumber(java.lang.String)
	 */
	@Override
	public void setNumber(String number) {
		this.number = number;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber#getNumber()
	 */
	@Override
	public String getNumber() {
		
		return this.number;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.number || this.number.isEmpty()) {
			return false;
		}
		
		if( MIN_LEN > this.number.length() ) {
			return false;
		}
		
		if( MAX_LEN < this.number.length() ) {
			return false;
		}
		
		//TODO regex
		
		return true;
	}

	public static AusPostShipmentPhoneNumber copy(PhoneNumber source) {
		if(null == source) {
			return null;
		}
		
		AusPostShipmentPhoneNumber.Builder builder = AusPostShipmentPhoneNumber.builder();
		
		builder.setNumber(source.getNumber());
		
		return builder.build();
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
