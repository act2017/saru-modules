/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.Weight;
import com.digsarustudio.saru.erp.musaceae.shared.WeightUnit;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The implementation of {@link Weight} for the {@link DangerousGoods} to represents the net weight for {@link Shipment} used by Australia Post API.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DangerousGoodsNetWeight implements Weight, IsSerializable {
	public static final Integer 	MAX_LEN	= 5;
	public static final WeightUnit	UNIT	= WeightUnit.Kilogram;
	
	/**
	 * 
	 * The object builder for {@link DangerousGoodsNetWeight}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements Weight.Builder {
		private DangerousGoodsNetWeight result = null;

		private Builder() {
			this.result = new DangerousGoodsNetWeight();
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.Weight.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.Weight.Builder#setUnit(com.digsarustudio.saru.erp.musaceae.shared.WeightUnit)
		 */
		@Override
		public Builder setUnit(WeightUnit unit) {
			this.result.setUnit(unit);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DangerousGoodsNetWeight build() {
			return this.result;
		}

	}

	private String	weight	= null;
	
	/**
	 * 
	 */
	private DangerousGoodsNetWeight() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Weight#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.weight = value;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Weight#setUnit(com.digsarustudio.saru.erp.musaceae.shared.WeightUnit)
	 */
	@Override
	public void setUnit(WeightUnit unit) {
		String msg = this.getClass().getName() + " used KGs as the unit";
		throw new UnsupportedOperationException(msg);

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Weight#getValue()
	 */
	@Override
	public String getValue() {

		return this.weight;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Weight#getIntValue()
	 */
	@Override
	public Integer getIntValue() {

		return Integer.parseInt(this.weight);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Weight#getUnit()
	 */
	@Override
	public WeightUnit getUnit() {

		return UNIT;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Weight#toKilograms()
	 */
	@Override
	public Weight toKilograms() {
		return this;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.Weight#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.weight || this.weight.isEmpty()) {
			return false;
		}
		
		if(MAX_LEN < this.weight.length()) {
			return false;
		}

		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
