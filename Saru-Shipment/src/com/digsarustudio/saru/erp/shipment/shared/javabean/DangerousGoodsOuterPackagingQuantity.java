/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.javabean;

import com.digsarustudio.saru.erp.shipment.shared.Quantity;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Contains the quantity of the dangerous goods. This quantity is the number of outer packages of each type.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DangerousGoodsOuterPackagingQuantity implements Quantity, IsSerializable {
	public static final Integer MAX_LEN	= 4;

	/**
	 * 
	 * The object builder for {@link DangerousGoodsOuterPackagingQuantity}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements Quantity.Builder {
		private DangerousGoodsOuterPackagingQuantity result = null;

		private Builder() {
			this.result = new DangerousGoodsOuterPackagingQuantity();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.Quantity.Builder#setValue(java.lang.Integer)
		 */
		@Override
		public Builder setValue(Integer value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.Quantity.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DangerousGoodsOuterPackagingQuantity build() {
			return this.result;
		}

	}
	
	private String value	= null;
	
	/**
	 * 
	 */
	private DangerousGoodsOuterPackagingQuantity() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.Quantity#setValue(java.lang.Integer)
	 */
	@Override
	public void setValue(Integer value) {
		this.value = value.toString();

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.Quantity#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.value = value;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.Quantity#getValue()
	 */
	@Override
	public Integer getValue() {

		return Integer.parseInt(this.value);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.Quantity#getValueString()
	 */
	@Override
	public String getValueString() {

		return this.value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.Quantity#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.value || this.value.isEmpty()) {
			return false;
		}
		
		if( MAX_LEN < this.value.length() ) {
			return false;
		}

		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
