/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label;

import java.util.HashMap;
import java.util.Map;

/**
 * StarTrack and Australia Post labels are available in various layouts. 
 * The printed shipping label layout specifies the number of labels per page and the size of the page.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelFormatLayouts {
	public static final LabelFormatLayouts	A4_4pp				= new LabelFormatLayouts("A4-4pp", "A4-4pp");
	public static final LabelFormatLayouts	A4_3pp				= new LabelFormatLayouts("A4-3pp", "A4-3pp");
	public static final LabelFormatLayouts	A4_2pp				= new LabelFormatLayouts("A4-2pp", "A4-2pp");
	public static final LabelFormatLayouts	A4_1pp				= new LabelFormatLayouts("A4-1pp", "A4-1pp");
	public static final LabelFormatLayouts	A4_2pp_Landscape	= new LabelFormatLayouts("A4-2pp Landscape", "A4-2pp Landscape");
	public static final LabelFormatLayouts	A4_1pp_Landscape	= new LabelFormatLayouts("A4-1pp Landscape", "A4-1pp Landscape");
	public static final LabelFormatLayouts	A6_1pp				= new LabelFormatLayouts("A6-1pp", "A6-1pp");
	public static final LabelFormatLayouts	ThermalLableA6_1pp	= new LabelFormatLayouts("THERMAL-LABEL-A6-1PP", "Thermal Label A6-1pp");
	


	private static Map<String, LabelFormatLayouts> VALUES = new HashMap<>();
		
	static {
		VALUES.put(A4_4pp.getValue(), A4_4pp);
		VALUES.put(A4_3pp.getValue(), A4_3pp);
		VALUES.put(A4_2pp.getValue(), A4_2pp);
		VALUES.put(A4_1pp.getValue(), A4_1pp);
		VALUES.put(A4_2pp_Landscape.getValue(), A4_2pp_Landscape);
		VALUES.put(A4_1pp_Landscape.getValue(), A4_1pp_Landscape);
		VALUES.put(A6_1pp.getValue(), A6_1pp);
		VALUES.put(ThermalLableA6_1pp.getValue(), ThermalLableA6_1pp);
	}	
	
	private String value = null;
	private String display = null;
	
	private LabelFormatLayouts() {
		
	}
	
	private LabelFormatLayouts(String value, String display) {
		this.value = value;
		this.display = display;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public String getDisplayString() {
		return this.display;
	}
	
	public static LabelFormatLayouts fromValue(String value) {
		if (null == value || !VALUES.containsKey(value)) {
			return null;
		}
		
		return VALUES.get(value);
	}
	
	public static LabelFormatLayouts getDefault() {
		return A4_4pp;
	}
		
}
