/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection;

/**
 * This service creates an order for the referenced shipments that have previously been created using the Create Shipments service.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-order-from-shipments">Create Order From Shipments</a>
 *
 */
public interface CreateOrderRequest {
	/**
	 * 
	 * The builder for {@link CreateOrderRequest}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<CreateOrderRequest> {
		Builder setOrderReference(OrderReference reference);
		Builder setPaymentMethod(PaymentMethod method);
		Builder setConsignor(Consignor consignor);
		Builder addShipment(Shipment shipment);
		Builder setShipments(List<Shipment> shipments);
		Builder setShipments(ShipmentCollection shipments);
	}
	
	void setOrderReference(OrderReference reference);
	void setPaymentMethod(PaymentMethod method);
	void setConsignor(Consignor consignor);
	void addShipment(Shipment shipment);
	void setShipments(List<Shipment> shipments);
	void setShipments(ShipmentCollection shipments);
	
	OrderReference		getOrderReference();
	PaymentMethod		getPaymentMethod();
	Consignor			getConsignor();
	Shipment			getShipment(Integer index);
	ShipmentCollection	getShipments();
	
	Boolean validate();
}
