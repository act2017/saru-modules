/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.ShipmentFeatureAttribute;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.ShipmentFeatureAttributeDate;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * This feature represents the date selection for delivery of a shipment. 
 * The delivery date feature contains a single attribute of "date" which is required if this feature is used.
 * <br>
 * Attributes:<br>
 * date - should be supplied in the format yyyy-mm-dd<br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
 */
public class DeliveryDate implements ShipmentFeature, IsSerializable {
	public static final String NAME			= "DELIVERY_DATE";
	public static final String ATTR_DATE	= "date";
	
	/**
	 * 
	 * The object builder for {@link DeliveryDate}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ShipmentFeature.DeliveryDateBuilder {
		private DeliveryDate result = null;

		private Builder() {
			this.result = new DeliveryDate();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature.DeliveryDateBuilder#setDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date)
		 */
		@Override
		public DeliveryDateBuilder setDate(Date date) {
			this.result.setValue(date.getDate());
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature.DeliveryDateBuilder#setDate(java.lang.String)
		 */
		@Override
		public DeliveryDateBuilder setDate(String date) {
			this.result.setValue(date);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature.DeliveryDateBuilder#setDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.ShipmentFeatureAttributeDate)
		 */
		@Override
		public DeliveryDateBuilder setDate(ShipmentFeatureAttributeDate date) {
			this.result.setAttribute(date);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DeliveryDate build() {
			return this.result;
		}

	}
	
	private ShipmentFeatureAttribute<?>	date	= null;
	
	/**
	 * 
	 */
	private DeliveryDate() {

	}
	
	public void setValue(String date) {
		this.date = ShipmentFeatureAttributeDate.builder().setValue(date)
														  .build();
	}
	
	public void setValue(Date date) {
		this.date = ShipmentFeatureAttributeDate.builder().setValue(date)
				  										  .build();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature#setAttribute(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.ShipmentFeatureAttribute)
	 */
	@Override
	public void setAttribute(ShipmentFeatureAttribute<?> attribute) {
		this.date = attribute;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature#getAttribute()
	 */
	@Override
	public ShipmentFeatureAttribute<?> getAttribute() {

		return this.date;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.date || !this.date.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
