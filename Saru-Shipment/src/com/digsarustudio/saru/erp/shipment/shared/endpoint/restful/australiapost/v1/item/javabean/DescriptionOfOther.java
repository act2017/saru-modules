/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.DescriptionOfOther}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DescriptionOfOther implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.DescriptionOfOther
										 , IsSerializable {
	public static final Integer MAX_LEN	= 40;
	
	/**
	 * 
	 * The object builder for {@link DescriptionOfOther}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.DescriptionOfOther.Builder {
		private DescriptionOfOther result = null;

		private Builder() {
			this.result = new DescriptionOfOther();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.DescriptionOfOther.Builder#setDescription(java.lang.String)
		 */
		@Override
		public Builder setDescription(String desc) {
			this.result.setDescription(desc);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DescriptionOfOther build() {
			return this.result;
		}

	}
	
	private String description	= null;
	
	/**
	 * 
	 */
	private DescriptionOfOther() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.DescriptionOfOther#setDescription(java.lang.String)
	 */
	@Override
	public void setDescription(String desc) {
		this.description = desc;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.DescriptionOfOther#getDescription()
	 */
	@Override
	public String getDescription() {
		
		return this.description;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.DescriptionOfOther#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.description || this.description.isEmpty()) {
			return false;
		}
		
		if(MAX_LEN < this.description.length()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
