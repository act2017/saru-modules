/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalGst;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummary;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderSummary}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class OrderSummary implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderSummary
									  , IsSerializable {
	public static final String	ATTR_TOTAL_COST				= "total_cost";
	public static final String	ATTR_TOTAL_GST				= "total_gst";
	public static final String	ATTR_STATUS					= "status";
	public static final String	ATTR_NUMBER_OF_SHIPMENTS	= "number_of_shipments";
	public static final String	ATTR_TRACKING_SUMMARY		= "tracking_summary";
	
	/**
	 * 
	 * The object builder for {@link OrderSummary}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderSummary.Builder {
		private OrderSummary result = null;

		private Builder() {
			this.result = new OrderSummary();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderSummary.Builder#setTotalCost(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost)
		 */
		@Override
		public Builder setTotalCost(TotalCost cost) {
			this.result.setTotalCost(cost);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderSummary.Builder#setTotalGST(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalGst)
		 */
		@Override
		public Builder setTotalGST(TotalGst gst) {
			this.result.setTotalGST(gst);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderSummary.Builder#setStatus(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus)
		 */
		@Override
		public Builder setStatus(OrderShipmentItemStatus status) {
			this.result.setStatus(status);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderSummary.Builder#setNumberOfShipments(java.lang.Integer)
		 */
		@Override
		public Builder setNumberOfShipments(Integer number) {
			this.result.setNumberOfShipments(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderSummary.Builder#setTrackingSummaries(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummaryCollection)
		 */
		@Override
		public Builder setTrackingSummaries(TrackingSummary summaries) {
			this.result.setTrackingSummaries(summaries);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public OrderSummary build() {
			return this.result;
		}

	}
	
	private TotalCost					total_cost			= null;
	private TotalGst					total_gst			= null;
	private OrderShipmentItemStatus		status				= null;
	private Integer						number_of_shipments	= 0;
	private TrackingSummary				tracking_summary	= null;
	
	/**
	 * 
	 */
	private OrderSummary() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderSummary#getTotalCost()
	 */
	@Override
	public TotalCost getTotalCost() {

		return this.total_cost;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderSummary#getTotalGST()
	 */
	@Override
	public TotalGst getTotalGST() {

		return this.total_gst;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderSummary#getStatus()
	 */
	@Override
	public OrderShipmentItemStatus getStatus() {

		return this.status;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderSummary#getNumberOfShipments()
	 */
	@Override
	public Integer getNumberOfShipments() {

		return this.number_of_shipments;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderSummary#getTrackingSummary()
	 */
	@Override
	public TrackingSummary getTrackingSummaries() {

		return this.tracking_summary;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderSummary#setTotalCost(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost)
	 */
	@Override
	public void setTotalCost(TotalCost cost) {
		this.total_cost = cost;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderSummary#setTotalGST(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalGst)
	 */
	@Override
	public void setTotalGST(TotalGst gst) {
		this.total_gst = gst;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderSummary#setStatus(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus)
	 */
	@Override
	public void setStatus(OrderShipmentItemStatus status) {
		this.status = status;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderSummary#setNumberOfShipments(java.lang.Integer)
	 */
	@Override
	public void setNumberOfShipments(Integer number) {
		this.number_of_shipments = number;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderSummary#setTrackingSummaries(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummaryCollection)
	 */
	@Override
	public void setTrackingSummaries(TrackingSummary summaries) {
		this.tracking_summary = summaries;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderSummary#validate()
	 */
	@Override
	public Boolean validate() {
		if(null != this.total_cost && !this.total_cost.validate()) {
			return false;
		}

		if(null != this.total_gst && !this.total_gst.validate()) {
			return false;
		}

		if(null != this.status && !this.status.validate()) {
			return false;
		}

		if(null == this.number_of_shipments ) {
			return false;
		}

		if(null != this.tracking_summary && !this.tracking_summary.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
