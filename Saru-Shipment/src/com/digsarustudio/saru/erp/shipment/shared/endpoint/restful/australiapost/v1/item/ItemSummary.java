/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalGst;

/**
 * Summary information for each item in the shipment.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface ItemSummary {
	/**
	 * 
	 * The builder for {@link ItemSummary}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemSummary> {
		Builder setTotalCost(TotalCost cost);
		Builder setTotalGST(TotalGst gst);
		Builder setStatus(OrderShipmentItemStatus status);
	}
	
	/**
	 * <ul>
	 * 	<li>Type: Decimal</li>
	 * 	<li>Length: N.2</li>
	 * </ul>
	 * <br>
	 * The total price of the shipment, including goods and services tax (G.S.T). This field is not used for StarTrack shipments, 
	 * as they are costed at the shipment level only.
	 * 	
	 * @return The total price of the shipment, including goods and services tax (G.S.T). This field is not used for StarTrack shipments, as they are costed at the shipment level only.	
	 */
	TotalCost getTotalCost();
	void setTotalCost(TotalCost cost);
	
	/**
	 * <ul>
	 * 	<li>Type: Decimal</li>
	 * 	<li>Length: N.2</li>
	 * </ul>
	 * <br>
	 * The goods and services tax amount included in the total cost. This field is not used for StarTrack shipments, as they are costed at the shipment level only.
	 * 	
	 * @return The goods and services tax amount included in the total cost. This field is not used for StarTrack shipments, as they are costed at the shipment level only.	
	 */
	TotalGst getTotalGST();
	void setTotalGST(TotalGst gst);
	
	/**
	 * 	The status for the item.
	 * 	
	 * @return 	The status for the item.	
	 */
	OrderShipmentItemStatus getStatus();
	void setStatus(OrderShipmentItemStatus status);
	
	Boolean validate();
}
