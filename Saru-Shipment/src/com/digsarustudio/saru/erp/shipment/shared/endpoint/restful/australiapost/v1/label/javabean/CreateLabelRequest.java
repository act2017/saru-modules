/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean;

import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceCollection;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class CreateLabelRequest implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest
										 , IsSerializable {
	public static final String ATTR_PREFERENCES	= "preferences";
	public static final String ATTR_SHIPMENTS	= "shipments";
	
	/**
	 * 
	 * The object builder for {@link CreateLabelRequest}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest.Builder {
		private CreateLabelRequest result = null;

		private Builder() {
			this.result = new CreateLabelRequest();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest.Builder#addPreference(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference)
		 */
		@Override
		public Builder addPreference(LabelPreference preference) {
			this.result.addPreference(preference);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest.Builder#setPreferences(java.util.List)
		 */
		@Override
		public Builder setPreferences(List<LabelPreference> preferences) {
			this.result.setPreferences(preferences);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest.Builder#setPreferences(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceCollection)
		 */
		@Override
		public Builder setPreferences(LabelPreferenceCollection preferences) {
			this.result.setPreferences(preferences);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest.Builder#addShipment(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment)
		 */
		@Override
		public Builder addShipment(Shipment shipment) {
			this.result.addShipment(shipment);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest.Builder#setShipments(java.util.List)
		 */
		@Override
		public Builder setShipments(List<Shipment> shipments) {
			this.result.setShipments(shipments);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest.Builder#setShipments(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection)
		 */
		@Override
		public Builder setShipments(ShipmentCollection shipments) {
			this.result.setShipments(shipments);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public CreateLabelRequest build() {
			return this.result;
		}

	}
	
	private	LabelPreferenceCollection	preferences	= null;
	private ShipmentCollection			shipments	= null;
	
	/**
	 * 
	 */
	private CreateLabelRequest() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest#addPreference(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference)
	 */
	@Override
	public void addPreference(LabelPreference preference) {
		if(null == preference) {
			return;
		}
		
		if(null == this.preferences) {
			this.preferences = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.javabean.LabelPreferenceCollection.builder().build();
		}

		this.preferences.addPreference(preference);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest#setPreferences(java.util.List)
	 */
	@Override
	public void setPreferences(List<LabelPreference> preferences) {
		for (LabelPreference pre : preferences) {
			this.addPreference(pre);
		}

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest#addShipment(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment)
	 */
	@Override
	public void addShipment(Shipment shipment) {
		if(null == shipment) {
			return;
		}

		if(null == this.shipments) {
			this.shipments = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentCollection.builder().build();
		}
		
		this.shipments.addShipment(shipment);
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest#setShipments(java.util.List)
	 */
	@Override
	public void setShipments(List<Shipment> shipments) {
		for (Shipment shipment : shipments) {
			this.addShipment(shipment);
		}

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest#setShipments(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection)
	 */
	@Override
	public void setShipments(ShipmentCollection shipments) {
		this.shipments = shipments;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest#getPreference(java.lang.Integer)
	 */
	@Override
	public LabelPreference getPreference(Integer index) {
		if(null == this.preferences || null == index || index >= this.preferences.size()) {
			return null;
		}
		
		return this.preferences.getPreference(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest#getShipment(java.lang.Integer)
	 */
	@Override
	public Shipment getShipment(Integer index) {
		if(null == this.shipments || null == index || index >= this.shipments.size()) {
			return null;
		}
		
		return this.shipments.getShipment(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest#setPreferences(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceCollection)
	 */
	@Override
	public void setPreferences(LabelPreferenceCollection preferences) {
		this.preferences = preferences;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest#getPreferences()
	 */
	@Override
	public LabelPreferenceCollection getPreferences() {
		return this.preferences;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest#getShipments()
	 */
	@Override
	public ShipmentCollection getShipments() {
		return this.shipments;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelRequest#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.preferences || !this.preferences.validate()) {
			return false;
		}
		
		if(null == this.shipments || !this.shipments.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
