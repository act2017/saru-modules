/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.javabean;


import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * the javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.BarcodeId}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class BarcodeId implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.BarcodeId
								, IsSerializable {

	/**
	 * 
	 * The object builder for {@link BarcodeId}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.BarcodeId.Builder {
		private BarcodeId result = null;

		private Builder() {
			this.result = new BarcodeId();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.BarcodeId.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String id) {
			this.result.setValue(id);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public BarcodeId build() {
			return this.result;
		}

	}
	
	private String id	= null;
	
	/**
	 * 
	 */
	private BarcodeId() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.BarcodeId#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String id) {
		this.id = id;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.BarcodeId#getValue()
	 */
	@Override
	public String getValue() {

		return this.id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.BarcodeId#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.id || this.id.isEmpty()) {
			return false;
		}

		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
