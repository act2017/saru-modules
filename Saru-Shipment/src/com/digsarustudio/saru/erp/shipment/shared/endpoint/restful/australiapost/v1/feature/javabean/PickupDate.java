/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.ShipmentFeatureAttribute;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.ShipmentFeatureAttributeDate;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The pick up date of {@link ShipmentFeature}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PickupDate implements ShipmentFeature, IsSerializable {
	public static final String NAME			= "PICKUP_DATE";
	public static final String ATTR_DATE	= "date";

	/**
	 * 
	 * The object builder for {@link PickupDate}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ShipmentFeature.PickupDateBuilder {
		private PickupDate result = null;

		private Builder() {
			this.result = new PickupDate();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature.PickupDateBuilder#setDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date)
		 */
		@Override
		public PickupDateBuilder setDate(Date date) {
			this.result.setValue(date);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature.PickupDateBuilder#setDate(java.lang.String)
		 */
		@Override
		public PickupDateBuilder setDate(String date) {
			this.result.setValue(date);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature.PickupDateBuilder#setDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.ShipmentFeatureAttributeDate)
		 */
		@Override
		public PickupDateBuilder setDate(ShipmentFeatureAttributeDate date) {
			this.result.setAttribute(date);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public PickupDate build() {
			return this.result;
		}

	}
	
	private ShipmentFeatureAttribute<?>	date	= null;
	
	/**
	 * 
	 */
	private PickupDate() {
	}
	
	public void setValue(String date) {
		this.setAttribute(ShipmentFeatureAttributeDate.builder().setValue(date)
				  												.build());
	}
	
	public void setValue(Date date) {
		this.setAttribute(ShipmentFeatureAttributeDate.builder().setValue(date)
				  												.build());
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature#setAttribute(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.ShipmentFeatureAttribute)
	 */
	@Override
	public void setAttribute(ShipmentFeatureAttribute<?> attribute) {
		this.date = attribute;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature#getAttribute()
	 */
	@Override
	public ShipmentFeatureAttribute<?> getAttribute() {
		return this.date;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.date || !this.date.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
