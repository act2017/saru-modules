/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.PackingGroupDesignators;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackingGroupDesignator}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PackingGroupDesignator implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackingGroupDesignator
											 , IsSerializable {
	public static final Integer MAX_LEN	= 3;

	/**
	 * 
	 * The object builder for {@link PackingGroupDesignator}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackingGroupDesignator.Builder {
		private PackingGroupDesignator result = null;

		private Builder() {
			this.result = new PackingGroupDesignator();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackingGroupDesignator.Builder#setValue(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.PackingGroupDesignators)
		 */
		@Override
		public Builder setValue(PackingGroupDesignators value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackingGroupDesignator.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public PackingGroupDesignator build() {
			return this.result;
		}

	}
	
	private String value	= null;
	
	/**
	 * 
	 */
	private PackingGroupDesignator() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackingGroupDesignator#setValue(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.PackingGroupDesignators)
	 */
	@Override
	public void setValue(PackingGroupDesignators value) {
		this.value = value.getValue();
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackingGroupDesignator#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.value = value;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackingGroupDesignator#getPackingGroupDesignator()
	 */
	@Override
	public PackingGroupDesignators getPackingGroupDesignator() {
		
		return PackingGroupDesignators.fromValue(this.value);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackingGroupDesignator#getValue()
	 */
	@Override
	public String getValue() {
		
		return this.value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackingGroupDesignator#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.value || this.value.isEmpty()) {
			return false;
		}
		
		if( MAX_LEN < this.value.length() ) {
			return false;
		}
		
		return null;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
