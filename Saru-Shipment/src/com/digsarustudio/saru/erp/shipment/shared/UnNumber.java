/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * UN numbers are numbers that identify dangerous goods.They are assigned by the United Nations Committee of Experts 
 * on the Transport of dangerous goods. This field is an integer value, but must be passed as a string. 
 * Examples: 1170, 1789.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface UnNumber {
	/**
	 * 
	 * The builder for {@link UnNumber}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<UnNumber> {
		Builder setValue(Integer value);
		Builder setValue(String value);
	}
	
	void setValue(Integer value);
	void setValue(String value);
	Integer getValue();
	
	Boolean validate();
}
