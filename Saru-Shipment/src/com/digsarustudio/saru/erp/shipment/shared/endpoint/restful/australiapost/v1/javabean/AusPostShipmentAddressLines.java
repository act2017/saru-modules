/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AusPostShipmentAddressLines implements com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines
									,IsSerializable{
	public static final String	ATTR_LINES	= "lines";
	public static final Integer	MAX_LEN		= 40;
	public static final Integer MAX_COUNT	= 3;
	
	/**
	 * 
	 * The object builder for {@link AusPostShipmentAddressLines}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines.Builder {
		private AusPostShipmentAddressLines result = null;

		private Builder() {
			this.result = new AusPostShipmentAddressLines();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines.Builder#addAddress(java.lang.String)
		 */
		@Override
		public Builder addAddress(String address) {
			this.result.addAddress(address);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines.Builder#setAddresses(java.util.List)
		 */
		@Override
		public Builder setAddresses(List<String> addresses) {
			this.result.setAddresses(addresses);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines.Builder#setAddresses(java.lang.String[])
		 */
		@Override
		public Builder setAddresses(String[] addresses) {
			this.result.setAddresses(addresses);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AusPostShipmentAddressLines build() {
			return this.result;
		}

	}
	
	private List<String>	lines	= null;
	
	/**
	 * 
	 */
	private AusPostShipmentAddressLines() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines#addAddress(java.lang.String)
	 */
	@Override
	public void addAddress(String address) {
		if(null == this.lines) {
			this.lines = new ArrayList<String>();
		}
		
		this.lines.add(address);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines#setAddresses(java.util.List)
	 */
	@Override
	public void setAddresses(List<String> addresses) {
		this.lines = addresses;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines#getAddress(java.lang.Integer)
	 */
	@Override
	public String getAddress(Integer index) {
		if(null == this.lines || index >= this.lines.size()) {
			return null;
		}
		
		return this.lines.get(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines#setAddresses(java.lang.String[])
	 */
	@Override
	public void setAddresses(String[] addresses) {
		this.setAddresses(Arrays.asList(addresses));
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines#getAddresses()
	 */
	@Override
	public List<String> getAddresses() {
		return this.lines;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines#getCombinedLines()
	 */
	@Override
	public String getCombinedLines() {
		if(null == this.lines) {
			return null;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		Integer count = 0;
		for (String line : this.lines) {
			if( 0 != count++ ) {
				buffer.append(" ");
			}
			
			buffer.append(line);
		}
		
		
		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.lines || this.lines.isEmpty()) {
			return false;
		}
		
		for (String line : this.lines) {
			if(MAX_LEN < line.length()) {
				return false;
			}
		}
		
		return true;
	}

	/**
	 * To convert {@link AddressLines} to {@link AusPostShipmentAddressLines}.<br>
	 * 
	 * @param lines
	 * @return
	 */
	public static AusPostShipmentAddressLines copy(AddressLines lines) {
		if(null == lines) {
			return null;
		}
		
		AusPostShipmentAddressLines.Builder builder = AusPostShipmentAddressLines.builder();
		
		builder.setAddresses(lines.getAddresses());
		
		return builder.build();
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	protected Boolean isExceedTotalMaxLength() {
		if(null == this.lines) {
			return true;
		}
		
		Integer currentLen = 0;
		for (String line : this.lines) {
			if(null == line) {
				continue;
			}
			
			currentLen += line.length();
		}
				
		return (MAX_LEN < currentLen);
	}
}
