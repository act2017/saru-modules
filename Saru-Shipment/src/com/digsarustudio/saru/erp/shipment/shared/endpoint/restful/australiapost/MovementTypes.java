/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Use "DESPATCH" to indicate an outbound or normal shipment (this is the default), 
 * "RETURN" to indicate a returns shipment, 
 * "TRANSFER" to indicate a transfer. "TRANSFER" is only available for StarTrack products.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @reference	<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments</a>
 */
public class MovementTypes implements IsSerializable{
	/**
	 * "DESPATCH" to indicate an outbound or normal shipment (this is the default)
	 */
	public static final MovementTypes Despatch		= new MovementTypes("DESPATCH", "Despatch");
	
	/**
	 * "RETURN" to indicate a returns shipment
	 */
	public static final MovementTypes Return		= new MovementTypes("RETURN", "Return");
	
	/**
	 * "TRANSFER" to indicate a transfer. "TRANSFER" is only available for StarTrack products.
	 */
	public static final MovementTypes Transfer		= new MovementTypes("TRANSFER", "Transfer");


	private static Map<String, MovementTypes> VALUES = new HashMap<>();
	
	static {
		VALUES.put(Despatch.getValue(), Despatch);
		VALUES.put(Return.getValue(), Return);
		VALUES.put(Transfer.getValue(), Transfer);
	}	
	
	private String value = null;
	private String display = null;
	
	private MovementTypes() {
		
	}
	
	private MovementTypes(String value, String display) {
		this.value = value;
		this.display = display;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public String getDisplayString() {
		return this.display;
	}
	
	public static MovementTypes fromValue(String value) {
		if (null == value || !VALUES.containsKey(value)) {
			return null;
		}
		
		return VALUES.get(value);
	}
	
	public static MovementTypes getDefault() {
		return Despatch;
	}
}
