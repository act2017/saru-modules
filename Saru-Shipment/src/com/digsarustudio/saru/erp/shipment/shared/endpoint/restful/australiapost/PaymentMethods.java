/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost;

import java.util.HashMap;
import java.util.Map;

/**
 * The intended method of payment, the only valid value is: CHARGE_TO_ACCOUNT.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PaymentMethods {
	public static final PaymentMethods	ChargeToAccount	= new PaymentMethods("CHARGE_TO_ACCOUNT", "Charge To Account");	
	
	private static Map<String, PaymentMethods> VALUES = new HashMap<>();
		
	static {
		VALUES.put(ChargeToAccount.getValue(), ChargeToAccount);
	}	
	
	private String value = null;
	private String display = null;
	
	private PaymentMethods() {
		
	}
	
	private PaymentMethods(String value, String display) {
		this.value = value;
		this.display = display;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public String getDisplayString() {
		return this.display;
	}
	
	public static PaymentMethods fromValue(String value) {
		if (null == value || !VALUES.containsKey(value)) {
			return null;
		}
		
		return VALUES.get(value);
	}
	
	public static PaymentMethods getDefault() {
		return ChargeToAccount;
	}
		
}
