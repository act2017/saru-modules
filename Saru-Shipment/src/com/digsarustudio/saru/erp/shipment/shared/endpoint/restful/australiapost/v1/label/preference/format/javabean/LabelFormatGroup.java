/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelFormatGroups;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatGroup}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelFormatGroup implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatGroup
										, IsSerializable {
	public static final LabelFormatGroupParcelPost	ParcelPost	= new LabelFormatGroupParcelPost();
	public static final LabelFormatGroupExpressPost	ExpressPost	= new LabelFormatGroupExpressPost();
	public static final LabelFormatGroupStarTrack	StarTrack	= new LabelFormatGroupStarTrack();
	
	/**
	 * 
	 * The object builder for {@link LabelFormatGroup}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatGroup.Builder {
		private LabelFormatGroup result = null;

		private Builder() {
			this.result = new LabelFormatGroup();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatGroup.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatGroup.Builder#setValue(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelFormatGroups)
		 */
		@Override
		public Builder setValue(LabelFormatGroups value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public LabelFormatGroup build() {
			return this.result;
		}

	}
	
	private String value	= null;
	
	/**
	 * 
	 */
	protected LabelFormatGroup() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatGroup#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.value = value;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatGroup#getValue()
	 */
	@Override
	public String getValue() {

		return this.value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatGroup#setValue(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelFormatGroups)
	 */
	@Override
	public void setValue(LabelFormatGroups value) {
		if(null == value) {
			return;
		}
		
		this.setValue(value.getValue());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatGroup#getLabelFormatGroups()
	 */
	@Override
	public LabelFormatGroups getLabelFormatGroups() {
		return LabelFormatGroups.fromValue(this.value);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatGroup#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.value || this.value.isEmpty()) {
			return false;
		}

		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
