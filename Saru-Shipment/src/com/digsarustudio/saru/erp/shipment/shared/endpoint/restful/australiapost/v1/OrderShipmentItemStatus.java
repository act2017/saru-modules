/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.OrderShipmentItemStatuses;

/**
 * The status for the item.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface OrderShipmentItemStatus {
	/**
	 * 
	 * The builder for {@link OrderShipmentItemStatus}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<OrderShipmentItemStatus> {
		Builder setStatus(String status);
		Builder setStatus(OrderShipmentItemStatuses status);
	}

	void setStatus(String status);
	void setStatus(OrderShipmentItemStatuses status);
	
	String getStatus();	
	OrderShipmentItemStatuses getOrderShipmentItemStatuses();
	
	Boolean validate();
}
