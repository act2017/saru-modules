/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemDescription}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemDescription implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemDescription
									  , IsSerializable {
	public static final Integer MAX_LEN	= 50;

	/**
	 * 
	 * The object builder for {@link ItemDescription}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemDescription.Builder {
		private ItemDescription result = null;

		private Builder() {
			this.result = new ItemDescription();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemDescription.Builder#setDescription(java.lang.String)
		 */
		@Override
		public Builder setDescription(String desc) {
			this.result.setDescription(desc);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemDescription build() {
			return this.result;
		}

	}
	
	private String	description	= null;
	
	/**
	 * 
	 */
	private ItemDescription() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemDescription#setDescription(java.lang.String)
	 */
	@Override
	public void setDescription(String desc) {
		this.description = (null == desc || MAX_LEN >= desc.length()) ? desc : desc.substring(0, MAX_LEN);

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemDescription#getDescription()
	 */
	@Override
	public String getDescription() {
		
		return this.description;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemDescription#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.description || this.description.isEmpty()) {
			return false;
		}
		
		if(MAX_LEN < this.description.length()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
