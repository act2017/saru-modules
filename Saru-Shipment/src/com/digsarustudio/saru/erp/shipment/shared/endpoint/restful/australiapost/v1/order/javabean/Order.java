/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean;

import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Consignor;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderSummary;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.PaymentMethod;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class Order implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order
							, IsSerializable {
	public static final String	ATTR_ORDER_ID			= "order_id";
	public static final String	ATTR_ORDER_REFERENCE	= "order_reference";
	public static final String	ATTR_ORDER_CREATION_DATE= "order_creation_date";
	public static final String	ATTR_ORDER_SUMMARY		= "order_summary";
	public static final String	ATTR_SHIPMENTS			= "shipments";
	public static final String	ATTR_PAYMENT_METHOD		= "payment_method";
	
	public static final String	ATTR_CONSIGNOR			= "consignor";
	
	/**
	 * 
	 * The object builder for {@link Order}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order.Builder {
		private Order result = null;

		private Builder() {
			this.result = new Order();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order.Builder#setOrderId(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderId)
		 */
		@Override
		public Builder setOrderId(OrderId id) {
			this.result.setOrderId(id);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order.Builder#setOrderReference(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderReference)
		 */
		@Override
		public Builder setOrderReference(OrderReference reference) {
			this.result.setOrderReference(reference);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order.Builder#setOrderCreationDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime)
		 */
		@Override
		public Builder setOrderCreationDate(DateTime date) {
			this.result.setOrderCreationDate(date);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order.Builder#setOrderSummary(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderSummary)
		 */
		@Override
		public Builder setOrderSummary(OrderSummary summary) {
			this.result.setOrderSummary(summary);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order.Builder#addShipment(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment)
		 */
		@Override
		public Builder addShipment(Shipment shipment) {
			this.result.addShipment(shipment);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order.Builder#setShipments(java.util.List)
		 */
		@Override
		public Builder setShipments(List<Shipment> shipments) {
			this.result.setShipments(shipments);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order.Builder#setShipments(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection)
		 */
		@Override
		public Builder setShipments(ShipmentCollection shipments) {
			this.result.setShipments(shipments);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order.Builder#setPaymentMethod(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.PaymentMethod)
		 */
		@Override
		public Builder setPaymentMethod(PaymentMethod method) {
			this.result.setPaymentMethod(method);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order.Builder#setConsignor(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Consignor)
		 */
		@Override
		public Builder setConsignor(Consignor consignor) {
			this.result.setConsignor(consignor);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Order build() {
			return this.result;
		}

	}
	
	private OrderId				order_id			= null;
	private OrderReference		order_reference		= null;
	private DateTime			order_creation_date	= null;
	private OrderSummary		order_summary		= null;
	private ShipmentCollection	shipments			= null;
	private PaymentMethod		payment_method		= null;
	private Consignor			consignor			= null;
	
	/**
	 * 
	 */
	private Order() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order#setOrderId(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderId)
	 */
	@Override
	public void setOrderId(OrderId id) {
		this.order_id = id;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order#setOrderReference(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderReference)
	 */
	@Override
	public void setOrderReference(OrderReference reference) {
		this.order_reference = reference;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order#setOrderCreationDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime)
	 */
	@Override
	public void setOrderCreationDate(DateTime date) {
		this.order_creation_date = date;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order#setOrderSummary(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderSummary)
	 */
	@Override
	public void setOrderSummary(OrderSummary summary) {
		this.order_summary = summary;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order#addShipment(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment)
	 */
	@Override
	public void addShipment(Shipment shipment) {
		if(null == shipment) {
			return;
		}else if(null == this.shipments) {
			this.shipments = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentCollection.builder().build();
		}

		this.shipments.addShipment(shipment);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order#setShipments(java.util.List)
	 */
	@Override
	public void setShipments(List<Shipment> shipments) {
		if(null == shipments) {
			return;
		}
		
		for (Shipment shipment : shipments) {
			this.shipments.addShipment(shipment);
		}

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order#setShipments(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection)
	 */
	@Override
	public void setShipments(ShipmentCollection shipments) {
		this.shipments = shipments;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order#setPaymentMethod(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.PaymentMethod)
	 */
	@Override
	public void setPaymentMethod(PaymentMethod method) {
		this.payment_method = method;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order#getOrderId()
	 */
	@Override
	public OrderId getOrderId() {
		
		return this.order_id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order#getOrderReference()
	 */
	@Override
	public OrderReference getOrderReference() {
		
		return this.order_reference;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order#getOrderCreationDate()
	 */
	@Override
	public DateTime getOrderCreationDate() {
		
		return this.order_creation_date;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order#getOrderSummary()
	 */
	@Override
	public OrderSummary getOrderSummary() {
		
		return this.order_summary;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order#getShipment(java.lang.Integer)
	 */
	@Override
	public Shipment getShipment(Integer index) {
		if(null == this.shipments || null == index || index >= this.shipments.size()) {
			return null;
		}
		
		return this.shipments.getShipment(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order#getShipments()
	 */
	@Override
	public ShipmentCollection getShipments() {
		
		return this.shipments;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order#getPaymentMethod()
	 */
	@Override
	public PaymentMethod getPaymentMethod() {
		
		return this.payment_method;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order#setConsignor(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Consignor)
	 */
	@Override
	public void setConsignor(Consignor consignor) {
		this.consignor = consignor;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order#getConsignor()
	 */
	@Override
	public Consignor getConsignor() {
		return this.consignor;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.order_id || !this.order_id.validate()) {
			return false;
		}
		
		if(null != this.order_reference && !this.order_reference.validate()) {
			return false;
		}
		
		if(null != this.order_creation_date && !this.order_creation_date.validate()) {
			return false;
		}
		
		if(null != this.order_summary && !this.order_summary.validate()) {
			return false;
		}
		
		if(null != this.shipments && !this.shipments.validate()) {
			return false;
		}
		
		if(null != this.payment_method && !this.payment_method.validate()) {
			return false;
		}
		
		if(null != this.consignor && !this.consignor.validate()) {
			return false;
		}
				
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
