/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The implementation of {@link EmailAddress} for {@link Shipment} to use.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AusPostShipmentEmail implements EmailAddress, IsSerializable {
	public static final Integer MAX_LEN	= 50;

	/**
	 * 
	 * The object builder for {@link AusPostShipmentEmail}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements EmailAddress.Builder {
		private AusPostShipmentEmail result = null;

		private Builder() {
			this.result = new AusPostShipmentEmail();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress.Builder#setAccountName(java.lang.String)
		 */
		@Override
		public Builder setAccountName(String name) {
			this.result.setAccountName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress.Builder#setDomainName(java.lang.String)
		 */
		@Override
		public Builder setDomainName(String name) {
			this.result.setDomainName(name);			
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress.Builder#setAddress(java.lang.String)
		 */
		@Override
		public Builder setAddress(String address) {
			this.result.setAddress(address);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AusPostShipmentEmail build() {
			return this.result;
		}

	}
	
	private String	email	= null;
	
	/**
	 * 
	 */
	private AusPostShipmentEmail() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress#setAccountName(java.lang.String)
	 */
	@Override
	public void setAccountName(String name) {
		throw new UnsupportedOperationException("Please use #setAddress() instead");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress#setDomainName(java.lang.String)
	 */
	@Override
	public void setDomainName(String name) {
		throw new UnsupportedOperationException("Please use #setAddress() instead");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress#setAddress(java.lang.String)
	 */
	@Override
	public void setAddress(String address) {
		this.email = address;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress#getAccountName()
	 */
	@Override
	public String getAccountName() {
		if(null == this.email) {
			return null;
		}
		
		String[] fields = this.email.split(EmailAddress.ACCOUNT_DELIMITER);
		
		return fields[0];
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress#getDomainName()
	 */
	@Override
	public String getDomainName() {
		if(null == this.email) {
			return null;
		}
		
		String[] fields = this.email.split(EmailAddress.ACCOUNT_DELIMITER);
		if( 1 > fields.length ) {
			return null;
		}
		
		return fields[1];
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress#getAddress()
	 */
	@Override
	public String getAddress() {
		
		return this.email;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.email || this.email.isEmpty()) {
			return false;
		}
		
		if(MAX_LEN < this.email.length()) {
			return false;
		}
		
		return true;
	}

	public static AusPostShipmentEmail copy(EmailAddress source) {
		if(null == source) {
			return null;
		}
		
		AusPostShipmentEmail.Builder builder = AusPostShipmentEmail.builder();
		
		builder.setAddress(source.getAddress());
		
		return builder.build();
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
