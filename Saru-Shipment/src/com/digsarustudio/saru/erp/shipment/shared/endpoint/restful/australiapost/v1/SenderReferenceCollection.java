/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * A merchant specified array of references for the shipment. 
 * Effectively deprecates customer_reference_1 and customer_reference_2. 
 * The array length supported varies as noted in the 
 * <a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">
 * Create Shipments</a> service.<br>
 * <br>
 * The introduction of the sender_references array effectively deprecates the existing fields 
 * customer_reference_1 and customer_reference_2. For convenience, the value of both of the above fields 
 * are copied into this array, making the use of these existing fields backward compatible.<br>
 * <br>
 * Use of the sender_references or the existing customer_reference_1 and 2 are mutually exclusive. 
 * Attempting to provide both an array and individual fields will cause a failure with appropriate error 
 * responses.<br>
 * <br>
 * The length of the array varies dependent on the product family. 
 * StarTrack products are unrestricted, whereas Australia Post products are currently limited to a 
 * maximum array length of two (2).
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface SenderReferenceCollection {
	/**
	 * 
	 * The builder for {@link SenderReferenceCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<SenderReferenceCollection> {
		Builder addSenderReference(SenderReference reference);
		Builder setSenderReferences(List<SenderReference> references);
		Builder setSenderReferences(SenderReference[] references);
	}
	
	void addSenderReference(SenderReference reference);
	void setSenderReferences(List<SenderReference> references);
	void setSenderReferences(SenderReference[] references);
	
	SenderReference getSenderReference(Integer index);
	List<SenderReference> getSenderReferences();
	
	Boolean validate();
}
