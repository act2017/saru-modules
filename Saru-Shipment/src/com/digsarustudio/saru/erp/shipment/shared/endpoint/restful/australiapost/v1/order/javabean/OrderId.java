/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderRequest;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The implementation of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderId} used for {@link CreateOrderRequest}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class OrderId implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderId
							  , IsSerializable {
	public static final Integer	MAX_LEN	= 50;

	/**
	 * 
	 * The object builder for {@link OrderId}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderId.Builder {
		private OrderId result = null;

		private Builder() {
			this.result = new OrderId();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderId.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String id) {
			this.result.setValue(id);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public OrderId build() {
			return this.result;
		}

	}
	
	private String id = null;
	
	/**
	 * 
	 */
	private OrderId() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderId#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderId#getValue()
	 */
	@Override
	public String getValue() {
		return this.id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderId#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.id || this.id.isEmpty()){
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
