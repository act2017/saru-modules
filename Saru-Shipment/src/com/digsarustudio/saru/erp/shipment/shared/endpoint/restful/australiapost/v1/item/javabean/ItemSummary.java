/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalGst;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemSummary}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemSummary implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemSummary
								  , IsSerializable {
	public static final	String	ATTR_TOTAL_COST	= "total_cost";
	public static final	String	ATTR_TOTAL_GST	= "total_gst";
	public static final	String	ATTR_STATUS		= "status";
	
	/**
	 * 
	 * The object builder for {@link ItemSummary}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemSummary.Builder {
		private ItemSummary result = null;

		private Builder() {
			this.result = new ItemSummary();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemSummary.Builder#setTotalCost(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost)
		 */
		@Override
		public Builder setTotalCost(TotalCost cost) {
			this.result.setTotalCost(cost);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemSummary.Builder#setTotalGST(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalGst)
		 */
		@Override
		public Builder setTotalGST(TotalGst gst) {
			this.result.setTotalGST(gst);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemSummary.Builder#setStatus(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus)
		 */
		@Override
		public Builder setStatus(OrderShipmentItemStatus status) {
			this.result.setStatus(status);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemSummary build() {
			return this.result;
		}

	}
	
	private TotalCost				total_cost	= null;
	private	TotalGst				total_gst	= null;
	private	OrderShipmentItemStatus	status		= null;

	/**
	 * 
	 */
	private ItemSummary() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemSummary#getTotalCost()
	 */
	@Override
	public TotalCost getTotalCost() {
		return this.total_cost;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemSummary#setTotalCost(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost)
	 */
	@Override
	public void setTotalCost(TotalCost cost) {
		this.total_cost = cost;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemSummary#getTotalGST()
	 */
	@Override
	public TotalGst getTotalGST() {
		return this.total_gst;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemSummary#setTotalGST(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalGst)
	 */
	@Override
	public void setTotalGST(TotalGst gst) {
		this.total_gst = gst;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemSummary#getStatus()
	 */
	@Override
	public OrderShipmentItemStatus getStatus() {
		
		return this.status;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemSummary#setStatus(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus)
	 */
	@Override
	public void setStatus(OrderShipmentItemStatus status) {
		this.status = status;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemSummary#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.total_cost || !this.total_cost.validate()) {
			return false;
		}

		if(null == this.total_gst || !this.total_gst.validate()) {
			return false;
		}

		if(null == this.status || !this.status.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
