/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The context for the error with the name and value.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface AusPostErrorContextPair {
	/**
	 * 
	 * The builder for {@link AusPostErrorContextPair}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<AusPostErrorContextPair> {
		Builder setName(String name);
		Builder setValue(String value);
	}
	
	void setName(String name);
	void setValue(String value);
	
	String getName();
	String getValue();
	
	Boolean validate();
}
