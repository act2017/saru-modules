/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The required preferences for label printing to be applied.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-labels">
 *
 */
public interface LabelPreferenceCollection {
	/**
	 * 
	 * The builder for {@link LabelPreferenceCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<LabelPreferenceCollection> {
		Builder addPreference(LabelPreference preference);
		Builder setPreferences(List<LabelPreference> preferences);
	}
	
	void addPreference(LabelPreference preference);
	void setPreferences(List<LabelPreference> preferences);
	
	LabelPreference			getPreference(Integer index);
	List<LabelPreference>	getPreferences();
	
	Integer					size();
	Boolean					isEmpty();
	
	Boolean validate();	
}
