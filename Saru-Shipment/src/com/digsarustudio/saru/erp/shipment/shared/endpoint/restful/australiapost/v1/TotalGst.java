/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The goods and services tax amount included in the total cost. 
 * This field is not used for StarTrack shipments, as they are costed at the shipment level only.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface TotalGst {
	/**
	 * 
	 * The builder for {@link TotalGst}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<TotalGst> {
		Builder setValue(Integer gst);
		Builder setValue(Integer gst, Integer precision);

		Builder setValue(String gst);
		Builder setValue(String gst, String precision);
	}
	
	void setValue(Integer gst);
	void setValue(Integer gst, Integer precision);

	void setValue(String gst);
	void setValue(String gst, String precision);
	
	String getGst();
	String getPrcision();
	String getDecimalGst();
	
	Boolean validate();
}
