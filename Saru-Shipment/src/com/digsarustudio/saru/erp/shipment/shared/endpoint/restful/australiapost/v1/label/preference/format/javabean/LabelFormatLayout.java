/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelFormatLayouts;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatLayout}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelFormatLayout implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatLayout
										, IsSerializable {
	/**
	 * 
	 * The object builder for {@link LabelFormatLayout}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatLayout.Builder {
		private LabelFormatLayout result = null;

		private Builder() {
			this.result = new LabelFormatLayout();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatLayout.Builder#setLayout(java.lang.String)
		 */
		@Override
		public Builder setLayout(String layout) {
			this.result.setLayout(layout);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatLayout.Builder#setLayout(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelFormatLayouts)
		 */
		@Override
		public Builder setLayout(LabelFormatLayouts layout) {
			this.result.setLayout(layout);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public LabelFormatLayout build() {
			return this.result;
		}

	}
	
	private String layout	= null;
	
	/**
	 * 
	 */
	protected LabelFormatLayout() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatLayout#setLayout(java.lang.String)
	 */
	@Override
	public void setLayout(String layout) {
		this.layout = layout;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatLayout#getLayout()
	 */
	@Override
	public String getLayout() {
		
		return this.layout;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatLayout#setLayout(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelFormatLayouts)
	 */
	@Override
	public void setLayout(LabelFormatLayouts layout) {
		this.layout = layout.getValue();
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatLayout#getLabelFormatLayouts()
	 */
	@Override
	public LabelFormatLayouts getLabelFormatLayouts() {
		
		return LabelFormatLayouts.fromValue(this.layout);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatLayout#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.layout || this.layout.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
