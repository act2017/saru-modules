/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Description of the goods. This is printed on the auxiliary label. 
 * The maximum of 40 characters.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 */
public interface GoodsDescription {
	/**
	 * 
	 * The builder for {@link GoodsDescription}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<GoodsDescription> {
		Builder setDescription(String desc);
	}
	
	void setDescription(String desc);
	String getDescription();
	
	Boolean validate();
}
