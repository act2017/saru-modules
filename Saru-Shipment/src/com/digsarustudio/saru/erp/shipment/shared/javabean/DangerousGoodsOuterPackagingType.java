/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.javabean;

import com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsPackagingType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The outer packaging type of {@link DangerousGoods}.<br> 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DangerousGoodsOuterPackagingType implements DangerousGoodsPackagingType, IsSerializable {
	public static final Integer MAX_LEN	= 25;
	
	/**
	 * 
	 * The object builder for {@link DangerousGoodsOuterPackagingType}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements DangerousGoodsPackagingType.Builder {
		private DangerousGoodsOuterPackagingType result = null;

		private Builder() {
			this.result = new DangerousGoodsOuterPackagingType();
		}

		
		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsPackagingType.Builder#setDescription(java.lang.String)
		 */
		@Override
		public Builder setDescription(String description) {
			this.result.setDescription(description);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DangerousGoodsOuterPackagingType build() {
			return this.result;
		}

	}
	
	private String description	= null;

	/**
	 * 
	 */
	private DangerousGoodsOuterPackagingType() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsPackagingType#setDescription(java.lang.String)
	 */
	@Override
	public void setDescription(String description) {
		this.description = description;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsPackagingType#getDescription()
	 */
	@Override
	public String getDescription() {
		
		return this.description;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsPackagingType#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.description || this.description.isEmpty()) {
			return false;
		}
		
		if(MAX_LEN < this.description.length()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
