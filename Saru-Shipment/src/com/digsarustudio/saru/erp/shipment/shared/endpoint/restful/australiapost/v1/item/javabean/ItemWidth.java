/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWidth}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemWidth implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWidth
								 , IsSerializable {
	public static final Integer	MAX_LEN_MAIN		= 4;
	public static final Integer	MAX_LEN_PRECISION	= 1;
	
	/**
	 * 
	 * The object builder for {@link ItemWidth}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWidth.Builder {
		private ItemWidth result = null;

		private Builder() {
			this.result = new ItemWidth();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWidth.Builder#setWidth(java.lang.Integer)
		 */
		@Override
		public Builder setWidth(Integer length) {
			this.result.setWidth(length);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWidth.Builder#setWidth(java.lang.Integer, java.lang.Integer)
		 */
		@Override
		public Builder setWidth(Integer length, Integer precision) {
			this.result.setWidth(length, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWidth.Builder#setWidth(java.lang.String)
		 */
		@Override
		public Builder setWidth(String length) {
			this.result.setWidth(length);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWidth.Builder#setWidth(java.lang.String, java.lang.String)
		 */
		@Override
		public Builder setWidth(String width, String precision) {
			this.result.setWidth(width, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemWidth build() {
			return this.result;
		}

	}
	
	private String main			= null;
	private String precision	= null;
	
	/**
	 * 
	 */
	public ItemWidth() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWidth#setWidth(java.lang.Integer)
	 */
	@Override
	public void setWidth(Integer length) {
		this.setWidth(length, null);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWidth#setWidth(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void setWidth(Integer length, Integer precision) {
		String main = (null == length) ? null : length.toString();
		String pre	= (null == precision) ? null : precision.toString();
		
		this.setWidth(main, pre);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWidth#setWidth(java.lang.String)
	 */
	@Override
	public void setWidth(String length) {
		if(null == length) {
			return;
		}
		
		String[] weights = length.split(DECIMAL_DELIMITER);
		
		String main = weights[0];
		String pre	= null;
		
		if(2 == weights.length) {
			pre = weights[1];
		}
		
		this.setWidth(main, pre);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWidth#setWidth(java.lang.String, java.lang.String)
	 */
	@Override
	public void setWidth(String width, String precision) {
		this.main		= width;
		this.precision	= precision;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWidth#getWidth()
	 */
	@Override
	public Integer getWidth() {
		return Integer.parseInt(this.main);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWidth#getPrecision()
	 */
	@Override
	public Integer getPrecision() {
		return Integer.parseInt(this.precision);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWidth#getDecimalWidth()
	 */
	@Override
	public String getDecimalWidth() {
		if(null == this.main && null == this.precision) {
			return null;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		if(null != this.main && !this.main.isEmpty()) {
			buffer.append(this.main);
		}else {
			buffer.append("0");
		}
		
		if(null != this.precision) {
			buffer.append(DECIMAL_GLUE);
			buffer.append(this.precision);
		}

		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWidth#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.main && null == this.precision) {
			return false;
		}
		
		if(this.main.isEmpty() && this.precision.isEmpty()) {
			return false;
		}
		
		if( null != this.main 
			&& ( MAX_LEN_MAIN < this.main.length()) ) {
			return false;
		}
		
		if( null != this.precision 
			&& (MAX_LEN_PRECISION < this.precision.length()) ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
