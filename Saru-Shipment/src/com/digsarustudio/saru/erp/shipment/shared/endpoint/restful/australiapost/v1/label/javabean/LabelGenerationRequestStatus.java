/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.LabelGenerationRequestStatuses;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelGenerationRequestStatus}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelGenerationRequestStatus implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelGenerationRequestStatus
								  , IsSerializable {
	/**
	 * 
	 * The object builder for {@link LabelGenerationRequestStatus}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelGenerationRequestStatus.Builder {
		private LabelGenerationRequestStatus result = null;

		private Builder() {
			this.result = new LabelGenerationRequestStatus();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.LabelStatus.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.LabelStatus.Builder#setValue(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.LabelStatuses)
		 */
		@Override
		public Builder setValue(LabelGenerationRequestStatuses value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public LabelGenerationRequestStatus build() {
			return this.result;
		}

	}
	
	private String	status	= null;
	
	/**
	 * 
	 */
	private LabelGenerationRequestStatus() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.LabelStatus#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.status = value;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.LabelStatus#setValue(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.LabelStatuses)
	 */
	@Override
	public void setValue(LabelGenerationRequestStatuses value) {
		if(null == value) {
			return;
		}

		this.status = value.getValue();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.LabelStatus#getValue()
	 */
	@Override
	public String getValue() {
		
		return this.status;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.LabelStatus#getLabelStatus()
	 */
	@Override
	public LabelGenerationRequestStatuses getLabelStatus() {
		
		return LabelGenerationRequestStatuses.fromValue(this.status);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.LabelStatus#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.status || this.status.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
