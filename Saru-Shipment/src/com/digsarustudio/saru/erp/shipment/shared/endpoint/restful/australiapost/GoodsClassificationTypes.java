/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Category or classification of the goods. 
 * If commercial_value is true, then this field must be set to "OTHER", 
 * otherwise it must be set to "GIFT", "SAMPLE", "DOCUMENT" or "RETURN".<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
 *
 */
public class GoodsClassificationTypes implements IsSerializable{
	public static final GoodsClassificationTypes	Other	= new GoodsClassificationTypes("OTHER", "Other");
	public static final GoodsClassificationTypes	Gift	= new GoodsClassificationTypes("GIFT", "Gift");
	public static final GoodsClassificationTypes	Sample	= new GoodsClassificationTypes("SAMPLE", "Sample");
	public static final GoodsClassificationTypes	Document= new GoodsClassificationTypes("DOCUMENT", "Document");
	public static final GoodsClassificationTypes	Return	= new GoodsClassificationTypes("RETURN", "Return");

	private static Map<String, GoodsClassificationTypes> VALUES = new HashMap<>();
		
	static {
		VALUES.put(Other.getValue(), Other);
		VALUES.put(Gift.getValue(), Gift);
		VALUES.put(Sample.getValue(), Sample);
		VALUES.put(Document.getValue(), Document);
		VALUES.put(Return.getValue(), Return);
	}	
	
	private String value = null;
	private String display = null;
	
	private GoodsClassificationTypes() {
		
	}
	
	private GoodsClassificationTypes(String value, String display) {
		this.value = value;
		this.display = display;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public String getDisplayString() {
		return this.display;
	}
	
	public static GoodsClassificationTypes fromValue(String value) {
		if (null == value || !VALUES.containsKey(value)) {
			return null;
		}
		
		return VALUES.get(value);
	}

	public static GoodsClassificationTypes getDefault() {
		return Other;
	}
}
