/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The format for Australia to print out the label.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface LabelFormat {
	/**
	 * 
	 * The builder for {@link LabelFormat}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<LabelFormat> {
		Builder setGroup(LabelFormatGroup group);
		Builder setLayout(LabelFormatLayout layout);
		Builder setLabelBranded(LabelBranded branded);
		Builder setLeftOffset(LabelFormatOffset offset);
		Builder setTopOffset(LabelFormatOffset offset);
	}
	
	void setGroup(LabelFormatGroup group);
	void setLayout(LabelFormatLayout layout);
	void setLabelBranded(LabelBranded branded);
	void setLeftOffset(LabelFormatOffset offset);
	void setTopOffset(LabelFormatOffset offset);
		
	LabelFormatGroup	getGroup();
	LabelFormatLayout	getLayout();
	LabelBranded		getLabelBranded();
	LabelFormatOffset	getLeftOffset();
	LabelFormatOffset 	getTopOffset();
	
	Boolean validate();
}
