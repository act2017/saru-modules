/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.javabean;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationResponse}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SuburbValidationResponse implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationResponse
												, IsSerializable {
	public static final String ATTR_FOUND	= "found";
	public static final String ATTR_RESULTS	= "results";
	
	/**
	 * 
	 * The object builder for {@link SuburbValidationResponse}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationResponse.Builder {
		private SuburbValidationResponse result = null;

		private Builder() {
			this.result = new SuburbValidationResponse();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationResponse.Builder#setFound()
		 */
		@Override
		public Builder setFound() {
			this.result.setFound();
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationResponse.Builder#setNotFound()
		 */
		@Override
		public Builder setNotFound() {
			this.result.setNotFound();
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationResponse.Builder#addSuburb(java.lang.String)
		 */
		@Override
		public Builder addSuburb(String suburb) {
			this.result.addSuburb(suburb);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationResponse.Builder#addSuburb(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb)
		 */
		@Override
		public Builder addSuburb(Suburb suburb) {
			this.result.addSuburb(suburb);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationResponse.Builder#setSuburbs(java.util.List)
		 */
		@Override
		public Builder setSuburbs(List<String> suburbs) {
			this.result.setSuburbs(suburbs);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public SuburbValidationResponse build() {
			return this.result;
		}

	}
	
	private Boolean 		found	= null;
	private	List<Suburb>	results	= null;
	
	/**
	 * 
	 */
	private SuburbValidationResponse() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationResponse#setFound()
	 */
	@Override
	public void setFound() {
		this.found = true;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationResponse#setNotFound()
	 */
	@Override
	public void setNotFound() {
		this.found = false;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValiationResponse#addSuburb(java.lang.String)
	 */
	@Override
	public void addSuburb(String suburb) {
		if(null == suburb || suburb.isEmpty()) {
			return;
		}

		this.addSuburb(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.javabean.Suburb.builder().setName(suburb)
																											   .build());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValiationResponse#addSuburb(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb)
	 */
	@Override
	public void addSuburb(Suburb suburb) {
		if(null == suburb || !suburb.validate()) {
			return;
		}else if( null == this.results) {
			this.results = new ArrayList<>();
		}

		this.results.add(suburb);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValiationResponse#setSuburbs(java.util.List)
	 */
	@Override
	public void setSuburbs(List<String> suburbs) {
		for (String suburb : suburbs) {
			this.addSuburb(suburb);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValiationResponse#getSuburb(java.lang.Integer)
	 */
	@Override
	public Suburb getSuburb(Integer index) {
		if(null == this.results || null == index || index >= this.results.size()) {
			return null;
		}
		
		return this.results.get(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValiationResponse#getSuburbs()
	 */
	@Override
	public List<Suburb> getSuburbs() {
		
		return this.results;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValiationResponse#getSuburbsString()
	 */
	@Override
	public List<String> getSuburbsString() {
		List<String> rtns = new ArrayList<>();
		
		for (Suburb suburb : this.results) {
			rtns.add(suburb.getName());
		}
		
		return rtns;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValidationResponse#isFound()
	 */
	@Override
	public Boolean isFound() {
		return this.found;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address.SuburbValiationResponse#validate()
	 */
	@Override
	public Boolean validate() {
		if( null == this.found || !this.found ) {
			return false;
		}
		
		if(null == this.results || this.results.isEmpty()){
			return false;
		}
		
		return true;
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
