/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import java.util.HashMap;
import java.util.Map;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummaryPair;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummary}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class TrackingSummary implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummary
												, IsSerializable {
	
	/**
	 * 
	 * The object builder for {@link TrackingSummary}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummary.Builder {
		private TrackingSummary result = null;

		private Builder() {
			this.result = new TrackingSummary();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummary.Builder#addPair(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummaryPair)
		 */
		@Override
		public Builder addPair(TrackingSummaryPair summary) {
			this.result.addPair(summary);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummary.Builder#addPair(java.lang.String, java.lang.String)
		 */
		@Override
		public Builder addPair(String key, String value) {
			this.result.addPair(key, value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public TrackingSummary build() {
			return this.result;
		}

	}
		
	private Map<String, TrackingSummaryPair> pairs	= null;

	/**
	 * 
	 */
	private TrackingSummary() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummary#addPair(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummaryPair)
	 */
	@Override
	public void addPair(TrackingSummaryPair summary) {
		if(null == summary) {
			return;
		}
		
		if(null == this.pairs) {
			this.pairs = new HashMap<String, TrackingSummaryPair>();
		}
		
		this.pairs.put(summary.getValue(), summary);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummary#addPair(java.lang.String, java.lang.String)
	 */
	@Override
	public void addPair(String key, String value) {
		if(null == key || key.isEmpty()) {
			return;
		}
		
		this.addPair(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.TrackingSummaryPair.builder().setName(key)
																																		.setValue(value)
																																		.build());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummary#getPair(java.lang.String)
	 */
	@Override
	public TrackingSummaryPair getPair(String key) {
		if(null == key || !this.pairs.containsKey(key)) {
			return null;
		}
		
		
		return this.pairs.get(key);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummary#getPairs()
	 */
	@Override
	public Map<String, TrackingSummaryPair> getPairs() {
		return this.pairs;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummaryCollection#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.pairs || this.pairs.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
