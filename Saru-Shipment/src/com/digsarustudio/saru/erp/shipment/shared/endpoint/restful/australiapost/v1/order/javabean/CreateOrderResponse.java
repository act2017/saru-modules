/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderResponse}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class CreateOrderResponse implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderResponse
										  , IsSerializable {
	public static final String	ATTR_ORDER	= "order";
	
	/**
	 * 
	 * The object builder for {@link CreateOrderResponse}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderResponse.Builder {
		private CreateOrderResponse result = null;

		private Builder() {
			this.result = new CreateOrderResponse();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderResponse.Builder#setOrder(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order)
		 */
		@Override
		public Builder setOrder(Order order) {
			this.result.setOrder(order);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public CreateOrderResponse build() {
			return this.result;
		}

	}
	
	private Order	order	= null;
	
	/**
	 * 
	 */
	private CreateOrderResponse() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderResponse#setOrder(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.Order)
	 */
	@Override
	public void setOrder(Order order) {
		this.order = order;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderResponse#getOrder()
	 */
	@Override
	public Order getOrder() {
		
		return this.order;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.CreateOrderResponse#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.order || !this.order.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
