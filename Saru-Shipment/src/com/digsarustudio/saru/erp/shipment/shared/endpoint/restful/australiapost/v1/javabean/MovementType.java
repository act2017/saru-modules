/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MovementTypes;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.MovementType}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class MovementType implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.MovementType
									, IsSerializable {
	public static final Integer	MAX_LEN	= 8;
	
	/**
	 * 
	 * The object builder for {@link MovementType}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.MovementType.Builder {
		private MovementType result = null;

		private Builder() {
			this.result = new MovementType();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.MovementType.Builder#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MovementTypes)
		 */
		@Override
		public Builder setType(MovementTypes type) {
			this.result.setType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.MovementType.Builder#setType(java.lang.String)
		 */
		@Override
		public Builder setType(String type) {
			this.result.setType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public MovementType build() {
			return this.result;
		}

	}

	private String type	 = null;
	
	/**
	 * 
	 */
	private MovementType() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.MovementType#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MovementTypes)
	 */
	@Override
	public void setType(MovementTypes type) {
		String tp = (null == type) ? null : type.toString();

		this.setType(tp);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.MovementType#setType(java.lang.String)
	 */
	@Override
	public void setType(String type) {
		this.type = type;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.MovementType#getType()
	 */
	@Override
	public String getType() {
		
		return this.type;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.MovementType#getMovementType()
	 */
	@Override
	public MovementTypes getMovementType() {
		
		return MovementTypes.fromValue(this.type);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.MovementType#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.type || this.type.isEmpty()) {
			return false;
		}
		
		if(MAX_LEN < this.type.length()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
