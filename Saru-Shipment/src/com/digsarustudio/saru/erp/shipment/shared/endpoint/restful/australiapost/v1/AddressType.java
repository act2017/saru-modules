/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.AddressTypes;

/**
 * The type of address.<br>
 * The type might be passed from database.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface AddressType {
	/**
	 * 
	 * The builder for {@link AddressType}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<AddressType> {
		Builder setType(AddressTypes type);
		Builder setType(String type);
	}
	void setType(AddressTypes type);
	void setType(String type);
	
	String getType();
	AddressTypes getAddressType();
	
	Boolean validate();
}
