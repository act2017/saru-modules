/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalGst;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeaturePrice}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemFeaturePrice implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeaturePrice
										, IsSerializable {
	public static final String ATTR_CALCULATED_PRICE	= "calculated_price";
	public static final String ATTR_CALCULATED_GST		= "calculated_gst";

	/**
	 * 
	 * The object builder for {@link ItemFeaturePrice}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeaturePrice.Builder {
		private ItemFeaturePrice result = null;

		private Builder() {
			this.result = new ItemFeaturePrice();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeaturePrice.Builder#setCalculatedPrice(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost)
		 */
		@Override
		public Builder setCalculatedPrice(TotalCost price) {
			this.result.setCalculatedPrice(price);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeaturePrice.Builder#setCalculatedGst(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalGst)
		 */
		@Override
		public Builder setCalculatedGst(TotalGst gst) {
			this.result.setCalculatedGst(gst);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemFeaturePrice build() {
			return this.result;
		}

	}
	
	private TotalCost	calculated_price	= null;
	private TotalGst	calculated_gst		= null;
	
	/**
	 * 
	 */
	private ItemFeaturePrice() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeaturePrice#setCalculatedPrice(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost)
	 */
	@Override
	public void setCalculatedPrice(TotalCost price) {
		this.calculated_price = price;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeaturePrice#setCalculatedGst(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalGst)
	 */
	@Override
	public void setCalculatedGst(TotalGst gst) {
		this.calculated_gst = gst;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeaturePrice#getCalculatedPrice()
	 */
	@Override
	public TotalCost getCalculatedPrice() {
		
		return this.calculated_price;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeaturePrice#getCalculatedGst()
	 */
	@Override
	public TotalGst getCalculatedGst() {
		
		return this.calculated_gst;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeaturePrice#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.calculated_price || !this.calculated_price.validate()) {
			return false;
		}
		
		if(null == this.calculated_gst || !this.calculated_gst.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
