/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelFormatGroups;

/**
 * The label group to apply the print preference. Valid types are:<br>
 * 		Parcel Post<br>
 * 		Express Post<br>
 * 		StarTrack<br>
 * <br>
 * Note that for international shipments, the group is ignored (though it is still mandatory.)
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface LabelFormatGroup {
	/**
	 * 
	 * The builder for {@link LabelFormatGroup}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<LabelFormatGroup> {
		Builder setValue(String value);
		Builder setValue(LabelFormatGroups value);
	}
	
	void setValue(String value);
	void setValue(LabelFormatGroups value);
	
	String 				getValue();
	LabelFormatGroups	getLabelFormatGroups();
	
	Boolean validate();
}
