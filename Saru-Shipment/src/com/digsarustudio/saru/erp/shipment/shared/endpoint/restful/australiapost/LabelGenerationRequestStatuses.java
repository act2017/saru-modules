/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost;

import java.util.HashMap;
import java.util.Map;

/**
 * The status of the label generation request. Statuses returned will be:<br>
 * 	Pending: Labels are being generated<br>
 * 	Error: Label generation failed<br>
 * 	Available: Label generation has completed<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelGenerationRequestStatuses {
	public static final LabelGenerationRequestStatuses	Pending	= new LabelGenerationRequestStatuses("Pending", "Pending", "Labels are being generated");
	public static final LabelGenerationRequestStatuses	Error	= new LabelGenerationRequestStatuses("Error", "Error", "Label generation failed");
	public static final LabelGenerationRequestStatuses	Available	= new LabelGenerationRequestStatuses("Available", "Available", "Label generation has completed");

	
	
	private static Map<String, LabelGenerationRequestStatuses> VALUES = new HashMap<>();
		
	static {
		VALUES.put(Pending.getValue(), Pending);
		VALUES.put(Error.getValue(), Error);
		VALUES.put(Available.getValue(), Available);
	}	
	
	private String value 		= null;
	private String display 		= null;
	private String description	= null;
	
	private LabelGenerationRequestStatuses() {
		
	}
	
	private LabelGenerationRequestStatuses(String value, String display, String description) {
		this.value 			= value;
		this.display 		= display;
		this.description 	= description;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public String getDisplayString() {
		return this.display;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public static LabelGenerationRequestStatuses fromValue(String value) {
		if (null == value || !VALUES.containsKey(value)) {
			return null;
		}
		
		return VALUES.get(value);
	}
	
	public static LabelGenerationRequestStatuses getDefault() {
		return Pending;
	}
		

}
