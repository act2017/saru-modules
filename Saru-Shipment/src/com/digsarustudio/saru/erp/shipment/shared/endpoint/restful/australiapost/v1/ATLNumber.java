/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Authority to Leave (ATL) authorisation code. 
 * If this field is provided, it must begin with C and be followed by a 9 digit number.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface ATLNumber {
	/**
	 * 
	 * The builder for {@link ATLNumber}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ATLNumber> {
		Builder setNumber(String number);
	}
	
	void setNumber(String number);
	String getNumber();
	
	Boolean validate();
}
