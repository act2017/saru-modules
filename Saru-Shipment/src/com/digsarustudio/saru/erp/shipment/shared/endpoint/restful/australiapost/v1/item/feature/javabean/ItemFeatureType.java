/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureTypes;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureType}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemFeatureType implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureType
										, IsSerializable {
	/**
	 * 
	 * The object builder for {@link ItemFeatureType}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureType.Builder {
		private ItemFeatureType result = null;

		private Builder() {
			this.result = new ItemFeatureType();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureType.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureType.Builder#setValue(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureTypes)
		 */
		@Override
		public Builder setValue(ItemFeatureTypes type) {
			this.result.setValue(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemFeatureType build() {
			return this.result;
		}

	}
	
	private String value	= null;
	
	/**
	 * 
	 */
	private ItemFeatureType() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureType#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureType#setValue(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureTypes)
	 */
	@Override
	public void setValue(ItemFeatureTypes type) {
		if(null == type) {
			return;
		}
		
		this.setValue(type.getValue());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureType#getValue()
	 */
	@Override
	public String getValue() {

		return this.value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureType#getType()
	 */
	@Override
	public ItemFeatureTypes getType() {
		return ItemFeatureTypes.fromValue(this.value);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureType#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.value || this.value.isEmpty()) {
			return false;
		}
		
		return true;
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}

}
