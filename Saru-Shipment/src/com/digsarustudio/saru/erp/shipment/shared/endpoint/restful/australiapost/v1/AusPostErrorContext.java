/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import java.util.Map;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The collection of context for the error with the names and values.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface AusPostErrorContext {
	/**
	 * 
	 * The builder for {@link AusPostErrorContext}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<AusPostErrorContext> {
		Builder addPair(AusPostErrorContextPair pair);
		Builder addPair(String key, String value);
	}
	
	void addPair(AusPostErrorContextPair pair);
	void addPair(String key, String value);
	
	Map<String, AusPostErrorContextPair> getPairs();
	AusPostErrorContextPair getPair(String key);
	
	Boolean validate();
}
