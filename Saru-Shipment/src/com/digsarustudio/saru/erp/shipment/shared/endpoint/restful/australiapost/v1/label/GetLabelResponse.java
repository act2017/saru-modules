/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The response from Australia Post Get Label API.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface GetLabelResponse {
	/**
	 * 
	 * The builder for {@link GetLabelResponse}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<GetLabelResponse> {
		Builder addLabel(Label label);
		Builder setLabels(List<Label> labels);	
		Builder setLabels(LabelCollection labels);
	}
	
	void addLabel(Label label);
	void setLabels(List<Label> labels);	
	void setLabels(LabelCollection labels);
		
	Label getLabel(Integer index);	
	LabelCollection getLabels();
	
	Boolean validate();	
}
