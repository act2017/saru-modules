/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.GoodsClassificationTypes;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.GoodsClassificationType}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class GoodsClassificationType implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.GoodsClassificationType
											  , IsSerializable {
	/**
	 * 
	 * The object builder for {@link GoodsClassificationType}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.GoodsClassificationType.Builder {
		private GoodsClassificationType result = null;

		private Builder() {
			this.result = new GoodsClassificationType();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.GoodsClassificationType.Builder#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.GoodsClassificationTypes)
		 */
		@Override
		public Builder setType(GoodsClassificationTypes type) {
			this.result.setType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.GoodsClassificationType.Builder#setType(java.lang.String)
		 */
		@Override
		public Builder setType(String type) {
			this.result.setType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public GoodsClassificationType build() {
			return this.result;
		}

	}

	private String type	= null;
	
	/**
	 * 
	 */
	private GoodsClassificationType() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.GoodsClassificationType#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.GoodsClassificationTypes)
	 */
	@Override
	public void setType(GoodsClassificationTypes type) {
		this.type = type.getValue();

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.GoodsClassificationType#setType(java.lang.String)
	 */
	@Override
	public void setType(String type) {
		this.type = type;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.GoodsClassificationType#getType()
	 */
	@Override
	public String getType() {
		
		return this.type;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.GoodsClassificationType#getGoodsClassificationType()
	 */
	@Override
	public GoodsClassificationTypes getGoodsClassificationType() {
		
		return GoodsClassificationTypes.fromValue(this.type);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.GoodsClassificationType#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.type || this.type.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
