/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * An item_contents element describes part of the contents of the item.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface ItemContentCollection {
	/**
	 * 
	 * The builder for {@link ItemContentCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemContentCollection> {
		Builder addItemContent(ItemContent content);
		Builder setItemCotents(List<ItemContent> contents);
	}
	
	void addItemContent(ItemContent content);
	void setItemCotents(List<ItemContent> contents);
	
	ItemContent getItemContent(Integer index);
	List<ItemContent> getItemContents();
	
	Boolean validate();
}
