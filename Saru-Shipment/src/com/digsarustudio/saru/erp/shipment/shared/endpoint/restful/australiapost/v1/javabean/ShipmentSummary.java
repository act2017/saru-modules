/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Discount;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FreightCharge;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FuelSurcharge;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SecuritySurcharge;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalGst;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummary;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TransitCover;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ShipmentSummary implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary
									  , IsSerializable {
	public static final String	ATTR_TOTAL_COST			= "total_cost";
	public static final String	ATTR_TOTAL_GST			= "total_gst";
	public static final String	ATTR_STATUS				= "status";
	public static final String	ATTR_NUMBER_OF_ITEMS	= "number_of_items";
	public static final String	ATTR_TRACKING_SUMMARY	= "tracking_summary";
	public static final String	ATTR_FREIGHT_CHARGE		= "freight_charge";
	public static final String	ATTR_DISCOUNT			= "discount";
	public static final String	ATTR_TRANSIT_COVER		= "transit_cover";
	public static final String	ATTR_SECURITY_SURCHARGE	= "security_surcharge";
	public static final String	ATTR_FUEL_SURCHARGE		= "fuel_surcharge";
	
	/**
	 * 
	 * The object builder for {@link ShipmentSummary}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary.Builder {
		private ShipmentSummary result = null;

		private Builder() {
			this.result = new ShipmentSummary();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary.Builder#setTotalCost(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost)
		 */
		@Override
		public Builder setTotalCost(TotalCost cost) {
			this.result.setTotalCost(cost);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary.Builder#setTotalGST(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalGst)
		 */
		@Override
		public Builder setTotalGST(TotalGst gst) {
			this.result.setTotalGST(gst);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary.Builder#setStatus(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus)
		 */
		@Override
		public Builder setStatus(OrderShipmentItemStatus status) {
			this.result.setStatus(status);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary.Builder#setNumberOfItems(java.lang.Integer)
		 */
		@Override
		public Builder setNumberOfItems(Integer number) {
			this.result.setNumberOfItems(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary.Builder#setTrackingSummaries(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummaryCollection)
		 */
		@Override
		public Builder setTrackingSummaries(TrackingSummary summaries) {
			this.result.setTrackingSummaries(summaries);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary.Builder#setFreightCharge(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FreightCharge)
		 */
		@Override
		public Builder setFreightCharge(FreightCharge charge) {
			this.result.setFreightCharge(charge);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary.Builder#setDiscount(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Discount)
		 */
		@Override
		public Builder setDiscount(Discount discount) {
			this.result.setDiscount(discount);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary.Builder#setTransitCover(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TransitCover)
		 */
		@Override
		public Builder setTransitCover(TransitCover cover) {
			this.result.setTransitCover(cover);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary.Builder#setFuelSurcharge(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FuelSurcharge)
		 */
		@Override
		public Builder setFuelSurcharge(FuelSurcharge charge) {
			this.result.setFuelSurcharge(charge);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary.Builder#setSecuritySurcharge(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SecuritySurcharge)
		 */
		@Override
		public Builder setSecuritySurcharge(SecuritySurcharge charge) {
			this.result.setSecuritySurcharge(charge);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ShipmentSummary build() {
			return this.result;
		}

	}
	
	private TotalCost					total_cost			= null;
	private TotalGst					total_gst			= null;
	private OrderShipmentItemStatus		status				= null;
	private Integer						number_of_items		= 0;
	private TrackingSummary	tracking_summary	= null;
	private	FreightCharge				freight_charge		= null;
	private	Discount					discount			= null;
	private TransitCover				transit_cover		= null;
	private SecuritySurcharge			security_surcharge	= null;
	private FuelSurcharge				fuel_surcharge		= null;
	
	/**
	 * 
	 */
	private ShipmentSummary() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#getTotalCost()
	 */
	@Override
	public TotalCost getTotalCost() {

		return this.total_cost;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#getTotalGST()
	 */
	@Override
	public TotalGst getTotalGST() {

		return this.total_gst;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#getStatus()
	 */
	@Override
	public OrderShipmentItemStatus getStatus() {

		return this.status;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#getNumberOfItems()
	 */
	@Override
	public Integer getNumberOfItems() {

		return this.number_of_items;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#getTrackingSummary()
	 */
	@Override
	public TrackingSummary getTrackingSummaries() {

		return this.tracking_summary;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#getFreightCharge()
	 */
	@Override
	public FreightCharge getFreightCharge() {

		return this.freight_charge;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#getDiscount()
	 */
	@Override
	public Discount getDiscount() {

		return this.discount;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#getTransitCover()
	 */
	@Override
	public TransitCover getTransitCover() {

		return this.transit_cover;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#getFuelSurcharge()
	 */
	@Override
	public FuelSurcharge getFuelSurcharge() {

		return this.fuel_surcharge;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#setTotalCost(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost)
	 */
	@Override
	public void setTotalCost(TotalCost cost) {
		this.total_cost = cost;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#setTotalGST(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalGst)
	 */
	@Override
	public void setTotalGST(TotalGst gst) {
		this.total_gst = gst;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#setStatus(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus)
	 */
	@Override
	public void setStatus(OrderShipmentItemStatus status) {
		this.status = status;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#setNumberOfItems(java.lang.Integer)
	 */
	@Override
	public void setNumberOfItems(Integer number) {
		this.number_of_items = number;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#setTrackingSummaries(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingSummaryCollection)
	 */
	@Override
	public void setTrackingSummaries(TrackingSummary summaries) {
		this.tracking_summary = summaries;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#setFreightCharge(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FreightCharge)
	 */
	@Override
	public void setFreightCharge(FreightCharge charge) {
		this.freight_charge = charge;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#setDiscount(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Discount)
	 */
	@Override
	public void setDiscount(Discount discount) {
		this.discount = discount;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#setTransitCover(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TransitCover)
	 */
	@Override
	public void setTransitCover(TransitCover cover) {
		this.transit_cover = cover;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#getSecuritySurcharge()
	 */
	@Override
	public SecuritySurcharge getSecuritySurcharge() {
		return this.security_surcharge;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#setSecuritySurcharge(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SecuritySurcharge)
	 */
	@Override
	public void setSecuritySurcharge(SecuritySurcharge charge) {
		this.security_surcharge = charge;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#setFuelSurcharge(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FuelSurcharge)
	 */
	@Override
	public void setFuelSurcharge(FuelSurcharge charge) {
		this.fuel_surcharge = charge;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary#validate()
	 */
	@Override
	public Boolean validate() {
		if(null != this.total_cost && !this.total_cost.validate()) {
			return false;
		}

		if(null != this.total_gst && !this.total_gst.validate()) {
			return false;
		}

		if(null != this.status && !this.status.validate()) {
			return false;
		}

		if(null == this.number_of_items ) {
			return false;
		}

		if(null != this.tracking_summary && !this.tracking_summary.validate()) {
			return false;
		}

		if(null != this.freight_charge && !this.freight_charge.validate()) {
			return false;
		}

		if(null != this.discount && !this.discount.validate()) {
			return false;
		}

		if(null != this.transit_cover && !this.transit_cover.validate()) {
			return false;
		}

		if(null != this.security_surcharge && !this.security_surcharge.validate()) {
			return false;
		}

		if(null != this.fuel_surcharge && !this.fuel_surcharge.validate()) {
			return false;
		}		

		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
