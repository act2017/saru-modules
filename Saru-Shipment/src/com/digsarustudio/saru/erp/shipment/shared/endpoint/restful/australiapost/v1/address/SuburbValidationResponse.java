/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb;

/**
 * The sub-type represents the result of Australia Post Suburb Validation API according to the name of 
 * suburb, state, and postcode.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface SuburbValidationResponse {
	/**
	 * 
	 * The builder for {@link SuburbValidationResponse}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<SuburbValidationResponse> {
		Builder setFound();
		Builder setNotFound();
		Builder addSuburb(String suburb);
		Builder addSuburb(Suburb suburb);
		Builder setSuburbs(List<String> suburbs);
	}
	
	void setFound();
	void setNotFound();
	void addSuburb(String suburb);
	void addSuburb(Suburb suburb);
	void setSuburbs(List<String> suburbs);
	
	Suburb			getSuburb(Integer index);
	List<Suburb>	getSuburbs();
	List<String>	getSuburbsString();
	Boolean			isFound();
	
	Boolean validate();
}
