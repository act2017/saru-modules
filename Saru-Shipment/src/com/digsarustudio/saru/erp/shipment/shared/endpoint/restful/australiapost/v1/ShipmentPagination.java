/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Information about the pagination of the shipments available. This is provided when you use the offset and number_of_shipments parameters in the request.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/get-shipments">Get Shipments</a>
 *
 */
public interface ShipmentPagination {
	/**
	 * 
	 * The builder for {@link ShipmentPagination}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ShipmentPagination> {
		/**
		 * The total number of shipment records that can be returned.<br>
		 * 
		 * @param number The total number of shipment records that can be returned.
		 */
		Builder setTotalNumberOfRecords(Integer number);
		
		/**
		 * The number of shipment records per page, that you requested.<br>
		 * 
		 * @param number The number of shipment records per page, that you requested.
		 */
		Builder setNumberOfRecordsPerPage(Integer number);
		
		/**
		 * The current page number you requested.<br>
		 * 
		 * @param number The current page number you requested.
		 */
		Builder setCurrentPageNumber(Integer number);
	}
	
	/**
	 * The total number of shipment records that can be returned.<br>
	 * 
	 * @param number The total number of shipment records that can be returned.
	 */
	void setTotalNumberOfRecords(Integer number);
	Integer getTotalNumberOfRecords();
	
	/**
	 * The number of shipment records per page, that you requested.<br>
	 * 
	 * @param number The number of shipment records per page, that you requested.
	 */
	void setNumberOfRecordsPerPage(Integer number);
	Integer getNumberOfRecordsPerPage();
	
	/**
	 * The current page number you requested.<br>
	 * 
	 * @param number The current page number you requested.
	 */
	void setCurrentPageNumber(Integer number);
	Integer getCurrentPageNumber();
		
	Boolean validate();
}
