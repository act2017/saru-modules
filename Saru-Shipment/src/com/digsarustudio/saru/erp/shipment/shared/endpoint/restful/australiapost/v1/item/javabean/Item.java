/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.GoodsClassificationTypes;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ATLNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.NonDeliveryAction;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackagingType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.CertificateNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Comment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.CubicVolume;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.DescriptionOfOther;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ExportDeclarationNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.GoodsClassificationType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ImportReferenceNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.InvoiceNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContentCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemDescription;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemHeight;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemLength;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemSummary;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWeight;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWidth;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.LicenceNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ProductId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ReasonForReturn;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeature;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.TrackingDetails;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class Item implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item
							,IsSerializable {
	
	/**
	 * [Optional] .<br>
	 * 
	 * <br>
	 * Type: String
	 */
	public static final String	ATTR_ITEM_REFERENCE				= "item_reference";
	
	/**
	 * [Mandatory] .<br>
	 * Type: String
	 */
	public static final String	ATTR_PRODUCT_ID					= "product_id";
	
	/**
	 * [Optional] .<br>
	 * 
	 * <br>
	 * Type: String
	 */
	public static final String	ATTR_ITEM_DESCRIPTION			= "item_description";
	
	/**
	 * [Mandatory] If volumetric pricing applies.<br>
	 * [Optional]
	 * Type: Decimal
	 */
	public static final String	ATTR_LENGTH						= "length";
	
	/**
	 * [Mandatory] If volumetric pricing applies.<br>
	 * [Optional]
	 * Type: Decimal
	 */
	public static final String	ATTR_WIDTH						= "width";
	
	/**
	 * [Mandatory] If volumetric pricing applies.<br>
	 * [Optional]
	 * Type: Decimal
	 */
	public static final String	ATTR_HEIGHT						= "height";
	
	/**
	 * [Mandatory] If volumetric pricing applies.<br>
	 * [Optional]
	 * Type: Decimal
	 */
	public static final String	ATTR_CUBIC_VOLUME				= "cubic_volume";
	
	/**
	 * [Mandatory] .<br>
	 * Type: Decimal
	 */
	public static final String	ATTR_WEIGHT						= "weight";
	
	/**
	 * [Optional] .<br>
	 * Defaults to false.<br>
	 * 
	 * <br>
	 * Type: Boolean
	 */
	public static final String	ATTR_CONTAIN_DANGEROUS_GOODS	= "contains_dangerous_goods";
	
	/**
	 * [Optional] .<br>
	 * Whether the item can be left in a safe place on delivery without receiving a signature. 
	 * This is optional for domestic shipments (and defaults to true), but must not be specified for international shipments. 
	 * This field works with the safe_drop_enabled field. Please review the fact sheet for full details.
	 * <br>
	 * Type: Boolean
	 */
	public static final String	ATTR_AUTHORITY_TO_LEAVE			= "authority_to_leave";
	
	/**
	 * [Optional] .<br>
	 * Whether your customer can request that the item be left in a safe place at the delivery address if they are not 
	 * home to sign for their delivery. Your customer can request this while the item is on its way. 
	 * This field is optional for domestic shipments (and defaults to true), but must not be specified for 
	 * international shipments. This field works with the authority_to_leave field. 
	 * If the authority_to_leave field is set to true, the safe_drop_enabled field is ignored. 
	 * Please review the fact sheet for full details.
	 * <br>
	 * Type: Boolean
	 */
	public static final String	ATTR_SAFE_DROP_ENABLED			= "safe_drop_enabled";
	
	/**
	 * [Optional] .<br>
	 * 
	 * <br>
	 * Type: String
	 */
	public static final String	ATTR_REASON_FOR_RETURN			= "reason_for_return";
	
	/**
	 * [Optional] .<br>
	 * Defaults to false.
	 * <br>
	 * Type: Boolean
	 */
	public static final String	ATTR_ALLOW_PARTIAL_DELIVERY		= "allow_partial_delivery";
	
	/**
	 * [Mandatory] for StarTrack.<br>
	 * [Optional] for others.<br>
	 * 
	 * <br>
	 * Type: String
	 */
	public static final String	ATTR_PACKAGING_TYPE				= "packaging_type";
	
	/**
	 * [Optional] .<br>
	 * 
	 * <br>
	 * Type: String
	 */
	public static final String	ATTR_ATL_NUMBER					= "atl_number";
	
	/**
	 * [Optional] .<br>
	 * 
	 * <br>
	 * Type: Object
	 */
	public static final String	ATTR_FEATURES					= "features";
	
	/**
	 * [Optional] .<br>
	 * 
	 * <br>
	 * Type: Object
	 */
	public static final String	ATTR_TRACKING_DETAILS			= "tracking_details";
	
	/**
	 * [Mandatory] for International.<br>
	 * Type: Boolean
	 */
	public static final String	ATTR_COMMERCIAL_VALUE			= "commercial_value";
	
	/**
	 * [Optional]<br>
	 * Type: String
	 */
	public static final String	ATTR_EXPORT_DECLARATION_NUMBER	= "export_declaration_number";/**
	
	 * [Optional]<br>
	 * Type: String
	 */
	public static final String	ATTR_IMPORT_REFERENCE_NUMBER	= "import_reference_number";
	
	/**
	 * [Mandatory] for international<br>
	 * Type: String
	 */
	public static final String	ATTR_CLASSIFICATION_TYPE		= "classification_type";
	
	/**
	 * [Mandatory] if the classification_type is {@link GoodsClassificationTypes#Other}.<br>
	 * Type: String
	 */
	public static final String	ATTR_DESCRIPTION_OF_OTHER		= "description_of_other";
	
	/**
	 * [Optional] .<br>
	 * Your desired action if the delivery cannot be completed. 
	 * You can specify "RETURN" or "ABANDONED". The default if not specified is to return the item by the most economical route or by air.
	 * <br>
	 * Type: String
	 */
	public static final String	ATTR_NON_DELIVERY_ACTION		= "non_delivery_action";
	
	/**
	 * [Optional] .<br>
	 * If you are supplying certificates with your customs forms, you can include the certificate numbers in this field.
	 * <br>
	 * Type: String
	 */
	public static final String	ATTR_CERTIFICATE_NUMBER			= "certificate_number";
	
	/**
	 * [Optional] .<br>
	 * Use this field to include an invoice number on the label.<br>
	 * <br>
	 * Type: String
	 */
	public static final	String	ATTR_INVOICE_NUMBER				= "invoice_number";
	
	/**
	 * [Optional] .<br>
	 * If you are supplying licence information with your customs forms, you can include the licence numbers in this field.
	 * <br>
	 * Type: String
	 */
	public static final	String	ATTR_LICENCE_NUMBER				= "licence_number";
	
	/**
	 * [Optional] .<br>
	 * Use this field to include any additional comments on the label.<br>
	 * <br>
	 * Type: String
	 */
	public static final String	ATTR_COMMENT					= "comments";
	
	/**
	 * [Mandatory] for international.<br>
	 * Type: Array
	 */
	public static final String	ATTR_ITEM_CONTENTS				= "item_contents";
	
	//======== Response fields (The response field doens't need to validate) ============
	public static final String	ATTR_ITEM_ID					= "item_id";
	public static final String	ATTR_ITEM_SUMMARY				= "item_summary";
	public static final String	ATTR_LABEL						= "label";
	
	/**
	 * 
	 * The object builder for {@link Item}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder {
		private Item result = null;

		private Builder() {
			this.result = new Item();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setReference(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemReference)
		 */
		@Override
		public Builder setReference(ItemReference reference) {
			this.result.setReference(reference);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setProductId(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ProductId)
		 */
		@Override
		public Builder setProductId(ProductId id) {
			this.result.setProductId(id);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setDescription(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemDescription)
		 */
		@Override
		public Builder setDescription(ItemDescription desc) {
			this.result.setDescription(desc);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setLength(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemLength)
		 */
		@Override
		public Builder setLength(ItemLength length) {
			this.result.setLength(length);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setWidth(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWidth)
		 */
		@Override
		public Builder setWidth(ItemWidth width) {
			this.result.setWidth(width);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setHeight(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemHeight)
		 */
		@Override
		public Builder setHeight(ItemHeight height) {
			this.result.setHeight(height);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setCubicVolume(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.CubicVolume)
		 */
		@Override
		public Builder setCubicVolume(CubicVolume cubicVolume) {
			this.result.setCubicVolume(cubicVolume);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setWeight(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWeight)
		 */
		@Override
		public Builder setWeight(ItemWeight weight) {
			this.result.setWeight(weight);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setContainsDangerousGoods(java.lang.Boolean)
		 */
		@Override
		public Builder setContainsDangerousGoods(Boolean contained) {
			this.result.setContainsDangerousGoods(contained);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setAuthorityToLeave(java.lang.Boolean)
		 */
		@Override
		public Builder setAuthorityToLeave(Boolean isATL) {
			this.result.setAuthorityToLeave(isATL);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setSafeDropEnabled(java.lang.Boolean)
		 */
		@Override
		public Builder setSafeDropEnabled(Boolean enabled) {
			this.result.setSafeDropEnabled(enabled);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setReasonForReturn(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ReasonForReturn)
		 */
		@Override
		public Builder setReasonForReturn(ReasonForReturn reason) {
			this.result.setReasonForReturn(reason);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setAllowPartialDelivery(java.lang.Boolean)
		 */
		@Override
		public Builder setAllowPartialDelivery(Boolean isPartialDelivery) {
			this.result.setAllowPartialDelivery(isPartialDelivery);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setPackagingType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackagingType)
		 */
		@Override
		public Builder setPackagingType(PackagingType type) {
			this.result.setPackagingType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setATLNumber(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ATLNumber)
		 */
		@Override
		public Builder setATLNumber(ATLNumber number) {
			this.result.setATLNumber(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setItemFeatures(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeatureCollection)
		 */
		@Override
		public Builder setItemFeatures(ItemFeatureCollection features) {
			this.result.setItemFeatures(features);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#addItemFeature(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeature)
		 */
		@Override
		public Builder addItemFeature(ItemFeature feature) {
			this.result.addItemFeature(feature);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setTrackingDetails(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.TrackingDetails)
		 */
		@Override
		public Builder setTrackingDetails(TrackingDetails details) {
			this.result.setTrackingDetails(details);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setHasCommercialValue(java.lang.Boolean)
		 */
		@Override
		public Builder setHasCommercialValue(Boolean hasCommercialValue) {
			this.result.setHasCommercialValue(hasCommercialValue);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setExportDeclarationNumber(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ExportDeclarationNumber)
		 */
		@Override
		public Builder setExportDeclarationNumber(ExportDeclarationNumber number) {
			this.result.setExportDeclarationNumber(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setImportReferenceNumber(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ImportReferenceNumber)
		 */
		@Override
		public Builder setImportReferenceNumber(ImportReferenceNumber number) {
			this.result.setImportReferenceNumber(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setClassificationType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.GoodsClassificationType)
		 */
		@Override
		public Builder setClassificationType(GoodsClassificationType type) {
			this.result.setClassificationType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setDescriptionOfOther(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.DescriptionOfOther)
		 */
		@Override
		public Builder setDescriptionOfOther(DescriptionOfOther desc) {
			this.result.setDescriptionOfOther(desc);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setNonDeliveryAction(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.NonDeliveryAction)
		 */
		@Override
		public Builder setNonDeliveryAction(NonDeliveryAction action) {
			this.result.setNonDeliveryAction(action);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setCertificateNumber(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.CertificateNumber)
		 */
		@Override
		public Builder setCertificateNumber(CertificateNumber number) {
			this.result.setCertificateNumber(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setLicenceNumber(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.LicenceNumber)
		 */
		@Override
		public Builder setLicenceNumber(LicenceNumber number) {
			this.result.setLicenceNumber(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setInvoiceNumber(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.InvoiceNumber)
		 */
		@Override
		public Builder setInvoiceNumber(InvoiceNumber number) {
			this.result.setInvoiceNumber(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setComment(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Comment)
		 */
		@Override
		public Builder setComment(Comment comment) {
			this.result.setComment(comment);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setItemContents(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContentCollection)
		 */
		@Override
		public Builder setItemContents(ItemContentCollection itemContents) {
			this.result.setItemContents(itemContents);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#addItemContent(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent)
		 */
		@Override
		public Builder addItemContent(ItemContent content) {
			this.result.addItemContent(content);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setItemId(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemId)
		 */
		@Override
		public Builder setItemId(ItemId id) {
			this.result.setItemId(id);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setItemSummary(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemSummary)
		 */
		@Override
		public Builder setItemSummary(ItemSummary summary) {
			this.result.setItemSummary(summary);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item.Builder#setLabel(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label)
		 */
		@Override
		public Builder setLabel(Label label) {
			this.result.setLabel(label);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Item build() {
			return this.result;
		}

	}
	
	private ItemReference			item_reference				= null;
	private ProductId				product_id					= null;
	private ItemDescription			item_description			= null;
	private ItemLength				length						= null;
	private ItemWidth				width						= null;
	private ItemHeight				height						= null;
	private ItemWeight				weight						= null;
	private CubicVolume				cubic_volume				= null;
	private Boolean					contains_dangerous_goods	= null;
	private Boolean					authority_to_leave			= null;
	private Boolean					safe_drop_enabled			= null;
	private ReasonForReturn			reason_for_return			= null;
	private Boolean					allow_partial_delivery		= null;
	private PackagingType			packaging_type				= null;
	private ATLNumber				atl_number					= null;
	private ItemFeatureCollection	features					= null;
	private TrackingDetails			tracking_details			= null;
	private Boolean					commercial_value			= null;
	private ExportDeclarationNumber	export_declaration_number	= null;
	private ImportReferenceNumber	import_reference_number		= null;
	private GoodsClassificationType	classification_type			= null;
	private DescriptionOfOther		description_of_other		= null;
	private NonDeliveryAction		non_delivery_action			= null;
	private CertificateNumber		certificate_number			= null;
	private InvoiceNumber			invoice_number				= null;
	private LicenceNumber			licence_number				= null;
	private	Comment					comments					= null;
	private ItemContentCollection	item_contents				= null;
	
	//====== Response Fields =========
	private ItemId					item_id						= null;
	private	ItemSummary				item_summary				= null;
	private Label					label						= null;
	
	/**
	 * 
	 */
	private Item() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setReference(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemReference)
	 */
	@Override
	public void setReference(ItemReference reference) {
		this.item_reference	 = reference;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setProductId(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ProductId)
	 */
	@Override
	public void setProductId(ProductId id) {
		this.product_id = id;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setDescription(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemDescription)
	 */
	@Override
	public void setDescription(ItemDescription desc) {
		this.item_description	= desc;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setLength(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemLength)
	 */
	@Override
	public void setLength(ItemLength length) {
		this.length = length;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setWidth(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWidth)
	 */
	@Override
	public void setWidth(ItemWidth width) {
		this.width = width;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setHeight(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemHeight)
	 */
	@Override
	public void setHeight(ItemHeight height) {
		this.height = height;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setCubicVolume(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.CubicVolume)
	 */
	@Override
	public void setCubicVolume(CubicVolume cubicVolume) {
		this.cubic_volume = cubicVolume;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setWeight(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWeight)
	 */
	@Override
	public void setWeight(ItemWeight weight) {
		this.weight = weight;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setContainsDangerousGoods(java.lang.Boolean)
	 */
	@Override
	public void setContainsDangerousGoods(Boolean contained) {
		this.contains_dangerous_goods = contained;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setAuthorityToLeave(java.lang.Boolean)
	 */
	@Override
	public void setAuthorityToLeave(Boolean isATL) {
		this.authority_to_leave = isATL;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setSafeDropEnabled(java.lang.Boolean)
	 */
	@Override
	public void setSafeDropEnabled(Boolean enabled) {
		this.safe_drop_enabled = enabled;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setReasonForReturn(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ReasonForReturn)
	 */
	@Override
	public void setReasonForReturn(ReasonForReturn reason) {
		this.reason_for_return	= reason;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setAllowPartialDelivery(java.lang.Boolean)
	 */
	@Override
	public void setAllowPartialDelivery(Boolean isPartialDelivery) {
		this.allow_partial_delivery = isPartialDelivery;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setPackagingType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackagingType)
	 */
	@Override
	public void setPackagingType(PackagingType type) {
		this.packaging_type = type;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setATLNumber(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ATLNumber)
	 */
	@Override
	public void setATLNumber(ATLNumber number) {
		this.atl_number = number;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setItemFeatures(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeatureCollection)
	 */
	@Override
	public void setItemFeatures(ItemFeatureCollection features) {
		this.features = features;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#addItemFeature(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeature)
	 */
	@Override
	public void addItemFeature(ItemFeature feature) {
		if(null == this.features) {
			this.features = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureCollection.builder().build();
		}

		this.features.addFeature(feature);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setTrackingDetails(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TrackingDetails)
	 */
	@Override
	public void setTrackingDetails(TrackingDetails details) {
		this.tracking_details = details;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setHasCommercialValue(java.lang.Boolean)
	 */
	@Override
	public void setHasCommercialValue(Boolean hasCommercialValue) {
		this.commercial_value = hasCommercialValue;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setExportDeclarationNumber(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ExportDeclarationNumber)
	 */
	@Override
	public void setExportDeclarationNumber(ExportDeclarationNumber number) {
		this.export_declaration_number = number;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setImportReferenceNumber(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ImportReferenceNumber)
	 */
	@Override
	public void setImportReferenceNumber(ImportReferenceNumber number) {
		this.import_reference_number = number;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setClassificationType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsClassificationType)
	 */
	@Override
	public void setClassificationType(GoodsClassificationType type) {
		this.classification_type = type;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setDescriptionOfOther(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DescriptionOfOther)
	 */
	@Override
	public void setDescriptionOfOther(DescriptionOfOther desc) {
		this.description_of_other = desc;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setNonDeliveryAction(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.NonDeliveryAction)
	 */
	@Override
	public void setNonDeliveryAction(NonDeliveryAction action) {
		this.non_delivery_action = action;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setCertificateNumber(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.CertificateNumber)
	 */
	@Override
	public void setCertificateNumber(CertificateNumber number) {
		this.certificate_number = number;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setLicenceNumber(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.LicenseNumber)
	 */
	@Override
	public void setLicenceNumber(LicenceNumber number) {
		this.licence_number = number;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setInvoiceNumber(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.InvoiceNumber)
	 */
	@Override
	public void setInvoiceNumber(InvoiceNumber number) {
		this.invoice_number = number;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setComment(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Comment)
	 */
	@Override
	public void setComment(Comment comment) {
		this.comments = comment;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setItemContents(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContentCollection)
	 */
	@Override
	public void setItemContents(ItemContentCollection itemContents) {
		this.item_contents = itemContents;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#addItemContent(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent)
	 */
	@Override
	public void addItemContent(ItemContent content) {
		if(null == this.item_contents) {
			this.item_contents = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemContentCollection.builder().build();
		}

		this.item_contents.addItemContent(content);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#hasComercialValue()
	 */
	@Override
	public Boolean hasComercialValue() {
		return this.commercial_value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getItemId()
	 */
	@Override
	public ItemId getItemId() {

		return this.item_id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getItemReference()
	 */
	@Override
	public ItemReference getItemReference() {

		return this.item_reference;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getProductId()
	 */
	@Override
	public ProductId getProductId() {

		return this.product_id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getTrackingDetails()
	 */
	@Override
	public TrackingDetails getTrackingDetails() {

		return this.tracking_details;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getItemSummary()
	 */
	@Override
	public ItemSummary getItemSummary() {

		return this.item_summary;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getDescription()
	 */
	@Override
	public ItemDescription getDescription() {

		return this.item_description;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getHeight()
	 */
	@Override
	public ItemHeight getHeight() {

		return this.height;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getLenth()
	 */
	@Override
	public ItemLength getLenth() {

		return this.length;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getWidth()
	 */
	@Override
	public ItemWidth getWidth() {

		return this.width;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getCubicVolume()
	 */
	@Override
	public CubicVolume getCubicVolume() {

		return this.cubic_volume;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getWeight()
	 */
	@Override
	public ItemWeight getWeight() {

		return this.weight;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getPackagingType()
	 */
	@Override
	public PackagingType getPackagingType() {

		return this.packaging_type;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getATLNumber()
	 */
	@Override
	public ATLNumber getATLNumber() {

		return this.atl_number;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#isContainingDangerousGoods()
	 */
	@Override
	public Boolean isContainingDangerousGoods() {

		return this.contains_dangerous_goods;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#isAuthorityToLeave()
	 */
	@Override
	public Boolean isAuthorityToLeave() {

		return this.authority_to_leave;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#isSafeDropEnabled()
	 */
	@Override
	public Boolean isSafeDropEnabled() {

		return this.safe_drop_enabled;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#isAllowingPartialDelivery()
	 */
	@Override
	public Boolean isAllowingPartialDelivery() {

		return this.allow_partial_delivery;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getFeatures()
	 */
	@Override
	public ItemFeatureCollection getFeatures() {

		return this.features;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setItemId(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemId)
	 */
	@Override
	public void setItemId(ItemId id) {
		this.item_id = id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setItemSummary(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemSummary)
	 */
	@Override
	public void setItemSummary(ItemSummary summary) {
		this.item_summary = summary;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getReasonForReturn()
	 */
	@Override
	public ReasonForReturn getReasonForReturn() {

		return this.reason_for_return;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getExportDeclarationNumber()
	 */
	@Override
	public ExportDeclarationNumber getExportDeclarationNumber() {

		return this.export_declaration_number;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getImportReferenceNumber()
	 */
	@Override
	public ImportReferenceNumber getImportReferenceNumber() {

		return this.import_reference_number;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getClassificationType()
	 */
	@Override
	public GoodsClassificationType getClassificationType() {

		return this.classification_type;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getDescriptionOfOther()
	 */
	@Override
	public DescriptionOfOther getDescriptionOfOther() {

		return this.description_of_other;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getNonDeliveryAction()
	 */
	@Override
	public NonDeliveryAction getNonDeliveryAction() {

		return this.non_delivery_action;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getCertificateNumber()
	 */
	@Override
	public CertificateNumber getCertificateNumber() {

		return this.certificate_number;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getLicenceNumber()
	 */
	@Override
	public LicenceNumber getLicenceNumber() {

		return this.licence_number;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getInvoiceNumber()
	 */
	@Override
	public InvoiceNumber getInvoiceNumber() {

		return this.invoice_number;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getComment()
	 */
	@Override
	public Comment getComment() {

		return this.comments;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getItemContents()
	 */
	@Override
	public ItemContentCollection getItemContents() {

		return this.item_contents;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getItemContent(java.lang.Integer)
	 */
	@Override
	public ItemContent getItemContent(Integer index) {
		if(null == this.item_contents) {
			return null;
		}
		
		return this.item_contents.getItemContent(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#setLabel(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label)
	 */
	@Override
	public void setLabel(Label label) {
		this.label = label;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#getLabel()
	 */
	@Override
	public Label getLabel() {
		return this.label;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item#validate()
	 */
	@Override
	public Boolean validate() {
		if(null != this.item_reference && !this.item_reference.validate()) {
			return false;
		}
		
		if(null == this.product_id || !this.product_id.validate()) {
			return false;
		}
		
		if(null != this.item_description && !this.item_description.validate()) {
			return false;
		}
		
		if(null != this.length && !this.length.validate()) {
			return false;
		}
		
		if(null != this.width && !this.width.validate()) {
			return false;
		}
		
		if(null != this.height && !this.height.validate()) {
			return false;
		}
		
		if(null != this.cubic_volume && !this.cubic_volume.validate()) {
			return false;
		}
		
		if(null == this.weight || !this.weight.validate()) {
			return false;
		}
		
		//contains_dangerous_goods is optional Boolean
		//authority_to_leave is optional Boolean
		//safe_drop_enabled is optional Boolean
		
		if(null != this.reason_for_return && !this.reason_for_return.validate()) {
			return false;
		}
		
		//allow_partial_delivery is optional Boolean
		
		if(null != this.packaging_type && !this.packaging_type.validate()) {
			return false;
		}
		
		if(null != this.atl_number && !this.atl_number.validate()) {
			return false;
		}
		
		if(null != this.features && !this.features.validate()) {
			return false;
		}
		
		if(null != this.tracking_details && !this.tracking_details.validate()) {
			return false;
		}
		
		//commercial_value is optional Boolean
		
		if(null != this.export_declaration_number && !this.export_declaration_number.validate()) {
			return false;
		}
		
		if(null != this.import_reference_number && !this.import_reference_number.validate()) {
			return false;
		}
		
		if(null != this.classification_type && !this.classification_type.validate()) {
			return false;
		}if( null != this.classification_type && GoodsClassificationTypes.Other == this.classification_type.getGoodsClassificationType()) {
			if(null == this.description_of_other || !this.description_of_other.validate()) {
				return false;
			}
		}else {
			if(null != this.description_of_other && !this.description_of_other.validate()) {
				return false;
			}
		}
		
		if(null != this.non_delivery_action && !this.non_delivery_action.validate()) {
			return false;
		}
		
		if(null != this.certificate_number && !this.certificate_number.validate()) {
			return false;
		}
		
		if(null != this.licence_number && !this.licence_number.validate()) {
			return false;
		}
		
		if(null != this.invoice_number && !this.invoice_number.validate()) {
			return false;
		}
		
		if(null != this.comments && !this.comments.validate()) {
			return false;
		}
		
		if(null != this.item_contents && !this.item_contents.validate()) {
			return false;
		}
		
		if(null != this.label && !this.label.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
