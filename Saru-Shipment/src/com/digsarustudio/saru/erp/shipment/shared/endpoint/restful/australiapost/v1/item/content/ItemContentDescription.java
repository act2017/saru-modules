/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Description of the contents. This description is validated against a defined list of keywords, 
 * to determine whether the item can be carried by Australia Post and/or the item is prohibited in the destination country.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface ItemContentDescription {
	/**
	 * 
	 * The builder for {@link ItemContentDescription}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemContentDescription> {
		Builder setDescription(String desc);
	}
	
	void setDescription(String desc);
	String getDescription();
	
	Boolean validate();
}
