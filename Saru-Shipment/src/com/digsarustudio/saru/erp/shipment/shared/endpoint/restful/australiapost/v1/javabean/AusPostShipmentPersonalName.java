/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.javabean.PersonalName;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The name of sender used by Australia Post.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AusPostShipmentPersonalName implements com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName
												 , IsSerializable{
	public static final Integer	MAX_LEN	= 40;
	
	/**
	 * 
	 * The object builder for {@link PersonalName}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName.Builder {
		private AusPostShipmentPersonalName result = null;

		private Builder() {
			this.result = new AusPostShipmentPersonalName();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName.Builder#setSurname(java.lang.String)
		 */
		@Override
		public Builder setSurname(String name) {
			this.result.setSurname(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName.Builder#setMiddleName(java.lang.String)
		 */
		@Override
		public Builder setMiddleName(String name) {
			this.result.setMiddleName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName.Builder#setGivenName(java.lang.String)
		 */
		@Override
		public Builder setGivenName(String name) {
			this.result.setGivenName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName.Builder#setFullName(java.lang.String)
		 */
		@Override
		public Builder setFullName(String name) {
			this.result.setFullName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.Identity.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AusPostShipmentPersonalName build() {
			return this.result;
		}

	}
	
	private PersonalName	name	= null;

	/**
	 * 
	 */
	private AusPostShipmentPersonalName() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName#setSurname(java.lang.String)
	 */
	@Override
	public void setSurname(String name) {
		if(null == name || name.isEmpty()) {
			return;
		}
		
		this.getName2Set().setSurname(name);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName#setMiddleName(java.lang.String)
	 */
	@Override
	public void setMiddleName(String name) {
		if(null == name || name.isEmpty()) {
			return;
		}
		
		this.getName2Set().setMiddleName(name);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName#setGivenName(java.lang.String)
	 */
	@Override
	public void setGivenName(String name) {
		if(null == name || name.isEmpty()) {
			return;
		}
		
		this.getName2Set().setGivenName(name);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName#setFullName(java.lang.String)
	 */
	@Override
	public void setFullName(String name) {
		if(null == name || name.isEmpty()) {
			return;
		}
		
		this.getName2Set().setFullName(name);		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName#getSurname()
	 */
	@Override
	public String getSurname() {
		if(null == this.name) {
			return null;
		}
		
		return this.name.getSurname();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName#getMiddleName()
	 */
	@Override
	public String getMiddleName() {
		if(null == this.name) {
			return null;
		}
		
		return this.name.getMiddleName();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName#getGivenName()
	 */
	@Override
	public String getGivenName() {
		if(null == this.name) {
			return null;
		}
		
		return this.name.getGivenName();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName#getFullName()
	 */
	@Override
	public String getFullName() {
		if(null == this.name) {
			return null;
		}
		
		return this.name.getFullName();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.name || !this.name.validate()) {
			return false;
		}
		
		if( MAX_LEN < this.name.getFullName().length() ) {
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.Identity#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.setFullName(value);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.Identity#getValue()
	 */
	@Override
	public String getValue() {
		return this.getFullName();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	public static AusPostShipmentPersonalName copy(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName source) {
		if(null == source) {
			return null;
		}
		
		AusPostShipmentPersonalName.Builder builder = AusPostShipmentPersonalName.builder();
		
		builder.setGivenName(source.getGivenName());
		if( MAX_LEN >= source.getFullName().length() ) {
			builder.setMiddleName(source.getMiddleName());
		}
		builder.setSurname(source.getSurname());
		
		return builder.build();
	}
	
	public static AusPostShipmentPersonalName copy(BusinessName source) {
		if(null == source) {
			return null;
		}
		
		AusPostShipmentPersonalName.Builder builder = AusPostShipmentPersonalName.builder();

		builder.setFullName(source.getName());
		
		return builder.build();
	}
	
	private PersonalName getName2Set() {
		if(null == this.name) {
			this.name = PersonalName.builder().build();
		}
		
		return this.name;
	}
}
