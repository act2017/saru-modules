/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Only three roman numerals allowed: I, II and III.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PackingGroupDesignators implements IsSerializable{
	public static final PackingGroupDesignators I		= new PackingGroupDesignators("I");
	public static final PackingGroupDesignators II		= new PackingGroupDesignators("II");
	public static final PackingGroupDesignators III		= new PackingGroupDesignators("III");

	private static Map<String, PackingGroupDesignators> VALUES = new HashMap<>();
		
	static {
		VALUES.put(I.getValue(), I);
		VALUES.put(II.getValue(), II);
		VALUES.put(III.getValue(), III);
	}	
	
	private String value = null;
	
	private PackingGroupDesignators() {
		
	}
	
	private PackingGroupDesignators(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public static PackingGroupDesignators fromValue(String value) {
		if (null == value || !VALUES.containsKey(value)) {
			return null;
		}
		
		return VALUES.get(value);
	}
}
