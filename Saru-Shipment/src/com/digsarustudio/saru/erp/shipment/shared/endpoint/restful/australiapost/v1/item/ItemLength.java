/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The length of the parcel in centimetres. If volumetric pricing applies then this is mandatory. 
 * Alternatively, you can use the cubic_volume field. You cannot specify both length/width/height and cubic_volume.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface ItemLength {
	public static final String DECIMAL_DELIMITER	= "\\.";
	public static final String DECIMAL_GLUE			= ".";
	
	/**
	 * 
	 * The builder for {@link ItemLength}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemLength> {
		Builder setLength(Integer length);
		Builder setLength(Integer length, Integer precision);
		Builder setLength(String length);
		Builder setLength(String length, String precision);
	}
	
	void setLength(Integer length);
	void setLength(Integer length, Integer precision);
	void setLength(String length);
	void setLength(String length, String precision);
	
	Integer getLength();
	Integer getPrecision();
	String getDecimalLength();
	
	Boolean validate();
}
