/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The technical or proper shipping name of the dangerous goods. 
 * Examples: ETHYL ALCOHOL, ETHANOL.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface ProperShippingName {
	/**
	 * 
	 * The builder for {@link ProperShippingName}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ProperShippingName> {
		Builder setName(String name);
	}
	
	void setName(String name);
	String getName();
	
	Boolean validate();
}
