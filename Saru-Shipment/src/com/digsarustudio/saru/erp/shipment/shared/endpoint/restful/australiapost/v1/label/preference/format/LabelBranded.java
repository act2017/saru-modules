/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Whether the Australia Post branding should be included on the labels. 
 * If you are using purchased stationary from Australia Post, then the generated labels should not include the branding. 
 * Note that for international shipments, this field is ignored.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface LabelBranded {
	/**
	 * 
	 * The builder for {@link LabelBranded}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<LabelBranded> {
		Builder setBranded();
		Builder setUnbranded();
	}
	
	void setBranded();
	void setUnbranded();
	
	Boolean isBranded();
	
	Boolean validate();
}
