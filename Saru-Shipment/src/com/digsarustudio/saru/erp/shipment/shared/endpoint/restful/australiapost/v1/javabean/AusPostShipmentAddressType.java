/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.AddressTypes;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AddressType;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The address type of sender.<br>
 * For Australia Post, the type of this is {@link AddressTypes#MerchantLocation}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AusPostShipmentAddressType implements AddressType, IsSerializable {
	
	/**
	 * 
	 * The object builder for {@link AusPostShipmentAddressType}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements AddressType.Builder {
		private AusPostShipmentAddressType result = null;

		private Builder() {
			this.result = new AusPostShipmentAddressType();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AddressType.Builder#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.AddressTypes)
		 */
		@Override
		public Builder setType(AddressTypes type) {
			this.result.setType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AddressType.Builder#setType(java.lang.String)
		 */
		@Override
		public Builder setType(String type) {
			this.result.setType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AusPostShipmentAddressType build() {
			return this.result;
		}

	}
	
	private AddressTypes	type	= null;
	
	/**
	 * 
	 */
	private AusPostShipmentAddressType() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AddressType#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.AddressTypes)
	 */
	@Override
	public void setType(AddressTypes type) {
		this.type = type;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AddressType#setType(java.lang.String)
	 */
	@Override
	public void setType(String type) {
		this.type = AddressTypes.fromValue(type);

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AddressType#getType()
	 */
	@Override
	public String getType() {
		
		return this.type.getValue();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AddressType#getAddressType()
	 */
	@Override
	public AddressTypes getAddressType() {
		
		return this.type;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AddressType#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.type) {
			return false;
		}
		
		return true;
	}

	public static AusPostShipmentAddressType copy(AddressType source) {
		AusPostShipmentAddressType.Builder builder = AusPostShipmentAddressType.builder();
		
		builder.setType(source.getType());
		
		return builder.build();
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}	
}
