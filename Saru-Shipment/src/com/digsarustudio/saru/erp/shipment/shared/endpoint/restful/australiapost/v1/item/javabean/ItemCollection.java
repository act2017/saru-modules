/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemCollection}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemCollection implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemCollection
									 , IsSerializable {

	/**
	 * 
	 * The object builder for {@link ItemCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemCollection.Builder {
		private ItemCollection result = null;

		private Builder() {
			this.result = new ItemCollection();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemCollection.Builder#addItem(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item)
		 */
		@Override
		public Builder addItem(Item item) {
			this.result.addItem(item);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemCollection.Builder#setItems(java.util.List)
		 */
		@Override
		public Builder setItems(List<Item> items) {
			this.result.setItems(items);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemCollection.Builder#setItems(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item[])
		 */
		@Override
		public Builder setItems(Item[] items) {
			this.result.setItems(items);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemCollection build() {
			return this.result;
		}

	}
	
	private List<Item>	items	= null;
	
	/**
	 * 
	 */
	private ItemCollection() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ItemCollection#addItem(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item)
	 */
	@Override
	public void addItem(Item item) {
		if(null == this.items) {
			this.items = new ArrayList<>();
		}

		this.items.add(item);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ItemCollection#setItems(java.util.List)
	 */
	@Override
	public void setItems(List<Item> items) {
		this.items = items;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ItemCollection#setItems(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item[])
	 */
	@Override
	public void setItems(Item[] items) {
		this.setItems(Arrays.asList(items));
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ItemCollection#getItem(java.lang.Integer)
	 */
	@Override
	public Item getItem(Integer index) {
		if(null == this.items || index > this.items.size()) {
			return null;
		}

		return this.items.get(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ItemCollection#getItems()
	 */
	@Override
	public List<Item> getItems() {

		return this.items;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ItemCollection#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.items || this.items.isEmpty()) {
			return false;
		}

		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
