/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Use this field to adjust the left margin of the page.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface LabelFormatOffset {
	/**
	 * 
	 * The builder for {@link LabelFormatOffset}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<LabelFormatOffset> {
		Builder setValue(String value);
		Builder setValue(Integer value);
	}
	
	void setValue(String value);
	void setValue(Integer value);
	
	Integer getValue();
	String	getValueString();
	
	Boolean validate();
}
