/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean;

import java.util.HashMap;
import java.util.Map;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeature;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureCollection}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemFeatureCollection implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureCollection
											, IsSerializable {
	public static final	String	ATTR_TRANSIT_COVER	= "TRANSIT_COVER";


	/**
	 * 
	 * The object builder for {@link ItemFeatureCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureCollection.Builder {
		private ItemFeatureCollection result = null;

		private Builder() {
			this.result = new ItemFeatureCollection();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeatureCollection.Builder#addFeature(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeature)
		 */
		@Override
		public Builder addFeature(ItemFeature feature) {
			this.result.addFeature(feature);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeatureCollection.Builder#setTransitCover(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeature)
		 */
		@Override
		public Builder setTransitCover(ItemFeature transitCover) {
			this.result.setTransitCover(transitCover);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemFeatureCollection build() {
			return this.result;
		}

	}
	
	private Map<String, ItemFeature>	features	= null;
	
	/**
	 * 
	 */
	private ItemFeatureCollection() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeatureCollection#addFeature(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeature)
	 */
	@Override
	public void addFeature(ItemFeature feature) {
		if(null == this.features) {
			this.features = new HashMap<String, ItemFeature>();
		}

		this.features.put(feature.getName(), feature);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeatureCollection#getFeature(java.lang.String)
	 */
	@Override
	public ItemFeature getFeature(String name) {
		if(null == this.features || null == name || !this.features.containsKey(name)) {
			return null;
		}
		
		return this.features.get(name);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeatureCollection#setTransitCover(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeature)
	 */
	@Override
	public void setTransitCover(ItemFeature transitCover) {
		this.addFeature(transitCover);
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeatureCollection#getTransitCover()
	 */
	@Override
	public ItemFeature getTransitCover() {
		return this.getFeature(ATTR_TRANSIT_COVER);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeatureCollection#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.features || this.features.isEmpty()) {
			return false;
		}
				
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
