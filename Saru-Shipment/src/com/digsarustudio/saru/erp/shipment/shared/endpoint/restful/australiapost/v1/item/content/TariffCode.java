/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The Harmonised Tariff number that indicates the type of this described content. 
 * It is only mandatory if commercial_value is "true", otherwise it is optional. 
 * However, it is a recommended field to expedite the customs process in the receiving country.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface TariffCode {
	/**
	 * 
	 * The builder for {@link TariffCode}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<TariffCode> {
		Builder setCode(Integer code);
		Builder setCode(String code);
	}
	
	void setCode(Integer code);
	void setCode(String code);
	
	Integer getCode();
	String getCodeString();
	
	Boolean validate();
}
