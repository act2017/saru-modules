/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import java.util.HashMap;
import java.util.Map;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorContextPair;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorContext}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AusPostErrorContext implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorContext
									, IsSerializable {
	/**
	 * 
	 * The object builder for {@link AusPostErrorContext}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorContext.Builder {
		private AusPostErrorContext result = null;

		private Builder() {
			this.result = new AusPostErrorContext();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorContext.Builder#addPair(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorContextPair)
		 */
		@Override
		public Builder addPair(AusPostErrorContextPair pair) {
			this.result.addPair(pair);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorContext.Builder#addPair(java.lang.String, java.lang.String)
		 */
		@Override
		public Builder addPair(String key, String value) {
			this.result.addPair(key, value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AusPostErrorContext build() {
			return this.result;
		}

	}

	Map<String, AusPostErrorContextPair> pairs	= null;
	
	/**
	 * 
	 */
	private AusPostErrorContext() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorContext#addPair(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorContextPair)
	 */
	@Override
	public void addPair(AusPostErrorContextPair pair) {
		if(null == pair) {
			return;
		}
		
		if(null == this.pairs) {
			this.pairs = new HashMap<>();
		}

		this.pairs.put(pair.getName(), pair);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorContext#addPair(java.lang.String, java.lang.String)
	 */
	@Override
	public void addPair(String key, String value) {
		if(null == key || key.isEmpty()) {
			return;
		}

		this.addPair(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorContextPair.builder().setName(key)
																																	 .setValue(value)
																																	 .build());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorContext#getPairs()
	 */
	@Override
	public Map<String, AusPostErrorContextPair> getPairs() {
		
		return this.pairs;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorContext#getPair(java.lang.String)
	 */
	@Override
	public AusPostErrorContextPair getPair(String key) {
		if(null == key || key.isEmpty()) {
			return null;
		}
		
		if(null == this.pairs || !this.pairs.containsKey(key)) {
			return null;
		}
		
		return this.pairs.get(key);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ErrorContext#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.pairs || this.pairs.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
