/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.OrderShipmentItemStatuses;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class OrderShipmentItemStatus implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus
											  , IsSerializable {
	/**
	 * 
	 * The object builder for {@link OrderShipmentItemStatus}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus.Builder {
		private OrderShipmentItemStatus result = null;

		private Builder() {
			this.result = new OrderShipmentItemStatus();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus.Builder#setStatus(java.lang.String)
		 */
		@Override
		public Builder setStatus(String status) {
			this.result.setStatus(status);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus.Builder#setStatus(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.OrderShipmentItemStatuses)
		 */
		@Override
		public Builder setStatus(OrderShipmentItemStatuses status) {
			this.result.setStatus(status);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public OrderShipmentItemStatus build() {
			return this.result;
		}

	}
	
	private String status	= null;
	
	/**
	 * 
	 */
	private OrderShipmentItemStatus() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus#setStatus(java.lang.String)
	 */
	@Override
	public void setStatus(String status) {
		this.status = status;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus#setStatus(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.OrderShipmentItemStatuses)
	 */
	@Override
	public void setStatus(OrderShipmentItemStatuses status) {
		if(null == status) {
			return;
		}
		
		this.setStatus(status.getValue());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus#getStatus()
	 */
	@Override
	public String getStatus() {

		return this.status;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus#getOrderShipmentItemStatuses()
	 */
	@Override
	public OrderShipmentItemStatuses getOrderShipmentItemStatuses() {

		return OrderShipmentItemStatuses.fromValue(this.status);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderShipmentItemStatus#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.status || this.status.isEmpty()) {
			return false;
		}

		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
