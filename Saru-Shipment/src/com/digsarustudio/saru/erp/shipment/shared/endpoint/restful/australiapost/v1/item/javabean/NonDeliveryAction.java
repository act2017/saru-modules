/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.NonDeliveryActions;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.NonDeliveryAction}.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class NonDeliveryAction implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.NonDeliveryAction
										, IsSerializable {

	/**
	 * 
	 * The object builder for {@link NonDeliveryAction}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.NonDeliveryAction.Builder {
		private NonDeliveryAction result = null;

		private Builder() {
			this.result = new NonDeliveryAction();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.NonDeliveryAction.Builder#setAction(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.NonDeliveryActions)
		 */
		@Override
		public Builder setAction(NonDeliveryActions action) {
			this.result.setAction(action);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.NonDeliveryAction.Builder#setAction(java.lang.String)
		 */
		@Override
		public Builder setAction(String action) {
			this.result.setAction(action);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public NonDeliveryAction build() {
			return this.result;
		}

	}
	
	private String action = null;
	
	/**
	 * 
	 */
	private NonDeliveryAction() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.NonDeliveryAction#setAction(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.NonDeliveryActions)
	 */
	@Override
	public void setAction(NonDeliveryActions action) {
		this.action = action.getValue();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.NonDeliveryAction#setAction(java.lang.String)
	 */
	@Override
	public void setAction(String action) {
		this.action = action;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.NonDeliveryAction#getAction()
	 */
	@Override
	public String getAction() {
		return this.action;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.NonDeliveryAction#getNonDeliveryAction()
	 */
	@Override
	public NonDeliveryActions getNonDeliveryAction() {
		return NonDeliveryActions.fromValue(this.action);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.NonDeliveryAction#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.action || this.action.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
