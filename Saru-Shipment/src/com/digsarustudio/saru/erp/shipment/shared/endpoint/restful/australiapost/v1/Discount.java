/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The discount for this shipment.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface Discount {
	/**
	 * 
	 * The builder for {@link Discount}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Discount> {
		Builder setValue(Integer discount);
		Builder setValue(Integer discount, Integer precision);

		Builder setValue(String discount);
		Builder setValue(String discount, String precision);
	}
	
	void setValue(Integer discount);
	void setValue(Integer discount, Integer precision);

	void setValue(String discount);
	void setValue(String discount, String precision);
	
	String getDiscount();
	String getPrcision();
	String getDecimalDiscount();
	
	Boolean validate();
}
