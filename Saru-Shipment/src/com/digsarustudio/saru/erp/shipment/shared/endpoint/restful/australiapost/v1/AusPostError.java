/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The errors thrown from Australia Post API.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface AusPostError {
	/**
	 * 
	 * The builder for {@link AusPostError}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<AusPostError> {
		Builder setCode(String code);
		Builder setName(String name);
		Builder setMessage(String message);
		Builder addContextPair(AusPostErrorContextPair pair);
		Builder setContext(AusPostErrorContext context);
	}
	
	void setCode(String code);
	void setName(String name);
	void setMessage(String message);
	void addContextPair(AusPostErrorContextPair pair);
	void setContext(AusPostErrorContext context);

	/**
	 * Returns the code associated with the error.<br>
	 * 
	 * @return The code associated with the error.
	 */
	String getCode();
	
	/**
	 * Returns the name category for the error.<br>
	 * 
	 * @return The name category for the error.
	 */
	String getName();
	
	/**
	 * Returns the human readable error message.<br>
	 * 
	 * @return The human readable error message.
	 */
	String getMessage();
	
	/**
	 * Returns a map of values that attempts to highlight important information that may have caused 
	 * the error. This may or may not be populated depending on the type of error.<br>
	 * 
	 * @return A map of values that attempts to highlight important information that may have caused 
	 * 			the error. This may or may not be populated depending on the type of error.
	 */
	AusPostErrorContext getContext();
	
	Boolean validate();
}
