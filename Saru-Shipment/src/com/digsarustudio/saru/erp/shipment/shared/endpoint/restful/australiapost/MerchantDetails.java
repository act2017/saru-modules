/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type has the details of merchant.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface MerchantDetails {
	public static final String PASSWORD_DELIMITER	= ":";
	/**
	 * 
	 * The builder for {@link MerchantDetails}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<MerchantDetails> {
		Builder setAccountNumber(String number);
		Builder setAPIKey(String key);
		Builder setPassword(String password);
	}
	
	void setAccountNumber(String number);
	void setAPIKey(String key);
	void setPassword(String password);
	
	String getAccountNumber();
	String getAPIKey();
	String getPassword();
	String getBase64Authorisation();
}
