/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean;

import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.DeliveryWindow;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.ShipmentFeatureAttribute;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.DeliveryWindowCollection;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Please note that only one delivery time window is currently supported.

Attributes:
windows
A list of delivery time windows. Each window has a start time and an end time.
start
Start time of window. Time should be supplied in the following format hh24:mm:ss and should be before the end time.
end
End time of window. Time should be supplied in the following format hh24:mm:ss and should be after the start time.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
 */
public class DeliveryTimes implements ShipmentFeature, IsSerializable {
	public static final String NAME			= "DELIVERY_TIME";

	private DeliveryWindowCollection	windows	= null;
	
	/**
	 * 
	 * The object builder for {@link DeliveryTimes}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ShipmentFeature.DeliveryTimesBuilder {
		private DeliveryTimes result = null;

		private Builder() {
			this.result = new DeliveryTimes();
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature.DeliveryTimesBuilder#addDeliveryWindow(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.DeliveryWindow)
		 */
		@Override
		public DeliveryTimesBuilder addDeliveryWindow(DeliveryWindow window) {
			this.result.addDeliveryWindow(window);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature.DeliveryTimesBuilder#setDeliveryWindows(java.util.List)
		 */
		@Override
		public DeliveryTimesBuilder setDeliveryWindows(List<DeliveryWindow> windows) {
			this.result.setDeliveryWindows(windows);
			return this;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature.DeliveryTimesBuilder#setDeliveryWindows(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.DeliveryWindowCollection)
		 */
		@Override
		public DeliveryTimesBuilder setDeliveryWindows(DeliveryWindowCollection windows) {
			this.result.setAttribute(windows);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DeliveryTimes build() {
			return this.result;
		}

	}
	
	/**
	 * 
	 */
	private DeliveryTimes() {

	}
	
	public void addDeliveryWindow(DeliveryWindow window) {
		if(null == this.windows) {
			this.windows = DeliveryWindowCollection.builder().addDeliveryWindow(window)
															 .build();
			return;
		}
		
		this.windows.addDeliveryWindow(window);
	}
	
	public void setDeliveryWindows(List<DeliveryWindow> windows) {
		if(null == this.windows) {
			this.windows = DeliveryWindowCollection.builder().setDeliveryWindows(windows)
															 .build();
			return;
		}
		
		this.windows.setDeliveryWindows(windows);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature#setAttribute(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.ShipmentFeatureAttribute)
	 */
	@Override
	public void setAttribute(ShipmentFeatureAttribute<?> attribute) {
		this.windows = (DeliveryWindowCollection) attribute;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature#getAttribute()
	 */
	@Override
	public DeliveryWindowCollection getAttribute() {
		return this.windows;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.windows || !this.windows.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
