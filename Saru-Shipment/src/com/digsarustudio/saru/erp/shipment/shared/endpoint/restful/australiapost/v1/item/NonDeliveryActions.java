/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Your desired action if the delivery cannot be completed. 
 * You can specify "RETURN" or "ABANDONED". 
 * The default if not specified is to return the item by the most economical route or by air.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
 *
 */
public class NonDeliveryActions implements IsSerializable{
	public static final NonDeliveryActions	Return		= new NonDeliveryActions("RETURN", "Return");
	public static final NonDeliveryActions	Abandoned	= new NonDeliveryActions("ABANDONED", "Abandoned");

	private static Map<String, NonDeliveryActions> VALUES = new HashMap<>();
		
	static {
		VALUES.put(Return.getValue(), Return);
		VALUES.put(Abandoned.getValue(), Abandoned);
	}	
	
	private String value = null;
	private String display = null;
	
	private NonDeliveryActions() {
		
	}
	
	private NonDeliveryActions(String value, String display) {
		this.value = value;
		this.display = display;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public String getDisplayString() {
		return this.display;
	}
	
	public static NonDeliveryActions fromValue(String value) {
		if (null == value || !VALUES.containsKey(value)) {
			return null;
		}
		
		return VALUES.get(value);
	}
	
	public static NonDeliveryActions getDefault() {
		return Return;
	}
}
