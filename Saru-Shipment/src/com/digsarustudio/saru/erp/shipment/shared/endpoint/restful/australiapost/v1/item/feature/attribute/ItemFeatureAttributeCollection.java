/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeature;

/**
 * The attributes of {@link ItemFeature}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface ItemFeatureAttributeCollection {
	/**
	 * 
	 * The builder for {@link ItemFeatureAttributeCollectin}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemFeatureAttributeCollection> {
		Builder addAttribute(ItemFeatureAttribute attribute);
	}
	
	void addAttribute(ItemFeatureAttribute attribute);
	ItemFeatureAttribute getAttribute(String name);
	
	
	Boolean validate();
}
