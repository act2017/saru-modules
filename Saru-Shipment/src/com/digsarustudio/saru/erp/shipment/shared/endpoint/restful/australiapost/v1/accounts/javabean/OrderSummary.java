/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.accounts.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.accounts.OrderSummary}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class OrderSummary implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.accounts.OrderSummary
									, IsSerializable {
	/**
	 * 
	 * The object builder for {@link OrderSummary}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.accounts.OrderSummary.Builder {
		private OrderSummary result = null;

		private Builder() {
			this.result = new OrderSummary();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.accounts.OrderSummary.Builder#setPDFContent(java.lang.String)
		 */
		@Override
		public Builder setPDFContent(String content) {
			this.result.setPDFContent(content);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.accounts.OrderSummary.Builder#setOrderId(java.lang.String)
		 */
		@Override
		public Builder setOrderId(String id) {
			this.result.setOrderId(id);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public OrderSummary build() {
			return this.result;
		}

	}

	private String orderId		= null;
	private String pdfContent	= null;
	
	/**
	 * 
	 */
	private OrderSummary() {
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.accounts.OrderSummary#setPDFContent(java.lang.String)
	 */
	@Override
	public void setPDFContent(String content) {
		this.pdfContent = content;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.accounts.OrderSummary#getPDFContent()
	 */
	@Override
	public String getPDFContent() {
		
		return this.pdfContent;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.accounts.OrderSummary#setOrderId(java.lang.String)
	 */
	@Override
	public void setOrderId(String id) {
		this.orderId = id;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.accounts.OrderSummary#getOrderNo()
	 */
	@Override
	public String getOrderId() {
		return this.orderId;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.accounts.OrderSummary#valdiate()
	 */
	@Override
	public Boolean valdiate() {
		if(null == this.pdfContent || this.pdfContent.isEmpty()) {
			return false;
		}
		
		return true;
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}

}
