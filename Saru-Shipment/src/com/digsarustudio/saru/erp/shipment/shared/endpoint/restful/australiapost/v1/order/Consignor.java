/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The name or ID of the consignor for this order. If provided this information is included in the delivery documentation to assist traceability.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Consignor {
	/**
	 * 
	 * The builder for {@link Consignor}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Consignor> {
		Builder setValue(String value);
	}
	
	void setValue(String value);
	String getValue();
	
	Boolean validate();
}
