/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The value in Australian Dollars of this described content.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface ItemContentValue {
	/**
	 * 
	 * The builder for {@link ItemContentValue}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemContentValue> {
		Builder setValue(Integer value);
		Builder setValue(Integer value, Integer precision);
		Builder setValue(String value);
		Builder setValue(String value, String precision);
	}

	void setValue(Integer value);
	void setValue(Integer value, Integer precision);
	void setValue(String value);
	void setValue(String value, String precision);
	
	String getValue();
	String getPrecision();
	String getDecimalString();
	
	Boolean validate();
}
