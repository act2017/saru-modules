/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The suburb used by Australia Post Shipment API.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AusPostShipmentSuburb implements Suburb, IsSerializable {
	public static final Integer MAX_LEN	= 30;
	
	/**
	 * 
	 * The object builder for {@link AusPostShipmentSuburb}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements Suburb.Builder {
		private AusPostShipmentSuburb result = null;

		private Builder() {
			this.result = new AusPostShipmentSuburb();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb.Builder#setName(java.lang.String)
		 */
		@Override
		public Builder setName(String name) {
			this.result.setName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb.Builder#setCode(java.lang.String)
		 */
		@Override
		public Builder setCode(String code) {
			this.result.setCode(code);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AusPostShipmentSuburb build() {
			return this.result;
		}

	}

	private String suburb	= null;
	
	/**
	 * 
	 */
	private AusPostShipmentSuburb() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.suburb = name;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb#getName()
	 */
	@Override
	public String getName() {
		return this.suburb;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb#getCode()
	 */
	@Override
	public String getCode() {

		return null;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.suburb || this.suburb.isEmpty()) {
			return false;
		}
		
		if(MAX_LEN < this.suburb.length()) {
			return false;
		}

		return true;
	}

	public static AusPostShipmentSuburb copy(Suburb source) {
		if(null == source) {
			return null;
		}
		
		AusPostShipmentSuburb.Builder builder = AusPostShipmentSuburb.builder();
		
		builder.setCode(source.getCode());
		builder.setName(source.getName());
		
		return builder.build();
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
