/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The response object returned by Australia Post Order API.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface CreateOrderResponse {
	/**
	 * 
	 * The builder for {@link CreateOrderResponse}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<CreateOrderResponse> {
		Builder setOrder(Order order);
	}
	
	void setOrder(Order order);
	
	Order getOrder();
	
	Boolean validate();
}
