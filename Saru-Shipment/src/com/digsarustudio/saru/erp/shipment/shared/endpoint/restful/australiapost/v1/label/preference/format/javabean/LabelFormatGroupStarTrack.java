/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelFormatGroups;

/**
 * This class represents the label format - Star Track.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelFormatGroupStarTrack extends LabelFormatGroup {
	public static final LabelFormatLayout	A4_1pp				= new LabelFormatLayoutA4_1pp();
	public static final LabelFormatLayout	A4_2pp				= new LabelFormatLayoutA4_2pp();
	public static final LabelFormatLayout	A4_1pp_Landscape	= new LabelFormatLayoutA4_1pp_Landscape();
	public static final LabelFormatLayout	A4_2pp_Landscape	= new LabelFormatLayoutA4_2pp_Landscape();
	public static final LabelFormatLayout	A6_1pp				= new LabelFormatLayoutA6_1pp();
	
	public LabelFormatGroupStarTrack() {
		this.setValue(LabelFormatGroups.StarTrack);
	}
}
