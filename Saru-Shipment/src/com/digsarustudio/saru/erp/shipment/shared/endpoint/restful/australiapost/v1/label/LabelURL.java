/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The HTTP URL from which to download the PDF label. A request to this link will return data with a content type of application/pdf.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface LabelURL {
	/**
	 * 
	 * The builder for {@link LabelURL}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<LabelURL> {
		Builder setValue(String value);
	}
	
	void setValue(String value);
	String getValue();
	
	Boolean validate();
}
