/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ATLNumber}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ATLNumber implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ATLNumber
								, IsSerializable {
	public static final Integer LENGTH		= 10;
	public static final Integer	LEN_PREFIX	= 1;
	public static final	String	PREFIX		= "C";
	public static final	Integer	LEN_SUFFIX	= 9;

	/**
	 * 
	 * The object builder for {@link ATLNumber}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ATLNumber.Builder {
		private ATLNumber result = null;

		private Builder() {
			this.result = new ATLNumber();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ATLNumber.Builder#setNumber(java.lang.String)
		 */
		@Override
		public Builder setNumber(String number) {
			this.result.setNumber(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ATLNumber build() {
			return this.result;
		}

	}
	
	private String number	= null;
	
	/**
	 * 
	 */
	private ATLNumber() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ATLNumber#setNumber(java.lang.String)
	 */
	@Override
	public void setNumber(String number) {
		this.number = number;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ATLNumber#getNumber()
	 */
	@Override
	public String getNumber() {
		return this.number;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ATLNumber#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.number || this.number.isEmpty()) {
			return false;
		}
		
		if(LENGTH != this.number.length()) {
			return false;
		}
		
		if( !PREFIX.equals(this.number.substring(0, LEN_PREFIX)) ) {
			return false;
		}
		
		
		String digits = this.number.substring(LEN_PREFIX, LENGTH-1);
		if( LEN_SUFFIX != digits.length() ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
