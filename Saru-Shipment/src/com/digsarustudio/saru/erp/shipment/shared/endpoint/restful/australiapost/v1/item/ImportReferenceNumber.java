/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Receiver's reference number. 
 * The merchant can use this field to provide information to the receiver as it is printed on the label.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface ImportReferenceNumber {
	/**
	 * 
	 * The builder for {@link ImportReferenceNumber}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ImportReferenceNumber> {
		Builder setNumber(String number);	
	}
	
	void setNumber(String number);
	String getNumber();
	
	Boolean validate();
}
