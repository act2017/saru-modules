/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The error codes and descriptions related to the action attempted.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface AusPostErrorsResponse {

	/**
	 * 
	 * The builder for {@link AusPostErrorCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<AusPostErrorsResponse> {
		Builder addError(AusPostError error);
		Builder setErrors(List<AusPostError> errors);
		Builder setErrors(AusPostError[] errors);
		Builder setErrors(AusPostErrorCollection errors);
	}

	void addError(AusPostError error);
	void setErrors(List<AusPostError> errors);
	void setErrors(AusPostError[] errors);
	void setErrors(AusPostErrorCollection errors);
	
	AusPostError getError(Integer code);
	AusPostError getError(String code);
	List<AusPostError> getErrors();
	AusPostErrorCollection getErrorCollecion();
	
	Boolean validate();
}
