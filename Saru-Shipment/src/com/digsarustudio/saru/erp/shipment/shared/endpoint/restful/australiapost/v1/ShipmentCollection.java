/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.ShipmentEndpoint;

/**
 * This is a collection of {@link Shipment} used for {@link ShipmentEndpoint}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface ShipmentCollection {
	/**
	 * 
	 * The builder for {@link ShipmentCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ShipmentCollection> {
		Builder addShipment(Shipment shipment);
		Builder setShipments(List<Shipment> shipments);
	}
	
	void addShipment(Shipment shipment);
	void setShipments(List<Shipment> shipments);
	
	Shipment getShipment(Integer index);
	List<Shipment> getShipments();
	Integer size();
	
	Boolean validate();
}
