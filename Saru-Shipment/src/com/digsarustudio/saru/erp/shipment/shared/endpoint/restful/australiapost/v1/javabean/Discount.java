/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Discount}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class Discount implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Discount
								, IsSerializable {
	public static final Integer	MAX_LEN_PRECISION	= 2;
	public static final String	DECIMAL_DELIMITER	= "\\.";
	public static final String	DECIMAL_GLUE		= ".";
	
	/**
	 * 
	 * The object builder for {@link Discount}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Discount.Builder {
		private Discount result = null;

		private Builder() {
			this.result = new Discount();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Discount.Builder#setValue(java.lang.Integer)
		 */
		@Override
		public Builder setValue(Integer discount) {
			this.result.setValue(discount);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Discount.Builder#setValue(java.lang.Integer, java.lang.Integer)
		 */
		@Override
		public Builder setValue(Integer discount, Integer precision) {
			this.result.setValue(discount, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Discount.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String discount) {
			this.result.setValue(discount);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Discount.Builder#setValue(java.lang.String, java.lang.String)
		 */
		@Override
		public Builder setValue(String discount, String precision) {
			this.result.setValue(discount, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Discount build() {
			return this.result;
		}

	}
	
	private String main			= null;
	private String precision	= null;
	
	/**
	 * 
	 */
	private Discount() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Discount#setValue(java.lang.Integer)
	 */
	@Override
	public void setValue(Integer discount) {
		this.setValue(discount, null);

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Discount#setValue(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void setValue(Integer discount, Integer precision) {
		String main = (null == discount) ? null : discount.toString();
		String pre	= (null == precision) ? null : precision.toString();

		this.setValue(main, pre);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Discount#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String discount) {
		if(null == discount) {
			return;
		}

		String[] values = discount.split(DECIMAL_DELIMITER);
		
		String main = values[0];
		String pre	= null;
		
		if(2 == values.length) {
			pre = values[1];
		}
		
		this.setValue(main, pre);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Discount#setValue(java.lang.String, java.lang.String)
	 */
	@Override
	public void setValue(String discount, String precision) {
		this.main 		= discount;
		this.precision	= precision;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Discount#getDiscount()
	 */
	@Override
	public String getDiscount() {
		return this.main;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Discount#getPrcision()
	 */
	@Override
	public String getPrcision() {
		return this.precision;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Discount#getDecimalDiscount()
	 */
	@Override
	public String getDecimalDiscount() {
		if( !this.hasMain() && !this.hasPrecision() ) {
			return null;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		if( !this.hasMain() ) {
			buffer.append("0");
		}else {
			buffer.append(this.main);
		}
		
		if( this.hasPrecision() ) {
			buffer.append(DECIMAL_GLUE);
			buffer.append(this.precision);
		}
		
		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Discount#validate()
	 */
	@Override
	public Boolean validate() {
		if( !this.hasMain() && !this.hasPrecision() ) {
			return false;
		}
		
		if( this.hasPrecision() && MAX_LEN_PRECISION < this.precision.length() ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	private Boolean hasMain() {
		return (null != this.main && !this.main.isEmpty());
	}
	
	private Boolean hasPrecision() {
		return (null != this.precision && !this.precision.isEmpty());
	}
}
