/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The packaging type for this item. Some example values are: CTN, PAL, SAT, BAG, ENV, ITM, JIF, SKI
 * Note, for StarTrack products, the packaging type (refereed to as a unit type by StarTrack) is mandatory and 
 * has a maximum length of 3.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
 */
public class PackagingTypes implements IsSerializable{
	public static final PackagingTypes	Carton		= new PackagingTypes("CTN");
	public static final PackagingTypes	Pallet		= new PackagingTypes("PAL");
	public static final PackagingTypes	Satchel		= new PackagingTypes("SAT");
	public static final PackagingTypes	Bag			= new PackagingTypes("BAG");
	public static final PackagingTypes	Envelope	= new PackagingTypes("ENV");
	public static final PackagingTypes	ITM			= new PackagingTypes("ITM");
	public static final PackagingTypes	JIF			= new PackagingTypes("JIF");
	public static final PackagingTypes	SKI			= new PackagingTypes("SKI");
	
	
	private static Map<String, PackagingTypes> VALUES = new HashMap<>();
		
	static {
		VALUES.put(Carton.getValue(), Carton);
		VALUES.put(Pallet.getValue(), Pallet);
		VALUES.put(Satchel.getValue(), Satchel);
		VALUES.put(Bag.getValue(), Bag);
		VALUES.put(Envelope.getValue(), Envelope);
		VALUES.put(ITM.getValue(), ITM);
		VALUES.put(JIF.getValue(), JIF);
		VALUES.put(SKI.getValue(), SKI);
	}	
	
	private String value = null;
	
	private PackagingTypes() {
		
	}
	
	private PackagingTypes(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public static PackagingTypes fromValue(String value) {
		if (null == value || !VALUES.containsKey(value)) {
			return null;
		}
		
		return VALUES.get(value);
	}
}
