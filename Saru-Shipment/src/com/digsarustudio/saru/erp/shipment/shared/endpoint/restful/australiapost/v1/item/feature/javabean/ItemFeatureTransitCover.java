/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeature;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureBundled;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeaturePrice;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureTypes;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttribute;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttributeCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.ItemFeatureTransitCoverAttributeCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverAmount;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverIncluded;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverMaximum;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverRate;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The item transit cover wokrs as {@link ItemFeature} for {@link Item}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemFeatureTransitCover implements ItemFeature, IsSerializable {
	public static final String	ATTRIBUTE_COVER_AMOUNT	= "cover_amount";
	
	//===	Response	====
	public static final String	ATTRIBUTE_RATE			= "rate";
	public static final String	ATTRIBUTE_INCLUDED_COVER= "included_cover";
	public static final String	ATTRIBUTE_MAXIMUM_COVER	= "maximum_cover";
	
	public static final String	ATTR_PRICE				= "price";
	public static final	String	ATTR_BUNDLED			= "bundled";
	
	public static final String	NAME					= "TRANSIT_COVER";

	/**
	 * 
	 * The object builder for {@link ItemFeatureTransitCover}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ItemFeature.Builder {
		private ItemFeatureTransitCover result = null;

		private Builder() {
			this.result = new ItemFeatureTransitCover();
		}

		/**
		 * @param coverAmount
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureTransitCover#setTransitCoverAmount(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttribute)
		 */
		public Builder setTransitCoverAmount(ItemFeatureAttribute coverAmount) {
			result.setTransitCoverAmount(coverAmount);
			return this;
		}

		/**
		 * @param coverAmount
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureTransitCover#setRate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttribute)
		 */
		public Builder setRate(ItemFeatureAttribute coverAmount) {
			result.setRate(coverAmount);
			return this;
		}

		/**
		 * @param coverAmount
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureTransitCover#setCoverIncluded(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttribute)
		 */
		public Builder setCoverIncluded(ItemFeatureAttribute coverAmount) {
			result.setCoverIncluded(coverAmount);
			return this;
		}

		/**
		 * @param cover
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureTransitCover#setMaximumCover(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttribute)
		 */
		public Builder setMaximumCover(ItemFeatureAttribute cover) {
			result.setMaximumCover(cover);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeature.Builder#addAttribute(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute)
		 */
		@Override
		public Builder addAttribute(ItemFeatureAttribute attribute) {
			this.result.addAttribute(attribute);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeature.Builder#setItemFeatureAttributeCollection(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttributeCollection)
		 */
		@Override
		public Builder setItemFeatureAttributeCollection(ItemFeatureAttributeCollection attributes) {
			this.result.setItemFeatureAttributeCollection(attributes);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeature.Builder#setName(java.lang.String)
		 */
		@Override
		public Builder setName(String name) {
			this.result.setName(name);
			return this;
		}

		/**
		 * @param amount
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureTransitCover#setTransitCoverAmount(java.lang.Integer)
		 */
		public Builder setTransitCoverAmount(Integer amount) {
			result.setTransitCoverAmount(amount);
			return this;
		}

		/**
		 * @param amount
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureTransitCover#setTransitCoverAmount(java.lang.String)
		 */
		public Builder setTransitCoverAmount(String amount) {
			result.setTransitCoverAmount(amount);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeature.Builder#setType(java.lang.String)
		 */
		@Override
		public Builder setType(String type) {
			this.result.setType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeature.Builder#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureTypes)
		 */
		@Override
		public Builder setType(ItemFeatureTypes type) {
			this.result.setType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeature.Builder#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureType)
		 */
		@Override
		public Builder setType(ItemFeatureType type) {
			this.result.setType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeature.Builder#setPrice(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeaturePrice)
		 */
		@Override
		public Builder setPrice(ItemFeaturePrice price) {
			this.result.setPrice(price);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeature.Builder#setBundled(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureBundled)
		 */
		@Override
		public Builder setBundled(ItemFeatureBundled bundled) {
			this.result.setBundled(bundled);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemFeatureTransitCover build() {
			return this.result;
		}

	}

	private ItemFeatureAttributeCollection	attributes	= null;
	private ItemFeaturePrice				price		= null;
	private ItemFeatureType					type		= null;
	private ItemFeatureBundled				bundled		= null;
	
	private String							name		= null;
	
	/**
	 * 
	 */
	private ItemFeatureTransitCover() {
		this.setName(NAME);
		this.setType(ItemFeatureTypes.TransitCover);
	}
	
	public void setTransitCoverAmount(ItemFeatureAttribute coverAmount) {
		this.addAttribute(coverAmount);
	}
	
	public void setTransitCoverAmount(Integer amount) {
		if(null == amount) {
			return;
		}
		
		this.setTransitCoverAmount(amount.toString());
	}
	
	public void setTransitCoverAmount(String amount) {
		this.setTransitCoverAmount(TransitCoverAmount.builder().setValue(amount)
															   .build());
	}
	
	public TransitCoverAmount	getTransitCoverAmount() {
		return (TransitCoverAmount) this.getAttribute(ATTRIBUTE_COVER_AMOUNT);
	}
	
	public void setRate(ItemFeatureAttribute coverAmount) {
		this.addAttribute(coverAmount);
	}
	
	public TransitCoverRate	getRate() {
		return (TransitCoverRate) this.getAttribute(ATTRIBUTE_RATE);
	}
	
	public void setCoverIncluded(ItemFeatureAttribute coverAmount) {
		this.addAttribute(coverAmount);
	}
	
	public TransitCoverIncluded	getCoverIncluded() {
		return (TransitCoverIncluded) this.getAttribute(ATTRIBUTE_INCLUDED_COVER);
	}
	
	public void setMaximumCover(ItemFeatureAttribute cover) {
		this.addAttribute(cover);
	}
	
	public TransitCoverMaximum	getMaximumCover() {
		return (TransitCoverMaximum) this.getAttribute(ATTRIBUTE_MAXIMUM_COVER);
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeature#setType(java.lang.String)
	 */
	@Override
	public void setType(String type) {
		this.type = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureType.builder().setValue(type).build();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeature#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureTypes)
	 */
	@Override
	public void setType(ItemFeatureTypes type) {
		if(null == type) {
			return;
		}
		
		this.setType(type.getValue());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeature#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureType)
	 */
	@Override
	public void setType(ItemFeatureType type) {
		this.type = type;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeature#setPrice(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeaturePrice)
	 */
	@Override
	public void setPrice(ItemFeaturePrice price) {
		this.price = price;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeature#getPrice()
	 */
	@Override
	public ItemFeaturePrice getPrice() {
		return this.price;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeature#getItemFeatureType()
	 */
	@Override
	public ItemFeatureType getItemFeatureType() {
		return this.type;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeature#setBundled(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureBundled)
	 */
	@Override
	public void setBundled(ItemFeatureBundled bundled) {
		this.bundled = bundled;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeature#getBundled()
	 */
	@Override
	public ItemFeatureBundled getBundled() {
		return this.bundled;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeature#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeature#getName()
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeature#addAttribute(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute)
	 */
	@Override
	public void addAttribute(ItemFeatureAttribute attribute) {
		if(null == this.attributes) {
			this.attributes = ItemFeatureTransitCoverAttributeCollection.builder().addAttribute(attribute)
																				  .build();
			return;
		}
		
		this.attributes.addAttribute(attribute);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeature#setItemFeatureAttributeCollection(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttributeCollection)
	 */
	@Override
	public void setItemFeatureAttributeCollection(ItemFeatureAttributeCollection attributes) {
		this.attributes = attributes;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeature#getAttribute(java.lang.String)
	 */
	@Override
	public ItemFeatureAttribute getAttribute(String name) {
		if( !this.validate() ) {
			return null;
		}
				
		return this.attributes.getAttribute(name);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeature#getAttributes()
	 */
	@Override
	public ItemFeatureAttributeCollection getAttributes() {
		return this.attributes;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeature#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.type || !this.type.validate()) {
			return false;
		}
		
		if(null == this.attributes || !this.attributes.validate()) {
			return false;
		}
		
		if(null != this.price && !this.price.validate()) {
			return false;
		}
		
		if(null != this.bundled && !this.bundled.validate()) {
			return false;
		}				
		
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
