/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.DeliveryWindow;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.ShipmentFeatureAttribute;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.DeliveryWindowCollection}.<br>
 * Please note that only one delivery time window is currently supported.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
 */
public class DeliveryWindowCollection implements ShipmentFeatureAttribute<List<DeliveryWindow>>
											   , IsSerializable {
	public static final Integer MAX_COUNT	= 1;
	public static final String ATTR_WINDOWS	= "windows";

	/**
	 * 
	 * The object builder for {@link DeliveryWindowCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ShipmentFeatureAttribute.Builder<List<DeliveryWindow>> {
		private DeliveryWindowCollection result = null;

		private Builder() {
			this.result = new DeliveryWindowCollection();
		}

		public Builder setDeliveryWindows(List<DeliveryWindow> window) {
			this.result.setDeliveryWindows(window);
			return this;
		}

		public Builder setDeliveryWindows(DeliveryWindow[] window) {
			this.result.setDeliveryWindows(window);
			return this;
		}

		public Builder addDeliveryWindow(DeliveryWindow window) {
			this.result.addDeliveryWindow(window);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.ShipmentFeatureAttribute.Builder#setValue(java.lang.Object)
		 */
		@Override
		public Builder setValue(List<DeliveryWindow> value) {
			this.result.setDeliveryWindows(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DeliveryWindowCollection build() {
			return this.result;
		}

	}
	
	private List<DeliveryWindow> windows	= null;
	
	/**
	 * 
	 */
	private DeliveryWindowCollection() {

	}

	public void setDeliveryWindows(List<DeliveryWindow> windows) {
		this.windows = windows;

	}

	public void addDeliveryWindow(DeliveryWindow window) {
		if(null == this.windows) {
			this.windows = new ArrayList<>();
		}

		this.windows.add(window);
	}

	public DeliveryWindow getDeliveryWindow(Integer index) {
		if(null == index || null == this.windows || index >= this.windows.size()) {
			return null;
		}
		
		return this.windows.get(index);
	}

	public void setDeliveryWindows(DeliveryWindow[] windows) {
		this.setDeliveryWindows(Arrays.asList(windows));		
	}

	public List<DeliveryWindow> getDeliveryWindows() {
		
		return this.windows;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.ShipmentFeatureAttribute#setValue(java.lang.Object)
	 */
	@Override
	public void setValue(List<DeliveryWindow> value) {
		this.setDeliveryWindows(value);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.ShipmentFeatureAttribute#getValue()
	 */
	@Override
	public List<DeliveryWindow> getValue() {
		return this.getDeliveryWindows();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DeliveryWindowCollection#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.windows || this.windows.isEmpty()) {
			return false;
		}
		
		if(MAX_COUNT < this.windows.size()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
