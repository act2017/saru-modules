/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Time should be supplied in the following format hh24:mm:ss
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class Time implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time
						   , IsSerializable {
	/**
	 * 
	 * The object builder for {@link Time}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time.Builder {
		private Time result = null;

		private Builder() {
			this.result = new Time();
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time.Builder#setHour(java.lang.Integer)
		 */
		@Override
		public Builder setHour(Integer hr) {
			this.result.setHour(hr);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time.Builder#setMinute(java.lang.Integer)
		 */
		@Override
		public Builder setMinute(Integer min) {
			this.result.setMinute(min);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time.Builder#setSecond(java.lang.Integer)
		 */
		@Override
		public Builder setSecond(Integer sec) {
			this.result.setSecond(sec);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time.Builder#setTime(java.lang.String)
		 */
		@Override
		public Builder setTime(String time) {
			this.result.setTime(time);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Time build() {
			return this.result;
		}

	}
	
	private Integer hour		= null;
	private Integer minute		= null;
	private Integer second		= null;
	private com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time	timezone	= null;
	
	/**
	 * 
	 */
	private Time() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time#setHour(java.lang.Integer)
	 */
	@Override
	public void setHour(Integer hr) {
		this.hour = hr;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time#setMinute(java.lang.Integer)
	 */
	@Override
	public void setMinute(Integer min) {
		this.minute = min;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time#setSecond(java.lang.Integer)
	 */
	@Override
	public void setSecond(Integer sec) {
		this.second = sec;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time#getHour()
	 */
	@Override
	public Integer getHour() {
		
		return this.hour;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time#getMinute()
	 */
	@Override
	public Integer getMinute() {
		
		return this.minute;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time#getSecond()
	 */
	@Override
	public Integer getSecond() {
		
		return this.second;
	}	
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time#setTime(java.lang.String)
	 */
	@Override
	public void setTime(String time) {
		String[] times = time.split(TIME_ZONE_DELIMITER);
		
		if(2 == times.length) {
			this.setTimezone(times[1]);
		}
		
		String[] hms = times[0].split(DELIMITER);
		
		if( 1 <= hms.length ) {
			this.hour	= Integer.parseInt(hms[0]);
		}
		
		if( 2 <= hms.length ) {
			this.minute = Integer.parseInt(hms[1]);
		}
		
		if( 3 <= hms.length ) {
			this.second = Integer.parseInt(hms[2]);
		}		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time#getTime()
	 */
	@Override
	public String getTime() {
		StringBuffer buffer = new StringBuffer();
		
		if(null == this.hour) {
			buffer.append("00");
		}else if(10 > this.hour) {
			buffer.append("0");
			buffer.append(this.hour);
		}else {
			buffer.append(this.hour);
		}
		buffer.append(DELIMITER);
		
		if(null == this.minute) {
			buffer.append("00");
		}else if( 10 > this.minute ) {
			buffer.append("0");
			buffer.append(this.minute);
		}else {
			buffer.append(this.minute);
		}
		buffer.append(DELIMITER);
		
		if(null == this.second) {
			buffer.append("00");
		}else if( 10 > this.second ) {
			buffer.append("0");
			buffer.append(this.second);
		}else {
			buffer.append(this.second);
		}		
				
		if(null != this.timezone) {
			buffer.append(TIME_ZONE_DELIMITER);
			buffer.append(this.timezone.getTime());
		}
		
		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time#getDatabaseDateTime()
	 */
	@Override
	public String getDatabaseDateTime() {
		StringBuffer buffer = new StringBuffer();
		
		if(null == this.hour) {
			buffer.append("00");
		}else if(10 > this.hour) {
			buffer.append("0");
			buffer.append(this.hour);
		}else {
			buffer.append(this.hour);
		}
		buffer.append(DELIMITER);
		
		if(null == this.minute) {
			buffer.append("00");
		}else if( 10 > this.minute ) {
			buffer.append("0");
			buffer.append(this.minute);
		}else {
			buffer.append(this.minute);
		}
		buffer.append(DELIMITER);
		
		if(null == this.second) {
			buffer.append("00");
		}else if( 10 > this.second ) {
			buffer.append("0");
			buffer.append(this.second);
		}else {
			buffer.append(this.second);
		}		
		
		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time#setTimezone(java.lang.String)
	 */
	@Override
	public void setTimezone(String timezone) {
		this.timezone = Time.builder().setTime(timezone)
									  .build();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time#getTimezone()
	 */
	@Override
	public com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time getTimezone() {
		return this.timezone;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.hour || 0 > this.hour || 24 < this.hour) {
			return false;
		}
		
		if(null == this.minute || 0 > this.minute || 60 <= this.hour) {
			return false;
		}
		
		if(null == this.second || 0 > this.second || 60 <= this.second) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
