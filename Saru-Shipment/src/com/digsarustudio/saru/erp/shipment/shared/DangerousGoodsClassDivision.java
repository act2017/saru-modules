/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Contains the dangerous goods class or division. 
 * This field is a decimal value, but must be passed as a string.
 * Examples: 3, 1.4, 2.1.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface DangerousGoodsClassDivision {
	/**
	 * 
	 * The builder for {@link DangerousGoodsClassDivision}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<DangerousGoodsClassDivision> {
		Builder setValue(DangerousGoodsClassDivisions classDivision);
		Builder setValue(String classDivision);
	}
	
	void setValue(DangerousGoodsClassDivisions classDivision);
	void setValue(String classDivision);
	
	DangerousGoodsClassDivisions getClassDivision();
	String getValue();
	
	Boolean validate();
}
