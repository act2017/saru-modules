/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface LabelPreferenceType {
	/**
	 * 
	 * The builder for {@link LabelPreferenceType}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<LabelPreferenceType> {
		Builder setValue(String type);
	}

	void setValue(String type);	
	String getValue();
	
	Boolean validate();
}
