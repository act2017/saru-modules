/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean;

import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.GetLabelResponse}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class GetLabelResponse implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.GetLabelResponse
										  , IsSerializable {
	public static final String	ATTR_LABELS		= "labels";
	
	/**
	 * 
	 * The object builder for {@link GetLabelResponse}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.GetLabelResponse.Builder {
		private GetLabelResponse result = null;

		private Builder() {
			this.result = new GetLabelResponse();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.GetLabelResponse.Builder#addLabel(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label)
		 */
		@Override
		public Builder addLabel(Label label) {
			this.result.addLabel(label);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.GetLabelResponse.Builder#setLabels(java.util.List)
		 */
		@Override
		public Builder setLabels(List<Label> labels) {
			this.result.setLabels(labels);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.GetLabelResponse.Builder#setLabels(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection)
		 */
		@Override
		public Builder setLabels(LabelCollection labels) {
			this.result.setLabels(labels);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public GetLabelResponse build() {
			return this.result;
		}

	}
	
	private LabelCollection					labels	= null;
	
	/**
	 * 
	 */
	private GetLabelResponse() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.GetLabelResponse#addLabel(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label)
	 */
	@Override
	public void addLabel(Label label) {
		if(null == label) {
			return;
		}else if(null == this.labels) {
			this.labels = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelCollection.builder().build();
		}

		this.labels.addLabel(label);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.GetLabelResponse#setLabels(java.util.List)
	 */
	@Override
	public void setLabels(List<Label> labels) {
		for (Label label : labels) {
			this.addLabel(label);
		}

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.GetLabelResponse#setLabels(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection)
	 */
	@Override
	public void setLabels(LabelCollection labels) {
		this.labels = labels;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.GetLabelResponse#getLabel(java.lang.Integer)
	 */
	@Override
	public Label getLabel(Integer index) {
		if(null == this.labels) {
			return null;
		}
		
		return this.labels.getLabel(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.GetLabelResponse#getLabels()
	 */
	@Override
	public LabelCollection getLabels() {
		
		return this.labels;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.GetLabelResponse#validate()
	 */
	@Override
	public Boolean validate() {		
		if(null != this.labels && !this.labels.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
