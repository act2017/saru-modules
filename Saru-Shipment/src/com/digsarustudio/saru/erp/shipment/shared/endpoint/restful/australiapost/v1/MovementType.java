/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.MovementTypes;

/**
 * Use "DESPATCH" to indicate an outbound or normal shipment (this is the default), 
 * "RETURN" to indicate a returns shipment, "TRANSFER" to indicate a transfer. 
 * "TRANSFER" is only available for StarTrack products.
 * <br>
 * The details might be returned from database.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 */
public interface MovementType {
	/**
	 * 
	 * The builder for {@link MovementType}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<MovementType> {
		Builder setType(MovementTypes type);
		Builder setType(String type);
	}
	
	void setType(MovementTypes type);
	void setType(String type);
	
	String getType();
	MovementTypes getMovementType();
	
	Boolean validate();
}
