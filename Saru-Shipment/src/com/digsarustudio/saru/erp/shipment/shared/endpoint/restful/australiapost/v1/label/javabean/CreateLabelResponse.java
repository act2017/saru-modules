/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean;

import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelGenerationProgressCode;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelGenerationProgressMessage;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class CreateLabelResponse implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse
										  , IsSerializable {
	public static final String	ATTR_MESSAGE	= "message";
	public static final String	ATTR_CODE		= "code";
	public static final String	ATTR_LABELS		= "labels";
	public static final String	ATTR_ERRORS		= "errors";
	
	/**
	 * 
	 * The object builder for {@link CreateLabelResponse}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse.Builder {
		private CreateLabelResponse result = null;

		private Builder() {
			this.result = new CreateLabelResponse();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse.Builder#setGenerationProgressMessage(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelGenerationProgressMessage)
		 */
		@Override
		public Builder setGenerationProgressMessage(LabelGenerationProgressMessage msg) {
			this.result.setGenerationProgressMessage(msg);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse.Builder#setGenerationProgressCode(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelGenerationProgressCode)
		 */
		@Override
		public Builder setGenerationProgressCode(LabelGenerationProgressCode code) {
			this.result.setGenerationProgressCode(code);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse.Builder#addLabel(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label)
		 */
		@Override
		public Builder addLabel(Label label) {
			this.result.addLabel(label);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse.Builder#setLabels(java.util.List)
		 */
		@Override
		public Builder setLabels(List<Label> labels) {
			this.result.setLabels(labels);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse.Builder#setLabels(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection)
		 */
		@Override
		public Builder setLabels(LabelCollection labels) {
			this.result.setLabels(labels);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse.Builder#setAusPostErrors(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection)
		 */
		@Override
		public Builder setAusPostErrors(AusPostErrorCollection errors) {
			this.result.setAusPostErrors(errors);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse.Builder#setAusPostErrors(java.util.List)
		 */
		@Override
		public Builder setAusPostErrors(List<AusPostError> errors) {
			this.result.setAusPostErrors(errors);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse.Builder#addAusPostError(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError)
		 */
		@Override
		public Builder addAusPostError(AusPostError error) {
			this.result.addAusPostError(error);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public CreateLabelResponse build() {
			return this.result;
		}

	}
	
	private LabelGenerationProgressMessage	message	= null;
	private LabelGenerationProgressCode		code	= null;
	private LabelCollection					labels	= null;
	private AusPostErrorCollection			errors	= null;
	
	/**
	 * 
	 */
	private CreateLabelResponse() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse#setGenerationProgressMessage(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelGenerationProgressMessage)
	 */
	@Override
	public void setGenerationProgressMessage(LabelGenerationProgressMessage msg) {
		this.message = msg;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse#setGenerationProgressCode(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelGenerationProgressCode)
	 */
	@Override
	public void setGenerationProgressCode(LabelGenerationProgressCode code) {
		this.code = code;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse#addLabel(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label)
	 */
	@Override
	public void addLabel(Label label) {
		if(null == label) {
			return;
		}else if(null == this.labels) {
			this.labels = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean.LabelCollection.builder().build();
		}

		this.labels.addLabel(label);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse#setLabels(java.util.List)
	 */
	@Override
	public void setLabels(List<Label> labels) {
		for (Label label : labels) {
			this.addLabel(label);
		}

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse#setLabels(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection)
	 */
	@Override
	public void setLabels(LabelCollection labels) {
		this.labels = labels;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse#setAusPostErrors(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection)
	 */
	@Override
	public void setAusPostErrors(AusPostErrorCollection errors) {
		this.errors = errors;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse#setAusPostErrors(java.util.List)
	 */
	@Override
	public void setAusPostErrors(List<AusPostError> errors) {
		if(null == errors) {
			return;
		}
		
		for (AusPostError error : errors) {
			this.addAusPostError(error);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse#addAusPostError(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError)
	 */
	@Override
	public void addAusPostError(AusPostError error) {
		if(null == error) {
			return;
		}else if(null == this.errors) {
			this.errors = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorCollection.builder().build();
		}

		this.errors.addError(error);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse#getGenerationProgressMessage()
	 */
	@Override
	public LabelGenerationProgressMessage getGenerationProgressMessage() {
		
		return this.message;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse#getGenerationProgressCode()
	 */
	@Override
	public LabelGenerationProgressCode getGenerationProgressCode() {
		
		return this.code;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse#getLabel(java.lang.Integer)
	 */
	@Override
	public Label getLabel(Integer index) {
		if(null == this.labels) {
			return null;
		}
		
		return this.labels.getLabel(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse#getLabels()
	 */
	@Override
	public LabelCollection getLabels() {
		
		return this.labels;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse#getAusPostErrors()
	 */
	@Override
	public AusPostErrorCollection getAusPostErrors() {
		
		return this.errors;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.CreateLabelResponse#validate()
	 */
	@Override
	public Boolean validate() {
		if(null != this.message && !this.message.validate()) {
			return false;
		}
		
		if(null != this.code && !this.code.validate()) {
			return false;
		}
		
		if(null != this.labels && !this.labels.validate()) {
			return false;
		}
		
		if(null != this.errors && !this.errors.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
