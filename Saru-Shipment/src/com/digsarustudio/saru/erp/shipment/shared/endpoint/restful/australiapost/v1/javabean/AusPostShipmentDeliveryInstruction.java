/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.shipment.shared.DeliveryInstruction;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The implementation of {@link DeliveryInstruction} for Australia Post API to use.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AusPostShipmentDeliveryInstruction implements DeliveryInstruction, IsSerializable {
	public static final Integer	MAX_LEN	= 128;
	
	/**
	 * 
	 * The object builder for {@link AusPostShipmentDeliveryInstruction}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements DeliveryInstruction.Builder {
		private AusPostShipmentDeliveryInstruction result = null;

		private Builder() {
			this.result = new AusPostShipmentDeliveryInstruction();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.DeliveryInstruction.Builder#setInstruction(java.lang.String)
		 */
		@Override
		public Builder setInstruction(String instruction) {
			this.result.setInstruction(instruction);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AusPostShipmentDeliveryInstruction build() {
			return this.result;
		}

	}

	private String insturction = null;
	
	/**
	 * 
	 */
	private AusPostShipmentDeliveryInstruction() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.DeliveryInstruction#setInstruction(java.lang.String)
	 */
	@Override
	public void setInstruction(String instruction) {
		this.insturction = instruction;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.DeliveryInstruction#getInstruction()
	 */
	@Override
	public String getInstruction() {
		
		return this.insturction;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.DeliveryInstruction#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.insturction || this.insturction.isEmpty()) {
			return false;
		}
		
		if(MAX_LEN < this.insturction.length()) {
			return false;
		}
		
		return true;
	}
	
	public static AusPostShipmentDeliveryInstruction copy(DeliveryInstruction source) {
		AusPostShipmentDeliveryInstruction.Builder builder = AusPostShipmentDeliveryInstruction.builder();
		
		builder.setInstruction(source.getInstruction());
		
		return builder.build();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
