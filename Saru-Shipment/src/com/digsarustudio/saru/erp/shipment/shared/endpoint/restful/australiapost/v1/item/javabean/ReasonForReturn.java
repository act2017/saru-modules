/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ReasonForReturn}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ReasonForReturn implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ReasonForReturn
									  , IsSerializable {
	public static final Integer	MAX_LEN	= 60;
	
	/**
	 * 
	 * The object builder for {@link ReasonForReturn}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ReasonForReturn.Builder {
		private ReasonForReturn result = null;

		private Builder() {
			this.result = new ReasonForReturn();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ReasonForReturn.Builder#setReason(java.lang.String)
		 */
		@Override
		public Builder setReason(String reason) {
			this.result.setReason(reason);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ReasonForReturn build() {
			return this.result;
		}

	}
	
	private String reason	= null;
	
	/**
	 * 
	 */
	private ReasonForReturn() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ReasonForReturn#setReason(java.lang.String)
	 */
	@Override
	public void setReason(String reason) {
		this.reason = reason;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ReasonForReturn#getReason()
	 */
	@Override
	public String getReason() {
		
		return this.reason;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ReasonForReturn#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.reason || this.reason.isEmpty()) {
			return false;
		}
		
		if( MAX_LEN < this.reason.length() ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
