/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.LabelGenerationRequestStatuses;

/**
 * The status of the label generation request. Statuses returned will be:<br>
 * 	Pending: Labels are being generated<br>
 * 	Error: Label generation failed<br>
 * 	Available: Label generation has completed<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface LabelGenerationRequestStatus {
	/**
	 * 
	 * The builder for {@link LabelGenerationRequestStatus}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<LabelGenerationRequestStatus> {
		Builder setValue(String value);
		Builder setValue(LabelGenerationRequestStatuses value);
	}
	
	void setValue(String value);
	void setValue(LabelGenerationRequestStatuses value);
	
	String getValue();
	LabelGenerationRequestStatuses getLabelStatus();
	
	Boolean validate();
}
