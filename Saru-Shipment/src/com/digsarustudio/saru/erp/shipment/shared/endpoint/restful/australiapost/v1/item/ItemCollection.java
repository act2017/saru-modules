/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * An item element is to be supplied for each parcel.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface ItemCollection {
	/**
	 * 
	 * The builder for {@link ItemCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemCollection> {
		Builder addItem(Item item);
		Builder setItems(List<Item> items);
		Builder setItems(Item[] items);
	}
	
	void addItem(Item item);
	void setItems(List<Item> items);
	void setItems(Item[] items);
	
	Item getItem(Integer index);
	List<Item> getItems();
	
	Boolean validate();
}
