/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.PackagingTypes;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackagingType}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PackagingType implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackagingType
									, IsSerializable {
	public static final Integer	MAX_LEN	= 3;
	
	/**
	 * 
	 * The object builder for {@link PackagingType}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackagingType.Builder {
		private PackagingType result = null;

		private Builder() {
			this.result = new PackagingType();
		}

		
		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackagingType.Builder#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.PackagingTypes)
		 */
		@Override
		public Builder setType(PackagingTypes type) {
			this.result.setType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackagingType.Builder#setType(java.lang.String)
		 */
		@Override
		public Builder setType(String type) {
			this.result.setType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public PackagingType build() {
			return this.result;
		}

	}
	
	private PackagingTypes	type	= null;
	
	/**
	 * 
	 */
	private PackagingType() { 
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackagingType#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.PackagingTypes)
	 */
	@Override
	public void setType(PackagingTypes type) {
		this.type = type;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackagingType#setType(java.lang.String)
	 */
	@Override
	public void setType(String type) {
		this.type = PackagingTypes.fromValue(type);

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackagingType#getType()
	 */
	@Override
	public String getType() {
		
		return this.type.getValue();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackagingType#getPackagingType()
	 */
	@Override
	public PackagingTypes getPackagingType() {
		
		return this.type;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.PackagingType#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.type ) {
			return false;
		}
		
		String typeValue = this.type.getValue();
		
		if(null == typeValue || typeValue.isEmpty()) {
			return false;
		}
		
		if(MAX_LEN < typeValue.length()) {
			return false;
		}
		
		return null;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
