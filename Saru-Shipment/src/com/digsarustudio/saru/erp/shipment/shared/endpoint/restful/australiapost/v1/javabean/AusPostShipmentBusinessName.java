/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The implementation of {@link BusinessName} for {@link Shipment} to used by Australia Post shipping API.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AusPostShipmentBusinessName implements BusinessName, IsSerializable {
	public static final Integer MAX_LEN	= 40;
	
	/**
	 * 
	 * The object builder for {@link AusPostShipmentBusinessName}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements BusinessName.Builder {
		private AusPostShipmentBusinessName result = null;

		private Builder() {
			this.result = new AusPostShipmentBusinessName();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName.Builder#setName(java.lang.String)
		 */
		@Override
		public Builder setName(String name) {
			this.result.setName(name);			
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName.Builder#setAbbreviation(java.lang.String)
		 */
		@Override
		public Builder setAbbreviation(String name) {
			this.result.setAbbreviation(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.Identity.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AusPostShipmentBusinessName build() {
			return this.result;
		}

	}
	
	private String name	= null;

	/**
	 * 
	 */
	public AusPostShipmentBusinessName() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName#setAbbreviation(java.lang.String)
	 */
	@Override
	public void setAbbreviation(String name) {
		String msg = this.getClass().getName() + " doesn't support #setAbbreviation().";
		throw new UnsupportedOperationException(msg);

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName#getName()
	 */
	@Override
	public String getName() {
		
		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName#getAbbreviation()
	 */
	@Override
	public String getAbbreviation() {
		String msg = this.getClass().getName() + " doesn't support #getAbbreviation().";
		throw new UnsupportedOperationException(msg);
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.Identity#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.setName(value);
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.Identity#getValue()
	 */
	@Override
	public String getValue() {
		return this.getName();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.name || this.name.isEmpty()) {
			return false;
		}
		
		if( MAX_LEN < this.name.length() ) {
			return false;
		}
		
		return true;
	}

	public AusPostShipmentBusinessName copy(BusinessName name) {
		AusPostShipmentBusinessName.Builder builder = AusPostShipmentBusinessName.builder();
		
		builder.setValue(name.getValue());
		
		return builder.build();		
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
