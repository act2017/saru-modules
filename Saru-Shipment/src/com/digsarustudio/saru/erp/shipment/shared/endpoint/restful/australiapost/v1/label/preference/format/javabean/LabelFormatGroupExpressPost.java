/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelFormatGroups;

/**
 * This class represents the label format - Express Post.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelFormatGroupExpressPost extends LabelFormatGroup {
	public static final LabelFormatLayout	A4_3pp				= new LabelFormatLayoutA4_3pp();
	public static final LabelFormatLayout	A4_1pp				= new LabelFormatLayoutA4_1pp();
	public static final LabelFormatLayout	ThermalLableA6_1pp	= new LabelFormatLayoutThermalLabel_A6_1pp();
	
	public LabelFormatGroupExpressPost() {
		this.setValue(LabelFormatGroups.ExpressPost);
	}
}
