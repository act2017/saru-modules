/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.CountryOfOrigin;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentDescription;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentQuantity;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentValue;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentWeight;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.TariffCode;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemContent implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent
								  , IsSerializable {
	/**
	 * [Mandatory] .<br>
	 * Type: String
	 */
	public static final String	ATTR_DESCRIPTION		= "description";
	
	/**
	 * [Mandatory] .<br>
	 * Type: Integer
	 */
	public static final String	ATTR_QUANTITY			= "quantity";
	
	/**
	 * [Mandatory] .<br>
	 * Type: Decimal [N, 3]
	 */
	public static final String	ATTR_WEIGHT				= "weight";
	
	/**
	 * [Mandatory] .<br>
	 * Type: Decimal [4, 2]
	 */
	public static final String	ATTR_VALUE				= "value";
	
	/**
	 * [Mandatory] when commercial_value is "true".<br>
	 * 
	 * <br>
	 * Type: Integer
	 */
	public static final String	ATTR_TARRIFF_CODE		= "tariff_code";
	
	/**
	 * [Mandatory] when commercial_value is "true".<br>
	 * 
	 * <br>
	 * Type: String
	 */
	public static final String	ATTR_COUNTRY_OF_ORIGIN	= "country_of_origin";

	/**
	 * 
	 * The object builder for {@link ItemContent}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent.Builder {
		private ItemContent result = null;

		private Builder() {
			this.result = new ItemContent();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent.Builder#setDescription(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentDescription)
		 */
		@Override
		public Builder setDescription(ItemContentDescription description) {
			this.result.setDescription(description);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent.Builder#setQuantity(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentQuantity)
		 */
		@Override
		public Builder setQuantity(ItemContentQuantity quantity) {
			this.result.setQuantity(quantity);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent.Builder#setWeight(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentWeight)
		 */
		@Override
		public Builder setWeight(ItemContentWeight weight) {
			this.result.setWeight(weight);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent.Builder#setValue(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentValue)
		 */
		@Override
		public Builder setValue(ItemContentValue value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent.Builder#setTariffCode(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.TariffCode)
		 */
		@Override
		public Builder setTariffCode(TariffCode code) {
			this.result.setTariffCode(code);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent.Builder#setCountryOfOrigin(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.CountryOfOrigin)
		 */
		@Override
		public Builder setCountryOfOrigin(CountryOfOrigin country) {
			this.result.setCountryOfOrigin(country);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent.Builder#setParent(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item)
		 */
		@Override
		public Builder setParent(Item parent) {
			this.result.setParent(parent);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemContent build() {
			return this.result;
		}

	}
	
	private ItemContentDescription	description			= null;
	private ItemContentQuantity		quantity			= null;
	private	ItemContentWeight		weight				= null;
	private	ItemContentValue		value				= null;
	private	TariffCode				tariff_code			= null;
	private	CountryOfOrigin			contry_of_origin	= null;
	
	private Item					parent				= null;
	
	/**
	 * 
	 */
	private ItemContent() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent#setDescription(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentDescription)
	 */
	@Override
	public void setDescription(ItemContentDescription description) {
		this.description = description;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent#getDescription()
	 */
	@Override
	public ItemContentDescription getDescription() {

		return this.description;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent#setQuantity(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentQuantity)
	 */
	@Override
	public void setQuantity(ItemContentQuantity quantity) {
		this.quantity = quantity;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent#getQuantity()
	 */
	@Override
	public ItemContentQuantity getQuantity() {

		return this.quantity;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent#setWeight(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentWeight)
	 */
	@Override
	public void setWeight(ItemContentWeight weight) {
		this.weight = weight;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent#getWeight()
	 */
	@Override
	public ItemContentWeight getWeight() {

		return this.weight;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent#setValue(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentValue)
	 */
	@Override
	public void setValue(ItemContentValue value) {
		this.value = value;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent#getValue()
	 */
	@Override
	public ItemContentValue getValue() {
		return this.value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent#setTariffCode(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TariffCode)
	 */
	@Override
	public void setTariffCode(TariffCode code) {
		this.tariff_code = code;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent#getTariffCode()
	 */
	@Override
	public TariffCode getTariffCode() {
		
		return this.tariff_code;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent#setCountryOfOrigin(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.CountryOfOrigin)
	 */
	@Override
	public void setCountryOfOrigin(CountryOfOrigin country) {
		this.contry_of_origin = country;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent#getCountryOfOrigin()
	 */
	@Override
	public CountryOfOrigin getCountryOfOrigin() {
		
		return this.contry_of_origin;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent#setParent(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item)
	 */
	@Override
	public void setParent(Item parent) {
		this.parent = parent;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent#getParent()
	 */
	@Override
	public Item getParent() {
		return this.parent;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemContent#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.description || !this.description.validate()) {
			return false;
		}
		
		if(null == this.quantity || !this.quantity.validate()) {
			return false;
		}
		
		if(null == this.weight || !this.weight.validate()) {
			return false;
		}
		
		if(null == this.value || !this.value.validate()) {
			return false;
		}
		
		if(null != this.parent && this.parent.hasComercialValue()) {
			if(null == this.tariff_code || !this.tariff_code.validate()) {
				return false;
			}
			
			if(null == this.contry_of_origin || !this.contry_of_origin.validate()) {
				return false;
			}
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
