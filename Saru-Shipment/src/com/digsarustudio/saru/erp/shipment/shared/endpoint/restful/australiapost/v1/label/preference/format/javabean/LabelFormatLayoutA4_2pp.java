/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelFormatLayouts;

/**
 * The label layout printed in A4 paper and 2 printed per page.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelFormatLayoutA4_2pp extends LabelFormatLayout {

	/**
	 * 
	 */
	public LabelFormatLayoutA4_2pp() {
		this.setLayout(LabelFormatLayouts.A4_2pp);
	}

}
