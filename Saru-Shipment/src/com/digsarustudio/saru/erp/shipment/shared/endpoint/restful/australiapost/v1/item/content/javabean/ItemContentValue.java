/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentValue}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemContentValue implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentValue
									   , IsSerializable {
	public static final Integer	MAX_LEN_MAIN		= 4;
	public static final Integer	MAX_LEN_PRECISION	= 2;
	public static final String	DECIMAL_DELIMITER	= "\\.";
	public static final String	DECIMAL_GLUE		= ".";
	
	/**
	 * 
	 * The object builder for {@link ItemContentValue}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentValue.Builder {
		private ItemContentValue result = null;

		private Builder() {
			this.result = new ItemContentValue();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentValue.Builder#setValue(java.lang.Integer)
		 */
		@Override
		public Builder setValue(Integer value) {
			this.result.setValue(value);
			return null;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentValue.Builder#setValue(java.lang.Integer, java.lang.Integer)
		 */
		@Override
		public Builder setValue(Integer value, Integer precision) {
			this.result.setValue(value, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentValue.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentValue.Builder#setValue(java.lang.String, java.lang.String)
		 */
		@Override
		public Builder setValue(String value, String precision) {
			this.result.setValue(value, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemContentValue build() {
			return this.result;
		}

	}
	
	private String main 		= null;
	private String precision	= null;
	
	/**
	 * 
	 */
	private ItemContentValue() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentValue#setValue(java.lang.Integer)
	 */
	@Override
	public void setValue(Integer value) {
		this.setValue(value, null);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentValue#setValue(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void setValue(Integer value, Integer precision) {
		String incoming = (null == value) ? null : value.toString();
		String pre		= (null == precision) ? null : precision.toString();
		
		this.setValue(incoming, pre);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentValue#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		if(null == value) {
			return;
		}
		
		String[] weights = value.split(DECIMAL_DELIMITER);
		
		String main = weights[0];
		String pre	= null;
		
		if(2 == weights.length) {
			pre = weights[1];
		}
		
		this.setValue(main, pre);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentValue#setValue(java.lang.String, java.lang.String)
	 */
	@Override
	public void setValue(String value, String precision) {
		this.main = value;
		this.precision = precision;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentValue#getValue()
	 */
	@Override
	public String getValue() {
		
		return this.main;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentValue#getPrecision()
	 */
	@Override
	public String getPrecision() {
		
		return this.precision;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentValue#getDecimalString()
	 */
	@Override
	public String getDecimalString() {
		Boolean hasMain = (null != this.main && !this.main.isEmpty());
		Boolean hasPre	= (null != this.precision && !this.precision.isEmpty());
		
		if( !hasMain && !hasPre ) {
			return null;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		if(!hasMain) {
			buffer.append("0");
		}else {
			buffer.append(this.main);
		}
		
		if(hasPre) {
			buffer.append(DECIMAL_GLUE);
			buffer.append(this.precision);
		}
		
		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentValue#validate()
	 */
	@Override
	public Boolean validate() {
		Boolean hasMain = (null != this.main && !this.main.isEmpty());
		Boolean hasPre	= (null != this.precision && !this.precision.isEmpty());
		
		if( !hasMain && !hasPre) {
			return false;
		}
		
		if( hasMain && MAX_LEN_MAIN < this.main.length() ) {
			return false;
		}
		
		if( hasPre && MAX_LEN_PRECISION < this.precision.length()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
