/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The identifier for the request, generated and returned by the Create Labels service.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface LabelRequestId {
	/**
	 * 
	 * The builder for {@link LabelRequestId}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<LabelRequestId> {
		Builder setValue(String value);
	}
	
	void setValue(String value);	
	String getValue();
	
	Boolean validate();
}
