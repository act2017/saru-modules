/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Country;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AddressType;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class Sender implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender
							 , IsSerializable {
	/**
	 * [Mandatory] .<br>
	 * Type: String
	 */
	public static final String	ATTR_NAME		= "name";

	/**
	 * [Optional] .<br>
	 * Type: String
	 */
	public static final String	ATTR_TYPE		= "type";

	/**
	 * [Mandatory] .<br>
	 * Type: String
	 */
	public static final String	ATTR_LINES		= "lines";

	/**
	 * [Mandatory] .<br>
	 * Type: String
	 */
	public static final String	ATTR_SUBURB		= "suburb";

	/**
	 * [Mandatory] .<br>
	 * Type: String
	 */
	public static final String	ATTR_STATE		= "state";

	/**
	 * [Mandatory] .<br>
	 * Type: String
	 */
	public static final String	ATTR_POSTCODE	= "postcode";

	/**
	 * [Optional] .<br>
	 * Type: String
	 */
	public static final String	ATTR_COUNTRY	= "country";

	/**
	 * [Optional] .<br>
	 * Type: String
	 */
	public static final String	ATTR_PHONE		= "phone";

	/**
	 * [Optional] .<br>
	 * Type: String
	 */
	public static final String	ATTR_EMAIL		= "email";

	/**
	 * 
	 * The object builder for {@link Sender}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender.Builder {
		private Sender result = null;

		private Builder() {
			this.result = new Sender();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender.Builder#setName(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName)
		 */
		@Override
		public Builder setName(PersonalName name) {
			this.result.setName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender.Builder#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AddressType)
		 */
		@Override
		public Builder setType(AddressType type) {
			this.result.setType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender.Builder#setAddressLines(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines)
		 */
		@Override
		public Builder setAddressLines(AddressLines addresslines) {
			this.result.setAddressLines(addresslines);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender.Builder#setSuburb(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb)
		 */
		@Override
		public Builder setSuburb(Suburb suburb) {
			this.result.setSuburb(suburb);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender.Builder#setState(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State)
		 */
		@Override
		public Builder setState(State state) {
			this.result.setState(state);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender.Builder#setPostcode(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode)
		 */
		@Override
		public Builder setPostcode(Postcode postcode) {
			this.result.setPostcode(postcode);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender.Builder#setCountry(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Country)
		 */
		@Override
		public Builder setCountry(Country country) {
			this.result.setCountry(country);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender.Builder#setPhone(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber)
		 */
		@Override
		public Builder setPhone(PhoneNumber number) {
			this.result.setPhone(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender.Builder#setEmail(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress)
		 */
		@Override
		public Builder setEmail(EmailAddress email) {
			this.result.setEmail(email);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Sender build() {
			return this.result;
		}

	}
	
	private PersonalName	name	= null;
	private	AddressType		type	= null;
	private AddressLines	lines	= null;
	private	Suburb			suburb	= null;
	private	State			state	= null;
	private	Postcode		postcode= null;
	private Country			country	= null;
	private	PhoneNumber		phone	= null;
	private EmailAddress	email	= null;
	
	/**
	 * 
	 */
	private Sender() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender#setName(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName)
	 */
	@Override
	public void setName(PersonalName name) {
		this.name = name;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender#getName()
	 */
	@Override
	public PersonalName getName() {
		
		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AddressType)
	 */
	@Override
	public void setType(AddressType type) {
		this.type = type;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender#getType()
	 */
	@Override
	public AddressType getType() {
		
		return this.type;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender#setAddressLines(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines)
	 */
	@Override
	public void setAddressLines(AddressLines addresslines) {
		this.lines = addresslines;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender#getAddressLines()
	 */
	@Override
	public AddressLines getAddressLines() {
		
		return this.lines;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender#setSuburb(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb)
	 */
	@Override
	public void setSuburb(Suburb suburb) {
		this.suburb = suburb;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender#getSuburb()
	 */
	@Override
	public Suburb getSuburb() {
		
		return this.suburb;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender#setState(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State)
	 */
	@Override
	public void setState(State state) {
		this.state = state;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender#getState()
	 */
	@Override
	public State getState() {
		
		return this.state;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender#setPostcode(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode)
	 */
	@Override
	public void setPostcode(Postcode postcode) {
		this.postcode = postcode;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender#getPostcode()
	 */
	@Override
	public Postcode getPostcode() {
		
		return this.postcode;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender#setCountry(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Country)
	 */
	@Override
	public void setCountry(Country country) {
		this.country = country;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender#getCountry()
	 */
	@Override
	public Country getCountry() {
		
		return this.country;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender#setPhone(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber)
	 */
	@Override
	public void setPhone(PhoneNumber number) {
		this.phone = number;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender#getPhone()
	 */
	@Override
	public PhoneNumber getPhone() {
		
		return this.phone;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender#setEmail(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress)
	 */
	@Override
	public void setEmail(EmailAddress email) {
		this.email = email;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender#getEmail()
	 */
	@Override
	public EmailAddress getEmail() {
		
		return this.email;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.name || !this.name.validate()) {
			return false;
		}
		
		if(null != this.type && !this.type.validate()) {
			return false;
		}
		
		if(null == this.lines || !this.lines.validate()) {
			return false;
		}
		
		if(null == this.suburb || !this.suburb.validate()) {
			return false;
		}
		
		if(null == this.state || !this.state.validate()) {
			return false;
		}
		
		if(null == this.postcode || !this.postcode.validate()) {
			return false;
		}
		
		if(null != this.country && !this.country.validate()) {
			return false;
		}
		
		if(null != this.phone && !this.phone.validate()) {
			return false;
		}
		
		if(null != this.email && !this.email.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
