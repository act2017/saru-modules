/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FreightCharge}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class FreightCharge implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FreightCharge
								, IsSerializable {
	public static final Integer	MAX_LEN_PRECISION	= 2;
	public static final String	DECIMAL_DELIMITER	= "\\.";
	public static final String	DECIMAL_GLUE		= ".";
	
	/**
	 * 
	 * The object builder for {@link FreightCharge}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FreightCharge.Builder {
		private FreightCharge result = null;

		private Builder() {
			this.result = new FreightCharge();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FreightCharge.Builder#setValue(java.lang.Integer)
		 */
		@Override
		public Builder setValue(Integer charge) {
			this.result.setValue(charge);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FreightCharge.Builder#setValue(java.lang.Integer, java.lang.Integer)
		 */
		@Override
		public Builder setValue(Integer charge, Integer precision) {
			this.result.setValue(charge, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FreightCharge.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String charge) {
			this.result.setValue(charge);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FreightCharge.Builder#setValue(java.lang.String, java.lang.String)
		 */
		@Override
		public Builder setValue(String charge, String precision) {
			this.result.setValue(charge, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public FreightCharge build() {
			return this.result;
		}

	}
	
	private String main			= null;
	private String precision	= null;
	
	/**
	 * 
	 */
	private FreightCharge() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FreightCharge#setValue(java.lang.Integer)
	 */
	@Override
	public void setValue(Integer charge) {
		this.setValue(charge, null);

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FreightCharge#setValue(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void setValue(Integer charge, Integer precision) {
		String main = (null == charge) ? null : charge.toString();
		String pre	= (null == precision) ? null : precision.toString();

		this.setValue(main, pre);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FreightCharge#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String charge) {
		if(null == charge) {
			return;
		}

		String[] values = charge.split(DECIMAL_DELIMITER);
		
		String main = values[0];
		String pre	= null;
		
		if(2 == values.length) {
			pre = values[1];
		}
		
		this.setValue(main, pre);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FreightCharge#setValue(java.lang.String, java.lang.String)
	 */
	@Override
	public void setValue(String charge, String precision) {
		this.main 		= charge;
		this.precision	= precision;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FreightCharge#getCharge()
	 */
	@Override
	public String getCharge() {
		return this.main;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FreightCharge#getPrcision()
	 */
	@Override
	public String getPrcision() {
		return this.precision;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FreightCharge#getDecimalCharge()
	 */
	@Override
	public String getDecimalCharge() {
		if( !this.hasMain() && !this.hasPrecision() ) {
			return null;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		if( !this.hasMain() ) {
			buffer.append("0");
		}else {
			buffer.append(this.main);
		}
		
		if( this.hasPrecision() ) {
			buffer.append(DECIMAL_GLUE);
			buffer.append(this.precision);
		}
		
		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.FreightCharge#validate()
	 */
	@Override
	public Boolean validate() {
		if( !this.hasMain() && !this.hasPrecision() ) {
			return false;
		}
		
		if( this.hasPrecision() && MAX_LEN_PRECISION < this.precision.length() ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	private Boolean hasMain() {
		return (null != this.main && !this.main.isEmpty());
	}
	
	private Boolean hasPrecision() {
		return (null != this.precision && !this.precision.isEmpty());
	}
}
