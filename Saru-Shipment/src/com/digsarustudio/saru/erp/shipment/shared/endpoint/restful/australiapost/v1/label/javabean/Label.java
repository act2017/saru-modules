/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean;

import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelGenerationRequestStatus;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelRequestId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelURL;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class Label implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label
							, IsSerializable {
	//For create label
	public static final String	ATTR_REQUEST_ID				= "request_id";
	public static final String	ATTR_REQUEST_DATE			= "request_date";
	public static final String	ATTR_SHIPMENTS				= "shipments";
	
	//For get label
	public static final String	ATTR_URL					= "url";
	public static final String	ATTR_URL_CREATION_DATE		= "url_creation_date";
	
	//For get shipment
	public static final String	ATTR_LABEL_URL				= "label_url";
	public static final String	ATTR_STATUS					= "status";
	public static final String	ATTR_LABEL_CREATION_DATE	= "label_creation_date";
	public static final String	ATTR_LABEL_ERRORS			= "errors";
	
	/**
	 * 
	 * The object builder for {@link Label}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label.Builder {
		private Label result = null;

		private Builder() {
			this.result = new Label();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Label.Builder#setPrintableURL(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.LabelURL)
		 */
		@Override
		public Builder setPrintableURL(LabelURL url) {
			this.result.setPrintableURL(url);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Label.Builder#setStatus(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.LabelStatus)
		 */
		@Override
		public Builder setStatus(LabelGenerationRequestStatus status) {
			this.result.setStatus(status);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Label.Builder#setCreationDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime)
		 */
		@Override
		public Builder setCreationDate(DateTime date) {
			this.result.setCreationDate(date);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Label.Builder#setErrors(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection)
		 */
		@Override
		public Builder setErrors(AusPostErrorCollection errors) {
			this.result.setErrors(errors);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label.Builder#setRequestId(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelRequestId)
		 */
		@Override
		public Builder setRequestId(LabelRequestId id) {
			this.result.setRequestId(id);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label.Builder#setRequestDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime)
		 */
		@Override
		public Builder setRequestDate(DateTime date) {
			this.result.setRequestDate(date);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label.Builder#addShipment(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment)
		 */
		@Override
		public Builder addShipment(Shipment shipment) {
			this.result.addShipment(shipment);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label.Builder#setShipments(java.util.List)
		 */
		@Override
		public Builder setShipments(List<Shipment> shipments) {
			this.result.setShipments(shipments);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label.Builder#setShipments(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection)
		 */
		@Override
		public Builder setShipments(ShipmentCollection shipments) {
			this.result.setShipments(shipments);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label.Builder#setURL(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelURL)
		 */
		@Override
		public Builder setURL(LabelURL url) {
			this.result.setURL(url);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label.Builder#setURLCreationDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime)
		 */
		@Override
		public Builder setURLCreationDate(DateTime date) {
			this.result.setURLCreationDate(date);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label.Builder#setContnent(java.lang.String)
		 */
		@Override
		public Builder setContnent(String content) {
			this.result.setContnent(content);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Label build() {
			return this.result;
		}

	}
	
	private	LabelRequestId					request_id			= null;
	private	DateTime						request_date		= null;
	private ShipmentCollection				shipments			= null;
	
	private	LabelURL						url					= null;
	private DateTime						url_creation_date	= null;
	
	private LabelURL						label_url			= null;
	private LabelGenerationRequestStatus	status				= null;
	private	DateTime						label_creation_date	= null;
	private	AusPostErrorCollection			errors				= null;		
	
	private String							labelContent		= null;

	/**
	 * 
	 */
	private Label() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Label#setPrintableURL(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.LabelURL)
	 */
	@Override
	public void setPrintableURL(LabelURL url) {
		this.label_url = url;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Label#setStatus(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.LabelStatus)
	 */
	@Override
	public void setStatus(LabelGenerationRequestStatus status) {
		this.status = status;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Label#setCreationDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime)
	 */
	@Override
	public void setCreationDate(DateTime date) {
		this.label_creation_date	 = date;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Label#getPrintableURL()
	 */
	@Override
	public LabelURL getPrintableURL() {

		return this.label_url;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Label#getStatus()
	 */
	@Override
	public LabelGenerationRequestStatus getStatus() {

		return this.status;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Label#getRequestDate()
	 */
	@Override
	public DateTime getCreationDate() {

		return this.label_creation_date	;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Label#getErrors()
	 */
	@Override
	public AusPostErrorCollection getErrors() {

		return this.errors;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label#setRequestId(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelRequestId)
	 */
	@Override
	public void setRequestId(LabelRequestId id) {
		this.request_id = id;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label#setRequestDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime)
	 */
	@Override
	public void setRequestDate(DateTime date) {
		this.request_date = date;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label#addShipment(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment)
	 */
	@Override
	public void addShipment(Shipment shipment) {
		if(null == shipment) {
			return;
		}else if(null == this.shipments){
			this.shipments = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.ShipmentCollection.builder().build();
		}
		
		this.shipments.addShipment(shipment);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label#setShipments(java.util.List)
	 */
	@Override
	public void setShipments(List<Shipment> shipments) {
		for (Shipment shipment : shipments) {
			this.addShipment(shipment);
		}
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label#setShipments(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection)
	 */
	@Override
	public void setShipments(ShipmentCollection shipments) {
		this.shipments = shipments;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label#setURL(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelURL)
	 */
	@Override
	public void setURL(LabelURL url) {
		this.url = url;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label#setURLCreationDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime)
	 */
	@Override
	public void setURLCreationDate(DateTime date) {
		this.url_creation_date = date;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label#getRequestId()
	 */
	@Override
	public LabelRequestId getRequestId() {
		return this.request_id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label#getRequestDate()
	 */
	@Override
	public DateTime getRequestDate() {
		return this.request_date;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label#getShipment(java.lang.Integer)
	 */
	@Override
	public Shipment getShipment(Integer index) {
		if(null == this.shipments || null == index || index >= this.shipments.size()) {
			return null;
		}
		
		return this.shipments.getShipment(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label#getShipments()
	 */
	@Override
	public ShipmentCollection getShipments() {
		return this.shipments;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label#getURL()
	 */
	@Override
	public LabelURL getURL() {
		return this.url;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label#getURLCreationDate()
	 */
	@Override
	public DateTime getURLCreationDate() {
		return this.url_creation_date;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label#setContnent(java.lang.String)
	 */
	@Override
	public void setContnent(String content) {
		this.labelContent = content;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label#getContent()
	 */
	@Override
	public String getContent() {
		return this.labelContent;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Label#validate()
	 */
	@Override
	public Boolean validate() {
		if(null != this.request_id && !this.request_id.validate()) {
			return false;
		}
		
		if(null != this.request_date && !this.request_date.validate()) {
			return false;
		}
		
		if(null != this.shipments && !this.shipments.validate()) {
			return false;
		}
		
		if(null != this.url && !this.url.validate()) {
			return false;
		}
		
		if(null != this.url_creation_date && !this.url_creation_date.validate()) {
			return false;
		}
		
		if(null != this.label_url && !this.label_url.validate()) {
			return false;
		}

		if(null != this.status && !this.status.validate()) {
			return false;
		}

		if(null != this.label_creation_date	 && !this.label_creation_date.validate()) {
			return false;
		}

		if(null != this.labelContent && this.labelContent.isEmpty()) {
			return false;
		}

		if(null != this.errors && !this.errors.validate()) {
			return false;
		}

		return true;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Label#setErrors(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection)
	 */
	@Override
	public void setErrors(AusPostErrorCollection errors) {
		this.errors = errors;

	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
