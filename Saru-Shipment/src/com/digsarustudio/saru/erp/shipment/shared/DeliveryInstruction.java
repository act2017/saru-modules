/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-types represents a delivery instruction object.<br>
 * The sub-types can extends this interface for its purpose.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface DeliveryInstruction {
	/**
	 * 
	 * The builder for {@link DeliveryInstruction}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<DeliveryInstruction> {
		Builder setInstruction(String instruction);
	}
	
	void setInstruction(String instruction);
	String getInstruction();
	
	Boolean validate();
}
