/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean;

import java.util.HashMap;
import java.util.Map;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttribute;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttributeCollection}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemFeatureAttributeCollection implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttributeCollection
													 , IsSerializable {
	
	/**
	 * 
	 * The object builder for {@link ItemFeatreAttributeCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttributeCollection.Builder {
		private ItemFeatureAttributeCollection result = null;

		private Builder() {
			this.result = new ItemFeatureAttributeCollection();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttributeCollection.Builder#addAttribute(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute)
		 */
		@Override
		public Builder addAttribute(ItemFeatureAttribute attribute) {
			this.result.addAttribute(attribute);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemFeatureAttributeCollection build() {
			return this.result;
		}

	}
	
	private Map<String, ItemFeatureAttribute> attributes	= null;

	/**
	 * 
	 */
	private ItemFeatureAttributeCollection() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttributeCollection#addAttribute(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute)
	 */
	@Override
	public void addAttribute(ItemFeatureAttribute attribute) {
		if(null == this.attributes) {
			this.attributes = new HashMap<String, ItemFeatureAttribute>();
		}

		this.attributes.put(attribute.getName(), attribute);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttributeCollection#getAttribute(java.lang.String)
	 */
	@Override
	public ItemFeatureAttribute getAttribute(String name) {
		if(null == this.attributes || !this.attributes.containsKey(name)) {
			return null;
		}
		
		return this.attributes.get(name);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttributeCollection#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.attributes || this.attributes.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
