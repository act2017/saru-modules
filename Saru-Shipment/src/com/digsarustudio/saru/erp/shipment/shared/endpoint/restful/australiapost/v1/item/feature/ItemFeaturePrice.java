/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalGst;

/**
 * The price of attribute of an {@link ItemFeature}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface ItemFeaturePrice {
	/**
	 * 
	 * The builder for {@link ItemFeaturePrice}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemFeaturePrice> {
		Builder setCalculatedPrice(TotalCost price);
		Builder setCalculatedGst(TotalGst gst);
	}
	
	void setCalculatedPrice(TotalCost price);
	void setCalculatedGst(TotalGst gst);
	
	TotalCost getCalculatedPrice();
	TotalGst getCalculatedGst();
	
	Boolean validate();
}
