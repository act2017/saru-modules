/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Contains the dangerous goods subsidiary risk of sub class. This represent the subsidiary risk of a substance, 
 * for example, a product may have a primary risk of toxic (class 6.1) with a subsidiary risk of flammable liquid (class 3). 
 * This field is a decimal value, but must be passed as a string.<br>
 * Normally, the details of this subsidiary risk are from database.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface SubsidiaryRisk {
	/**
	 * 
	 * The builder for {@link SubsidiaryRisk}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<SubsidiaryRisk> {
		Builder setValue(String value);
		Builder setValue(DangerousGoodsSubsidiaryRisks value);
	}
	
	void setValue(String value);
	void setValue(DangerousGoodsSubsidiaryRisks value);
	
	DangerousGoodsSubsidiaryRisks getSubsidiaryRisk();
	String getValue();
	
	Boolean validate();
}
