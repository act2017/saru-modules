/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Type of additional feature. Eg: TRANSIT_COVER.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface ItemFeatureType {
	/**
	 * 
	 * The builder for {@link ItemFeatureType}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemFeatureType> {
		Builder setValue(String value);
		Builder setValue(ItemFeatureTypes type);
	}
	
	void setValue(String value);
	void setValue(ItemFeatureTypes type);
	
	String getValue();
	ItemFeatureTypes getType();
	
	Boolean validate();
}
