/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label;

import java.util.HashMap;
import java.util.Map;

/**
 * StarTrack and Australia Post labels are available in various layouts. 
 * The printed shipping label layout specifies the number of labels per page and the size of the page.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelFormatGroups {
	public static final LabelFormatGroups	ParcelPost					= new LabelFormatGroups("Parcel Post", "Parcel Post");
	public static final LabelFormatGroups	ExpressPost					= new LabelFormatGroups("Express Post", "Express Post");
	public static final LabelFormatGroups	AustraliaPostReturns		= new LabelFormatGroups("Australia Post Returns", "Australia Post Returns");
	public static final LabelFormatGroups	AustraliaPostInternational	= new LabelFormatGroups("Australia Post International", "Australia Post International");
	public static final LabelFormatGroups	StarTrack					= new LabelFormatGroups("StarTrack", "StarTrack");

	private static Map<String, LabelFormatGroups> VALUES = new HashMap<>();
		
	static {
		VALUES.put(ParcelPost.getValue(), ParcelPost);
		VALUES.put(ExpressPost.getValue(), ExpressPost);
		VALUES.put(AustraliaPostReturns.getValue(), AustraliaPostReturns);
		VALUES.put(AustraliaPostInternational.getValue(), AustraliaPostInternational);
		VALUES.put(StarTrack.getValue(), StarTrack);
	}	
	
	private String value = null;
	private String display = null;
	
	private LabelFormatGroups() {
		
	}
	
	private LabelFormatGroups(String value, String display) {
		this.value = value;
		this.display = display;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public String getDisplayString() {
		return this.display;
	}
	
	public static LabelFormatGroups fromValue(String value) {
		if (null == value || !VALUES.containsKey(value)) {
			return null;
		}
		
		return VALUES.get(value);
	}
	
	public static LabelFormatGroups getDefault() {
		return ParcelPost;
	}
		
}
