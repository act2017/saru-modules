/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Additional features for the item.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface ItemFeatureCollection {
	/**
	 * 
	 * The builder for {@link ItemFeatureCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemFeatureCollection> {
		Builder addFeature(ItemFeature feature);
		
		Builder setTransitCover(ItemFeature transitCover);
	}
	
	void addFeature(ItemFeature feature);
	
	void setTransitCover(ItemFeature transitCover);
	
	ItemFeature 			getFeature(String name);
	
	ItemFeature 			getTransitCover();
	
	Boolean validate();
}
