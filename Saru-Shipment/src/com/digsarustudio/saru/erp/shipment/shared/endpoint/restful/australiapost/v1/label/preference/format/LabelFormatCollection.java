/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The required page format for the labels.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface LabelFormatCollection {
	/**
	 * 
	 * The builder for {@link LabelFormatCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<LabelFormatCollection> {
		Builder addFormat(LabelFormat format);
		Builder setFormats(List<LabelFormat> formats);
	}
	
	void addFormat(LabelFormat format);
	void setFormats(List<LabelFormat> formats);
	
	LabelFormat 		getFormat(Integer index);
	List<LabelFormat>	getFormats();
	
	Integer				size();
	Boolean				isEmpty();
	
	Boolean validate();
}
