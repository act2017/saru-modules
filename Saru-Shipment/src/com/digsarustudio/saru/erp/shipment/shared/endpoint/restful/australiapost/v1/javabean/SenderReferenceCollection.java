/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SenderReferenceCollection implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection
												, IsSerializable {
	public static final String ATTR_SENDER_REFERENCES	= "references";
	public static final Integer	MAX_COUNT				= 2;

	/**
	 * 
	 * The object builder for {@link SenderReferenceCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection.Builder {
		private SenderReferenceCollection result = null;

		private Builder() {
			this.result = new SenderReferenceCollection();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection.Builder#addSenderReference(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference)
		 */
		@Override
		public Builder addSenderReference(SenderReference reference) {
			this.result.addSenderReference(reference);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection.Builder#setSenderReferences(java.util.List)
		 */
		@Override
		public Builder setSenderReferences(List<SenderReference> references) {
			this.result.setSenderReferences(references);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection.Builder#setSenderReferences(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference[])
		 */
		@Override
		public Builder setSenderReferences(SenderReference[] references) {
			this.result.setSenderReferences(references);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public SenderReferenceCollection build() {
			return this.result;
		}

	}
	
	private List<SenderReference>	references	= null;
	
	/**
	 * 
	 */
	private SenderReferenceCollection() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection#addSenderReference(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference)
	 */
	@Override
	public void addSenderReference(SenderReference reference) {
		if(null == this.references) {
			this.references = new ArrayList<SenderReference>();
		}

		this.references.add(reference);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection#setSenderReferences(java.util.List)
	 */
	@Override
	public void setSenderReferences(List<SenderReference> references) {		
		this.references = references;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection#setSenderReferences(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference[])
	 */
	@Override
	public void setSenderReferences(SenderReference[] references) {
		this.setSenderReferences(Arrays.asList(references));
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection#getSenderReference(java.lang.Integer)
	 */
	@Override
	public SenderReference getSenderReference(Integer index) {
		if(null == this.references || index >= this.references.size()) {
			return null;
		}

		return this.references.get(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection#getSenderReferences()
	 */
	@Override
	public List<SenderReference> getSenderReferences() {

		return this.references;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.references || this.references.isEmpty()) {
			return false;
		}		

		if( this.isOverSize(this.references) ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	private Boolean isOverSize(List<SenderReference> source) {
		return (null != source && MAX_COUNT > source.size());
	}
}
