/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AusPostErrorCollection implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection
											 , IsSerializable {
	/**
	 * 
	 * The object builder for {@link AusPostErrorCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection.Builder {
		private AusPostErrorCollection result = null;

		private Builder() {
			this.result = new AusPostErrorCollection();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection.Builder#addError(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError)
		 */
		@Override
		public Builder addError(AusPostError error) {
			this.result.addError(error);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection.Builder#setErrors(java.util.List)
		 */
		@Override
		public Builder setErrors(List<AusPostError> errors) {
			this.result.setErrors(errors);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection.Builder#setErrors(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError[])
		 */
		@Override
		public Builder setErrors(AusPostError[] errors) {
			this.result.setErrors(errors);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AusPostErrorCollection build() {
			return this.result;
		}

	}
	
	private Map<String, AusPostError>	errors	= null;
	
	/**
	 * 
	 */
	private AusPostErrorCollection() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection#addError(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError)
	 */
	@Override
	public void addError(AusPostError error) {
		if(null == error || !error.validate()) {
			return;
		}else if(null == this.errors) {
			this.errors = new HashMap<>();
		}

		this.errors.put(error.getCode(), error);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection#setErrors(java.util.List)
	 */
	@Override
	public void setErrors(List<AusPostError> errors) {
		this.errors = errors.stream().collect(Collectors.toMap(x -> x.getCode(), x -> x));

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection#setErrors(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError[])
	 */
	@Override
	public void setErrors(AusPostError[] errors) {
		this.setErrors(Arrays.asList(errors));

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection#getError(java.lang.Integer)
	 */
	@Override
	public AusPostError getError(Integer code) {
		if(null == code) {
			return null;
		}		
		
		return this.getError(code.toString());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection#getError(java.lang.String)
	 */
	@Override
	public AusPostError getError(String code) {
		if(null == code || code.isEmpty()) {
			return null;
		}

		if(null == this.errors || !this.errors.containsKey(code)) {
			return null;
		}
		
		
		return this.errors.get(code);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection#getErrors()
	 */
	@Override
	public List<AusPostError> getErrors() {
		if(null == this.errors || this.errors.isEmpty()) {
			return null;
		}
		
		return new ArrayList<AusPostError>(this.errors.values());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection#validate()
	 */
	@Override
	public Boolean validate() {
		//The error can be empty due to the Australia Post API will return an empty array for the error.
		if(null == this.errors) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
