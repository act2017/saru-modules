/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.javabean;

import com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsClassDivisions;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The implementation of {@link DangerousGoodsClassDivision} for {@link DangerousGoods} to used by {@link Shipment}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DangerousGoodsClassDivision implements com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsClassDivision
												  , IsSerializable {
	public static final Integer MAX_LEN	= 7;

	/**
	 * 
	 * The object builder for {@link DangerousGoodsClassDivision}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsClassDivision.Builder {
		private DangerousGoodsClassDivision result = null;

		private Builder() {
			this.result = new DangerousGoodsClassDivision();
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsClassDivision.Builder#setValue(com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsClassDivisions)
		 */
		@Override
		public Builder setValue(DangerousGoodsClassDivisions classDivision) {
			this.result.setValue(classDivision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsClassDivision.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String classDivision) {
			this.result.setValue(classDivision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DangerousGoodsClassDivision build() {
			return this.result;
		}

	}
	
	private String	classDivision	= null;
	
	/**
	 * 
	 */
	private DangerousGoodsClassDivision() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsClassDivision#setValue(com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsClassDivisions)
	 */
	@Override
	public void setValue(DangerousGoodsClassDivisions classDivision) {
		this.classDivision = classDivision.getClassification();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsClassDivision#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String classDivision) {
		this.classDivision = classDivision;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsClassDivision#getClassDivision()
	 */
	@Override
	public DangerousGoodsClassDivisions getClassDivision() {
		return DangerousGoodsClassDivisions.fromValue(this.classDivision);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsClassDivision#getValue()
	 */
	@Override
	public String getValue() {
		return this.classDivision;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsClassDivision#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.classDivision) {
			return false;
		}
		
		if(MAX_LEN < this.classDivision.length()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
