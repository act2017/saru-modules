/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * A merchant specified array of references for the shipment. 
 * Effectively deprecates customer_reference_1 and customer_reference_2. 
 * The array length supported varies as noted in the 
 * <a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">
 * Create Shipments</a> service.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface SenderReference {
	/**
	 * 
	 * The builder for {@link SenderReference}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<SenderReference> {
		Builder setReference(String reference);
	}
		
	void setReference(String reference);	
	String getReference();
	
	Boolean validate();
}
