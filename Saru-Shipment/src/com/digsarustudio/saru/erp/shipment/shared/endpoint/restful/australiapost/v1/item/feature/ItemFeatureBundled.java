/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Indicates whether the feature is automatically included with the product.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface ItemFeatureBundled {
	/**
	 * 
	 * The builder for {@link ItemFeatureBundled}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemFeatureBundled> {
		Builder setBundled();
		Builder setUnbundled();
	}
	
	void setBundled();
	void setUnbundled();
	
	Boolean isBundled();
	
	Boolean validate();
}
