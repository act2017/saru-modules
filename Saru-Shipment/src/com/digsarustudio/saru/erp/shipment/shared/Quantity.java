/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The quantity of a particular item.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Quantity {
	/**
	 * 
	 * The builder for {@link Quantity}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Quantity> {
		Builder setValue(Integer value);
		Builder setValue(String value);
	}
	
	void setValue(Integer value);
	void setValue(String value);
	
	Integer getValue();
	String getValueString();
	
	Boolean validate();
}
