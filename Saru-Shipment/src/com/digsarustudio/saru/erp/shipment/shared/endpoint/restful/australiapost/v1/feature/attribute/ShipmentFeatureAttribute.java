/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature;

/**
 * The attribute object of {@link ShipmentFeature}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
 * @param		T The data type of the value of attribute.
 * 
 */
public interface ShipmentFeatureAttribute<T> {
	/**
	 * 
	 * The builder for {@link ShipmentFeatureAttribute}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 * @param		S The data type of the value of attribute.
	 */
	public static interface Builder<S> extends ObjectBuilder<ShipmentFeatureAttribute<S>> {
		Builder<S> setValue(S value);
	}
	
	void setValue(T value);
	T getValue();	
	
	Boolean validate();
}
