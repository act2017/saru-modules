/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.address;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb;

/**
 * The sub-type contains the name of suburb, state, and the postcode for Suburb Validation API to use.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface SuburbValidationRequest {
	/**
	 * 
	 * The builder for {@link SuburbValidationRequest}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<SuburbValidationRequest> {
		Builder setSuburb(String suburb);
		Builder setSuburb(Suburb suburb);
		
		Builder setState(String state);
		Builder setState(State state);
		
		Builder setPostcode(String postcode);
		Builder setPostcode(Postcode postcode);
	}
	
	void setSuburb(String suburb);
	void setSuburb(Suburb suburb);
	
	void setState(String state);
	void setState(State state);
	
	void setPostcode(String postcode);
	void setPostcode(Postcode postcode);
	
	Suburb getSuburb();
	String getSuburbString();
	
	State getState();
	String getStateString();
	
	Postcode getPostcode();
	String getPostcodeString();
	
	Boolean validate();
}
