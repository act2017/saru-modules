/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.javabean;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceCollection}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelPreferenceCollection implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceCollection
												, IsSerializable {
	
	/**
	 * 
	 * The object builder for {@link LabelPreferenceCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceCollection.Builder {
		private LabelPreferenceCollection result = null;

		private Builder() {
			this.result = new LabelPreferenceCollection();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceCollection.Builder#addPreference(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference)
		 */
		@Override
		public Builder addPreference(LabelPreference preference) {
			this.result.addPreference(preference);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceCollection.Builder#setPreferences(java.util.List)
		 */
		@Override
		public Builder setPreferences(List<LabelPreference> preferences) {
			this.result.setPreferences(preferences);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public LabelPreferenceCollection build() {
			return this.result;
		}

	}
	
	private List<LabelPreference>	preferences	= null;

	/**
	 * 
	 */
	private LabelPreferenceCollection() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceCollection#addPreference(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference)
	 */
	@Override
	public void addPreference(LabelPreference preference) {
		if(null == preference) {
			return;
		}
		
		if(null == this.preferences) {
			this.preferences = new ArrayList<>();
		}

		this.preferences.add(preference);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceCollection#setPreferences(java.util.List)
	 */
	@Override
	public void setPreferences(List<LabelPreference> preferences) {
		this.preferences = preferences;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceCollection#getPreference(java.lang.Integer)
	 */
	@Override
	public LabelPreference getPreference(Integer index) {
		if(null == this.preferences || this.preferences.isEmpty() || null == index || index >= this.preferences.size()) {
			return null;
		}
		
		return this.preferences.get(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceCollection#getPreferences()
	 */
	@Override
	public List<LabelPreference> getPreferences() {
		
		return this.preferences;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceCollection#size()
	 */
	@Override
	public Integer size() {
		if(null == this.preferences) {
			return 0;
		}
		
		return this.preferences.size();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceCollection#isEmpty()
	 */
	@Override
	public Boolean isEmpty() {
		if(null == this.preferences) {
			return true;
		}
		
		return this.preferences.isEmpty();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceCollection#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.preferences || this.preferences.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
