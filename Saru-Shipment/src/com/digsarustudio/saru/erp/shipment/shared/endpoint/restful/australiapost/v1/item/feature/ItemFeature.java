/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttribute;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttributeCollection;

/**
 * The sub-type represents the feature of an item.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface ItemFeature {
	public static final String	ATTRIBUTES	= "attributes";
	public static final String	TYPE		= "type";
	
	/**
	 * 
	 * The builder for Transit cover
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemFeature> {
		Builder addAttribute(ItemFeatureAttribute attribute);
		Builder setItemFeatureAttributeCollection(ItemFeatureAttributeCollection attributes);
		Builder setName(String name);
		Builder setType(String type);
		Builder setType(ItemFeatureTypes type);
		Builder setType(ItemFeatureType type);
		Builder setPrice(ItemFeaturePrice price);
		Builder setBundled(ItemFeatureBundled bundled);
	}	
	
	void addAttribute(ItemFeatureAttribute attribute);
	void setItemFeatureAttributeCollection(ItemFeatureAttributeCollection attributes);
	void setName(String name);
	void setType(String type);
	void setType(ItemFeatureTypes type);
	void setType(ItemFeatureType type);
	void setPrice(ItemFeaturePrice price);
	void setBundled(ItemFeatureBundled bundled);
	
	String getName();
	
	ItemFeatureAttribute getAttribute(String name);	
	ItemFeatureAttributeCollection	getAttributes();
	
	ItemFeaturePrice getPrice();
	ItemFeatureType getItemFeatureType();
	ItemFeatureBundled getBundled();
	
	Boolean validate();
}
