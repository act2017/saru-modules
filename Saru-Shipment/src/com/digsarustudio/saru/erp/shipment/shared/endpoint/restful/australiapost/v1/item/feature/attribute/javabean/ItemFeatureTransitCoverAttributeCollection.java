/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean;

import java.util.HashMap;
import java.util.Map;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttribute;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttributeCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureTransitCover;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link ItemFeatureAttributeCollection}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemFeatureTransitCoverAttributeCollection implements ItemFeatureAttributeCollection, IsSerializable {
	
	/**
	 * 
	 * The object builder for {@link ItemFeatureTransitCoverAttributeCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ItemFeatureAttributeCollection.Builder {
		private ItemFeatureTransitCoverAttributeCollection result = null;

		private Builder() {
			this.result = new ItemFeatureTransitCoverAttributeCollection();
		}

		/**
		 * @param coverAmount
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.ItemFeatureTransitCoverAttributeCollection#setTransitCoverAmount(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttribute)
		 */
		public Builder setTransitCoverAmount(ItemFeatureAttribute coverAmount) {
			result.setTransitCoverAmount(coverAmount);
			return this;
		}

		/**
		 * @param coverAmount
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.ItemFeatureTransitCoverAttributeCollection#setRate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttribute)
		 */
		public Builder setRate(ItemFeatureAttribute rate) {
			result.setRate(rate);
			return this;
		}

		/**
		 * @param coverAmount
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.ItemFeatureTransitCoverAttributeCollection#setCoverIncluded(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttribute)
		 */
		public Builder setCoverIncluded(ItemFeatureAttribute coverIncluded) {
			result.setCoverIncluded(coverIncluded);
			return this;
		}

		/**
		 * @param cover
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.ItemFeatureTransitCoverAttributeCollection#setMaximumCover(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttribute)
		 */
		public Builder setMaximumCover(ItemFeatureAttribute cover) {
			result.setMaximumCover(cover);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttributeCollection.Builder#addAttribute(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute)
		 */
		@Override
		public Builder addAttribute(ItemFeatureAttribute attribute) {
			this.result.addAttribute(attribute);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemFeatureTransitCoverAttributeCollection build() {
			return this.result;
		}

	}
	
	private Map<String, ItemFeatureAttribute>	attributes	= null;

	/**
	 * 
	 */
	private ItemFeatureTransitCoverAttributeCollection() {
	}
	
	public void setTransitCoverAmount(ItemFeatureAttribute coverAmount) {
		this.addAttribute(coverAmount);
	}
	
	public TransitCoverAmount getTransitCoverAmount() {
		return (TransitCoverAmount) this.getAttribute(ItemFeatureTransitCover.ATTRIBUTE_COVER_AMOUNT);
	}
	
	public void setRate(ItemFeatureAttribute rate) {
		this.addAttribute(rate);
	}
	
	public TransitCoverRate	getRate() {
		return (TransitCoverRate) this.getAttribute(ItemFeatureTransitCover.ATTRIBUTE_RATE);
	}
	
	public void setCoverIncluded(ItemFeatureAttribute coverIncluded) {
		this.addAttribute(coverIncluded);
	}
	
	public TransitCoverIncluded	getCoverIncluded() {
		return (TransitCoverIncluded) this.getAttribute(ItemFeatureTransitCover.ATTRIBUTE_INCLUDED_COVER);
	}
	
	public void setMaximumCover(ItemFeatureAttribute cover) {
		this.addAttribute(cover);
	}
	
	public TransitCoverMaximum	getMaximumCover() {
		return (TransitCoverMaximum) this.getAttribute(ItemFeatureTransitCover.ATTRIBUTE_MAXIMUM_COVER);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttributeCollection#addAttribute(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute)
	 */
	@Override
	public void addAttribute(ItemFeatureAttribute attribute) {
		if(null == this.attributes) {
			this.attributes = new HashMap<String, ItemFeatureAttribute>();
		}

		this.attributes.put(attribute.getName(), attribute);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttributeCollection#getAttribute(java.lang.String)
	 */
	@Override
	public ItemFeatureAttribute getAttribute(String name) {
		if(null == this.attributes || !this.attributes.containsKey(name)) {
			return null;
		}
		
		return this.attributes.get(name);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttributeCollection#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.attributes || this.attributes.isEmpty()) {
			return false;
		}
		
		if( null == this.getAttribute(ItemFeatureTransitCover.ATTRIBUTE_COVER_AMOUNT)) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
