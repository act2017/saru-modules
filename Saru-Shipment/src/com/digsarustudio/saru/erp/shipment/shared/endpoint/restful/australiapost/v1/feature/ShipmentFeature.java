/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.DeliveryWindow;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.ShipmentFeatureAttribute;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.DeliveryWindowCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.ShipmentFeatureAttributeDate;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.ShipmentFeatureAttributeTime;

/**
 * Available shipment features are:<br>
 * <ul>
 * 	<li>DELIVERY_DATE</li>
 * 	<li>DELIVERY_TIMES</li>
 * 	<li>PICKUP_DATE</li>
 * 	<li>PICKUP_TIME</li>
 * </ul>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
 *
 */
public interface ShipmentFeature {
	public static final String ATTR_ATTRIBUTES	= "attributes";
	
	/**
	 * 
	 * The builder for Delivery date.<br>
	 * <br>
	 * [FEATURE_KEY] : DELIVERY_DATE
	 * [ATTRIBUTE_OBJECT] : date - should be supplied in the following format yyyy-MM-dd<br>
	 * <br>
	 * This feature represents the date selection for delivery of a shipment. 
	 * The delivery date feature contains a single attribute of "date" which is required if this feature is used.<br>
	 * For example:<br>
	 * "DELIVERY_DATE": {<br>
	 * 		"attributes": {<br>
	 *			"date": "2017-07-15"<br>
	 *		}<br>
	 *	}<br>
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
	 */
	public static interface DeliveryDateBuilder extends ObjectBuilder<ShipmentFeature> {
		DeliveryDateBuilder setDate(Date date);
		DeliveryDateBuilder setDate(String date);
		DeliveryDateBuilder setDate(ShipmentFeatureAttributeDate date);
	}
	
	/**
	 * 
	 * The builder for Delivery Times.<br>
	 * <br>
	 * [FEATURE_KEY] : DELIVERY_TIMES<br>
	 * [ATTRIBUTE_OBJECT] : windows - A list of delivery time windows. Each window has a start date and an end date.
	 * <ul>
	 * 	<li>start - Start time of window. Time should be supplied in the following format hh24:mm:ss and should be before the end time.</li>
	 *  <li>end - End time of window. Time should be supplied in the following format hh24:mm:ss and should be after the start time.</li>
	 * </ul>
	 * Please note that only one delivery time window is currently supported.<br>
	 * For example:<br>
	 * "DELIVERY_TIMES": {<br>
     *       "attributes": {<br>
     *           "windows": [<br>
     *               {<br>
     *                   "start": "17:00:00",<br>
     *                   "end": "19:00:00"<br>
     *               }<br>
     *           ]<br>
     *       }<br>
     *   }<br>
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
	 */
	public static interface DeliveryTimesBuilder extends ObjectBuilder<ShipmentFeature> {
		DeliveryTimesBuilder addDeliveryWindow(DeliveryWindow window);
		DeliveryTimesBuilder setDeliveryWindows(List<DeliveryWindow> windows);
		DeliveryTimesBuilder setDeliveryWindows(DeliveryWindowCollection windows);
	}
	
	/**
	 * 
	 * The builder for Pickup date.<br>
	 * <br>
	 * [FEATURE_KEY] : PICKUP_DATE<br>
	 * [ATTRIBUTE_OBJECT] : date - should be supplied in the format of yyyy-MM-dd<br>
	 * <br>
	 * This feature represents the pick up date for a shipment. 
	 * The date supplied must not be in the past relative to the local date of the pickup address.<br> 
	 * If the pickup date is not supplied for a product where the field is required, then it will default to today, 
	 * based on the local date of the pickup address.<br>
	 * <br>
	 * For example:<br>
	 * "PICKUP_DATE": {<br>
     *          "attributes": {<br>
     *              "date": "2017-07-15"<br>
     *          }<br>
     *      }<br>
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
	 */
	public static interface PickupDateBuilder extends ObjectBuilder<ShipmentFeature> {
		PickupDateBuilder setDate(Date date);
		PickupDateBuilder setDate(String date);
		PickupDateBuilder setDate(ShipmentFeatureAttributeDate date);
	}
	
	/**
	 * 
	 * The builder for Pickup Time.<br>
	 * <br>
	 * [FEATURE_KEY] : PICKUP_TIME<br>
	 * [ATTRIBUTE_OBJECT] : time - should be supplied in the format of hh:mm:ss<br>
	 * <br>
	 * This feature represents the pick up time for a shipment. The pickup time, in combination with the pickup date, 
	 * must not be in the past relative to the local time of the pickup address. 
	 * This field is mandatory for products that allow pickup time to be provided.<br>
	 * <br>
	 * For example:<br>
	 * "PICKUP_TIME": {<br>
     *      "attributes": {<br>
     *          "time": "10:00:00"<br>
     *      }<br>
     *  }<br>
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
	 */
	public static interface PickupTimeBuilder extends ObjectBuilder<ShipmentFeature> {
		PickupTimeBuilder setTime(Time time);
		PickupTimeBuilder setTime(String time);
		PickupTimeBuilder setTime(ShipmentFeatureAttributeTime time);
	}
	
	void setAttribute(ShipmentFeatureAttribute<?> attribute);
	ShipmentFeatureAttribute<?> getAttribute();
	
	Boolean validate();	
}
