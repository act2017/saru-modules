/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SenderReference implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference
									  , IsSerializable {
	public static final String 	ATTR_REFERENCE		= "reference";
	public static final Integer	MAX_LEN_REFERENCE	= 20;
	
	/**
	 * 
	 * The object builder for {@link SenderReference}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference.Builder {
		private SenderReference result = null;

		private Builder() {
			this.result = new SenderReference();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference.Builder#setReference(java.lang.String)
		 */
		@Override
		public Builder setReference(String reference) {
			this.result.setReference(reference);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public SenderReference build() {
			return this.result;
		}

	}

	private String reference	= null;
	
	/**
	 * 
	 */
	private SenderReference() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference#setReference(java.lang.String)
	 */
	@Override
	public void setReference(String reference) {
		this.reference = (null == reference || MAX_LEN_REFERENCE >= reference.length()) ? reference : reference.substring(0, MAX_LEN_REFERENCE);;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference#getReference()
	 */
	@Override
	public String getReference() {

		return this.reference;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReference#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.reference || this.reference.isEmpty()) {
			return false;
		}

		if( MAX_LEN_REFERENCE < this.reference.length() ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
