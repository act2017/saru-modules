/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature;

import java.util.HashMap;
import java.util.Map;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The type of additional feature of an item carried by {@link Shipment}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemFeatureTypes implements IsSerializable {
	public static final ItemFeatureTypes	TransitCover	= new ItemFeatureTypes("TRANSIT_COVER", "Transit Cover");

	private static Map<String, ItemFeatureTypes> VALUES = new HashMap<>();
		
	static {
		VALUES.put(TransitCover.getValue(), TransitCover);
	}	
	
	private String value = null;
	private String display = null;
	
	private ItemFeatureTypes() {
		
	}
	
	private ItemFeatureTypes(String value, String display) {
		this.value = value;
		this.display = display;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public String getDisplayString() {
		return this.display;
	}
	
	public static ItemFeatureTypes fromValue(String value) {
		if (null == value) {
			return null;
		}else if (!VALUES.containsKey(value)) {
			return new ItemFeatureTypes(value, "Unknown");
		}
		
		return VALUES.get(value);
	}
	
	public static ItemFeatureTypes getDefault() {
		return TransitCover;
	}
	

}
