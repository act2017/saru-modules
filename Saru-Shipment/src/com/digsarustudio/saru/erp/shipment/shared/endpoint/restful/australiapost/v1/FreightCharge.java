/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The freight charge for this shipment.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface FreightCharge {
	/**
	 * 
	 * The builder for {@link FreightCharge}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<FreightCharge> {
		Builder setValue(Integer charge);
		Builder setValue(Integer charge, Integer precision);

		Builder setValue(String charge);
		Builder setValue(String charge, String precision);
	}
	
	void setValue(Integer charge);
	void setValue(Integer charge, Integer precision);

	void setValue(String charge);
	void setValue(String charge, String precision);
	
	String getCharge();
	String getPrcision();
	String getDecimalCharge();
	
	Boolean validate();
}
