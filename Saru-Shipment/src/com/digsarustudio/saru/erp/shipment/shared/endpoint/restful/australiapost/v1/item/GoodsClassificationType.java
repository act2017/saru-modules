/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.GoodsClassificationTypes;

/**
 * Category or classification of the goods. 
 * If commercial_value is true, then this field must be set to "OTHER", 
 * otherwise it must be set to "GIFT", "SAMPLE", "DOCUMENT" or "RETURN".<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface GoodsClassificationType {
	/**
	 * 
	 * The builder for {@link GoodsClassificationType}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<GoodsClassificationType> {
		Builder setType(GoodsClassificationTypes type);
		Builder setType(String type);
	}
	
	void setType(GoodsClassificationTypes type);
	void setType(String type);
	
	String getType();
	GoodsClassificationTypes getGoodsClassificationType();
	
	Boolean validate();
}
