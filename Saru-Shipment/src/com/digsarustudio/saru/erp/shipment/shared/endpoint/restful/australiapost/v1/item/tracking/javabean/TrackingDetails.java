/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ArticleId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.BarcodeId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ConsignmentId;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.TrackingDetails}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class TrackingDetails implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.TrackingDetails
									  , IsSerializable {
	/**
	 * [Mandatory]
	 * Type: String
	 */
	public static final String	ATTR_CONSIGNMENT_ID	= "consignment_id";
	
	/**
	 * [Mandatory]
	 * Type: String
	 */
	public static final String	ATTR_ARTICLE_ID		= "article_id";
	
	/**
	 * [Mandatory]
	 * Type: String
	 */
	public static final String	ATTR_BARCODE_ID		= "barcode_id";

	/**
	 * 
	 * The object builder for {@link TrackingDetails}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.TrackingDetails.Builder {
		private TrackingDetails result = null;

		private Builder() {
			this.result = new TrackingDetails();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.TrackingDetails.Builder#setConsignmentId(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ConsignmentId)
		 */
		@Override
		public Builder setConsignmentId(ConsignmentId consignment) {
			this.result.setConsignmentId(consignment);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.TrackingDetails.Builder#setArticleId(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ArticleId)
		 */
		@Override
		public Builder setArticleId(ArticleId article) {
			this.result.setArticleId(article);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.TrackingDetails.Builder#setBarcodeId(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.BarcodeId)
		 */
		@Override
		public Builder setBarcodeId(BarcodeId barcode) {
			this.result.setBarcodeId(barcode);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public TrackingDetails build() {
			return this.result;
		}

	}
	
	private ConsignmentId	consignment_id	= null;
	private ArticleId		article_id		= null;
	private BarcodeId		barcode_id		= null;
	
	/**
	 * 
	 */
	private TrackingDetails() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.TrackingDetails#setConsignmentId(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ConsignmentId)
	 */
	@Override
	public void setConsignmentId(ConsignmentId consignment) {
		this.consignment_id = consignment;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.TrackingDetails#getConsignmentId()
	 */
	@Override
	public ConsignmentId getConsignmentId() {
		
		return this.consignment_id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.TrackingDetails#setArticleId(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ArticleId)
	 */
	@Override
	public void setArticleId(ArticleId article) {
		this.article_id = article;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.TrackingDetails#getArticleId()
	 */
	@Override
	public ArticleId getArticleId() {
		
		return this.article_id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.TrackingDetails#setBarcodeId(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.BarcodeId)
	 */
	@Override
	public void setBarcodeId(BarcodeId barcode) {
		this.barcode_id = barcode;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.TrackingDetails#getBarcodeId()
	 */
	@Override
	public BarcodeId getBarcodeId() {
		
		return this.barcode_id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.TrackingDetails#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.consignment_id || !this.consignment_id.validate()) {
			return false;
		}
		
		if(null == this.article_id || !this.article_id.validate()) {
			return false;
		}

		if(null != this.barcode_id && !this.barcode_id.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
