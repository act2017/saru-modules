/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DateTime implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime
								, IsSerializable {

	/**
	 * 
	 * The object builder for {@link DateTime}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime.Builder {
		private DateTime result = null;

		private Builder() {
			this.result = new DateTime();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime.Builder#setDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date)
		 */
		@Override
		public Builder setDate(Date date) {
			this.result.setDate(date);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime.Builder#setTime(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time)
		 */
		@Override
		public Builder setTime(Time time) {
			this.result.setTime(time);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime.Builder#setDateTime(java.lang.String)
		 */
		@Override
		public Builder setDateTime(String dateTime) {
			this.result.setDateTime(dateTime);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DateTime build() {
			return this.result;
		}

	}
	
	private Date 	date	= null;
	private Time	time	= null;
	
	/**
	 * 
	 */
	private DateTime() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime#setDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date)
	 */
	@Override
	public void setDate(Date date) {
		this.date = date;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime#setTime(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time)
	 */
	@Override
	public void setTime(Time time) {
		this.time = time;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime#getDate()
	 */
	@Override
	public Date getDate() {
		
		return this.date;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime#getTime()
	 */
	@Override
	public Time getTime() {
		
		return this.time;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime#setDateTime(java.lang.String)
	 */
	@Override
	public void setDateTime(String dateTime) {
		if(null == dateTime || dateTime.isEmpty()) {
			return;
		}
		
		String[] dateTimes = dateTime.split(DELIMITER_DATE_AND_TIME);
		
		Date date = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Date.builder().setDate(dateTimes[0])
																														.build();
		this.setDate(date);
		
		if(2 == dateTimes.length) {
			Time time = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Time.builder().setTime(dateTimes[1])
																															.build();
			this.setTime(time);
		}		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime#getDateTime()
	 */
	@Override
	public String getDateTime() {
		if( !this.hasDate() || !this.hasTime() ) {
			return null;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		if(null != this.date) {
			buffer.append(this.date.getDate());
		}
		
		if(null != this.time) {
			buffer.append(DELIMITER_DATE_AND_TIME);
			buffer.append(this.time.getTime());
		}
		
		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime#getDatabaseDateTime()
	 */
	@Override
	public String getDatabaseDateTime() {
		if( !this.hasDate() || !this.hasTime() ) {
			return null;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		if(this.hasDate()) {
			buffer.append(this.date.getDatabaseDateTime());
		}
		
		if(this.hasTime()) {
			buffer.append(DELIMITER_DATABASE_DATE_AND_TIME);
			buffer.append(this.time.getDatabaseDateTime());
		}
		
		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime#validate()
	 */
	@Override
	public Boolean validate() {
		if( !this.hasDate() || !this.hasTime() ) {
			return false;
		}		
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	private Boolean hasDate() {
		return (null != this.date && this.date.validate());
	}
	
	private Boolean hasTime() {
		return (null != this.time && this.time.validate());
	}
}
