/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Use this field to note the reason the item is being returned. This field is only valid for RETURNS.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface ReasonForReturn {
	/**
	 * 
	 * The builder for {@link ReasonForReturn}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ReasonForReturn> {
		Builder setReason(String reason);
	}
	
	void setReason(String reason);
	String getReason();
	
	Boolean validate();
}
