/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The intended method of payment, the only valid value is: CHARGE_TO_ACCOUNT.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface PaymentMethod {
	/**
	 * 
	 * The builder for {@link PaymentMethod}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<PaymentMethod> {
		Builder setValue(String value);
	}
	
	void setValue(String value);
	String getValue();
	
	Boolean validate();
}
