/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * A message related to the generation progress.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface LabelGenerationProgressMessage {
	/**
	 * 
	 * The builder for {@link LabelGenerationProgressMessage}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<LabelGenerationProgressMessage> {
		Builder setValue(String value);
	}
	
	void setValue(String value);
	String getValue();
	
	Boolean validate();
}
