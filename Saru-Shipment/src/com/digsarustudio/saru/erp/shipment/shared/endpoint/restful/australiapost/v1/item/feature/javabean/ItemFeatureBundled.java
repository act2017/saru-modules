/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Indicates whether the feature is automatically included with the product.<br>
 * Type: Boolean.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemFeatureBundled implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureBundled
										 , IsSerializable {
	
	/**
	 * 
	 * The object builder for {@link ItemFeatureBundled}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.ItemFeatureBundled.Builder {
		private ItemFeatureBundled result = null;

		private Builder() {
			this.result = new ItemFeatureBundled();
		}

		/**
		 * 
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureBundled#setBundled()
		 */
		public Builder setBundled() {
			result.setBundled();
			return this;
		}

		/**
		 * 
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.javabean.ItemFeatureBundled#setUnbundled()
		 */
		public Builder setUnbundled() {
			result.setUnbundled();
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemFeatureBundled build() {
			return this.result;
		}

	}

	private Boolean	bundled	= null;
	
	/**
	 * 
	 */
	private ItemFeatureBundled() {
	}
	
	public void setBundled() {
		this.bundled = true;
	}

	public void setUnbundled() {
		this.bundled = false;
	}
	
	public Boolean isBundled() {
		return this.bundled;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemFeature#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.bundled) {
			return false;
		}
				
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
