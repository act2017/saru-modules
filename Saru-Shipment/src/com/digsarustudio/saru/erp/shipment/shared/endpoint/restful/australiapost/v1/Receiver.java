/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Country;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber;
import com.digsarustudio.saru.erp.shipment.shared.DeliveryInstruction;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.AddressTypes;

/**
 * The details of a receiver.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 */
public interface Receiver {
	/**
	 * 
	 * The builder for {@link Receiver}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Receiver> {
		/**
		 * [Mandatory]<br>
		 * <br>
		 * The name of the receiver in 40 characters.<br>
		 * 
		 * @param name The name of the receiver
		 * @since Oct 2014
		 */
		Builder setName(PersonalName name);
		
		/**
		 * [Optional]<br>
		 * <br>
		 * The Australia Post customer number in 10 characters.<br>
		 * 
		 * @param number The Australia Post customer number.
		 * @since Oct 2014
		 * 
		 * @return The instance of builder
		 */
		Builder setAPCN(AustraliaPostCustomerNumber number);
		
		/**
		 * [Optional]<br>
		 * <br>
		 * The name of the business for the address in 40 characters.
		 * 
		 * @param name The name of the business for the address.
		 * @since Oct 2014
		 * 
		 * @return The instance of this builder
		 */
		Builder setBusinessName(BusinessName name);
		
		/**
		 * [Optional]<br>
		 * <br>
		 * The type of address. Valid values are:<br>
		 * 	PARCEL_LOCKER - an address with this type represents an Australia Post Parcel Locker.<br>
		 * 	PARCEL_COLLECT - an address with this type represents an Australia Post Parcel Collection location.<br>
		 * 	STANDARD_ADDRESS - an address with this type represents a normal delivery address.<br>
		 * 
		 * @param type The type of address.
		 * @since Oct 2014
		 */
		Builder setType(AddressTypes type);
		Builder setType(AddressType type);
		
		/**
		 * [Mandatory]<br>
		 * <br>
		 * The address lines of the address. 
		 * Minimum of one address line, up to a maximum of three. 
		 * Each address line is a maximum of 40 characters.<br>
		 * 
		 * @param addresslines The array of address lines.
		 * @since Oct 2014
		 */
		Builder setAddressLines(AddressLines addresslines);
		
		/**
		 * [Mandatory]<br>
		 * <br>
		 * The suburb for the address in 30 characters.<br>
		 * 
		 * @param suburb The suburb for the address.
		 * @since Oct 2014
		 */
		Builder setSuburb(Suburb suburb);
		
		/**
		 * [Mandatory] for AU<br>
		 * [Optional] for International<br>
		 * <br>
		 * The state code for the address in 3 characters or AU, 10 characters for International. 
		 * Valid values are: ACT, NSW, NT, QLD, SA, TAS, VIC, WA.<br>
		 * 
		 * @param state The state code for the address
		 * @since Oct 2014
		 */
		Builder setState(State state);
		
		/**
		 * [Mandatory] for AU and International Kahala Posts Group<br>
		 * [Optional] for the rest of International.<br> 
		 * <br>
		 * The postcode for the address in 4 digits for AU, but 10 characters for International. 
		 * This is mandatory for domestic shipments and shipments to International Kahala Posts Group. 
		 * It is optional for other International shipments.
		 * 
		 * @param postcode The postcode for the address
		 * @since Oct 2014
		 */
		Builder setPostcode(Postcode postcode);
		
		/**
		 * [Optional]<br>
		 * <br>
		 * The country code for the address. The country code must conform to 
		 * <a href="https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2">ISO 3166-1 alpha-2 standard</a>.	
		 * 
		 * @param country The country code for the address
		 * @since Oct 2014
		 */
		Builder setCountry(Country country);
		
		/**
		 * [Optional]<br>
		 * <br>
		 * The receiver’s phone number. For StarTrack shipments, the maximum length is 10 characters.
		 * For AusPost shipments, the phone number must be between 10 and 20 characters in length and the allowable 
		 * characters are "()- 0-9" (digits, space, hyphen and parentheses.) Examples are: 0491-570-156, 02) 5550 1234, 
		 * +61 (3) 7010 4321.
		 * 
		 * @param number The sender’s phone number
		 * @since Oct 2014
		 */
		Builder setPhone(PhoneNumber number);
		
		/**
		 * [Optional]<br>
		 * <br>
		 * The receiver's email address in 50 characters.<br>
		 * 
		 * @param email The sender’s email address.
		 * @since Oct 2014
		 */
		Builder setEmail(EmailAddress email);
		
		/**
		 * [Optional]<br>
		 * <br>
		 * Instructions to aid in prompt delivery of the item in 128 characters.<br>
		 * Note - For StarTrack products, delivery instructions is an array of strings with a maximum 
		 * 		  of 3 lines, each with a maximum of 25 characters. 
		 * 		  Delivery instructions for StarTrack products will be printed as "Special instructions" 
		 * 		  on labels.
		 * 
		 * @param email The sender’s email address.
		 * @since Oct 2014
		 */
		Builder setDeliveryInstructions(DeliveryInstruction instruction);
	}
	
	/**
	 * [Mandatory]<br>
	 * <br>
	 * The name of the receiver in 40 characters.<br>
	 * 
	 * @param name The name of the receiver
	 * @since Oct 2014
	 */
	void setName(PersonalName name);
	PersonalName getName();
	
	/**
	 * [Optional]<br>
	 * <br>
	 * The Australia Post customer number in 10 characters.<br>
	 * 
	 * @param number The Australia Post customer number.
	 * @since Oct 2014
	 */
	void setAPCN(AustraliaPostCustomerNumber number);
	AustraliaPostCustomerNumber getAPCN();
	
	/**
	 * [Optional]<br>
	 * <br>
	 * The name of the business for the address in 40 characters.
	 * 
	 * @param name The name of the business for the address.
	 * @since Oct 2014
	 */
	void setBusinessName(BusinessName name);
	BusinessName getBusinessName();
	
	/**
	 * [Optional]<br>
	 * <br>
	 * The type of address. Valid values are:<br>
	 * 	PARCEL_LOCKER - an address with this type represents an Australia Post Parcel Locker.<br>
	 * 	PARCEL_COLLECT - an address with this type represents an Australia Post Parcel Collection location.<br>
	 * 	STANDARD_ADDRESS - an address with this type represents a normal delivery address.<br>
	 * 
	 * @param type The type of address.
	 * @since Oct 2014
	 */
	void setType(AddressTypes type);
	void setType(AddressType type);
	AddressType getType();
	AddressTypes getAddressType();
	
	/**
	 * [Mandatory]<br>
	 * <br>
	 * The address lines of the address. 
	 * Minimum of one address line, up to a maximum of three. 
	 * Each address line is a maximum of 40 characters.<br>
	 * 
	 * @param addresslines The array of address lines.
	 * @since Oct 2014
	 */
	void setAddressLines(AddressLines addresslines);
	AddressLines getAddressLines();
	
	/**
	 * [Mandatory]<br>
	 * <br>
	 * The suburb for the address in 30 characters.<br>
	 * 
	 * @param suburb The suburb for the address.
	 * @since Oct 2014
	 */
	void setSuburb(Suburb suburb);
	Suburb getSuburb();
	
	/**
	 * [Mandatory] for AU<br>
	 * [Optional] for International<br>
	 * <br>
	 * The state code for the address in 3 characters or AU, 10 characters for International. 
	 * Valid values are: ACT, NSW, NT, QLD, SA, TAS, VIC, WA.<br>
	 * 
	 * @param state The state code for the address
	 * @since Oct 2014
	 */
	void setState(State state);
	State getState();
	
	/**
	 * [Mandatory] for AU and International Kahala Posts Group<br>
	 * [Optional] for the rest of International.<br> 
	 * <br>
	 * The postcode for the address in 4 digits for AU, but 10 characters for International. 
	 * This is mandatory for domestic shipments and shipments to International Kahala Posts Group. 
	 * It is optional for other International shipments.
	 * 
	 * @param postcode The postcode for the address
	 * @since Oct 2014
	 */
	void setPostcode(Postcode postcode);
	Postcode getPostcode();
	
	/**
	 * [Optional]<br>
	 * <br>
	 * The country code for the address. The country code must conform to 
	 * <a href="https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2">ISO 3166-1 alpha-2 standard</a>.
	 * 
	 * @param country The country code for the address
	 * @since Oct 2014
	 */
	void setCountry(Country country);
	Country getCountry();
	
	/**
	 * [Optional]<br>
	 * <br>
	 * The receiver’s phone number. For StarTrack shipments, the maximum length is 10 characters.
	 * For AusPost shipments, the phone number must be between 10 and 20 characters in length and the allowable 
	 * characters are "()- 0-9" (digits, space, hyphen and parentheses.) Examples are: 0491-570-156, 02) 5550 1234, 
	 * +61 (3) 7010 4321.
	 * 
	 * @param number The sender’s phone number
	 * @since Oct 2014
	 */
	void setPhone(PhoneNumber number);
	PhoneNumber getPhone();
	
	/**
	 * [Optional]<br>
	 * <br>
	 * The receiver's email address in 50 characters.<br>
	 * 
	 * @param email The sender’s email address.
	 * @since Oct 2014
	 */
	void setEmail(EmailAddress email);
	EmailAddress getEmail();
	
	/**
	 * [Optional]<br>
	 * <br>
	 * Instructions to aid in prompt delivery of the item in 128 characters.<br>
	 * Note - For StarTrack products, delivery instructions is an array of strings with a maximum 
	 * 		  of 3 lines, each with a maximum of 25 characters. 
	 * 		  Delivery instructions for StarTrack products will be printed as "Special instructions" 
	 * 		  on labels.
	 * 
	 * @param email The sender’s email address.
	 * @since Oct 2014
	 */
	void setDeliveryInstructions(DeliveryInstruction instruction);
	DeliveryInstruction getDeliveryInstructions();
	
	Boolean validate();
}
