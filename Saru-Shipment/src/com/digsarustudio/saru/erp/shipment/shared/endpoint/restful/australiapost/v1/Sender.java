/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Country;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber;

/**
 * The details of a sender.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 */
public interface Sender {
	/**
	 * 
	 * The builder for {@link Sender}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Sender> {
		/**
		 * [Mandatory]<br>
		 * <br>
		 * The name of the sender in 40 characters.<br>
		 * 
		 * @param name The name of the sender
		 * @since Oct 2014
		 */
		Builder setName(PersonalName name);
		
		/**
		 * [Optional]<br>
		 * <br>
		 * The type of address. If this field is supplied, its value must be "MERCHANT_LOCATION".<br>
		 * 
		 * @param type The type of address.
		 * @since Oct 2014
		 */
		Builder setType(AddressType type);
		
		/**
		 * [Mandatory]<br>
		 * <br>
		 * The address lines of the address. 
		 * Minimum of one address line, up to a maximum of three. 
		 * Each address line is a maximum of 40 characters.<br>
		 * 
		 * @param addresslines The array of address lines.
		 * @since Oct 2014
		 */
		Builder setAddressLines(AddressLines addresslines);
		
		/**
		 * [Mandatory]<br>
		 * <br>
		 * The suburb for the address in 30 characters.<br>
		 * 
		 * @param suburb The suburb for the address.
		 * @since Oct 2014
		 */
		Builder setSuburb(Suburb suburb);
		
		/**
		 * [Mandatory]<br>
		 * <br>
		 * The state code for the address in 3 characters. 
		 * Valid values are: ACT, NSW, NT, QLD, SA, TAS, VIC, WA.<br>
		 * 
		 * @param state The state code for the address
		 * @since Oct 2014
		 */
		Builder setState(State state);
		
		/**
		 * [Mandatory]<br>
		 * <br>
		 * The postcode for the address in 4 characters.<br>
		 * 
		 * @param postcode The postcode for the address
		 * @since Oct 2014
		 */
		Builder setPostcode(Postcode postcode);
		
		/**
		 * [Optional]<br>
		 * <br>
		 * The country code for the address in 2 character.
		 * It must be "AU" if specified.<br>
		 * 
		 * @param country The country code for the address
		 * @since Oct 2014
		 */
		Builder setCountry(Country country);
		
		/**
		 * [Optional]<br>
		 * <br>
		 * The sender’s phone number. For StarTrack shipments, the maximum length is 10 characters.
		 * For AusPost shipments, the phone number must be between 10 and 20 characters in length and 
		 * the allowable characters are "()- 0-9" (digits, space, hyphen and parentheses.) 
		 * Examples are: 0491-570-156, 02) 5550 1234, +61 (3) 7010 4321.<br>
		 * 
		 * @param number The sender’s phone number
		 * @since Oct 2014
		 */
		Builder setPhone(PhoneNumber number);
		
		/**
		 * [Optional]<br>
		 * <br>
		 * The sender’s email address in 50 characters.<br>
		 * 
		 * @param email The sender’s email address.
		 * @since Oct 2014
		 */
		Builder setEmail(EmailAddress email);
	}
	
	/**
	 * [Mandatory]<br>
	 * <br>
	 * The name of the sender in 40 characters.<br>
	 * 
	 * @param name The name of the sender
	 * @since Oct 2014
	 */
	void setName(PersonalName name);
	PersonalName getName();
	
	/**
	 * [Optional]<br>
	 * <br>
	 * The type of address. If this field is supplied, its value must be "MERCHANT_LOCATION".<br>
	 * 
	 * @param type The type of address.
	 * @since Oct 2014
	 */
	void setType(AddressType type);
	AddressType getType();
	
	/**
	 * [Mandatory]<br>
	 * <br>
	 * The address lines of the address. 
	 * Minimum of one address line, up to a maximum of three. 
	 * Each address line is a maximum of 40 characters.<br>
	 * 
	 * @param addresslines The array of address lines.
	 * @since Oct 2014
	 */
	void setAddressLines(AddressLines addresslines);
	AddressLines getAddressLines();
	
	/**
	 * [Mandatory]<br>
	 * <br>
	 * The suburb for the address in 30 characters.<br>
	 * 
	 * @param suburb The suburb for the address.
	 * @since Oct 2014
	 */
	void setSuburb(Suburb suburb);
	Suburb getSuburb();
	
	/**
	 * [Mandatory]<br>
	 * <br>
	 * The state code for the address in 3 characters. 
	 * Valid values are: ACT, NSW, NT, QLD, SA, TAS, VIC, WA.<br>
	 * 
	 * @param state The state code for the address
	 * @since Oct 2014
	 */
	void setState(State state);
	State getState();
	
	/**
	 * [Mandatory]<br>
	 * <br>
	 * The postcode for the address in 4 characters.<br>
	 * 
	 * @param postcode The postcode for the address
	 * @since Oct 2014
	 */
	void setPostcode(Postcode postcode);
	Postcode getPostcode();
	
	/**
	 * [Optional]<br>
	 * <br>
	 * The country code for the address in 2 character.
	 * It must be "AU" if specified.<br>
	 * 
	 * @param country The country code for the address
	 * @since Oct 2014
	 */
	void setCountry(Country country);
	Country getCountry();
	
	/**
	 * [Optional]<br>
	 * <br>
	 * The sender’s phone number. For StarTrack shipments, the maximum length is 10 characters.
	 * For AusPost shipments, the phone number must be between 10 and 20 characters in length and 
	 * the allowable characters are "()- 0-9" (digits, space, hyphen and parentheses.) 
	 * Examples are: 0491-570-156, 02) 5550 1234, +61 (3) 7010 4321.<br>
	 * 
	 * @param number The sender’s phone number
	 * @since Oct 2014
	 */
	void setPhone(PhoneNumber number);
	PhoneNumber getPhone();
	
	/**
	 * [Optional]<br>
	 * <br>
	 * The sender’s email address in 50 characters.<br>
	 * 
	 * @param email The sender’s email address.
	 * @since Oct 2014
	 */
	void setEmail(EmailAddress email);
	EmailAddress getEmail();
	
	Boolean validate();
}
