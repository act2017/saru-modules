/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains the dangerous goods class or division. 
 * This field is a decimal value, but must be passed as a string.
 * Examples: 3, 1.4, 2.1.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://auspost.com.au/content/dam/auspost_corp/media/documents/dangerous-and-prohibited-goods-guide.pdf">Dangerous Goods Guide</a>
 *
 */
public class DangerousGoodsClassDivisions {
	public static final String CLASS_DELIMITER	= ".";
	
	public static final DangerousGoodsClassDivisions	Explosives	= new DangerousGoodsClassDivisions("2.1", "Explosives");

	
	private static Map<String, DangerousGoodsClassDivisions> VALUES = new HashMap<>();
		
	static {
		VALUES.put(Explosives.getClassification(), Explosives);
	}	
	
	private String 		 classification	= null;
	private String 		 name				= null;
	
	private DangerousGoodsClassDivisions() {
		
	}
	
	private DangerousGoodsClassDivisions(String classification, String name) {
		this.classification = classification;
		this.name			= name;
	}	
	
	/**
	 * @return the classification
	 */
	public String getClassification() {
		return classification;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	public static DangerousGoodsClassDivisions fromValue(String value) {
		if (null == value) {
			return null;
		}
		
		if (!VALUES.containsKey(value)) {
			return new DangerousGoodsClassDivisions(value, "Unknown classification");
		}
		
		return VALUES.get(value);
	}
	
	public static DangerousGoodsClassDivisions getDefault() {
		return Explosives;
	}
		
}
