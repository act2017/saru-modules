/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The parcel’s weight in kilograms. This must not be specified for flat-rate return shipments (movement_type = RETURN). 
 * However, it is mandatory for normal (outbound) shipments (movement_type = DESPATCH) and zonal returns (movement_type = RETURN). 
 * Your contract with Australia Post (not your API call) determines whether a return shipment has zonal pricing or flat-rate pricing. 
 * Note that most returns contracts are flat-rate.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface ItemWeight {
	/**
	 * 
	 * The builder for {@link ItemWeight}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemWeight> {
		Builder setWeight(Integer weight);
		Builder setWeight(Integer weight, Integer precision);
		Builder setWeight(String weight);
		Builder setWeight(String weight, String precision);
	}
	
	
	void setWeight(Integer weight);
	void setWeight(Integer weight, Integer precision);
	void setWeight(String weight);
	void setWeight(String weight, String precision);
	
	Integer getWeight();
	Integer getPrecision();
	
	String getDecimalWeight();
	
	Boolean validate();
}
