/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The number of items in the shipment for each delivery status. 
 * The delivery status will be listed with the number of items in the shipment where the delivery status 
 * has been applied.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface TrackingSummaryPair {
	/**
	 * 
	 * The builder for {@link TrackingSummaryPair}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<TrackingSummaryPair> {
		Builder setName(String name);
		Builder setValue(String value);
	}
	
	void setName(String name);
	void setValue(String value);
	
	String getName();
	String getValue();
	
	Boolean validate();
}
