/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.LicenceNumber}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LicenceNumber implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.LicenceNumber
									, IsSerializable {
	private static final Integer MAX_LEN	= 35;
	
	/**
	 * 
	 * The object builder for {@link LicenceNumber}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.LicenceNumber.Builder {
		private LicenceNumber result = null;

		private Builder() {
			this.result = new LicenceNumber();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.LicenceNumber.Builder#setNumber(java.lang.String)
		 */
		@Override
		public Builder setNumber(String number) {
			this.result.setNumber(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public LicenceNumber build() {
			return this.result;
		}

	}
	
	private String number	= null;

	/**
	 * 
	 */
	private LicenceNumber() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.LicenceNumber#setNumber(java.lang.String)
	 */
	@Override
	public void setNumber(String number) {
		this.number = number;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.LicenceNumber#getNumber()
	 */
	@Override
	public String getNumber() {
		
		return this.number;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.LicenceNumber#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.number || this.number.isEmpty()) {
			return false;
		}
		
		if(MAX_LEN < this.number.length()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
