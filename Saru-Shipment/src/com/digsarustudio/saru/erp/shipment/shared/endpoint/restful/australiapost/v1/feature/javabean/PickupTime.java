/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.ShipmentFeatureAttribute;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.ShipmentFeatureAttributeTime;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The pick up date of {@link ShipmentFeature}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class PickupTime implements ShipmentFeature, IsSerializable {
	public static final String NAME			= "PICKUP_TIME";
	public static final String ATTR_TIME	= "time";

	/**
	 * 
	 * The object builder for {@link PickupTime}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ShipmentFeature.PickupTimeBuilder {
		private PickupTime result = null;

		private Builder() {
			this.result = new PickupTime();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature.PickupTimeBuilder#setTime(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time)
		 */
		@Override
		public PickupTimeBuilder setTime(Time time) {
			this.result.setValue(time);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature.PickupTimeBuilder#setTime(java.lang.String)
		 */
		@Override
		public PickupTimeBuilder setTime(String time) {
			this.result.setValue(time);
			return this;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature.PickupTimeBuilder#setTime(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean.ShipmentFeatureAttributeTime)
		 */
		@Override
		public PickupTimeBuilder setTime(ShipmentFeatureAttributeTime time) {
			this.result.setAttribute(time);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public PickupTime build() {
			return this.result;
		}

	}
	
	private ShipmentFeatureAttribute<?>	date	= null;
	
	/**
	 * 
	 */
	private PickupTime() {
	}
	
	public void setValue(String time) {
		this.setAttribute(ShipmentFeatureAttributeTime.builder().setValue(time)
				  												.build());
	}
	
	public void setValue(Time time) {
		this.setAttribute(ShipmentFeatureAttributeTime.builder().setValue(time)
				  												.build());
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature#setAttribute(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.ShipmentFeatureAttribute)
	 */
	@Override
	public void setAttribute(ShipmentFeatureAttribute<?> attribute) {
		this.date = attribute;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature#getAttribute()
	 */
	@Override
	public ShipmentFeatureAttribute<?> getAttribute() {
		return this.date;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.date || !this.date.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
