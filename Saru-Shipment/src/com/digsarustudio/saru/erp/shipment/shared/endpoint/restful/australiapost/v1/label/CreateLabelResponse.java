/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;

/**
 * The response from Australia Post Create Label API.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface CreateLabelResponse {
	/**
	 * 
	 * The builder for {@link CreateLabelResponse}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<CreateLabelResponse> {
		Builder setGenerationProgressMessage(LabelGenerationProgressMessage msg);
		Builder setGenerationProgressCode(LabelGenerationProgressCode code);
		Builder addLabel(Label label);
		Builder setLabels(List<Label> labels);	
		Builder setLabels(LabelCollection labels);
		
		Builder setAusPostErrors(AusPostErrorCollection errors);
		Builder setAusPostErrors(List<AusPostError> errors);
		Builder addAusPostError(AusPostError error);
	}
	
	void setGenerationProgressMessage(LabelGenerationProgressMessage msg);
	void setGenerationProgressCode(LabelGenerationProgressCode code);
	void addLabel(Label label);
	void setLabels(List<Label> labels);	
	void setLabels(LabelCollection labels);
	
	void setAusPostErrors(AusPostErrorCollection errors);
	void setAusPostErrors(List<AusPostError> errors);
	void addAusPostError(AusPostError error);
	
	LabelGenerationProgressMessage	getGenerationProgressMessage();
	LabelGenerationProgressCode 	getGenerationProgressCode();
	Label getLabel(Integer index);	
	LabelCollection getLabels();
	
	AusPostErrorCollection getAusPostErrors();
	
	Boolean validate();	
}
