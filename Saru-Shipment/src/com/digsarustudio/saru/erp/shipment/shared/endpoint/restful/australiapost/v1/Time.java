/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Time including hours, minutes, and seconds.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Time {
	public static final String DELIMITER			= ":";
	public static final String TIME_ZONE_DELIMITER	= "\\+";
	
	/**
	 * 
	 * The builder for {@link Time}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Time> {
		Builder setHour(Integer hr);
		Builder setMinute(Integer min);
		Builder setSecond(Integer sec);
		Builder setTime(String time);
	}
	
	void setHour(Integer hr);
	void setMinute(Integer min);
	void setSecond(Integer sec);
	void setTime(String time);
	void setTimezone(String timezone);
	
	String getTime();
	
	Integer getHour();
	Integer getMinute();
	Integer getSecond();
	Time	getTimezone();
	
	String getDatabaseDateTime();
	
	Boolean validate();
}
