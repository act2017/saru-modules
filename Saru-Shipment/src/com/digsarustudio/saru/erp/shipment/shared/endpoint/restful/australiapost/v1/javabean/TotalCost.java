/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class TotalCost implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost
								, IsSerializable {
	public static final Integer	MAX_LEN_PRECISION	= 2;
	public static final String	DECIMAL_DELIMITER	= "\\.";
	public static final String	DECIMAL_GLUE		= ".";
	
	/**
	 * 
	 * The object builder for {@link TotalCost}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost.Builder {
		private TotalCost result = null;

		private Builder() {
			this.result = new TotalCost();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost.Builder#setValue(java.lang.Integer)
		 */
		@Override
		public Builder setValue(Integer cost) {
			this.result.setValue(cost);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost.Builder#setValue(java.lang.Integer, java.lang.Integer)
		 */
		@Override
		public Builder setValue(Integer cost, Integer precision) {
			this.result.setValue(cost, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String cost) {
			this.result.setValue(cost);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost.Builder#setValue(java.lang.String, java.lang.String)
		 */
		@Override
		public Builder setValue(String cost, String precision) {
			this.result.setValue(cost, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public TotalCost build() {
			return this.result;
		}

	}
	
	private String main			= null;
	private String precision	= null;
	
	/**
	 * 
	 */
	private TotalCost() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost#setValue(java.lang.Integer)
	 */
	@Override
	public void setValue(Integer cost) {
		this.setValue(cost, null);

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost#setValue(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void setValue(Integer cost, Integer precision) {
		String main = (null == cost) ? null : cost.toString();
		String pre	= (null == precision) ? null : precision.toString();

		this.setValue(main, pre);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String cost) {
		if(null == cost) {
			return;
		}

		String[] values = cost.split(DECIMAL_DELIMITER);
		
		String main = values[0];
		String pre	= null;
		
		if(2 == values.length) {
			pre = values[1];
		}
		
		this.setValue(main, pre);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost#setValue(java.lang.String, java.lang.String)
	 */
	@Override
	public void setValue(String cost, String precision) {
		this.main 		= cost;
		this.precision	= precision;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost#getCost()
	 */
	@Override
	public String getCost() {
		return this.main;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost#getPrcision()
	 */
	@Override
	public String getPrcision() {
		return this.precision;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost#getDecimalCost()
	 */
	@Override
	public String getDecimalCost() {
		if( !this.hasMain() && !this.hasPrecision() ) {
			return null;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		if( !this.hasMain() ) {
			buffer.append("0");
		}else {
			buffer.append(this.main);
		}
		
		if( this.hasPrecision() ) {
			buffer.append(DECIMAL_GLUE);
			buffer.append(this.precision);
		}
		
		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.TotalCost#validate()
	 */
	@Override
	public Boolean validate() {
		if( !this.hasMain() && !this.hasPrecision() ) {
			return false;
		}
		
		if( this.hasPrecision() && MAX_LEN_PRECISION < this.precision.length() ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	private Boolean hasMain() {
		return (null != this.main && !this.main.isEmpty());
	}
	
	private Boolean hasPrecision() {
		return (null != this.precision && !this.precision.isEmpty());
	}
}
