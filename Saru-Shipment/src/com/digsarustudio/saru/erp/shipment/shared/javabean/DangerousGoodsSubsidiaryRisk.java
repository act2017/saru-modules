/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.javabean;

import com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsSubsidiaryRisks;
import com.digsarustudio.saru.erp.shipment.shared.SubsidiaryRisk;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.DangerousGoods;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The subsidiary risk used by {@link DangerousGoods}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DangerousGoodsSubsidiaryRisk implements SubsidiaryRisk, IsSerializable {
	public static final Integer	MAX_LEN	= 4;
	
	/**
	 * 
	 * The object builder for {@link DangerousGoodsSubsidiaryRisk}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements SubsidiaryRisk.Builder {
		private DangerousGoodsSubsidiaryRisk result = null;

		private Builder() {
			this.result = new DangerousGoodsSubsidiaryRisk();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.SubsidiaryRisk.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.SubsidiaryRisk.Builder#setValue(com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsSubsidiaryRisks)
		 */
		@Override
		public Builder setValue(DangerousGoodsSubsidiaryRisks value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DangerousGoodsSubsidiaryRisk build() {
			return this.result;
		}

	}
	
	private String	risk	= null;

	/**
	 * 
	 */
	private DangerousGoodsSubsidiaryRisk() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.SubsidiaryRisk#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.risk = value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.SubsidiaryRisk#setValue(com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsSubsidiaryRisks)
	 */
	@Override
	public void setValue(DangerousGoodsSubsidiaryRisks value) {
		this.risk = value.getClassification();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.SubsidiaryRisk#getSubsidiaryRisk()
	 */
	@Override
	public DangerousGoodsSubsidiaryRisks getSubsidiaryRisk() {
		return DangerousGoodsSubsidiaryRisks.fromValue(this.risk);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.SubsidiaryRisk#getValue()
	 */
	@Override
	public String getValue() {
		return this.risk;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.SubsidiaryRisk#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.risk || this.risk.isEmpty()) {
			return false;
		}
		
		if( MAX_LEN < this.risk.length() ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
