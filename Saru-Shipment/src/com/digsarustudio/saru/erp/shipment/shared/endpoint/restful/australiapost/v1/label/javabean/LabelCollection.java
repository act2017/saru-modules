/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.javabean;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelCollection implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection
										, IsSerializable {

	/**
	 * 
	 * The object builder for {@link LabelCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection.Builder {
		private LabelCollection result = null;

		private Builder() {
			this.result = new LabelCollection();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection.Builder#addLabel(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label)
		 */
		@Override
		public Builder addLabel(Label label) {
			this.result.addLabel(label);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection.Builder#setLabels(java.util.List)
		 */
		@Override
		public Builder setLabels(List<Label> labels) {
			this.result.setLabels(labels);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public LabelCollection build() {
			return this.result;
		}

	}
	
	private List<Label>	labels	= null;
	
	/**
	 * 
	 */
	private LabelCollection() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection#addLabel(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.Label)
	 */
	@Override
	public void addLabel(Label label) {
		if(null == label) {
			return;
		}else if(null == this.labels) {
			this.labels = new ArrayList<Label>();
		}

		this.labels.add(label);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection#setLabels(java.util.List)
	 */
	@Override
	public void setLabels(List<Label> labels) {
		this.labels = labels;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection#getLabel(java.lang.Integer)
	 */
	@Override
	public Label getLabel(Integer index) {
		if(null == this.labels || null == index || index >= this.labels.size()) {
			return null;
		}
		
		return this.labels.get(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection#getLabels()
	 */
	@Override
	public List<Label> getLabels() {
		
		return this.labels;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.LabelCollection#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.labels || this.labels.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
