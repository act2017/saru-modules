/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.CubicVolume}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class CubicVolume implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.CubicVolume
								  , IsSerializable {
	public static final Integer MAX_LEN_MAIN		= 5;
	public static final Integer	MAX_LEN_PRECISION	= 7;
	public static final String	DECIMAL_DELIMITER	= "\\.";
	public static final String	DECIMAL_GLUE		= ".";
	
	/**
	 * 
	 * The object builder for {@link CubicVolume}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.CubicVolume.Builder {
		private CubicVolume result = null;

		private Builder() {
			this.result = new CubicVolume();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.CubicVolume.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.CubicVolume.Builder#setValue(java.lang.Integer)
		 */
		@Override
		public Builder setValue(Integer value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.CubicVolume.Builder#setValue(java.lang.Integer, java.lang.Integer)
		 */
		@Override
		public Builder setValue(Integer main, Integer precision) {
			this.result.setValue(main, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.CubicVolume.Builder#setValue(java.lang.String, java.lang.String)
		 */
		@Override
		public Builder setValue(String main, String precision) {
			this.result.setValue(main, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public CubicVolume build() {
			return this.result;
		}

	}
	
	private String main			= null;
	private String precision	= null;
	
	/**
	 * 
	 */
	private CubicVolume() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.CubicVolume#setValue(java.lang.Integer)
	 */
	@Override
	public void setValue(Integer value) {
		this.setValue(value, null);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.CubicVolume#setValue(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void setValue(Integer main, Integer precision) {
		String one = (null == main) ? null : main.toString();
		String two = (null == precision) ? null : precision.toString();
		
		this.setValue(one, two);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.CubicVolume#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		if(null == value) {
			return;
		}
		
		String[] weights = value.split(DECIMAL_DELIMITER);
		
		String main = weights[0];
		String pre	= null;
		
		if(2 == weights.length) {
			pre = weights[1];
		}
		
		this.setValue(main, pre);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.CubicVolume#setValue(java.lang.String, java.lang.String)
	 */
	@Override
	public void setValue(String main, String precision) {
		this.main = main;
		this.precision = precision;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.CubicVolume#getValue()
	 */
	@Override
	public String getValue() {
		if(null == this.main && null == this.precision) {
			return null;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		if(null != this.main && !this.main.isEmpty()) {
			buffer.append(this.main);
		}else {
			buffer.append("0");
		}
		
		if(null != this.precision) {
			buffer.append(DECIMAL_GLUE);
			buffer.append(this.precision);
		}

		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.CubicVolume#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.main && null == this.precision) {
			return false;
		}
		
		if(this.main.isEmpty() && this.precision.isEmpty()) {
			return false;
		}
		
		if( null != this.main 
			&& MAX_LEN_MAIN < this.main.length() ) {
			return false;
		}
		
		if( null != this.precision 
			&& (MAX_LEN_PRECISION < this.precision.length()) ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
