/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The total price of the shipment, including goods and services tax (G.S.T). 
 * This field is not used for StarTrack shipments, as they are costed at the shipment level only.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface TotalCost {
	/**
	 * 
	 * The builder for {@link TotalGst}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<TotalCost> {
		Builder setValue(Integer cost);
		Builder setValue(Integer cost, Integer precision);

		Builder setValue(String cost);
		Builder setValue(String cost, String precision);
	}
	
	void setValue(Integer cost);
	void setValue(Integer cost, Integer precision);

	void setValue(String cost);
	void setValue(String cost, String precision);
	
	String getCost();
	String getPrcision();
	String getDecimalCost();
	
	Boolean validate();

}
