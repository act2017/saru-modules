/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Description of the outer packages in which the dangerous goods are contained.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface DangerousGoodsPackagingType {
	/**
	 * 
	 * The builder for {@link DangerousGoodsPackagingType}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<DangerousGoodsPackagingType> {
		Builder setDescription(String description);
	}

	void setDescription(String description);
	String getDescription();
	
	Boolean validate();
}
