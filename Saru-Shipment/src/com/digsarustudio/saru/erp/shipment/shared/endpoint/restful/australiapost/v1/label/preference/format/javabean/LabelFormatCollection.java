/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatCollection}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelFormatCollection implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatCollection
											, IsSerializable {
	public static final String	ATTR_GROUPS	= "groups";

	/**
	 * 
	 * The object builder for {@link LabelFormatCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatCollection.Builder {
		private LabelFormatCollection result = null;

		private Builder() {
			this.result = new LabelFormatCollection();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatCollection.Builder#addFormat(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat)
		 */
		@Override
		public Builder addFormat(LabelFormat format) {
			this.result.addFormat(format);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatCollection.Builder#setFormats(java.util.List)
		 */
		@Override
		public Builder setFormats(List<LabelFormat> formats) {
			this.result.setFormats(formats);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public LabelFormatCollection build() {
			return this.result;
		}

	}
	
	private List<LabelFormat>	groups	= null;
	
	/**
	 * 
	 */
	private LabelFormatCollection() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatCollection#addFormat(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormat)
	 */
	@Override
	public void addFormat(LabelFormat format) {
		if(null == this.groups) {
			this.groups = new ArrayList<>();
		}

		this.groups.add(format);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatCollection#setFormats(java.util.List)
	 */
	@Override
	public void setFormats(List<LabelFormat> formats) {
		this.groups = formats;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatCollection#getFormat(java.lang.Integer)
	 */
	@Override
	public LabelFormat getFormat(Integer index) {
		if(null == this.groups || null == index || index >= this.groups.size()) {
			return null;
		}
		
		return this.groups.get(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatCollection#getFormats()
	 */
	@Override
	public List<LabelFormat> getFormats() {
		
		return this.groups;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatCollection#size()
	 */
	@Override
	public Integer size() {
		if(null == this.groups) {
			return 0;
		}
		
		return this.groups.size();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatCollection#isEmpty()
	 */
	@Override
	public Boolean isEmpty() {
		if(null == this.groups) {
			return true;
		}
		
		return this.groups.isEmpty();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatCollection#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.groups || this.groups.isEmpty()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
