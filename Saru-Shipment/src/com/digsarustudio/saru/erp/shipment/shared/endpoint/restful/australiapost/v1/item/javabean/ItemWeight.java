/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWeight}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemWeight implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWeight
								 , IsSerializable {
	public static final Integer	MAX_LEN_PRECISION	= 3;
	public static final String DECIMAL_DELIMITER	= "\\.";
	public static final String DECIMAL_GLUE			= ".";
	
	/**
	 * 
	 * The object builder for {@link ItemWeight}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWeight.Builder {
		private ItemWeight result = null;

		private Builder() {
			this.result = new ItemWeight();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWeight.Builder#setWeight(java.lang.Integer)
		 */
		@Override
		public Builder setWeight(Integer weight) {
			this.result.setWeight(weight);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWeight.Builder#setWeight(java.lang.Integer, java.lang.Integer)
		 */
		@Override
		public Builder setWeight(Integer weight, Integer precision) {
			this.result.setWeight(weight, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWeight.Builder#setWeight(java.lang.String)
		 */
		@Override
		public Builder setWeight(String weight) {
			this.result.setWeight(weight);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWeight.Builder#setWeight(java.lang.String, java.lang.String)
		 */
		@Override
		public Builder setWeight(String weight, String precision) {
			this.result.setWeight(weight, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemWeight build() {
			return this.result;
		}

	}
	
	private String main			= null;
	private String precision	= null;

	/**
	 * 
	 */
	private ItemWeight() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWeight#setWeight(java.lang.Integer)
	 */
	@Override
	public void setWeight(Integer weight) {
		this.setWeight(weight, null);

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWeight#setWeight(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void setWeight(Integer weight, Integer precision) {
		String main = (null == weight) ? null : weight.toString();
		String pre	= (null == precision) ? null : precision.toString();
		
		this.setWeight(main, pre);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWeight#setWeight(java.lang.String)
	 */
	@Override
	public void setWeight(String weight) {
		if(null == weight) {
			return;
		}
		
		String[] weights = weight.split(DECIMAL_DELIMITER);
		
		String main = weights[0];
		String pre	= null;
		
		if(2 == weights.length) {
			pre = weights[1];
		}
		
		this.setWeight(main, pre);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWeight#setWeight(java.lang.String, java.lang.String)
	 */
	@Override
	public void setWeight(String weight, String precision) {
		this.main = weight;
		this.precision = precision;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWeight#getWeight()
	 */
	@Override
	public Integer getWeight() {
		if(null == this.main) {
			return null;
		}
		
		return Integer.parseInt(this.main);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWeight#getPrecision()
	 */
	@Override
	public Integer getPrecision() {
		if(null == this.precision) {
			return null;
		}
		
		return Integer.parseInt(this.precision);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWeight#getDecimalWeight()
	 */
	@Override
	public String getDecimalWeight() {
		if( !this.hasMain() && !this.hasPrecision() ) {
			return null;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		if( !this.hasMain() ) {
			buffer.append("0");
		}else {
			buffer.append(this.main);
		}
		
		if( this.hasPrecision() ) {
			buffer.append(DECIMAL_GLUE);
			buffer.append(this.precision);
		}
		
		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemWeight#validate()
	 */
	@Override
	public Boolean validate() {
		if( !this.hasMain() && !this.hasPrecision() ) {
			return false;
		}
		
		if( this.hasPrecision() && MAX_LEN_PRECISION < this.precision.length()) {
			return false;
		}
		
		return true;
	}
	
	private Boolean hasMain() {
		return (null != this.main && !this.main.isEmpty());
	}
	
	private Boolean hasPrecision() {
		return (null != this.precision && !this.precision.isEmpty());
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
