/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The merchant's internal reference ID for a returns or transfer shipment. 
 * This is mandatory for StarTrack if movement_type is "RETURN" and "TRANSFER".
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface AuthorisationNumber {
	/**
	 * 
	 * The builder for {@link AuthorisationNumber}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<AuthorisationNumber> {
		Builder setValue(String number);
	}
	
	void setValue(String number);
	String getValue();
	
	Boolean validate();
}
