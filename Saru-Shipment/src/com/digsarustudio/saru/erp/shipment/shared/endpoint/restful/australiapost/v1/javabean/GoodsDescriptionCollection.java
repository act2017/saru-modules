/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescription;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescriptionCollection}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class GoodsDescriptionCollection implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescriptionCollection
												 , IsSerializable {
	public static final String	ATTR_GOODS_DESCRIPTIONS	= "goods_descriptions";
	public static final Integer	MAX_COUNT				= 3;

	/**
	 * 
	 * The object builder for {@link GoodsDescriptionCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescriptionCollection.Builder {
		private GoodsDescriptionCollection result = null;

		private Builder() {
			this.result = new GoodsDescriptionCollection();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescriptionCollection.Builder#addDescription(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescription)
		 */
		@Override
		public Builder addDescription(GoodsDescription description) {
			this.result.addDescription(description);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescriptionCollection.Builder#setDescriptions(java.util.List)
		 */
		@Override
		public Builder setDescriptions(List<GoodsDescription> descriptions) {
			this.result.setDescriptions(descriptions);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescriptionCollection.Builder#setDescriptions(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescription[])
		 */
		@Override
		public Builder setDescriptions(GoodsDescription[] descriptions) {
			this.result.setDescriptions(descriptions);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public GoodsDescriptionCollection build() {
			return this.result;
		}

	}
	
	
	private List<GoodsDescription> goods_descriptions	= null;
	
	/**
	 * 
	 */
	private GoodsDescriptionCollection() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescriptionCollection#addDescription(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescription)
	 */
	@Override
	public void addDescription(GoodsDescription description) {
		if(null == this.goods_descriptions) {
			this.goods_descriptions = new ArrayList<GoodsDescription>();
		}

		this.goods_descriptions.add(description);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescriptionCollection#setDescriptions(java.util.List)
	 */
	@Override
	public void setDescriptions(List<GoodsDescription> description) {
		this.goods_descriptions = description;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescriptionCollection#setDescriptions(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescription[])
	 */
	@Override
	public void setDescriptions(GoodsDescription[] descriptions) {
		this.setDescriptions(Arrays.asList(descriptions));
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescriptionCollection#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.goods_descriptions || this.goods_descriptions.isEmpty()) {
			return false;
		}
		
		if( this.isOverSize(this.goods_descriptions) ) {
			return false;
		}
		
		return true;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescriptionCollection#getDescription(java.lang.Integer)
	 */
	@Override
	public GoodsDescription getDescription(Integer index) {
		if(null == this.goods_descriptions || index >= this.goods_descriptions.size()) {
			return null;
		}
		
		return this.goods_descriptions.get(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescriptionCollection#getDescriptions()
	 */
	@Override
	public List<GoodsDescription> getDescriptions() {
		
		return this.goods_descriptions;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescriptionCollection#size()
	 */
	@Override
	public Integer size() {
		if(null == this.goods_descriptions) {
			return 0;
		}
		
		return this.goods_descriptions.size();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	private Boolean isOverSize(List<GoodsDescription> descriptions) {
		return (null != descriptions && MAX_COUNT < descriptions.size());
	}
}
