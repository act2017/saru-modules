/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.item.javabean.gsonparser.ItemBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.javabean.gsonparser.ShipmentBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.LabelShipmentBiserialiser;
import com.digsarustudio.saru.erp.shipment.server.endpoint.restful.australiapost.v1.label.javabean.jsonparser.LabelShipmentItemBiserialiser;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.Item;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceCollection;

/**
 * This service initiates the generation of labels for the requested shipments that have been previously created using the Create Shipments service.
 * <br>
 * Please use {@link LabelShipmentBiserialiser} and {@link LabelShipmentItemBiserialiser} 
 * to parse {@link Shipment} and {@link Item} json string.<br>
 * Please do not use {@link ShipmentBiserialiser} and {@link ItemBiserialiser} to parse this.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		10.0.0
 * @since		1.0.0
 *
 */
public interface CreateLabelRequest {
	/**
	 * 
	 * The builder for {@link CreateLabelRequest}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<CreateLabelRequest> {
		Builder addPreference(LabelPreference preference);
		Builder setPreferences(List<LabelPreference> preferences);
		Builder setPreferences(LabelPreferenceCollection preferences);
		Builder addShipment(Shipment shipment);
		Builder setShipments(List<Shipment> shipments);
		Builder setShipments(ShipmentCollection shipments);
	}
	
	void addPreference(LabelPreference preference);
	void setPreferences(List<LabelPreference> preferences);
	void setPreferences(LabelPreferenceCollection preferences);
	void addShipment(Shipment shipment);
	void setShipments(List<Shipment> shipments);
	void setShipments(ShipmentCollection shipments);
	
	LabelPreference				getPreference(Integer index);
	LabelPreferenceCollection	getPreferences();
	
	Shipment 				getShipment(Integer index);
	ShipmentCollection		getShipments();
	
	Boolean validate();

}
