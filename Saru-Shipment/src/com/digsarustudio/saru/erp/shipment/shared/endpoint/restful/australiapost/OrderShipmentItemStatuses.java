/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Review the order, shipments, and items statuses returned by API services.<br>
 * Orders, shipments and items change state throughout their lifecycle, beginning with their creation within the system through to physical delivery of 
 * the related articles. The following table lists the various status values that are applicable:
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/statuses">Statuses</a>
 */
public class OrderShipmentItemStatuses implements IsSerializable{
	/**
	 * The item or items in the shipment have been created, but have not been finalised in an order.
	 */
	public static final OrderShipmentItemStatuses Created	= new OrderShipmentItemStatuses("Created", "Created");
	
	/**
	 * The shipment has been added to an order.
	 */
	public static final OrderShipmentItemStatuses Sealed	= new OrderShipmentItemStatuses("Sealed", "Sealed");
	
	/**
	 * The item or items in the shipment have been finalised in an order and will be delivered when the parcels are received by Australia Post.
	 */
	public static final OrderShipmentItemStatuses Initiated	= new OrderShipmentItemStatuses("Initiated", "Initiated");
	
	/**
	 * The item or items in the shipment are being delivered.
	 */
	public static final OrderShipmentItemStatuses InTransit	= new OrderShipmentItemStatuses("In transit", "In Transit");
	
	/**
	 * The item or items in the shipment have been delivered.
	 */
	public static final OrderShipmentItemStatuses Delivered = new OrderShipmentItemStatuses("Delivered", "Delivered");
	
	/**
	 * The item or items in the shipment are awaiting collection.
	 */
	public static final OrderShipmentItemStatuses AwaitingCollection	= new OrderShipmentItemStatuses("Awaiting collection", "Awaiting Collection");
	
	/**
	 * A delay to the delivery of item or items in the shipment is highly likely. Refer to the Australia Post website or call 13 21 31 for more information.
	 */
	public static final OrderShipmentItemStatuses PossibleDelay	= new OrderShipmentItemStatuses("Possible delay", "Possible Delay");
	
	/**
	 * The item or items in the shipment could not be collected by Australia Post for delivery.
	 */
	public static final OrderShipmentItemStatuses UnsuccessfulPickup	= new OrderShipmentItemStatuses("Unsuccessful pickup", "Unsuccessful Pickup");
	
	/**
	 * The item or items in the shipment were damaged during delivery.
	 */
	public static final OrderShipmentItemStatuses ArticleDamaged	= new OrderShipmentItemStatuses("Article damaged", "Article Damaged");
	
	/**
	 * Delivery of item or items in the shipment was cancelled.
	 */
	public static final OrderShipmentItemStatuses Cancelled	= new OrderShipmentItemStatuses("Cancelled", "Cancelled");
	
	/**
	 * The item or items in the shipment have been held by the courier.
	 */
	public static final OrderShipmentItemStatuses HeldByCourier = new OrderShipmentItemStatuses("Held by courier", "Held by Courier");
	
	/**
	 * The item or items in the shipment cannot be delivered as addressed.
	 */
	public static final OrderShipmentItemStatuses CannotBeDelivered	= new OrderShipmentItemStatuses("Cannot be delivered", "Cannot be delivered");
	
	/**
	 * A shipment level delivery summary cannot be determined, as the items in the shipment are at differing delivery statuses. Track the individual items in the shipment for detailed delivery information.
	 */
	public static final OrderShipmentItemStatuses TrackItemsForDetailedDeliveryInformation = new OrderShipmentItemStatuses("Track items for detailed delivery information", "Track Item for Detailed Delivery Information");

	private static Map<String, OrderShipmentItemStatuses> VALUES = new HashMap<>();
		
	static {
		VALUES.put(Created.getValue(), Created);
		VALUES.put(Sealed.getValue(), Sealed);
		VALUES.put(Initiated.getValue(), Initiated);
		VALUES.put(InTransit.getValue(), InTransit);
		VALUES.put(Delivered.getValue(), Delivered);
		VALUES.put(AwaitingCollection.getValue(), AwaitingCollection);
		VALUES.put(PossibleDelay.getValue(), PossibleDelay);
		VALUES.put(UnsuccessfulPickup.getValue(), UnsuccessfulPickup);
		VALUES.put(ArticleDamaged.getValue(), ArticleDamaged);
		VALUES.put(Cancelled.getValue(), Cancelled);
		VALUES.put(HeldByCourier.getValue(), HeldByCourier);
		VALUES.put(CannotBeDelivered.getValue(), CannotBeDelivered);
		VALUES.put(TrackItemsForDetailedDeliveryInformation.getValue(), TrackItemsForDetailedDeliveryInformation);
		
	}	
	
	private String value = null;
	private String display = null;
	
	private OrderShipmentItemStatuses() {
		
	}
	
	private OrderShipmentItemStatuses(String value, String display) {
		this.value = value;
		this.display = display;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public String getDisplayString() {
		return this.display;
	}
	
	public static OrderShipmentItemStatuses fromValue(String value) {
		if (null == value || !VALUES.containsKey(value)) {
			return null;
		}
		
		return VALUES.get(value);
	}
}
