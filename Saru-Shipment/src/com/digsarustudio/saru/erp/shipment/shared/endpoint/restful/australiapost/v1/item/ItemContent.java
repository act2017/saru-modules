/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.CountryOfOrigin;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentDescription;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentQuantity;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentValue;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentWeight;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.TariffCode;

/**
 * The contents of the item used by an item_contents element.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface ItemContent {
	/**
	 * 
	 * The builder for {@link ItemContent}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemContent> {
		/**
		 * [Mandatory]<br>
		 * <br>
		 * Description of the contents. This description is validated against a defined list of keywords, 
		 * to determine whether the item can be carried by Australia Post and/or the item is prohibited in 
		 * the destination country.<br>
		 * 
		 * @param description Description of the contents.
		 * 
		 * @since Oct 2014
		 * @see	<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
		 */
		Builder setDescription(ItemContentDescription description);
		
		/**
		 * [Mandatory]<br>
		 * <br>
		 * The quantity of this described content<br>
		 * 
		 * @param quantity The quantity of this described content
		 * 
		 * @since Oct 2014
		 * @see	<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
		 */
		Builder setQuantity(ItemContentQuantity quantity);
		
		/**
		 * [Optional]<br>
		 * <br>
		 * The weight in kilograms of this described content. If you provide a weight for one item_contents element, 
		 * you must provide weights for all item_contents elements.
		 * 
		 * @param weight The weight in kilograms of this described content
		 * 
		 * @since Oct 2014
		 * @see	<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
		 * 
		 */
		Builder setWeight(ItemContentWeight weight);
		
		/**
		 * [Mandatory]<br>
		 * <br>
		 * The value in Australian Dollars of this described content.
		 * 
		 * @param value The value in Australian Dollars of this described content.
		 * 
		 * @since Oct 2014
		 * @see	<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
		 * 
		 */
		Builder setValue(ItemContentValue value);
		
		/**
		 * [Mandatory] while comercial_value is true<br>
		 * [Optional] while comercial_value is false<br>
		 * <br>
		 * The Harmonised Tariff number that indicates the type of this described content. 
		 * It is only mandatory if commercial_value is "true", otherwise it is optional. 
		 * However, it is a recommended field to expedite the customs process in the receiving country.
		 * 
		 * @param code The Harmonised Tariff number that indicates the type of this described content.
		 * 
		 * @since Oct 2014
		 * @see	<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
		 * 
		 */
		Builder setTariffCode(TariffCode code);
		
		/**
		 * [Mandatory] while comercial_value is true<br>
		 * [Optional] while comercial_value is false<br>
		 * <br>
		 * The country in which this described content was manufactured. 
		 * It is only mandatory if commercial_value is "true", otherwise it is optional.
		 * 
		 * @param country The country in which this described content was manufactured.
		 * 
		 * @since Oct 2014
		 * @see	<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
		 * 
		 */
		Builder setCountryOfOrigin(CountryOfOrigin country);
		
		Builder setParent(Item parent);
	}
	
	/**
	 * [Mandatory]<br>
	 * <br>
	 * Description of the contents. This description is validated against a defined list of keywords, 
	 * to determine whether the item can be carried by Australia Post and/or the item is prohibited in 
	 * the destination country.<br>
	 * 
	 * @param description Description of the contents.
	 * 
	 * @since Oct 2014
	 * @see	<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
	 */
	void setDescription(ItemContentDescription description);
	ItemContentDescription getDescription();
	
	/**
	 * [Mandatory]<br>
	 * <br>
	 * The quantity of this described content<br>
	 * 
	 * @param quantity The quantity of this described content
	 * 
	 * @since Oct 2014
	 * @see	<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
	 */
	void setQuantity(ItemContentQuantity quantity);
	ItemContentQuantity getQuantity();
	
	/**
	 * [Optional]<br>
	 * <br>
	 * The weight in kilograms of this described content. If you provide a weight for one item_contents element, 
	 * you must provide weights for all item_contents elements.
	 * 
	 * @param weight The weight in kilograms of this described content
	 * 
	 * @since Oct 2014
	 * @see	<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
	 * 
	 */
	void setWeight(ItemContentWeight weight);
	ItemContentWeight getWeight();
	
	/**
	 * [Mandatory]<br>
	 * <br>
	 * The value in Australian Dollars of this described content.
	 * 
	 * @param value The value in Australian Dollars of this described content.
	 * 
	 * @since Oct 2014
	 * @see	<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
	 * 
	 */
	void setValue(ItemContentValue value);
	ItemContentValue getValue();
	
	/**
	 * [Mandatory] while comercial_value is true<br>
	 * [Optional] while comercial_value is false<br>
	 * <br>
	 * The Harmonised Tariff number that indicates the type of this described content. 
	 * It is only mandatory if commercial_value is "true", otherwise it is optional. 
	 * However, it is a recommended field to expedite the customs process in the receiving country.
	 * 
	 * @param code The Harmonised Tariff number that indicates the type of this described content.
	 * 
	 * @since Oct 2014
	 * @see	<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
	 * 
	 */
	void setTariffCode(TariffCode code);
	TariffCode getTariffCode();
	
	/**
	 * [Mandatory] while comercial_value is true<br>
	 * [Optional] while comercial_value is false<br>
	 * <br>
	 * The country in which this described content was manufactured. 
	 * It is only mandatory if commercial_value is "true", otherwise it is optional.
	 * 
	 * @param country The country in which this described content was manufactured.
	 * 
	 * @since Oct 2014
	 * @see	<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
	 * 
	 */
	void setCountryOfOrigin(CountryOfOrigin country);
	CountryOfOrigin getCountryOfOrigin();
	
	void setParent(Item parent);
	Item getParent();
	
	Boolean validate();
}
