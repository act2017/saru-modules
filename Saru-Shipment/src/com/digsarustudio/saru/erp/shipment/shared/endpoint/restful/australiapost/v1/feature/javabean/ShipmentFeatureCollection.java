/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeatureCollection}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ShipmentFeatureCollection implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeatureCollection
												, IsSerializable {
	public static final String	ATTR_DELIVERY_DATE	= "DELIVERY_DATE";
	public static final String	ATTR_DELIVERY_TIMES	= "DELIVERY_TIMES";
	public static final String	ATTR_PICKUP_DATE	= "PICKUP_DATE";
	public static final String	ATTR_PICKUP_TIME	= "PICKUP_TIME";
	
	/**
	 * 
	 * The object builder for {@link ShipmentFeatureCollection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeatureCollection.Builder {
		private ShipmentFeatureCollection result = null;

		private Builder() {
			this.result = new ShipmentFeatureCollection();
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeatureCollection.Builder#setDeliveryDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature)
		 */
		@Override
		public Builder setDeliveryDate(ShipmentFeature date) {
			this.result.setPickupTime(date);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeatureCollection.Builder#setDeliveryTimes(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature)
		 */
		@Override
		public Builder setDeliveryTimes(ShipmentFeature time) {
			this.result.setPickupTime(time);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeatureCollection.Builder#setPickupDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature)
		 */
		@Override
		public Builder setPickupDate(ShipmentFeature date) {
			this.result.setPickupTime(date);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeatureCollection.Builder#setPickupTime(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature)
		 */
		@Override
		public Builder setPickupTime(ShipmentFeature time) {
			this.result.setPickupTime(time);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ShipmentFeatureCollection build() {
			return this.result;
		}

	}

	private ShipmentFeature		deliveryDate	= null;
	private ShipmentFeature		deliveryTimes	= null;
	private ShipmentFeature		pickupDate		= null;
	private ShipmentFeature		pickupTime		= null;
	
	/**
	 * 
	 */
	private ShipmentFeatureCollection() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeatureCollection#setDeliveryDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature)
	 */
	@Override
	public void setDeliveryDate(ShipmentFeature date) {
		this.deliveryDate = date;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeatureCollection#getDeliveryDate()
	 */
	@Override
	public ShipmentFeature getDeliveryDate() {

		return this.deliveryDate;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeatureCollection#setDeliveryTimes(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature)
	 */
	@Override
	public void setDeliveryTimes(ShipmentFeature time) {
		this.deliveryTimes = time;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeatureCollection#getDeliveryTimes()
	 */
	@Override
	public ShipmentFeature getDeliveryTimes() {

		return this.deliveryTimes;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeatureCollection#setPickupDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature)
	 */
	@Override
	public void setPickupDate(ShipmentFeature date) {
		this.pickupDate = date;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeatureCollection#getPickupDate()
	 */
	@Override
	public ShipmentFeature getPickupDate() {

		return this.pickupDate;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeatureCollection#setPickupTime(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature)
	 */
	@Override
	public void setPickupTime(ShipmentFeature time) {
		this.pickupTime = time;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeatureCollection#getPickupTime()
	 */
	@Override
	public ShipmentFeature getPickupTime() {

		return this.pickupTime;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeatureCollection#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.deliveryDate || !this.deliveryDate.validate() ) {
			return false;
		}

		if(null == this.deliveryTimes || !this.deliveryTimes.validate() ) {
			return false;
		}

		if(null == this.pickupDate || !this.pickupDate.validate() ) {
			return false;
		}

		if(null == this.pickupTime || !this.pickupTime.validate() ) {
			return false;
		}
		
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
