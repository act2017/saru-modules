/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatOffset;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Use this field to adjust the left margin of the page.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelFormatLeftOffset implements LabelFormatOffset, IsSerializable {
	/**
	 * 
	 * The object builder for {@link LabelFormatLeftOffset}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements LabelFormatOffset.Builder {
		private LabelFormatLeftOffset result = null;

		private Builder() {
			this.result = new LabelFormatLeftOffset();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatOffset.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatOffset.Builder#setValue(java.lang.Integer)
		 */
		@Override
		public Builder setValue(Integer value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public LabelFormatLeftOffset build() {
			return this.result;
		}

	}

	private Integer	value	= null;
	
	/**
	 * 
	 */
	private LabelFormatLeftOffset() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatOffset#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		if(null == value || value.isEmpty()) {
			this.value = 0;
		}else {		
			this.value = Integer.parseInt(value);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatOffset#setValue(java.lang.Integer)
	 */
	@Override
	public void setValue(Integer value) {
		this.value = value;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatOffset#getValue()
	 */
	@Override
	public Integer getValue() {
		return this.value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatOffset#getValueString()
	 */
	@Override
	public String getValueString() {
		if(null == this.value) {
			return null;
		}
		
		return this.value.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelFormatOffset#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.value) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
