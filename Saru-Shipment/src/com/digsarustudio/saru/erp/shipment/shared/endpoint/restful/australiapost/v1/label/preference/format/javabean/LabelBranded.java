/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelBranded}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelBranded implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelBranded
									, IsSerializable {

	/**
	 * 
	 * The object builder for {@link LabelBranded}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelBranded.Builder	{
		private LabelBranded result = null;

		private Builder() {
			this.result = new LabelBranded();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelBranded.Builder#setBranded()
		 */
		@Override
		public Builder setBranded() {
			this.result.setBranded();
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelBranded.Builder#setUnbranded()
		 */
		@Override
		public Builder setUnbranded() {
			this.result.setUnbranded();
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public LabelBranded build() {
			return this.result;
		}

	}
	
	private Boolean value	= null;
	
	/**
	 * 
	 */
	private LabelBranded() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelBranded#setBranded()
	 */
	@Override
	public void setBranded() {
		this.value = true;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelBranded#setUnbranded()
	 */
	@Override
	public void setUnbranded() {
		this.value = false;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelBranded#isBranded()
	 */
	@Override
	public Boolean isBranded() {
		
		return this.value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.format.LabelBranded#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.value) {
			return false;
		}
		
		return this.value;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
