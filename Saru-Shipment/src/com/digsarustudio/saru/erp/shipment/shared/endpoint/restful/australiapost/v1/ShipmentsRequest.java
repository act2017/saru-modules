/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The HTTP request object which contains the details of created shipment by Australia Post Shipment API.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 */
public interface ShipmentsRequest {
	/**
	 * 
	 * The builder for {@link ShipmentsRequest}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ShipmentsRequest> {
		Builder addShipment(Shipment shipment);
		Builder setShipments(ShipmentCollection shipments);
		Builder setShipments(List<Shipment> shipments);
		Builder setShipments(Shipment[] shipments);
	}

	void addShipment(Shipment shipment);
	void setShipments(ShipmentCollection shipments);
	void setShipments(List<Shipment> shipments);
	void setShipments(Shipment[] shipments);
	
	Shipment getShipment(Integer index);
	List<Shipment> getShipments();
	ShipmentCollection getShipmentCollection();
	
	Boolean validate();
}
