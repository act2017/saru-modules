/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.ItemFeatureAttribute;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Percentage rate for calculating the TRANSIT_COVER cost.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 */
public class TransitCoverRate implements ItemFeatureAttribute, IsSerializable {
	public static final Integer	MAX_LEN_MAIN		= 2;
	public static final Integer	MAX_LEN_PRECISION	= 1;
	public static final String	DECIMAL_DELIMITER	= "\\.";
	public static final String	DECIMAL_GLUE		= ".";
	public static final	String	NAME				= "rate";

	/**
	 * 
	 * The object builder for {@link TransitCoverRate}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ItemFeatureAttribute.Builder {
		private TransitCoverRate result = null;

		private Builder() {
			this.result = new TransitCoverRate();
		}

		/**
		 * @param value
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverRate#setValue(java.lang.Integer)
		 */
		public Builder setValue(Integer value) {
			result.setValue(value);
			return this;
		}

		/**
		 * @param value
		 * @param precision
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverRate#setValue(java.lang.Integer, java.lang.Integer)
		 */
		public Builder setValue(Integer value, Integer precision) {
			result.setValue(value, precision);
			return this;
		}

		/**
		 * @param value
		 * @param precision
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.feature.attribute.javabean.TransitCoverRate#setValue(java.lang.String, java.lang.String)
		 */
		public Builder setValue(String value, String precision) {
			result.setValue(value, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute.Builder#setName(java.lang.String)
		 */
		@Override
		public Builder setName(String name) {
			this.result.setName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public TransitCoverRate build() {
			return this.result;
		}

	}
	
	private String name			= null;	
	private String	main		= null;
	private	String	precision	= null;
	
	/**
	 * 
	 */
	private TransitCoverRate() {
		this.setName(NAME);
	}

	public void setValue(Integer value) {
		this.setValue(value, null);
	}
	
	public void setValue(Integer value, Integer precision) {
		String main = (null == value) ? null : value.toString();
		String pre	= (null == precision) ? null : precision.toString();
		
		this.setValue(main, pre);
	}
	
	public void setValue(String value, String precision) {
		this.main		= value;
		this.precision	= precision;
	}
	
	public String getMain() {
		return this.main;
	}
	
	public String getPrecision() {
		return this.precision;
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		if(null == value || value.isEmpty()) {
			return;
		}
		
		String[] values = value.split(DECIMAL_DELIMITER);
		
		String main = values[0];
		String pre	= null;
		if(2 == values.length) {
			pre = values[1];
		}
		
		this.setValue(main, pre);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute#getValue()
	 */
	@Override
	public String getValue() {
		if( !this.hasMain() && !this.hasPrecision() ) {
			return null;
		}
		
		StringBuffer buffer = new StringBuffer();
		
		if( !this.hasMain() ) {
			buffer.append("0");
		}else {
			buffer.append(this.main);
		}
		
		if( this.hasPrecision() ) {
			buffer.append(DECIMAL_GLUE);
			buffer.append(this.precision);
		}
		
		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute#getName()
	 */
	@Override
	public String getName() {
		
		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.attribute.ItemFeatureAttribute#validate()
	 */
	@Override
	public Boolean validate() {
		if( !this.hasMain() && !this.hasPrecision() ) {
			return false;
		}
		
		if( null != this.hasMain() && MAX_LEN_MAIN < this.main.length() ) {
			return false;
		}
		
		if( null != this.hasPrecision() && MAX_LEN_PRECISION < this.precision.length() ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	private Boolean hasMain() {
		return (null != this.main && !this.main.isEmpty());
	}
	
	private Boolean hasPrecision() {
		return (null != this.precision && !this.precision.isEmpty());
	}
}
