/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeature;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.ShipmentFeatureAttribute;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The attribute of {@link ShipmentFeature} used to represent a date.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-features">Shipment Features</a>
 */
public class ShipmentFeatureAttributeDate implements ShipmentFeatureAttribute<String>, IsSerializable {
	public static final String ATTR_DATE	= "date";
	
	/**
	 * 
	 * The object builder for {@link ShipmentFeatureAttributeDate}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ShipmentFeatureAttribute.Builder<String> {
		private ShipmentFeatureAttributeDate result = null;

		private Builder() {
			this.result = new ShipmentFeatureAttributeDate();
		}

		
		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.ShipmentFeatureAttribute.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}
		
		public Builder setValue(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ShipmentFeatureAttributeDate build() {
			return this.result;
		}

	}

	private com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date value	= null;
	
	/**
	 * 
	 */
	private ShipmentFeatureAttributeDate() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.ShipmentFeatureAttribute#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		if(null == this.value) {
			this.value = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.Date.builder().setDate(value)
																															 .build();
			return;
		}
		
		this.value.setDate(value);
	}
	
	public void setValue(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date value) {
		this.value = value;
	}
	
	public com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date getDate(){
		return this.value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.ShipmentFeatureAttribute#getValue()
	 */
	@Override
	public String getValue() {
		return this.value.getDate();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute.ShipmentFeatureAttribute#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.value || !this.value.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
