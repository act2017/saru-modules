/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The type of address used by australia post.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @reference	<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">
 * 				Create Shipment</a>
 *
 */
public class AddressTypes implements IsSerializable{
	public static final AddressTypes MerchantLocation	= new AddressTypes("MERCHANT_LOCATION", "Merchant Location");
	
	/**
	 * An address with this type represents an Australia Post Parcel Locker.
	 */
	public static final AddressTypes ParcelLocker		= new AddressTypes("PARCEL_LOCKER", "Parcel Locker");
	
	/**
	 * An address with this type represents an Australia Post Parcel Collection location.
	 */
	public static final AddressTypes ParcelCollect		= new AddressTypes("PARCEL_COLLECT", "Parcel Collect");
	
	/**
	 * An address with this type represents a normal delivery address.
	 */
	public static final AddressTypes StandardAddress		= new AddressTypes("STANDARD_ADDRESS", "Standard Address");

	
	private static Map<String, AddressTypes> VALUES = new HashMap<>();
	
	static {
		VALUES.put(MerchantLocation.getValue(), MerchantLocation);
		VALUES.put(ParcelCollect.getValue(), ParcelCollect);
		VALUES.put(StandardAddress.getValue(), StandardAddress);
	}	

	private String value = null;
	private String display = null;
	
	private AddressTypes() {
		
	}

	private AddressTypes(String value, String display) {
		this.value = value;
		this.display = display;
	}

	public String getValue() {
		return this.value;
	}

	public String getDisplayString() {
		return this.display;
	}

	public static AddressTypes fromValue(String value) {
		if (null == value || !VALUES.containsKey(value)) {
			return null;
		}

		return VALUES.get(value);
	}
}
