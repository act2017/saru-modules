/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Country;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName;
import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber;
import com.digsarustudio.saru.erp.shipment.shared.DeliveryInstruction;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.AddressTypes;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AddressType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AustraliaPostCustomerNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The implementation of {@link Receiver} used by Australia Post Shipping API.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DomesticFreightReceiver implements Receiver, IsSerializable {
	
	/**
	 * [Mandatory]
	 * Type: String
	 */
	public static final String	ATTR_NAME	= "name";
	
	/**
	 * [Optional]
	 * Type: String
	 */
	public static final String	ATTR_APCN	= "apcn";
	
	/**
	 * [Optional]
	 * Type: String
	 */
	public static final String	ATTR_BUSINESS_NAME	= "business_name";
	
	/**
	 * [Optional]
	 * Type: String
	 */
	public static final String	ATTR_TYPE	= "type";
	
	/**
	 * [Mandatory]
	 * Type: Array
	 */
	public static final String	ATTR_LINES	= "lines";
	
	/**
	 * [Mandatory]
	 * Type: String
	 */
	public static final String	ATTR_SUBURB	= "suburb";
	
	/**
	 * [Mandatory] for domestic
	 * Type: String
	 */
	public static final String	ATTR_STATE	= "state";
	
	/**
	 * [Mandatory]
	 * Type: String
	 */
	public static final String	ATTR_POSTCODE	= "postcode";
	
	/**
	 * [Optional]
	 * Type: String
	 */
	public static final String	ATTR_COUNRY	= "country";
	
	/**
	 * [Optional]
	 * Type: String
	 */
	public static final String	ATTR_PHONE	= "phone";
	
	/**
	 * [Optional]
	 * Type: String
	 */
	public static final String	ATTR_EMAIL	= "email";
	
	/**
	 * [Optional]
	 * Type: String
	 */
	public static final String	ATTR_DELIVERY_INSTRUCTIONS	= "delivery_instructions";
	
	/**
	 * 
	 * The object builder for {@link DomesticFreightReceiver}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements Receiver.Builder {
		private DomesticFreightReceiver result = null;

		private Builder() {
			this.result = new DomesticFreightReceiver();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver.Builder#setName(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName)
		 */
		@Override
		public Builder setName(PersonalName name) {
			this.result.setName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver.Builder#setAPCN(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AustraliaPostCustomerNumber)
		 */
		@Override
		public Builder setAPCN(AustraliaPostCustomerNumber number) {
			this.result.setAPCN(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver.Builder#setBusinessName(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName)
		 */
		@Override
		public Builder setBusinessName(BusinessName name) {
			this.result.setBusinessName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver.Builder#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.AddressTypes)
		 */
		@Override
		public Builder setType(AddressTypes type) {
			this.result.setType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver.Builder#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AddressType)
		 */
		@Override
		public Builder setType(AddressType type) {
			this.result.setType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver.Builder#setAddressLines(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines)
		 */
		@Override
		public Builder setAddressLines(AddressLines addresslines) {
			this.result.setAddressLines(addresslines);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver.Builder#setSuburb(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb)
		 */
		@Override
		public Builder setSuburb(Suburb suburb) {
			this.result.setSuburb(suburb);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver.Builder#setState(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State)
		 */
		@Override
		public Builder setState(State state) {
			this.result.setState(state);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver.Builder#setPostcode(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode)
		 */
		@Override
		public Builder setPostcode(Postcode postcode) {
			this.result.setPostcode(postcode);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver.Builder#setCountry(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Country)
		 */
		@Override
		public Builder setCountry(Country country) {
			this.result.setCountry(country);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver.Builder#setPhone(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber)
		 */
		@Override
		public Builder setPhone(PhoneNumber number) {
			this.result.setPhone(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver.Builder#setEmail(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress)
		 */
		@Override
		public Builder setEmail(EmailAddress email) {
			this.result.setEmail(email);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver.Builder#setDeliveryInstructions(com.digsarustudio.saru.erp.shipment.shared.DeliveryInstruction)
		 */
		@Override
		public Builder setDeliveryInstructions(DeliveryInstruction instruction) {
			this.result.setDeliveryInstructions(instruction);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DomesticFreightReceiver build() {
			return this.result;
		}

	}

	private PersonalName					name					= null;
	private AustraliaPostCustomerNumber		apcn					= null;
	private BusinessName					business_name			= null;
	private AddressType						type					= null;
	private AddressLines					lines					= null;
	private Suburb							suburb					= null;
	private State							state					= null;
	private Postcode						postcode				= null;
	private Country							country					= null;
	private PhoneNumber						phone					= null;
	private EmailAddress					email					= null;
	private DeliveryInstruction				delivery_instructions	= null;
	
	/**
	 * 
	 */
	private DomesticFreightReceiver() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#setName(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.PersonalName)
	 */
	@Override
	public void setName(PersonalName name) {
		this.name = name;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#getName()
	 */
	@Override
	public PersonalName getName() {

		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#setAPCN(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AustraliaPostCustomerNumber)
	 */
	@Override
	public void setAPCN(AustraliaPostCustomerNumber number) {
		this.apcn = number;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#getAPCN()
	 */
	@Override
	public AustraliaPostCustomerNumber getAPCN() {

		return this.apcn;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#setBusinessName(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.naming.BusinessName)
	 */
	@Override
	public void setBusinessName(BusinessName name) {
		this.business_name = name;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#getBusinessName()
	 */
	@Override
	public BusinessName getBusinessName() {

		return this.business_name;
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.AddressTypes)
	 */
	@Override
	public void setType(AddressTypes type) {
		this.setType(AusPostShipmentAddressType.builder().setType(type)
														 .build());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#setType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AddressType)
	 */
	@Override
	public void setType(AddressType type) {
		this.type = type;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#getType()
	 */
	@Override
	public AddressType getType() {
	
		return this.type;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#getAddressType()
	 */
	@Override
	public AddressTypes getAddressType() {
	
		return this.type.getAddressType();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#setAddressLines(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.AddressLines)
	 */
	@Override
	public void setAddressLines(AddressLines addresslines) {
		this.lines = addresslines;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#getAddressLines()
	 */
	@Override
	public AddressLines getAddressLines() {

		return this.lines;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#setSuburb(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Suburb)
	 */
	@Override
	public void setSuburb(Suburb suburb) {
		this.suburb = suburb;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#getSuburb()
	 */
	@Override
	public Suburb getSuburb() {

		return this.suburb;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#setState(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.State)
	 */
	@Override
	public void setState(State state) {
		this.state = state;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#getState()
	 */
	@Override
	public State getState() {

		return this.state;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#setPostcode(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Postcode)
	 */
	@Override
	public void setPostcode(Postcode postcode) {
		this.postcode = postcode;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#getPostcode()
	 */
	@Override
	public Postcode getPostcode() {

		return this.postcode;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#setCountry(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Country)
	 */
	@Override
	public void setCountry(Country country) {
		this.country = country;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#getCountry()
	 */
	@Override
	public Country getCountry() {

		return this.country;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#setPhone(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.phone.PhoneNumber)
	 */
	@Override
	public void setPhone(PhoneNumber number) {
		this.phone = number;		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#getPhone()
	 */
	@Override
	public PhoneNumber getPhone() {

		return this.phone;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#setEmail(com.digsarustudio.saru.erp.musaceae.shared.contactInfo.EmailAddress)
	 */
	@Override
	public void setEmail(EmailAddress email) {
		this.email = email;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#getEmail()
	 */
	@Override
	public EmailAddress getEmail() {

		return this.email;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#setDeliveryInstructions(com.digsarustudio.saru.erp.shipment.shared.DeliveryInstruction)
	 */
	@Override
	public void setDeliveryInstructions(DeliveryInstruction instruction) {
		this.delivery_instructions = instruction;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#getDeliveryInstructions()
	 */
	@Override
	public DeliveryInstruction getDeliveryInstructions() {

		return this.delivery_instructions;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.name || !this.name.validate()) {
			return false;
		}
		
		if(null != this.apcn && !this.apcn.validate()) {
			return false;
		}
		
		if(null != this.business_name && !this.business_name.validate()) {
			return false;
		}
		
		if(null != this.type && !this.type.validate()) {
			return false;
		}
		
		if(null == this.lines || !this.lines.validate()) {
			return false;
		}
		
		if(null == this.suburb || !this.suburb.validate()) {
			return false;
		}
		
		if(null == this.state || !this.state.validate()) {
			return false;
		}
		
		if(null == this.postcode || !this.postcode.validate()) {
			return false;
		}
		
		if(null != this.country && !this.country.validate()) {
			return false;
		}
		
		if(null != this.phone && !this.phone.validate()) {
			return false;
		}
		
		if(null != this.email && !this.email.validate()) {
			return false;
		}
		
		if(null != this.delivery_instructions && !this.delivery_instructions.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
