/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The value in Australian Dollars of this described content.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface ItemValue {
	/**
	 * 
	 * The builder for {@link ItemValue}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemValue> {
		Builder setValue(Integer dollars);
		Builder setValue(Integer dollars, Integer cents);
	}
	
	void setValue(Integer dollars);
	void setValue(Integer dollars, Integer cents);
	
	Integer getDollars();
	Integer getCents();
	
	String getValue();
	
	Boolean validate();
}
