/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.attribute;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Time;

/**
 * Delivery time window which has a start date and an end date.
 * <ul>
 * 	<li>start - Start time of window. Time should be supplied in the following format hh24:mm:ss and should be before the end time.</li>
 *  <li>end - End time of window. Time should be supplied in the following format hh24:mm:ss and should be after the start time.</li>
 * </ul>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface DeliveryWindow {
	/**
	 * 
	 * The builder for {@link DeliveryWindow}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<DeliveryWindow> {
		Builder setStartTime(Time time);
		Builder setEndTime(Time time);
	}
	
	void setStartTime(Time time);
	void setEndTime(Time time);
	
	Time getStartTime();
	Time getEndTime();
	
	Boolean validate();
}
