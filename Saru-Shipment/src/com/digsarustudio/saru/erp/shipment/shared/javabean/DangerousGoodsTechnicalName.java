/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.javabean;

import com.digsarustudio.saru.erp.shipment.shared.ProperShippingName;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The technical name of dangerous goods used by {@link DangerousGoods}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DangerousGoodsTechnicalName implements ProperShippingName, IsSerializable {
	public static final Integer MAX_LEN	= 35;
	
	/**
	 * 
	 * The object builder for {@link DangerousGoodsTechnicalName}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ProperShippingName.Builder {
		private DangerousGoodsTechnicalName result = null;

		private Builder() {
			this.result = new DangerousGoodsTechnicalName();
		}

		
		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.ProperShippingName.Builder#setName(java.lang.String)
		 */
		@Override
		public Builder setName(String name) {
			this.result.setName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DangerousGoodsTechnicalName build() {
			return this.result;
		}

	}

	private String name	= null;
	
	/**
	 * 
	 */
	private DangerousGoodsTechnicalName() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.ProperShippingName#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.ProperShippingName#getName()
	 */
	@Override
	public String getName() {
		
		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.ProperShippingName#validate()
	 */
	@Override
	public Boolean validate() {
		if( null == this.name || this.name.isEmpty() ) {
			return false;
		}
		
		if( MAX_LEN < this.name.length() ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
