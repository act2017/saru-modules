/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AuthorisationNumber}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AuthorisationNumber implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AuthorisationNumber
										  , IsSerializable {
	public static final Integer MAX_LEN	= 10;

	/**
	 * 
	 * The object builder for {@link AuthorissationNumber}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AuthorisationNumber.Builder {
		private AuthorisationNumber result = null;

		private Builder() {
			this.result = new AuthorisationNumber();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AuthorisationNumber.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String number) {
			this.result.setValue(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AuthorisationNumber build() {
			return this.result;
		}

	}
	
	private String number = null;
	
	/**
	 * 
	 */
	private AuthorisationNumber() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AuthorisationNumber#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String number) {
		this.number = number;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AuthorisationNumber#getValue()
	 */
	@Override
	public String getValue() {
		
		return this.number;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AuthorisationNumber#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.number || this.number.isEmpty()) {
			return false;
		}
		
		if( MAX_LEN < this.number.length() ) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
