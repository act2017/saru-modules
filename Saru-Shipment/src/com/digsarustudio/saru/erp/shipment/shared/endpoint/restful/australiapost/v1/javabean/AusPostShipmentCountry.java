/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Country;
import com.digsarustudio.saru.erp.shipment.shared.CountryCodes;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The {@link Country} used by Australia Post Shipment API.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AusPostShipmentCountry implements Country, IsSerializable {
	public static final Integer MAX_LEN	= 2;
	
	/**
	 * 
	 * The object builder for {@link AusPostShipmentCountry}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements Country.Builder {
		private AusPostShipmentCountry result = null;

		private Builder() {
			this.result = new AusPostShipmentCountry();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Country.Builder#setCode(java.lang.String)
		 */
		@Override
		public Builder setCode(String code) {
			this.result.setCode(code);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Country.Builder#setName(java.lang.String)
		 */
		@Override
		public Builder setName(String name) {
			this.result.setName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AusPostShipmentCountry build() {
			return this.result;
		}

	}

	private String	country	= null;
	
	/**
	 * 
	 */
	private AusPostShipmentCountry() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Country#setCode(java.lang.String)
	 */
	@Override
	public void setCode(String code) {
		this.country = code;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Country#getCode()
	 */
	@Override
	public String getCode() {
		
		return this.country;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Country#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Country#getName()
	 */
	@Override
	public String getName() {
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.musaceae.shared.contactInfo.address.Country#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.country || this.country.isEmpty()) {
			return false;
		}
		
		if(MAX_LEN < this.country.length()) {
			return false;
		}
		
		return true;
	}
	
	public static AusPostShipmentCountry copy(Country source) {
		if(null == source) {
			return null;
		}

		String countryCode = null;
		if(null != source.getName() && !source.getName().isEmpty()) {
			CountryCodes code = CountryCodes.fromName(source.getName());
			if(null != code) {
				countryCode = code.getValue();
			}
		}
		
		AusPostShipmentCountry.Builder builder = AusPostShipmentCountry.builder();		
		builder.setCode(countryCode);
		
		AusPostShipmentCountry rtn = builder.build();
		
		if(null != rtn.getCode() && rtn.validate()) {
			countryCode = source.getCode();
		}else {
			CountryCodes code = CountryCodes.fromName(source.getCode());
			if(null != code) {
				countryCode = code.getValue();
				rtn.setCode(countryCode);
			}				
		}
		
		//Use this to debug
		if(null == countryCode || countryCode.isEmpty()) {
			rtn.setCode(source.getCode());
		}
		
		return rtn;
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}

}
