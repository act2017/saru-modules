/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The width of the parcel in centimetres. If volumetric pricing applies then this is mandatory.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
 *
 */
public interface ItemWidth {
	public static final String DECIMAL_DELIMITER	= "\\.";
	public static final String DECIMAL_GLUE			= ".";
	
	/**
	 * 
	 * The builder for {@link ItemWidth}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemWidth> {
		Builder setWidth(Integer width);
		Builder setWidth(Integer width, Integer precision);
		Builder setWidth(String length);
		Builder setWidth(String width, String precision);
	}
	
	void setWidth(Integer width);
	void setWidth(Integer width, Integer precision);
	void setWidth(String length);
	void setWidth(String width, String precision);
	
	Integer getWidth();
	Integer getPrecision();
	String getDecimalWidth();
	
	Boolean validate();
}
