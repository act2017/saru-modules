/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.label.preference.LabelPreferenceTypes;

/**
 * The type "PRINT" of {@link LabelPreference}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LabelPreferenceTypePrint extends LabelPreferenceType {

	/**
	 * 
	 */
	public LabelPreferenceTypePrint() {
		this.setValue(LabelPreferenceTypes.Print.getValue());
	}

}
