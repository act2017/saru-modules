/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The quantity of this described content.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface ItemContentQuantity {
	/**
	 * 
	 * The builder for {@link ItemContentQuantity}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ItemContentQuantity> {
		Builder setValue(String value);
		Builder setValue(Integer value);
	}

	void setValue(String value);
	void setValue(Integer value);
	Integer getValue();
	
	Boolean validate();
}
