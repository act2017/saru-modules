/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The java bean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentReference}<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentReference
 */
public class ShipmentReference implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentReference
										, IsSerializable {
	/**
	 * @see <a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a>
	 */
	public static final Integer	LEN_VALUE	= 50;

	/**
	 * 
	 * The object builder for {@link ShipmentReference}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentReference.Builder {
		private ShipmentReference result = null;

		private Builder() {
			this.result = new ShipmentReference();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentReference.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ShipmentReference build() {
			return this.result;
		}
	}
	
	private String reference	= null;
	
	/**
	 * 
	 */
	private ShipmentReference() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentReference#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String value) {
		this.reference = value;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentReference#getValue()
	 */
	@Override
	public String getValue() {
		
		return this.reference;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentReference#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.reference || this.reference.isEmpty()) {
			return false;
		}
		
		if(!this.validateLength(this.reference)) {
			return false;
		}
		
		return true;
	}

	private Boolean validateLength(String value) {
		if(LEN_VALUE < value.length()) {
			return false;
		}
		
		return true;
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
