/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentCollection;

/**
 * The order of {@link Shipment}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Order {
	/**
	 * 
	 * The builder for {@link Order}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Order> {
		Builder setOrderId(OrderId id);
		Builder setOrderReference(OrderReference reference);
		Builder setOrderCreationDate(DateTime date);	
		Builder setOrderSummary(OrderSummary summary);	
		Builder addShipment(Shipment shipment);
		Builder setShipments(List<Shipment> shipments);
		Builder setShipments(ShipmentCollection shipments);	
		Builder setPaymentMethod(PaymentMethod method);
		
		Builder setConsignor(Consignor consignor);
	}
	
	void setOrderId(OrderId id);
	void setOrderReference(OrderReference reference);
	void setOrderCreationDate(DateTime date);	
	void setOrderSummary(OrderSummary summary);	
	void addShipment(Shipment shipment);
	void setShipments(List<Shipment> shipments);
	void setShipments(ShipmentCollection shipments);	
	void setPaymentMethod(PaymentMethod method);
	
	void setConsignor(Consignor consignor);
	
	OrderId 			getOrderId();
	OrderReference 		getOrderReference();
	DateTime 			getOrderCreationDate();	
	OrderSummary 		getOrderSummary();	
	Shipment 			getShipment(Integer index);
	ShipmentCollection 	getShipments();	
	PaymentMethod	 	getPaymentMethod();
	
	Consignor getConsignor();
	
	Boolean validate();
}
