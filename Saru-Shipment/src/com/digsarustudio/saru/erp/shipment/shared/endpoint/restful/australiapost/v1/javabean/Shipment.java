/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AuthorisationNumber;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescription;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescriptionCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.MovementType;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentId;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentPagination;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentReference;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeatureCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemCollection;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.order.OrderId;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @see			<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">Create Shipments</a><br>
 * 				<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/get-shipments">Get Shipments</a>
 *
 */
public class Shipment implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment
								, IsSerializable {
	/**
	 * [Optional] .<br>
	 * 
	 * <br>
	 * Type: String
	 */
	public static final String	ATTR_SHIPMENT_REFERENCE	= "shipment_reference";

	/**
	 * [Optional] .<br>
	 * 
	 * <br>
	 * Type: Array
	 */
	public static final String	ATTR_SENDER_REFERENCE	= "sender_references";

	/**
	 * [Optional] .<br>
	 * 
	 * <br>
	 * Type: Array
	 */
	public static final String	ATTR_GOODS_DESCRIPTIONS	= "goods_descriptions";

	/**
	 * [Optional] .<br>
	 * Despatch Date of a shipment. Date should be supplied in the following format yyyy-MM-dd.<br>
	 * Note - Despatch Date can not be more than 14 days in past or more than 14 days in future.
	 * <br>
	 * Type: String
	 */
	public static final String	ATTR_DESPATCH_DATE	= "despatch_date";

	/**
	 * [Optional] if unassigned.<br>
	 * defaults to true for StarTrack products, otherwise defaults to false<br>
	 * <br>
	 * If set true, the system attempts to search for existing pending shipments to consolidate this shipment with. 
	 * If set to false, a new shipment is created as per normal workflow. For further information on consolidation, 
	 * please refer to <a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/shipment-consolidation">Shipment Consolidation</a>.
	 * <br>
	 * Type: Boolean
	 */
	public static final String	ATTR_CONSOLIDATE	= "consolidate";
	
	/**
	 * [Optional] .<br>
	 * for some products this defaults to true<br>
	 * Whether the recipient of the shipment will receive tracking notification email. 
	 * The email address of the receiver must be provided if the email_tracking_enabled field is set to true.
	 * <br>
	 * Type: Boolean
	 */
	public static final String	ATTR_EMAIL_TRACKING_ENABLED	= "email_tracking_enabled";

	/**
	 * [Mandatory] .<br>
	 * 
	 * <br>
	 * Type: Object
	 */
	public static final String	ATTR_FROM	= "from";

	/**
	 * [Mandatory] .<br>
	 * 
	 * <br>
	 * Type: Object
	 */
	public static final String	ATTR_TO	= "to";

	/**
	 * [Optional] .<br>
	 * Dangerous Goods (DG) information. This information is processed by StarTrack products. 
	 * The DG information will be included in the PDF for dangerous goods (when product is EXP). 
	 * For Premium products, customers will need to download the IATA DG document and complete it themselves. 
	 * DG Information is mandatory for StarTrack products when one or more items being sent are marked as dangerous (see the "contains_dangerous_goods" field.)
	 * <br>
	 * Type: Object
	 */
	public static final String	ATTR_DANGEROUS_GOODS	= "dangerous_goods";

	/**
	 * [Optional] .<br>
	 * 
	 * <br>
	 * Type: Object
	 */
	public static final String	ATTR_MOVEMENT_TYPE	= "movement_type";

	/**
	 * [Optional] .<br>
	 * 
	 * <br>
	 * Type: Object
	 */
	public static final String	ATTR_FEATURES	= "features";

	/**
	 * [Optional] .<br>
	 * The merchant's internal reference ID for a returns or transfer shipment. This is mandatory for StarTrack if movement_type is "RETURN" and "TRANSFER".
	 * <br>
	 * Type: String
	 */
	public static final String	ATTR_AUTHORISATION_NUMBER	= "authorisation_number";

	/**
	 * [Mandatory] .<br>
	 * 
	 * <br>
	 * Type: Array
	 */
	public static final String	ATTR_ITEMS	= "items";
	
	//==== Response ====
	/**
	 * 
	 * Type: String
	 */
	public static final String	ATTR_SHIPMENT_ID	= "shipment_id";	

	/**
	 * 
	 * Type: Date and Time
	 */
	public static final String	ATTR_SHIPMENT_CREATION_DATE	= "shipment_creation_date";	

	/**
	 * 
	 * Type: Object
	 */
	public static final String	ATTR_SHIPMENT_SUMMARY	= "shipment_summary";
	
	//==== Get Shipment Response ====

	/**
	 * 
	 * Type: String
	 */	
	public static final	String	ATTR_ORDER_ID			= "order_id";

	/**
	 * 
	 * Type: Object
	 */	
	public static final	String	ATTR_PAGINATION			= "pagination";
	
	/**
	 * 
	 * The object builder for {@link Shipment}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder {
		private Shipment result = null;

		private Builder() {
			this.result = new Shipment();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#setShipmentReference(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentReference)
		 */
		@Override
		public Builder setShipmentReference(ShipmentReference reference) {
			this.result.setShipmentReference(reference);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#setSenderReferences(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection)
		 */
		@Override
		public Builder setSenderReferences(SenderReferenceCollection senderReferences) {
			this.result.setSenderReferences(senderReferences);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#setGoodsDescriptions(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescriptionCollection)
		 */
		@Override
		public Builder setGoodsDescriptions(GoodsDescriptionCollection desces) {
			this.result.setGoodsDescriptions(desces);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#addGoodsDescription(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescription)
		 */
		@Override
		public Builder addGoodsDescription(GoodsDescription desc) {
			this.result.addGoodsDescription(desc);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#setDespatchDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date)
		 */
		@Override
		public Builder setDespatchDate(Date date) {
			this.result.setDespatchDate(date);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#setConsolidate(java.lang.Boolean)
		 */
		@Override
		public Builder setConsolidate(Boolean consolidate) {
			this.result.setConsolidate(consolidate);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#setEmailTracking(java.lang.Boolean)
		 */
		@Override
		public Builder setEmailTracking(Boolean enabled) {
			this.result.setEmailTracking(enabled);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#setSender(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender)
		 */
		@Override
		public Builder setSender(Sender sender) {
			this.result.setSender(sender);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#setReceiver(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver)
		 */
		@Override
		public Builder setReceiver(Receiver receiver) {
			this.result.setReceiver(receiver);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#setDangerousGoods(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods)
		 */
		@Override
		public Builder setDangerousGoods(DangerousGoods dangerousGoods) {
			this.result.setDangerousGoods(dangerousGoods);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#setMovementType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.MovementType)
		 */
		@Override
		public Builder setMovementType(MovementType type) {
			this.result.setMovementType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#setShipmentFeatures(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeatureCollection)
		 */
		@Override
		public Builder setShipmentFeatures(ShipmentFeatureCollection features) {
			this.result.setShipmentFeatures(features);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#setAuthorisationNumber(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AuthorisationNumber)
		 */
		@Override
		public Builder setAuthorisationNumber(AuthorisationNumber number) {
			this.result.setAuthorisationNumber(number);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#setItems(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemCollection)
		 */
		@Override
		public Builder setItems(ItemCollection items) {
			this.result.setItems(items);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#addItem(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item)
		 */
		@Override
		public Builder addItem(Item item) {
			this.result.addItem(item);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#setShipmentId(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentId)
		 */
		@Override
		public Builder setShipmentId(ShipmentId id) {
			this.result.setShipmentId(id);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#setShipmentCreationDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime)
		 */
		@Override
		public Builder setShipmentCreationDate(DateTime date) {
			this.result.setShipmentCreationDate(date);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#setShipmentSummary(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary)
		 */
		@Override
		public Builder setShipmentSummary(ShipmentSummary summary) {
			this.result.setShipmentSummary(summary);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#setOrderId(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderId)
		 */
		@Override
		public Builder setOrderId(OrderId id) {
			this.result.setOrderId(id);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment.Builder#setPagination(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentPagination)
		 */
		@Override
		public Builder setPagination(ShipmentPagination pagination) {
			this.result.setPagination(pagination);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Shipment build() {
			return this.result;
		}

	}
	
	private ShipmentReference			shipment_reference		= null;
	private SenderReferenceCollection	sender_references		= null;
	private GoodsDescriptionCollection	goods_descriptions		= null;
	private Date						despatch_date			= null;
	private Boolean						consolidate				= null;
	private Boolean						email_tracking_enabled	= null;
	private Sender						from					= null;
	private Receiver					to						= null;
	private DangerousGoods				dangerous_goods			= null;
	private MovementType				movement_type			= null;
	private	ShipmentFeatureCollection	features				= null;
	private	AuthorisationNumber			authorisation_number	= null;
	private	ItemCollection				items					= null;
	
	//==== Response ====
	private	ShipmentId					shipment_id				= null;
	private	DateTime					shipment_creation_date	= null;
	private	ShipmentSummary				shipment_summary		= null;
	
		//=== Get Shipment ===
	private OrderId						order_id				= null;
	private ShipmentPagination			pagination				= null;
		
	/**
	 * 
	 */
	private Shipment() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#setShipmentReference(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentReference)
	 */
	@Override
	public void setShipmentReference(ShipmentReference reference) {
		this.shipment_reference = reference;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#setSenderReferences(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.SenderReferenceCollection)
	 */
	@Override
	public void setSenderReferences(SenderReferenceCollection senderReferences) {
		this.sender_references = senderReferences;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#setGoodsDescriptions(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescriptionCollection)
	 */
	@Override
	public void setGoodsDescriptions(GoodsDescriptionCollection desces) {
		this.goods_descriptions = desces;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#addGoodsDescription(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.GoodsDescription)
	 */
	@Override
	public void addGoodsDescription(GoodsDescription desc) {
		if(null == this.goods_descriptions) {
			this.goods_descriptions = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.GoodsDescriptionCollection.builder().build();
		}
		
		this.goods_descriptions.addDescription(desc);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#setDespatchDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Date)
	 */
	@Override
	public void setDespatchDate(Date date) {
		this.despatch_date = date;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#setConsolidate(java.lang.Boolean)
	 */
	@Override
	public void setConsolidate(Boolean consolidate) {
		this.consolidate = consolidate;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#setEmailTracking(java.lang.Boolean)
	 */
	@Override
	public void setEmailTracking(Boolean enabled) {
		this.email_tracking_enabled = enabled;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#setSender(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Sender)
	 */
	@Override
	public void setSender(Sender sender) {
		this.from = sender;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#setReceiver(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Receiver)
	 */
	@Override
	public void setReceiver(Receiver receiver) {
		this.to = receiver;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#setDangerousGoods(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DangerousGoods)
	 */
	@Override
	public void setDangerousGoods(DangerousGoods dangerousGoods) {
		this.dangerous_goods = dangerousGoods;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#setMovementType(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.MovementType)
	 */
	@Override
	public void setMovementType(MovementType type) {
		this.movement_type = type;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#setShipmentFeatures(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.feature.ShipmentFeatureCollection)
	 */
	@Override
	public void setShipmentFeatures(ShipmentFeatureCollection features) {
		this.features = features;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#setAuthorisationNumber(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AuthorisationNumber)
	 */
	@Override
	public void setAuthorisationNumber(AuthorisationNumber number) {
		this.authorisation_number = number;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#setItems(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.ItemCollection)
	 */
	@Override
	public void setItems(ItemCollection items) {
		this.items = items;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#addItem(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.Item)
	 */
	@Override
	public void addItem(Item item) {
		if(null == this.items) {
			this.items = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.javabean.ItemCollection.builder().build();
		}

		this.items.addItem(item);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#getShipmentId()
	 */
	@Override
	public ShipmentId getShipmentId() {
		
		return this.shipment_id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#getShipmentReference()
	 */
	@Override
	public ShipmentReference getShipmentReference() {
		
		return this.shipment_reference;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#getShipmentCreationDate()
	 */
	@Override
	public DateTime getShipmentCreationDate() {
		
		return this.shipment_creation_date;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#isEmailTrackingEnabled()
	 */
	@Override
	public Boolean isEmailTrackingEnabled() {
		
		return this.email_tracking_enabled;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#getDespatchDate()
	 */
	@Override
	public Date getDespatchDate() {
		return this.despatch_date;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#isConsolidate()
	 */
	@Override
	public Boolean isConsolidate() {
		return this.consolidate;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#getItems()
	 */
	@Override
	public ItemCollection getItems() {
		
		return this.items;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#getMovementType()
	 */
	@Override
	public MovementType getMovementType() {
		
		return this.movement_type;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#getAuthorisationNumber()
	 */
	@Override
	public AuthorisationNumber getAuthorisationNumber() {
		
		return this.authorisation_number;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#getShipmentSummary()
	 */
	@Override
	public ShipmentSummary getShipmentSummary() {
		
		return this.shipment_summary;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#setShipmentId(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentId)
	 */
	@Override
	public void setShipmentId(ShipmentId id) {
		this.shipment_id = id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#setShipmentCreationDate(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.DateTime)
	 */
	@Override
	public void setShipmentCreationDate(DateTime date) {
		this.shipment_creation_date = date;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#setShipmentSummary(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentSummary)
	 */
	@Override
	public void setShipmentSummary(ShipmentSummary summary) {
		this.shipment_summary = summary;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#getSenderReferences()
	 */
	@Override
	public SenderReferenceCollection getSenderReferences() {

		return this.sender_references;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#getGoodsDescriptions()
	 */
	@Override
	public GoodsDescriptionCollection getGoodsDescriptions() {

		return this.goods_descriptions;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#getGoodsDescription(java.lang.Integer)
	 */
	@Override
	public GoodsDescription getGoodsDescription(Integer index) {
		if(null == this.goods_descriptions || null == index || index >= this.goods_descriptions.size()) {
			return null;
		}

		return this.goods_descriptions.getDescription(index);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#getSender()
	 */
	@Override
	public Sender getSender() {

		return this.from;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#getReceiver()
	 */
	@Override
	public Receiver getReceiver() {

		return this.to;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#getDangerousGoods()
	 */
	@Override
	public DangerousGoods getDangerousGoods() {

		return this.dangerous_goods;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#getShipmentFeatures()
	 */
	@Override
	public ShipmentFeatureCollection getShipmentFeatures() {

		return this.features;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#getOrderId()
	 */
	@Override
	public OrderId getOrderId() {

		return this.order_id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#setOrderId(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.OrderId)
	 */
	@Override
	public void setOrderId(OrderId id) {
		this.order_id = id;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#getPagination()
	 */
	@Override
	public ShipmentPagination getPagination() {

		return this.pagination;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#setPagination(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.ShipmentPagination)
	 */
	@Override
	public void setPagination(ShipmentPagination pagination) {
		this.pagination = pagination;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.Shipment#validate()
	 */
	@Override
	public Boolean validate() {
		if( null != this.shipment_reference && !this.shipment_reference.validate() ) {
			return false;
		}
		
		if( null != this.sender_references && !this.sender_references.validate() ) {
			return false;
		}
		
		if( null != this.goods_descriptions && !this.goods_descriptions.validate() ) {
			return false;
		}
		
		if( null == this.from || !this.from.validate() ) {
			return false;
		}
		
		if( null == this.to || !this.to.validate() ) {
			return false;
		}
		
		if( null != this.dangerous_goods && !this.dangerous_goods.validate() ) {
			return false;
		}
		
		if( null != this.movement_type && !this.movement_type.validate() ) {
			return false;
		}
		
		if( null != this.features && !this.features.validate() ) {
			return false;
		}
		
		if( null == this.items || !this.items.validate() ) {
			return false;
		}
		
		if( null != this.shipment_id && !this.shipment_id.validate()) {
			return false;
		}
		
		if( null != this.order_id && !this.order_id.validate()) {
			return false;
		}
		
		if( null != this.pagination && !this.pagination.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
