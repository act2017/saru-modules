/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentWeight}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemContentWeight implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentWeight
										, IsSerializable {
	public static final Integer	MAX_LEN_PRECISION	= 3;
	public static final String	DECIMAL_DELIMITER	= "\\.";
	public static final String	DECIMAL_GLUE		= ".";
	
	/**
	 * 
	 * The object builder for {@link ItemContentWeight}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentWeight.Builder{
		private ItemContentWeight result = null;

		private Builder() {
			this.result = new ItemContentWeight();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentWeight.Builder#setWeight(java.lang.Integer)
		 */
		@Override
		public Builder setWeight(Integer weight) {
			this.result.setWeight(weight);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentWeight.Builder#setWeight(java.lang.Integer, java.lang.Integer)
		 */
		@Override
		public Builder setWeight(Integer weight, Integer precision) {
			this.result.setWeight(weight, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentWeight.Builder#setWeight(java.lang.String)
		 */
		@Override
		public Builder setWeight(String weight) {
			this.result.setWeight(weight);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentWeight.Builder#setWeight(java.lang.String, java.lang.String)
		 */
		@Override
		public Builder setWeight(String weight, String precision) {
			this.result.setWeight(weight, precision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemContentWeight build() {
			return this.result;
		}

	}
	
	private String main			= null;
	private String precision	= null;
	
	/**
	 * 
	 */
	private ItemContentWeight() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentWeight#setWeight(java.lang.Integer)
	 */
	@Override
	public void setWeight(Integer weight) {
		this.setWeight(weight, null);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentWeight#setWeight(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void setWeight(Integer weight, Integer precision) {
		String main = (null == weight) ? null : weight.toString();
		String pre	= (null == precision) ? null : precision.toString();

		this.setWeight(main, pre);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentWeight#setWeight(java.lang.String)
	 */
	@Override
	public void setWeight(String weight) {
		if(null == weight) {
			return;
		}
		
		String[] weights = weight.split(DECIMAL_DELIMITER);
		
		String main = weights[0];
		String pre	= null;
		
		if(2 == weights.length) {
			pre = weights[1];
		}
		
		this.setWeight(main, pre);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentWeight#setWeight(java.lang.String, java.lang.String)
	 */
	@Override
	public void setWeight(String weight, String precision) {
		this.main		= weight;
		this.precision	= precision;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentWeight#getWeight()
	 */
	@Override
	public Integer getWeight() {
		if(null == this.main) {
			return null;
		}
		
		return Integer.parseInt(this.main);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentWeight#getPrecision()
	 */
	@Override
	public Integer getPrecision() {
		if(null == this.precision) {
			return null;
		}
		
		return Integer.parseInt(this.precision);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentWeight#getDecimalWeight()
	 */
	@Override
	public String getDecimalWeight() {
		StringBuffer buffer = new StringBuffer();
		
		if(null == this.main || this.main.isEmpty()) {
			buffer.append("0");
		}else {
			buffer.append(this.main);
		}
		
		if(null != this.precision) {
			buffer.append(DECIMAL_GLUE);
			buffer.append(this.precision);
		}
		
		return buffer.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentWeight#validate()
	 */
	@Override
	public Boolean validate() {
		if( (null == this.main || this.main.isEmpty())
		 && (null == this.precision || this.precision.isEmpty()) ) {
			return false;
		}
		
		if(null != this.precision && MAX_LEN_PRECISION < this.precision.length()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
