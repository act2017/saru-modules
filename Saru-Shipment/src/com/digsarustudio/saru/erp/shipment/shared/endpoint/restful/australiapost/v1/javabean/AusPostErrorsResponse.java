/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean;

import java.util.List;

import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError;
import com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorsResponse}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AusPostErrorsResponse implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorsResponse
											 , IsSerializable {
	public static final String	ATTR_ERRORS	= "errors";	
	
	/**
	 * 
	 * The object builder for {@link AusPostErrorsResponse}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorsResponse.Builder {
		private AusPostErrorsResponse result = null;

		private Builder() {
			this.result = new AusPostErrorsResponse();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorsResponse.Builder#addError(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError)
		 */
		@Override
		public Builder addError(AusPostError error) {
			this.result.addError(error);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorsResponse.Builder#setErrors(java.util.List)
		 */
		@Override
		public Builder setErrors(List<AusPostError> errors) {
			this.result.setErrors(errors);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorsResponse.Builder#setErrors(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError[])
		 */
		@Override
		public Builder setErrors(AusPostError[] errors) {
			this.result.setErrors(errors);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorsResponse.Builder#setErrors(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection)
		 */
		@Override
		public Builder setErrors(AusPostErrorCollection errors) {
			this.result.setErrors(errors);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public AusPostErrorsResponse build() {
			return this.result;
		}

	}
	
	private AusPostErrorCollection	errors	= null;
	
	/**
	 * 
	 */
	private AusPostErrorsResponse() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorsResponse#addError(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError)
	 */
	@Override
	public void addError(AusPostError error) {
		if(null == this.errors) {
			this.errors = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorCollection.builder().build();
		}
		
		this.errors.addError(error);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorsResponse#setErrors(java.util.List)
	 */
	@Override
	public void setErrors(List<AusPostError> errors) {
		if(null == this.errors) {
			this.errors = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorCollection.builder().build();
		}
		
		this.errors.setErrors(errors);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorsResponse#setErrors(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostError[])
	 */
	@Override
	public void setErrors(AusPostError[] errors) {
		if(null == this.errors) {
			this.errors = com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.javabean.AusPostErrorCollection.builder().build();
		}
		
		this.errors.setErrors(errors);		

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorsResponse#getError(java.lang.Integer)
	 */
	@Override
	public AusPostError getError(Integer code) {
		if(null == code) {
			return null;
		}
		
		return this.getError(code.toString());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorsResponse#getError(java.lang.String)
	 */
	@Override
	public AusPostError getError(String code) {
		if(null == this.errors) {
			return null;
		}
		
		return this.errors.getError(code);		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorsResponse#getErrors()
	 */
	@Override
	public List<AusPostError> getErrors() {
		if(null == this.errors) {
			return null;
		}
		
		return this.errors.getErrors();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorsResponse#setErrors(com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorCollection)
	 */
	@Override
	public void setErrors(AusPostErrorCollection errors) {
		this.errors = errors;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorsResponse#getErrorCollecion()
	 */
	@Override
	public AusPostErrorCollection getErrorCollecion() {
		return this.errors;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.AusPostErrorsResponse#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.errors || !this.errors.validate()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
