/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentDescription}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ItemContentDescription implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentDescription
											 , IsSerializable {
	public static final Integer	MAX_LEN	= 40;
	
	/**
	 * 
	 * The object builder for {@link ItemContentDescription}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentDescription.Builder {
		private ItemContentDescription result = null;

		private Builder() {
			this.result = new ItemContentDescription();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentDescription.Builder#setDescription(java.lang.String)
		 */
		@Override
		public Builder setDescription(String desc) {
			this.result.setDescription(desc);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ItemContentDescription build() {
			return this.result;
		}

	}

	private String description = null;
	
	/**
	 * 
	 */
	private ItemContentDescription() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentDescription#setDescription(java.lang.String)
	 */
	@Override
	public void setDescription(String desc) {
		this.description = desc;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentDescription#getDescription()
	 */
	@Override
	public String getDescription() {
		
		return this.description;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.content.ItemContentDescription#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.description || this.description.isEmpty()) {
			return false;
		}
		
		if(MAX_LEN < this.description.length()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
