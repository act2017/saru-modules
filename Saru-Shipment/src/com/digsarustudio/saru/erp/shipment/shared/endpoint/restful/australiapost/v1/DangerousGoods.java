/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.saru.erp.musaceae.shared.Weight;
import com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsClassDivision;
import com.digsarustudio.saru.erp.shipment.shared.DangerousGoodsPackagingType;
import com.digsarustudio.saru.erp.shipment.shared.ProperShippingName;
import com.digsarustudio.saru.erp.shipment.shared.Quantity;
import com.digsarustudio.saru.erp.shipment.shared.SubsidiaryRisk;
import com.digsarustudio.saru.erp.shipment.shared.UnNumber;

/**
 * Dangerous Goods (DG) information. This information is processed by StarTrack products. 
 * The DG information will be included in the PDF for dangerous goods (when product is EXP). 
 * For Premium products, customers will need to download the IATA DG document and complete it themselves. 
 * DG Information is mandatory for StarTrack products when one or more items being sent are marked as dangerous 
 * (see the "contains_dangerous_goods" field.)
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @reference	<a href="https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments">https://developers.auspost.com.au/apis/shipping-and-tracking/reference/create-shipments</a>
 */
public interface DangerousGoods {
	/**
	 * 
	 * The builder for {@link DangerousGoods}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<DangerousGoods> {
		/**
		 * [Mandatory]<br>
		 * <br>
		 * UN numbers are numbers that identify dangerous goods in 4 digits.
		 * They are assigned by the United Nations Committee of Experts on the Transport of dangerous goods. 
		 * This field is an integer value, but must be passed as a string. Examples: 1170, 1789.
		 *  
		 * @param number The UN number in 4 digits.
		 * 
		 * @since Oct 2014
		 */
		Builder setUnNumber(UnNumber number);
		
		
		/**
		 * [Mandatory]<br>
		 * <br>
		 * Contains the technical or proper shipping name of the dangerous goods in 35 characters.
		 * Examples: ETHYL ALCOHOL, ETHANOL.<br>
		 * Normally, this name is from the database.<br>
		 *  
		 * @param name The proper shipping name of the dangerous goods.
		 * 
		 * @since Oct 2014
		 */
		Builder setTechnicalName(ProperShippingName name);
		
		/**
		 * [Mandatory]<br>
		 * <br>
		 * Contains the aggregate net quantity of the dangerous goods (whole kilos or litres) in 5 digits.
		 * Note this does not include the weight of the packages but represents only the weight of the DG itself. 
		 * This field is an integer value, but must be passed as a string.
		 *  
		 * @param weight The aggregate net quantity of the dangerous goods (whole kilos or litres) in 5 digits.
		 * 
		 * @since Oct 2014
		 */
		Builder setNetWeight(Weight weight);
		
		/**
		 * [Mandatory]<br>
		 * <br>
		 * Contains the dangerous goods class or division in 7 characters. This field is a decimal value, but must be passed as a string. 
		 * Examples: 3, 1.4, 2.1.
		 * 
		 * @param classDivision The dangerous goods class or division in 7 characters.
		 * 
		 * @since Oct 2014
		 */
		Builder setClassDivision(DangerousGoodsClassDivision classDivision);
		
		/**
		 * [Optional]<br>
		 * <br>
		 * Contains the dangerous goods subsidiary risk of sub class in 4 characters. This represent the subsidiary risk of a substance, 
		 * for example, a product may have a primary risk of toxic (class 6.1) with a subsidiary risk of flammable liquid (class 3). 
		 * This field is a decimal value, but must be passed as a string.
		 *  
		 * @param subsidiaryRisk The dangerous goods subsidiary risk of sub class in 4 characters
		 * 
		 * @since Oct 2014
		 */
		Builder setSubsidiaryRisk(SubsidiaryRisk subsidiaryRisk);

		/**
		 * [Optional]<br>
		 * <br>
		 * Only three Roman numerals allowed: I, II and III.
		 * 
		 * @param designator Only three Roman numerals allowed: I, II and III.
		 * 
		 * @since Oct 2014
		 */
		Builder setPackingGroupDesignator(PackingGroupDesignator designator);
		
		/**
		 * [Mandatory]<br>
		 * <br>
		 * Description of the outer packages in which the dangerous goods are contained in 25 characters.
		 *  
		 * @param desc
		 * 
		 * @since Oct 2014
		 */
		Builder setOuterPackingType(DangerousGoodsPackagingType type);
			
		/**
		 * [Mandatory]<br>
		 * <br>
		 * Contains the quantity of the dangerous goods in 4 digits. 
		 * This quantity is the number of outer packages of each type.<br>
		 * 
		 * @param quantity The quantity of the dangerous goods in 4 digits.
		 * 
		 * @since Oct 2014
		 */
		Builder setOuterPackagingQuantity(Quantity quantity);
	}
	
	/**
	 * [Mandatory]<br>
	 * <br>
	 * UN numbers are numbers that identify dangerous goods in 4 digits.
	 * They are assigned by the United Nations Committee of Experts on the Transport of dangerous goods. 
	 * This field is an integer value, but must be passed as a string. Examples: 1170, 1789.
	 *  
	 * @param number The UN number in 4 digits.
	 * 
	 * @since Oct 2014
	 */
	void setUnNumber(UnNumber number);
	UnNumber getUnNumber();
	
	
	/**
	 * [Mandatory]<br>
	 * <br>
	 * Contains the technical or proper shipping name of the dangerous goods in 35 characters.
	 * Examples: ETHYL ALCOHOL, ETHANOL.<br>
	 * Normally, this name is from the database.<br>
	 *  
	 * @param name The proper shipping name of the dangerous goods.
	 * 
	 * @since Oct 2014
	 */
	void setTechnicalName(ProperShippingName name);
	ProperShippingName getTechnicalName();
	
	/**
	 * [Mandatory]<br>
	 * <br>
	 * Contains the aggregate net quantity of the dangerous goods (whole kilos or litres) in 5 digits.
	 * Note this does not include the weight of the packages but represents only the weight of the DG itself. 
	 * This field is an integer value, but must be passed as a string.
	 *  
	 * @param weight The aggregate net quantity of the dangerous goods (whole kilos or litres) in 5 digits.
	 * 
	 * @since Oct 2014
	 */
	void setNetWeight(Weight weight);
	Weight getNetWeight();
	
	/**
	 * [Mandatory]<br>
	 * <br>
	 * Contains the dangerous goods class or division in 7 characters. This field is a decimal value, but must be passed as a string. 
	 * Examples: 3, 1.4, 2.1.
	 * 
	 * @param classDivision The dangerous goods class or division in 7 characters.
	 * 
	 * @since Oct 2014
	 */
	void setClassDivision(DangerousGoodsClassDivision classDivision);
	DangerousGoodsClassDivision getClassDivision();
	
	/**
	 * [Optional]<br>
	 * <br>
	 * Contains the dangerous goods subsidiary risk of sub class in 4 characters. This represent the subsidiary risk of a substance, 
	 * for example, a product may have a primary risk of toxic (class 6.1) with a subsidiary risk of flammable liquid (class 3). 
	 * This field is a decimal value, but must be passed as a string.
	 *  
	 * @param subsidiaryRisk The dangerous goods subsidiary risk of sub class in 4 characters
	 * 
	 * @since Oct 2014
	 */
	void setSubsidiaryRisk(SubsidiaryRisk subsidiaryRisk);
	SubsidiaryRisk getSubsidiaryRisk();

	/**
	 * [Optional]<br>
	 * <br>
	 * Only three Roman numerals allowed: I, II and III.
	 * 
	 * @param designator Only three Roman numerals allowed: I, II and III.
	 * 
	 * @since Oct 2014
	 */
	void setPackingGroupDesignator(PackingGroupDesignator designator);
	PackingGroupDesignator getPackingGroupDesinator();
	
	/**
	 * [Mandatory]<br>
	 * <br>
	 * Description of the outer packages in which the dangerous goods are contained in 25 characters.
	 *  
	 * @param desc
	 * 
	 * @since Oct 2014
	 */
	void setOuterPackingType(DangerousGoodsPackagingType type);
	DangerousGoodsPackagingType getOuterPackingType();
		
	/**
	 * [Mandatory]<br>
	 * <br>
	 * Contains the quantity of the dangerous goods in 4 digits. 
	 * This quantity is the number of outer packages of each type.<br>
	 * 
	 * @param quantity The quantity of the dangerous goods in 4 digits.
	 * 
	 * @since Oct 2014
	 */
	void setOuterPackagingQuantity(Quantity quantity);
	Quantity getOuterPackaginggQuantity();
	
	Boolean validate();
}
