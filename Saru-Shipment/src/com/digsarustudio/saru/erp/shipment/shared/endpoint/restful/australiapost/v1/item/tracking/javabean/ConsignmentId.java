/**
 * 
 */
package com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.javabean;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The javabean of {@link com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ConsignmentId}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ConsignmentId implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ConsignmentId
								, IsSerializable {
	public static final Integer MAX_LEN	= 100;

	/**
	 * 
	 * The object builder for {@link ConsignmentId}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ConsignmentId.Builder {
		private ConsignmentId result = null;

		private Builder() {
			this.result = new ConsignmentId();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ConsignmentId.Builder#setValue(java.lang.String)
		 */
		@Override
		public Builder setValue(String id) {
			this.result.setValue(id);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ConsignmentId build() {
			return this.result;
		}

	}
	
	private String id	= null;
	
	/**
	 * 
	 */
	private ConsignmentId() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ConsignmentId#setValue(java.lang.String)
	 */
	@Override
	public void setValue(String id) {
		this.id = id;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ConsignmentId#getValue()
	 */
	@Override
	public String getValue() {
		
		return this.id;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.saru.erp.shipment.shared.endpoint.restful.australiapost.v1.item.tracking.ConsignmentId#validate()
	 */
	@Override
	public Boolean validate() {
		if(null == this.id || this.id.isEmpty()) {
			return false;
		}
		
		if(MAX_LEN < this.id.length()) {
			return false;
		}
		
		return true;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
